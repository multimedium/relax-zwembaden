<?php

namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'relax-zwembaden');

// Project repository
set('repository', 'git@bitbucket.org:multimedium/relax-zwembaden.git');

// Files will be shared
set('shared_dirs', [
    'files/administrator',
    'files/cache',
    'files/cache-localized',
    'files/overlay',
    'files/log',
    'files/media',
    'files/temp',
    'files/thumbnail',
    'files/config',
]);

set('shared_files', [
    'config/db-connection.php'
]);

/* -- PRODUCTION -- */

host('www.relaxzwembaden.be')
    ->stage('production')
    ->hostname('web-zwartwitcommunicatie@ssh.multimedium.be')
    ->set('deploy_path', '/web/zwartwitcommunicatie/relaxzwembaden.be/www')
    ->set('branch', 'master');

/* -- DEPLOY TASKS -- */

task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// -- DO NOT TOUCH ANYTHING BEYOND THIS LINE! -- //

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Writable dirs by web server
set('writable_dirs', []); // Tigron does not require any extra
set('allow_anonymous_stats', false);

task('get_version', function() {
    return writeln(run("cd {{deploy_path}}/current && git log -1"));
});

// Tigron has aliases for the different PHP versions. Alias the version needed,
// to ensure future deployments will still use the correct version if the base
// php binary is upgraded to a newer version on the server
set('bin/php', function () {
    return 'php73';
});