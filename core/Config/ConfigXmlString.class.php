<?php
/**
 * M_ConfigXmlString class
 * 
 * This is a specific implementation of the {@link MI_Config} 
 * interface, that loads the data structure from an XML string.
 * 
 * NOTE:
 * The M_ConfigXmlString constructs out of a string, but enables you
 * to save the contained structure to a (new) XML file.
 * 
 * IMPORTANT NOTE:
 * A M_Config object is a simple structure of keys and values. 
 * Note that, if the tags in the original XML string come with 
 * attributes, they will not be parsed into the M_Config object. 
 * Once parsed into a M_Config and saved by the save() method, 
 * you will have lost all attributes!!
 * 
 * @package Core
 */
class M_ConfigXmlString extends M_ConfigXml {
	/**
	 * Constructor
	 * 
	 * Example 1
	 * <code>
	 *    $config = new M_ConfigXmlString(
	 *       '<root>
	 *           <firstname>Tom</firstname>
	 *           <surnames>Bauwens</surnames>
	 *        </root>',
	 *       M_Loader::getAbsolute('config/new.xml')
	 *    );
	 * </code>
	 * 
	 * @access public
	 * @param string $source
	 * 		The XML string that should be loaded and parsed into the 
	 * 		M_Config object.
	 * @param string $source
	 * 		The file to save the config's data structure to, if 
	 * 		{@link M_ConfigXml::save()} is called.
	 * @param boolean $lock
	 * 		Set to TRUE to lock the M_Config, FALSE to unlock. 
	 * 		Read the documentation on {@link M_Config::lock()} 
	 * 		for more info.
	 * @return M_ConfigXml
	 */
	public function __construct($xml, $source, $lock = FALSE) {
		// Add the root node to the Config object
		$this->_index = 0;
		$this->addSimpleXMLElement(simplexml_load_string($xml));
		
		// Set the source
		$this->_source = $source;
		
		// Locking the object is the last thing we do. We don't want
		// Exceptions to be thrown when writing to the config object
		// while constructing.
		$this->_locked = $lock;
	}
}
?>