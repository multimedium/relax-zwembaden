<?php
// This is a very similar, if not almost identical, implementation
// to the Zend_Config implementation. However, to benefit from 
// the observer pattern that we have implemented (IEventDispatcher),
// we implement our own version of Config. Config has been a fast-
// and easy-to-implement addition to the framework, but enables us 
// to flexibly work with definitions and configurations.
// Here, we define the interface that M_Config objects should implement.
// Note that the MI_Config interface extends the Countable, Iterator,
// and MI_EventDispatcher, which enables easy access to the contained
// data and should enable the MI_Config objects to dispatch events.
interface MI_Config extends Countable, Iterator, MI_EventDispatcher {
	public function isChanged();
	public function get($name, $defaultValue = NULL);
	public function getRequired($path, $errorMessage = NULL, $errorCode = NULL);
	public function getTitleOf($name);
	public function getDescriptionOf($name);
	public function set($name, $value, $dispatchEvent = TRUE);
	public function setTitleOf($name, $title);
	public function setDescriptionOf($name, $description);
	public function setIsChanged($flag);
	public function removeMetaData();
	public function removeMetaDataOf($name);
	public function removeTitleOf($name);
	public function removeDescriptionOf($name);
	public function lock();
	public function unlock();
	public function merge(MI_Config $config, $mode);
	public function toArray();
	public function toNumericKeyArray();
	public function save();
	
	/* --- Magic methods --- */
	
	public function __get($name);
	public function __set($name, $value);
	public function __isset($name);
	public function __unset($name);
	public function __toString();
}
?>