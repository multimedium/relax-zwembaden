<?php
/**
 * M_Config class
 * 
 * The M_Config class implements the {@link MI_Config} interface.
 * 
 * NOTE:
 * The M_Config class has to implement the MI_EventDispatcher interface,
 * so event listeners can be attached and events can be dispatched
 * from the M_Config class. The easiest way to implement this, is to
 * inherit from the M_EventDispatcher class.
 * 
 * @package Core
 */
class M_Config extends M_EventDispatcher implements MI_Config {
	/**
	 * Constant to indicate merging mode
	 * 
	 * This constant is used to indicate the mode in which 2 M_Config
	 * objects should be merged. This constant will cause the merged
	 * config object to overwrite the original object, where values
	 * overlap.
	 */
	const OVERWRITE = 1;
	
	/**
	 * The M_Config data
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_data = array();
	
	/**
	 * The metadata (titles and descriptions)
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_metaData = array();
	
	/**
	 * M_Config Lock
	 * 
	 * This property, a boolean, indicates whether or not the M_Config
	 * object has been locked. If the value of this property is TRUE,
	 * the config object is locked, if FALSE, the object is not locked.
	 * 
	 * By default, this property is set to FALSE.
	 * 
	 * @access protected
	 * @var boolean
	 */
	protected $_locked = FALSE;
	
	/**
	 * Iterator support
	 * 
	 * This is an internal variable to support the Iterator interface. 
	 * More specifically, this is the current cursor position.
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_index;
	
	/**
	 * Countable support
	 * 
	 * This is an internal variable to support the Countable interface.
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_count;
	
	/**
	 * Changed?
	 * 
	 * A boolean flag, that tells us whether or not the config object has changed
	 * 
	 * @access protected
	 * @var boolean $flag
	 */
	protected $_isChanged = FALSE;
	
	/**
	 * Constructor
	 * 
	 * The base class parses a M_Config object out of an (associative)
	 * array.
	 * 
	 * @access public
	 * @param array $data
	 * 		The associative array. This array will be parsed into the 
	 * 		data structure of the M_Config object.
	 * @param boolean $lock
	 * 		Set to TRUE to lock the M_Config, FALSE to unlock. Read the
	 * 		documentation on {@link M_Config::lock()} for more info.
	 * @return M_Config
	 */
	public function __construct(array $data = array(), $lock = FALSE) {
		$this->_index = 0;
		$this->_count = count($data);
		foreach($data as $name => $value) {
			// Note that, when the configuration object is constructed,
			// no events are dispatched to notify about changes.
			$this->set($name, $value, FALSE);
		}
		
		// Locking the object is the last thing we do. We don't want
		// Exceptions to be thrown when writing to the config object
		// while constructing.
		$this->_locked = $lock;
	}
	
	/**
	 * Is changed?
	 * 
	 * Will tell whether or not the values in the config have been changed. Returns
	 * TRUE if a value - or more values - have changed, FALSE if not. Note that
	 * this method will work recursively, to check the flag...
	 * 
	 * @see M_Config::setIsChanged()
	 * @see M_Config::set()
	 * @see M_Config::setTitleOf()
	 * @see M_Config::setDescriptionOf()
	 * @access public
	 * @return bool $flag
	 */
	public function isChanged() {
		// For each of the contained values:
		foreach($this->_data as $value) {
			// If the value is a config
			if(M_Helper::isInstanceOf($value, 'MI_Config')) {
				// Then, we check if the config has changed:
				/* @var $value MI_Config */
				if($value->isChanged()) {
					// If so, we return TRUE
					return TRUE;
				}
			}
		}
		
		// Return the internal flag, if none of the children have changed:
		return $this->_isChanged;
	}
	
	/**
	 * Get data
	 * 
	 * This method can be used to retrieve data from the M_Config object.
	 * Note that you can also address the data as if it were a property
	 * of the M_Config object, thanks to PHP's magic methods __get() and
	 * __set(). Check out an example at {@link M_Config::__get()}.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the variable you want to fetch from the M_Config
	 * 		object.
	 * @param mixed $defaultValue
	 * 		The default value. This value is returned if the requested
	 * 		data could not have been found.
	 * @return mixed
	 */
	public final function get($name, $defaultValue = NULL) {
		return isset($this->_data[$name]) ? $this->_data[$name] : $defaultValue;
	}
	
	/**
	 * Magic Getter
	 * 
	 * Typically, you will access the variables in the config by 
	 * directly addressing them by their name. PHP will automatically
	 * call the "magic method" __get(), to retrieve the value of the
	 * requested variable. For example:
	 * 
	 * Example 1, get myVariable:
	 * <code>
	 *    $config = new M_Config(array(
	 *       'myVariable' => 'This is the value of myVariable'
	 *    ));
	 *    echo $config->myVariable;
	 * </code>
	 * 
	 * The code illustrated in Example 1 will actually route the 
	 * request to the {@link M_Config::get()} method. So, Example 1
	 * does exactly the same as Example 2.
	 * 
	 * Example 2, get myVariable:
	 * <code>
	 *    $config = new M_Config(array(
	 *       'myVariable' => 'This is the value of myVariable'
	 *    ));
	 *    echo $config->get('myVariable', NULL);
	 * </code>
	 * 
	 * Note that the magic getter will set the default value to NULL.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the variable you want to fetch from the 
	 * 		M_Config object.
	 * @return mixed
	 */
	public final function __get($name) {
		return $this->get($name, NULL);
	}

	/**
	 * Get required value
	 *
	 * This method will fetch a *required* value from a configuration object.
	 * Note that this method will throw an exception, if it cannot find a value
	 * for the requested variable name or path. Also, an exception will be thrown
	 * if a value can be found, but is empty!
	 *
	 * Example 1, get a required variable:
	 * <code>
	 *    $config = new M_Config(array(
	 *       'myVariable' => 'This is the value of myVariable'
	 *    ));
	 *
	 *    echo $config->getRequired(
	 *       'myVariable',
	 *       'Cannot find a value for the @varname variable.'
	 *    );
	 * </code>
	 *
	 * The example above illustrates how you would go about fetching the value of
	 * the required variable 'myVariable'. Note that, while fetching the value,
	 * we are already providing the error message that should be used in the thrown
	 * exception, should the variable not be found in the config object.
	 *
	 * In this error message, we can use some placeholders, which will automatically
	 * be replaced by their corresponding value:
	 *
	 * <code>@varname</code>
	 * The name of the required variable.
	 *
	 * <code>@varpath</code>
	 * The complete path to the variable (see example further down).
	 *
	 * <code>@curpath</code>
	 * The path at which an error message has occurred.
	 *
	 * <code>@configAsString</code>
	 * The configuration object, represented in a string
	 *
	 * You can also use this method to specify a path to the required variable.
	 * This is particularly handy if the configuration object contains other,
	 * nested, configuration objects. Consider the following example:
	 *
	 * Example 2, get a nested required variable:
	 * <code>
	 *    $config = new M_Config(array(
	 *       'myApp' => array(
	 *          'settings' => array(
	 *             'logDirectory' => 'files/log',
	 *             'cacheDirectory' => 'files/cache'
	 *          ),
	 *          'modules' => array(
	 *             // ...
	 *          )
	 *       )
	 *    ));
	 *
	 *    echo $config->getRequired(
	 *       'myApp/settings/logDirectory',
	 *       'Cannot find the directory for log files.'
	 *    );
	 * </code>
	 *
	 * NOTE:
	 * You can provide with a very generic error message, as this method will
	 * already append a very comprehensive error message to the provided one.
	 *
	 * @access public
	 * @param string $path
	 *		The name of the required variable you want fetch from the M_Config
	 *		object, or the path to the variable.
	 * @param string $errorMessage
	 *		The error message with which to construct the thrown exception, if
	 *		the variable cannot be found
	 * @param integer $errorCode
	 *		The error code with which to construct the thrown exception, if the
	 *		variable cannot be found
	 * @return mixed
	 */
	public function getRequired($path, $errorMessage = NULL, $errorCode = NULL) {
		// First of all, we cast the requested path to a string, and we clean it
		// up for use in this function:
		$path = is_array($path) 
			// If the provided path is an array, we glue the pieces of the array
			// together in a string, separating the array elements with a slash
			? implode('/', $path)
			// If the provided path is anything but an array, then we cast a string
			// and clean up redundant whitespaces or slashes at both the beginning
			// and ending of the string:
			: M_Helper::trimCharlist((string) $path, '/');

		// We explode the path into an array of path elements:
		$pathElements       = preg_split('/[\/\s]+/m', $path);
		$pathCountZeroBased = count($pathElements) - 1;

		// We start looking for the required value in the root of the config. So,
		// we store the root of the config as the current "working directory":
		$wd     = $this;
		$wdPath = '/';

		/* @var $wd MI_Config */
		// If only one path element is present, then the requested path is
		// actually the name of a variable in the root:
		if($pathCountZeroBased == 0) {
			// So, then we check if that variable is present:
			if($wd->get($pathElements[0], NULL) === NULL) {
				// If not present, then the required variable has no value in the
				// configuration object. We throw an exception to inform about the
				// error:
				throw new M_Exception(
					strtr(
						(string) $errorMessage . ' (The *required* variable ' .
						'"@varname" is not existing or does not have a value ' .
						'in the configuration object @configAsString)',
						array(
							'@varname'        => $pathElements[0],
							'@varpath'        => implode('/', $pathElements),
							'@curpath'        => '/' . $pathElements[0],
							'@configAsString' => $wd
						)
					)
				);
			}
		}

		// For each of the elements in the requested path:
		foreach($pathElements as $elementIndex => $element) {
			// If the current element is not present as variable:
			if(! isset($wd->$element)) {
				// Then, we throw an exception:
				throw new M_Exception(
					strtr(
						(string) $errorMessage . ' (The *required* variable at ' .
						'"@varpath" is not existing or does not have a value in ' .
						'the configuration object @configAsString)',
						array(
							'@varname'        => $element,
							'@varpath'        => implode('/', $pathElements),
							'@curpath'        => $wdPath,
							'@configAsString' => $wd
						)
					)
				);
			}

			// If the current element is not the last one in the path:
			if($elementIndex < $pathCountZeroBased) {
				// More elements are to be looked up further down this path.
				// So, as a matter of fact, we are expecting another instance of
				// MI_Config here, which in turn contains more data for the remaining 
				// elements in the requested path. We check if that's the case:
				if(! M_Helper::isInstanceOf($wd->$element, 'MI_Config')) {
					// If not, we throw an exception:
					throw new M_Exception(
						strtr(
							(string) $errorMessage . ' (Expecting an instance ' .
							'of MI_Config for "@varname" at @curpath in the ' .
							'configuration object @configAsString)',
							array(
								'@varname'        => $element,
								'@varpath'        => implode('/', $pathElements),
								'@curpath'        => $wdPath,
								'@configAsString' => $wd
							)
						)
					);
				}
			}

			// We set the new "working directory":
			$wd      = $wd->$element;
			$wdPath .= $element . '/';
		}

		// If we are still here, then everything should have gone well. We output
		// the "working directory" as the final value:
		return $wd;
	}
	
	/**
	 * Get title of data
	 * 
	 * This method can be used to retrieve the title of a specific piece of data
	 * in the M_Config object.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the variable of which you want to fetch the title
	 * @return string
	 */
	public final function getTitleOf($name) {
		return (isset($this->_metaData[$name]) && isset($this->_metaData[$name][0]))
			? $this->_metaData[$name][0]
			: '';
	}
	
	/**
	 * Get description of data
	 * 
	 * This method can be used to retrieve the description of a specific piece of 
	 * data in the M_Config object.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the variable of which you want to fetch the description
	 * @return string
	 */
	public final function getDescriptionOf($name) {
		return (isset($this->_metaData[$name]) && isset($this->_metaData[$name][1]))
			? $this->_metaData[$name][1]
			: '';
	}
	
	/**
	 * Set data
	 * 
	 * This method can be used to set data in the M_Config object.
	 * Note that you can also address the data as if it were a 
	 * property of the M_Config object, thanks to PHP's magic methods 
	 * __get() and __set(). Check out an example at 
	 * {@link M_Config::__set()}.
	 * 
	 * @access public
	 * @throws M_ConfigException
	 * @param string $name
	 * 		The name you want to give to the data/variable. This 
	 * 		name can be any name you wish. However, you should look
	 * 		for short and descriptive names.
	 * @param mixed $value
	 * 		The data; the value of the variable
	 * @param boolean $dispatchEvent
	 * 		If set to TRUE, this method will dispatch an event, to
	 * 		notify about the change in the M_Config object. The event
	 * 		is a {@link M_ConfigEvent} of type {@link M_ConfigEvent::CHANGE}.
	 * 		If set to FALSE, no event will dispatched at all.
	 * @return void
	 */
	public final function set($name, $value, $dispatchEvent = TRUE) {
		if($this->_locked) {
			throw new M_ConfigException('The M_Config object is locked; Cannot write data.');
		} else {
			if(is_array($value)) {
				$this->_data[$name] = new M_Config($value, $this->_locked);
			} elseif(M_Helper::isInstanceOf($value, 'MI_Config')) {
				$this->_data[$name] = $value;
			} else {
				$this->_data[$name] = $value;
			}
			$this->_count = count($this->_data);
			$this->_isChanged = TRUE;
			if($dispatchEvent) {
				$this->dispatchEvent(new M_ConfigEvent(M_ConfigEvent::CHANGE, $this));
			}
		}
	}
	
	/**
	 * Magic Setter
	 * 
	 * Typically, you will access the variables in the config by 
	 * directly addressing them by their name. PHP will automatically
	 * call the "magic method" __set(), to set the value of the
	 * variable. For example:
	 * 
	 * Example 1, set myVariable:
	 * <code>
	 *    $config = new M_Config;
	 *    $config->myVariable = 'This is the value of myVariable';
	 * </code>
	 * 
	 * The code illustrated in Example 1 will actually route the 
	 * request to the {@link M_Config::set()} method. So, Example 1
	 * does exactly the same as Example 2.
	 * 
	 * Example 2, set myVariable:
	 * <code>
	 *    $config = new M_Config
	 *    $config->set('myVariable', 'This is the value of myVariable');
	 * </code>
	 * 
	 * @access public
	 * @param string $name
	 * 		The name you want to give to the data/variable. This 
	 * 		name can be any name you wish. However, you should look
	 * 		for short and descriptive names.
	 * @param mixed $value
	 * 		The data; the value of the variable
	 * @return void
	 */
	public final function __set($name, $value) {
		$this->set($name, $value);
	}
	
	/**
	 * Set title of data
	 * 
	 * This method can be used to set the title of a specific piece of data
	 * in the M_Config object.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the variable of which you want to set the title
	 * @param stringr $title
	 * 		The title
	 * @return string
	 */
	public final function setTitleOf($name, $title) {
		// Initiate meta-data, if not done yet
		if(! isset($this->_metaData[$name])) {
			$this->_metaData[$name] = array(
				'', // title
				''  // description
			);
		}
		
		// Set the title:
		$this->_metaData[$name][0] = (string) $title;
		$this->_isChanged = TRUE;
	}
	
	/**
	 * Set description of data
	 * 
	 * This method can be used to set the description of a specific piece of 
	 * data in the M_Config object.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the variable of which you want to set the description
	 * @param stringr $description
	 * 		The description
	 * @return string
	 */
	public final function setDescriptionOf($name, $description) {
		// Initiate meta-data, if not done yet
		if(! isset($this->_metaData[$name])) {
			$this->_metaData[$name] = array(
				'', // title
				''  // description
			);
		}
		
		// Set the title:
		$this->_metaData[$name][1] = (string) $description;
		$this->_isChanged = TRUE;
	}
	
	/**
	 * Set flag: is changed?
	 * 
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if you want to set the config to "changed", FALSE if not
	 * @return void
	 */
	public function setIsChanged($flag) {
		$this->_isChanged = (bool) $flag;
	}
	
	/**
	 * Remove all metadata
	 * 
	 * This method can be used to remove all metadata (titles and descriptions)
	 * from the M_Config object.
	 * 
	 * @see M_Config::setTitleOf()
	 * @see M_Config::setDescriptionOf()
	 * @access public
	 * @return void
	 */
	public function removeMetaData() {
		$this->_metaData = array();
	}
	
	/**
	 * Remove metadata
	 * 
	 * This method can be used to remove the metadata (both title and description)
	 * from a specific piece of data in the M_Config object.
	 * 
	 * @see M_Config::setTitleOf()
	 * @see M_Config::setDescriptionOf()
	 * @see M_Config::removeMetaData()
	 * @access public
	 * @param string $name
	 * 		The name of the variable from which you want to remove the metadata
	 * @return void
	 */
	public function removeMetaDataOf($name) {
		if(isset($this->_metaData[$name])) {
			unset($this->_metaData[$name]);
		}
	}
	
	/**
	 * Remove title
	 * 
	 * This method can be used to remove the title from a specific piece of data 
	 * in the M_Config object.
	 * 
	 * @see M_Config::setTitleOf()
	 * @see M_Config::removeMetaData()
	 * @see M_Config::removeMetaDataOf()
	 * @access public
	 * @param string $name
	 * 		The name of the variable from which you want to remove the title
	 * @return void
	 */
	public function removeTitleOf($name) {
		if(isset($this->_metaData[$name]) && isset($this->_metaData[$name][0])) {
			unset($this->_metaData[$name][0]);
		}
	}
	
	/**
	 * Remove description
	 * 
	 * This method can be used to remove the description from a specific piece 
	 * of data in the M_Config object.
	 * 
	 * @see M_Config::setDescriptionOf()
	 * @see M_Config::removeMetaData()
	 * @see M_Config::removeMetaDataOf()
	 * @access public
	 * @param string $name
	 * 		The name of the variable from which you want to remove the description
	 * @return void
	 */
	public function removeDescriptionOf($name) {
		if(isset($this->_metaData[$name]) && isset($this->_metaData[$name][1])) {
			unset($this->_metaData[$name][1]);
		}
	}
	
	/**
	 * Overloading of isset()
	 *
	 * This method will handle the isset() call on the config
	 * variables.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the variable
	 * @return boolean
	 * 		Will return TRUE if the variable has been set, 
	 * 		or FALSE if the variable does not exist.
	 */
	public final function __isset($name) {
		return isset($this->_data[$name]);
	}
	
	/**
	 * Overloading of unset()
	 * 
	 * This method will handle the unset() call on the config
	 * variables.
	 *
	 * @access public
	 * @throws M_ConfigException
	 * @param string $name
	 * 		The name of the variable
	 * @return void
	 */
	public final function __unset($name) {
		if($this->_locked) {
			throw new M_ConfigException('The M_Config object is locked; Cannot unset data.');
		} else {
			unset($this->_data[$name]);
			$this->_count = count($this->_data);
		}
	}
	
	/**
	 * Export to string
	 * 
	 * This is a "magic method", called automatically by PHP when the
	 * M_Config object is being casted to a string. For example, an echo 
	 * call on the M_Config object will print the result of this function.
	 * 
	 * Example 1
	 * <code>
	 *    $config = new M_Config;
	 *    echo $config; // prints the result of __toString()
	 * </code>
	 * 
	 * Example 2
	 * <code>
	 *    $config = new M_Config;
	 *    $str  = (string) $config; // saves the result of __toString()
	 * </code>
	 * 
	 * @access public
	 * @return string
	 */
	public function __toString() {
		$nl = "\r\n";
		$string  = '';
		$string .= 'M_Config (' . $nl;
		foreach($this as $name => $value) {
			$string .= '  ' . $name . ' : ';
			$string .= '  ' . (string) $value;
			$string .= $nl;
		}
		$string .= ')';
		return $string;
	}
	
	/**
	 * Lock the config object
	 * 
	 * The M_Config object can be locked, to prevent changes to the 
	 * data it contains. Once a M_Config object has been locked, a 
	 * {@link M_ConfigException} is thrown when other objects try to 
	 * make changes to the M_Config object. The following methods 
	 * will cause M_Config to throw an exception, if locked:
	 * 
	 * - {@link M_Config::set()}, and {@link M_Config::__set()}
	 * - {@link M_Config::unset()}
	 * - {@link M_Config::merge()}
	 * 
	 * @access public
	 * @return void
	 */
	public final function lock() {
		$this->_locked = TRUE;
	}
	
	/**
	 * Unlock the config object
	 * 
	 * @see M_Config::lock()
	 * @access public
	 * @return void
	 */
	public final function unlock() {
		$this->_locked = FALSE;
	}
	
	/**
	 * Merge with other config object
	 * 
	 * This method will merge 2 M_Config objects into one. Note 
	 * that this method will throw a {@link M_ConfigException}, 
	 * if the config object is locked; see {@link M_Config::lock()}.
	 * 
	 * @access public
	 * @param MI_Config $config
	 * 		The config object to be merged into this object
	 * @param mixed $mode
	 * 		The mode in which the merge should be performed
	 * @param boolean $dispatchEvent
	 * 		If set to TRUE, this method will cause {@link M_Config::set()}
	 * 		to dispatch {@link M_ConfigEvent} events, with type set to
	 * 		{@link M_ConfigEvent::CHANGE}. If set to FALSE, no event will
	 * 		dispatched at all.
	 * @return void
	 */
	public final function merge(MI_Config $config, $mode, $dispatchEvent = TRUE) {
		foreach($config as $name => $value) {
			switch($mode) {
				case M_Config::OVERWRITE:
					if($value instanceof M_Config) {
						$this->$name->merge($value, $mode, $dispatchEvent);
					} else {
						$this->set($name, $value, $dispatchEvent);
						$this->setTitleOf($name, $config->getTitleOf($name));
						$this->setDescriptionOf($name, $config->getDescriptionOf($name));
					}
					break;
			}
		}
	}
	
	/**
	 * Save the config
	 * 
	 * The save() implementation of the basic M_Config object doe not 
	 * do anything, because the original format with which it has 
	 * been constructed was an array: {@link M_Config::__construct()}.
	 * So, it is impossible to save the changed data back to the 
	 * original format. However, other config objects, such as 
	 * {@link M_ConfigXml}, will save the config back to the original
	 * format and location.
	 * 
	 * IMPORTANT NOTE:
	 * The save() method should return a boolean, indicating whether 
	 * or not the changes to the M_Config object have been changed 
	 * successfully. In the case of a regular M_Config object, this 
	 * method throws a {@link M_ConfigException}, in order to inform
	 * about the fact that it cannot be saved.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function save() {
		throw new M_ConfigException('M_Config cannot save() changes (stored in array).');
	}
	
	/**
	 * Export the M_Config object to an associative array
	 * 
	 * This method will export the data structure contained in the
	 * config object, into an associative array.
	 * 
	 * @access public
	 * @return array
	 */
	public function toArray() {
		$output = array();
		foreach($this->_data as $name => $value) {
			if($value instanceof M_Config) {
				$output[$name] = $value->toArray();
			} else {
				$output[$name] = $value;
			}
		}
		return $output;
	}
	
	/**
	 * Export the M_Config object to an numeric array
	 * 
	 * This method will export the data structure contained in the
	 * config object, into an array. Note that the keys of the 
	 * resulting array will be numeric, so you loose the original 
	 * variable names. If you need an associative array, you should
	 * check out the {@link M_Config::toArray()} method.
	 * 
	 * @access public
	 * @return array
	 */
	public function toNumericKeyArray() {
		$output = array();
		foreach($this->_data as $value) {
			if($value instanceof M_Config) {
				array_push($output, $value->toNumericKeyArray());
			} else {
				array_push($output, $value);
			}
		}
		return $output;
	}
	
	/**
	 * Support for the Countable interface
	 *
	 * @access public
	 * @return integer
	 * 		The item count
	 */
	public final function count() {
		return $this->_count;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return mixed
	 * 		The current item in stored data
	 */
	public final function current() {
		return current($this->_data);
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return string
	 * 		The key of the current item in stored data
	 */
	public final function key() {
		return key($this->_data);
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Moves the internal cursor to the next item in stored data
	 * 
	 * @access public
	 * @return void
	 */
	public final function next() {
		next($this->_data);
		$this->_index += 1;
	}
	
	/**
	 * Support for the Iterator interface
	 * 
	 * Resets the internal cursor for iteration. foreach() calls on
	 * an instance of the Registry class will call the rewind()
	 * method, to start looping at the beginning.
	 *
	 * @access public
	 * @return void
	 */
	public final function rewind() {
		reset($this->_data);
		$this->_index = 0;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Will indicate if the end of the iterator has been reached.
	 * 
	 * @return boolean
	 * 		Will return TRUE if not reached the end of the iterator,
	 * 		or FALSE if all items in the iterator have been accessed
	 * 		in the loop.
	 */
	public final function valid() {
		return $this->_index < $this->_count;
	}
}
?>