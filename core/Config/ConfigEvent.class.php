<?php
/**
 * M_ConfigEvent class
 *
 * The M_ConfigEvent class is used to dispatch events from the M_Config
 * class. For more information on events, read the documentation on
 * the following classes:
 * 
 * - {@link M_EventDispatcher}
 * - {@link M_Event}
 * 
 * @package Core
 */
class M_ConfigEvent extends M_Event {
	const CHANGE = 'config-change';
}
?>