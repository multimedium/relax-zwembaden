<?php
/**
 * M_ConfigException class
 *
 * Exceptions thrown by the {@link M_Config} class(es)
 * 
 * @package Core
 */
class M_ConfigException extends M_Exception {
}
?>