<?php
/**
 * M_FormConfig class
 * 
 * M_FormConfig, a subclass of {@link M_Form}, can be used to easily mount a form
 * that creates/edits an {@link MI_Config} instance.
 * 
 * @package Core
 */
class M_FormConfig extends M_Form {
	/**
	 * {@link MI_Config} instance
	 * 
	 * This property stores the {@link MI_Config} instance that is to be edited
	 * by the form.
	 * 
	 * @access private
	 * @var MI_Config
	 */
	private $_config;
	
	/**
	 * Constructor
	 * 
	 * @see M_Form::__construct()
	 * @access public
	 * @param string $id
	 * 		The form id
	 * @return M_FormConfig
	 */
	public function __construct($id) {
		parent::__construct($id);
	}
	
	/**
	 * Add form definition
	 * 
	 * This method is used by {@link M_Form::factory()}, to mount 
	 * the form with a given definition. This method overrides
	 * {@link M_Form::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * M_FormConfig adds 1 new definition key:
	 * 
	 * <code>config</code>
	 * 
	 * Inside this new key, you can add a complete config structure. A
	 * new instance of {@link M_Config} will be mounted, based on the supplied
	 * structure. Also, you can simply provide with an instance of {@link MI_Config}.
	 * For more info, read {@link M_FormConfig::setConfig()}
	 * 
	 * <code>
	 *    addFields
	 *    addFieldsFromConfig
	 * </code>
	 * 
	 * With this new key (boolean), you can specify whether or not the form should
	 * be auto-populated with fields for each of the config's variables. For more
	 * info, read {@link M_FormConfig::addFieldsFromConfig()}
	 * 
	 * Example 1, illustrates how you could mount an instance of M_FormConfig
	 * <code>
	 *    $form = M_Form::factory('myForm', new Config(array(
	 *       'type' => 'config',
	 *       'config' => array(
	 *          'variableOne'   => 'Value of variable 1',
	 *          'variableTwo'   => 'Value of variable 2',
	 *          'variableThree' => 'Value of variable 3'
	 *       ),
	 *       'addFields' => 'yes'
	 *    )));
	 * </code>
	 * 
	 * @access public
	 * @see M_Form::factory()
	 * @see M_Form::set()
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @return void
	 */
	public function set($spec, $definition) {
		switch($spec) {
			case 'config':
				$this->setConfig($definition);
				break;
			
			case 'addFields':
			case 'addFieldsFromConfig':
				if(M_Helper::isBooleanTrue($definition)) {
					$this->addFieldsFromConfig();
				}
				break;
			
			default:
				parent::set($spec, $definition);
				break;
		}
	}
	
	/**
	 * Set {@link MI_Config} instance
	 * 
	 * This method can be used to set the {@link MI_Config} instance that is to 
	 * be edited by the form.
	 * 
	 * @access public
	 * @param MI_Config $config
	 * @return void
	 */
	public function setConfig(MI_Config $config) {
		$this->_config = $config;
	}
	
	/**
	 * Add fields from {@link MI_Config} instance
	 * 
	 * This method will automatically populate the form with fields. The collection
	 * of fields is defined by the variables that are contained by the config
	 * object (which has been provided previously with {@link M_FormConfig::setConfig()})
	 * 
	 * NOTE:
	 * Only config variables that contain a string value will be included in the 
	 * collection of fields! Config variabels that contain a nested config,
	 * are not included in the form.
	 * 
	 * @access public
	 * @return void
	 */
	public function addFieldsFromConfig() {
		// If a config object is available:
		if($this->_config) {
			// For each of the variables in the config object:
			foreach($this->_config as $name => $value) {
				// If the variable's value is not a nested config object:
				if(! ($value instanceof MI_Config)) {
					// Create a new field:
					$field = new M_FieldText($name);
					
					// Set the default value of the field to the current value of 
					// the config variable:
					$field->setDefaultValue($value);
					
					// Also, we set the field's title and description to the title
					// and description that has been given to the config variable.
					// Note that, if the config variable does not have a title, the
					// field will receive the config variable's name as title.
					$title = $this->_config->getTitleOf($name);
					if(! $title) {
						$title = $name;
					}
					
					$field->setTitle($title);
					$field->setDescription($this->_config->getDescriptionOf($name));
					
					// We add the field to the form:
					$this->addField($field);
				}
			}
		}
	}
	
	/**
	 * Form actions
	 * 
	 * All {@link M_Form} subclasses have to implement this method.
	 * This method overrides {@link M_Form::actions()}.
	 * 
	 * NOTE:
	 * This method should return TRUE on success, and FALSE on failure.
	 * 
	 * @access public
	 * @see M_Form::getValues()
	 * @param array $values
	 * 		The values that have been collected by the method
	 * 		{@link M_Form::getValues()}.
	 * @return boolean
	 */
	public function actions(array $values) {
		// For each of the fields in the form:
		foreach($values as $name => $value) {
			
		}
	}
}