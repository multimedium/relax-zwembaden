<?php
/**
 * Exceptions thrown by the Session API
 *
 * @package Core
 */
class M_SessionException extends M_Exception {
}
?>