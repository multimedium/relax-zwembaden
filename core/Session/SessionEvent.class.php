<?php
/**
 * M_SessionEvent class
 * 
 * M_SessionEvent is used to construct events that are dispatched
 * from the {@link M_Session} class.
 * 
 * @package Core
 */
class M_SessionEvent extends M_Event {
	/**
	 * Event Type
	 * 
	 * This type of event is dispatched from {@link M_Session} at the
	 * user's first page request.
	 */
	const INITIATE = 'session-init';
	
	/**
	 * Event Type
	 * 
	 * This type of event is dispatched from {@link M_Session} at each
	 * page request. The first page request however is dispatched 
	 * with the event type set to {@link M_SessionEvent::INITIATE}.
	 */
	const REFRESH  = 'session-refresh';
	
	/**
	 * Event Type
	 * 
	 * This type of event is dispatched from {@link M_Session} when
	 * the Session ID is regenerated.
	 */
	const RENEWAL  = 'session-id-renewal';
	
	/**
	 * Event Type
	 * 
	 * This type of event is dispatched from {@link M_Session} when
	 * the Session is being destroyed.
	 */
	const DESTROY  = 'session-destroy';
	
	/**
	 * Event Type
	 * 
	 * This type of event is dispatched from {@link M_Session} when
	 * the Session is being closed (end of page request).
	 */
	const CLOSE    = 'session-close';
}
?>