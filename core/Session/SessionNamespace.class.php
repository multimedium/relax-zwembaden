<?php
/**
 * M_SessionNamespace class
 *
 * The M_SessionNamespace class is used to store data in the user
 * session. A namespace, when constructed, is given a name. With
 * that name, M_SessionNamespace will store the data in the PHP
 * superglobal variable $_SESSION.
 * 
 * Note that M_SessionNamespace implements the Countable and Iterator
 * interface, for easy access to the data stored in the namespace.
 * Also, it implements the MI_EventDispatcher interface, so event
 * listeners can be attached to a given namespace.
 * 
 * TODO: M_SessionNamespace::destroy()
 * 
 * @package Core
 */
class M_SessionNamespace extends M_EventDispatcher implements Countable, Iterator, MI_EventDispatcher {

	/* -- PROPERTIES -- */

	/**
	 * The M_SessionNamespace's name. This is the name under which
	 * the data will be stored in the $_SESSION variable.
	 *
	 * @var string
	 * @access private
	 */
	private $_name;
	
	/**
	 * Locked namespaces. A M_SessionNamespace can be construced with a
	 * lock. Doing so, it prevents other objects to construct a name-
	 * space with the given name. Typically, this feature is used to 
	 * reserve a namespace in a specific class.
	 *
	 * @var array
	 * @access private
	 */
	private static $_lockedNamespaces = array();
	
	/**
	 * Internal cursor of the iterator.
	 *
	 * @var integer
	 * @access private
	 */
	private $_index;
	
	/**
	 * Internal count of the data, to support the Countable interface
	 *
	 * @var integer
	 * @access private
	 */
	private $_count;

	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * When a M_SessionNamespace is constructed, it reads the data from
	 * PHP's superglobal $_SESSION.
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the namespace
	 * @param boolean $lock
	 * 		A M_SessionNamespace can be constructed, and locked. By 
	 * 		locking a namespace, you prevent other objects to construct 
	 * 		a namespace with the same name. Typically, this feature is 
	 * 		used to reserve a namespace for use in a specific class.
	 * @return M_SessionNamespace
	 */
	public function __construct($name, $lock = FALSE) {
		// We make sure that the session has been started:
		$session = M_Session::getInstance();
		if(!$session->isStarted()) {
			throw new M_SessionException(sprintf(
				'The session has to be started, before creating session namespaces; ' .
				'see M_Session::start()'
			));
		}

		// If this session namespace has already been locked by another
		// session object:
		if(in_array($name, self::$_lockedNamespaces)) {
			// Then, we cannot use the namespace. We need to inform about the error
			// with an exception
			throw new M_SessionException(sprintf(
				'The session namespace "%s" is locked',
				$name
			));
		}

		// Initiate the session variables, if not already done:
		if(!isset($_SESSION[$name])) {
			$_SESSION[$name] = array();
		}

		// Set some internals for the object:
		$this->_name = $name;
		$this->_index = 0;
		$this->_count = count($this->_data);

		// Lock the namespace, if requested
		if($lock) {
			array_push(self::$_lockedNamespaces, $name);
		}
	}

	/**
	 * Construct from cookie
	 *
	 * NOTE:
	 * Will construct the session namespace with {@link M_SessionNamespace::__construct()},
	 * and will ADD the variables from a given cookie to that namespace...
	 *
	 * @static
	 * @see M_SessionNamespace::writeCookie()
	 * @access public
	 * @param string $name
	 * 		The name of the namespace
	 * @param boolean $lock
	 * 		A M_SessionNamespace can be constructed, and locked. By
	 * 		locking a namespace, you prevent other objects to construct
	 * 		a namespace with the same name. Typically, this feature is
	 * 		used to reserve a namespace for use in a specific class.
	 * @param string $cookieName
	 *		The name of the cookie (optional)
	 * @return M_SessionNamespace
	 */
	public static function constructFromCookie($name, $lock = FALSE, $cookieName = NULL) {
		// Contruct the session namespace:
		$o = new self($name, $lock);

		// Compose the cookie's name (if not provided):
		$cookieName = self::_getCookieName($o, $cookieName);

		// Construct the cookie:
		$cookie = new M_Cookie($cookieName);

		// We read the data in the cookie:
		$cookieData = $cookie->read();

		// If we could read the data in the cookie correctly:
		if($cookieData) {
			// Then, we make sure that an associative array has been read from
			// the cookie. If this cookie was created by a session namespace, it
			// was created with an entry of the $_SESSION variable, so we know it
			// should be an array:
			if(! is_array($cookieData)) {
				// If that's not the case, we inform about the error:
				throw new M_Exception(sprintf(
					'Cannot construct an instance of %s with data of cookie %s; ' .
					'Unexpected data type/structure',
					$this->getClassName(),
					$cookieName
				));
			}

			// For each of the variables in the cookie array:
			foreach($cookieData as $cKey => $cValue) {
				// We set the variable in the session namespace:
				$o->$cKey = $cValue;
			}
		}

		// Return the object:
		return $o;
	}

	/* -- SETTERS -- */

	/**
	 * Add array property
	 *
	 * This method will allow you to easily add elements to a given array in the
	 * session namespace. Assume we want to create the following array in the
	 * session namespace:
	 *
	 * <code>
	 *    Array(
	 *       1 => 'one',
	 *       2 => 'two',
	 *       3 => 'three'
	 *    )
	 * </code>
	 *
	 * You could do so easily by writing the following code:
	 *
	 * <code>
	 *    $ns = new M_SessionNamespace('example');
	 *    $ns->myArray = array();
	 *    $ns->addArrayElement('myArray', 'one');
	 *    $ns->addArrayElement('myArray', 'two');
	 *    $ns->addArrayElement('myArray', 'three');
	 * </code>
	 *
	 * TIP:
	 * If you use this method, you do not need to declare the array in the session
	 * namespace. If the array does not yet exist, it will be created automatically
	 * for you. So, you could rewrite the example as following:
	 *
	 * <code>
	 *    $ns = new M_SessionNamespace('example');
	 *    $ns->addArrayElement('myArray', 'one');
	 *    $ns->addArrayElement('myArray', 'two');
	 *    $ns->addArrayElement('myArray', 'three');
	 * </code>
	 *
	 * NOTE:
	 * You can provide with the key that is to be used for the new array element,
	 * by providing the third argument to this method. Let's assume that we would
	 * add another element, this time with a string name as a key:
	 *
	 * <code>
	 *    Array(
	 *       1      => 'one',
	 *       2      => 'two',
	 *       3      => 'three',
	 *       'four' => 'new element'
	 *    )
	 * </code>
	 *
	 * You can do so by writing the following code:
	 *
	 * <code>
	 *    $ns = new M_SessionNamespace('example');
	 *    $ns->addArrayElement('myArray', 'new element', 'four');
	 * </code>
	 *
	 * Note that, if the key that you provide already is used in the array
	 * that is stored in the session namespace, then the array element for that
	 * key will be overwritten by the newly provided value.
	 *
	 * If you do not provide with a key, than the value is appended to the end of
	 * the array, and a numeric key is created automatically for the new value...
	 *
	 * @access public
	 * @param string $arrayName
	 *		The name of the array to which an element is to be added
	 * @param mixed $arrayValue
	 *		The value that is to be stored into the array element
	 * @param mixed $arrayKey
	 *		The key in which the value should be stored (optional string|integer)
	 * @return void
	 */
	public function addArrayElement($arrayName, $arrayValue, $arrayKey = NULL) {
		// We cast the name of the array to a string
		$arrayName = (string) $arrayName;

		// If the variable with the array's name does not yet exist in the
		// session namespace:
		if(! isset($this->$arrayName)) {
			// Then, we initiate it now, with an empty array as value. Note that
			// we use the magic setter, so the COUNT is also updated (for the
			// iterator functionality)
			$this->$arrayName = array();
		}
		
		// We make sure that the variable name is actually an array in
		// the session namespace:
		if(! is_array($_SESSION[$this->_name][$arrayName])) {
			// If not, we throw an exception to inform about the error:
			throw new M_Exception(sprintf(
				'Cannot add Array Element. "%s" is not an array in the session namespace',
				$arrayName
			));
		}

		// If a key has been defined for the new value:
		if($arrayKey) {
			// Then, we add the new value with that key:
			$_SESSION[$this->_name][$arrayName][(string) $arrayKey] = $arrayValue;
		}
		// If no key has been specified:
		else {
			// Then, we append the value to the end of the array, and we automatically
			// create a numerical key for it
			$_SESSION[$this->_name][$arrayName][] = $arrayValue;
		}
	}

	/**
	 * Clear variables
	 *
	 * Will clear all variables in the session namespace.
	 *
	 * @access public
	 * @return void
	 */
	public function clear() {
		$_SESSION[$this->_name] = array();
		$this->_count = 0;
		$this->_index = 0;
	}

	/**
	 * Write the {@link SessionNamespace} to a cookie
	 *
	 * Will write the session namespace to a cookie, so it can later be constructed
	 * with the {@link M_SessionNamespace::constructFromCookie()} method.
	 *
	 * @see M_SessionNamespace::constructFromCookie()
	 * @access public
	 * @param string $name
	 *		The name of the cookie (optional)
	 * @param M_Date $expire
	 *		The expiration date of the cookie. This date is optional. If not provided,
	 *		the session namespace object will write a cookie that lives for a year
	 *		long...
	 * @return void
	 */
	public function writeCookie($name = NULL, M_Date $expire = NULL) {
		// Construct the cookie:
		$cookie = new M_Cookie(self::_getCookieName($this, $name));

		// Then, write the data to the cookie:
		$cookie->write(
			$_SESSION[$this->_name],
			$expire
				? $expire
				: new M_Date(array('year' => M_Date::getCurrentYear() + 1))
		);
	}

	/**
	 * Remove the cookie
	 *
	 * Will remove the cookie that may have been created for the namespace previously
	 * with {@link M_SessionNamespace::writeCookie()}
	 *
	 * @access public
	 * @param string $name
	 *		The name of the cookie (optional)
	 * @return void
	 */
	public function deleteCookie() {
		// Construct the cookie:
		$cookie = new M_Cookie(self::_getCookieName($this, $name));

		// Remove the cookie:
		$cookie->delete();
	}

	/* -- GETTERS -- */

	/**
	 * Get the name of the session namespace
	 *
	 * Will provide with the name with which the session namespace has been
	 * constructed.
	 *
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * Get a variable
	 * 
	 * This method will return the value of a variable in the namespace.
	 * Note that you can provide a default value to this method, which
	 * is returned if the requested variable cannot be found.
	 *
	 * @access public
	 * @param string $key
	 * 		The name of the variable
	 * @param mixed $defaultValue
	 * 		The default value, to be returned if the variable does not
	 * 		exist in the namespace.
	 * @return mixed
	 */
	public final function get($key, $defaultValue = NULL) {
		return isset($_SESSION[$this->_name][$key])
				? $_SESSION[$this->_name][$key]
				: $defaultValue;
	}

	/**
	 * Get (associative) array
	 *
	 * Will convert the session namespace to an associative array. The keys
	 * of the array are the namespace's variable names, while the values of the
	 * array are the corresponding values.
	 *
	 * @access public
	 * @return array
	 */
	public function toArray() {
		// Output array
		$out = array();

		// for each of the elements in the session variable
		foreach($_SESSION[$this->_name] as $key => $value) {
			// Add the elements to the array:
			$out[$key] = $value;
		}

		// Return the array
		return $out;
	}

	/* -- PHP MAGIC METHODS -- */
	
	/**
	 * Magic Setter
	 * 
	 * Typically, you will access the variables in the namespace by 
	 * directly addressing them by their name. PHP will automatically
	 * call the "magic method" __set(), to set the value of the
	 * variable. For example:
	 * 
	 * <code>
	 *    $namespace = new M_SessionNamespace('myNamespace');
	 *    $namespace->myVariable = 'Hello World';
	 * </code>
	 *
	 * @access public
	 * @param string $key
	 * 		The name of the variable
	 * @param mixed $value
	 * 		The value of the variable
	 * @return void
	 */
	public final function __set($key, $value) {
		$key = (string) $key;
		$_SESSION[$this->_name][$key] = $value;
		$this->_count = count($_SESSION[$this->_name]);
	}
	
	/**
	 * Magic Getter
	 * 
	 * Typically, you will access the variables in the namespace by 
	 * directly addressing them by their name. PHP will automatically
	 * call the "magic method" __get(), to retrieve the value of the
	 * requested variable. For example:
	 * 
	 * <code>
	 *    $namespace = new M_SessionNamespace('myNamespace');
	 *    echo $namespace->myVariable;
	 * </code>
	 *
	 * @access public
	 * @param name $key
	 * 		The name of the variable
	 * @return mixed
	 */
	public final function __get($key) {
		return $this->get($key);
	}
	
	/**
	 * Overloading of isset()
	 *
	 * This method will handle the isset() call on the namespace's
	 * variables.
	 * 
	 * @access public
	 * @param string $key
	 * 		The name of the variable
	 * @return boolean
	 * 		Will return TRUE if the variable has been set, or FALSE
	 * 		if the variable does not exist.
	 */
	public final function __isset($key) {
		return isset($_SESSION[$this->_name][$key]);
	}
	
	/**
	 * Overloading of unset()
	 * 
	 * This method will handle the unset() call on the namespace's
	 * variables.
	 *
	 * @access public
	 * @param string $key
	 * 		The name of the variable
	 * @return void
	 */
	public final function __unset($key) {
		unset($_SESSION[$this->_name][$key]);
		$this->_count = count($_SESSION[$this->_name]);
	}

	/* -- ITERATOR SUPPORT -- */
	
	/**
	 * Support for the Countable interface
	 *
	 * @access public
	 * @return integer
	 * 		The item count
	 */
	public final function count() {
		return $this->_count;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return mixed
	 * 		The current item in stored data
	 */
	public final function current() {
		return current($_SESSION[$this->_name]);
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return string
	 * 		The key of the current item in stored data
	 */
	public final function key() {
		return key($_SESSION[$this->_name]);
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Moves the internal cursor to the next item in stored data
	 * 
	 * @access public
	 * @return void
	 */
	public final function next() {
		next($_SESSION[$this->_name]);
		$this->_index += 1;
	}
	
	/**
	 * Support for the Iterator interface
	 * 
	 * Resets the internal cursor for iteration. foreach() calls on
	 * an instance of the M_SessionNamespace class will call the rewind()
	 * method, to start looping at the beginning.
	 *
	 * @access public
	 * @return void
	 */
	public final function rewind() {
		reset($_SESSION[$this->_name]);
		$this->_index = 0;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Will indicate if the end of the iterator has been reached.
	 * 
	 * @return boolean
	 * 		Will return TRUE if not reached the end of the iterator,
	 * 		or FALSE if all items in the iterator have been accessed
	 * 		in the loop.
	 */
	public final function valid() {
		return $this->_index < $this->_count;
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get cookie name
	 *
	 * Used by the following methods, in order to compose the name of the cookie
	 * for the session namespace:
	 *
	 * - {@link M_SessionNamespace::constructFromCookie()}
	 * - {@link M_SessionNamespace::writeCookie()}
	 * - {@link M_SessionNamespace::deleteCookie()}
	 *
	 * @access protected
	 * @param M_SessionNamespace $namespace
	 *		The namespace for which to compose the cookie name
	 * @param string $name
	 *		The name of the cookie (optional)
	 * @return string
	 */
	protected static function _getCookieName(M_SessionNamespace $namespace, $name = NULL) {
		return ($name ? (string) $name : md5('ns-' . $namespace->getName()));
	}
}