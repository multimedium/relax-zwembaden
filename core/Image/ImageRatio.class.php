<?php
/**
 * M_ImageRatio class
 * 
 * The M_ImageRatio class provides with methods to calculate new sizes 
 * of an image. Currently, the M_ImageRatio is in use by 
 * {@link M_FilterImageDecorator} classes that resize images.
 * 
 * @package Core
 */
class M_ImageRatio {
	/**
	 * Force to width
	 * 
	 * This method calculates a new width and height, in order to match
	 * a given (fixed) width. The final width will be set to the 
	 * specified target value, and the corresponding height will be
	 * calculated (in order to maintain the image's aspect ratio).
	 * 
	 * Example 1, force an image's width to 200 pixels:
	 * <code>
	 *    // Construct an image:
	 *    $image = new M_Image('files/my-image.jpg');
	 *    
	 *    // Get the images current size:
	 *    // (We assume this is 400 x 200)
	 *    list($curWidth, $curHeight) = $image->getDimensions();
	 *    
	 *    // force the width of the image to 200 pixels:
	 *    // (result will be 200 x 100)
	 *    list($newWidth, $newHeight) = M_ImageRatio::getForcedToWidth($curWidth, $curHeight, 200);
	 * </code>
	 *
	 * @param integer $width
	 * 		The current width of the image
	 * @param integer $height
	 * 		The current height of the image
	 * @param integer $targetWidth
	 * 		The width you want to force the image to
	 * @return array
	 */
	public static function getForcedToWidth($width, $height, $targetWidth) {
		$ratio = ($targetWidth / $width);
		return array(round($width * $ratio), round($height * $ratio));
	}
	
	/**
	 * Force to height
	 * 
	 * This method calculates a new width and height, in order to match
	 * a given (fixed) height. The final height will be set to the 
	 * specified target value, and the corresponding width will be
	 * calculated (in order to maintain the image's aspect ratio).
	 * 
	 * Example 1, force an image's height to 400 pixels:
	 * <code>
	 *    // Construct an image:
	 *    $image = new M_Image('files/my-image.jpg');
	 *    
	 *    // Get the images current size:
	 *    // (We assume this is 400 x 200)
	 *    list($curWidth, $curHeight) = $image->getDimensions();
	 *    
	 *    // force the height of the image to 400 pixels:
	 *    // (result will be 800 x 400)
	 *    list($newWidth, $newHeight) = M_ImageRatio::getForcedToHeight($curWidth, $curHeight, 400);
	 * </code>
	 *
	 * @param integer $width
	 * 		The current width of the image
	 * @param integer $height
	 * 		The current height of the image
	 * @param integer $targetHeight
	 * 		The height you want to force the image to
	 * @return array
	 */
	public static function getForcedToHeight($width, $height, $targetHeight) {
		$ratio = ($targetHeight / $height);
		return array(round($width * $ratio), round($height * $ratio));
	}
	
	/**
	 * Get maximum size
	 * 
	 * This method calculates a new width and height, in order to make
	 * the original width and height fit within the restrictions of a 
	 * given maximum size. In doing so, this method makes sure to 
	 * maintain the image aspect ratio.
	 * 
	 * Example 1
	 * <code>
	 *    // Construct an image:
	 *    $image = new M_Image('files/my-image.jpg');
	 *    
	 *    // Get the images current size:
	 *    // (We assume this is 400 x 200)
	 *    list($curWidth, $curHeight) = $image->getDimensions();
	 *    
	 *    // If we want the image to fit in an area of 300 x 200,
	 *    // we would do the following:
	 *    // (result will be 300 x 150)
	 *    list($newWidth, $newHeight) = M_ImageRatio::getMaxSize($curWidth, $curHeight, 300, 200);
	 *    
	 *    // If we want the image to fit in an area of 400 x 100,
	 *    // we would do the following:
	 *    // (result will be 200 x 100)
	 *    list($newWidth, $newHeight) = M_ImageRatio::getMaxSize($curWidth, $curHeight, 400, 100);
	 *    
	 *    // If we want the image to fit in an area of 300 x 120,
	 *    // we would do the following:
	 *    // (result will be 240 x 120)
	 *    list($newWidth, $newHeight) = M_ImageRatio::getMaxSize($curWidth, $curHeight, 300, 120);
	 * </code>
	 * 
	 * {@link M_FilterImageResize} uses this method to calculate the
	 * new size of the resized image.
	 * 
	 * @param integer $width
	 * 		The current width of the image
	 * @param integer $height
	 * 		The current height of the image
	 * @param integer $maxWidth
	 * 		The maximum width
	 * @param integer $maxHeight
	 * 		The maximum height
	 * @return array
	 */
	public static function getMaxSize($width, $height, $maxWidth, $maxHeight) {
		if($width > $maxWidth || $height > $maxHeight) {
			$ratioW = ($maxWidth  / $width);
			$ratioH = ($maxHeight / $height);
			$ratio  = min(array($ratioW, $ratioH));
			if($ratio < 1) {
				return array(round($width * $ratio), round($height * $ratio));
			}
		}
		
		return array($width, $height);
	}
	
	
	/**
	 * Get minimum size
	 * 
	 * This method calculates a new width and height, in order to make
	 * the original width and height fit within the restrictions of a 
	 * given minimum size. In doing so, this method makes sure to 
	 * maintain the image aspect ratio.
	 * 
	 * As a result mostly only one dimension-requirement will be met, and only
	 * if the given width and height is smaller as the given requirements it 
	 * will not meet the requirements. By default the image will be resized to 
	 * a bigger and or height, but this can be disabled by setting the $allowGrow
	 * parameter to FALSE
	 * 
	 * Example 1
	 * <code>
	 *    // Construct an image:
	 *    $image = new M_Image('files/my-image.jpg');
	 *    
	 *    // Get the images current size:
	 *    // (We assume this is 400 x 300)
	 *    list($curWidth, $curHeight) = $image->getDimensions();
	 *    
	 *    // If we want the image to fit in an area of 300 x 200,
	 *    // we would do the following:
	 *    // (result will be 265 x 200)
	 *    list($newWidth, $newHeight) = M_ImageRatio::getMinSize($curWidth, $curHeight, 300, 200);
	 *    
	 *    // If we want the image to fit in an area of 200 x 250,
	 *    // we would do the following:
	 *    // (result will be 332 x 250)
	 *    list($newWidth, $newHeight) = M_ImageRatio::getMaxSize($curWidth, $curHeight, 200, 250);
	 * </code>
	 * 
	 * {@link M_FilterImageResize} uses this method to calculate the
	 * new size of the resized image.
	 * 
	 * @param integer $width
	 * 		The current width of the image
	 * @param integer $height
	 * 		The current height of the image
	 * @param integer $maxWidth
	 * 		The maximum width
	 * @param integer $maxHeight
	 * 		The maximum height
	 * @param bool $allowGrow
	 * 		Allow the image to grow (if it's smaller as the required width or height)
	 * @return array
	 */
	public static function getMinSize($width, $height, $minWidth, $minHeight, $allowGrow = true) {
		$ratioW = ($minWidth  / $width);
		$ratioH = ($minHeight / $height);
		$ratio  = max(array($ratioW, $ratioH));
		
		//if ratio is bigger as 1, this means the image is smaller as the given
		//requirements. We only resize the image to a bigger w or h when we are
		//allowed to
		if($ratio < 1 || ($allowGrow == true && $ratio > 1)) {
			return array(round($width * $ratio), round($height * $ratio));
		}
		
		//don't grow: return original values
		return array($width, $height);
	}
}