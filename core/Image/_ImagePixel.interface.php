<?php
/**
 * MI_ImagePixel interface
 * 
 * Implemented by {@link M_ImagePixel}.
 * 
 * @package Core
 */
interface MI_ImagePixel {
	/**
	 * Constructor
	 *
	 * @access public
	 * @param integer $x
	 * 		The X Coordinate of the pixel
	 * @param integer $y
	 * 		The Y Coordinate of the pixel
	 * @param M_Color $color
	 * 		The color of the pixel
	 * @return MI_ImagePixel
	 */
	public function __construct($x, $y, M_Color $color);
	
	/**
	 * Get X and Y Coordinates
	 * 
	 * Will return both the X and Y Coordinates of the pixel.
	 *
	 * @access public
	 * @return array
	 */
	public function getXY();
	
	/**
	 * Get X Coordinate
	 *
	 * @access public
	 * @return integer
	 */
	public function getX();
	
	/**
	 * Get Y Coordinate
	 *
	 * @access public
	 * @return integer
	 */
	public function getY();
	
	/**
	 * Get color
	 *
	 * @access public
	 * @return M_Color
	 */
	public function getColor();
	
	/**
	 * Set color
	 * 
	 * @access public
	 * @param M_Color $color
	 * 		The new color of the pixel
	 * @return void
	 */
	public function setColor(M_Color $color);
	
	/**
	 * Set X Coordinate
	 * 
	 * @access public
	 * @param integer $x
	 * 		The new X Coordinate of the pixel
	 * @return void
	 */
	public function setX($x);
	
	/**
	 * Set Y Coordinate
	 * 
	 * @access public
	 * @param integer $y
	 * 		The new Y Coordinate of the pixel
	 * @return void
	 */
	public function setY($y);
}
?>