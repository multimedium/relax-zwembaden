<?php
/**
 * M_ImageResourceGd class
 * 
 * This class is a specific implementation of {@link MI_ImageResource},
 * based on the GD Library.
 * 
 * NOTE:
 * This class can be used to create a new (empty) image resource. If
 * you want to create an image resource out of existing image data,
 * you should use the following classes instead:
 * 
 * - {@link M_ImageResourceGdFromFile}
 * - {@link M_ImageResourceGdFromStream}
 * 
 * @package Core
 */
class M_ImageResourceGd extends M_ImageResourceAbstract {
	/**
	 * Image resource identifier
	 * 
	 * This property stores the internal image resource identifier,
	 * which is created by functions such as PHP's imagecreatetruecolor()
	 *
	 * @access protected
	 * @var resource
	 */
	protected $_resource;
	
	/**
	 * Image width
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_width;
	
	/**
	 * Image height
	 *
	 * @access protected
	 * @var integer
	 */
	protected $_height;

	/**
	 * Holds all the image-filters which are processed using {@link M_ImageResourceGd::_applyImageFilters()}
	 *
	 * For more information please consult http://be2.php.net/manual/en/function.imagefilter.php
	 * @see M_ImageResourceGd::grayscale()
	 * @see M_ImageResourceGd::blur()
	 * @see M_ImageResourceGd::gaussianBlur()
	 * @see M_ImageResourceGd::_applyImageFilters()
	 * @see M_ImageResourceGd::_addImageFilter()
	 * @var array
	 */
	protected $_imageFilters = array();
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param integer $width
	 * 		The image's width, expressed in a number of pixels
	 * @param integer $height
	 * 		The image's height, expressed in a number of pixels
	 * @return M_ImageResourceGd
	 */
	public function __construct($width, $height) {
		$this->_resource = $this->_getNewResource($width, $height);
		$this->_width = $width;
		$this->_height = $height;
	}
	
	/**
	 * Destructor
	 * 
	 * PHP says:
	 * The destructor method will be called as soon as all references 
	 * to a particular object are removed or when the object is 
	 * explicitly destroyed or in any order in shutdown sequence.
	 * 
	 * NOTE:
	 * M_ImageResourceGd uses this magic method to destroy the image
	 * resource that has been created internally.
	 * 
	 * @access public
	 * @return void
	 */
	public function __destruct() {
		imagedestroy($this->_resource);
	}
	
	/**
	 * Get width
	 * 
	 * @access public
	 * @see MI_ImageResource::getWidth()
	 * @return integer
	 */
	public function getWidth() {
		return $this->_width;
	}
	
	/**
	 * Get height
	 * 
	 * @access public
	 * @see MI_ImageResource::getHeight()
	 * @return integer
	 */
	public function getHeight() {
		return $this->_height;
	}
	
	/**
	 * Get pixel
	 * 
	 * This method can be used to get a specific pixel from the image 
	 * resource. Note that you specify the pixel by providing its X-
	 * and Y-coordinates.
	 * 
	 * NOTE:
	 * This method will throw an M_ImageException, if the requested 
	 * pixel does not exist in the image resource. For example: you
	 * cannot ask for pixel (100, 75) if the image is only 50 pixels
	 * wide.
	 *
	 * @access public
	 * @see MI_ImageResource::getPixel()
	 * @throws M_ImageException
	 * @param integer $x
	 * 		The X-Coordinate
	 * @param integer $y
	 * 		The Y-Coordinate
	 * @return M_ImagePixel
	 */
	public function getPixel($x, $y) {
		if($x > $this->_width || $y > $this->_height) {
			throw new M_ImageException('Could not find pixel for coordinates (%d,%d)', $x, $y);
		}
		return new M_ImagePixel($x, $y, new M_Color(M_Color::getRGBInInteger(imagecolorat($this->_resource, $x, $y))));
	}
	
	/**
	 * Get average lightness
	 * 
	 * Will get the average lightness of the image.
	 * The higher the sample rate, the higher the accuracy.
	 * 
	 * 
	 * @param int $samples
	 * @return int
	 */
	public function getAverageLightness($samples = 10) {
		$samples = (int) $samples;
		
		// calculate the steps
		$xStep = floor($this->getWidth() / $samples);
		$yStep = floor($this->getHeight() / $samples);

		// prepare the lightness counter
		$lightness = 0;
		
		// loop trough the pixels
		for ($x = 0; $x < $samples; $x++) {
			for ($y = 0; $y < $samples; $y++) {	
				// add the ligthness of the current pixel
				$lightness += $this
					->getPixel($x * $xStep, $y * $yStep)
					->getColor()
					->getLightness();
			}
		}

		// return the average
		return ($lightness / pow($samples,2));
	}
	
	/**
	 * Set pixel
	 * 
	 * This method can be used to change a given pixel in the image.
	 * Note that you do not need to provide the X- and Y-coordinates
	 * of the pixel, because that is already contained by the pixel
	 * object itself.
	 *
	 * @access public
	 * @param MI_ImagePixel $pixel
	 * 		The pixel to be changed in the image
	 * @return void
	 */
	public function setPixel(MI_ImagePixel $pixel) {
		imagesetpixel($this->_resource, $pixel->getX(), $pixel->getY(), $this->_getColorIndex($pixel->getColor()));
	}
	
	/**
	 * Insert image
	 * 
	 * This method can be used to insert/paste an image into the 
	 * existing image resource. Note that you can specify the position
	 * at which the image should be pasted, by passing in the top left
	 * X- and Y-coordinates. Also, you can specify the size of the 
	 * inserted image, by passing in two additional arguments: the width
	 * and the height.
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 *
	 * @access public
	 * @param M_Image $image
	 * 		The image to be inserted
	 * @param integer $x
	 * 		The X-Coordinate of the top left corner (optional)
	 * @param integer $y
	 * 		The Y-Coordinate of the top left corner (optional)
	 * @param integer $width
	 * 		The width of the inserted image (optional)
	 * @param integer $height
	 * 		The height of the inserted image (optional)
	 * @return boolean
	 */
	public function insertImage(M_Image $image, $x = 0, $y = 0, $width = NULL, $height = NULL) {
		// create new image resource out of provided image:
		$rs = $this->_getResource($image);

		// get the inserted image's dimensions:
		$dim = $image->getDimensions();
		
		// get the width and height from $image if not provided
		if (is_null($width)) $width = $image->getWidth();
		if (is_null($height)) $height = $image->getHeight();

		// Re-activate alpha-blending, temporarily
		imagealphablending($this->_resource, true);

		// copy the new image resource into the resource:
		$success = (boolean) (imagecopyresampled($this->_resource, $rs, $x, $y, 0, 0, $width, $height, $dim[0], $dim[1]));
		
		// free associated memory (new image resource)
		imagedestroy($rs);
		
		// Disable alpha-blending again
		imagealphablending($this->_resource, false);

		// return success/failure:
		return $success;
	}
	
	/**
	 * Insert text
	 * 
	 * This method can be used to insert text into the image resource.
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 *
	 * @access public
	 * @param M_Font $font
	 * 		The font in which to write the text
	 * @param integer $x
	 * 		The X-Coordinate of the top left corner of the text's 
	 * 		bounding box
	 * @param integer $y
	 * 		The Y-Coordinate of the top left corner of the text's 
	 * 		bounding box
	 * @param string $text
	 * 		The text to be written
	 * @return boolean
	 */
	public function insertText(M_Font $font, $x, $y, $text) {
		// Fetch the RGB values
		$rgb = $font->getColor()->getRGB();
		
		// Create a new PHP color with them
		$color = imagecolorallocate($this->_resource, $rgb[0], $rgb[1], $rgb[2]);
		
		// Fetch the result, which will insert the text
		$result = imagettftext($this->_resource, $font->getSize(), 0, $x, $y , $color, M_Loader::getAbsolute($font->getPath()), $text);
		
		// DEPRECATED: this does NOT work!
		// $success = (boolean) imagestring($this->_resource, $id, $x, $y, $text, '0x' . $font->getColor()->getHex());
		
		// If everything went OK, we received an array with 8 elements, which
		// describe the coordinates the text was inserted at. Otherwise, we
		// received FALSE. As such, return the result as a boolean 
		return is_array($result);
	}
	
	/**
	 * Insert text center
	 * 
	 * This method can be used to insert text into the image resource.
	 * It wil also center the text based on the resource width and height
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 *
	 * @access public
	 * @param M_Font $font
	 * 		The font in which to write the text
	 * @param integer $x
	 * 		The X-Coordinate of the top left corner of the text's 
	 * 		bounding box
	 * @param integer $y
	 * 		The Y-Coordinate of the top left corner of the text's 
	 * 		bounding box
	 * @param string $text
	 * 		The text to be written
	 * @param integer $resourceWidth
	 * 		The width of the resource (eg.: an image)
	 * @param string $resourceHeight
	 * 		The height of the resource (eg.: an image)
	 * @param string $offsetX
	 * 		You can offset the x pos text when it's centered
	 * @param string $offsetY
	 * 		You can offset the y pos text when it's centered
	 * @return boolean
	 */
	public function insertTextCenter(M_Font $font, $x, $y, $text, $resourceWidth, $resourceHeight, $offsetX = 0, $offsetY = 0) {
		// Fetch the RGB values
		$rgb = $font->getColor()->getRGB();
		
		// Create a new PHP color with them
		$color = imagecolorallocate($this->_resource, $rgb[0], $rgb[1], $rgb[2]);
		
		// Start with smallets font size
		$fontSize = 1;
		
		// See if the text fits in our image
		// If it does, we can increase the font size
		// and calculate the center position
		do {
			$fontSize++;
			$p = imagettfbbox($fontSize,0,M_Loader::getAbsolute($font->getPath()),$text);
			$txt_width=$p[2]-$p[0];
		} while ($txt_width <= intval(0.8 * $resourceWidth));
		
		// Keep the max font size in mind
		if($fontSize > $font->getSize()) {
			$fontSize = $font->getSize();
		}

		// Calculate the dimensions of our text needed to center it
        $dimensions = imagettfbbox($fontSize, 0, M_Loader::getAbsolute($font->getPath()), $text);
        $x = ceil(($resourceWidth - $dimensions[4]) / 2);  
		$y = ($resourceHeight / 2) - $dimensions[5];

		// Print it
		$result = imagettftext($this->_resource, $fontSize, 0, $x + $offsetX, $y + $offsetY, $color, M_Loader::getAbsolute($font->getPath()), $text);
		
		return is_array($result);
	}

	/**
	 * Insert resource
	 * 
	 * @param M_ImageResourceGd $resource
	 * @param int $x
	 * @param int $y
	 * @param int $width
	 * @param int $height
	 * @return bool $flag
	 */
	public function insertResource(M_ImageResourceGd $resource, $x = 0, $y = 0, $width = NULL, $height = NULL) {
		// Re-activate alpha-blending, temporarily
		imagealphablending($this->_resource, true);

		// copy the new image resource into the resource:
		$success = (boolean) (imagecopyresampled($this->_resource, $resource->_resource, $x, $y, 0, 0, $resource->getWidth(), $resource->getHeight(),
			// Target width
			$width ? $width : $resource->getWidth(),
			// Target height
			$height ? $height : $resource->getHeight()));

		// Disable alpha-blending again
		imagealphablending($this->_resource, false);

		// return success/failure:
		return $success;
	}
	
	/**
	 * Draw filled rectangle
	 * 
	 * @access public
	 * @param M_Color $color
	 * 		The color with which to fill the rectangle
	 * @param int $alpha
	 * 		A value between 0 and 100. 0 indicates completely opaque while 100 indicates completely transparent. 
	 * @param integer $x
	 * 		The X-Coordinate of the rectangle's top left corner
	 * @param integer $y
	 * 		The Y-Coordinate of the rectangle's top left corner
	 * @param integer $width
	 * 		The rectangle's width
	 * @param integer $height
	 * 		The rectangle's height
	 * @return boolean
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function drawFill(M_Color $color, $alpha = 0, $x = 0, $y = 0, $width = NULL, $height = NULL) {
		// Set the new width
		if($width == NULL) {
			$width = $this->_width;
		}

		// Set the new height
		if($height == NULL) {
			$height = $this->_height;
		}

		// draw a fill
		return imagefilledrectangle(
			$this->_resource, 
			$x, 
			$y, 
			$x + $width, 
			$y + $height, 
			$this->_getColorIndex($color, $alpha)
		);
	}
	
	/**
	 * Draw a polygon image
	 * 
	 * <code>
	 * $points = array(
	 * 		0,30 //start at x = 0 and y = 30
	 * 		10,40 //continue at x = 10 and y = 40
	 * 		10,80 //continue at x = 10 and y = 80
	 * 		0,90 // stop at x = 0 and y = 80
	 * );
	 * $rs = new M_ImageResourceGd(100,200);
	 * $rs->drawPolygon($points, new M_Color('#146bc3'));
	 * </code>
	 * 
	 * @param array $points
	 * 		Array containing the points (alternative x and y values). The number
	 * 		of elements should always be a even number (since we always have x and y
	 * 		coördinates per point
	 * @param M_Color $fillColor
	 * 		The color whith which to fill the polygon
	 * @param int $alpha
	 * 		Alpha transparency (value from 0 - 100)
	 * @return boolean
	 * 		Returns TRUE on success, FALSE on failure
	 * @author Ben Brughmans
	 */
	public function drawPolygon( array $points, M_Color $fillColor, $alpha = 0) {
		return imagefilledpolygon(
			$this->_resource, 
			$points, 
			(int)count($points)/2, 
			$this->_getColorIndex($fillColor, $alpha)
		);
	}
	
	/**
	 * Resize the image
	 * 
	 * This method can be used to resize the image. You only need to
	 * provide the resource with a new width and height. Note that,
	 * in many cases, you will want to use {@link M_ImageRatio} to 
	 * calculate the image's new size.
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 * 
	 * @access public
	 * @param integer $width
	 * 		The image's new width
	 * @param integer $height
	 * 		The image's new height
	 * @return boolean
	 */
	public function resize($width, $height) {
		// create new (empty) image resource (of the size we're resizing to)
		$rs = $this->_getNewResource($width, $height);
		
		// copy the current resource to the new resource. In the process, we're
		// already resizing the resource to its new dimensions:
		if(imagecopyresampled($rs, $this->_resource, 0, 0, 0, 0, $width, $height, $this->_width, $this->_height)) {
			// destroy the current resource, and set the resized resource as
			// the new one:
			imagedestroy($this->_resource);
			$this->_resource = $rs;
			
			// update the dimension properties
			$this->_width  = $width;
			$this->_height = $height;
			
			// return success
			return TRUE;
		}
		// If failed to resize:
		else {
			// destroy the new image resource we created
			imagedestroy($rs);
			
			// return failure:
			return FALSE;
		}
	}
	
	/**
	 * Crop a part of image
	 * 
	 * This method can be used to crop a part out of the image. You 
	 * need to provide the X- and Y-coordinates of the crop rectangle's
	 * top left corner, and the size of the crop rectangle.
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 *
	 * @access public
	 * @param integer $x
	 * 		The X-Coordinate of the top left corner
	 * @param integer $y
	 * 		The Y-Coordinate of the top left corner
	 * @param integer $width
	 * 		The width of the crop rectangle
	 * @param integer $height
	 * 		The height of the crop rectangle
	 * @return boolean
	 */
	public function crop($x, $y, $width, $height) {
		// create new (empty) image resource (of the size we're cropping out)
		$rs = $this->_getNewResource($width, $height);

		// copy the cropped part to the new resource:
		if(imagecopy($rs, $this->_resource, 0, 0, $x, $y, $width, $height)) {
			// destroy the current resource, and set the cropped resource as
			// the new one:
			imagedestroy($this->_resource);
			$this->_resource = $rs;
			
			// update the dimension properties
			$this->_width  = $width;
			$this->_height = $height;
			
			// return success
			return TRUE;
		}
		// If failed to resize:
		else {
			// destroy the new image resource we created
			imagedestroy($rs);
			
			// return failure:
			return FALSE;
		}
	}
	
	/**
	 * Rotate an image
	 * 
	 * This method can be used to rotate the image
	 * 
	 * NOTE:
	 * The return value is TRUE on success, FALSE on failure.
	 *
	 * @access public
	 * @param integer $degrees
	 * 		The number of degrees you want to rotate
	 * @param M_Color $color
	 * 		The Color to be used for filling of empty area's
	 * @return boolean
	 */
	public function rotate($degrees, M_Color $fillColor = null) {
		// default the fill color to black
		if (!$fillColor) {
			$fillColor = new M_Color('000000');
		}
		
		// rotate the image and store the result
		$rotated = imagerotate($this->_resource, (int) $degrees, $this->_getColorIndex($fillColor, $fillColor->getAlpha()));
		
		// if the rotation return false, something went wrong
		if (! $rotated) {
			return false;
		}
		
		// replace the resource with the rotated version
		$this->_resource = $rotated;
		
		// set width and height again, based on the rotated image
		$this->_width = imagesx($this->_resource);
		$this->_height = imagesy($this->_resource);
	}
	
	/** -- Image filters functions -- */
	
	/**
	 * Add grayscale effect
	 *
	 * @author Ben Brughmans
	 * @return M_ImageResourceGd
	 */
	public function grayscale() {
		$this->_addImageFilter(IMG_FILTER_GRAYSCALE);
		return $this;
	}

	/**
	 * Blur the image
	 *
	 * @author Ben Brughmans
	 * @return M_ImageResourceGd
	 */
	public function blur() {
		$this->_addImageFilter(IMG_FILTER_SELECTIVE_BLUR);
		return $this;
	}

	/**
	 * Blur the image using Gaussian blur effect
	 *
	 * @author Ben Brughmans
	 * @return M_ImageResourceGd
	 */
	public function gaussianBlur() {
		$this->_addImageFilter(IMG_FILTER_GAUSSIAN_BLUR);
		return $this;
	}
	
	/**
	 * Pixelate the image
	 *
	 * @param int the blocksize in pixels
	 * @param bool advanced pixelation
	 * @return M_ImageResourceGd
	 */
	public function pixelate($blocksize = 10, $advanced = false) {
		$this->_addImageFilter(IMG_FILTER_PIXELATE, $blocksize, $advanced);
		return $this;
	}
	
	/**
	 * Sharpen the image
	 * 
	 * @access public
	 * @return M_ImageResourceGd
	 */
	public function sharpen() {
		// Sharpen settings:
		$matrix = array( 
			array(-1, -1, -1),
			array(-1, 16, -1),
			array(-1, -1, -1)
		); 

		// Calculate the sharpen divisor 
		$divisor = array_sum(array_map('array_sum', $matrix));            
		$offset = 0; 

		// Apply the matrix 
		imageconvolution($this->_resource, $matrix, $divisor, $offset);
		
		// Return myself:
		return $this;
	}

	/**
	 * Save to file
	 * 
	 * This method can be used to save the image resource to an actual
	 * image file. The return value of this method is an instance of
	 * {@link M_File} that represents the new image file.
	 *
	 * @access public
	 * @param string $filename
	 * 		The filename to which to save the image. Note that this is
	 * 		the filename (without extension), and not the basename (with
	 * 		extension)
	 * @param string $format
	 * 		The format to which the image should be exported. Possible
	 * 		values are: jpg, gif, png
	 * @param int $quality
	 * 		The quality of the new image file. This is an integer, describing the
	 * 		quality, and can range from 0 (worst) to 100 (best). The lower this
	 * 		number, the higher the compression and the lower the quality.
	 * @return M_File
	 */
	public function saveToFile($filename, $format, $quality = 85) {
		// Save to file
		$this->_saveToFile($format, $quality, $filename);
		
		// Return the file
		return new M_File($filename . '.' . $format);
	}
	
	/**
	 * Fetch
	 * @param int $quality
	 * @return string 
	 */
	public function fetch($format, $quality = 85) {
		// Only fetch the image
		return $this->_saveToFile($format, $quality, NULL);
	}

	/* -- PRIVATE -- */
	
	/**
	 * Save to file
	 * 
	 * @param string $format
	 * @param int $quality
	 * @param string $filename
	 *		Leave the filename NULL if you only want to fetch the image (not save)
	 *  
	 * @return bool
	 * @throws M_ImageException 
	 */
	protected function _saveToFile($format, $quality = 85, $filename = NULL) {
		// Compose the filename if its known:
		if($filename) {
			$path = $filename . '.' . $format;
		} else {
			$path = null;
		}
		
		// Make sure a valid quality indicator has been provided
		$isValidQuality = ($quality >= 0 && $quality < 101);
		
		// If not:
		if(! $isValidQuality) {
			// Then, we throw an exception:
			throw new M_ImageException(sprintf(
				'Cannot save image file %s with quality %d',
				$path,
				$quality
			));
		}

		// Apply additional image filters
		$this->_applyImageFilters();
		
		// The way in which we create an image file, depends on the requested format
		switch(strtolower($format)) {
			// JPEG Images
			case 'jpg':
			case 'jpeg':
				// Save the image
				$res = imagejpeg($this->_resource, $path, $quality);
				break;
			
			// GIF Images
			case 'gif':
				// Save the image
				$res = imagegif($this->_resource, $path);
				break;
			
			// PNG Images
			case 'png':
				
				// Save all alpha information
				imagesavealpha($this->_resource, true);
				
				// PNG Images are saved with a compression indicator and NOT
				// a quality indicator. This means 100 stands for compression 100
				// It ranges from 0 to 9.

				// Since PNG images are already saved with a lossles compression,
				// so it's pointless to compress more/less, that's why we skip
				// to quality parameter
				// 
				// Save the image
				$res = imagepng($this->_resource, $path);
				
				break;
			
			// No format set
			case null:
				throw new M_ImageException(
					'Cannot save file when format is not set'
				);
				
			// Other formats
			default:
				throw new M_ImageException(sprintf(
					'%s cannot save to "%s" files', 
					__CLASS__,
					$format
				));
		}
		
		return $res;
	}

	/**
	 * Internal method used to add image-filters
	 *
	 * When we add an image filters, this filter is stored internally and is
	 * applied only when the resource is saved to a file
	 *
	 * This way we can add multiple filters to a resource
	 *
	 * Please check http://be2.php.net/manual/en/function.imagefilter.php for
	 * more information about the different arguments
	 *
	 * @author Ben Brughmans
	 * @param int $filter
	 * @param mixed (optional)
	 * @param int (optional)
	 * @param int (optional)
	 * @return M_ImageResourceGd
	 */
	protected function _addImageFilter() {
		$args = func_get_args();
		if(!isset($args[0])) {
			throw new M_ImageException(
				'No filter-type given, please pass the filter-type as first argument to '.__FUNCTION__
			);
		}
		$this->_imageFilters[] = $args;
		return $this;
	}

	/**
	 * Apply all image filters to the resource
	 *
	 * Note: throws exception when filter could not be applied
	 *
	 * @author Ben Brughmans
	 * @return void
	 */
	protected function _applyImageFilters() {

		//iterate through all filters and apply them to the resource
		foreach($this->_imageFilters AS $filterArguments) {

			//use the resource as first argument in combination with the other
			//filter arguments to pass htem to the imagefilter function
			array_unshift($filterArguments, $this->_resource);
			$res = call_user_func_array('imagefilter', $filterArguments);
			
			if (!$res) {
				array_shift($filterArguments);
				throw new M_ImageException(sprintf(
					'Could not apply filter to the resource with arguments %s',
					implode(',', $filterArguments))
				);
			}
		}
	}
	
	/**
	 * Allocate color
	 * 
	 * This method returns a color identifier for the image resource 
	 * representing the given color.
	 *
	 * @param M_Color $color
	 * @param int $alpha
	 * 		Alpha-transparency: value from 0 till 100 (fully transparent)
	 * @return unknown 
	 * 		A color identifier or FALSE if the allocation failed
	 */
	protected function _getColorIndex(M_Color $color, $alpha = 0) {
		return imagecolorallocatealpha(
			$this->_resource, 
			$color->getChannel(M_Color::RED), 
			$color->getChannel(M_Color::GREEN), 
			$color->getChannel(M_Color::BLUE),
			$this->_getPHPAlphaValue($alpha) //re-calculate alpha value to php-standards
		);
	}
	
	/**
	 *  Re-calculate alpha to 127, this is the max alpha value used in PHP
	 *	since this isn't very intuitive, we changed this to 100.
	 * 
	 * @param int $alpha
	 * 		Alpha value from 0 - 100
	 * @return int $alpha
	 * 		Alpha value from 0 - 127
	 * @author Ben Brughmans
	 */
	protected function _getPHPAlphaValue($alpha) {
		return (int) $alpha * 1.27;
	}
	
	/**
	 * Get image resource
	 * 
	 * This method will mount an image resource, on which graphical
	 * operations can then be performed. Depending on the format of 
	 * the image, this method will use the following functions, in
	 * order to construct an image resource:
	 * 
	 * - {@link imagecreatefromjpeg()}
	 * - {@link imagecreatefromgif()}
	 * - {@link imagecreatefrompng()}
	 * 
	 * @access protected
	 * @param M_Image $image
	 * @return resource
	 */
	protected function _getResource(M_Image $image) {
		// If the image exists:
		if($image->exists()) {
			// The way in which we mount an image resource, depends on
			// the original format of the provided image. Before, we
			// used the extension of the image, in order to determine
			// its format:
			// switch(strtolower($image->getFileExtension())) {
			
			// Now, we use the image type that is provided to us by
			// getimagesize(). This will avoid problems with images 
			// that have been given a mistaken file extension.
			switch($image->getImageType()) {
				// JPEG Image
				case M_Image::TYPE_JPEG:
					return imagecreatefromjpeg($image->getPath());
				
				// GIF Image
				case M_Image::TYPE_GIF:
					return imagecreatefromgif($image->getPath());
				
				// PNG Image
				case M_Image::TYPE_PNG:
					return imagecreatefrompng($image->getPath());
				
				// Unsupported image formats:
				default:
					throw new M_ImageException(
						sprintf(
							'%s does not support "%s" images',
							__CLASS__, 
							$image->getFileExtension()
						)
					);
			}
		}
		// If the image does not yet exist:
		else {
			throw new M_ImageException(
				sprintf(
					'Cannot locate image file "%s"', 
					$image->getPath()
				)
			);
		}
	}
	
	/**
	 * Get new image resource
	 *
	 * This method will mount a new image resource of a given width and height,
	 * on which graphical operations can then be performed.
	 *
	 * @access protected
	 * @param integer $width
	 * @param integer $height
	 * @return resource
	 */
	protected function _getNewResource($width, $height) {
		// Create a truecolor image, of the given width and height:
		$rs = imagecreatetruecolor($width, $height);

		// ImageAlphaBlending allows for two different modes of drawing on
		// truecolor images. In blending mode, the alpha channel component of
		// the color supplied to all drawing function, such as imagesetpixel()
		// determines how much of the underlying color should be allowed to shine
		// through. As a result, gd automatically blends the existing color at
		// that point with the drawing color, and stores the result in the image.
		// The resulting pixel is opaque. In non-blending mode, the drawing color
		// is copied literally with its alpha channel information, replacing the
		// destination pixel. Blending mode is not available when drawing on
		// palette images.
		imagealphablending($rs, false);

		// Finally, we return the image resource:
		return $rs;
	}
}