<?php
/**
 * M_ImageResourcePixelIterator class
 * 
 * This class is used by implementations of {@link MI_ImageResource}.
 * It allows other objects to iterate through each and every pixel that
 * is contained by an image resource.
 * 
 * NOTE:
 * This class implements both the Countable and Iterator interface,
 * making it available in the foreach() structure as if it were an 
 * array.
 * 
 * @package Core
 */
class M_ImageResourcePixelIterator implements Countable, Iterator {
	/**
	 * Image resource
	 * 
	 * This property stores a reference to the {@link MI_ImageResource}
	 * object that contains the collection of pixels.
	 *
	 * @access private
	 * @var MI_ImageResource
	 */
	private $_resource;
	
	/**
	 * Countable support
	 * 
	 * This is an internal variable to support the Countable interface.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_count;
	
	/**
	 * Iterator support
	 * 
	 * This is an internal variable to support the Iterator interface. 
	 * More specifically, this is the current cursor position. Note 
	 * however that M_ImageResourcePixelIterator holds two cursors; 
	 * this is the X-Axis cursor.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_xIndex;
	
	/**
	 * Iterator support
	 * 
	 * This is an internal variable to support the Iterator interface. 
	 * More specifically, this is the current cursor position. Note 
	 * however that M_ImageResourcePixelIterator holds two cursors; 
	 * this is the Y-Axis cursor.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_yIndex;
	
	/**
	 * Image width
	 *
	 * @access private
	 * @var integer
	 */
	private $_xMaximum;
	
	/**
	 * Image height
	 *
	 * @access private
	 * @var integer
	 */
	private $_yMaximum;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param MI_ImageResource $resource
	 * 		The image resource that contains the collection of pixels.
	 * @return M_ImageResourcePixelIterator
	 */
	public function __construct(MI_ImageResource $resource) {
		$this->_resource = $resource;
		$this->_xMaximum = $resource->getWidth();
		$this->_yMaximum = $resource->getHeight();
		$this->_count = $this->_xMaximum * $this->_yMaximum;
		$this->_xIndex = 0;
		$this->_yIndex = 0;
	}
	
	/**
	 * Support for the Countable interface
	 *
	 * @access public
	 * @return integer
	 * 		The item count
	 */
	public final function count() {
		return $this->_count;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return M_ImagePixel
	 * 		The current pixel
	 */
	public final function current() {
		return $this->_resource->getPixel($this->_xIndex, $this->_yIndex);
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return string
	 * 		The key/name of the current pixel
	 */
	public final function key() {
		return 'pixel(' . $this->_xIndex . '-' . $this->_yIndex . ')';
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Moves the internal x-cursor to the next pixel, and moves the y-
	 * cursor to the next line if necessary.
	 * 
	 * @access public
	 * @return void
	 */
	public final function next() {
		if(++ $this->_xIndex > $this->_xMaximum) {
			$this->_xIndex = 0;
			$this->_yIndex += 1;
		}
	}
	
	/**
	 * Support for the Iterator interface
	 * 
	 * Resets the internal cursor for iteration. foreach() calls on
	 * an instance of the Registry class will call the rewind()
	 * method, to start looping at the beginning.
	 *
	 * @access public
	 * @return void
	 */
	public final function rewind() {
		$this->_xIndex = 0;
		$this->_yIndex = 0;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Will indicate if the end of the iterator has been reached.
	 * 
	 * @return boolean
	 * 		Will return TRUE if not reached the end of the iterator,
	 * 		or FALSE if all items in the iterator have been accessed
	 * 		in the loop.
	 */
	public final function valid() {
		return ($this->_xIndex < $this->_xMaximum || $this->_yIndex < $this->_yMaximum);
	}
}
?>