<?php
/**
 * M_Factory class
 * 
 * M_Factory is used to mount objects with a given definition. It is
 * used by classes such as {@link M_Form}, to identify class names,
 * method names, etc.
 * 
 * @package Core
 */
class M_Factory {
	/**
	 * Get class name
	 * 
	 * This method is used to parse a class name out of a definition
	 * string. Class names always start with a capital letter, and 
	 * words are separated using camel-casing. However, a definition
	 * can use all lowercase characters, separating words by a dash.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_Factory::getClassName('field-text');
	 * </code>
	 * 
	 * Example 1 will output the following class name:
	 * 
	 * <code>
	 *    FieldText
	 * </code>
	 * 
	 * @access public
	 * @static
	 * @param string $definition
	 * 		The definition string
	 * @return string
	 */
	public static function getClassName($definition) {
		return implode(array_map('ucfirst', preg_split('/[\s\-]+/', $definition)));
	}
	
	/**
	 * Get method name
	 * 
	 * This method is used to parse a method name out of a definition
	 * string. Method names always start with a lowercased letter, and 
	 * words are separated using camel-casing. However, a definition
	 * can use all lowercase characters, separating words by a dash.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_Factory::getMethodName('get-method-name');
	 * </code>
	 * 
	 * Example 1 will output the following method name:
	 * 
	 * <code>
	 *    getMethodName
	 * </code>
	 * 
	 * @access public
	 * @static
	 * @param string $definition
	 * 		The definition string
	 * @return string
	 */
	public static function getMethodName($definition) {
		$out = '';
		$elm = preg_split('/[\s\-]+/', $definition);
		for($i = 0, $n = count($elm); $i < $n; $i ++) {
			if($i == 0) {
				$out .= $elm[0];
			} else {
				$out .= ucfirst($elm[$i]);
			}
		}
		return $out;
	}
	
	/**
	 * Call method statically
	 * 
	 * This method will call a given class's method (also given by 
	 * name), optionally passing in the provided set of arguments.
	 * 
	 * The return value of this method is the return value of the 
	 * given class and method name.
	 * 
	 * NOTE:
	 * M_Factory will use the following methods, to parse both the 
	 * class name and the method name:
	 * 
	 * - {@link M_Factory::getClassName()}
	 * - {@link M_Factory::getMethodName()}
	 * 
	 * NOTE:
	 * This method will call the method STATICALLY. This means that
	 * M_Factory will NOT construct an object of the given class. If
	 * you want to call a method on a constructed object of the class,
	 * you should use {@link M_Factory::getObjectMethodReturn()}.
	 * 
	 * @access public
	 * @static
	 * @uses M_Factory::getClassName()
	 * @uses M_Factory::getMethodName()
	 * @param string $class
	 * 		The class name
	 * @param string $method
	 * 		The name of the method
	 * @param array $args
	 * 		The set of arguments to be passed into the method call
	 * @return string
	 */
	public static function getStaticMethodReturn($class, $method, array $args = array()) {
		return call_user_func_array(array(self::getClassName($class), self::getMethodName($method)), $args);
	}
	
	/**
	 * Call method
	 * 
	 * This method will call a given method of the provided object. 
	 * Optionally, the provided set of arguments is passed into the 
	 * method call.
	 * 
	 * The return value of this method is the return value of the 
	 * called method.
	 * 
	 * NOTE:
	 * M_Factory will use the following methods, to parse the method 
	 * name:
	 * 
	 * - {@link M_Factory::getMethodName()}
	 * 
	 * @access public
	 * @static
	 * @throws M_Exception
	 * @uses M_Factory::getMethodName()
	 * @param object $object
	 * 		The constructed object
	 * @param string $method
	 * 		The name of the method
	 * @param array $args
	 * 		The set of arguments to be passed into the method call
	 * @return string
	 */
	public static function getObjectMethodReturn($object, $method, array $args = array()) {
		$method = self::getMethodName($method);
		if(is_object($object)) {
			return call_user_func_array(array($object, $method), $args);
		} else {
			throw new M_Exception(__CLASS__ . ': ' . __FUNCTION__ . '() expects a constructed object');
		}
	}
	
	/**
	 * Construct object
	 * 
	 * This method will construct an object of a given class name.
	 * Optionally, the provided set of arguments is passed into the 
	 * class's constructor.
	 * 
	 * NOTE:
	 * M_Factory will use the following methods, to parse the class 
	 * name:
	 * 
	 * - {@link M_Factory::getClassName()}
	 * 
	 * NOTE:
	 * This method will not load the class, it will only (try to)
	 * construct an object of the class. It relies on the 
	 * {@link __autoload()} function to have the class loaded 
	 * automatically.
	 * 
	 * @access public
	 * @static
	 * @throws M_Exception
	 * @uses M_Factory::getClassName()
	 * @param object $object
	 * 		The constructed object
	 * @param string $method
	 * 		The name of the method
	 * @param array $args
	 * 		The set of arguments to be passed into the method call
	 * @return string
	 */
	public static function getObject($class, array $args = array()) {
		$class = self::getClassName($class);
		// check if class exists + autoload
		if(class_exists($class, TRUE)) {
			$c = new ReflectionClass($class);
			if($c->isInstantiable()) {
				$constructor   = $c->getConstructor();
				// If the class has a constructor:
				if($constructor instanceof ReflectionMethod) {
					$nArguments    = count($args);
					$nRequiredArgs = $constructor->getNumberOfRequiredParameters();
					if($nArguments == $nRequiredArgs) {
						if($nArguments == 0) {
							return $c->newInstance();
						} else {
							return $c->newInstanceArgs($args);
						}
					} else {
						throw new M_Exception($class . ' expects (at least) ' . $nRequiredArgs . ' arguments');
					}
				}
				// if the class does not have a constructor, but can
				// be instantiated:
				else {
					return new $class;
				}
			} else {
				throw new M_Exception($class . ' cannot be instantiated');
			}
		} else {
			throw new M_Exception($class . ' does not exist');
		}
	}
}
?>