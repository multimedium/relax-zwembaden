<?php
/**
 * M_ControllerException class
 * 
 * Exceptions thrown by:
 * 
 * - {@link M_ControllerDispatcher}
 * - {@link M_Controller}
 * 
 * @package Core
 */
class M_ControllerException extends M_Exception {
}
?>