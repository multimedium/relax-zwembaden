<?php
/**
 * ControllerDispatcherPathRewriter
 * 
 * @package Core
 */
class M_ControllerDispatcherPathRewriter {
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get a regular expression for given rule
	 * 
	 * Routing rules can be added to the Controller Dispatcher. A custom syntax
	 * can be used to describe strings, numbers, etc. in each rule's expression. 
	 * An overview of the syntaxes available:
	 * 
	 * <code>
	 *    Added           | Translates to
	 *    ----------------|-----------------------------
	 *    (:string)       | ([a-z0-9\.\-\+_]+)
	 *    (:num)          | ([0-9]+)
	 *    (:any)          | (.*)
	 * </code>
	 * 
	 * This method will convert the provided rule to a pattern of PCRE regex 
	 * syntax, as used by PHP's built-in functions such as preg_match(), 
	 * preg_replace(), etc...
	 * 
	 * Note that you can choose to ignore case in the regular expression that is
	 * returned by this function. This is particularly handy if you want the rule
	 * to be converted to an expression that does not take into account uppercase
	 * and lowercase matches.
	 * 
	 * @static
	 * @access public
	 * @param string $rule
	 *		The rule, optionally using custom syntaxes to describe matches
	 * @param bool $ignoreCase
	 *		Set to TRUE if the outputted expression should not take into account
	 *		uppercase and lowercase matches.
	 * @return string $expression
	 *		The regular expression in PCRE syntax
	 */
	public static function getRegularExpressionFromRule($rule, $ignoreCase = TRUE) {
		// Replace special words in the rule:
		$rule = strtr($rule, array(
			'/'          => '\\/',
			'(:num)'     => '([0-9]+)',
			'(:string)'  => '([a-z0-9\.\-\+_]+)',
			'(:any)'     => '(.*)',
			'(:locale)'  => self::getRegularExpressionLocales()
		));
		
		// Add the delimiters to the expression, and return the final expression:
		return ('/^'. $rule .'$/' . ($ignoreCase ? 'i' : ''));
	}
	
	/**
	 * Get a regular expression to match locales
	 * 
	 * This method will return return a piece of regular expression, that can be
	 * used to match a locale string against. Typically, this method is used to
	 * replace the (:locale) syntax in routing rules.
	 * 
	 * @static
	 * @access public
	 * @return string
	 */
	public static function getRegularExpressionLocales() {
		// Cache variable
		static $regexp = NULL;
		
		// If not requested before:
		if(! $regexp) {
			// Then, create the regular expression now:
			$regexp = '(' . implode('|', M_LocaleMessageCatalog::getInstalled()) . ')';
		}
		
		// Return the expression
		return $regexp;
	}
	
	/**
	 * Does the path match the rule?
	 * 
	 * @static
	 * @uses M_ControllerDispatcherPathRewriter::getRegularExpressionFromRule()
	 * @access public
	 * @param string $rule
	 *		The rule with which the provided path should match
	 * @param string $path
	 *		The path to be evaluated
	 * @param bool $ignoreCase
	 *		Set to TRUE if the check should not take into account uppercase and 
	 *		lowercase matches.
	 * @return bool $flag
	 *		Will return TRUE if the provided path matches the rule, FALSE if not.
	 */
	public static function isPathMatchingRule($rule, $path, $ignoreCase = TRUE) {
		// Convert the provided rule to a regular expression, and match:
		return (preg_match(self::getRegularExpressionFromRule($rule, $ignoreCase), $path) == 1);
	}
	
	/**
	 * Get locale and path from URI
	 * 
	 * This method is used by the {@link M_Controller} API in order to extract
	 * the locale and the (relative) path from a given instance of {@link M_Uri}.
	 * Among others, this is used by {@link M_ControllerDispatcher} in order to
	 * decide what the active path is, and ultimatly: which controller should handle
	 * the request.
	 * 
	 * @static
	 * @access public
	 * @param M_Uri $uri
	 * @return array $localeAndPath
	 *		An array with the locale at index 0, and the path at index 1
	 */
	public static function getLocaleAndPathFromUri(M_Uri $uri) {
		// Get base href on server:
		$base = M_Helper::trimCharlist(M_Server::getBaseHref(), '/');

		// Extract the path from the URI provided:
		$path = urldecode(M_Helper::trimCharlist($uri->getPath(), '/'));

		// If the path in the URI is empty, or equalling the base href on the server:
		if(empty($path) || $base == $path) {
			// Then, we return NULL as locale and path:
			return array(NULL, NULL);
		}

		// If the base href on the server is not empty (the application is not
		// hosted in the root), and the path's length is higher than the length
		// of the base href:
		if(! empty($base) && strlen($path) >= strlen($base)) {
			// Then, we trim the base href from the extracted path
			$path = M_Helper::trimCharlist(substr($path, strlen($base)), '/');
		}

		// If the path extracted is empty:
		if(! $path) {
			// Then, we return NULL as locale and path:
			return array(NULL, NULL);
		}
		
		// If locales are being enabled in the ControllerDispatcher:
		if(M_ControllerDispatcher::getInstance()->getEnableLocale()) {
			// Then, we shift off the first element from the path. This 
			// element should indicate the active locale:
			list($locale, $path) = M_Uri::shiftElementFromPath($path);

			// If the locale is not provided, or if the locale is not 
			// supported by this application:
			if(! $locale || ! M_LocaleMessageCatalog::isInstalled($locale)) {
				// Recompose the path
				$elm = array();
				if($locale) array_push($elm, $locale);
				if($path) array_push($elm, $path);
				$path = count($elm) > 0 ? implode('/', $elm) : NULL;
				
				// Then, return NULL as locale and the path:
				return array(NULL, $path);
			}
			
			// If still here, we return the locale and path
			return array($locale, $path ? $path : NULL);
		}
		
		// If still here, return NULL as locale and the path:
		return array(NULL, $path);
	}
}