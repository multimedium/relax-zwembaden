<?php
/**
 * M_Controller class
 * 
 * Typically, objects of M_Controller are instantiated by an object of 
 * {@link M_ControllerDispatcher}. M_Controller objects are in charge of handling 
 * a very specific page request.
 * 
 * This abstract base class provides with the basic functionality, shared by all 
 * controller implementations in the application.
 * 
 * Note that M_Controller inherits from {@link M_EventDispatcher}, in order to
 * fully comply with the {@link MI_Controller} interface.
 * 
 * @package Core
 */
abstract class M_Controller extends M_EventDispatcher implements MI_Controller {
	/**
	 * Magic routing
	 * 
	 * {@link M_ControllerDispatcher} will dispatch a given request to a controller.
	 * It will also check the routing rules that are defined by the specific 
	 * controller's __router() method.
	 * 
	 * The return value of this method is expected in the same way as expected
	 * by {@link M_ControllerDispatcher::setRoutingRules()}.
	 * 
	 * NOTE:
	 * This standard implementation of the router returns an empty array.
	 * 
	 * @see MI_Controller::__router()
	 * @access public
	 * @return array $rules
	 * 		Returns an array with routing/rewriting rules
	 */
	public function __router() {
		return array(
		);
	}
	
	/**
	 * Handle error
	 * 
	 * {@link M_ControllerDispatcher} will run the controller inside
	 * a try{} block. Any exception that is thrown during the processing
	 * of the page request, is catched and passed into the controller's
	 * handleError() method.
	 * 
	 * @see MI_Controller::handleError()
	 * @access public
	 * @param Exception $exception
	 * 		The exception that has been thrown, due to an error in the 
	 * 		page request.
	 * @return void
	 */
	public function handleError(Exception $exception) {
		$html  = '<h2>';
		$html .=    'Error ' . $exception->getCode();
		$html .= '</h2>';
		$html .= '<table cellpadding="5" cellspacing="0" border="0">';
		$html .=    '<tr>';
		$html .=       '<td valign="top">Message</td>';
		$html .=       '<td valign="top">' . $exception->getMessage() . '</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=       '<td valign="top">File</td>';
		$html .=       '<td valign="top">' . $exception->getFile() . '</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=       '<td valign="top">Line</td>';
		$html .=       '<td valign="top">' . $exception->getLine() . '</td>';
		$html .=    '</tr>';
		$html .=    '<tr>';
		$html .=       '<td valign="top">Trace As String</td>';
		$html .=       '<td valign="top"><pre>' . $exception->getTraceAsString() . '</pre></td>';
		$html .=    '</tr>';
		$html .= '</table>';
		echo $html;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get a (constructed) Data Object Mapper
	 * 
	 * @access protected
	 * @param string $dataObjectMapperClassName
	 * 		The class name of the data object mapper
	 * @param string $moduleId
	 * 		The module from which to load the mapper
	 * @return M_DataObjectMapper
	 */
	protected function _getDataObjectMapper($dataObjectMapperClassName, $moduleId = NULL) {
		// Load the mapper:
		M_Loader::loadDataObjectMapper(
			$dataObjectMapperClassName, 
			$moduleId
		);
		
		// Construct the mapper, and return as result:
		$dataObjectMapperClassName .= M_DataObjectMapper::MAPPER_SUFFIX;
		return new $dataObjectMapperClassName;
	}
	
	/**
	 * Get a (constructed) view
	 * 
	 * @access protected
	 * @param string $viewClassName
	 * 		The class name of the view
	 * @param string $moduleId
	 * 		The module from which to load the view
	 * @return M_View
	 */
	protected function _getView($viewClassName, $moduleId = NULL) {
		// Load the view:
		M_Loader::loadView($viewClassName, $moduleId);
		
		// Construct the view, and return as result:
		return new $viewClassName;
	}
	
	/**
	 * Get a (constructed) data object, or model
	 * 
	 * @access protected
	 * @param string $modelClassName
	 * 		The class name of the model
	 * @param string $moduleId
	 * 		The module from which to load the model
	 * @return M_View
	 */
	protected function _getModel($modelClassName, $moduleId = NULL) {
		// Load the model:
		M_Loader::loadModel($modelClassName, $moduleId);
		
		// Construct the model, and return as result:
		return new $modelClassName;
	}
	
	/**
	 * Get a (constructed) form
	 * 
	 * @access protected
	 * @param string $formClassName
	 * 		The class name of the form
	 * @param string $moduleId
	 * 		The module from which to load the form
	 * @return M_View
	 */
	protected function _getForm($formClassName, $moduleId = NULL) {
		// Load the form:
		M_Loader::loadForm($formClassName, $moduleId);
		
		// Construct the form, and return as result:
		return new $formClassName;
	}
	
	/**
	 * Get a (constructed) controller
	 * 
	 * @access protected
	 * @param string $controllerClassName
	 * 		The class name of the controller
	 * @param string $moduleId
	 * 		The module from which to load the controller
	 * @return M_View
	 */
	protected function _getController($controllerClassName, $moduleId = NULL) {
		// Load the controller:
		M_Loader::loadController($controllerClassName, $moduleId);
		
		// Construct the controller, and return as result:
		return new $controllerClassName;
	}
}