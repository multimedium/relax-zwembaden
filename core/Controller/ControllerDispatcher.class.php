<?php
/**
 * M_ControllerDispatcher
 * 
 * Typically, all requests in an application are routed through an
 * instance of M_ControllerDispatcher, which is then in charge
 * of delegating to another {@link M_Controller} instance.
 * 
 * M_ControllerDispatcher is an abstract page request handler, while
 * instances of {@link M_Controller} handle very specific requests.
 * 
 * - Create contexts
 * - Modify the way a specific URI is routed to a method of controller
 * - Validation "public" signature and of method arguments
 * - Control over different area's in the template
 * - Validate arguments to the URI, before dispatching
 * - Bind element in path to given task/variable (eg en/home)
 * 
 * NOTE:
 * Note that M_ControllerDispatcher implements the {@link MI_EventDispatcher} 
 * interface. This enables other classes to listen to events triggered 
 * by the M_ControllerDispatcher class. For more info, read the docs
 * on {@link M_ControllerDispatcherEvent}.
 * 
 * @package Core
 */
class M_ControllerDispatcher extends M_EventDispatcher implements MI_EventDispatcher {
	/**
	 * Singleton instance
	 * 
	 * @see M_ControllerDispatcher::getInstance()
	 * @static
	 * @access private
	 * @var M_ControllerDispatcher
	 */
	private static $_instance;
	
	/**
	 * The Request URI
	 * 
	 * This property stores the {@link M_Uri} instance with which the
	 * dispatcher has been constructed.
	 * 
	 * @access private
	 * @var M_Uri
	 */
	private $_uri;
	
	/**
	 * Path
	 * 
	 * This property stores the path that is being taken into account
	 * to dispatch to a {@link M_Controller}. This path is extracted
	 * from either:
	 * 
	 * - {@link M_ControllerDispatcher::$_uri}
	 * - {@link M_ControllerDispatcher::$_homepage}
	 * 
	 * @access private
	 * @var string
	 */
	private $_path = FALSE;
	
	/**
	 * Host
	 * 
	 * This property stores the host that is being taken into account to dispatch
	 * to a {@link M_Controller}. This host is extracted from one of these:
	 * 
	 * - {@link M_ControllerDispatcher::$_uri}
	 * - {@link M_ControllerDispatcher::$_homepage}
	 * 
	 * @access private
	 * @var string
	 */
	private $_host = NULL;
	
	/**
	 * Controller Namespaces
	 * 
	 * This property stores the controller namespaces, and is populated
	 * by {@link M_ControllerDispatcher::setControllerNamespaceByHost()}
	 * 
	 * @access private
	 * @var array
	 */
	private $_namespace = array();
	
	/**
	 * Homepage
	 * 
	 * This property stores the Homepage URI, which can be set by 
	 * {@link M_ControllerDispatcher::setHomepageUri()}.
	 * 
	 * @access private
	 * @var M_Uri
	 */
	private $_homepage;
	
	/**
	 * Enable locale?
	 * 
	 * This property stores a boolean flag, which indicates whether or not 
	 * locales should be enabled in the path. By default, locales are disabled
	 * in the controller dispatcher.
	 * 
	 * @see M_ControllerDispatcher::setEnableLocale()
	 * @access private
	 * @var boolean
	 */
	private $_enableLocale = FALSE;
	
	/**
	 * Enable sub-controllers?
	 * 
	 * @see M_ControllerDispatcher::setEnableSubControllers()
	 * @access private
	 * @var boolean
	 */
	private $_enableSubcontrollers = TRUE;
	
	/**
	 * Routing Rules
	 * 
	 * This property stores the routing rules, which can be set with
	 * {@link M_ControllerDispatcher::setRoutingRules()}.
	 * 
	 * @access private
	 * @var array
	 */
	private $_rules = array();
	
	/**
	 * PRIVATE Constructor
	 * 
	 * M_ControllerDispatcher is a singleton object. In order to construct the
	 * object, you should use {@link M_ControllerDispatcher::getInstance()}.
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * 		The Request URI. The dispatcher will analyze this URI, in
	 * 		order to identify the {@link M_Controller} instance that is
	 * 		in charge of handling the request.
	 * @return M_ControllerDispatcher
	 */
	private function __construct(M_Uri $uri) {
		$this->_uri = $uri;
	}
	
	/**
	 * Singleton Constructor
	 * 
	 * Typically, an {@link M_ControllerDispatcher} instance is created
	 * in the /index.php file of the installed application:
	 * 
	 * <code>
	 *    $dispatcher = M_ControllerDispatcher::getInstance(M_Request::getUri());
	 * </code>
	 * 
	 * @access public
	 * @return M_ControllerDispatcher
	 */
	public static function getInstance() {
		if(! self::$_instance) {
			self::$_instance = new self(M_Request::getUri());
		}
		return self::$_instance;
	}
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get class name
	 * 
	 * This method is used to parse a class name out of a string. Class 
	 * names always start with a capital letter, and words are separated 
	 * using camel-casing. However, a string can use all lowercase 
	 * characters, separating words by a dash.
	 * 
	 * Also, this method will append the word "Controller" to the 
	 * class name.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_ControllerDispatcher::getClassNameOutOfPathElement('my-example');
	 * </code>
	 * 
	 * Example 1 will output the following class name:
	 * 
	 * <code>MyExampleController</code>
	 * 
	 * NOTE:
	 * {@link M_ControllerDispatcher::getControllerCallback()} uses this
	 * method to compose the controller class name.
	 * 
	 * @access public
	 * @static
	 * @param string $string
	 * @return string
	 */
	public static function getClassNameOutOfPathElement($string) {
		return implode(array_map('ucfirst', preg_split('/[\s\-]+/', $string))) . 'Controller';
	}
	
	/**
	 * Get method name
	 * 
	 * This method is used to parse a method name out of a string. Method 
	 * names always start with a lowercased letter, and words are 
	 * separated using camel-casing. However, a string can use all 
	 * lowercase characters, separating words by a dash.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_ControllerDispatcher::getMethodNameOutOfPathElement('my-example');
	 * </code>
	 * 
	 * Example 1 will output the following method name:
	 * 
	 * <code>myExample</code>
	 * 
	 * NOTE:
	 * {@link M_ControllerDispatcher::getControllerCallback()} uses this
	 * method to identify the method name.
	 * 
	 * @access public
	 * @static
	 * @param string $definition
	 * 		The definition string
	 * @return string
	 */
	public static function getMethodNameOutOfPathElement($string) {
		$out = '';
		$elm = preg_split('/[\s\-]+/', $string);
		for($i = 0, $n = count($elm); $i < $n; $i ++) {
			if($i == 0) {
				$out .= $elm[0];
			} else {
				$out .= ucfirst($elm[$i]);
			}
		}
		return $out;
	}
	
	/**
	 * Is Controller Method Compliant?
	 * 
	 * This method will check whether or not the provided method (name) is 
	 * compliant, by making sure the method is
	 * 
	 * - implemented,
	 * - in the public scope,
	 * - expecting/accepting the arguments as provided here.
	 * 
	 * This method is also used internally, in order to check if a given controller
	 * can be used in order to handle the current request.
	 * 
	 * @static
	 * @access public
	 * @param M_Object $controller
	 * @param type $methodName
	 * @param array $methodArgs
	 * @return boolean
	 */
	public static function isControllerMethodCompliant(M_Object $controller, $methodName, array $methodArgs) {
		// Check if the provided controller implements the requested method. If
		// that is not the case, then the controller is not compliant:
		if(! method_exists($controller, $methodName)) {
			return FALSE;
		}
		
		// Now, we have a controller object and an implemented method name. We 
		// still need to make sure that the method is in the public scope! In 
		// order to do so, we'll use the Reflection API:
		$method = new ReflectionMethod(get_class($controller), $methodName);
		
		// If the method is private, the controller is not compliant. In that 
		// case, we return FALSE:
		if(! $method->isPublic()) {
			return FALSE;
		}
		
		// If still here, we still have a valid outcome! In that case, we also 
		// make sure that the number of arguments provided here is matching with 
		// the number of arguments expected/accepted by the controller method:
		if($method->getNumberOfRequiredParameters() > count($methodArgs)) {
			// If that is not the case, the controller is not compliant. So,
			// we return FALSE to indicate so:
			return FALSE;
		}
		
		// Return TRUE if still here:
		return TRUE;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get host
	 * 
	 * This method will return the host of the {@link M_Uri} that is
	 * being used to dispatch the request to an {@link M_Controller}.
	 * Note that, if the path is empty, this method will return the 
	 * host of the Homepage URI, which can be set with
	 * {@link M_ControllerDispatcher::setHomepageUri()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getHost() {
		// If the host has not been requested before:
		if(is_null($this->_host)) {
			// Notify with event, about the receival of the host name:
			$this->dispatchEvent(
				new M_ControllerDispatcherEvent(
					M_ControllerDispatcherEvent::RECEIVE_HOST, 
					$this
				)
			);
			
			// Get the path from the URI with which the dispatcher has been
			// constructed:
			$path = trim($this->_uri->getPath(), " \t\n\r\0\x0B/");
			
			// If that path is empty, we'll default to the host of the homepage
			// URI instead:
			$this->_host = (empty($path) && $this->_homepage)
				? $this->_homepage->getHost()
				: $this->_uri->getHost();
		}
		
		// Return host:
		return $this->_host;
	}
	
	/**
	 * Get the path
	 * 
	 * This method will return the path that is used by the ControllerDispatcher
	 * in order to dispatch the request to an instance of {@link M_Controller}.
	 * This path will be extracted from the {@link M_Uri} instance with which the
	 * dispatcher was constructed. If the path extracted from that {@link M_Uri} 
	 * instance is empty, the Homepage URI is used instead.
	 * 
	 * Note that, if resulting the path is empty, or if the path is not correctly
	 * formatted (eg. a missing or unsupported locale), this method will return
	 * NULL instead. In that case, the path cannot be determined by the controller
	 * dispatcher.
	 * 
	 * NOTE:
	 * If a path has been extracted, this function will rewrite the path using the 
	 * Routing Rules that may have been provided earlier with the dispatcher's
	 * {@link M_ControllerDispatcher::setRoutingRules()} method.
	 * 
	 * @access public
	 * @return string
	 */
	public function getPath() {
		// If not requested before:
		if($this->_path === FALSE) {
			// Get the Singleton Instance that is used to check protection 
			// against CSRF:
			$csrf = M_ControllerDispatcherCsrf::getInstance();
			
			// First, we try the URI with which the ControllerDispatcher has
			// been constructed. Get a copy of that URI, to make sure it does
			// not get modified by the validation processes performed further
			// down:
			$queue = array(
				// First in Queue:
				array(
					// The URL
					clone($this->_uri),
					// Use the path only (TRUE), or only locale (FALSE)
					TRUE
				)
			);
			
			// If a Homepage URI is provided as a backup
			if($this->_homepage) {
				// Then, we add it to the queue of URI's for processing:
				// (also a copy). 
				array_push($queue, array(
					// The URL
					clone($this->_homepage),
					// Note that we'll only use the locale from the
					// homepage...
					FALSE
				));
			}
			
			// Work variables:
			$isMissingPath = TRUE;
			
			// For each of the URI's in the queue
			foreach($queue as $current) {
				// Get the URI and configuration var:
				$uri = $current[0];
				$usePath = $current[1];
				
				/* @var $uri M_Uri */
				// We extract the locale and the path from the URI:
				list($locale, $path) = M_ControllerDispatcherPathRewriter::getLocaleAndPathFromUri($uri);
				
				// Is the locale missing?
				$isMissingLocale = ($this->getEnableLocale() && ! $locale);
				
				// If NOT missing locale, and NO LOCALE has been configured yet:
				if(! $isMissingLocale && ! M_Locale::getCategory(M_Locale::LANG)) {
					// Set the active locale
					M_Locale::setCategory(M_Locale::LANG, $locale);

					// Also, we set the locale as a prefix for links that are 
					// generated in the application:
					M_Request::setLinkPrefix($locale);
				}
				
				// If we should not use the current URI's path, then we continue 
				// to the next in the queue. Note however that we make an exception,
				// if the path of the 
				if(! $usePath && ! $isMissingPath) {
					continue;
				}
				
				// If the URI is protected, but not correctly secured against CSRF:
				if(! $csrf->getValidationAndRemoveProtectionIn($uri)) {
					// We return NULL. This will cause the dispatcher to route to 
					// the ErrorController, as if the page did not exist:
					return $this->_getPathReturnValue(NULL);
				}
				// If the URI is secure:
				else {
					// Check on the path:
					$isMissingPath = (! $path);
					
					// If all elements are present in the URI
					if(! $isMissingLocale && ! $isMissingPath) {
						// Return the path (rewritten):
						return $this->_getPathReturnValue(
							$this->_getRewrittenPath($path, $this->_rules)
						);
					}
				}
			}
			
			// If still here, we return NULL. This will cause the dispatcher 
			// to route to the ErrorController (non-existing page).
			return $this->_getPathReturnValue(NULL);
		}
		
		// return path
		return $this->_path;
	}
	
	/**
	 * Get path element, by index
	 * 
	 * Looks up the element in the path, selected by its index number, and returns 
	 * it. If the path element could not be found, this method will return NULL 
	 * instead.
	 * 
	 * @access public
	 * @param integer $index
	 * @return string|NULL
	 */
	public function getPathElement($index) {
		// Get the path elements:
		$elm = explode('/', M_Helper::trimCharlist((string) $this->getPath(), '/'));
		
		// Return the requested element
		return (
			array_key_exists($index, $elm)
				? $elm[$index]
				: NULL
		);
	}
	
	/**
	 * Is Homepage?
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Will return TRUE if the current request routes to the homepage
	 */
	public function isHomepage() {
		return (M_Helper::trimCharlist($this->getUri()->toString(), '/') == M_Helper::trimCharlist($this->getHomepageUri()->toString(), '/'));
	}
	
	/**
	 * Get Homepage URI
	 * 
	 * Will provide with the {@link M_Uri} instance that is has been assigned
	 * as the homepage by {@link M_ControllerDispatcher::setHomepageUri()}.
	 * 
	 * @access public
	 * @return M_Uri
	 */
	public function getHomepageUri() {
		return $this->_homepage;
	}
	
	/**
	 * Get URI
	 * 
	 * Will provide with the {@link M_Uri} instance that is being used to handle
	 * the request. You can use this method, for example, to get the original 
	 * request URI (not rewritten by {@link M_ControllerDispatcher::getPath()}).
	 * 
	 * @access public
	 * @return M_Uri
	 */
	public function getUri() {
		// Get base href on server:
		$basePath = M_Helper::trimCharlist(M_Server::getBaseHref(), "/");
		
		// Check the path of the original request URI
		$path = M_Helper::trimCharlist($this->_uri->getPath(), "/");
		
		// If the path is empty:
		if(empty($path) || $basePath == $path) {
			// Return the Homepage URI (if available):
			if($this->_homepage) {
				return $this->_homepage;
			}
		}
		
		// Return the original request URI, if available
		return $this->_uri;
	}
	
	/**
	 * Get controller callback
	 * 
	 * This method will lookup the {@link M_Controller} class name that
	 * corresponds to the {@link M_Uri} instance for which the dispatcher
	 * has been set up:
	 * 
	 * {@link M_ControllerDispatcher::__construct()}
	 * {@link M_ControllerDispatcher::setUri()}
	 * 
	 * It will construct an instance of that class, and also identify 
	 * the method that needs to be called of that object, in order to
	 * handle the Request URI. Additionally, an array of arguments is 
	 * parsed, which should be passed into the method call.
	 * 
	 * Let's assume that an M_ControllerDispatcher instance is being 
	 * constructed for the following request:
	 * 
	 * <code>http://multimedium.be/info/contact</code>
	 * 
	 * In the normal dispatcher logic, this request would cause
	 * getControllerCallback() to construct an instance of the "Info" 
	 * controller class, and to provide "contact" as the name of the
	 * method. Since no more arguments are available in the path, an
	 * empty array of arguments would be parsed. 
	 * 
	 * The return value of this method is an array, that contains two
	 * elements:
	 * 
	 * - The definition of the callback function (object + method name)
	 * - The arguments for the callback function
	 * 
	 * So, typically, you would use this method as follows:
	 * 
	 * Example 1
	 * <code>
	 *    $dispatcher = M_ControllerDispatcher::getInstance(M_Request::getUri());
	 *    list($callback, $args) = $dispatcher->getControllerCallback();
	 * </code>
	 * 
	 * With the result of Example 1, you could dispatch the request
	 * to the corresponding controller by adding the following line to 
	 * your code:
	 * 
	 * <code>
	 *    call_user_func_array($callback, $args);
	 * </code>
	 * 
	 * NOTE:
	 * If no method name could have been extracted, the method name 
	 * will be defaulted to "index".
	 * 
	 * @uses M_ControllerDispatcher::isControllerMethodCompliant()
	 * @access public
	 * @return array
	 */
	public function getControllerCallback() {
		// Get the path of the current request:
		$path = $this->getPath();
		
		// If the path is empty
		if(! $path) {
			// Then, we return the ErrorController in order to indicate a 
			// non-existing page in the application:
			return $this->getErrorControllerCallback();
		}
		
		// We extract the collection of path elements:
		$elm = explode('/', $path);
		
		// With the collection of path elements, we construct a controller object:
		$controller = $this->_getControllerObjectFromPathElements($elm);
		
		// If no controller could have been identified:
		if(! $controller) {
			// We translate that into an HTTP 404 File Not Found error, by returning
			// the ErrorController
			return $this->getErrorControllerCallback();
		}

		// Dispatch an event with the controller:
		$event = new M_ControllerDispatcherEvent(M_ControllerDispatcherEvent::CONSTRUCT_CONTROLLER, $this);
		$event->setController($controller);
		$this->dispatchEvent($event);
		
		// We compose the name of the method. Note that the name 
		// of the method is defaulted to "index" if not available:
		$methodName = array_shift($elm);
		if(! $methodName) {
			$methodName = 'index';
		}
		// Note that we do not allow some predefined method names
		// (eg handleError is a predefined method of M_Controller)
		elseif($methodName == 'handleError') {
			$methodName = 'index';
		}
		
		// We check if the controller object provides with an implementation of 
		// the requested method. First of all, we rewrite the path, with the 
		// routing rules that may be provided by the controller to which we are
		// dispatching:
		$router = $controller->__router();
		if(! is_array($router)) {
			throw new M_ControllerException(sprintf(
				'%s expects an array from MI_Controller::__router()',
				__CLASS__
			));
		}
		
		// Rewrite, and recompose:
		// - Path Elements
		// - Method Name
		$route      = implode('/', $elm);
		$elm        = explode('/', $this->_getRewrittenPath(empty($route) ? $methodName : $methodName . '/' . $route, $router, 1));
		$methodName = self::getMethodNameOutOfPathElement(array_shift($elm));
		
		// Make sure the controller method is compliant. If that is not the case:
		if(! self::isControllerMethodCompliant($controller, $methodName, $elm)) {
			// Then, we translate that into an HTTP 404 File Not Found error, 
			// by returning the ErrorController
			return $this->getErrorControllerCallback();
		}
		
		// Return the final callback definition
		return array(array($controller, $methodName), $elm);
	}
	
	/**
	 * Get Error Controller callback
	 * 
	 * Used by {@link M_ControllerDispatcher::getControllerCallback()}
	 * if no controller could have been found for the Request URI.
	 * This method has the same return value; an array, that contains 
	 * two elements:
	 * 
	 * - The definition of the callback function (object + method name)
	 * - The arguments for the callback function (empty)
	 * 
	 * The return value
	 * <code>
	 *    Array(
	 *       Array(
	 *          #ErrorController,
	 *          'index'
	 *       ),
	 *       Array()
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @return ErrorController
	 */
	public function getErrorControllerCallback() {
		// Load the Error Controller
		M_Loader::loadController('ErrorController');

		// Construct the controller:
		$controller = new ErrorController();

		// Dispatch an event with the controller:
		$event = new M_ControllerDispatcherEvent(M_ControllerDispatcherEvent::CONSTRUCT_CONTROLLER, $this);
		$event->setController($controller);
		$this->dispatchEvent($event);

		// Return the callback:
		return array(array($controller, 'index'), array());
	}
	
	/**
	 * Is locale enabled?
	 * 
	 * This method will tell whether or not locales are enabled in the controller
	 * dispatcher. For more information about enabling locales in the dispatcher,
	 * please read {@link M_ControllerDispatcher::setEnableLocale()}.
	 * 
	 * @see M_ControllerDispatcher::setEnableLocale()
	 * @access public
	 * @return boolean
	 *		Returns TRUE if enabled, FALSE if not
	 */
	public function getEnableLocale() {
		return $this->_enableLocale;
	}
	
	/**
	 * Is subcontrollers enabled?
	 * 
	 * @see M_ControllerDispatcher::setEnableSubcontrollers()
	 * @access public
	 * @return boolean
	 *		Returns TRUE if enabled, FALSE if not
	 */
	public function getEnableSubcontrollers() {
		return $this->_enableSubcontrollers;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set URI
	 * 
	 * This method can be used to set the URI for which the controller
	 * dispatcher should identify the {@link M_Controller} instance to
	 * be used.
	 * 
	 * Typically, this method is used by the {@link M_ControllerDispatcher}
	 * to initiate the dispatcher with a given {@link M_Uri} instance.
	 * 
	 * @see M_ControllerDispatcher::__construct()
	 * @access public
	 * @param M_Uri $uri
	 * @return void
	 */
	public function setUri(M_Uri $uri) {
		$this->_uri  = $uri;
		$this->_path = NULL;
	}
	
	/**
	 * Set path
	 * 
	 * This method allows to set a new path for the ControllerDispatcher. This
	 * way, you can force the request to be rewritten to a different controller
	 * after all, should this be required.
	 * 
	 * Note however that, since you are setting the new path yourself with this
	 * function, no event will be dispatched to inform about the new path. Such
	 * an event is dispatched only if the path is auto-discovered by this class,
	 * with {@link M_ControllerDispatcher::getPath()}.
	 * 
	 * @access public
	 * @param string $path
	 * @return void
	 */
	public function setPath($path) {
		$this->_path = M_Helper::trimCharlist((string) $path, '/');
	}
	
	/**
	 * Shifts the first value of the path off, and returns it
	 * 
	 * Shifts the first value of the path off and returns it, shortening 
	 * the path by one element. Typically, this method is used to
	 * remove elements from the path, before dispatching to an instance
	 * of {@link M_Controller}.
	 * 
	 * NOTE:
	 * If no more elements are available in the path, this method will
	 * return (boolean) FALSE.
	 * 
	 * @access public
	 * @return string|boolean
	 */
	public function shiftPathElement() {
		list($first, $path) = M_Uri::shiftElementFromPath($this->getPath());
		$this->_path = $path;
		return $first;
	}
	
	/**
	 * Remove path element, by index
	 * 
	 * This method allows to remove a given element from the path. The element
	 * is selected by providing this method with an index number (starting at
	 * zero).
	 * 
	 * @access public
	 * @param integer $index
	 * @return void
	 */
	public function removePathElement($index) {
		// Get the elements in the current path:
		$elements = explode('/', M_Helper::trimCharlist((string) $this->getPath(), '/'));
		
		// Cast the provided index to an integer:
		$index = (int) $index;
		
		// If an element at that path can be found:
		if(array_key_exists($index, $elements)) {
			// Then, we remove the element:
			unset($elements[$index]);
		}
		
		// Set the new path:
		$this->_path = implode('/', $elements);
	}
	
	/**
	 * Set homepage
	 * 
	 * This method can be used to set the homepage of the website. When
	 * a request comes in with an empty path, M_ControllerDispatcher
	 * will redirect the request to the URI that has been set up as
	 * the homepage.
	 * 
	 * @access public
	 * @param M_Uri $uri
	 * 		The Homepage URI
	 * @return void
	 */
	public function setHomepageUri(M_Uri $uri) {
		$this->_homepage = $uri;
	}
	
	/**
	 * Create Controller Namespace, by host
	 * 
	 * Consider the following example: Your code is running on one 
	 * single server machine, but two different domain names can be 
	 * used to access the same application. No problem so far! But maybe 
	 * you would want to create a different context for each of the 
	 * domains, because functionality might be slightly (or 
	 * significantly) different for the two domains.
	 * 
	 * Let's turn this scenario into a more real example, by assuming
	 * that the following two domains have been made available to access
	 * the same application:
	 * 
	 * <code>
	 *    http://www.multimedium.be
	 *    http://www.censio.be
	 * </code>
	 * 
	 * With this setup, the requests listed below will be dispatched to
	 * one single controller (the same for both domains): the Portfolio
	 * controller.
	 * 
	 * <code>
	 *    http://www.multimedium.be/portfolio
	 *    http://www.censio.be/portfolio
	 * </code>
	 * 
	 * However, maybe you want the application to behave differently on 
	 * each of the domains. To acomplish this, you would have to create 
	 * a context for each of the domains. In order to do so, you could 
	 * do the following:
	 * 
	 * <code>
	 *    $dispatcher = M_ControllerDispatcher::getInstance(M_Request::getUri());
	 *    $dispatcher->setControllerNamespaceByHost('www.multimedium.be', 'M');
	 *    $dispatcher->setControllerNamespaceByHost('www.censio.be', 'C');
	 * </code>
	 * 
	 * Now, {@link M_ControllerDispatcher} will route the Request URI's
	 * to different controllers for each of the domains, even though
	 * the requested path is the same. Instead of dispatching the request
	 * to one single controller (Portfolio), two different controllers
	 * will be used with this new setup: MPortfolio and CPortfolio.
	 * 
	 * @see M_Request::getHost()
	 * @access public
	 * @param string $host
	 * 		The host for which a context is to be created
	 * @param string $name
	 * 		The namespace
	 * @return void
	 */
	public function setControllerNamespaceByHost($host, $name) {
		$this->_namespace[(string) $host] = (string) $name;
	}
	
	/**
	 * Set flag: Enable locales
	 * 
	 * This method allows to enable locales in the Controller Dispatcher. When 
	 * enabled, the first element in the active path is interpreted as a locale
	 * string. This locale string will then be used to set the active locale.
	 * For more information about the active locale, please read the docs on:
	 * 
	 * - {@link M_Locale::setCategory()}
	 * - {@link M_Locale::LANG}
	 * 
	 * If you want to enable locales in the path that is handled by the controller
	 * dispatcher, you should provide TRUE as an argument to this method. If not,
	 * you should provide FALSE instead.
	 * 
	 * Note that, when enabled, you do not need to include the locale string in 
	 * routing rules anymore. For more information about setting routing rules,
	 * please read {@link M_Locale::setRoutingRules()}
	 * 
	 * @see M_ControllerDispatcher::$_enableLocale
	 * @access public
	 * @param bool $flag
	 * @return void
	 */
	public function setEnableLocale($flag) {
		$this->_enableLocale = (bool) $flag;
	}
	
	/**
	 * Set flag: Enable Sub-Controllers
	 * 
	 * @access public
	 * @return bool $flag
	 * @return void
	 */
	public function setEnableSubcontrollers($flag) {
		$this->_enableSubcontrollers = (bool) $flag;
	}
	
	/**
	 * Set Routing Rules
	 * 
	 * This method can be used to set the Routing Rules. The Routing
	 * Rules define how specific URI's are rewritten. Typically, this 
	 * is used to map a collection of URI's to a collection of rewritten
	 * URI's.
	 * 
	 * Let's assume we want to change the way the following request is 
	 * routed to its corresponding controller:
	 * 
	 * <code>http://www.multimedium.be/info/contact</code>
	 * 
	 * Normally, this request would be routed to the controller Info,
	 * and the method contact() would be called in that controller.
	 * However, we want the request to be routed to the controller Info
	 * (no changes there), and the method detail() to be called. In 
	 * that case, we would have to add a Routing Rule:
	 * 
	 * <code>
	 *    URI Pattern      | Rewritten URI
	 *    -----------------|----------------------
	 *    info/(:string)   | info/detail/$1
	 * </code>
	 * 
	 * By adding this Routing Rule, the URI in our example will be
	 * rewritten internally to the following:
	 * 
	 * <code>http://www.multimedium.be/info/detail/contact</code>
	 * 
	 * Note that this method expects an (associative) array. In order
	 * to specify the rules of our example to the controller dispatcher,
	 * we would do the following:
	 * 
	 * <code>
	 *    $dispatcher = M_ControllerDispatcher::getInstance(M_Request::getUri());
	 *    $dispatcher->setRoutingRules(array(
	 *       'info/(:string)' => 'info/detail/$1'
	 *    ));
	 * </code>
	 * 
	 * As you can see, the keys of the associative array are the URI
	 * patterns that need to be rewritten, and the corresponding values
	 * are the rewritten URI's.
	 * 
	 * Patterns should be provided in PCRE regex syntax, as used by
	 * PHP's built-in functions such as preg_match(), preg_replace(),
	 * etc... However, {@link M_ControllerDispatcher} adds some special
	 * syntaxes to the mix, for ease of use:
	 * 
	 * <code>
	 *    Added           | Translates to
	 *    ----------------|-----------------------------
	 *    (:string)       | ([a-z0-9\.\-\+_]+)
	 *    (:num)          | ([0-9]+)
	 *    (:any)          | (.*)
	 * </code>
	 * 
	 * @access public
	 * @param array $rules
	 * 		The Routing Rules; or URI Rewriting Rules
	 * @return void
	 */
	public function setRoutingRules(array $rules) {
		$this->_rules = $rules;
	}
	
	/**
	 * Dispatch!
	 * 
	 * Uses {@link M_ControllerDispatcher::getControllerCallback()} to
	 * identify the {@link M_Controller} object. Once the controller
	 * has been identified, it will dispatch (or delegate) the request
	 * to that controller.
	 * 
	 * NOTE:
	 * This method will run the controller's actions inside a try{}
	 * block. If an exception is thrown, it will catch that exception,
	 * and pass it into the {@link M_Controller::handleError()} method
	 * of the controller.
	 * 
	 * @access public
	 * @return void
	 */
	public function dispatch() {
		// We try:
		try {
			// start output buffering. We don't want to output anything
			// to the browser window until we're absolutely sure that the 
			// entire page request has been handled correctly.
			ob_start(array(M_ControllerDispatcherCsrf::getInstance(), 'getHtmlProtected'));
			
			// Get the controller callback in charge of handling the request:
			list($callback, $args) = $this->getControllerCallback();
			
			// We try:
			try {
				// Set the persistent memory in the CSRF Protection, for the 
				// next page request:
				M_ControllerDispatcherCsrf::getInstance()->setPersistentMemoryForNextPageRequest();
				
				// Run the controller's actions:
				call_user_func_array($callback, $args);
				
				// Flush, and clear, the output buffer
				ob_end_flush();
			}
			// If we fail:
			catch(Exception $e) {
				// Clear the output buffer (don't output)
				ob_end_clean();
				
				// Call the controller's handleError() method with the
				// exception:
				$callback[0]->handleError($e);
			}
		}
		// If we could not construct the Controller Callback
		// (caused LiteSpeed server to show the "Server Busy" error?)
		catch(Exception $e) {
			// Get the error controller:
			list($callback, $args) = $this->getErrorControllerCallback();
			
			// We'll call "handleError" instead:
			$callback[1] = 'handleError';
			
			// Set the exception as argument:
			$args = array($e);
			
			// Run the controller's actions:
			call_user_func_array($callback, $args);
		}
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get rewritten path
	 * 
	 * Will rewrite a path, given a set of routing rules. This method is used by
	 * 
	 * - {@link M_ControllerDispatcher::setRoutingRules()}
	 * - {@link MI_Controller::__router()}
	 * 
	 * @access private
	 * @param string $path
	 * 		The path to be rewritten
	 * @param array $rules
	 * 		The routing rules; @see M_ControllerDispatcher::setRoutingRules()
	 * @param integer $pathIndex
	 *		This number provides with the index at which the path has been
	 *		extracted. For example, if the path is being rewritten by the __router()
	 *		method of a controller, this index will be 1 instead of 0.
	 * @return string
	 */
	protected function _getRewrittenPath($path, array $rules, $pathIndex = 0) {
		// Clean up the path:
		$path = M_Helper::ltrimCharlist((string) $path, "/");
		
		// For each of the routing rules:
		foreach($rules as $rule => $rewritten) {
			// If locale-sensitive path elements are enabled in the application
			if(M_Request::isLocalePathElementsEnabled()) {
				// For each of the path elements in the current routing rule:
				foreach(preg_split('/(\(:[^)]+\)\/?\??|\/\??)/i', $rule, NULL) as $i => $element) {
					// If the current element is not empty:
					if(! empty($element)) {
						// Get the index of the element:
						$index = $pathIndex + $i;
						
						// Then, we check if the element is to be translated by 
						// the application. If that is the case:
						if(M_Request::isLocalePathElement($element, $index)) {
							// Then, we get the translated path element and we
							// insert the translation into the routing rule:
							$rule = str_replace($element, M_Request::getTranslatedPathElement($element, $index), $rule);
						}
					}
				}
			}
			
			// Parse a regular expression out of the rewrite rule:
			$pattern = M_ControllerDispatcherPathRewriter::getRegularExpressionFromRule($rule);
			
			// Do we have a match?
			if(preg_match($pattern, $path) == 1) {
				// Rewrite and return!
				return M_Helper::ltrimCharlist(preg_replace($pattern, $rewritten, $path), "/");
			}
		}
		
		// If we're still here, it means that the path did not get rewritten:
		return $path;
	}
	
	/**
	 * Get controller object, with array of path elements
	 * 
	 * This method is used internally, in order to look up the controller that
	 * corresponds to an array of path elements. This array is typically extracted
	 * from the path in the URI with which the controller has been constructed.
	 * 
	 * Also, this method will check whether or not the corresponding controller
	 * is implemented correctly, and that it is instantiable. If for any reason
	 * a controller cannot be constructed for the given collection of path elements,
	 * this method will return NULL instead.
	 * 
	 * This method will also leave affected the incoming array. Each path element
	 * that is used to compose the name of the controller, will be removed from 
	 * the array, leaving you with an array of fewer elements.
	 * 
	 * @access protected
	 * @param array $pathElements
	 *		The array of path elements (passed as a reference)
	 * @return MI_Controller $controller
	 *		The controller object, that corresponds to the collection of path 
	 *		elements
	 */
	protected function _getControllerObjectFromPathElements(array & $pathElements) {
		// If no path elements are present in the provided array
		if(count($pathElements) == 0) {
			// Then, we return NULL:
			return NULL;
		}
		
		// Get the namespace that has been assigned to this request (by host)
		$ns = $this->_getControllerNamespaceByHost();
		
		// A queue of controller class names:
		$queue = array();
		
		// Working variables:
		$tmp   = '';
		
		// For each of the path elements:
		foreach($pathElements as $element) {
			// Convert the current element to a controller class name, and add
			// it to the queue of class names:
			$tmp = $tmp . ' ' . $element;
			array_push($queue, self::getClassNameOutOfPathElement($tmp));
			
			// If subcontrollers are not enabled:
			if(! $this->getEnableSubcontrollers()) {
				// Then, stop adding subcontrollers to the queue:
				break;
			}
		}
		
		// If a namespace applies to this controller:
		if($ns) {
			// Then, we'll prepend the controller namespace to the names of each
			// class name in the queue:
			foreach($queue as $i => $name) {
				$queue[$i] = $ns . $name;
			}
		}
		
		// We prepare a variable, in which we'll store the name of the controller:
		$controller = NULL;
		$moduleId   = $pathElements[0];
		
		// For each of the controller class names in the queue:
		foreach($queue as $i => $name) {
			// We compose the absolute path to the class file:
			$classPath = M_Loader::getAbsolute(
				// Path to the module, with
				M_Loader::getModulePath($moduleId) . '/'. 
				// The controllers' folder
				M_Loader::getControllersFolder() . '/' . 
				// The class file basename:
				$name . '.class.php'
			);
			
			// If we cannot find the controller class file
			if(! is_file($classPath)) {
				// And the current class name is the main controller:
				if($i == 0) {
					// Then, we cannot construct the controller. The main controller
					// MUST be present in the application:
					return NULL;
				}
				
				// If the current class name is not for the main controller, then
				// we return the controller that has been constructed in the previous
				// loop (may be the main controller)
				return $controller;
			}
			// If the controller file has been found:
			else {
				// Then, we remove the corresponding element from the array of
				// path elements. Since it has been used to compose the controller
				// class name, it is no longer to be taken into account...
				array_shift($pathElements);
				
				// We load the controller file:
				require_once $classPath;
				
				// We make sure that the controller implements the MI_Controller 
				// interface:
				if(! M_Helper::isInterface($name, 'MI_Controller')) {
					// If not implementing the MI_Controller interface, we 
					// return NULL instead;
					return NULL;
				}

				// We also make sure that we can create an instance of the class:
				$class = new ReflectionClass($name);
				
				// If not the case
				if(! $class->isInstantiable()) {
					// We return NULL instead:
					return NULL;
				}
				
				// We store the constructed controller, for output
				$previousController = get_class($controller);
				$controller = new $name();
				
				// If this is not the first controller encountered:
				if($i > 0) {
					// Then, this controller must be an instance of the controller
					// encountered previously. If that is not the case
					if(! M_Helper::isInstanceOf($controller, $previousController)) {
						// Then, we return NULL
						return NULL;
					}
				}
			}
		}
		
		// Return the controller:
		return $controller;
	}
	
	/**
	 * Get controller namespace
	 * 
	 * This method will return the namespace that has been configured for the 
	 * controllers, by host.
	 * 
	 * @access protected
	 * @return string $ns
	 *		The namespace for controllers' class names, or NULL if not applicable
	 */
	protected function _getControllerNamespaceByHost() {
		// Get the host that is being taken into account in order to dispatch
		// the current request:
		$host = $this->getHost();
		
		// If a namespace has been created for the host:
		if(isset($this->_namespace[$host])) {
			// Then, we return that namespace:
			return $this->_namespace[$host];
		}
		
		// If we're still here, then we return NULL
		return NULL;
	}
	
	/**
	 * Get return value for getPath()
	 * 
	 * This method is used internally, by {@link M_ControllerDispatcher::getPath()},
	 * in order to return the path extracted. This method will also set the value
	 * of the property {@link M_ControllerDispatcher::$_path}, and dispatch an
	 * event that notifies about the receival of the path.
	 * 
	 * This function will set the value of {@link M_ControllerDispatcher::$_path},
	 * so the {@link M_ControllerDispatcher::getPath()} function does not trigger
	 * a call to this function again.
	 * 
	 * @access protected
	 * @param string $path
	 *		The path that is to be returned (may also be NULL)
	 * @return mixed
	 */
	protected function _getPathReturnValue($path) {
		// Set the internal property
		$this->_path = $path;
		
		// We trigger an event, to notify about the fact that we have
		// received the full path:
		$this->dispatchEvent(
			new M_ControllerDispatcherEvent(
				M_ControllerDispatcherEvent::RECEIVE_PATH,
				$this
			)
		);
		
		// Return the path:
		// (note that we do not use the $path variable, because event listeners
		//  may have chosen to alter the path in the dispatcher)
		return $this->_path;
	}
}