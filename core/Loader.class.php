<?php
/**
 * @file M_Loader class
 * @package Core
 */

/**
 * Root folder
 *
 * This constant holds the root folder of the installed system.
 * This constant, to the public API, is an alias for the
 * following alternative methods of obtaining the root folder:
 * 
 * - {@link M_Loader::ROOT}
 * - {@link M_Loader::getRootFolder()}
 */
define('FOLDER_ROOT', dirname(dirname(__FILE__)));

/**
 * Application constants
 * 
 * The following constants are used in the active application, you can choose
 * your own application folder name and/or other folder names.
 * 
 * @internal maybe we should move this part to an "application-config" file?
 *
 */
define('FOLDER_APPLICATION_DEFAULT', 'application');
define('FOLDER_MODELS', 'models');
define('FOLDER_CONTROLLERS', 'controllers');
define('FOLDER_VIEWS', 'views');
define('FOLDER_RESOURCES', 'resources');
define('FOLDER_DEFINITIONS', 'definitions');
define('FOLDER_MODULES', 'modules');
define('FOLDER_FORMS', 'forms');
define('FOLDER_MODULE_CORE', 'core');

//used in core
define('FOLDER_THIRDPARTY', '_thirdparty');

/**
 * M_Loader class
 *
 * The M_Loader class is used to work with paths to directories and
 * files. Also, it is used to load core classes and functions.
 * 
 * @package Core
 */
class M_Loader extends M_Object {
	/**
	 * Root folder
	 * 
	 * This constant holds the root folder of the installed system.
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the root folder:
	 * 
	 * - {@link FOLDER_ROOT}
	 * - {@link M_Loader::getRootFolder()}
	 */
	const ROOT = FOLDER_ROOT;
	
	/**
	 * Default application folder
	 * 
	 * This constant holds the folder of the default active application.
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the application folder:
	 * 
	 * - {@link FOLDER_APPLICATION_DEFAULT}
	 * - {@link M_Loader::getDefaultApplicationFolder()}
	 *
	 */
	const APPLICATION_DEFAULT = FOLDER_APPLICATION_DEFAULT;
	
	/**
	 * Models folder
	 * 
	 * This constant holds the folder of the models used throughout the 
	 * application.
	 * 
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the models folder:
	 * 
	 * - {@link FOLDER_MODELS}
	 * - {@link M_Loader::getModelsFolder()}
	 *
	 */
	const MODELS = FOLDER_MODELS;
	
	/**
	 * Views folder
	 * 
	 * This constant holds the folder of the views used throughout the 
	 * application.
	 * 
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the views folder:
	 * 
	 * - {@link FOLDER_VIEWS}
	 * - {@link M_Loader::getViewsFolder()}
	 *
	 */
	const VIEWS = FOLDER_VIEWS;
	
	/**
	 * Controllers folder
	 * 
	 * This constant holds the folder of the controllers used throughout the 
	 * application.
	 * 
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the controllers folder:
	 * 
	 * - {@link FOLDER_CONTROLLERS}
	 * - {@link M_Loader::getControllersFolder()}
	 *
	 */
	const CONTROLLERS = FOLDER_CONTROLLERS;
	
	/**
	 * Resources folder
	 * 
	 * This constant holds the folder of the resources used throughout the 
	 * application.
	 * 
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the resources folder:
	 * 
	 * - {@link FOLDER_RESOURCES}
	 * - {@link M_Loader::getResourcesFolder()}
	 *
	 */
	const RESOURCES = FOLDER_RESOURCES;
	
	/**
	 * Definitions folder
	 * 
	 * This method returns the model-definition folder used throughout the
	 * active application. The definition folder typiccally exists in the
	 * resources folder and holds the different xml files which define the model
	 * files
	 * 
	 * - {@link FOLDER_DEFINITIONS}
	 * - {@link M_Loader::getDefinitionsFolder()}
	 *
	 */
	const DEFINITIONS = FOLDER_DEFINITIONS;
	
	/**
	 * Forms folder
	 * 
	 * This constant holds the folder name of the form classes that are 
	 * being used throughout the application.
	 * 
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the forms' folder:
	 * 
	 * - {@link FOLDER_FORM}
	 * - {@link M_Loader::getFormsFolder()}
	 *
	 */
	const FORMS = FOLDER_FORMS;
	
	/**
	 * Module Core folder
	 * 
	 * This constant holds the folder name of the module core classes.
	 * 
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the forms' folder:
	 * 
	 * - {@link FOLDER_MODULE_CORE}
	 * - {@link M_Loader::getModuleCoreFolder()}
	 *
	 */
	const MODULE_CORE = FOLDER_MODULE_CORE;
	
	/**
	 * Modules folder
	 * 
	 * This constant holds the folder name where modules are located.
	 * 
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the resources folder:
	 * 
	 * - {@link FOLDER_MODULES}
	 * - {@link M_Loader::getModulesFolder()}
	 *
	 */
	const MODULES = FOLDER_MODULES;
	
	/**
	 * Class paths
	 * 
	 * This property holds the class paths that have been registered
	 * with {@link M_Loader::addClassPath()}.
	 * 
	 * @access private
	 * @static
	 * @var array
	 */
	private static $_classPath = array();

	/**
	 * Store application folder
	 * 
	 * @param 	str
	 * @return 	void
	 * @access 	public
	 */
	public static function setApplicationFolder($_applicationFolder)
	{ 
		$register = M_Registry::getInstance();
		if ($register->valid('application') == false)
		{
			$register->register('application', $_applicationFolder);
		}
		else throw new Exception('Application folder can only be set once');
	}
	
	/**
	 * Set the application folder by host name
	 *
	 */
	public static function setApplicationFolderByHost()
	{
		self::setApplicationFolder(M_Request::getHost());
	}
	
	/**
	 * Retrieve application folder
	 * 
	 * @param 	void
	 * @return	str
	 * @access 	public
	 */
	public static function getApplicationFolder()
	{ 
		$register = M_Registry::getInstance();
		$application = $register->get('application');
		//if no application folder is set: use default
		if (is_null($application))
		{
			return self::getDefaultApplicationFolder();
		}
		
		return $application; 
	}
	
	
	/**
	 * Get Root folder
	 * 
	 * This method returns the root folder of the installed system.
	 * This method, to the public API, is an alternative to the use
	 * of the following constants for obtaining the root folder:
	 * 
	 * - {@link FOLDER_ROOT}
	 * - {@link M_Loader::ROOT}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getRootFolder() {
		return self::ROOT;
	}
	
	/**
	 * Get Default application folder
	 * 
	 * This method returns the default application folder.
	 * This method, to the public API, is an alternative to the use
	 * of the following constants for obtaining the default application folder:
	 * 
	 * - {@link FOLDER_APPLICATION_DEFAULT}
	 * - {@link M_Loader::APPLICATION_DEFAULT}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getDefaultApplicationFolder() {
		return self::APPLICATION_DEFAULT;
	}
	
	/**
	 * Get Modules folder
	 * 
	 * This method returns the modules folder.
	 * 
	 * - {@link FOLDER_MODULES}
	 * - {@link M_Loader::MODULES}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getModulesFolder() {
		return self::MODULES;
	}
	
	/**
	 * Get Models folder
	 * 
	 * This method returns the models folder used throughout the active 
	 * application.
	 * This method, to the public API, is an alternative to the use
	 * of the following constants for obtaining the models folder:
	 * 
	 * - {@link FOLDER_MODELS}
	 * - {@link M_Loader::MODELS}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getModelsFolder() {
		return self::MODELS;
	}
	
	/**
	 * Get Views folder
	 * 
	 * This method returns the views folder used throughout the active 
	 * application.
	 * This method, to the public API, is an alternative to the use
	 * of the following constants for obtaining the views folder:
	 * 
	 * - {@link FOLDER_VIEWS}
	 * - {@link M_Loader::VIEWS}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getViewsFolder() {
		return self::VIEWS;
	}
	
	/**
	 * Get Controllers folder
	 * 
	 * This method returns the controllers folder used throughout the active 
	 * application.
	 * This method, to the public API, is an alternative to the use
	 * of the following constants for obtaining the controllers folder:
	 * 
	 * - {@link FOLDER_CONTROLLERS}
	 * - {@link M_Loader::CONTROLLERS}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getControllersFolder() {
		return self::CONTROLLERS;
	}
	
	/**
	 * Get Resources folder
	 * 
	 * This method returns the resources folder used throughout the active 
	 * application.
	 * 
	 * This method, to the public API, is an alternative to the use
	 * of the following constants for obtaining the resources folder:
	 * 
	 * - {@link FOLDER_RESOURCES}
	 * - {@link M_Loader::RESOURCES}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getResourcesFolder() {
		return self::RESOURCES;
	}
	
	/**
	 * Get Definition folder
	 * 
	 * This method returns the model-definition folder used throughout the
	 * active application. The definition folder typiccally exists in the
	 * resources folder and holds the different xml files which define the model
	 * files
	 * 
	 * - {@link FOLDER_DEFINITION}
	 * - {@link M_Loader::DEFINITION}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getDefinitionsFolder() {
		return self::DEFINITIONS;
	}
	
	/**
	 * Get Forms folder
	 * 
	 * This method returns the forms folder used throughout the active 
	 * application.
	 * 
	 * This method, to the public API, is an alternative to the use
	 * of the following constants for obtaining the forms folder:
	 * 
	 * - {@link FOLDER_FORM}
	 * - {@link M_Loader::FORMS}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getFormsFolder() {
		return self::FORMS;
	}
	
	/**
	 * Get Module's Core folder
	 * 
	 * This method returns the core folder used in modules.
	 * 
	 * This method, to the public API, is an alternative to the use
	 * of the following constants for obtaining the forms folder:
	 * 
	 * - {@link FOLDER_MODULE_CORE}
	 * - {@link M_Loader::MODULE_CORE}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getModuleCoreFolder() {
		return self::MODULE_CORE;
	}
	
	/**
	 * Convert relative path to absolute one
	 * 
	 * This method will accept a relative path to a directory or
	 * file, interprete it as "starting from the root folder", and 
	 * convert it to the corresponding absolute path.
	 * 
	 * @access public
	 * @param string $path
	 * 		The relative path
	 * @return string
	 * 		The absolute path
	 */
	public static function getAbsolute($path) {
		return self::ROOT . DIRECTORY_SEPARATOR . trim($path, " \t\n\r\0\x0B/\\");
	}
	
	/**
	 * Convert absolute path to relative one
	 * 
	 * This method will accept an absolute path to a directory or
	 * file (probably obtained by {@link M_Loader::getAbsolute()}), 
	 * and convert it to a relative path. To do so, it will strip the
	 * root folder ({@link M_Loader::getRootFolder()} from the path.
	 * 
	 * @access public
	 * @param string $path
	 * 		The relative path
	 * @return string
	 * 		The absolute path
	 */
	public static function getRelative($path) {
		$len = strlen(self::ROOT);
		if(!strncmp($path, self::ROOT, $len)) {
			return trim(substr($path, $len), " \t\n\r\0\x0B/\\");
		} else {
			return $path;
		}
	}
	
	/**
	 * Load Interface
	 * 
	 * Allows for loading an interface that is present in one of the modules
	 * of the application. There are a few rules that apply to this matter:
	 * 
	 * - a module interface should always be placed in the module's core folder
	 * - a module interface's filename is always "_MyClass.interface.php"
	 * - a module interface's classname is always "I_MyClass"
	 * 
	 * For example, imagine we have a "service" module, which contains an
	 * interface all Service related classes must implement. In this case
	 * the interface can be found at the following path:
	 * 
	 * <code>
	 *		"application/modules/service/core/_Service.interface.php"
	 * </code>
	 * 
	 * Additionally, the interface class itself is defined as follows:
	 * 
	 * <code>
	 *		<?php
	 *		interface I_Service {}
	 * </code>
	 * 
	 * If we want to load the class defined above, we use the following code:
	 * 
	 * <code>
	 *		M_Loader::loadInterface('I_Service', 'service');
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param string $interface
	 *		The interface's class name. Example: "I_MyClass"
	 * @param string $module
	 *		The interface's module
	 * @throws M_Exception
	 */
	public static function loadInterface($interface, $module) {
		// Make sure the provided interface has the correct prefix
		if(substr($interface, 0, 2) != 'I_') {
			throw new M_Exception(sprintf(
				'Cannot load interface %s; All interfaces should start with ' . 
				'the I_ prefix!',
				$interface
			));
		}
		
		// Has the path to the interface been requested before?
		$path = self::getClassPath($interface);
		
		// If not the case
		if(! $path) {
			// Parse the path to the file
			$path = self::getAbsolute(
				self::getModulePath($module) . '/' . 
				self::getModuleCoreFolder() . '/' .
				substr($interface, 1) . '.interface.php'
			);
			
			// If it is not a file:
			if(! is_file($path)) {
				throw new M_Exception(sprintf(
					'Cannot load interface %s; The class could not be found at %s',
					$interface,
					$path
				));
			}
		}
		
		// Include the file once
		require_once $path;
	}
	
	/**
	 * Load core interface
	 * 
	 * This method will load a core interface into the system.
	 * 
	 * @access public
	 * @param string $class
	 * 		The class name
	 * @return void
	 */
	public static function loadCoreInterface($interface) {
		if(! strncmp($interface, 'MI_', 3)) {
			$interface = substr($interface, 3);
		}
		$path = self::getClassPath('MI_' . $interface);
		if(! $path) {
			$path = self::getAbsolute('core/_' . $interface . '.interface.php');
			if(! is_file($path)) {
				$temp = M_Helper::explodeByCamelCasing($interface);
				$path = self::getAbsolute('core/' . $temp[0] . '/_' . $interface . '.interface.php');
				if(! is_file($path)) {
					throw new M_Exception(sprintf('Cannot find interface %s at path %s', 'M_' . 'MI_' . $interface, $path));
				}
			}
		} else {
			$path = self::getAbsolute(rtrim($path, '/') . '/_' . $interface . '.interface.php');
			if(! is_file($path)) {
				throw new M_Exception(sprintf('Cannot find interface %s at path %s', 'MI_' . $interface, $path));
			}
		}
		require_once $path;
	}
	
	/**
	 * Load core class
	 * 
	 * This method will load a core class into the system. A Core class
	 * is loaded from the core folder, unless it has been registered
	 * with another path, by {@link M_Loader::addClassPath()}.
	 * 
	 * NOTE:
	 * Typically, this method will be used to autoload classes, in the
	 * __autoload handler.
	 * 
	 * @access public
	 * @throws M_Exception
	 * @param string $class
	 * 		The class name
	 * @return void
	 */
	public static function loadCoreClass($class) {
		if(! strncmp($class, 'M_', 2)) {
			$class = substr($class, 2);
		}
		$path = self::getClassPath('M_' . $class);
		if(! $path) {
			$path = self::getAbsolute('core/' . $class . '.class.php');
			if(! is_file($path)) {
				$temp = M_Helper::explodeByCamelCasing($class);
				$path = self::getAbsolute('core/' . $temp[0] . '/' . $class . '.class.php');
				if(! is_file($path)) {
					throw new M_Exception(sprintf('Cannot find class %s at path %s', 'M_' . $class, $path));
				}
			}
		} else {
			$path = self::getAbsolute(rtrim($path, '/') . '/' . $class . '.class.php');
			if(! is_file($path)) {
				throw new M_Exception(sprintf('Cannot find class %s at path %s', 'M_' . $class, $path));
			}
		}
		require_once $path;
	}
	
	/**
	 * Get path for a module
	 * 
	 * Based on the module name, this method will return the full path
	 * for this specific module.
	 * 
	 * This method can also be used to get the "root" module folder, in normal
	 * circumstances this would always be the application-folder. This can be
	 * done by passing an empty $module parameter
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getModulePath($module = null) {
		$path = self::getApplicationFolder();

		if ($module)
		{
			$path .= '/' . self::getModulesFolder(). '/'. M_Helper::lcfirst($module);
		}
		
		return $path;
	}
	
	/**
	 * Load data object (for a specific module)
	 * 
	 * This method will try to load a class for the given object, which possibly
	 * is part of a module. Based on (the modulename and) classname the loader
	 * will be able to determine the path and object filename
	 * 
	 * This method is an alias for 
	 * 
	 * - {@link M_Loader::loadModel}
	 *
	 * @param str $className
	 * @param str $module
	 * @return void
	 */
	public static function loadDataObject( $className, $module = null) {
		return self::loadModel($className,$module);
	}
	
	/**
	 * Load data object mapper for a (specific module and) className
	 * 
	 * This method will try to load a mapper class for the given object, which
	 * possibly is part of a module. Based on the (modulename and) classname of 
	 * the object the loader will be able to determine the path and data-object 
	 * mapper filename
	 * 
	 * @param str $className
	 * @param str $module
	 * @return void
	 */
	public static function loadDataObjectMapper($className, $module = null) {
		$objectFile = ucfirst($className) 
			. M_DataObjectMapper::MAPPER_SUFFIX 
			. ".class.php";
		
		//try to load the model
		if($module) {
			self::loadRelative(
				self::getModulePath($module) . '/'. 
				self::getModelsFolder() . '/' . 
				$objectFile
			);
		} else {
			self::loadRelative(
				self::getApplicationFolder() . '/'. 
				self::getModelsFolder() . '/' . 
				$objectFile 
			);
		}
	}
	
	/**
	 * Load model (for a specific module)
	 * 
	 * This method will try to load a model class (for a given module).
	 * Based on the (modulename and) classname of the model the loader will be 
	 * able to determine the path and model filename
	 * 
	 * If module name is left null, it will try first to load a core module,
	 * if this fails it will try again but this time it will try to load
	 * the model from the module which has the same name as the model.
	 * 
	 * @param str $model
	 * @param str $module
	 * @return void
	 */
	public static function loadModel( $model, $module = null ) {
		$objectFile = ucfirst($model) . ".class.php";
		
		//try to load the model
		if($module) {
			self::loadRelative(
				self::getModulePath($module) . '/'. 
				self::getModelsFolder() . '/' . 
				$objectFile
			);
		} else {
			self::loadRelative(
				self::getApplicationFolder() . '/'. 
				self::getModelsFolder() . '/' . 
				$objectFile 
			);
		}
	}
	
	/**
	 * Load controller (for a specific module)
	 * 
	 * This method will try to load a controller class (for the given module).
	 * Based on the (modulename and) classname of the controller the loader will 
	 * be able to determine the path and controller filename
	 * 
	 * @param str $controller
	 * @param str $module
	 * @return void
	 */
	public static function loadController( $controller, $module = null ) {
		$objectFile = ucfirst($controller) . ".class.php";
		
		return self::loadRelative(
			self::getModulePath($module) . '/'. 
			self::getControllersFolder() . '/' . 
			$objectFile 
		);	
	}
	
	/**
	 * Load view (for a specific module)
	 * 
	 * This method will try to load a view class (for the given module).
	 * Based on the (modulename and) classname of the view the loader will be 
	 * able to determine the path and controller filename
	 * 
	 * @param str $controller
	 * @param str $module
	 * @return void
	 */
	public static function loadView( $view, $module = null ) {
		$objectFile = ucfirst($view) . ".class.php";
		
		return self::loadRelative(
			self::getModulePath($module) . '/'. 
			self::getViewsFolder() . '/' . 
			$objectFile 
		);	
	}
	
	/**
	 * Load form (for a specific module)
	 * 
	 * This method will try to load a form class (for the given module).
	 * Based on the (modulename and) classname of the form, the loader 
	 * will be  able to determine the path and form filename
	 * 
	 * @param str $form
	 * @param str $module
	 * @return void
	 */
	public static function loadForm($form, $module = null) {
		$objectFile = ucfirst($form) . ".class.php";
		return self::loadRelative(
			self::getModulePath($module) . '/'. 
			self::getFormsFolder() . '/' . 
			$objectFile 
		);
	}
	
	/**
	 * Load a module's core class
	 * 
	 * @static
	 * @access public
	 * @param string $class
	 * @param string $module
	 * @return void
	 */
	public static function loadModuleCoreClass($class, $module) {
		return self::loadRelative(
			self::getModulePath($module) . '/'. 
			self::getModuleCoreFolder() . '/' . 
			ucfirst($class) . ".class.php" 
		);
	}
	
	/**
	 * Return the file path for a resource file
	 * 
	 * To use a file located in a resource folder you can use this method: it
	 * will automatically generate a path to the resource-folder
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getResourcesPath($module = null) {
		return self::getRelative(
			self::getModulePath($module) . '/' . 
			self::getResourcesFolder() 
		);
	}
	
	/**
	 * Return the uri for resources
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getResourcesHref($module = null) {
		$uri = M_Request::getHostAddress();
		$uri .= M_Server::getBaseHref();
		$uri .= self::getApplicationFolder() . '/';
		if ($module) $uri .= 'modules/' . M_Helper::lcfirst($module) . '/';
		$uri .= self::getResourcesFolder();
		
		return $uri;
	}
	
	/**
	 * Load a file
	 * 
	 * This method will load a file, specified by its relative 
	 * path. The provided path will be passed through the 
	 * {@link M_Loader::getAbsolute()} method, in order for the
	 * loader to correctly load the file.
	 * 
	 * @access public
	 * @param string $path
	 * 		The relative path
	 * @param boolean $once
	 * 		Load only once, or load the requested file regardless
	 * 		of whether or not it already has been loaded. If set
	 * 		to TRUE (default value), this method will load the 
	 * 		file only once.
	 * @return void
	 */
	public static function loadRelative($path, $once = TRUE) {
		$abs = self::getAbsolute($path);
		if(is_file($abs)) {
			if($once) {
				require_once $abs;
			} else {
				require $abs;
			}
		} else {
			throw new M_Exception(sprintf('Cannot find file %s', $path));
		}
	}
	
	/**
	 * Load a file with an absolute path
	 *
	 * This method will load a file, specified by its absolute path.
	 * 
	 * @param str $path
	 * @param bool $once
	 */
	public static function loadAbsolute($path, $once = TRUE) {
		if (is_file($path)) {
			if($once) {
				require_once $path;
			} else {
				require $path;
			}
		} else {
			throw new M_Exception(sprintf('Cannot find file %s', $path));
		}
	}
	
	/**
	 * Add class path
	 * 
	 * This method will register a class path. By registering the path
	 * to a given class, you can later load the class with the method
	 * {@link M_Loader::loadCoreClass()}, even if the given class is
	 * not located in the core's root folder.
	 * 
	 * Example 1:
	 * <code>
	 *    M_Loader::setAsAutoLoader();
	 *    M_Loader::addClassPath('core/Fs', 'M_File');
	 * </code>
	 * 
	 * Example 1 shows how the class M_File will be loaded from the folder core/Fs,
	 * whenever required in the application. Note however that you might want to 
	 * map a group of classes to a given class path. You would go about doing so
	 * as following:
	 * 
	 * Example 2
	 * <code>
	 *    M_Loader::setAsAutoLoader();
	 *    M_Loader::addClassPath('core/Fs', 'M_File', 'M_Directory', 'M_Ftp*');
	 * </code>
	 * 
	 * Example 2 shows how the class M_File and M_Directory will be loaded from 
	 * the folder core/Fs. Also, any class that starts with M_Ftp (for example
	 * {@link M_Ftp}, {@link M_FtpItem}, etc) will be loaded from the same
	 * core/Fs folder.
	 * 
	 * @access public
	 * @param string $path
	 * 		The class path
	 * @param ...
	 * 		A variable number of class names (or patterns) that are located at
	 * 		the given class path
	 * @return void
	 */
	public static function addClassPath($path) {
		$args = func_get_args();
		$argn = count($args);
		if($argn < 2) {
			throw new M_Exception('Cannot add class path; no classes/interfaces provided');
		}
		for($i = 1; $i < $argn; $i ++) {
			$args[$i] = trim($args[$i]);
			$hasPrefix = (!strncmp($args[$i], 'MI_', 3) || !strncmp($args[$i], 'M_', 2));
			if(! $hasPrefix) {
				throw new M_Exception('Cannot add class path: class/interface is missing core prefix');
			}
			if(!array_key_exists($args[$i], self::$_classPath)) {
				self::$_classPath[$args[$i]] = $path;
			} else {
				throw new Exception(sprintf(
					'Class path already registered for %s: %s', 
					$args[$i], $path
				));
			}
		}
	}
	
	/**
	 * Look up class path
	 *
	 * Will look up the class path of a given class/interface, which may have been 
	 * defined earlier with {@link M_Loader::addClassPath()}.
	 * 
	 * @access public
	 * @param string $class
	 * @return string|null
	 */
	public static function getClassPath($class) {
		$class = trim($class);
		if(isset(self::$_classPath[$class])) {
			return self::$_classPath[$class];
		}
		foreach(self::$_classPath as $name => $path) {
			if(substr($name, -1) == '*') {
				if(substr($name, 0, -1) == substr($class, 0, strlen($name) - 1)) {
					return $path;
				}
			}
		}
		return NULL;
	}
	
	/**
	 * Add include path
	 * 
	 * @param string
	 * @return void
	 */
	public static function addIncludePath($path) {
		foreach (func_get_args() AS $path)
	    {
	        if (!file_exists($path) OR (file_exists($path) && filetype($path) !== 'dir'))
	        {
	            trigger_error("Include path '{$path}' not exists", E_USER_WARNING);
	            continue;
	        }
	       
	        $paths = explode(PATH_SEPARATOR, get_include_path());
	       
	        if (array_search($path, $paths) === false)
	            array_push($paths, $path);
	       
	        set_include_path(implode(PATH_SEPARATOR, $paths));
	    }
	}
	
	/**
	 * Get a (constructed) Data Object Mapper
	 * 
	 * @access public
	 * @param string $dataObjectClassName
	 * 		The data object class name
	 * @param string $moduleId
	 * 		The module from which to load the mapper
	 * @return M_DataObjectMapper
	 */
	public static function getDataObjectMapper($dataObjectClassName, $moduleId = NULL) {
		// Load the mapper:
		self::loadDataObjectMapper(
			$dataObjectClassName, 
			$moduleId
		);
		
		// Construct the mapper, and return as result:
		$dataObjectClassName .= M_DataObjectMapper::MAPPER_SUFFIX;
		return new $dataObjectClassName;
	}
	
	/**
	 * Get a (constructed) view
	 * 
	 * @access public
	 * @param string $viewClassName
	 * 		The class name of the view
	 * @param string $moduleId
	 * 		The module from which to load the view
	 * @return M_View
	 */
	public static function getView($viewClassName, $moduleId = NULL) {
		// Load the view:
		self::loadView($viewClassName, $moduleId);
		
		// Construct the view, and return as result:
		return new $viewClassName;
	}
	
	/**
	 * Get a (constructed) data object, or model
	 * 
	 * @access public
	 * @param string $modelClassName
	 * 		The class name of the model
	 * @param string $moduleId
	 * 		The module from which to load the model
	 * @return mixed
	 */
	public static function getModel($modelClassName, $moduleId = NULL) {
		// Load the model:
		self::loadModel($modelClassName, $moduleId);
		
		// Construct the model, and return as result:
		return new $modelClassName;
	}
	
	/**
	 * Get a (constructed) form
	 * 
	 * @access public
	 * @param string $formClassName
	 * 		The class name of the form
	 * @param string $moduleId
	 * 		The module from which to load the form
	 * @return M_Form
	 */
	public static function getForm($formClassName, $moduleId = NULL) {
		// Load the form:
		self::loadForm($formClassName, $moduleId);
		
		// Construct the form, and return as result:
		return new $formClassName;
	}
	
	/**
	 * Get a (constructed) controller
	 * 
	 * @access public
	 * @param string $controllerClassName
	 * 		The class name of the controller
	 * @param string $moduleId
	 * 		The module from which to load the controller
	 * @return M_Controller
	 */
	public static function getController($controllerClassName, $moduleId = NULL) {
		// Load the controller:
		self::loadController($controllerClassName, $moduleId);
		
		// Construct the controller, and return as result:
		return new $controllerClassName;
	}
	
	/**
	 * Has class been loaded?
	 * 
	 * This method will tell whether or not a class, identified with its name, 
	 * has been loaded.
	 * 
	 * @static
	 * @access public
	 * @param string $className
	 * @return boolean $flag
	 *		Returns TRUE if loaded, FALSE if not!
	 */
	public static function isLoadedClass($className) {
		return (in_array($className, get_declared_classes()));
	}
	
	/**
	 * Get path of loaded class
	 * 
	 * This method will provide with the path to the file where a class was 
	 * loaded from. Note that, if the class has not yet been loaded, this method
	 * will return NULL instead.
	 * 
	 * NOTE:
	 * Note that this method will return the ABSOLUTE path to the file that was
	 * included in order to load a class.
	 * 
	 * @static
	 * @access public
	 * @param string $className
	 * @return $path
	 */
	public static function getAbsolutePathOfLoadedClass($className) {
		// If the provided class name has not yet been loaded:
		if(! self::isLoadedClass($className)) {
			// Then, we cannot return the path to the class:
			return NULL;
		}
		
		// If the provided class name is a core class
		if(! strncmp($className, 'M_', 2)) {
			// Then, remove the prefix of core classes:
			$className = substr($className, 2);
		}
		
		// For each of the included files:
		foreach(get_included_files() as $file) {
			// Is the current path the one?
			if(strpos($file, '/' . $className . '.class.php')) {
				// Yes! Then, we return the current path as the result:
				return $file;
			}
		}
		
		// If still here, we return NULL
		return NULL;
	}
	
	/**
	 * Set as autoloader
	 * 
	 * PHP Says:
	 * 
	 * "Many developers writing object-oriented applications create 
	 * one PHP source file per-class definition. One of the biggest 
	 * annoyances is having to write a long list of needed includes at 
	 * the beginning of each script (one for each class).
	 * 
	 * In PHP 5, this is no longer necessary. You may define an 
	 * __autoload function which is automatically called in case you 
	 * are trying to use a class/interface which hasn't been defined 
	 * yet. By calling this function the scripting engine is given a 
	 * last chance to load the class before PHP fails with an error."
	 * 
	 * This method allows you to set M_Loader as the __autoload handler.
	 * If you do so, {@link M_Loader::autoload()} will be used to 
	 * autoload classes and interfaces.
	 * 
	 * @static
	 * @access public
	 * @return void
	 */
	public static function setAsAutoloader() {
		spl_autoload_register('autoload');
	}
}

/**
 * Autoloader
 * 
 * This function is used as the __autoload handler. Check the docs
 * on {@link M_Loader::setAsAutoloader()} for more info.
 * 
 * @access public
 * @return void
 */
function autoload($class) {
	// Call helper (only once)
	static $helperInitiated = FALSE;
	if(! $helperInitiated) {
		autoloadHelper();
		$helperInitiated = TRUE;
	}
	
	// Load requested class:
	if(!strncmp($class, 'MI_', 3)) {
		M_Loader::loadCoreInterface($class);
	} elseif(!strncmp($class, 'M_', 2)) {
		M_Loader::loadCoreClass($class);
	} elseif(strpos($class,'Controller')) {
		M_Loader::loadController($class);	
	} elseif(strpos($class,'View')) {
		M_Loader::loadView($class);	
	} elseif(strpos($class,'Model')) {
		M_Loader::loadModel($class);
	} elseif($class == 'M') {
		M_Loader::loadCoreClass('M');
	} 
}

/**
 * Autoloader helper
 * 
 * This function is employed as a helper to the {@link autoload()} function, in
 * order to correctly locate core classes. Its job is to register the class path
 * of core classes, wherever necessary.
 * 
 * @access public
 * @return void
 */
function autoloadHelper() {
	M_Loader::addClassPath('core/Fs', 'M_File', 'M_Directory', 'M_Ftp*', 'M_Archive*');
	M_Loader::addClassPath('core/Form', 'M_Field*', 'M_ViewField*', 'M_ViewForm*');
	M_Loader::addClassPath('core/MoneyTransaction', 'M_MoneyTransaction*');
	M_Loader::addClassPath('core/DataObject', 'M_DataObject*', 'M_FormDataObject');
	M_Loader::addClassPath('core/Client', 'M_Browser');
	M_Loader::addClassPath('core/Contact', 'M_ViewMicroformatHcard');
	M_Loader::addClassPath('core/Mail', 'MI_Mailer*', 'M_Mailer*');
	M_Loader::addClassPath('core/Config', 'M_FormConfig');
	M_Loader::addClassPath('core/OneTimePassword', 'M_OneTimePassword*');
	M_Loader::addClassPath('core/MultiSafepay', 'M_MultiSafepay*');
	M_Loader::addClassPath('core/Fs', 'M_Sftp');
}