<?php
/**
 * M_Request class
 * 
 * M_Request provides with tools to fetch the values of page request 
 * variables, to fetch the page request method, etc.
 *
 * @package Core
 */
class M_Request extends M_Object {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Constant to address data type
	 * 
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_STRING = 'string';

	/**
	 * Constant to address data type
	 *
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_STRING_SANITIZED = 'string-sanitized';
	
	/**
	 * Constant to address data type
	 * 
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_INT = 'int';

	/**
	 * Constant to address data type
	 *
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_NUMERIC = 'numeric';

	/**
	 * Constant to address data type
	 *
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_NUMERIC_LOCALE_DEPENDENT = 'numeric-locale-dependent';
	
	/**
	 * Constant to address data type
	 * 
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_ARRAY = 'array';
	
	/**
	 * Constant to address request method
	 * 
	 * This constant is used to address the GET method. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const GET = 'GET';
	
	/**
	 * Constant to address request method
	 * 
	 * This constant is used to address the POST method. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const POST = 'POST';
	
	/**
	 * Constant to address request method
	 * 
	 * This constant is used to address the PUT method. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const PUT = 'PUT';
	
	/**
	 * Constant to address request method
	 * 
	 * This constant is used to address the DELETE method. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const DELETE = 'DELETE';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Link prefix
	 * 
	 * This property stores the prefix, which is prepended to the relative path
	 * of each link that is rendered by {@link M_Request::getLink()}.
	 * 
	 * @static
	 * @see M_Request::setLinkPrefix()
	 * @access private
	 * @var string
	 */
	private static $_linkPrefix = '';
	
	/**
	 * Link prefix excludes
	 * 
	 * This property stores the links in the application for which the prefix 
	 * should not be added.
	 * 
	 * @static
	 * @see M_Request::addLinkPrefixExcludeRule()
	 * @access private
	 * @var array
	 */
	private static $_linkPrefixExcludes = array();
	
	/**
	 * Enable locale-dependent path elements?
	 * 
	 * This property stores a boolean that tells whether or not automatically
	 * translated path elements should be enabled, when generatings links for 
	 * the application.
	 * 
	 * By default, this property is set to FALSE. This means that path elements
	 * are never translated by default.
	 * 
	 * @static
	 * @see M_Request::setEnableLocalePathElements()
	 * @access private
	 * @var boolean
	 */
	private static $_enableLocalePathElements = FALSE;
	
	/**
	 * Locale-dependent path elements
	 * 
	 * This property stores an array with path elements that should be translated
	 * automatically in the application, when generating links.
	 * 
	 * @static
	 * @see M_Request::addLocalePathElement()
	 * @access private
	 * @var array
	 */
	private static $_localePathElements = array();
	
	/**
	 * Cache of path element translations
	 * 
	 * When path elements are translated, the translations are cache'd for
	 * performance. This property stores the cache'd translations.
	 * 
	 * @static
	 * @see M_Request::getTranslatedPathElement()
	 * @access private
	 * @var array
	 */
	private static $_localePathElementsCache = array();
	
	/**
	 * Flag: Sanitize all request variables?
	 * 
	 * {@link M_Request} allows you to explicitly sanitize ALL incoming request
	 * variables, in protection against some generic exploits such as XSS and
	 * SQL injection
	 * 
	 * @static
	 * @see M_Request::setSanitizeAllRequestVariables()
	 * @access private
	 * @var boolean
	 */
	private static $_sanitizeAll = FALSE;
	
	/**
	 * Array: Sanitize all exclude variables
	 * 
	 * {@link M_Request} allows you to exlude some request variables from sanitizing
	 * This can be helpful for some fields like password fields
	 * 
	 * @static
	 * @see M_Request::setSanitizeAllRequestVariables()
	 * @access private
	 * @var array
	 */
	private static $_sanitizeAllExcludeVariables = array();
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_Request
	 */
	public function __construct() {
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get request method
	 * 
	 * This method will return the request method with which the page
	 * has been requested.
	 * 
	 * @access public
	 * @static
	 * @return string
	 * 		The page request method (GET|POST|...)
	 */
	public static function getMethod() {
		return strtoupper($_SERVER['REQUEST_METHOD']);
	}
	
	/**
	 * Get request variables
	 * 
	 * This method returns the page request variables that have been
	 * sent to the current page request. If the page request method is
	 * POST, this method will return a copy of
	 * 
	 * <code>$_POST</code>
	 * 
	 * If the page request method is GET, this method will return a 
	 * copy of
	 * 
	 * <code>$_GET</code>
	 * 
	 * This method returns an exact copy; an associative array of
	 * which the keys are the names of the page request variables.
	 * 
	 * You can force this method to fetch the variables from a 
	 * specific requested method.
	 * 
	 * Example 1, fetch GET variables
	 * <code>
	 *    print_r(M_Request::getVariables(M_Request::GET));
	 * </code>
	 * 
	 * @access public
	 * @static
	 * @param string $method
	 * 		The method from which to fetch the page request
	 * 		variables.
	 * @return array
	 */
	public static function getVariables($method = NULL) {
		// Output:
		$out = array();
		
		// For each of the request variables:
		foreach(array_keys(self::_getVariablesRaw($method)) as $key) {
			// Add the variable to the output:
			// (we use M_Request::getVariable() so the variable gets sanitized)
			$out[$key] = self::getVariable($key);
		}
		
		// Return the variables:
		return $out;
	}
	
	/**
	 * Get query string
	 * 
	 * This method will return the query string, parsed into the
	 * URL-encoded format.
	 * 
	 * Example 1
	 * <code>
	 *    $request = M_Request::getInstance();
	 *    $request->setVariable('author', 'ben');
	 *    echo $request->getQueryString();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>author=ben</code>
	 * 
	 * NOTE:
	 * To separate the query variables, this method will use "&"
	 * 
	 * @access public
	 * @return string
	 */
	public function getQueryString() {
		return http_build_query(self::getVariables(), '', '&');
	}
	
	/**
	 * Get request variable
	 * 
	 * Get a selected variable from the page request variables.
	 * For example, consider a page that has been requested via the
	 * following URL:
	 * 
	 * <code>
	 *    http://www.multimedium.be/?page=news
	 * </code>
	 * 
	 * To fetch the value of the request variable "page", you could
	 * use the following code:
	 * 
	 * Example 1
	 * <code>
	 *    echo M_Request::getVariable('page');
	 * </code>
	 * 
	 * You can specify the default value that should be returned by
	 * this method, if no value has been found for the requested var.
	 * 
	 * Also, you can specify the expected data type. If a value has
	 * been found for the requested variable, but its type does not
	 * match the expected one, this method will also return the default
	 * value. If the expected data type is not given, it is ignored.
	 * 
	 * Last, but not least, you can also specify the request method
	 * from which you expect the variable's value. If the variable has
	 * not been find in the specified request method, this method will
	 * again return the default value. If the method is not given, it
	 * is ignored.
	 * 
	 * In Example 1, we know that the request variable "page" should 
	 * be a string, provided via the GET method. So we could rewrite
	 * Example 1 as following, in order to obtain more secure code:
	 * 
	 * Example 2, secure rewrite of Example 1
	 * (this example sets the default value to "home")
	 * <code>
	 *    echo M_Request::getVariable('page', 'home', M_Request::TYPE_STRING, M_Request::GET);
	 * </code>
	 * 
	 * By fetching variables as illustrated in Example 2, we write
	 * more secure code. We force the variable to a given type (a string
	 * in this case), and we only accept a value if it has been
	 * provided in the expected request method.
	 * 
	 * @access public
	 * @static
	 * @param string $name
	 * 		The name of the request variable
	 * @param mixed $defaultValue
	 * 		The default value of the variable
	 * @param string $cast
	 * 		The expected data type
	 * @param string $method
	 * 		The requested method
	 * @return mixed
	 */
	public static function getVariable($name, $defaultValue = FALSE, $cast = NULL, $method = NULL) {
		// Get the "raw" request variables, for the method specified:
		$vars = self::_getVariablesRaw($method);
		
		// If the variable is not defined:
		if(! isset($vars[$name])) {
			// Then, we return the default value:
			return $defaultValue;
		}
		
		// Get the variable's value. The output variable is initiated to that 
		// value:
		$out = $vars[$name];
		
		// If ALL variables are to be sanitized:
		if(self::$_sanitizeAll && ! in_array($name, self::$_sanitizeAllExcludeVariables)) {
			// Sanitize the variable
			$out = self::_getSanitized($out);
		}
		
		// A data type may be provided. This data type will influence the 
		// outcome of this function:
		switch($cast) {
			// A string:
			case self::TYPE_STRING:
				// Cast to a string, or return the default value if the variable
				// does not match the expected data type:
				return (is_string($out) ? (string) $out : $defaultValue);
			
			// A sanitized string:
			case self::TYPE_STRING_SANITIZED:
				// If the variable is not a string:
				if(!is_string($out)) {
					// Then, we return the default value:
					return $defaultValue;
				}
				
				// If not already sanitized earlier:
				if(! self::$_sanitizeAll) {
					// Sanitize the variable
					self::_getSanitized($out);
				}
				
				// Return the output:
				return $out;
			
			// An integer:
			case self::TYPE_INT:
				// Cast to an integer, or return the default value if the variable
				// does not match the expected data type:
				return (is_numeric($out) ? (int) $out : $defaultValue);
			
			// A numeric value:
			case self::TYPE_NUMERIC:
				// Return the numeric value, or return the default value if the 
				// variable does not match the expected data type:
				return is_numeric($out) ? $out : $defaultValue;
			
			// Numeric value, in locale-dependent syntax
			case self::TYPE_NUMERIC_LOCALE_DEPENDENT:
				// We try to
				try {
					// Parse a number with the provided value:
					return M_Number::constructWithString($out)->getNumber();
				
				// If we fail to do so:
				} catch(M_Exception $e) {
					// We return the default value:
					return $defaultValue;
				}
			
			// An array
			case self::TYPE_ARRAY:
				// Cast to an array, or return the default value if the variable
				// does not match the expected data type:
				return is_array($out) ? (array) $out : $defaultValue;
			
			// When data type is not specified
			default:
				// We return the output value as-is:
				// (will already have been sanitized if all variables are processed 
				//  against generic attacks)
				return $out;
		}
	}
	
	/**
	 * Get protocol
	 * 
	 * This method will return the protocol via which the page has
	 * been requested. Possible return values:
	 * 
	 * <code>
	 *    http
	 *    https
	 * </code>
	 * 
	 * @access public
	 * @static
	 * @return string
	 */
	public static function getProtocol() {
		// If the protocol is https
		if(self::isHttps()) {
			// Then, return https as protocol
			return 'https';
		}
		
		// Check if the server variables indicate a protocol:
		if(isset($_SERVER['SERVER_PROTOCOL'])) {
			// The server protocol string is a string like HTTP/1.0. So, we explode
			// the protocol string by the slash separator:
			$temp = explode('/', $_SERVER['SERVER_PROTOCOL']);
			
			// Get the first element, and convert to lowercased string:
			return strtolower($temp[0]);
		}
		// If the server does not define the protocol:
		else {
			// Then, we return 'http' by default
			return 'http';
		}
	}
	
	/**
	 * Get the host of the request
	 * 
	 * This method will return the name of the host via which the
	 * application has been requested.
	 * 
	 * Example 1. Assuming the application is requested at the following
	 * address: http://localhost/myApp
	 * 
	 * <code>
	 *    echo M_Request::getHost();
	 * </code>
	 * 
	 * Example 1 will produce the following output:
	 * 
	 * <code>
	 *    localhost
	 * </code>
	 * 
	 * NOTE:
	 * If the host could not have been identified, this method will
	 * return FALSE.
	 * 
	 * @access public
	 * @static
	 * @return string|FALSE
	 */
	public static function getHost() {
		if(isset($_SERVER['HTTP_HOST'])) {
			return $_SERVER['HTTP_HOST'];
		}
		return FALSE;
	}
	
	/**
	 * Get Host Address (string)
	 * 
	 * This method will return the full address to the domain via which 
	 * the application has been requested.
	 * 
	 * Example 1. Assuming the application is requested at the following
	 * address: http://localhost/myApp
	 * 
	 * <code>
	 *    echo M_Request::getHostAddress();
	 * </code>
	 * 
	 * Example 1 will produce the following output:
	 * 
	 * <code>
	 *    http://localhost
	 * </code>
	 * 
	 * @see M_Request::getHost()
	 * @access public
	 * @static
	 * @return string
	 */
	public static function getHostAddress() {
		return self::getProtocol() . '://' . self::getHost();
	}
	
	/**
	 * Get Request URI (string)
	 * 
	 * This method will return the Request URI. This method will check
	 * the server environment variables, in order to find a value for
	 * the Request URI (different for apache server, iis, ...).
	 * 
	 * @access public
	 * @static
	 * @return string
	 */
	public static function getUriString() {
		if(!isset($_SERVER['REQUEST_URI'])) {
			$uri = $_SERVER['SCRIPT_NAME'];
			if(isset($_SERVER['QUERY_STRING'])) {
				$uri .= '?' . $_SERVER['QUERY_STRING'];
			} elseif(isset($_SERVER['argv'])) {
				$uri .= '?' . $_SERVER['argv'];
			}
			return $uri;
		} else {
			return $_SERVER['REQUEST_URI'];
		}
	}
	
	/**
	 * Get Request URI (object)
	 * 
	 * This method will return the Request URI, in the form of an
	 * {@link M_Uri} object. This object will include all properties
	 * of the Request URI:
	 * 
	 * - host
	 * - path
	 * - variables
	 * - etc.
	 * 
	 * @access public
	 * @static
	 * @return M_Uri
	 */
	public static function getUri() {
		return new M_Uri(self::getProtocol() . '://' . self::getHost() . self::getUriString());
	}
	
	/**
	 * Get (absolute) link
	 * 
	 * This method will create an absolute link out of a relative path.
	 * To do so, it uses the {@link M_Uri} object that is returned by
	 * {@link M_Request::getUri()}, and changes the path to the 
	 * relative one.
	 * 
	 * Note that this method will prepend the application's base href,
	 * as returned by {@link M_Server::getBaseHref()}, to the given
	 * path.
	 * 
	 * Also, this method will prepend the link prefix (if any), that may have
	 * been set previously with {@link M_Request::setLinkPrefix()}.
	 * 
	 * Also, you can provide the query variables for the new link 
	 * (second argument to this method). For more info on that, check
	 * {@link M_Uri::setQueryVariables()}.
	 * 
	 * @access public
	 * @static
	 * @see M_Request::getLinkWithoutPrefix()
	 * @param string $path
	 * 		The relative path for the link
	 * @param string $variables
	 * 		The query variables for the new link (associative array)
	 * @return string
	 */
	public static function getLink($path = NULL, array $variables = array()) {
		// Clean up the provided path:
		$path = M_Helper::ltrimCharlist((string) $path, '/');
		
		// Translate the path:
		$pathOut = self::getTranslatedPath($path);
		
		// If a link prefix is to be added:
		if(! empty(self::$_linkPrefix)) {
			// If exclude rules have been added for the link prefix, then we will not
			// prepend the prefix to the path of the link. So, for each of the exclude
			// rules defined:
			foreach(self::$_linkPrefixExcludes as $rule) {
				// We check if the provided path matches with the exclude rule.
				// If that is the case:
				if(M_ControllerDispatcherPathRewriter::isPathMatchingRule($rule, $path)) {
					// Then, we return the link without prefix:
					return self::getLinkWithoutPrefix($pathOut, $variables);
				}
			}
			
			// If still here, then none of the exclude rules stopped the path
			// from having the prefix prepended to it. In this case, we return 
			// the link with prefix:
			return self::getLinkWithoutPrefix(self::$_linkPrefix . '/' . $pathOut, $variables);
		}
		
		// If still here, then no link prefix has been defined. Return the link
		// without prefix:
		return self::getLinkWithoutPrefix($pathOut, $variables);
	}
	
	/**
	 * Get (absolute) link, without prefix
	 * 
	 * @access public
	 * @static
	 * @see M_Request::getLink()
	 * @param string $path
	 * 		The relative path for the link
	 * @param string $variables
	 * 		The query variables for the new link (associative array)
	 * @return string
	 */
	public static function getLinkWithoutPrefix($path = NULL, array $variables = array()) {
		$uri = self::getUri();
		$uri->setPath(M_Server::getBaseHref() . $path);
		$uri->setQueryVariables($variables, TRUE);
		return $uri->getUri();
	}
	
	/**
	 * Get link prefix
	 * 
	 * Link prefix is added to all absolute links
	 * 
	 * @see setLinkPrefix()
	 * @return string
	 */
	public static function getLinkPrefix() {
		return self::$_linkPrefix;
	}
	
	/**
	 * Get flag: Enable locale-dependent path elements?
	 * 
	 * This method will tell whether or not path elements should be translated 
	 * automatically, when generatings links for the application.
	 * 
	 * @see M_Request::setEnableLocalePathElements()
	 * @static
	 * @access public
	 * @return bool
	 */
	public static function isLocalePathElementsEnabled() {
		return self::$_enableLocalePathElements;
	}
	
	/**
	 * Is locale-dependent path element?
	 * 
	 * This method will tell whether or not a path element has been registered
	 * as a locale-dependent path element, which should be translated automatically
	 * when creating links in the application.
	 * 
	 * Note that the path element will be evaluated as locale-dependent, with
	 * the provided index taken into account. For more information about the 
	 * index, read {@link M_Request::addLocalePathElement()}.
	 * 
	 * @static
	 * @param string $element
	 *		The path element to be evaluated as a locale-dependent element
	 * @param integer $index
	 *		The index at which the element is to be evaluated
	 * @return bool $flag
	 *		Returns TRUE if the provided element is a locale-dependent element,
	 *		FALSE if not.
	 */
	public static function isLocalePathElement($element, $index = 0) {
		// Cast the provided element to a string, and clean up:
		$clean = M_Helper::trimCharlist((string) $element, '/');
		
		// Check if the element has been defined as locale-dependent:
		return (
			// Has the path element been registered as locale-dependent?
			array_key_exists($clean, self::$_localePathElements) &&
			// ... for the provided index?
			in_array((int) $index, self::$_localePathElements[$clean])
		);
	}
	
	/**
	 * Get translated path
	 * 
	 * This method will take a path as an argument, and translate the path 
	 * elements that have been registered as locale-dependent path elements. Of
	 * course, this method will only translate elements in the path, if automatic
	 * translation has been enabled: {@link M_Request::setEnableLocalePathElements()}
	 * 
	 * This method is also used by {@link M_Request::getLink()} in order to
	 * generate links in the application. Other methods that make use of this 
	 * function are:
	 * 
	 * - {@link M_Request::getLink()},
	 * - {@link M_Request::getLinkWithoutPrefix()}
	 * 
	 * @static
	 * @access public
	 * @param string $path
	 *		The path to be translated
	 * @return string $path
	 *		The translated path
	 */
	public static function getTranslatedPath($path) {
		// Clean up the path:
		$path = (string) $path;
		
		// If locale-dependent path elements are not enabled:
		if(! self::isLocalePathElementsEnabled()) {
			// Then, return the path as-is, unaffected
			return $path;
		}
		
		// The output of this function:
		$out = array();
		
		// For each of the path elements:
		$index = 0;
		foreach(explode('/', $path) as $element) {
			// Clean up the current path element:
			$clean = trim($element);
			
			// If the current path element is not empty:
			if($clean != '') {
				// Then, we check if the path element should be translated. If
				// so, we translate the element. If not, we add the element as-is,
				// to the output:
				array_push($out, self::getTranslatedPathElement($element, $index ++));
			}
		}
		
		// Return the translated path:
		return implode('/', $out);
	}
	
	/**
	 * Get translated path element
	 * 
	 * This method will translate the provided path element, if the element has
	 * been registered as a locale-dependent element. Note that, if the element
	 * has not been set as a locale-dependent element, this method will return
	 * the untranslated path element.
	 * 
	 * @static
	 * @access public
	 * @param string $element
	 *		The path element to be translated
	 * @param integer $index
	 *		The index at which the path is, or will be, used.
	 * @return string $element
	 *		The translated path element
	 */
	public static function getTranslatedPathElement($element, $index = 0) {
		// Cast the provided element to a string, and clean up:
		$clean = M_Helper::trimCharlist((string) $element, '/');
		
		// If locale-dependent path elements are not enabled:
		if(! self::isLocalePathElementsEnabled()) {
			// Then, return the path element as-is, unchanged
			return $clean;
		}
		
		// If the requested element is not locale-dependent:
		if(! self::isLocalePathElement($clean, $index)) {
			// Then, return the unchanged element
			return $clean;
		}
		
		// If not requested before:
		if(! array_key_exists($clean, self::$_localePathElementsCache)) {
			// Get the translated string. Note that we add a slash to the beginning
			// of the untranslated path element. This way, it is clear for 
			// translators that the string is being used in a path, by the 
			// application.
			$translated = t('/' . $clean);
			
			// Because the translated text is not always nicely formatted, we 
			// will make sure the translated path element is of valid format. 
			// We'll remove accents, and parse a URI Path element out of the 
			// translated text. Note that we'll only do this, if a translation 
			// is available:
			if($translated != $clean) {
				// Construct a filter to remove accents:
				$filter = new M_FilterTextAccents(new M_FilterTextValue($translated));

				// Apply the filter, remove slashes, and parse a path element out
				// of the result:
				$translated = M_Uri::getPathElementFromString(
					M_Helper::trimCharlist($filter->apply(), '/')
				);
			}
			
			// Store the translated path element in the cache, so this process
			// is not repeated for the same element:
			self::$_localePathElementsCache[$clean] = $translated;
		}
		
		// Return the translated path element:
		return self::$_localePathElementsCache[$clean];
	}
	     
	/**
	 * Clear Translated Path Elements In Cache
	 * 
	 * Will clear the currently set collection of translated path elements.
	 * Doing this is for example needed when asking the translation of a
	 * translatable path elements in 2 different locales, for example:
	 * 
	 * <code>
	 *	// The element that is to be translated
	 *	$element = 'nieuws';
	 * 
	 *	// Force the locale to French
	 *	M_Locale::setCategory(M_Locale::LANG, 'fr');
	 *	M_Request::setLinkPrefix('fr');
	 * 
	 *	// Translate the path element
	 *	$pathElement = M_Request::getLink($element);
	 * 
	 *	// Now force the locale to English
	 *	M_Locale::setCategory(M_Locale::LANG, 'en');
	 *	M_Request::setLinkPrefix('en');
	 * 
	 *	// IMPORTANT: clear the current cache, otherwise the French version
	 *	// will be returned
	 *	M_Request::clearTranslatedPathElementsInCache()
	 * 
	 *	// And translate the path element again
	 *	$pathElement = M_Request::getLink($element);
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @return void
	 */
	public static function clearTranslatedPathElementsInCache() {
		// Clear the cache
		self::$_localePathElementsCache = array();
	}
	
	/**
	 * Is https?
	 * 
	 * This method will check if the request has been made over a
	 * SSL (Secure Socket Layer) connection. This method will return
	 * TRUE if the request has been made over SSL, FALSE if not.
	 * 
	 * @access public
	 * @static
	 * @return boolean
	 */
	public static function isHttps() {
		// -- the traditional way:
		//
		// "HTTPS", PHP Predefined variable in SERVER
		// Set to a non-empty value if the script was queried through 
		// the HTTPS protocol. Note that when using ISAPI with IIS, the 
		// value will be "off" if the request was not made through the 
		// HTTPS protocol. 		
		if (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
			return true;
		}
		
		// -- The Tigron way
		// 
		// As tigron uses a load balancer, the above method is not
		// always waterproof. We can check if ["X-Forwarded-Proto"]
		// header variable is set to https as an alternative.
		// As we are not sure the getallheaders() function exists, we 
		// checj that first
		if (function_exists('getallheaders')) {
			$headers = getallheaders();
			// some weird code to solve a weird bug
			$keys = array_keys($headers);
			$values = array_values($headers);	
			$key = array_search('X-Forwarded-Proto', $keys);
		
			if ($key != false && $values[$key] == 'https' ) {
				return true;
			}
		}
		
		// if we are still here, we asume we are not in https, so we return false
		return false;
	}
	
	/**
	 * Is POST?
	 * 
	 * This method can be used to check whether or not the page has
	 * been requested via the POST method.
	 * 
	 * @static
	 * @access public
	 * @return boolean
	 */
	public static function isPost() {
		return (self::getMethod() == 'POST');
	}
	
	/**
	 * Is GET?
	 * 
	 * This method can be used to check whether or not the page has
	 * been requested via the GET method.
	 * 
	 * @static
	 * @access public
	 * @return boolean
	 */
	public static function isGet() {
		return (self::getMethod() == 'GET');
	}
	
	/**
	 * Parse the variables out of a path
	 * 
	 * This method will return an array with values for the variables
	 * in the given path (starting at index number 0). If the URL does
	 * not match the provided template path, this method will return
	 * FALSE.
	 * 
	 * Example 1
	 * <code>
	 *    $vars = M_Request::getPathVariables('news/my-news-article', 'news/%');
	 *    print_r($vars);
	 * </code>
	 * 
	 * Example 1 would output the following array:
	 * 
	 * <code>
	 *    Array (
	 *       0 => 'my-news-article'
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @static
	 * @param string $path
	 * 		The path to extract the variables from
	 * @param string $template
	 * 		The pattern that describes the path
	 * @return array|FALSE
	 */
	public static function getPathVariables($path, $template) {
		$template = str_replace('%', '(.+)', preg_quote($template, '/'));
		$match = array();
		if(preg_match('/'. $template .'/i', $path, $match)) {
			// We do not include the URL in the result:
			array_shift($match);
			
			// Return the variables
			return $match;
		}
		return FALSE;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set flag: sanitize ALL variables?
	 * 
	 * This method allows you to explicitly sanitize ALL incoming request 
	 * variables that may be used by the application. This adds a protection to
	 * your application, against generic exploits such as XSS and SQL injection.
	 * 
	 * NOTE:
	 * M_Request will use HTMLPurifier to clean up variables. However, for extra 
	 * security, M_Request will also remove all HTML from request variables in 
	 * order to remove masked javascript code.
	 * 
	 * @static
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE to sanitize ALL incoming request variables, FALSE to 
	 *		disable sanitizing. Variables will be sanitized when retrieved with
	 *		{@link M_Request::getVariable()} or {@link M_Request::getVariables()}
	 * @param array $excludeVariables
	 *		Add one or more exclude variables to prevent from sanitizing, this can be helpful
	 *		for some fields, for example password fields
	 * @return void
	 */
	public static function setSanitizeAllRequestVariables($flag, array $excludeVariables = array()) {
		self::$_sanitizeAll = (bool) $flag;
		self::$_sanitizeAllExcludeVariables = $excludeVariables;
	}
	
	/**
	 * Add sanitize all exclude variable
	 * 
	 * This will exclude a specific variable name from sanitizing
	 * This can be useful for some fields that you don't want to sanitize, like password fields
	 * 
	 * @static
	 * @access public
	 * @param string $name
	 */
	public static function addSanitizeAllExcludeVariable($name) {
		array_push(self::$_sanitizeAllExcludeVariables, (string) $name);
	}
	
	/**
	 * Set request variable
	 * 
	 * @static
	 * @access public
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public static function setVariable($name, $value) {
		$GLOBALS['_' . self::getMethod()][$name] = $value;
	}
	
	/**
	 * Remove request variable
	 * 
	 * @static
	 * @access public
	 * @param string $name
	 * @return void
	 */
	public static function removeVariable($name) {
		unset($GLOBALS['_' . self::getMethod()][$name]);
	}
	
	/**
	 * Set link prefix
	 * 
	 * Can be used to set the link prefix, which should be added to all absolute 
	 * links that are rendered by {@link M_Request::getLink()}.
	 * 
	 * Example 1. Assuming the application is requested at the following
	 * address: http://localhost/myApp
	 * 
	 * <code>
	 *    M_Request::setLinkPrefix('nl');
	 *    echo M_Request::getLink('myPage');
	 * </code>
	 * 
	 * Example 1 will produce the following output:
	 * 
	 * <code>
	 *    http://localhost/myApp/nl/myPage
	 * </code>
	 * 
	 * @access public
	 * @param string $prefix
	 * 		The link prefix, to be added to all links
	 * @return void
	 */
	public static function setLinkPrefix($prefix) {
		self::$_linkPrefix = M_Helper::trimCharlist((string) $prefix, '/');
	}
	
	/**
	 * Add exclude rule for link prefix
	 * 
	 * When a link prefix is set with {@link M_Request::setLinkPrefix()}, then
	 * this prefix will be prepended to all links in the application. Whenever 
	 * you need to get links that do not have the prefix prepended to its path,
	 * you can use {@link M_Request::getLinkWithoutPrefix()}... or you can register
	 * an exclude for that particular path.
	 * 
	 * Consider the following example:
	 * <code>
	 *    M_Request::setLinkPrefix('nl');
	 *    echo M_Request::getLink('thumbnail/my-image.jpg');
	 * </code>
	 * 
	 * The code above would output a link with path "nl/thumbnail/my-image.jpg".
	 * Note that the outputted link carries the prefix that was defined earlier.
	 * Let's assume that we do not want to include the prefix in any link that
	 * starts with "thumbnail". In that case, we would add the following exclude
	 * rule for link prefixes:
	 * 
	 * <code>
	 *    M_Request::setLinkPrefix('nl');
	 *    M_Request::addLinkPrefixExcludeRule('thumbnail/(:any)');
	 *    echo M_Request::getLink('thumbnail/my-image.jpg');
	 * </code>
	 * 
	 * Note that exclude rules are expressed in the same syntaxis as Routing Rules
	 * are, in {@link M_ControllerDispatcher::setRoutingRules()}.
	 * 
	 * Note that exclude rules are to be expressed in original (untranslated) 
	 * paths. Exclude rules will be evaluated on the untranslated path, when
	 * creating links in the application.
	 * 
	 * @static
	 * @see M_Request::setLinkPrefix()
	 * @access public
	 * @param string $rule
	 *		The expression that describes the path to which prefixes should not
	 *		be prepended in the application.
	 * @return void
	 */
	public static function addLinkPrefixExcludeRule($rule) {
		array_push(self::$_linkPrefixExcludes, M_Helper::ltrimCharlist((string) $rule, '/'));
	}
	
	/**
	 * Enable locale-dependent path elements?
	 * 
	 * This method allows to set a flag that indicates whether or not path 
	 * elements should be translated automatically, when generatings links for 
	 * the application.
	 * 
	 * @static
	 * @see M_Request::addLocalePathElement()
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE if locale-dependent path elements should be enabled,
	 *		FALSE if not
	 * @return void
	 */
	public static function setEnableLocalePathElements($flag) {
		self::$_enableLocalePathElements = (bool) $flag;
	}
	
	/**
	 * Add locale-dependent path element
	 * 
	 * This method allows to add a path element that should be translated
	 * automatically in the application, when generating links. Let's assume we
	 * want to translate the path element /news in our application. In that case,
	 * we would register that path element as following:
	 * 
	 * <code>
	 *    M_Request::addLocalePathElement('news');
	 * </code>
	 * 
	 * Note that it is possible to enable translation only if the path element 
	 * occurs at a given index in the path, by providing the second argument to 
	 * this function. Continuing on our example, we could specify that the /news
	 * path element is to be translated only if it is the first element in the
	 * path:
	 * 
	 * <code>
	 *    M_Request::addLocalePathElement('news', 0);
	 * </code>
	 * 
	 * Note that indexes start at ZERO. With our last piece of example code, 
	 * please look at some examples of translated and untranslated path elements:
	 * 
	 * <code>
	 *    // 'news' will be translated, because it is the first path element:
	 *    M_Request::getLink('news/my-news-article');
	 *    
	 *    // 'news' will NOT be translated, because it is not the first element
	 *    // in the path:
	 *    M_Request::getLink('admin/overview/news');
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param string $element
	 * @param integer $index
	 * @return void
	 */
	public static function addLocalePathElement($element, $index = 0) {
		// Cast the provided element to a string, and clean up the element:
		$clean = M_Helper::trimCharlist((string) $element, '/');
		
		// The path element must not contain slashes and other forbidden 
		// characters:
		if(strpos($element, '/') !== FALSE) {
			// If containing a forbidden character, we throw an exception to
			// inform about the error
			throw new M_Exception(sprintf(
				'Cannot add locale-dependent path element "%s". An element must ' . 
				'not contain forbidden characters, such as "/"',
				$element
			));
		}
		
		// Cast the index to an integer:
		$index = (int) $index;
		
		// The index must be greater than, or equal to, ZERO
		if($index < 0) {
			// If not the case, we throw an exception to inform about the error:
			throw new M_Exception(sprintf(
				'Cannot set locale-dependent path element at index %s. The index ' . 
				'must be greater than, or equal to, ZERO.',
				$index
			));
		}
		
		// First, we check if the path element has already been registered at 
		// another index. If that is not the case,
		if(! array_key_exists($clean, self::$_localePathElements)) {
			// Then, we initiate the entry with indexes now:
			self::$_localePathElements[$clean] = array();
		}
		
		// If not already registered
		if(! in_array($index, self::$_localePathElements[$clean])) {
			// Then, add the index now, for the provided path element:
			array_push(self::$_localePathElements[$clean], $index);
		}
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get variables for Request Method
	 * 
	 * Is used internally, in order to get the variables for a given request
	 * method. For example, if the request method passed to this function is
	 * {@link M_Request::GET}, the array $_GET will be returned.
	 * 
	 * Note that the request method is optional. If not provided, this function
	 * will default to {@link M_Request::getMethod()}.
	 * 
	 * The variables returned by this function are raw, meaning that they are
	 * not processed (eg. sanitized or casted to a data type) in any way. 
	 * 
	 * @static
	 * @access protected
	 * @param string $method
	 * @return array
	 */
	protected static function _getVariablesRaw($method = NULL) {
		// If a specific method (GET/POST/...) has NOT been provided as an 
		// argument:
		if(! $method) {
			// Default to the current page request's method:
			$method = self::getMethod();
		}
		
		// Fetch the variables:
		$key  = '_' . strtoupper($method);
		return (
			array_key_exists($key, $GLOBALS) && is_array($GLOBALS[$key])
			? $GLOBALS[$key]
			: array()
		);
	}
	
	/**
	 * Get sanitized value
	 * 
	 * @static
	 * @access protected
	 * @param mixed $value
	 * @return mixed
	 */
	protected static function _getSanitized($value) {
		// If the value is an array
		if(is_array($value)) {
			// For each of the elements in the array:
			foreach($value as $key => $currentValue) {
				// Sanitize the current value:
				$value[$key] = self::_getSanitized($currentValue);
			}
			
			// Return the array
			return $value;
		}
		// If not an array:
		else {
			// Get the filter to sanitize, and apply:
			return self::_getSanitizeFilter($value)->apply();
		}
	}
	
	/**
	 * Get filter for sanitizing
	 * 
	 * @static
	 * @access protected
	 * @param string $value
	 * @return MI_Filter
	 */
	protected static function _getSanitizeFilter($value) {
		return new M_FilterTextHtml(
			new M_FilterTextXss(
				new M_FilterTextValue($value)
			)
		);
	}
}