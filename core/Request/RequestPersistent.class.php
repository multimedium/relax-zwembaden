<?php
/**
 * M_RequestPersistent
 * 
 * Typically, a subclass of {@link M_RequestPersistent} is created in order to
 * implement an interface for retrieving filter values passed in the current
 * page request. These filter values are to be stored persistently in the session,
 * so that - upon the next page request, without query variables - the filter 
 * values are maintained active.
 * 
 * @abstract
 * @package Core
 */
abstract class M_RequestPersistent extends M_ObjectSingleton {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Session Namespace
	 * 
	 * @see M_RequestPersistent::_getSessionNamespace()
	 * @access private
	 * @var M_SessionNamespace
	 */
	private $_sessionNamespace;
	
	/**
	 * Variable names
	 * 
	 * @see M_RequestPersistent::_setVariableNames()
	 * @access private
	 * @var array
	 */
	private $_names;
	
	/**
	 * Are values retrieved from session, or from query variables
	 * 
	 * @see M_RequestPersistent::_isValueRetrievedFromSession()
	 * @access private
	 * @var boolean
	 */
	private $_isValueRetrievedFromSession;
	
	/**
	 * Values
	 * 
	 * @access private
	 * @var array
	 */
	private $_values = array();
	
	/**
	 * Data Object Values
	 * 
	 * @access private
	 * @var array
	 */
	private $_valuesDataObject = array();
	
	/**
	 * Default Values
	 * 
	 * @access private
	 * @var array
	 */
	private $_defaultValues = array();
	
	/* -- SETTERS -- */
	
	/**
	 * Clear all values
	 * 
	 * @access public
	 * @return M_RequestPersistent $request
	 *		Returns itself, for a fluent programming interface
	 */
	public function clearSession() {
		// Remove all variables from the SESSION:
		$this->_getSessionNamespace()->clear();
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set Values From Request Variables
	 * 
	 * @access public
	 * @return M_RequestPersistent $request
	 *		Returns itself, for a fluent programming interface
	 */
	public function setValuesFromRequest() {
		// First, we'll go through all variables defined in the current
		// scope and set the value for each of the variables:
		foreach($this->_names as $name) {
			// For the current variable, check if a value exists:
			$value = M_Request::getVariable($name, NULL);

			// If a value is found
			if($value) {
				// Store it in the session. We could remove this check
				// on empty values, but we do this in order to obtain
				// less storage in the session. If this should cause problems
				// in the future, we should probably remove this check!
				$this->_getSessionNamespace()->$name = $value;
			}
			// If no value is found:
			else {
				// We remove the variable in the session. No need for
				// storage anymore, for this specific variable:
				$ns = $this->_getSessionNamespace();
				unset($ns->$name);
			}

			// Set the value in the internal property of the RequestPersistent
			// class, so all of this does not need to be repeated:
			$this->_values[$name] = $value;
		}
		
		// Return myself
		return $this;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/* -- GETTERS -- */
	
	/**
	 * Get Data Object With Value
	 * 
	 * Will retrieve the value with {@link M_RequestPersistent::getValue()}, and
	 * look up the data object that is represented by the value.
	 * 
	 * @throws M_Exception
	 * @access protected
	 * @param string $name
	 *		The name of the variable, as you would use it to retrieve a value 
	 *		with {@link M_RequestPersistent::_getValue()}.
	 * @param string $moduleId
	 *		The Module ID in which the data object is located
	 * @param string $dataObjectName
	 *		The name of the data object
	 * @param string $mapperGetter
	 *		The name of the getter function that is to be used, in order to 
	 *		fetch the data object with the mapper.
	 * @param bool $refresh
	 *		Refresh, or retrieve from local cache? By default, the return value
	 *		of this cache'd in local storage.
	 * @return M_DataObject|NULL
	 */
	protected function _getDataObjectWithValue($name, $moduleId, $dataObjectName, $mapperGetter = 'getByAlphaId', $refresh = FALSE) {
		// If not requested, or if a refresh has been requested:
		if(! array_key_exists($name, $this->_valuesDataObject) || $refresh) {
			// Get the value with which to retrieve the data object:
			$value = $this->_getValue($name);
			
			// If this value is not given, then we cannot retrieve a data object.
			// In this case, we do an early return of NULL:
			if(! $value) {
				return NULL;
			}
			
			// Construct a mapper and, with the mapper, construct a callback 
			// object. This will enable us to check if the getter is a valid 
			// function name, for this specific mapper object:
			$callback = new M_Callback();
			$callback->setObject(M_Loader::getDataObjectMapper($dataObjectName, $moduleId));
			$callback->setFunctionName($mapperGetter);
			$callback->addArgument($value);
			
			// If not callable:
			if(! $callback->isCallable()) {
				// Then we throw an exception to inform about the error:
				throw new M_Exception(sprintf(
					'Cannot call %s::%s() to retrieve a data object! Corrupted ' . 
					'callback definition?',
					$callback->getClassName(),
					$callback->getFunctionName()
				));
			}
			
			// Run the callback, and get the data object:
			$dataObject = $callback->run();
			
			// If we cannot find a data object, then we set the result in the 
			// local storage to NULL. This result will be the return value of
			// the function:
			if(! $dataObject) {
				$this->_valuesDataObject[$name] = NULL;
			}
			// If a data object has been retrieved:
			else {
				// We store in local cache:
				$this->_valuesDataObject[$name] = $dataObject;
			}
		}
		
		// Return the result:
		return $this->_valuesDataObject[$name];
	}
	
	/**
	 * Get Value
	 * 
	 * This method will return the current value of a variable. Note that it will
	 * use {@link M_Request::_isValueRetrievedFromSession()} to determine where
	 * to retrieve the value from.
	 * 
	 * @throws M_Exception
	 * @access protected
	 * @param string $name
	 * @param bool $refresh
	 * @return mixed
	 */
	protected function _getValue($name, $refresh = FALSE) {
		// If the requested variable name has not been defined
		if(! in_array($name, $this->_names)) {
			// We throw an exception to inform about the error
			throw new M_Exception(sprintf(
				'Cannot retrieve the value of "%s"; Has not been added to the ' . 
				'scope with %s::%s()!',
				$name,
				__CLASS__,
				'_setVariableNames'
			));
		}
		
		// If not requested before
		if(! array_key_exists($name, $this->_values) || $refresh) {
			// If to be retrieved from the session:
			if($this->_isValueRetrievedFromSession($refresh)) {
				// Then, we check if the value can be found in the namespace:
				$this->_values[$name] = $this->_getSessionNamespace()->get($name, NULL);
			}
			// If to be fetched from the query variables:
			else {
				// Then, we use the provided collection of query variables to 
				// set all values in the object:
				$this->setValuesFromRequest();
			}
		}
		
		// If no value can be retrieved:
		if(is_null($this->_values[$name])) {
			// Then, we check if we can find a default value:
			if(array_key_exists($name, $this->_defaultValues)) {
				// If so, we return that value instead:
				return $this->_defaultValues[$name];
			}
		}
		
		// Return the value:
		return $this->_values[$name];
	}
	
	/**
	 * Are values retrieved from the session?
	 * 
	 * This method will tell whether or not the values are retrieved from the 
	 * session. If the return value of this method is TRUE, then all values returned
	 * by {@link M_RequestPersistent::_getValue()} will be retrieved from the 
	 * session. If FALSE, then the values will be retrieved from the query variables
	 * of the current request.
	 * 
	 * @access protected
	 * @param bool $refresh
	 * @return bool
	 */
	protected function _isValueRetrievedFromSession($refresh = FALSE) {
		// If not determined before, or if the outcome of this function is to
		// be refreshed:
		if(is_null($this->_isValueRetrievedFromSession) || $refresh) {
			// The output value:
			$out = TRUE;
			
			// For each of the variable names in the class, and while the output
			// value indicates that values are retrieved from session:
			for($i = 0, $n = count($this->_names); $out && $i < $n; $i ++) {
				// If the current variable is provided in the query variables:
				$value = M_Request::getVariable($this->_names[$i]);
				if($value) {
					// Then, we indicate that the values are to be retrieved 
					// from the request variables instead of the session:
					$out = FALSE;
				}
			}
			
			// Set the result:
			$this->_isValueRetrievedFromSession = $out;
		}
		
		// Return the output:
		return $this->_isValueRetrievedFromSession;
	}
	
	/**
	 * Is Variable Name Of Valid Syntax?
	 * 
	 * This method will whether or not a given variable name is of a valid syntax, 
	 * and can therefore be used as name in the {@link M_Request::_setVariableNames()}
	 * method.
	 * 
	 * @access protected
	 * @param string $name
	 * @return boolean $flag
	 *		Returns TRUE if the variable name is valid, FALSE if not
	 */
	protected function _isVariableNameOfValidSyntax($name) {
		// Variable names must be strings:
		if(! is_string($name)) {
			// We return FALSE if that is not the case:
			return FALSE;
		}
		
		// And non-empty:
		$n = strlen($name);
		if($n == 0) {
			// We return FALSE if the string is empty
			return FALSE;
		}
		
		// Also, variable names must be in alphanumeric format. This is important
		// for them to be valid query variables. First, we make sure the variable
		// does not start with a number:
		if(is_numeric(substr($name, 0, 1))) {
			// If the name starts with a number, we return FALSE to indicate an
			// invalid variable name:
			return FALSE;
		}
		
		// Make sure the name is alphanumeric. For each of the characters in the
		// variable name:
		for($i = 0; $i < $n; $i ++) {
			// The current character must be a letter, a dash or an integer
			if(strpos('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_', $name[$i]) === FALSE) {
				// If that is not the case, we return FALSE to indicate an 
				// invalid variable name:
				return FALSE;
			}
		}
		
		// Return TRUE if still here. This means that we did not find any errors
		return TRUE;
	}
	
	/**
	 * Get Session Namespace
	 * 
	 * Will return the session namespace that is used in order to store data
	 * persistently. Note that this namespace will be unique per subclass of
	 * the abstract {@link M_RequestPersistent} class.
	 * 
	 * @access protected
	 * @return M_SessionNamespace
	 */
	protected function _getSessionNamespace() {
		// If not requested before:
		if(! $this->_sessionNamespace) {
			// Then, we construct the namespace now:
			$this->_sessionNamespace = new M_SessionNamespace('M-RP-' . $this->getClassName());
		}
		
		// Return the Session Namespace:
		return $this->_sessionNamespace;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set Variable Names
	 * 
	 * This method allows you to set the collection of variables names you are
	 * working with, in the subclass of {@link M_RequestPersistent}.
	 * 
	 * @throws M_Exception
	 * @access protected
	 * @param array $names
	 * @return M_RequestPersistent
	 */
	protected function _setVariableNames(array $names) {
		// Reset the names property of this object to an empty array to begin with:
		$this->_names = array();
		
		// For each of the names:
		foreach($names as $name) {
			// If the current variable name is not valid
			if(! $this->_isVariableNameOfValidSyntax($name)) {
				// Then we will throw an exception to inform about the error:
				throw new M_Exception(sprintf(
					'Cannot use the variable name %s; Invalid variable name!',
					var_export($name, TRUE)
				));
			}
			
			// Add the current variable name:
			array_push($this->_names, $name);
		}
		
		// We must have a non-empty collection of variable names now. We make
		// sure this is the case:
		if(count($this->_names) == 0) {
			// We throw an exception to inform about the error, if the collection
			// of variable names is empty
			throw new M_Exception(sprintf(
				'Cannot set variable names; Empty array of names?'
			));
		}
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set Value
	 * 
	 * @access protected
	 * @param string $name
	 * @param mixed $value
	 * @return M_RequestPersistent
	 */
	protected function _setValue($name, $value) {
		// Set the value:
		$this->_values[$name] = $defaultValue;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set Default Value
	 * 
	 * @access protected
	 * @param string $name
	 * @param mixed $defaultValue
	 * @return M_RequestPersistent
	 */
	protected function _setValueDefault($name, $defaultValue) {
		// Set the value:
		$this->_defaultValues[$name] = $defaultValue;
		
		// Return myself
		return $this;
	}
}