<?php
/**
 * M_JsHelper
 * 
 * This helper class is used in order to generate javascript code. For example,
 * this class may help you in exporting a string to a piece of javascript code,
 * or creating a json object, etc.
 * 
 * @package Core
 */
class M_JsHelper {
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get var declaration
	 * 
	 * Will render a 'var' declaration in javascript, provided a variable name
	 * and a value.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_JsHelper::getStringVariable('message', "Hello \r\n'world'!");
	 * </code>
	 * 
	 * Output
	 * <code>
	 *    var message = 'Hello \n\'world\'';
	 * </code>
	 * 
	 * @uses M_JsHelper::getValue()
	 * @static
	 * @access public
	 * @param string $variable
	 *		The name of the variable to be used in the javascript code
	 * @param mixed $value
	 *		The value to be converted into its javascript equivalent
	 * @param boolean $singleQuote
	 *		Set to TRUE if you want strings to be outputted with single quotes, 
	 *		FALSE if you wish double quotes to be used for enclosing of strings
	 * @return string
	 */
	public static function getVar($variable, $value, $singleQuote = TRUE) {
		// Return the final string:
		return 'var ' . self::_getVariableName($variable) . ' = ' . self::getValue($value) . ';';
	}
	
	/**
	 * Get value
	 * 
	 * Will export a given variable to its equivalent in javascript. The result 
	 * of this function can be used inline, and is not a declaration of a variable.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_JsHelper::getValue("Hello \r\n'world'!");
	 *    echo M_JsHelper::getValue(1.5);
	 *    echo M_JsHelper::getValue(array(1, 2, 'three'));
	 *    echo M_JsHelper::getValue(NULL);
	 *    echo M_JsHelper::getValue(TRUE);
	 *    echo M_JsHelper::getValue(new M_Date());
	 * </code>
	 * 
	 * Output
	 * <code>
	 *    'Hello \n\'world\'!'
	 *    1.5
	 *    {0 : 1, 1 : 2, 2 : 'three'}
	 *    null
	 *    true
	 *    new Date(year, month, day, hours, minutes, seconds, milliseconds)
	 * </code>
	 * 
	 * @uses M_JsHelper::getNumericValue()
	 * @uses M_JsHelper::getStringValue()
	 * @uses M_JsHelper::getArrayValue()
	 * @static
	 * @access public
	 * @param mixed $value
	 *		The value to be converted into its javascript equivalent
	 * @param boolean $singleQuote
	 *		Set to TRUE if you want strings to be outputted with single quotes, 
	 *		FALSE if you wish double quotes to be used for enclosing of strings
	 * @return string
	 */
	public static function getValue($value, $singleQuote = TRUE) {
		// If the provided value is numeric:
		if(is_numeric($value)) {
			// Then, we return the value unquoted:
			return self::getNumericValue($value);
		}
		// If the provided value is a string:
		elseif(is_string($value)) {
			// Then, we return the quoted string:
			return self::getStringValue($value, $singleQuote);
		}
		// If the provided value is an array
		elseif(is_array($value)) {
			// Then, we return an array in javascript:
			return self::getArrayValue($value, $singleQuote);
		}
		// If the provided value is NULL
		elseif(is_null($value)) {
			return 'null';
		}
		// If the provided value is NULL
		elseif(is_bool($value)) {
			return ($value ? 'true' : 'false');
		}
		// If the value is a Date Object:
		elseif(M_Helper::isInstanceOf($value, 'M_Date')) {
			return self::getDateValue($value);
		}
		// Any other value cannot be translated into javascript for now:
		else {
			// We throw an exception if we cannot translate the value:
			throw new M_JsException(sprintf(
				'Cannot export to javascript value: %s',
				var_export($value, TRUE)
			));
		}
	}
	
	/**
	 * Get numeric value
	 * 
	 * Will accept a number as a parameter, and will return the provided number
	 * as a javascript number.
	 * 
	 * @static
	 * @access public
	 * @param float $number
	 * @return string
	 */
	public static function getNumericValue($value) {
		return (string) $value;
	}
	
	/**
	 * Get string value
	 * 
	 * Will accept a string as a parameter, and will return the provided string 
	 * as a javascript string. The result of this method will be a string that has 
	 * already been enclosed with simple/double quotes.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_JsHelper::getStringVariable('message', "Hello \r\n'world'!");
	 * </code>
	 * 
	 * Output
	 * <code>
	 *    var message = 'Hello \n\'world\'';
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param string $value
	 *		The string to be converted into javascript format
	 * @param boolean $singleQuote
	 *		Set to TRUE if you want the javascript string to be created with 
	 *		single quotes, FALSE if you wish double quotes to be used
	 * @return string
	 */
	public static function getStringValue($value, $singleQuote = TRUE) {
		// First of all, we escape backslashes in the provided string.
		$value = str_replace('\\', '\\\\', $value);
		
		// We remove the hidden \r return character:
		$value = str_replace("\r", '', $value);
		
		// And, we replace the newline character by its escaped equivalent:
		$value = str_replace("\n", '\\n', $value);
		
		// We do the same for tabs:
		$value = str_replace("\t", '\\t', $value);
		
		// If single quotes are to be used:
		if($singleQuote) {
			// Then, we escape the string for single quotes:
			$value = str_replace('\'', '\\\'', $value);
			
			// We prepare a temporary variable, which will be used in the final
			// javascript statement to enclose the string:
			$quote = '\'';
		}
		// If double quotes are to be used:
		else {
			// Then, we escape the double quotes:
			$value = str_replace('"', '\\"', $value);
			
			// We prepare a temporary variable, which will be used in the final
			// javascript statement to enclose the string:
			$quote = '"';
		}
		
		// Finally, return the string:
		return $quote . $value . $quote;
	}
	
	/**
	 * Get array value
	 * 
	 * Will accept an array as a parameter, and will return the provided array 
	 * as a javascript array.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_JsHelper::getArrayValue(array(1, 2, 3));
	 * </code>
	 * 
	 * Associative arrays in JavaScript, which act as hash tables, have nothing 
	 * to do with the built-in Array object. They simply rely on the fact that 
	 * object.property is the same as object["property"]. This means that the 
	 * length property is not used, nor do any Array methods (such as join) do 
	 * anything. 
	 * 
	 * In fact, it is better to create the associative array using the generic 
	 * Object() constructor. In conclusion, an array will be converted to a generic
	 * object construct. Continuing on our example, we would get the following 
	 * output:
	 * 
	 * <code>
	 *    {0 : 1, 1 : 2, 2 : 3}
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param array $value
	 *		The array to be converted into javascript format
	 * @param boolean $singleQuote
	 *		Set to TRUE if you want strings to be outputted with single quotes, 
	 *		FALSE if you wish double quotes to be used for enclosing of strings
	 * @return string
	 */
	public function getArrayValue(array $value, $singleQuote = TRUE) {
		// The output variable:
		$out = '{';
		
		// Temporary working variables:
		$n = count($value) - 1;
		$i = 0;
		
		// For each of the elements in the array:
		foreach($value as $key => $element) {
			// Add the key and the value to the array:
			$out .= self::getStringValue($key, FALSE) . ':' . self::getValue($element, FALSE);
			
			// If this is not the last element in the array:
			if($i ++ < $n) {
				// Then, we add a comma to prepare for the next item 
				// in the array:
				$out .= ',';
			}
		}
		
		// Finish the array:
		$out .= '}';
		
		// Return the array:
		return $out;
	}
	
	/**
	 * Get date
	 * 
	 * Will accept an instance of {@link M_Date} as a parameter, and will return 
	 * the provided date object as a date object in javascript.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_JsHelper::getDateValue(new M_Date('2013/02/25 12:00:00'));
	 * </code>
	 * 
	 * Will result in...
	 * 
	 * <code>
	 *    new Date(year, month, day, hours, minutes, seconds, milliseconds)
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param M_Date $date
	 * @return string
	 */
	public static function getDateValue(M_Date $date) {
		$out  = '';
		$out .= 'new Date(';
		$out .= $date->getYear() . ', ';
		$out .= $date->getMonth() .', ';
		$out .= $date->getDay() .', ';
		$out .= $date->getHour() .', ';
		$out .= $date->getMinutes() .', ';
		$out .= $date->getSeconds().', ';
		$out .= '0)';
		return $out;
	}
	
	/**
	 * Get javascript string
	 * 
	 * Will accept a string as a parameter, and will return the string as a 
	 * javascript string. The result of this method will be a string that has 
	 * already been enclosed with simple quotes.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_JsHelper::getStringVariable('message', "Hello \r\n'world'!");
	 * </code>
	 * 
	 * Output
	 * <code>
	 *    var message = 'Hello \n\'world\'';
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param string $variable
	 *		The name of the variable to be used in the javascript code
	 * @param string $value
	 *		The string to be converted into javascript format
	 * @param boolean $singleQuote
	 *		Set to TRUE if you want the javascript string to be created with 
	 *		single quotes, FALSE if you wish double quotes to be used
	 * @return string $javascriptString
	 */
	public static function getVariable($variable, $value, $singleQuote = TRUE) {
		// Return the final string:
		return 'var ' . self::_getVariableName($variable) . ' = ' . $quote . $value . $quote . ';';
	}
	
	/**
	 * Get new line(s)
	 * 
	 * @static
	 * @access public
	 * @param integer $number
	 *		The number of new lines to be returned by this function
	 * @return string
	 */
	public static function getNewLine($number = 1) {
		return str_repeat("\n", $number);
	}
	
	/**
	 * Get tab(s)
	 * 
	 * @static
	 * @access public
	 * @param integer $number
	 *		The number of tabs to be returned by this function
	 * @return string
	 */
	public static function getTab($number = 1) {
		return str_repeat("\t", $number);
	}
	
	/**
	 * Get new line(s) and tab(s)
	 * 
	 * @static
	 * @access public
	 * @param integer $numberOfLines
	 *		The number of new lines to be returned by this function
	 * @param integer $numberOfTabs
	 *		The number of tabs to be returned by this function
	 * @return string
	 */
	public static function getNewLineWithTab($numberOfLines = 1, $numberOfTabs = 1) {
		return str_repeat("\n" . str_repeat("\t", $numberOfTabs), $numberOfLines);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get variable name
	 * 
	 * Will accept a string, and will parse that string into a variable name. 
	 * Also, this function will check if the variable name contains forbidden 
	 * characters.
	 * 
	 * @static
	 * @access protected
	 * @param string $variable
	 * @return string
	 */
	protected static function _getVariableName($variable) {
		// Trim the variable name of redundant whitespaces:
		$variable = trim($variable);
		
		// For each character in the variable name:
		for($i = 0, $n = strlen($variable); $i < $n; $i ++) {
			// If this is the first character in the variable name:
			if($i == 0) {
				// Then, we do an additional check. Variable names cannot start
				// with numerical characters. If starting with a number though
				if(strpos(' 0123456789', $variable[$i])) {
					// We throw an exception to inform about the error:
					throw new M_JsException(sprintf(
						'Cannot parse javascript variable name %s; A variable ' . 
						'name cannot start with a number!',
						$variable,
						$variable[$i]
					));
				}
			}
			
			// Check if the current character is forbidden in a variable name:
			if(strpos(' ,;:/+-=.?*`\'"&#@"\\()!', $variable[$i]) !== FALSE) {
				// If so, we throw an exception to inform about the error:
				throw new M_JsException(sprintf(
					'Cannot parse javascript variable name %s; Containing the ' . 
					'forbidden character "%s"',
					$variable,
					$variable[$i]
				));
			}
		}
		
		// Return the variable name:
		return $variable;
	}
}