<?php
/**
 * M_AuthAttemptDataObjectProfile
 * 
 * @package Core
 */
class M_AuthAttemptDataObjectProfile extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Data Object Mapper
	 * 
	 * This property stores the {@link M_DataObjectMapper} that is in charge of
	 * retrieving the data object from the database
	 * 
	 * @access private
	 * @var M_DataObjectMapper
	 */
	private $_mapper;
	
	/**
	 * Identity field
	 * 
	 * This property stores the field, in the definition of the data object, 
	 * where the identities - typically the usernames - are stored.
	 * 
	 * @access private
	 * @var string
	 */
	private $_identityField;
	
	/**
	 * Password field
	 * 
	 * This property stores the field, in the definition of the data object, 
	 * where passwords are stored.
	 * 
	 * @access private
	 * @var string
	 */
	private $_passwordField;
	
	/**
	 * Identity
	 * 
	 * This property stores the identity, with which the data object is to be
	 * looked up in the database. Typically, this is a username, an email address,
	 * etc.
	 * 
	 * @access private
	 * @var string
	 */
	private $_identity;
	
	/**
	 * Candidate (Data object)
	 * 
	 * This property stores the candidate, a data object provided by the mapper,
	 * that can be used for authentication.
	 * 
	 * @access private
	 * @var M_DataObject
	 */
	private $_dataObject;
	
	/**
	 * Number of Candidates
	 * 
	 * @access private
	 * @var integer
	 */
	private $_dataObjectCount;
	
	/* -- SETTERS -- */
	
	/**
	 * Set Data Object Mapper
	 * 
	 * This method allows to set the {@link M_DataObjectMapper} that is in charge 
	 * of retrieving the data object from the database.
	 * 
	 * @access public
	 * @param M_DataObjectMapper $mapper
	 * @return M_AuthAttemptDataObjectProfile $profile
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDataObjectMapper(M_DataObjectMapper $mapper) {
		// Set the data object mapper:
		$this->_mapper = $mapper;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set Identity field
	 * 
	 * This method allows to set the field, in the definition of the data object, 
	 * where the identities - typically the usernames - are stored.
	 * 
	 * @access public
	 * @param string $field
	 * @return M_AuthAttemptDataObjectProfile $profile
	 *		Returns itself, for a fluent programming interface
	 */
	public function setIdentityField($field) {
		// Set the field:
		$this->_identityField = (string) $field;
		
		// Return myself:
		return $this;
	}
	
	/**
	 * Set Password field
	 * 
	 * This method allows to set the field, in the definition of the data object, 
	 * where passwords are stored.
	 * 
	 * @access public
	 * @param string $field
	 * @return M_AuthAttemptDataObjectProfile $profile
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPasswordField($field) {
		// Set the field:
		$this->_passwordField = (string) $field;
		
		// Return myself:
		return $this;
	}
	
	/**
	 * Set Identity
	 * 
	 * This method allows to set the identity, with which the data object is to be
	 * looked up in the database. Typically, this is a username, an email address,
	 * etc.
	 * 
	 * @access public
	 * @param string $id
	 * @return M_AuthAttemptDataObjectProfile $profile
	 *		Returns itself, for a fluent programming interface
	 */
	public function setIdentity($id) {
		// Set the identity
		$this->_identity = (string) $id;
		
		// Return myself:
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Candidate (Data object)
	 * 
	 * This method allows to get the candidate, a data object provided by the 
	 * mapper, that may apply for authentication. Note however that, if no candidate
	 * can be found, this method will return NULL instead.
	 * 
	 * @access public
	 * @param bool $refresh
	 *		By default, the result of this method is cache'd for performance. If
	 *		you want to force a refresh of the result, you should provide (boolean)
	 *		TRUE as an argument to this method.
	 * @return M_DataObject
	 */
	public function getCandidate($refresh = FALSE) {
		// If not requested before, or if a refresh has been requested:
		if(is_null($this->_dataObject) || $refresh) {
			// Get the data object mapper with filters:
			$mapper = $this->_getDataObjectMapperWithFilters();
			
			// Ask the mapper for the candidates that apply for authentication:
			$candidates = $mapper->getAll();
			
			// Authentication allows only ONE candidate. If more than one candidate
			// is found in an attempt to authenticate, then the result is ambiguous,
			// causing authentication to fail. So, we make sure exactly one entry
			// is found with the mapper. If that is not the case:
			if(count($candidates) != 1) {
				// Then, we'll return NULL as the final outcome. To prevent this
				// from running again (cache), we store FALSE in the property:
				$this->_dataObject = FALSE;
				
				// Get the number of candidates
				$this->_dataObjectCount = $mapper->getCount();
			}
			// If exactly one candidate is found:
			else {
				// Get the data object:
				$this->_dataObject = $candidates->current();
				
				// Set the number of candidates
				$this->_dataObjectCount = 1;
			}
		}
		
		// If the candidate is found:
		if($this->_dataObject) {
			// Then, return the data object:
			return $this->_dataObject;
		}
		
		// Return NULL if still here:
		return NULL;
	}
	
	/**
	 * Get password stored for candidate
	 * 
	 * @access public
	 * @param bool $refresh
	 *		By default, the result of this method is cache'd for performance. If
	 *		you want to force a refresh of the result, you should provide (boolean)
	 *		TRUE as an argument to this method.
	 * @return string
	 */
	public function getCandidatePasswordStored($refresh = FALSE) {
		// A password field should have been defined. If that is not the case:
		if(! $this->_passwordField) {
			// Then, we throw an exception to inform about the error!
			throw new M_AuthException(
				'Cannot lookup password for Authentication Candidate; missing ' . 
				'password field!'
			);
		}
		
		// Get the candidate:
		$candidate = $this->getCandidate($refresh);
		
		// If the candidate cannot be found:
		if(! $candidate) {
			// Then, we return NULL
			return NULL;
		}
		
		// Get the password from the data object:
		return $candidate->__get($this->_passwordField);
	}
	
	/**
	 * Get Number of Candidates
	 * 
	 * This method allows to get the number of candidates. The candidates are
	 * data objects for which the value of the identify field matches with the 
	 * identity provided to the profile.
	 * 
	 * @access public
	 * @param bool $refresh
	 *		By default, the result of this method is cache'd for performance. If
	 *		you want to force a refresh of the result, you should provide (boolean)
	 *		TRUE as an argument to this method.
	 * @return integer
	 */
	public function getNumberOfCandidates($refresh = FALSE) {
		// Get the candidate:
		$this->getCandidate($refresh);
		
		// Return the count:
		return $this->_dataObjectCount;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Data Object Mapper With Filters
	 * 
	 * @access protected
	 * @return M_DataObjectMapper
	 */
	protected function _getDataObjectMapperWithFilters() {
		// A data object mapper should have been defined. If that is not the 
		// case:
		if(! $this->_mapper) {
			// Then, we throw an exception to inform about the error!
			throw new M_AuthException(
				'Cannot lookup data object for authentication; missing instance ' .
				'of M_DataObjectMapper'
			);
		}
		
		// Also, we make sure the required details about the identity have been
		// provided. If not so:
		if(! $this->_identityField || ! $this->_identity) {
			// Then, we throw an exception to inform about the error!
			throw new M_AuthException(
				'Cannot lookup data object for authentication; missing ' .
				'identity field and/or identity!'
			);
		}
		
		// By now, we know that we have everything we need. With the information
		// at our disposal, we construct a filter and add it to a clone of the
		// original data object mapper:
		$mapper = clone($this->_mapper);
		$mapper->addFilter(new M_DbQueryFilterWhere($this->_identityField, $this->_identity));

		// Return the mapper:
		return $mapper;
	}
}
