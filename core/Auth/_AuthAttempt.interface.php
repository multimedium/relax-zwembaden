<?php
/**
 * MI_AuthAttempt interface
 * 
 * The MI_AuthAttempt interface describes the public API that must be implemented
 * by Authentication Attempts. An Authentication Attempt is a specific adapter, 
 * that is in charge of looking up the identity of a person/entity.
 * 
 * The only method that is defined in the MI_AuthAttempt interface, is the 
 * {@link MI_AuthAttempt::authenticate()} method. Other authentication properties
 * (such as username, password) should have been set prior to calling the 
 * authenticate() method.
 * 
 * @package Core
 */
interface MI_AuthAttempt {
	/**
	 * Authenticate
	 * 
	 * Will look up the identity of the person/entity to be authenticated.
	 * 
	 * @access public
	 * @return MI_AuthResult
	 */
	public function authenticate();
}