<?php
/**
 * MI_AuthStorage interface
 * 
 * MI_AuthStorage dictates the public API that must be implemented by authentication
 * storage. Authentication Storage is used to maintain an authenticated session
 * alive, so identity credentials should be repeated with each page request.
 * 
 * {@link MI_AuthStorage} is responsible of storing the authenticated session, 
 * and the identity of the person/entity that has been authenticated.
 * 
 * @package Core
 */
interface MI_AuthStorage {
	/**
	 * Set identity
	 * 
	 * Is used to set the identity of the authenticated person/entity in the 
	 * stored session. The identity is the result of {@link MI_AuthResult::getIdentity()}.
	 * 
	 * @see MI_AuthResult::getIdentity()
	 * @access public
	 * @param mixed $identity
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setIdentity($identity);
	
	/**
	 * Set IP Address
	 * 
	 * Is used to set the IP Address of the authenticated person/entity
	 * 
	 * @access public
	 * @param string $ip
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setIp($ip);
	
	/**
	 * Set lifetime
	 * 
	 * Will set the lifetime of the stored session. If the session is not refreshed,
	 * see {@link MI_AuthStorage::refresh()}, within the lifetime span of the session,
	 * it will be destroyed automatically.
	 * 
	 * @access public
	 * @param integer $length
	 * 		The lifetime, expressed in a number of seconds
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setLifetime($length);
	
	/**
	 * Set context
	 * 
	 * The application may create different environments, in which authenticated
	 * sessions may live independently. 
	 * 
	 * A typical example would be a website in which clients can login to place 
	 * new orders, while there are also administrators that can login to the CMS.
	 * Both sessions need to be authenticated, but they live in a very different
	 * context. If authenticated in one context, it does not necessarily mean that 
	 * the session is also authenticated in the other context(s).
	 * 
	 * In the same example of the website: if a client has logged into the website,
	 * to place a new order, it does not mean that this client has also been
	 * authenticated for the CMS. This is a completely different context.
	 * 
	 * In order to set the context for the authenticated session, you need to 
	 * specify the context to its storage. The storage will determine whether or
	 * not the session has been authenticated in a given context.
	 * 
	 * @access public
	 * @param string $context
	 * 		The context can be defined by any string that describes the context
	 * 		or environment in which the session has been authenticated and stored
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setContext($context);
	
	/**
	 * Set Storage ID
	 * 
	 * This method allows to set the ID of the storage object. Note that, 
	 * typically, this ID is the Session ID.
	 * 
	 * @access public
	 * @see MI_AuthResult::setStorageId()
	 * @access public
	 * @param string $id
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setStorageId($id);
	
	/**
	 * Get by Storage ID
	 * 
	 * Will lookup an instance of {@link MI_AuthStorage}, by a given Storage ID.
	 * 
	 * @access public
	 * @param mixed $id
	 * 		The Storage ID, with which to lookup the storage
	 * @return MI_AuthStorage
	 */
	public static function getByStorageId($id);
	
	/**
	 * Get Storage ID
	 * 
	 * Will return the Unique ID with which the stored session can be identified.
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getStorageId();
	
	/**
	 * Get identity
	 * 
	 * Will return the identity of the authenticated person/entity that has been 
	 * stored in the session.
	 * 
	 * @see MI_AuthStorage::setIdentity()
	 * @access public
	 * @return mixed
	 */
	public function getIdentity();
	
	/**
	 * Get start time
	 * 
	 * Will return the date + time when the stored session has been started.
	 * The result of this method is an instance of {@link M_Date}.
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getStartTime();
	
	/**
	 * Get refresh time
	 * 
	 * Will return the date + time when the stored session has been refreshed for
	 * the last time. The result of this method is an instance of {@link M_Date}.
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getRefreshTime();
	
	/**
	 * Get lifetime
	 * 
	 * Will return the lifetime of the stored session, set previously with
	 * {@link MI_AuthStorage::setLifetime()}
	 * 
	 * @access public
	 * @return integer $length
	 * 		The lifetime, expressed in a number of seconds
	 */
	public function getLifetime();
	
	/**
	 * Get context
	 * 
	 * For more information about the context of authenticated sessions, please
	 * read the docs on {@link MI_AuthStorage::setContext()}
	 * 
	 * @access public
	 * @return string $context
	 * 		The context in which the (authenticated) session is being stored.
	 */
	public function getContext();
	
	/**
	 * Get IP Address
	 * 
	 * Provides with the IP Address of the authenticated person/entity
	 * 
	 * @access public
	 * @return string
	 */
	public function getIp();
	
	/**
	 * Refresh the stored session
	 * 
	 * In order to maintain the authenticated session alive, we need to refresh 
	 * it in the session storage. This method does exactly that.
	 * 
	 * By refreshing a stored session, you update the result of 
	 * {@link MI_AuthStorage::getRefreshTime()}.
	 * 
	 * @see MI_AuthStorage::getRefreshTime()
	 * @see MI_AuthStorage::setLifetime()
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function refresh();
	
	/**
	 * Is expired?
	 * 
	 * Will tell whether or not the session in storage has expired. A session in
	 * storage has a given lifetime, defined by {@link MI_AuthStorage::setLifetime()},
	 * and expires when inactive for longer than allowed by that lifetime.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if expired, FALSE if not
	 */
	public function isExpired();
	
	/**
	 * Destroy expired sessions
	 * 
	 * Will remove the authenticated sessions that have expired from the storage.
	 * An expired session is a session that has been inactive (not refreshed; see
	 * {@link MI_AuthStorage::refresh()}) for longer than is allowed by the
	 * defined lifetime.
	 * 
	 * @see MI_AuthStorage::refresh()
	 * @see MI_AuthStorage::setLifetime()
	 * @see MI_AuthStorage::isExpired()
	 * @access public
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function destroyExpired();
	
	/**
	 * Destroy the session
	 * 
	 * Will remove the authenticated session that is being stored by the instance 
	 * of {@link MI_AuthStorage}.
	 * 
	 * @access public
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function destroy();
}