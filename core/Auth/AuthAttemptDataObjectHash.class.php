<?php
/**
 * M_AuthAttemptDataObjectHash
 * 
 * @author Tim
 * @package Core
 */
class M_AuthAttemptDataObjectHash implements MI_AuthAttempt {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Hash
	 * 
	 * @see M_AuthAttemptDataObjectHash::setHash()
	 * @see M_AuthAttemptDataObjectHash::getHash()
	 * @access private
	 * @var MI_Hash
	 */
	private $_hash;
	
	/**
	 * Data Object Profile
	 * 
	 * @see M_AuthAttemptDataObjectHash::setProfile()
	 * @access private
	 * @var M_AuthAttemptDataObjectProfile
	 */
	private $_profile;
	
	/* -- SETTERS -- */
	
	/**
	 * Set Hash
	 * 
	 * @access public
	 * @param MI_Hash $Hash
	 * @return M_AuthAttemptDataObjectHash
	 *		Returns itself, for a fluent programming interface
	 */
	public function setHash(MI_Hash $hash) {
		// Set the property
		$this->_hash = $hash;
		
		// Return self
		return $this;
	}
	
	/**
	 * Set Data Object Profile
	 * 
	 * @access public
	 * @param M_AuthAttemptDataObjectProfile $profile
	 * @return M_AuthAttemptDataObjectHash
	 *		Returns itself, for a fluent programming interface
	 */
	public function setProfile(M_AuthAttemptDataObjectProfile $profile) {
		// Set the property
		$this->_profile = $profile;
		
		// Return self
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get profile
	 * 
	 * @return M_AuthAttemptDataObjectProfile
	 */
	public function getProfile() {
		return $this->_profile;
	}
	
	/**
	 * Get Hash
	 * 
	 * @see M_AuthAttemptDataObjectHash::setHash()
	 * @access public
	 * @return MI_Hash
	 */
	public function getHash() {
		return $this->_hash;
	}
	
	/**
	 * Authenticate
	 * 
	 * @see MI_AuthAttempt::authenticate()
	 * @access public
	 * @return MI_AuthResult
	 */
	public function authenticate() {
		// Both an instance of MI_Hash and M_AuthAttemptDataObjectProfile should
		// be defined in the attempt. If not so:
		if(! $this->_hash || ! $this->_profile) {
			// Throw an exception to inform about the error:
			throw new M_AuthException(
				'Cannot perform attempt to authentication! Missing MI_Hash or ' . 
				'or M_AuthAttemptDataObjectProfile instance!'
			);
		}
		
		// Ask the profile for a possible candidate:
		$candidate = $this->_profile->getCandidate();
		
		// If no such candidate can be found:
		if(! $candidate) {
			// Then, authentication has failed! The reason why it has failed,
			// depends on the number of possible candidates though. If we have
			// found more than one candidate:
			if($this->_profile->getNumberOfCandidates() > 1) {
				// Then we have failed: ambiguous result!
				return new M_AuthResult(FALSE, MI_AuthResult::FAILURE_IDENTITY_AMBIGUOUS);
			}
			// If we did not find any candidate:
			else {
				// Then we have failed: no data object found!
				return new M_AuthResult(FALSE, MI_AuthResult::FAILURE_IDENTITY_NOT_FOUND);
			}
		}
		
		// If a candidate has been found, then we will need to compare the 
		// passwords. So, first of all, we get the password that is stored for 
		// the data object:
		$password = $this->_profile->getCandidatePasswordStored();
		
		// Then, we compute the hash for the candidate. First of all, we get the
		// SALT to be used for the candidate, and we pass along this SALT to the
		// hashing object:
		$this->_hash->setSalt(M_AuthAttemptDataObjectHelper::getSaltForDataObject($candidate));
		
		// Now, compare hashes:
		if($password != $this->_hash->getHash()) {
			// If they do not match, we return FAILURE!
			return new M_AuthResult(FALSE, MI_AuthResult::FAILURE_PASSWORD_INVALID);
		}
		
		// If we are still here, we'll return SUCCESS! This means that the hashes
		// match and that authentication has succeeded:
		$rs = new M_AuthResult(TRUE, MI_AuthResult::SUCCESS);
		$rs->setIdentity($candidate->getId());
		return $rs;
	}
}