<?php
/**
 * M_AuthException class
 * 
 * Exceptions thrown by the implementation(s) of the {@link M_Auth} API
 * 
 * @package Core
 */
class M_AuthException extends M_Exception {
}