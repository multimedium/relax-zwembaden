<?php
/**
 * M_AuthStorageDb class
 * 
 * M_AuthStorageDb is a specific implementation of {@link MI_AuthStorage}, that
 * stores authenticated sessions in the database.
 * 
 * @package Core
 */
class M_AuthStorageDb extends M_AuthStorage implements MI_AuthStorage {
	/**
	 * Storage ID
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_storageId;
	
	/**
	 * Flag: is stored in database?
	 * 
	 * This property stores a (boolean) flag, that tells us whether or not the
	 * session has already been stored in the database.
	 * 
	 * @access protected
	 * @var bool
	 */
	protected $_isInDb;
	
	/**
	 * Identity
	 * 
	 * This property stores the identity of the authenticated person/entity.
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_identity;
	
	/**
	 * IP Address
	 * 
	 * This property stores the IP Address of the authenticated person/entity
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_ip;
	
	/**
	 * Start time
	 * 
	 * This property stores the start time of the session in storage.
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_startTime;
	
	/**
	 * Refresh time
	 * 
	 * This property stores the refresh time of the session in storage.
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_refreshTime;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $storageId
	 * 		The Storage ID, with which to store the session. Typically, this ID
	 * 		is the Session ID
	 * @param string $context
	 * 		The context in which to store the session. For more info, read the
	 * 		docs on {@link MI_AuthStorage::setContext()}
	 * @return M_AuthStorageDb
	 */
	public function __construct($storageId = NULL, $context = NULL) {
		// Set context?
		if($context) {
			$this->setContext($context);
		}
		
		// By default, we set the internal flag to FALSE, meaning that the 
		// storage object is not yet stored:
		$this->_isInDb = FALSE;
		
		// If Storage ID has been provided
		if($storageId) {
			// Set internal storage id
			$this->_storageId = (string) $storageId;
			
			// Set default values:
			$this->_startTime   = time();
			$this->_refreshTime = time();
			$this->_ip          = M_Client::getIp();
			
			// (Try to) fetch from the database:
			try {
				// Compose the SQL Query, and run it:
				$rs = $this
					->_getDbTable()
					->select()
					->where('session_id = ?', $this->_storageId)
					->where('session_refresh_time > ?', time() - $this->getLifetime())
					->execute();
				
				// If the result set is valid:
				if($rs && count($rs) == 1) {
					// We set internal flag, so we know that the authenticated 
					// session is already stored in the database:
					$this->_isInDb = TRUE;
					
					// Get the record from the result set:
					$temp = $rs->current();
					
					// Set internals:
					$this->_identity    = $temp['session_identity'];
					$this->_ip          = $temp['session_ip'];
					$this->_startTime   = (int) $temp['session_start_time'];
					$this->_refreshTime = (int) $temp['session_refresh_time'];
				}
			}
			// If the query failed, we assume that the database table has not
			// yet been installed in the application. We install the table now:
			catch(Exception $e) {
				$this->_installDbTable();
			}
			
			// If no authenticated session could have been found, with the
			// given Storage ID, we initiate one now:
			if(! $this->_isInDb) {
				// Insert a new blank session
				$this->destroy();
				$this->_getDbTable()->insert(array(
					'session_id'           => $this->_storageId,
					'session_start_time'   => (int) $this->_startTime,
					'session_refresh_time' => (int) $this->_refreshTime,
					'session_ip'           => $this->_ip
				));
			}
		}
	}
	
	/**
	 * Destructor
	 * 
	 * In the case of {@link M_AuthStorageDb}, the destructor saves the changes
	 * that have been made to the storage instance to the database.
	 * 
	 * @access public
	 * @return void
	 */
	public function __destruct() {
		// We now store the data directly in the database, whenever a setter
		// is called. This is done like this, because this __destruct() method
		// seems to have been causing a "503 Temporarily unavailable" error
		// on the page!!
		
		/* // If not yet stored:
		if(! $this->_isInDb) {
			// Insert a new record:
			$this->_getDbTable()->insert(array(
				'session_id'           => $this->_storageId,
				'session_identity'     => $this->_identity,
				'session_start_time'   => (int) $this->_startTime,
				'session_refresh_time' => (int) $this->_refreshTime,
				'session_ip'           => $this->_ip
			));
		}
		// If already stored:
		else {
			// Update:
			$this->_getDbTable()->update(array(
					'session_identity'     => $this->_identity,
					'session_start_time'   => (int) $this->_startTime,
					'session_refresh_time' => (int) $this->_refreshTime,
					'session_ip'           => $this->_ip
				),
				'session_id = ?',
				$this->_storageId
			);
		} */
	}
	
	/* -- SUPPORT FOR MI_AuthStorage -- */
	
	/**
	 * Set Storage ID
	 * 
	 * @see MI_AuthResult::setStorageId()
	 * @access public
	 * @param string $id
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setStorageId($id) {
		// Cast to string, just in case:
		$id = (string) $id;
		
		// Update the entry in the database, with the Storage ID
		$this->_getDbTable()->update(array(
				// The Storage ID is the Session ID:
				'session_id' => $id
			),
			// Where Session ID is...
			'session_id = ?',
			// The previous Storage ID:
			$this->_storageId
		);
		
		// Set the new Storage ID:
		$this->_storageId = $id;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set identity
	 * 
	 * @see MI_AuthResult::setIdentity()
	 * @access public
	 * @param mixed $identity
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setIdentity($identity) {
		$this->_identity = (string) $identity;
		$this->_getDbTable()->update(array(
				'session_identity'     => $this->_identity
			),
			'session_id = ?',
			$this->_storageId
		);
		return $this;
	}
	
	/**
	 * Set IP Address
	 * 
	 * @see MI_AuthStorage::setIp()
	 * @access public
	 * @param string $ip
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setIp($ip) {
		$this->_ip = (string) $ip;
		$this->_getDbTable()->update(array(
				'session_ip'     => $this->_ip
			),
			'session_id = ?',
			$this->_storageId
		);
	}
	
	/**
	 * Get by Storage ID
	 * 
	 * @see MI_AuthStorage::getByStorageId()
	 * @access public
	 * @param mixed $id
	 * 		The Storage ID, with which to lookup the storage
	 * @return MI_AuthStorage
	 */
	public static function getByStorageId($id) {
		return new self($id);
	}
	
	/**
	 * Get Storage ID
	 * 
	 * @see MI_AuthStorage::getStorageId()
	 * @access public
	 * @return mixed
	 */
	public function getStorageId() {
		return $this->_storageId;
	}
	
	/**
	 * Get identity
	 * 
	 * @see MI_AuthStorage::getIdentity()
	 * @see MI_AuthStorage::setIdentity()
	 * @access public
	 * @return mixed
	 */
	public function getIdentity() {
		return $this->_identity;
	}
	
	/**
	 * Get start time
	 * 
	 * @see MI_AuthStorage::getStartTime()
	 * @access public
	 * @return M_Date
	 */
	public function getStartTime() {
		return new M_Date($this->_startTime);
	}
	
	/**
	 * Get refresh time
	 * 
	 * @see MI_AuthStorage::getRefreshTime()
	 * @access public
	 * @return M_Date
	 */
	public function getRefreshTime() {
		return new M_Date($this->_refreshTime);
	}
	
	/**
	 * Get IP Address
	 * 
	 * @see MI_AuthStorage::getIp()
	 * @access public
	 * @return string
	 */
	public function getIp() {
		return $this->_ip;
	}
	
	/**
	 * Refresh the stored session
	 * 
	 * @see MI_AuthStorage::refresh()
	 * @see MI_AuthStorage::getRefreshTime()
	 * @see MI_AuthStorage::setLifetime()
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function refresh() {
		$this->_refreshTime = time();
		return $this->_getDbTable()->update(array(
				'session_refresh_time'     => $this->_refreshTime
			),
			'session_id = ?',
			$this->_storageId
		);
	}
	
	/**
	 * Destroy expired sessions
	 * 
	 * @see MI_AuthStorage::destroyExpired()
	 * @see MI_AuthStorage::refresh()
	 * @see MI_AuthStorage::setLifetime()
	 * @see MI_AuthStorage::isExpired()
	 * @access public
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function destroyExpired() {
		$this->_getDbTable()->delete('session_refresh_time < ?', (time() - $this->getLifetime()));
		return $this;
	}
	
	/**
	 * Destroy the session
	 * 
	 * @see MI_AuthStorage::destroy()
	 * @access public
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function destroy() {
		$this->_getDbTable()->delete('session_id = ?', $this->_storageId);
	}
	
	/* -- ADDITIONAL TO MI_AuthStorage -- */
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get available contexts
	 * 
	 * This function will return the complete collection of contexts that have
	 * been created so far with {@link M_AuthStorageDb}. The result is an array
	 * with context strings.
	 * 
	 * @access public
	 * @return array
	 */
	public static function getContextsInstalled() {
		// Output variable:
		$out = array();
		
		// In order to get all contexts available in the database, we ask the
		// database for the defined tables:
		foreach(M_Db::getInstance()->getTables() as $table) {
			/* @var $table M_DbTable */
			// We check whether or not the current table is for storage of auth
			// sessions. We know that is the case if the table name starts with
			// "auth_storage":
			if(! strncmp('auth_session', $table->getName(), 12)) {
				// Add the current context to the output:
				array_push($out, substr($table->getName(), 13));
			}
		}
		
		// Return the contexts:
		return $out;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get database connection
	 * 
	 * @access protected
	 * @return MI_Db
	 */
	protected function _getDb() {
		return M_Db::getInstance();
	}
	
	/**
	 * Get database table
	 * 
	 * @access protected
	 * @return MI_DbTable
	 */
	protected function _getDbTable() {
		return $this
			->_getDb()
			->getTable(
				$this->_getDbTableName()
			);
	}
	
	/**
	 * Get database table name
	 * 
	 * @access protected
	 * @return string $tableName
	 * 		The name of the database table
	 */
	protected function _getDbTableName() {
		$c = $this->getContext();
		if(! $c) {
			return 'auth_session';
		} else {
			return 'auth_session_' . M_Helper::trimCharlist($c, '_');
		}
	}
	
	/**
	 * Install database table (first run)
	 * 
	 * When {@link M_AuthStorageDb} is being used for the first time, the database
	 * table is installed automatically. This method is in charge of installing
	 * the database table.
	 * 
	 * @access protected
	 * @return boolean $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	protected function _installDbTable() {
		// Ask the database connection for a new table:
		$table = $this->_getDb()->getNewTable();
		$table->setName($this->_getDbTableName());
		
		// Create the fields in the new table:
		$id = new M_DbColumn('session_id');
		$id->setType(M_DbColumn::TYPE_VARCHAR);
		$id->setLength(32);
		$table->addColumn($id);
		
		$identity = new M_DbColumn('session_identity');
		$identity->setType(M_DbColumn::TYPE_VARCHAR);
		$identity->setLength(32);
		$table->addColumn($identity);
		
		$start = new M_DbColumn('session_start_time');
		$start->setType(M_DbColumn::TYPE_INTEGER);
		$table->addColumn($start);
		
		$refresh = new M_DbColumn('session_refresh_time');
		$refresh->setType(M_DbColumn::TYPE_INTEGER);
		$table->addColumn($refresh);
		
		$ip = new M_DbColumn('session_ip');
		$ip->setType(M_DbColumn::TYPE_VARCHAR);
		$ip->setLength(32);
		$table->addColumn($ip);
		
		// Create the primary key of the new table:
		$primary = new M_DbIndex('PRIMARY');
		$primary->setType(M_DbIndex::PRIMARY);
		$primary->addColumn($id);
		$table->setPrimaryKey($primary);
		
		// Set table engine:
		$table->setEngine(M_DbTable::ENGINE_MYSQL_INNODB);
		
		// Set some comments
		$table->setComments('This table stores authenticated sessions. See ' . __CLASS__ . ' for more info.');
		
		// Create the table:
		return $table->create();
	}
}