<?php
/**
 * M_AuthStorage
 * 
 * M_AuthStorage is an abstract implementation of {@link MI_AuthStorage}, which
 * provides with factory implementations of common methods.
 * 
 * @abstract 
 * @package Core
 */
abstract class M_AuthStorage implements MI_AuthStorage {
	/**
	 * Lifetime
	 * 
	 * This property stores the lifetime of the stored session, expressed in a 
	 * number of seconds. For more information, read:
	 * 
	 * - {@link MI_AuthStorage::setLifetime()}
	 * - {@link MI_AuthStorage::isExpired()}
	 * 
	 * DEFAULT VALUE:
	 * The default lifetime is 3600 seconds (1 hour)
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_lifetime = 3600;
	
	/**
	 * Context
	 * 
	 * This property stores the context of the authenticated session. For more 
	 * information, read:
	 * 
	 * - {@link MI_AuthStorage::setContext()}
	 * - {@link MI_AuthStorage::getContext()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_context = '';
	
	/**
	 * Set lifetime
	 * 
	 * @see MI_AuthStorage::setLifetime()
	 * @access public
	 * @param integer $length
	 * 		The lifetime, expressed in a number of seconds
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setLifetime($length) {
		$this->_lifetime = (int) $length;
		return $this;
	}
	
	/**
	 * Set context
	 * 
	 * @see MI_AuthStorage::setContext
	 * @access public
	 * @param string $context
	 * 		The context can be defined by any string that describes the context
	 * 		or environment in which the session has been authenticated and stored
	 * @return MI_AuthStorage $authStorage
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setContext($context) {
		$this->_context = (string) $context;
		return $this;
	}
	
	/**
	 * Get lifetime
	 * 
	 * @see MI_AuthStorage::getLifetime()
	 * @access public
	 * @return integer $length
	 * 		The lifetime, expressed in a number of seconds
	 */
	public function getLifetime() {
		return $this->_lifetime;
	}
	
	/**
	 * Get context
	 * 
	 * @see MI_AuthStorage::getContext()
	 * @access public
	 * @return string $context
	 * 		The context in which the (authenticated) session is being stored.
	 */
	public function getContext() {
		return $this->_context;
	}
	
	/**
	 * Is expired?
	 * 
	 * @see MI_AuthStorage::isExpired()
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if expired, FALSE if not
	 */
	public function isExpired() {
		// Get number of elapsed seconds, since refresh time:
		$e = time() - $this->getRefreshTime()->getTimestamp();
		
		// The session in storage expires when inactive for longer than allowed
		// by the defined lifetime:
		return ($e <= $this->getLifetime());
	}
}