<?php
/**
 * M_Auth class
 * 
 * @package Core
 */
class M_Auth {
	/**
	 * Singleton instance
	 * 
	 * This property stores the singleton instance, which is issued by
	 * {@link M_Auth::getInstance()}.
	 * 
	 * @access private
	 * @var M_Auth
	 */
	private static $_instance;
	
	/**
	 * Authentication attempt
	 * 
	 * This property stores the authentication attempt, which has been used to
	 * authenticate with {@link M_Auth::authenticate()}
	 * 
	 * @access protected
	 * @var MI_AuthAttempt
	 */
	protected $_attempt;
	
	/**
	 * Authentication result
	 * 
	 * This property stores the result of the authentication, which is produced
	 * by {@link MI_AuthAttempt::authenticate()}
	 * 
	 * @access protected
	 * @var MI_AuthResult
	 */
	protected $_result;
	
	/**
	 * Storage
	 * 
	 * This property stores the {@link MI_AuthStorage} instance that is being
	 * used to store the authenticated session.
	 * 
	 * @access protected
	 * @var MI_AuthStorage
	 */
	protected $_storage;
	
	/**
	 * Regenerate Session ID?
	 * 
	 * This property stores a boolean variable, indicating whether or not the 
	 * Session ID is to be regenerated or not.
	 * 
	 * @see M_Auth::setNewSessionIdAutomatically()
	 * @access protected
	 * @var bool
	 */
	protected $_setNewSessionIdAutomatically;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor (private)
	 * 
	 * Since {@link M_Auth} is a singleton object, the constructor is private. 
	 * This way, an instance of {@link M_Auth} can only be constructed by the
	 * class itself.
	 * 
	 * In order to get the {@link M_Auth} singleton, you should use the standard
	 * {@link M_Auth::getInstance()} method.
	 * 
	 * @access public
	 * @return M_Auth
	 */
	private function __construct() {}
	
	/**
	 * Singleton constructor
	 * 
	 * @static
	 * @access public
	 * @return M_Auth
	 */
	public static function getInstance() {
		if(! self::$_instance) {
			self::$_instance = new self;
		}
		return self::$_instance;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get authentication attempt
	 * 
	 * Will return the authentication attempt, that has been provided previously
	 * to {@link M_Auth::authenticate()}
	 * 
	 * @access public
	 * @return MI_AuthAttempt
	 */
	public function getAttempt() {
		return $this->_attempt;
	}
	
	/**
	 * Get authentication result
	 * 
	 * Will return the result of the authentication, which has been produced by
	 * {@link MI_AuthAttempt::authenticate()}.
	 * 
	 * @see M_Auth::authenticate()
	 * @see MI_AuthAttempt::authenticate()
	 * @access public
	 * @return MI_AuthResult
	 */
	public function getResult() {
		return $this->_result;
	}
	
	/**
	 * Get authentication storage
	 * 
	 * Will return the instance of {@link MI_AuthStorage} that is in charge of
	 * storing the authentication session.
	 * 
	 * @access public
	 * @return MI_AuthStorage
	 */
	public function getStorage() {
		return $this->_storage;
	}
	
	/**
	 * Get identity
	 * 
	 * Will return the identity of the person/entity that has been authenticated.
	 * Note that this method will return the result of {@link MI_AuthStorage::getIdentity()}.
	 * 
	 * @see M_Auth::getStorage()
	 * @see MI_AuthStorage::getIdentity()
	 * @access public
	 * @return mixed
	 */
	public function getIdentity() {
		if($this->_storage) {
			return $this->_storage->getIdentity();
		} else {
			return NULL;
		}
	}
	
	/**
	 * Is authenticated?
	 * 
	 * Will tell whether or not the session has been authenticated. The session
	 * is considered to be authenticated, if an identity has been stored. For
	 * more info, read {@link M_Auth::getIdentity()}
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if authenticated, FALSE if not
	 */
	public function isAuthenticated() {
		return !(! $this->getIdentity());
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Authenticate
	 * 
	 * Will attempt to authenticate a person/entity. In order to do so, this
	 * method accepts an instance of {@link MI_AuthAttempt}, which is in charge
	 * of looking up the identity of the person/entity.
	 * 
	 * Check out {@link M_Auth::setIdentity()} for an elaborate example!
	 * 
	 * @access public
	 * @param MI_AuthAttempt $attempt
	 *		The attempt to authenticate
	 * @return bool $flag
	 * 		Returns identity if authenticated, FALSE if not
	 */
	public function authenticate(MI_AuthAttempt $attempt) {
		// Store an internal reference to the authentication attempt:
		$this->_attempt = $attempt;
		
		// Check if a storage object is available for M_Auth. If that is not
		// the case:
		if(! $this->_storage) {
			// Throw an exception to inform about the error:
			throw new M_AuthException(sprintf(
				'Cannot process M_Auth Attempt; Missing storage object! Use ' . 
				'%s::%s() to set the storage object',
				__CLASS__,
				'setStorage'
			));
		}
		
		// Authenticate, using the AuthAttempt object. This should produce an
		// instance of MI_AuthResult
		$rs = $attempt->authenticate();
		
		// Make sure we have received an MI_AuthResult object
		$expectedInterface = 'MI_AuthResult';
		$isRs = ($rs && M_Helper::isInterface($rs, $expectedInterface));
		
		// If not, we throw an exception
		if(! $isRs) {
			throw new M_AuthException(sprintf(
				'Expecting instance of %s from %s::authenticate()',
				$expectedInterface,
				get_class($attempt)
			));
		}
		
		// We store an internal reference to the result:
		$this->_result = $rs;
		
		// Check if authentication has been completed successfully:
		if($rs->isValid()) {
			// Set the identity in M_Auth:
			// (will also set the identity in the storage object, if any)
			$this->setIdentity($this->_result->getIdentity());
			
			// If the attempt is successful:
			if($this->_result->getCode() == MI_AuthResult::SUCCESS) {
				// If the Session ID is to be regenerated:
				if($this->_setNewSessionIdAutomatically) {
					// Then, regenerate the Session ID now:
					$this->setNewSessionId();
				}
				
				// Return TRUE, which means SUCCESS:
				return TRUE;
			}
		}
		
		// Return FALSE, if we're still here:
		return FALSE;
	}
	
	/**
	 * Get all storage objects, in all contexts, for a given Storage ID
	 * 
	 * This function will return the complete collection of storage objects 
	 * created with a given Storage ID. A storage object will be returned for
	 * each context available.
	 * 
	 * NOTE:
	 * For now, this function only returns instances of {@link M_AuthStorageDb} 
	 * since that is currently the only form in which authenticated sessions are
	 * being stored.
	 * 
	 * @access public
	 * @param string $storageId
	 *		The Storage ID with which to construct storage objects. Typically,
	 *		this is the Session ID
	 * @param string $excludeContext
	 *		The context to be excluded from the return value
	 * @return M_ArrayIterator $storages
	 *		The storage objects; each represented by an instances that implement
	 *		the interface {@link MI_AuthStorage}
	 */
	public function getAllStorageObjectsWithStorageId($storageId, $excludeContext = NULL) {
		// Output:
		$out = array();
		
		// For now, this function only returns instances of M_AuthStorageDb since
		// that is currently the only form in which authenticated sessions are
		// being stored.
		foreach(M_AuthStorageDb::getContextsInstalled() as $context) {
			// If no contexts are excluded, or if this context is not the 
			// excluded one:
			if(is_null($excludeContext) || $excludeContext != $context) {
				// Construct the storage object:
				array_push($out, new M_AuthStorageDb($storageId, $context));
			}
		}
		
		// Return the collection:
		return new M_ArrayIterator($out);
	}
	
	/**
	 * Set storage
	 * 
	 * Can be used to set the instance of {@link MI_AuthStorage} that is to be
	 * used in order to store the authenticated session.
	 * 
	 * @access public
	 * @param MI_AuthStorage $storage
	 * 		The storage instance that will be responsible of storing the
	 * 		authenticated session, and the identity of the person/entity that
	 * 		has been authenticated.
	 * @return void
	 */
	public function setStorage(MI_AuthStorage $storage) {
		$this->_storage = $storage;
	}
	
	/**
	 * Regenerate Session ID
	 * 
	 * This method is used to regenerate the Session ID of the authenticated
	 * session. Typically, a new Session ID is generated when the user of the
	 * application authenticates, in order to protect against hacking attacks 
	 * such as Session Hijacking.
	 * 
	 * Note however that {@link M_Auth} will automatically regenerate the Session
	 * ID if the outcome of {@link M_Auth::authenticate()} is successful. It is
	 * possible though to disable that, with {@link M_Auth::setNewSessionIdAutomatically()}.
	 * 
	 * @uses M_Session::regenerateId()
	 * @access public
	 * @return void
	 */
	public function setNewSessionId() {
		// Get the session:
		$session = M_Session::getInstance();

		// Regenerate the Session ID:
		$session->regenerateId();

		// For each of the storage objects that have been created with the 
		// previous Session ID:
		foreach($this->getAllStorageObjectsWithStorageId($session->getPreviousId()) as $storage) {
			/* @var $storage MI_AuthStorage */
			// Set the new Session ID in the storage object:
			$storage->setStorageId($session->getId());
		}
	}
	
	/**
	 * Regenerate Session ID automatically?
	 * 
	 * With this method, you can enable or disable the automatic regeneration
	 * of the Session ID. For more info, read the docs on {@link M_Auth::setNewSessionId()}.
	 * 
	 * NOTE:
	 * If disabled, you should regenerate the Session ID yourself in order to 
	 * secure your application against common attacks. For more information about
	 * that, read {@link M_Auth::setNewSessionId()}.
	 * 
	 * @access public
	 * @param bool $flag
	 *		Provide TRUE to enable, FALSE to disable
	 * @return void
	 */
	public function setNewSessionIdAutomatically($flag) {
		$this->_setNewSessionIdAutomatically = (bool) $flag;
	}
	
	/**
	 * Set ID
	 * 
	 * Will set the identity of the person/entity that has been authenticated.
	 * Note that this method will use {@link MI_AuthStorage::setIdentity()}.
	 * Typically, this method is used to initiate an authenticated session.
	 * Consider the following example:
	 * 
	 * Example 1
	 * <code>
	 *    // Create an authentication attempt:
	 *    $attempt = new M_AuthAttemptDataObject();
	 *    $attempt->setDataObjectMapper(new AdministratorMapper());
	 *    $attempt->setIdentityField('username');
	 *    $attempt->setIdentity('myUsername');
	 *    $attempt->setPasswordField('password');
	 *    $attempt->setPassword(md5('myPassword'));
	 *    
	 *    // Get the M_Auth Singleton:
	 *    $auth = M_Auth::getInstance();
	 *    
	 *    // Try to authenticate, with the authentication attempt we have created
	 *    // before. If authentication completes successfully:
	 *    if($auth->authenticate($attempt)) {
	 *       // We store the identity in the authenticated session. This will
	 *       // store the Identity in the session's storage.
	 *       $auth->setIdentity();
	 *    }
	 * </code>
	 * 
	 * @see MI_Auth::getIdentity()
	 * @see MI_AuthStorage::setIdentity()
	 * @access public
	 * @param mixed $identity
	 * @return void
	 */
	public function setIdentity($identity) {
		// Check if storage is available
		if($this->_storage) {
			// Set the new identity
			$this->_storage->setIdentity($identity);
		}
		// If storage is not available:
		else {
			// Throw an exception:
			throw new M_AuthException(sprintf(
				'Cannot store Auth Identity; Missing storage object! Use %s::%s() to set the storage object',
				__CLASS__,
				'setStorage'
			));
		}
	}
}