<?php
/**
 * M_ExceptionLoggerDb
 * 
 * ... is a specific implementation of {@link MI_ExceptionLogger}, that logs 
 * exceptions in a database table.
 * 
 * @package Core
 */
class M_ExceptionLoggerDb extends M_ExceptionLogger {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Database Connection ID
	 * 
	 * Stores the Database Connection ID that is used to store the exceptions. For
	 * more information, read {@link M_ExceptionLoggerDb::_getDb()}.
	 * 
	 * @access private
	 * @var MI_Db
	 */
	private $_dbId;
	
	/**
	 * Database Table
	 * 
	 * Stores the Database Table that is used to store the exceptions. For
	 * more information, read {@link M_ExceptionLoggerDb::_getDbTable()}.
	 * 
	 * @access private
	 * @var MI_DbTable
	 */
	private $_dbTable;
	
	/* -- MI_ExceptionLogger -- */
	
	/* -- SETTERS -- */
	
	/**
	 * Log an exception
	 * 
	 * @see MI_ExceptionLogger::logException()
	 * @access public
	 * @param Exception $exception
	 * 		The exception to be logged
	 * @return boolean $flag
	 * 		Returns TRUE if successful, FALSE if not
	 */
	public function logException(Exception $exception) {
		
		// Send a growl notification
		if(
				M_Debug::getDebugMode() && M_Debug::getGrowlNotifications()
				&& (M_Client::getIp() == '127.0.0.1' || M_Client::getIp() == '::1' || M_Request::getHost() == 'localhost')
		) {
			$this->_sendGrowlNotification($exception);
		}
		
		// Get the current URI
		$uri = M_Request::getUri();
		
		// Insert a new entry, and return result:
		return $this->_getDbTable()->insert(array(
			'e_time'    => time(),
			'e_code'    => $exception->getCode(),
			'e_line'    => $exception->getLine(),
			'e_message' => $exception->getMessage(),
			'e_uri'     => $uri->toString(),
			'e_ip'     	=> M_Client::getIp(),
			'e_host'    => $uri->getHost(),
			'e_file'    => $exception->getFile(),
			'e_object'  => '',
			'e_trace'	=> $exception->getTraceAsString()
		));
	}
	
	/**
	 * Clear (remove) exceptions
	 * 
	 * ... will remove the exceptions from logging storage. You may remove exceptions
	 * from a website/application at a given URL, or of a given type (code)
	 * 
	 * @see MI_ExceptionLogger::clearExceptions()
	 * @access public
	 * @param M_Uri $uri
	 * 		The website/application that has thrown the exception. This website/
	 * 		application is defined by a URI
	 * @param string $code
	 * 		You can select the type of exceptions, by providing the exception code
	 * @return boolean $flag
	 * 		Returns TRUE if successful, FALSE if not
	 */
	public function clearExceptions(M_Uri $uri = NULL, $code = NULL) {
		// Conditions
		$condition = array();
		$arguments = array();
		
		// URI?
		if($uri) {
			$condition[] = 'e_host = ?';
			$arguments[] = $uri->getHost();
		}
		
		// Code?
		if($code) {
			$condition[] = 'e_code = ?';
			$arguments[] = (string) $code;
		}
		
		// Delete, and return result
		return $this
			->_getDbTable()
			->delete(
				implode(' AND ', $condition), 
				$arguments
			);
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get number of exceptions
	 * 
	 * Will return the the total number of exceptions that has been logged by
	 * this logger.
	 * 
	 * Note that you may (optionally) provide with an instance of {@link M_Uri},
	 * in order to get the number of exceptions that has been originated by a
	 * website/application at that given URI.
	 * 
	 * @see MI_ExceptionLogger::getNumberOfExceptions()
	 * @access public
	 * @param M_Uri $uri
	 * 		The website/application that has thrown the exception. This website/
	 * 		application is defined by a URI
	 * @param string $code
	 * 		You can select the type of exceptions, by providing the exception code
	 * @return integer $number
	 * 		The number of exceptions that has been logged
	 */
	public function getNumberOfExceptions(M_Uri $uri = NULL, $code = NULL) {
		// Compose the SELECT statement
		$select = $this
			->_getDbTable()
			->select()
			->columnsLiteral($select->count('e_id'))
			->order('e_time', 'DESC');
		
		// URI?
		if($uri) {
			$select->where('e_host = ?', $uri->getHost());
		}
		
		// Code?
		if($uri) {
			$select->where('e_code = ?', (string) $code);
		}
		
		// Return result:
		return $select->getOne();
	}
	
	/**
	 * Get exceptions
	 * 
	 * Will provide with the collection of exceptions that has been logged by
	 * this logger. 
	 * 
	 * Note that you may (optionally) provide with an instance of {@link M_Uri},
	 * in order to get the exceptions that have been originated by a website/
	 * application at that given URI.
	 * 
	 * Note that you may provide with a given maximum, to limit the number of 
	 * exceptions in the collection with. Provide 0 (ZER0) as maximum in order 
	 * to fetch the complete collection.
	 * 
	 * @see MI_ExceptionLogger::getExceptions()
	 * @access public
	 * @param M_Uri $uri
	 * 		The website/application that has thrown the exception(s). This website/
	 * 		application is defined by a URI
	 * @param string $code
	 * 		You can select the type of exceptions, by providing the exception code
	 * @param integer $maximum
	 * @return ArrayIterator $exceptions
	 * 		The collection of {@link Exception} objects
	 */
	public function getExceptions(M_Uri $uri = NULL, $code = NULL, $maximum = 0) {
		// Output:
		$out = array();
		
		// Compose the SELECT statement
		$select = $this
			->_getDbTable()
			->select()
			->columns(array('e_object'))
			->order('e_time', 'DESC');
		
		// URI?
		if($uri) {
			$select->where('e_host = ?', $uri->getHost());
		}
		
		// Code?
		if($uri) {
			$select->where('e_code = ?', (string) $code);
		}
		
		// Maximum?
		if($uri) {
			$select->limit(0, $maximum);
		}
		
		// Run the query:
		$rs = $select->execute();
		
		// If the result set is valid, and it contains at least 1 record
		if($rs && $rs->count() > 0) {
			// For each of the records in the result set:
			foreach($rs as $row) {
				// Mount the exception, and add to output
				$out[] = unserialize($row['e_object']);
			}
		}
		
		// Return the collection of exceptions
		return new ArrayIterator($out);
	}
	
	/* -- SPECIFIC TO M_ExceptionLoggerDb -- */
	
	/* -- SETTERS -- */
	
	/**
	 * Set Database Connection ID
	 * 
	 * ... sets the Connection ID that is used to store and retrieve exceptions
	 * 
	 * @see M_ExceptionLoggerDb::_getDb()
	 * @see M_Db::getInstance()
	 * @access public
	 * @param string $id
	 * 		The Database Connection ID
	 * @return void
	 */
	public function setDbId($id) {
		$this->_dbId = (string) $id;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get database connection
	 * 
	 * Will provide with the database connection that is used to store connections.
	 * If not overridden with {@link M_ExceptionLoggerDb::setDbId()}, this method 
	 * will return the default database connection.
	 * 
	 * @access protected
	 * @return MI_Db
	 */
	protected function _getDb() {
		if($this->_dbId) {
			return M_Db::getInstance($this->_dbId);
		} else {
			return M_Db::getInstance();
		}
	}
	
	/**
	 * Get Database Table
	 * 
	 * Will provide with the instance of {@link MI_DbTable} that is used to store
	 * exceptions. Note that, if the database table does not exist in the database,
	 * this class will attempt to create it first
	 * 
	 * @access protected
	 * @return MI_DbTable $table
	 * 		The table that is used to store the exception
	 */
	protected function _getDbTable() {
		// If not requested before
		if(! $this->_dbTable) {
			// If the table does not exist:
			if(! $this->_getDb()->hasTable('exceptions')) {
				// Create the table now:
				$this->_dbTable = $this->_getDbTableNew();
				$this->_dbTable->create();
			}
			// If the table already exists:
			else {
				// Get the table:
				$this->_dbTable = $this->_getDb()->getTable('exceptions');
			}
		}
		
		// Return the table
		return $this->_dbTable;
	}
	
	/**
	 * 
	 * @access protected
	 * @return MI_DbTable $table
	 * 		The new database table
	 */
	protected function _getDbTableNew() {
		// Construct the table object
		$table = $this->_getDb()->getNewTable();
		$table->setName('exceptions');
		
		// Create a column for the ID (Auto-Increment)
		$id = new M_DbColumn('e_id');
		$id->setIsAutoIncrement(TRUE);
		$id->setType(M_DbColumn::TYPE_INTEGER);
		$table->addColumn($id);
		
		// Create a column for the time
		$time = new M_DbColumn('e_time');
		$time->setType(M_DbColumn::TYPE_INTEGER);
		$table->addColumn($time);
		
		// Create a column for the code
		$code = new M_DbColumn('e_code');
		$code->setType(M_DbColumn::TYPE_VARCHAR);
		$code->setLength(8);
		$table->addColumn($code);
		
		// Create a column for the code
		$line = new M_DbColumn('e_line');
		$line->setType(M_DbColumn::TYPE_INTEGER);
		$table->addColumn($line);
		
		// Create a column for the message
		$msg = new M_DbColumn('e_message');
		$msg->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($msg);
		
		// Create a column for the URI
		$uri = new M_DbColumn('e_uri');
		$uri->setType(M_DbColumn::TYPE_VARCHAR);
		$uri->setLength(255);
		$table->addColumn($uri);
		
		// Create a column for the host
		$host = new M_DbColumn('e_host');
		$host->setType(M_DbColumn::TYPE_VARCHAR);
		$host->setLength(128);
		$table->addColumn($host);
		
		// Create a column for the filename
		$file = new M_DbColumn('e_file');
		$file->setType(M_DbColumn::TYPE_VARCHAR);
		$file->setLength(255);
		$table->addColumn($file);
		
		// Create a column for the status
		$ip = new M_DbColumn('e_ip');
		$ip->setType(M_DbColumn::TYPE_VARCHAR);
		$ip->setLength(16);
		$table->addColumn($ip);
		
		// Create a column for the status
		$status = new M_DbColumn('e_status');
		$status->setType(M_DbColumn::TYPE_TINY_INTEGER);
		$status->setLength(1);
		$table->addColumn($status);
		
		// Create a column for the status
		$statusTime = new M_DbColumn('e_status_time');
		$statusTime->setType(M_DbColumn::TYPE_INTEGER);
		$statusTime->setLength(11);
		$table->addColumn($statusTime);
		
		// Create a column for the object copy
		$obj = new M_DbColumn('e_object');
		$obj->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($obj);

		// Create a column for the trace
		$trace = new M_DbColumn('e_trace');
		$trace->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($trace);
		
		// Create the PRIMARY key:
		$primary = new M_DbIndex('PRIMARY');
		$primary->setType(M_DbIndex::PRIMARY);
		$primary->addColumn($id);
		$table->setPrimaryKey($primary);
		
		// Create the INDEXES:
		$index = new M_DbIndex('URI');
		$index->setType(M_DbIndex::INDEX);
		$index->addColumn($host);
		$table->addIndex($index);
		
		$index = new M_DbIndex('CODE');
		$index->setType(M_DbIndex::INDEX);
		$index->addColumn($code);
		$table->addIndex($index);
		
		$index = new M_DbIndex('STATUS');
		$index->setType(M_DbIndex::INDEX);
		$index->addColumn($status);
		$table->addIndex($index);
		
		// Return the table
		return $table;
	}
	
	/**
	 * Send a growl message
	 * 
	 * @param Exception $exception
	 * @param bool $sticky 
	 */
	protected function _sendGrowlNotification($exception, $sticky = 1) {
		/*
		 * PLEASE NOTE THAT WE USE THRIDPARTY-FILES OF THE NET_GROWL PEAR PACKAGE:
		 * http://pear.php.net/package/Net_Growl/ 
		 */
		
		// Load the Growl classess
		M_Loader::loadAbsolute('core/_thirdparty/growl/Growl/Autoload.php');

		// Check if the class exists
		if(class_exists('Net_Growl')) {
			// Create a new notification
			$notifications = array(
				'GROWL_NOTIFY_EXCEPTION' => array(
					'display' => 'Error-Log'
				)
			);

			// Do some settings to send the notification to local IP
			$appName  = 'MultiGuard';
			$password = 'multiguard';
			$options  = array(
				'host'     => '127.0.0.1',
				'protocol' => 'tcp',
				'port'     => Net_Growl::GNTP_PORT,
				'timeout'  => 15,
			);

			try {

				// Register the Growl APP
				$growl = Net_Growl::singleton($appName, $notifications, $password, $options);
				$growl->register();

				// Set the options for the notification
				$name = 'GROWL_NOTIFY_EXCEPTION';
				$title = 'New exception';
				$description = $exception->getMessage();
				$options = array(
					'priority' => Net_Growl::PRIORITY_HIGH,
					'icon'   => M_Request::getLinkWithoutPrefix('core/_thirdparty/growl/multiguard.png'),
					'sticky' => $sticky
				);

				// Send the notification to the client
				$growl->notify($name, $title, $description, $options);

			} catch (Net_Growl_Exception $e) {
				// When an exception is caught: do nothing
			}
		}
	}
}