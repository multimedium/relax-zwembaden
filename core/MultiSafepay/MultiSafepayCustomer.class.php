<?php
/**
 * M_MultiSafepayCustomer
 * 
 * This class represents the customer who is using MultiSafepay to complete
 * their order. It contains a number of field with information about
 * their name, address etc. Typically, one {@link M_MultiSafepayCustomer} is
 * required for every {@link M_MultiSafepay} payment
 * 
 * @package Core
 */
class M_MultiSafepayCustomer extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Locale
	 * 
	 * NOTE: setting this property is required for proper customer functionality
	 * 
	 * @see M_MultiSafepayCustomer::setLocale()
	 * @see M_MultiSafepayCustomer::getLocale()
	 * @access private
	 * @var string
	 */
	private $_locale;
	
	/**
	 * IP address
	 * 
	 * @see M_MultiSafepayCustomer::setIp()
	 * @see M_MultiSafepayCustomer::getIp()
	 * @access private
	 * @var string
	 */
	private $_ip;
	
	/**
	 * First name
	 * 
	 * @see M_MultiSafepayCustomer::setFirstName()
	 * @see M_MultiSafepayCustomer::getFirstName()
	 * @access private
	 * @var string
	 */
	private $_firstName;
	
	/**
	 * Surnames
	 * 
	 * @see M_MultiSafepayCustomer::setSurnames()
	 * @see M_MultiSafepayCustomer::getSurnames()
	 * @access private
	 * @var string
	 */
	private $_surnames;
	
	/**
	 * Street Name
	 * 
	 * @see M_MultiSafepayCustomer::setAddress()
	 * @see M_MultiSafepayCustomer::setStreetName()
	 * @see M_MultiSafepayCustomer::getStreetName()
	 * @access private
	 * @var string
	 */
	private $_streetName;
	
	/**
	 * Street Number
	 * 
	 * @see M_MultiSafepayCustomer::setAddress()
	 * @see M_MultiSafepayCustomer::setStreetNumber()
	 * @see M_MultiSafepayCustomer::getStreetNumber()
	 * @access private
	 * @var string
	 */
	private $_streetNumber;
	
	/**
	 * Postal Code
	 * 
	 * @see M_MultiSafepayCustomer::setAddress()
	 * @see M_MultiSafepayCustomer::setPostalCode()
	 * @see M_MultiSafepayCustomer::getPostalCode()
	 * @access private
	 * @var string
	 */
	private $_postalCode;
	
	/**
	 * City
	 * 
	 * @see M_MultiSafepayCustomer::setAddress()
	 * @see M_MultiSafepayCustomer::setCity()
	 * @see M_MultiSafepayCustomer::getCity()
	 * @access private
	 * @var string
	 */
	private $_city;
	
	/**
	 * Country
	 * 
	 * @see M_MultiSafepayCustomer::setAddress()
	 * @see M_MultiSafepayCustomer::setCountry()
	 * @see M_MultiSafepayCustomer::getCountry()
	 * @access private
	 * @var string
	 */
	private $_country;
	
	/**
	 * Phone
	 * 
	 * @see M_MultiSafepayCustomer::setPhone()
	 * @see M_MultiSafepayCustomer::getPhone()
	 * @access private
	 * @var string
	 */
	private $_phone;
	
	/**
	 * Email
	 * 
	 * NOTE: setting this property is required for proper customer functionality
	 * 
	 * @see M_MultiSafepayCustomer::setEmail()
	 * @see M_MultiSafepayCustomer::getEmail()
	 * @access private
	 * @var string
	 */
	private $_email;
	
	/* -- SETTERS -- */
	
	/**
	 * Set Locale
	 * 
	 * Allows for setting the locale used by the customer. This locale will
	 * determine the language in which the MultiSafepay page will be displayed,
	 * as well as defining what country the customer is from. Typically,
	 * this locale looks for example like follows:
	 * 
	 * - nl_NL
	 * - nl_BE
	 * - fr_BE
	 * 
	 * The first 2 letters define the language, the last 2 letters
	 * represent the country of origin. Note that you may specify the locale
	 * in its official format, which may include the encoding and variant,
	 * as defined in {@link M_Locale::getNameParts()}
	 * 
	 * NOTE: setting the locale is mandatory for proper customer functionality
	 * 
	 * @access public
	 * @param string $locale
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLocale($locale) {
		// Fetch the locale name parts
		$parts = M_Locale::getNameParts($locale);
		
		// Fetch the language and country
		$language = $parts['language'];
		$country = $parts['country'];
		
		// Make sure the language and country are both given
		if(! $language) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no valid language given',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(! $country) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no valid country given',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Set the property
		$this->_locale = $language . '_' . $country;
		
		// And return self
		return $this;
	}
	
	/**
	 * Set Ip
	 * 
	 * Allows for setting the IP address from which the customer is about
	 * to make the payment
	 * 
	 * @access public
	 * @param string $ip
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setIp($ip) {
		$this->_ip = (string) $ip;
		return $this;
	}
	
	/**
	 * Set First Name
	 * 
	 * Allows for setting the customer's first name
	 * 
	 * @access public
	 * @param string $firstName
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFirstName($firstName) {
		$this->_firstName = (string) $firstName;
		return $this;
	}
	
	/**
	 * Set Surnames
	 * 
	 * Allows for setting the customer's surnames
	 * 
	 * @access public
	 * @param string $surnames
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSurnames($surnames) {
		$this->_surnames = (string) $surnames;
		return $this;
	}
	
	/**
	 * Set Street Name
	 * 
	 * Allows for setting the customer's street name
	 * 
	 * @access public
	 * @param string $streetName
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setStreetName($streetName) {
		$this->_streetName = (string) $streetName;
		return $this;
	}
	
	/**
	 * Set Street Number
	 * 
	 * Allows for setting the customer's street number
	 * 
	 * @access public
	 * @param string $streetNumber
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setStreetNumber($streetNumber) {
		$this->_streetNumber = (string) $streetNumber;
		return $this;
	}
	
	/**
	 * Set Postal Code
	 * 
	 * Allows for setting the customer's postal code
	 * 
	 * @access public
	 * @param string $postalCode
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPostalCode($postalCode) {
		$this->_postalCode = (string) $postalCode;
		return $this;
	}
	
	/**
	 * Set City
	 * 
	 * Allows for setting the customer's city
	 * 
	 * @access public
	 * @param string $city
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCity($city) {
		$this->_city = (string) $city;
		return $this;
	}
	
	/**
	 * Set Country
	 * 
	 * Allows for setting the customer's country
	 * 
	 * @access public
	 * @param string $country
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCountry($country) {
		$this->_country = (string) $country;
		return $this;
	}
	
	/**
	 * Set Address
	 * 
	 * Allows for providing a {@link M_ContactAddress}, creating a shortcut
	 * for setting the following:
	 * 
	 * - {@link M_MultiSafePayCustomer::setStreetName()}
	 * - {@link M_MultiSafePayCustomer::setStreetNumber()}
	 * - {@link M_MultiSafePayCustomer::setPostalCode()}
	 * - {@link M_MultiSafePayCustomer::setCity()}
	 * - {@link M_MultiSafePayCustomer::setCountry()}
	 * 
	 * @access public
	 * @param M_ContactAddress $address
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAddress(M_ContactAddress $address) {
		$this->setStreetName($address->getStreetName());
		$this->setStreetNumber($address->getStreetNumber());
		$this->setPostalCode($address->getPostalCode());
		$this->setCity($address->getCity());
		$this->setCountry($address->getCountry());
		return $this;
	}
	
	/**
	 * Set Phone
	 * 
	 * Allows for setting the customer's phone number
	 * 
	 * @access public
	 * @param string $phone
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPhone($phone) {
		$this->_phone = (string) $phone;
		return $this;
	}
	
	/**
	 * Set Email
	 * 
	 * Allows for setting the customer's email address
	 * 
	 * NOTE: setting the email address is mandatory for correct customer
	 * functionality
	 * 
	 * @access public
	 * @param string $email
	 * @return M_MultiSafepayCustomer
	 *		Returns itself, for a fluent programming interface
	 */
	public function setEmail($email) {
		$this->_email = (string) $email;
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Locale
	 * 
	 * @see M_MultiSafepayCustomer::setLocale()
	 * @access public
	 * @return string
	 */
	public function getLocale() {
		return $this->_locale;
	}
	
	/**
	 * Get Ip
	 * 
	 * @see M_MultiSafepayCustomer::setIp()
	 * @access public
	 * @return string
	 */
	public function getIp() {
		return $this->_ip;
	}
	
	/**
	 * Get First Name
	 * 
	 * @see M_MultiSafepayCustomer::setFirstName()
	 * @access public
	 * @return string
	 */
	public function getFirstName() {
		return $this->_firstName;
	}
	
	/**
	 * Get Surnames
	 * 
	 * @see M_MultiSafepayCustomer::setSurnames()
	 * @access public
	 * @return string
	 */
	public function getSurnames() {
		return $this->_surnames;
	}
	
	/**
	 * Get Street Name
	 * 
	 * @see M_MultiSafepayCustomer::setStreetName()
	 * @access public
	 * @return string
	 */
	public function getStreetName() {
		return $this->_streetName;
	}
	
	/**
	 * Get Street Number
	 * 
	 * @see M_MultiSafepayCustomer::setStreetNumber()
	 * @access public
	 * @return string
	 */
	public function getStreetNumber() {
		return $this->_streetNumber;
	}
	
	/**
	 * Get Postal Code
	 * 
	 * @see M_MultiSafepayCustomer::setPostalCode()
	 * @access public
	 * @return string
	 */
	public function getPostalCode() {
		return $this->_postalCode;
	}
	
	/**
	 * Get City
	 * 
	 * @see M_MultiSafepayCustomer::setCity()
	 * @access public
	 * @return string
	 */
	public function getCity() {
		return $this->_city;
	}
	
	/**
	 * Get Country
	 * 
	 * @see M_MultiSafepayCustomer::setCountry()
	 * @access public
	 * @return string
	 */
	public function getCountry() {
		return $this->_country;
	}
	
	/**
	 * Get Phone
	 * 
	 * @see M_MultiSafepayCustomer::setPhone()
	 * @access public
	 * @return string
	 */
	public function getPhone() {
		return $this->_phone;
	}
	
	/**
	 * Get Email
	 * 
	 * @see M_MultiSafepayCustomer::setEmail()
	 * @access public
	 * @return string
	 */
	public function getEmail() {
		return $this->_email;
	}
	
	/**
	 * Get Xml
	 * 
	 * Will return this {@link M_MultiSafepayCustomer} in XML format, which
	 * is typically used to make the XML request in {@link M_MultiSafepay}
	 * 
	 * @access public
	 * @return string
	 */
	public function getXml() {
		// Before proceeding, make sure the mandatory properties are provided:
		if(! $this->getLocale()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no locale has been set',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(! $this->getEmail()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no email address has been set',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Start the output
		$out = '<customer>';
		
		// Add the properties
		$out .=		'<locale>'.			$this->getLocale()		.'</locale>';
		$out .=		'<ipaddress>'.		$this->getIp()				.'</ipaddress>';
		$out .=		'<forwardedip>'		/* not in use currently */	.'</forwardedip>';
		$out .=		'<firstname><![CDATA['.		$this->getFirstName()		.']]></firstname>';
		$out .=		'<lastname><![CDATA['.		$this->getSurnames()		.']]></lastname>';
		$out .=		'<address1><![CDATA['.		$this->getStreetName()		.']]></address1>';
		$out .=		'<address2>'		/* not in use currently */	.'</address2>';
		$out .=		'<housenumber>'.	$this->getStreetNumber()	.'</housenumber>';
		$out .=		'<zipcode>'.		$this->getPostalCode()		.'</zipcode>';
		$out .=		'<city><![CDATA['.			$this->getCity()			.']]></city>';
		$out .=		'<country><![CDATA['.		$this->getCountry()			.']]></country>';
		$out .=		'<phone><![CDATA['.			$this->getPhone()			.']]></phone>';
		$out .=		'<email>'.			$this->getEmail()			.'</email>';
		
		// Close the output
		$out .= '</customer>';
		
		// Return return the output
		return $out;
	}
}