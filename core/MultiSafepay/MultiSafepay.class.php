<?php
/**
 * M_MultiSafepay
 * 
 * Main M_MultiSafepay class. Typically, when having to use MultiSafepay
 * as a payment option in a webshop, we follow the following flow:
 * 
 * - Create a new {@link M_MultiSafepay} object
 * 
 * - Create a new {@link M_MultiSafepayMerchant} object, which contains
 *   information on the merchant who is registered with MultiSafepay, and
 *   set it with {@link M_MultiSafepay::setMerchant()}
 * 
 * - Create a new {@link M_MultiSafepayCustomer} object, which contains
 *   information on the customer who is about to make a payment with
 *	 MultiSafepay, and set it with {@link M_MultiSafepay::setCustomer()}
 * 
 * - Create a new {@link M_MultiSafepayTransaction} object, which contains
 *   information on the payment itself, and set it with
 *	 {@link M_MultiSafepay::setTransaction()}
 * 
 * - Fetch the payment uri through {@link M_MultiSafepay::getUriForPayment()}
 * 
 * - Redirect the user to the received payment uri, initiating the payment
 *	 procedure in the MultiSafepay interface
 * 
 * NOTE: as of currently, this implementation only supports the Fast Checkout
 * version of MultiSafepay
 * 
 * NOTE: for more information on how the implementation works, read the docs
 * on all M_MultiSafepay related classes, as well as the documentation
 * provided on the MultiSafepay website.
 * 
 * @package Core
 */
class M_MultiSafepay extends M_Object {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Mode: live
	 * 
	 * Constant used to define that we are working in a live environment,
	 * rather than a testing environment.
	 * 
	 * @see M_MultiSafepay::__construct()
	 * @see M_MultiSafepay::setMode()
	 * @see M_MultiSafepay::getMode()
	 * @var string
	 */
	const MODE_LIVE = 'live';
	
	/**
	 * Mode: testing
	 * 
	 * Constant used to define that we are working in a testing environment,
	 * rather than a live environment.
	 * 
	 * @see M_MultiSafepay::__construct()
	 * @see M_MultiSafepay::setMode()
	 * @see M_MultiSafepay::getMode()
	 * @var string
	 */
	const MODE_TESTING = 'testing';
	
	/**
	 * Method: fast checkout
	 * 
	 * @var string
	 */
	const METHOD_FAST_CHECKOUT = 'checkouttransaction';
	
	/**
	 * Method: connect
	 * 
	 * @var string
	 */
	const METHOD_CONNECT = 'redirecttransaction';

	/* -- PROPERTIES -- */
	
	/**
	 * API Uri Live
	 * 
	 * Holds the uri we have to send the XML request to in a live
	 * environment. Whether we are in a live or testing environment can
	 * be changed through {@link MultiSafepay::setMode()}
	 * 
	 * @access private
	 * @var string
	 */
	private $_api_uri_live = 'https://api.multisafepay.com/ewx/';
	
	/**
	 * API Uri Testing
	 * 
	 * Holds the uri we have to send the XML request to in a testing
	 * environment. Whether we are in a live or testing environment can
	 * be changed through {@link MultiSafepay::setMode()}
	 * 
	 * @access private
	 * @var string
	 */
	private $_api_uri_testing = 'https://testapi.multisafepay.com/ewx/';
	
	/**
	 * Mode
	 * 
	 * Holds the currently active mode
	 * 
	 * @see M_MultiSafepay::setMode()
	 * @see M_MultiSafepay::getMode()
	 * @access private
	 * @var string
	 */
	private $_mode;
	
	/**
	 * Merchant
	 * 
	 * Holds the {@link M_MultiSafepayMerchant} that is to be used for the
	 * payment
	 * 
	 * NOTE: this property is mandatory for correct functionality
	 * 
	 * @see M_MultiSafepay::setMerchant()
	 * @see M_MultiSafepay::getMerchant()
	 * @access private
	 * @var M_MultiSafepayMerchant
	 */
	private $_merchant;
	
	/**
	 * Customer
	 * 
	 * Holds the {@link M_MultiSafepayCustomer}, which contains information
	 * about the customer that is about to make a payment
	 * 
	 * NOTE: this property is mandatory for correct functionality
	 * 
	 * @see M_MultiSafepay::setCustomer()
	 * @see M_MultiSafepay::getCustomer()
	 * @access private
	 * @var M_MultiSafepayCustomer
	 */
	private $_customer;
	
	/**
	 * Transaction
	 * 
	 * Holds the {@link M_MultiSafepayTransaction} that contains the
	 * transaction details for the payment
	 * 
	 * NOTE: this property is mandatory for correct functionality
	 * 
	 * @see M_MultiSafepay::setTransaction()
	 * @see M_MultiSafepay::getTransaction()
	 * @access private
	 * @var M_MultiSafepayTransaction
	 */
	private $_transaction;
	
	/**
	 * Error Code
	 * 
	 * Holds the error code, if any
	 * 
	 * @see M_MultiSafepay::getUriForPayment()
	 * @see M_MultiSafepay::_processError()
	 * @access private
	 * @var string
	 */
	private $_errorCode;
	
	/**
	 * Error Description
	 * 
	 * Holds the error description, if any
	 * 
	 * @see M_MultiSafepay::getUriForPayment()
	 * @see M_MultiSafepay::_processError()
	 * @access private
	 * @var string
	 */
	private $_errorDescription;
	
	/**
	 * Method
	 * 
	 * @var  string
	 */
	private $_method = self::METHOD_FAST_CHECKOUT;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Construct
	 * 
	 * @access public
	 * @return M_MultiSafepay
	 */
	public function __construct() {
		// Default the mode to live
		$this->setMode(self::MODE_LIVE);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set Mode
	 * 
	 * Allows for specifying the mode we are currently working in. Typically,
	 * this is used to specify whether we are working in a live or testing
	 * environment.
	 * 
	 * Setting this property makes use of the constants defined in this class:
	 * - {@link M_MultiSafepay::MODE_LIVE}
	 * - {@link M_MultiSafepay::MODE_TESTING}
	 * 
	 * NOTE: by default we are working in a live environment, as set
	 * in {@link M_MultiSafepay::__construct()}
	 * 
	 * @access public
	 * @param string $mode
	 * @return M_MultiSafepay
	 *		Returns itself, for a fluent programming interface
	 */
	public function setMode($mode) {
		// Cast to string before proceeding
		$mode = (string) $mode;
		
		// Make sure we received a valid mode
		if(! in_array($mode, array(self::MODE_LIVE, self::MODE_TESTING))) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): invalid mode %s given',
				__CLASS__,
				__FUNCTION__,
				$mode
			));
		}
		
		// Set the property
		$this->_mode = $mode;
		
		// Return self
		return $this;
	}
	
	/**
	 * Set method
	 * 
	 * @param string $method
	 * @return M_MultiSafepay
	 * @throws M_Exception
	 */
	public function setMethod($method) {
		// Cast to string before proceeding
		$method = (string) $method;
		
		// Make sure we received a valid mode
		if(! in_array($method, array(self::METHOD_CONNECT, self::METHOD_FAST_CHECKOUT))) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): invalid method %s given',
				__CLASS__,
				__FUNCTION__,
				$method
			));
		}
		
		// Set the property
		$this->_method = $method;
		
		// Return self
		return $this;
	}
	
	/**
	 * Get method
	 * 
	 * @return string
	 */
	public function getMethod() {
		return $this->_method;
	}
	
	/**
	 * Set Merchant
	 * 
	 * Allows for setting the {@link M_MultiSafepayMerchant} that is used
	 * for the payment. This object contains information about the merchant
	 * who is registered with MultiSafepay, and who will receive the payment
	 * after it has been completed. For more info, read the docs on
	 * {@link M_MultiSafepayMerchant} itself.
	 * 
	 * NOTE: specifying the merchant is mandatory for correct functionality
	 * 
	 * @access public
	 * @var M_MultiSafepayMerchant $merchant
	 * @return M_MultiSafepay
	 *		Returns itself, for a fluent programming interface
	 */
	public function setMerchant(M_MultiSafepayMerchant $merchant) {
		$this->_merchant = $merchant;
		return $this;
	}
	
	/**
	 * Set Customer
	 * 
	 * Allows for setting the {@link M_MultiSafepayCustomer} that is used
	 * for the payment. This object contains info about the cusomer who is
	 * about to make a payment with MultiSafepay. For more info, read the docs
	 * on {@link M_MultiSafepayCustomer} itself.
	 * 
	 * NOTE: specifying the customer is mandatory for correct functionality
	 * 
	 * @access public
	 * @var M_MultiSafepayCustomer $customer
	 * @return M_MultiSafepay
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCustomer(M_MultiSafepayCustomer $customer) {
		$this->_customer = $customer;
		return $this;
	}
	
	/**
	 * Set Transaction
	 * 
	 * Allows for setting the {@link M_MultiSafepayTransaction} that contains
	 * mandatory information for the payment. For more info, read the docs on
	 * {@link M_MultiSafepayTransaction} itself.
	 * 
	 * NOTE: specifying the transaction is mandatory for correct functionality
	 * 
	 * @access public
	 * @var M_MultiSafepayTransaction $transaction
	 * @return M_MultiSafepay
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTransaction(M_MultiSafepayTransaction $transaction) {
		$this->_transaction = $transaction;
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Mode
	 * 
	 * @see M_MultiSafepay::setMode()
	 * @access public
	 * @return string;
	 */
	public function getMode() {
		return $this->_mode;
	}
	
	/**
	 * Get Merchant
	 * 
	 * @see M_MultiSafepay::setMerchant()
	 * @access public
	 * @return M_MultiSafepayMerchant
	 */
	public function getMerchant() {
		return $this->_merchant;
	}
	
	/**
	 * Get Customer
	 * 
	 * @see M_MultiSafepay::setCustomer()
	 * @access public
	 * @return M_MultiSafepayCustomer
	 */
	public function getCustomer() {
		return $this->_customer;
	}
	
	/**
	 * Get Transaction
	 * 
	 * @see M_MultiSafepay::setTransaction()
	 * @access public
	 * @return M_MultiSafepayTransaction
	 */
	public function getTransaction() {
		return $this->_transaction;
	}
	
	/**
	 * Get Xml
	 * 
	 * Will return this {@link M_MultiSafepay} in XML format, which
	 * is typically used to make the XML request. To be able to create the XML,
	 * the following properties should be defined:
	 * 
	 * - {@link MultiSafepay::setMerchant()}
	 * - {@link MultiSafepay::setCustomer()}
	 * - {@link MultiSafepay::setTransaction()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getXml() {
		// Make sure all required properties are set:
		if(! $this->getMerchant()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no M_MultiSafepayMerchant has ' .
				'been defined yet',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(! $this->getCustomer()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no M_MultiSafepayCustomer has ' .
				'been defined yet',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(! $this->getTransaction()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no M_MultiSafepayTransaction has ' .
				'been defined yet',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Start the output
		$out =  '<?xml version="1.0" encoding="UTF-8"?>';
		$out .= '<' . $this->getMethod() . ' ua="Multimanage">';
		
		// Add the mandatory properties
		$out .=		$this->getMerchant()->getXml();
		$out .=		$this->getCustomer()->getXml();
		$out .=		$this->getTransaction()->getXml();
		
		// Quickfix: do not support transport methods
		// NOTE: transport methods are currently not supported by the Multisafepay core-classes
		$out .= '<checkout-settings><no-shipping-method>true</no-shipping-method></checkout-settings>';
		
		// And finally, add the signature
		$out .=		'<signature>' . $this->_getSignature() . '</signature>';
		
		// Close the output
		$out .= '</' . $this->getMethod() . '>';
		
		// Return return the output
		return $out;
	}
	
	/**
	 * Get XML for status
	 * 
	 * @return string
	 * @throws M_Exception
	 */
	public function getXmlForStatus() {
		// Make sure all required properties are set:
		if(! $this->getMerchant()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no M_MultiSafepayMerchant has ' .
				'been defined yet',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(! $this->getTransaction()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no M_MultiSafepayTransaction has ' .
				'been defined yet',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Start the output
		$out =  '<?xml version="1.0" encoding="UTF-8"?>';
		$out .= '<status ua="Multimanage">';
		
		// Add the mandatory properties
		$out .=		$this->getMerchant()->getXml();
		
		// Add the transaction id
		$out .= '<transaction><id>' . $this->getTransaction()->getId() . '</id></transaction>';
		
		// Close the output
		$out .= '</status>';
		
		// Return return the output
		return $out;
	}
	
	/**
	 * Get payment status
	 * 
	 * @return M_MultiSafepayStatus
	 * @throws M_Exception
	 */
	public function getPaymentStatus() {
		// Create an uri session with the correct API uri
		$uriSession = new M_UriSession($this->_getApiUri());
		
		// As we are working on HTTPS, make sure we allow all certificates
		$uriSession->setAcceptanceOfAnyPeerCertificate();
		
		// Specify that we are sending XML
		$uriSession->setHeaders(array('Content-Type: text/xml'));
		
		// Add the XML request as raw POST data
		$uriSession->setPostDataRaw($this->getXmlForStatus());
		
		// Make the call:	
		$result = $uriSession->getContents();
		
		// Create an XML element with the result
		$xml = M_XmlElement::constructWithString($result);
		
		// Before proceeding, we need to check whether the response was OK,
		// or whether we received an error. Fetch the status:
		$status = $xml->getAttribute('result');
		
		// If the status contained an error
		if($status == 'error') {
			// Then process the error
			$this->_processError($xml);

			// And stop here
			return false;
		}
		// If the status is not an error, it must be ok. If it isn't:
		elseif($status != 'ok') {
			throw new M_Exception(sprintf(
				'Unkown result status %s received in %s::%s()',
				$status,
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// The status of the request was ok, so fetch the payment status
		return M_MultiSafepayStatus::constructWithXml($xml);
	}
	
	/**
	 * Get Uri For Payment
	 * 
	 * Will generate the payment uri the user should be redirected to to start
	 * the payment in the MultiSafepay interface.
	 * 
	 * @access public
	 * @return M_Uri
	 */
	public function getUriForPayment() {
		// Create an uri session with the correct API uri
		$uriSession = new M_UriSession($this->_getApiUri());
		
		// As we are working on HTTPS, make sure we allow all certificates
		$uriSession->setAcceptanceOfAnyPeerCertificate();
		
		// Specify that we are sending XML
		$uriSession->setHeaders(array('Content-Type: text/xml'));
		
		// Add the XML request as raw POST data
		$uriSession->setPostDataRaw($this->getXml());
		
		// Make the call:	
		$result = $uriSession->getContents();
		
		// Create an XML element with the result
		$xml = M_XmlElement::constructWithString($result);
		
		// Before proceeding, we need to check whether the response was OK,
		// or whether we received an error. Fetch the status:
		$status = $xml->getAttribute('result');
		
		// If the status contained an error
		if($status == 'error') {
			// Then process the error
			$this->_processError($xml);

			// And stop here
			return false;
		}
		// If the status is not an error, it must be ok. If it isn't:
		elseif($status != 'ok') {
			throw new M_Exception(sprintf(
				'Unkown result status %s received in %s::%s()',
				$status,
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// The status of the request was ok, so fetch the payment uri
		$paymentUri = $xml->getOneWithXpath('transaction/payment_url');
		
		// If the payment uri was not found in the xml response
		if(! $paymentUri) {
			throw new M_Exception(sprintf(
				'No payment url was found in the received XML response in %s::%s()',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// We found the payment url, so return it as M_Uri
		return new M_Uri($paymentUri);
	}
	
	/**
	 * Get Error Code
	 * 
	 * Will return the error code, if any error occured during the request
	 * to obtain the payment uri
	 * 
	 * @see M_MultiSafepay::getUriForPayment()
	 * @see M_MultiSafepay::_processError()
	 * @access public
	 * @return string
	 */
	public function getErrorCode() {
		return $this->_errorCode;
	}
	
	/**
	 * Get Error Description
	 * 
	 * Will return the error description, if any error occured during the
	 * request to obtain the payment uri
	 * 
	 * @see M_MultiSafepay::getUriForPayment()
	 * @see M_MultiSafepay::_processError()
	 * @access public
	 * @return string
	 */
	public function getErrorDescription() {
		return $this->_errorDescription;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Signature
	 * 
	 * Will provide the md5 hashed signature that is required to verify the
	 * payment. This signature is created using the following elements:
	 * 
	 * - {@link M_MultiSafepayTransaction::getAmount()}
	 * - {@link M_MultiSafepayTransaction::getCurrency()}
	 * - {@link M_MultiSafepayMerchant::getAccount()}
	 * - {@link M_MultiSafepayMerchant::getSiteId()}
	 * - {@link M_MultiSafepayTransaction::getId()}
	 * 
	 * As a result, it is obviously necessary that both the
	 * {@link M_MultiSafepayTransaction} and {@link M_MultiSafepayMerchant}
	 * and their required parameters have been set before we are able
	 * to generate the signature.
	 * 
	 * @access protected
	 * @return string
	 */
	protected function _getSignature() {
		// Fetch the transaction
		$transaction = $this->getTransaction();
		
		// If not set
		if(! $transaction) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): the M_MultiSafepayTransaction ' .
				'must be specified before we can generate the signature',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Now fetch the merchant
		$merchant = $this->getMerchant();
		
		// If not set
		if(! $merchant) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): the M_MultiSafepayMerchant ' .
				'must be specified before we can generate the signature',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Fetch the amount
		$amount = $transaction->getAmountForProcessing();
		if(! $amount) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): a valid transaction amount ' .
				'must be specified before we can generate the signature',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Fetch the amount
		$currency = $transaction->getCurrency();
		if(! $currency) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): a valid transaction currency ' .
				'must be specified before we can generate the signature',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Fetch the account id
		$accountId = $merchant->getAccountId();
		if(! $accountId) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): the merchant account id ' .
				'must be specified before we can generate the signature',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Fetch the site id
		$siteId = $merchant->getSiteId();
		if(! $siteId) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): the merchant site id ' .
				'must be specified before we can generate the signature',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Fetch the transaction id
		$transactionId = $transaction->getId();
		if(! $transactionId) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): the transaction id ' .
				'must be specified before we can generate the signature',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Create the md5 hash, returning it
		return md5($amount . $currency . $accountId . $siteId . $transactionId);
	}

	/**
	 * Get API Uri
	 * 
	 * Will return the correct uri we have to send the XML request to
	 * 
	 * @access protected
	 * @return M_Uri
	 */
	protected function _getApiUri() {
		// Return the correct uri, based on which mode we are currently
		// working in
		switch($this->getMode()) {
			default:
			case self::MODE_LIVE:
				return new M_Uri($this->_api_uri_live);
				break;
			
			case self::MODE_TESTING:
				return new M_Uri($this->_api_uri_testing);
				break;
		}
	}
	
	/**
	 * Process Error
	 * 
	 * Called when receiving an error message upon requesting the payment
	 * uri, this function will process that error accordingly
	 * 
	 * @access protected
	 * @param M_XmlElement $xml
	 * @return void
	 */
	protected function _processError(M_XmlElement $xml) {
		// Set the error code and description
		$this->_errorCode = $xml->getOneWithXpath('error/code');
		$this->_errorDescription = $xml->getOneWithXpath('error/description');
		
		M_Console::getInstance('Multisafepay')->write($this->getXml());
		M_Console::getInstance('Multisafepay')->write($xml->toXmlSourceCodeString());
		throw new M_Exception(sprintf('Oops.. Multisafepay error: %s - %s', $this->_errorCode, $this->_errorDescription));
	}
}