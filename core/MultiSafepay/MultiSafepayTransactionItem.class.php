<?php
/**
 * M_MultiSafepayTransactionItem
 * 
 * Defines a single item that is part of a {@link MultiSafepayTransaction}.
 * Typically, these items are created from items from a shopping cart.
 * 
 * @package Core
 */
class M_MultiSafepayTransactionItem extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Title
	 * 
	 * Holds the title of this item
	 * 
	 * @see M_MultiSafepayTransactionItem::setTitle()
	 * @see M_MultiSafepayTransactionItem::getTitle()
	 * @access private
	 * @var string
	 */
	private $_title;
	
	/* -- SETTERS -- */
	
	/**
	 * Set Title
	 * 
	 * Allows for setting the title of this item
	 * 
	 * @access public
	 * @param string $title
	 * @return M_MultiSafepayTransactionItem
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Title
	 * 
	 * @see M_MultiSafepayTransactionItem::setTitle()
	 * @access public
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * To String
	 * 
	 * Will create a string from this {@link M_MultiSafepayTransactionItem},
	 * which is typically used for display purposes.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		return $this->getTitle();
	}
}