<?php
/**
 * M_MultiSafepayTransaction
 * 
 * Contains general information about the payment transaction. Typically,
 * one {@link M_MultiSafepay} should contain a single
 * {@link M_MultiSafepayTransaction}, providing it with all the necessary
 * base information, like the amount the client has to pay, the currency used
 * and the transaction's ID.
 * 
 * @package Core
 */
class M_MultiSafepayTransaction extends M_Object {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Currency: Euro
	 * 
	 * @var string
	 */
	const CURRENCY_EUR = 'EUR';
	
	/**
	 * Currency: US Dollar
	 * 
	 * @var string
	 */
	const CURRENCY_USD = 'USD';
	
	/**
	 * Currency: Great British Pounds
	 * 
	 * @var string
	 */
	const CURRENCY_GBP = 'GBP';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Id
	 * 
	 * The unique id of the transaction
	 * 
	 * NOTE: this property is mandatory for correct transaction functionality
	 * 
	 * @see M_MultiSafepayTransaction::setId()
	 * @see M_MultiSafepayTransaction::getId()
	 * @access private
	 * @var string
	 */
	private $_id;
	
	/**
	 * Currency
	 * 
	 * The active currency for the transaction
	 * 
	 * NOTE: this property is mandatory for correct transaction functionality
	 * 
	 * @see M_MultiSafepayTransaction::setCurrency()
	 * @see M_MultiSafepayTransaction::getCurrency()
	 * @access private
	 * @var string
	 */
	private $_currency;
	
	/**
	 * Amount
	 * 
	 * The total amount of the current currency the customer has to pay
	 * with the transaction
	 * 
	 * NOTE: this property is mandatory for correct transaction functionality
	 * 
	 * @see M_MultiSafepayTransaction::setAmount()
	 * @see M_MultiSafepayTransaction::getAmount()
	 * @access private
	 * @var float
	 */
	private $_amount;
	
	/**
	 * Description
	 * 
	 * A short description of the transaction, e.g. "Order #xxxxxx"
	 * 
	 * NOTE: this property is mandatory for correct transaction functionality
	 * 
	 * @see M_MultiSafepayTransaction::setDescription()
	 * @see M_MultiSafepayTransaction::getDescription()
	 * @access private
	 * @var string
	 */
	private $_description;
	
	/**
	 * Items
	 * 
	 * Holds the collection of {@link M_MultiSafepayTransactionItem} objects
	 * that are part of this transaction
	 * 
	 * @see M_MultiSafepayTransaction::addItem()
	 * @see M_MultiSafepayTransaction::getItems()
	 * @access private
	 * @var array
	 */
	private $_items = array();
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_MultiSafepayTransaction
	 */
	public function __construct() {
		// Default the currency to EUR
		$this->setCurrency(self::CURRENCY_EUR);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set Id
	 * 
	 * Allows for setting the unique id of this transaction. Typically, this
	 * is a string also used to uniquely identify the order within the
	 * webshop
	 * 
	 * NOTE: setting this property is mandatory for correct transaction
	 * functionality
	 * 
	 * @access public
	 * @param string $id
	 * @return M_MultiSafepayTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setId($id) {
		$this->_id = (string) $id;
		return $this;
	}
	
	/**
	 * Set Currency
	 * 
	 * Allows for setting the currency type that is to be used for the
	 * transaction. The supported currency types are defined as constants
	 * within this class. For example:
	 * 
	 * - {@link M_MultiSafepayTransaction::CURRENCY_EUR}
	 * - {@link M_MultiSafepayTransaction::CURRENCY_USD}
	 * - {@link M_MultiSafepayTransaction::CURRENCY_GBP}
	 * 
	 * NOTE: setting this property is mandatory for correct transaction
	 * functionality
	 * 
	 * @access public
	 * @param string $currency
	 * @return M_MultiSafepayTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCurrency($currency) {
		// Cast to string before proceeding
		$currency = (string) $currency;
		
		// Make sure the currency type is supported
		if(! in_array($currency, array(self::CURRENCY_EUR, self::CURRENCY_USD, self::CURRENCY_GBP))) {
			throw new M_Exception(sprintf(
				'Unsupported currency type %s given in %s::%s()',
				$currency,
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Set the property
		$this->_currency = $currency;
		
		// And return self
		return $this;
	}
	
	/**
	 * Set Amount
	 * 
	 * Allows for setting the total amount the customer has to pay, specified
	 * in the currently active currency. This amount is specified as a float
	 * including decimals, e.g. €25,99 will be set as follows:
	 * 
	 * <code>
	 *	$transaction = new M_MultiSafepayTransaction();
	 *  $transaction->setAmount(25.99);
	 * </code>
	 * 
	 * We clarify this here specifically, because in MultiSafepay itself,
	 * the amount has to be declared in amount of cents, e.g. 2599 in the
	 * above example. Converting the decimal number to a full integer
	 * happens internally here though, so you do not have to specify the
	 * amount in cents.
	 * 
	 * NOTE: setting this property is mandatory for correct transaction
	 * functionality
	 * 
	 * @access public
	 * @param float $amount
	 * @return M_MultiSafepayTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAmount($amount) {
		$this->_amount = (float) $amount;
		return $this;
	}
	
	/**
	 * Set Description
	 * 
	 * Allows for setting a short description for the transaction. Usually
	 * this description contains something like "Order #xxxxxx"
	 * 
	 * NOTE: setting this property is mandatory for correct transaction
	 * functionality
	 * 
	 * @access public
	 * @param string $description
	 * @return M_MultiSafepayTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
		return $this;
	}
	
	/**
	 * Add Item
	 * 
	 * Allows for adding a single {@link M_MultiSafepayTransactionItem} object
	 * to the transaction. These items are added to the output, and displayed
	 * on MultiSafepay payment page.
	 * 
	 * @access public
	 * @param M_MultiSafepayTransactionItem $item
	 * @return M_MultiSafepayTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function addItem(M_MultiSafepayTransactionItem $item) {
		$this->_items[] = $item;
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Id
	 * 
	 * @see M_MultiSafepayTransaction::setId()
	 * @access public
	 * @return string
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Get Currency
	 * 
	 * @see M_MultiSafepayTransaction::setCurrency()
	 * @access public
	 * @return string
	 */
	public function getCurrency() {
		return $this->_currency;
	}
	
	/**
	 * Get Amount
	 * 
	 * @see M_MultiSafepayTransaction::setAmount()
	 * @access public
	 * @return float
	 */
	public function getAmount() {
		return $this->_amount;
	}
	
	/**
	 * Get Amount For Processing
	 * 
	 * In the XML sent to MultiSafepay, the amount for payment should be
	 * specified in amount of cents, e.g. €25,99 needs to be specified as
	 * an integer with value 2599. This value is also needed to create
	 * the signature needed to make the call, typically created in
	 * {@link M_MultiSafepay::getSignature()}. As a result, this method
	 * will provide the set amount in the said format.
	 * 
	 * @access public
	 * @return int
	 */
	public function getAmountForProcessing() {
		// Make sure the amount is set
		if(is_null($this->getAmount())) {
			throw new M_Exception(spintf(
				'Cannot proceed in %s::%s(): no amount has been specified yet',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Return the correct amount for processing
		// IMPORTANT: note that we first case to a string, to avoid float
		// to int casting issues:
		// http://php.net/manual/en/function.intval.php (post Ben L)
		return (int) (string) ($this->getAmount() * 100);
	}
	
	/**
	 * Get Description
	 * 
	 * @see M_MultiSafepayTransaction::setDescription()
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 * Get Items
	 * 
	 * Will provide the collection of {@link M_MultiSafepayTransactionItem}
	 * objects that are part of this transaction
	 * 
	 * @see M_MultiSafepayTransaction::addItem()
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getItems() {
		return new M_ArrayIterator($this->_items);
	}
	
	/**
	 * Get Xml
	 * 
	 * Will return this {@link M_MultiSafepayTransaction} in XML format, which
	 * is typically used to make the XML request in {@link M_MultiSafepay}
	 * 
	 * @access public
	 * @return string
	 */
	public function getXml() {
		// Before proceeding, make sure the mandatory properties are provided:
		if(! $this->getId()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no transaction id has been set',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(! $this->getCurrency()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no active currency has been defined',
				__CLASS__,
				__FUNCTION__
			));
		}
		if(! $this->getDescription()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(): no description has been set',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Start the output
		$out = '<transaction>';
		
		// Add the properties
		$out .=		'<id>'.				$this->getId()					.'</id>';
		$out .=		'<currency>'.		$this->getCurrency()			.'</currency>';
		$out .=		'<amount>'.			$this->getAmountForProcessing()	.'</amount>';
		$out .=		'<description><![CDATA['.	$this->getDescription()			.']]></description>';
		$out .=		'<var1>'			/* not in use currently */		.'</var1>';
		$out .=		'<var2>'			/* not in use currently */		.'</var2>';
		$out .=		'<var3>'			/* not in use currently */		.'</var3>';
		
		// Open the items tag
		$out .=		'<items><![CDATA[';
		
		// If we received exactly 1 item
		if($this->getItems()->count() == 1) {
			// Then add that item
			$out .=		$this->getItems()->first()->toString();
		}
		// Otherwise, if more than one item is specified
		elseif($this->getItems()->count() > 1) {
			// Then we have to display them in an unordered list. Open it:
			$out .=		'<ul>';
			
			// Loop through the items
			foreach($this->getItems() as $item) {
				/* @var $item M_MultiSafepayTransactionItem */
				// And add each to the output
				$out .=		'<li>'. $item->toString() .'</li>';
			}
			
			// Close the unordered list
			$out .=		'</ul>';
		}
		
		// Close the items tag
		$out .=		']]></items>';
		
		// Add more properties
		$out .=		'<manual>false'		/* not in use currently */		.'</manual>';
		$out .=		'<gateway>'			/* not in use currently */		.'</gateway>';
		$out .=		'<daysactive>'		/* not in use currently */		.'</daysactive>';
		
		// Close the output
		$out .= '</transaction>';
		
		// Return return the output
		return $out;
	}
}