<?php
/**
 * Abstract class M_ViewPageElement
 * 
 * This class is created in order to provide shared functionality between
 * {@link M_ViewCss} and {@link M_ViewJavascript}
 * 
 * It stores inline and file content for css as well as javascript files. By
 * storing it before outputting we can gather all javascript and output it on
 * the end of the file. This way we have more control on both css and javascript
 * sourcecode
 */
abstract class M_ViewPageElement extends M_Object {
	
	/**
	 * Holds the inline data
	 * 
	 * @var array
	 */
	protected $_inlineData = array();

	/**
	 * Holds all file data
	 * 
	 * @var array
	 */
	protected $_fileData = array();
	
	
	/**
	 * Holds all external data
	 * 
	 * @var array
	 */
	protected $_externalData = array();
	
	/**
	 * @const string
	 */
	const CONDITION_IE = 'IE';
	
	/**
	 * @const string
	 */
	const CONDITION_IE6 = 'IE 6';
	
	/**
	 * @const string
	 */
	const CONDITION_IE7 = 'IE 7';
	
	/**
	 * @const string
	 */
	const CONDITION_GTE_IE6 = 'gte IE 6';
	
	/**
	 * @const string
	 */
	const CONDITION_GTE_IE7 = 'gte IE7';
	
	/**
	 * @const string
	 */
	const CONDITION_LTE_IE6 = 'lte IE 6';
	
	/**
	 * @const string
	 */
	const CONDITION_LTE_IE7 = 'lte IE 7';
	
	/**
	 * @const string
	 */
	const CONDITION_GT_IE6 = 'gt IE 6';
	
	/**
	 * @const string
	 */
	const CONDITION_LT_IE7 = 'lt IE 7';
	
    /**
     * Clone function
     * 
     * Note: this will throw an error since this cloning is not allowed for a
     * singleton
     */
    public function __clone() {
    	throw new M_Exception('Clone is not allowed for singleton');
    }
    
	/**
	 * Add inline data
	 * 
	 * @param string
	 * @param string $context
	 * @param string $condition
	 * @return M_ViewPageElement
	 */
    public function addInline($str, $context = null, $condition = null) {
    	$this->_addData((string)$str,$context,$condition,false);
    	return $this;
    }
    
	/**
	 * Add external URL
	 * 
	 * @param M_Uri
	 * @param string $context
	 * @param string $condition
	 * @return M_ViewPageElement
	 */
    public function addExternal(M_Uri $uri, $context = null, $condition = null) {
    	$this->_addData($uri,$context,$condition,false);
    	return $this;
    }
    
    /**
	 * Add file
	 * 
	 * @param M_File
	 * @param string $context
	 * @param string $condition
	 * @return M_ViewPageElement
	 */
    public function addFile(M_File $file, $context = null, $condition = null) {
    	//check if the path is a file
    	if (is_file($this->_getPathWithoutQueryVariables($file->getPath())) == false) {
    		throw new M_Exception(
    			sprintf('Failed to add file %s', $file->getPath())
    		);
    	}
    	
    	$this->_addData($file,$context,$condition,false);
		
    	return $this;
    }
    
    /**
     * Add final inline data
     * 
     * @param string
     * @param string $context
     * @param string $condition
     * @return M_ViewPageElement
     */
    public function addInlineFinal($str, $context, $condition = null) {
    	$this->_addData((string)$str,$context,$condition, true);
    	return $this;
    }
    
    /**
     * Add final file data
     * 
     * @param M_File
     * @param string $context
     * @param string $condition
     * @return M_ViewPageElement
     */
    public function addFileFinal(M_File $file, $context,$condition = null) {
    	$this->_addData($file,$context,$condition, true);
    	return $this;
    }
    
    /**
     * Check if data has already been stored under the given context
     * 
     * @param string $context
     * @return bool
     */
    public function isContextStored($context) {
    	return (array_key_exists($context, $this->_inlineData) || array_key_exists($context, $this->_fileData));
    }
    
    /**
     * Check if data has already been stored and defined as final. This means
     * it cannot be overwritten
     * 
     * @param string $context
     * @return bool
     */
    public function isContextStoredFinal($context) {
    	$storedFinal = false;
    	
    	if ($this->isContextStored($context)) {
    		//check inline first
    		$data = M_Helper::getArrayElement($context, $this->_inlineData);
    		
    		//if not found, it should exist in file data
    		if (is_null($data)) {
	    		$data = M_Helper::getArrayElement($context, $this->_fileData);
    		}
    		
    		return $storedFinal = $data['final'];
    	}
    	
    	return $storedFinal;
    }
    
    /**
     * Get conditions (aka conditional comments)
     * 
     * Note: conditions are only supported by Internet explorer
     * 
     * @return array
     */
    public function getConditions() {
    	return array(
    		self::CONDITION_GT_IE6,
    		self::CONDITION_GTE_IE6,
    		self::CONDITION_GTE_IE7,
    		self::CONDITION_IE,
    		self::CONDITION_IE6,
    		self::CONDITION_IE7,
    		self::CONDITION_LT_IE7,
    		self::CONDITION_LTE_IE6,
    		self::CONDITION_LTE_IE7
    	);
    }
    
    /**
     * Reset all inline data
     * 
     * @return M_ViewPageElement
     */
    public function resetInlineData() {
    	$this->_inlineData = array();
    	return $this;
    }
    
    /**
     * Return all file data
     * 
     * @return M_ViewPageElement
     */
    public function resetFileData() {
    	$this->_fileData = array();
    	return $this;
    }
    
    public function resetExternalData() {
    	 $this->_externalData = array();
    	return $this;
    }
    
    /**
     * Reset all data which has been stored
     * 
     * @return M_ViewPageElement
     */
    public function reset() {
    	$this->resetInlineData()->resetFileData()->resetExternalData();
    	return $this;
    }
    
	/**
     * Get all stored data and return as a string
     * 
     * @return string
     */
    public function toString() {
    	$nl = M_CodeHelper::getNewLine(1);

    	//output
		$output = '';
		    	
    	//first of all we try to find all conditional data
    	$conditionalData = array();
        foreach($this->_externalData AS $key => $data) {
    		if (!is_null($data['condition'])) {
    			$conditionalData[$data['condition']] = $data;
    			unset($this->_fileData[$key]);
    		}
    	}
    	foreach($this->_fileData AS $key => $data) {
    		if (!is_null($data['condition'])) {
    			$conditionalData[$data['condition']] = $data;
    			unset($this->_fileData[$key]);
    		}
    	}
    	foreach($this->_inlineData AS $key => $data) {
    		if (!is_null($data['condition'])) {
    			$conditionalData[$data['condition']] = $data;
    			unset($this->_inlineData[$key]);
    		}
    	}
    	
    	if (count($conditionalData) > 0) {
        	//sort by condition
        	ksort($conditionalData);
        	
        	$previousCondition = false;
        	foreach($conditionalData AS $condition => $data) {
    
        		//check if we need to open/close the condition
        		if ($condition != $previousCondition) {
        			//close (don't do this for the first one)
        			if ($previousCondition) {
        				$output .= $nl . '<![endif]-->' . $nl;
        			}
    	    		
        			//open
    	    		$output .= $nl . '<!--[if '.$condition.']>';
        		}
        		
        		//append output
        		$output .= $this->_getCode($data['data'], $data['media']);
        		
        		//store previous-condition
        		$previousCondition = $condition;
        	}
        	
        	//close last conditional comment
        	if (count($conditionalData) > 0) $output .= $nl . '<![endif]-->';
    	}
    	
    	//get all other external data
        foreach ($this->_externalData AS $data) {
    		$output .= $this->_getExternalCode($data['data'], $data['media']);
    	}
    	
    	//get all other file data
    	foreach ($this->_fileData AS $data) {
    		$output .= $this->_getFileCode($data['data'], $data['media']);
    	}
    	
    	//get all other inline data
    	if (count($this->_inlineData) > 0) {
	    	$output .= $nl . $this->_getInlineStartingTag();
    		foreach($this->_inlineData AS $data) {
    			$output .= $nl . $data['data'];
    		}
	    	$output .= $nl . $this->_getInlineClosingTag();
    	}
    	
    	return $output;
    }
    
    /**
     * Magic toString
     * 
     * @see toString()
     * @return string
     */
    public function __toString() {
    	return $this->toString();
    }
    
    /**
     * Add data to the data-containers
     * 
     * All data is stored in format
     * 
     * array(
     * 	'data' => 'the data to store',
     *  'condition' => 'conditional comment',
     * 	'final' => boolean
     * )
     * 
     * When context is provided it's stored as the key of the array
     * 
     * @param mixed $mixed
     * 		The data to store, this can be one of M_File or string
     * @param string $context
     * 		The context in which this data should be stored
     * @param string $condition
     * 		The condition (conditional comment)
     * @param bool $final
     * 		Should this data be stored as a final value (no override allowed)
	 * @param string $media
	 *		Use this to determine for which media types this data is meant (only
	 *		applicable for css data)
     */
    protected function _addData($mixed, $context, $condition, $final, $media = null) {
        //check condition
    	if (!is_null($condition) && !in_array($condition,$this->getConditions())) {
    		throw new M_Exception(sprintf(
    			'Unknown condition %s when adding page element',
    			$condition)
    		);
    	}
    	
    	//the data which we want to store
    	$storageData = array(
    		'data' => $mixed, 
    		'condition' => $condition,
    		'final' => $final,
			'media' => $media
    	);
    	
    	//we want to store data whith a context
    	if (!is_null($context)) {
    		//check if the container already has final data 
    		if ($final == true && $this->isContextStoredFinal($context)) {
    			throw new M_Exception(
    				sprintf('Cannot add inline data for context "%s", already stored before', $context));
    		}
    		//check if this is valid context variable
    		if (strlen((string)$context) == 0) {
    			throw new M_Exception('Cannot add date with empty context-string');
    		}
    		
    		//store the data
    		if ($mixed instanceof M_File) {
    			$this->_fileData[(string)$context] = $storageData;
    		} else if ($mixed instanceof M_Uri) {
    			$this->_externalData[(string)$context] = $storageData;
    		}
    		else {
    			$this->_inlineData[(string)$context] = $storageData;
    		}
    	}
    	
    	//no context is provided
    	else {
    		//check if one tried to add final non-context data
    		if ($final !== false) {
    			throw new M_Exception('Cannot add final page element when context is NULL');
    		}
    		
    		//store in correct data-container
    		if ($mixed instanceof M_File) {
    			$this->_fileData[] = $storageData;
    		} else if($mixed instanceof M_Uri) {
    			$this->_externalData[] = $storageData;
    		} else {
    			$this->_inlineData[] = $storageData;
    		}
    	}
    }
    
	/**
     * Get formatted (x)html code for a set of data
     * 
     * Array contains data in format of
     * 
     * array(
     * 	'data' => 'the data to store',
     *  'condition' => 'conditional comment',
     * 	'final' => boolean
     * )
     * @param array
     * 		Contains inline as well as file data
     * @return string
     */
    protected function _getCode($data) {
    	if ($data instanceof M_File) {
    		$code = $this->_getFileCode($data);
    	} else if ($data instanceof M_Uri) {
    		$code = $this->_getExternalCode($data);
    	} else {
    		$code = $this->_getInlineCode($data);
    	}
    	
    	return $code;
    }

	/**
	 * Get a path without query variables
	 *
	 * Used to check whether a file exists, we need to remove the query variables
	 * (which may have been added for cache busting)
	 *
	 * @param string $path
	 * @return string
	 */
	protected function _getPathWithoutQueryVariables($path) {
		//if cache busting query variables have been added, remove them
		$qPos = strpos($path, '?');
		if($qPos) {
			$path = substr($path, 0, $qPos);
		}

		return $path;
	}
    
    /**
     * Get inline data formatted in valid (x)html code
     * 
     * @param string $data
     * @return string
     */
	abstract protected function _getInlineCode($data);
    
	/**
     * Get file data formatted in valid (x)html code
     * 
     * @param M_File $file
     * @return string
     */
    abstract protected function _getFileCode(M_File $file);
    
    /**
     * Get file data formatted in valid (x)html code
     * 
     * @param M_File $file
     * @return string
     */
    abstract protected function _getExternalCode(M_Uri $uri);
    
    /**
	 * Defines the starting tag for inline content
	 * 
	 * @return string
	 */
	abstract protected function _getInlineStartingTag();
	
    /**
	 * Defines the closing tag for inline content
	 * 
	 * @return string
	 */
	abstract protected function _getInlineClosingTag();
}