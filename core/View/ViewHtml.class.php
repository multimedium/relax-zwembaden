<?php
/**
 * M_ViewHtml class
 * 
 * @package Core
 */
abstract class M_ViewHtml extends M_View {
	
	const FOLDER_CSS = 'css';
	const FOLDER_JAVASCRIPT = 'javascript';
	const FOLDER_IMAGES = 'images';
	const FOLDER_FLASH = 'flash';
	const FOLDER_XML = 'xml';
	const FOLDER_TEMPLATES = 'templates';
	
	
	/**
	 * The (unique) ID of the view
	 * 
	 * Each instance of M_View gets a unique ID assigned to it. This
	 * allows for javascript to address the generated view elements
	 * by their ID.
	 * 
	 * @access protected
	 * @see M_View::_getUniqueId()
	 * @see M_View::getId()
	 * @var string
	 */
	protected $_id;
	
	/**
	 * Presentation variables
	 * 
	 * This property holds the presentation variables that are being
	 * assigned to the view with {@link M_View::assign()}.
	 * 
	 * @access protected
	 * @see M_View::assign()
	 * @var array
	 */
	protected $_variables = array();
	
	/**
	 * Counter
	 * 
	 * A static counter is maintained in M_View. This counter helps
	 * us in generating a unique ID for each and every view that is
	 * rendered in the interface.
	 * 
	 * @access private
	 * @static
	 * @see M_View::$_id
	 * @var integer
	 */
	private static $_counter = 0;
	
	/**
	 * Persistent variables
	 * 
	 * Persistent variables are a special type of presentation 
	 * variables. Once assigned by {@link M_View::assignPersistent()},
	 * they are available in each M_View object that is rendered
	 * afterwards.
	 * 
	 * @access private
	 * @static
	 * @see M_View::assignPersistent()
	 * @var integer
	 */
	private static $_persistentVariables = array();
	
	/**
	 * Constructor
	 * 
	 * Note that the constructor of the abstract M_View class has
	 * been signed as final (cannot be overriden by subclasses). The
	 * constructor does some critical preparing of internal resources.
	 * 
	 * The M_View constructor accepts an (optional) {@link M_Theme} 
	 * object as argument. If given, M_View will request a template
	 * from that theme, with {@link M_Theme::getTemplate()}. If not
	 * given, M_View will request a template from the theme that has
	 * been set previously with {@link M_View::setTheme()}. If none of
	 * the mentioned themes are available, M_View will simply rely on
	 * the view's implementation of {@link M_View::getHtml()}, to 
	 * render the final HTML source code.
	 * 
	 * @access public
	 * @return M_View
	 */
	public function __construct() {
		$this->assign('persistent', self::$_persistentVariables);
	}
	
	/**
	 * Get ID
	 * 
	 * This method will return the ID that has been assigned to the
	 * view by {@link M_View::_getUniqueId()} or {@link M_View::setId()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getId() {
		if(!$this->_id) {
			$this->_id = $this->_getUniqueId();
		}
		return $this->_id;
	}
	
	/**
	 * Set ID
	 * 
	 * This method will set the View's ID. For more information, see
	 * 
	 * - {@link M_View::$_id}
	 * - {@link M_View::_getUniqueId()}
	 * - {@link M_View::getId()}
	 * 
	 * @access public
	 * @param string $id
	 * @return string
	 */
	public function setId($id) {
		$this->_id = (string) $id;
	}
	
	/**
	 * Assign a variable
	 * 
	 * This method will assign a presentation variable to the view.
	 * 
	 * (extracted from www.smarty.net):
	 * This method is crucial to facilitate a manageable way to 
	 * separate application logic and content from its presentation. 
	 * This is best described in a situation where the application 
	 * programmer and the template designer play different roles, or 
	 * in most cases are not the same person.
	 * 
	 * For example, let's say you are creating a web page that is 
	 * displaying a newspaper article.
	 * 
	 * The article $headline, $tagline, $author and $body are content 
	 * elements, they contain no information about how they will be 
	 * presented. They are passed into the view by the application.
	 * 
	 * Then the template designer edits the templates and uses a 
	 * combination of HTML tags and template tags to format the 
	 * presentation of these variables with elements such as tables, 
	 * div's, background colors, font sizes, style sheets, svg etc.
	 * 
	 * One day the programmer needs to change the way the article 
	 * content is retrieved, ie a change in application logic. This 
	 * change does not affect the template designer, the content will 
	 * still arrive in the template exactly the same.
	 * 
	 * Likewise, if the template designer wants to completely redesign 
	 * the templates, this would require no change to the application 
	 * logic.
	 * 
	 * Therefore, the programmer can make changes to the application 
	 * logic without the need to restructure templates, and the 
	 * template designer can make changes to templates without 
	 * breaking application logic.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the presentation variable. Templates will
	 * 		address the value by using this name.
	 * @param mixed $value
	 * 		The value of the variable
	 * @return void
	 */
	public final function assign($name, $value) {
		$this->_variables[$name] = $value;
		return $this;
	}
	
	/**
	 * Get presentation variable
	 * 
	 * This method is used to fetch the value of a presentation 
	 * variable, previously assigned by {@link M_View::assign()}.
	 * Note that, if the variable could not have been found (and thus
	 * no corresponding value is available), this method will return 
	 * the default value (second argument to this method).
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the presentation variable
	 * @param mixed $defaultValue
	 * 		The default value, which is returned if no value could
	 * 		have been found for the requested presentation variable.
	 * @return mixed
	 */
	public function getVariable($name, $defaultValue = FALSE) {
		return (isset($this->_variables[$name]) ? $this->_variables[$name] : $defaultValue);
	}
	
	/**
	 * Assign a persistent variable
	 * 
	 * This method is very much like {@link M_View::assign()}, but 
	 * persistent variables are a special type of presentation 
	 * variables. Once assigned to the presentation layer by this 
	 * method, the values are maintained in each M_View object that 
	 * is rendered afterwards.
	 * 
	 * NOTE:
	 * Persistent variables are addressed in a separate "namespace".
	 * Check out Example 1 to learn more.
	 * 
	 * Example 1
	 * <code>
	 *    $view = new M_ViewTest;
	 *    $view->assign('title', 'Hello');
	 *    $view->assign('name',  'John Doe');
	 *    $view->display();
	 * </code>
	 * 
	 * In Example 1, the template file can address the presentation 
	 * variables as follows:
	 * 
	 * <code>
	 *    <h1>{$title} {$persistent.name}</h1>
	 * </code>
	 * 
	 * Note that, from now on, the persistent variable "name" will be
	 * available in any view we construct. Assume that the code in
	 * Example 2 comes somewhere after the code in Example 1:
	 * 
	 * Example 2
	 * <code>
	 *    $view = new M_ViewTest2;
	 *    $view->display();
	 * </code>
	 * 
	 * In Example 2, the template file can address the following
	 * presentation variables, even though no variables have been
	 * assigned to it directly:
	 * 
	 * <code>
	 *    {$persistent.name}
	 * </code>
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the persistent variable. Templates will
	 * 		address the value by using this name.
	 * @param mixed $value
	 * 		The value of the variable
	 * @return void
	 */
	public static final function assignPersistent($name, $value) {
		self::$_persistentVariables[$name] = $value;
	}
	
	/**
	 * Get persistent variables
	 * 
	 * This method will return the persistent variables that have been
	 * assigned previously with {@link M_View::assignPersistent()}.
	 * Note that this method will return an ArrayIterator object, of
	 * which the keys are the variable names.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public static final function getPersistentVariables() {
		return new ArrayIterator(self::$_persistentVariables);
	}
	
	/**
	 * Get persistent variable
	 * 
	 * This method will return the value of a given (persistent)
	 * variable. Note that, if the variable could not have been found 
	 * (and thus no corresponding value is available), this method 
	 * will return the default value (second argument to this method).
	 * 
	 * @access public
	 * @see M_View::getPersistentVariables()
	 * @see M_View::getVariable()
	 * @param string $name
	 * 		The name of the persistent variable.
	 * @param mixed $defaultValue
	 * 		The default value
	 * @return mixed
	 */
	public static final function getPersistentVariable($name, $defaultValue = FALSE) {
		return isset(self::$_persistentVariables[$name]) ? self::$_persistentVariables[$name] : $defaultValue;
	}
	
	/**
	 * Fetch the view
	 * 
	 * This method will render the HTML source code of the view, and
	 * return it to the requesting code.
	 * 
	 * @access public
	 * @uses M_View::_render()
	 * @see M_View::display()
	 * @return string
	 */
	public function fetch() {
		return $this->_render();
	}
	
	/**
	 * Display the view
	 * 
	 * This method will render the HTML source code of the view, and
	 * output it to the browser. This function does not play with
	 * output buffers, it just echo's the final HTML source code.
	 * 
	 * @access public
	 * @uses M_View::_render()
	 * @see M_View::fetch()
	 * @return void
	 */
	public function display() {
		echo $this->_render();
	}
	
	/**
	 * Return the file path for a template file
	 * 
	 * To use a file located in a template folder you can use this method: it
	 * will automatically generate a path to the template-folder, possibly
	 * in a specific module
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getTemplatesPath($module = null)
	{
		return M_Loader::getResourcesPath($module) . '/' . self::FOLDER_TEMPLATES;
	}
	
	/**
	 * Return the uri for a css file
	 * 
	 * To use a file located in a css folder you can use this method: it
	 * will automatically generate a path to the css-folder, possibly
	 * in a specific module
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getCssHref($module = null)
	{
		return M_Loader::getResourcesHref($module) . '/' . self::FOLDER_CSS;
	}
	
	/**
	 * Return the uri for css files
	 * 
	 * To use a file located in a css folder you can use this method: it
	 * will automatically generate a path to the css-folder, possibly
	 * in a specific module
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getImagesHref($module = null)
	{
		return M_Loader::getResourcesHref($module) . '/' . self::FOLDER_IMAGES;
	}
	
	/**
	 * Return the uri for javascript files
	 * 
	 * To use a file located in a javascript folder you can use this method: it
	 * will automatically generate a path to the javascript-folder, possibly
	 * in a specific module
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getJavascriptHref($module = null)
	{
		return M_Loader::getResourcesHref($module) . '/' . self::FOLDER_JAVASCRIPT;
	}
	
	/**
	 * Return the uri for flash files
	 * 
	 * To use a file located in a flash folder you can use this method: it
	 * will automatically generate a path to the flash-folder, possibly
	 * in a specific module
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getFlashHref($module = null)
	{
		return M_Loader::getResourcesHref($module) . '/' . self::FOLDER_FLASH;
	}
	
	/**
	 * Return the uri for xml files
	 *
	 * To use a file located in a xml folder you can use this method: it
	 * will automatically generate a path to the xml-folder, possibly
	 * in a specific module
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getXmlHref($module = null)
	{
		return M_Loader::getResourcesHref($module) . '/' . self::FOLDER_XML;
	}

	/**
	 * Render HTML
	 * 
	 * This (protected) method renders the HTML source code of the view.
	 * The result of this method is used by:
	 * 
	 * - {@link M_View::fetch()}
	 * - {@link M_View::display()}
	 * 
	 * This method will do the following:
	 * 
	 * - pre-processing
	 * - HTML source code rendering (template or {@link M_View::getHtml()}
	 * - post-processing
	 * 
	 * Read the intro of {@link M_View} to learn more.
	 * 
	 * @access protected
	 * @return string
	 */
	protected function _render() {
		// Pre-processing
		if(method_exists($this, 'preProcessing')) {
			$this->preProcessing();
		}
		
		// Get HTML resource file (template):
		$r = $this->getResource();
		
		// Now, we assign all presentation variables to the template:
		foreach($this->_variables as $name => $variable) {
			$r->assign($name, $variable);
		}
		
		// Now, we assign all persistent presentation variables to the 
		// template:
		foreach(self::$_persistentVariables as $name => $variable) {
			$r->assign($name, $variable);
		}

		// Assign all persistents in an array
		$r->assign('persistent', self::$_persistentVariables);
		
		// Assign the view's unique ID to the template
		$r->assign('viewId', $this->getId());
		
		// Get the final output of the template:
		$html = $r->fetch();
		
		// Post-processing
		if(method_exists($this, 'postProcessing')) {
			$html = $this->postProcessing($html);
		}

		// return final render
		return $html;
	}
	
	/* -- PHP Magic Methods -- */
	
	/**
	 * Export to string
	 * 
	 * This is a "magic method", called automatically by PHP when the
	 * M_View object is being casted to a string. For example, an echo 
	 * call on the M_View object will print the result of this 
	 * function.
	 * 
	 * Example 1
	 * <code>
	 *    $view = new M_ViewTabs;
	 *    echo $view; // prints the result of __toString()
	 * </code>
	 * 
	 * Example 2
	 * <code>
	 *    $view = new M_ViewTabs;
	 *    $str  = (string) $view; // saves the result of __toString()
	 * </code>
	 * 
	 * This method will return the result of {@link M_View::_render()}.
	 * In other words, this methodology can be used as an alternative 
	 * to the method {@link M_View::fetch()}.
	 * 
	 * Example 3 (has exactly the same result as Example 1)
	 * <code>
	 *    $view = new M_ViewTabs;
	 *    echo $view->fetch();
	 * </code>
	 * 
	 * @access public
	 * @return string
	 */
	public function __toString() {
		try {
			$out = $this->_render();
		}
		catch(Exception $e) {
			return $e->getMessage();
		}
		
		return $out;
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Get unique ID
	 * 
	 * This (private) method generates a unique ID for the view. Each 
	 * instance of M_View gets a unique ID assigned to it. This
	 * allows, for example, for javascript to address the generated 
	 * view elements by their ID.
	 * 
	 * To fetch the ID of the view, use {@link M_View::getId()}.
	 * 
	 * @access public
	 * @return string
	 */
	private function _getUniqueId() {
		return 'view-' . (++ self::$_counter); 
	}
	
	/* -- ABSTRACT -- */
	
	/**
	 * Get template
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	abstract protected function getResource();
}