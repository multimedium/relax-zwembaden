<?php
/**
 * M_ViewResource class
 * 
 * The M_ViewHtmlResource class is typically used by {@link M_ViewHtml} classes,
 * in order to render the view with a template file. Currently, M_ViewHtmlResource
 * uses the Smarty Template Engine.
 *
 * @package Core
 */

// Set default timezone:
// (the Smarty_Compiler uses date functions, to compose filenames of compiled
// templates. We default the timezone, if not already done, to avoid notices)
if(! M_DateTimezone::isDefaultTimezoneAvailable()) {
	M_DateTimezone::setDefaultTimezone('Europe/Brussels');
}

// Load Smarty
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . FOLDER_THIRDPARTY . DIRECTORY_SEPARATOR . 'smarty' . DIRECTORY_SEPARATOR . 'Smarty.class.php';

/**
 * M_ViewResource class
 * 
 * The M_ViewHtmlResource class is typically used by {@link M_ViewHtml} classes,
 * in order to render the view with a template file. Currently, M_ViewHtmlResource
 * uses the Smarty Template Engine.
 *
 * @package Core
 */
class M_ViewHtmlResource extends M_Object {
	/**
	 * Templates' Base Path
	 * 
	 * This property stores the base path, where M_ViewHtmlResource will look
	 * for the requested template file, when constructed with the constructor
	 * {@link M_ViewHtmlResource::constructWithTemplateBasePath()}.
	 * 
	 * @static
	 * @see M_ViewHtmlResource::setTemplateBasePath()
	 * @see M_ViewHtmlResource::getTemplateBasePath()
	 * @access private
	 * @var string
	 */
	private static $_templateBasePath;
	
	/**
	 * Templates' Base Path, for module-specific templates
	 * 
	 * This property stores the base path, where M_ViewHtmlResource will look
	 * for the requested template file, when constructed with the constructor
	 * {@link M_ViewHtmlResource::constructWithModuleId()}.
	 * 
	 * @static
	 * @see M_ViewHtmlResource::setTemplateBasePathModule()
	 * @see M_ViewHtmlResource::getTemplateBasePathModule()
	 * @access private
	 * @var string
	 */
	private static $_templateBasePathModule;

	/**
	 * Holds additional pluginsdirectories
	 * 
	 * @var array
	 */
	private static $_pluginsDirectories = array();
	
	/**
	 * Smart Template
	 * 
	 * This property stores a smarty template, which is represented by an 
	 * instance of {@link Smarty}.
	 * 
	 * @access private
	 * @var Smarty
	 */
	private $_template;
	
	/**
	 * Smarty filename
	 * 
	 * This property stores the filename of the template file.
	 * 
	 * @access private
	 * @var Smarty
	 */
	private $_filename;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $filename
	 * 		The template filename
	 * @param M_Directory
	 *		You may specify a custom templates-compiled directory if required
	 * @return M_ViewHtmlResource
	 */
	public function __construct($filename, M_Directory $defaultDirectoryTemplatesCompiled = null) {
		// Set the filename
		$this->_filename = $filename;
		
		// Construct the internal smarty resource:
		$this->_template = new Smarty;
		$this->_template->setTemplateDir(
			pathinfo($filename, PATHINFO_DIRNAME)
		);
		
		// We set the full path to the directory where the compiled template is 
		// stored (we store all compiled templates in the same folder). If
		// the directory we have to use was not provided:
		if(! $defaultDirectoryTemplatesCompiled) {
			// Then create it ourselves
			$defaultDirectoryTemplatesCompiled = new M_Directory(
				M_Loader::getAbsolute(
					M_Loader::getResourcesPath() . '/templates-compiled/' . md5($filename)
				)
			);
		}
		
		// If the directory does not yet exist, we create it now:
		if(! $defaultDirectoryTemplatesCompiled->exists()) {
			// Create the directory:
			$defaultDirectoryTemplatesCompiled->make();
			
			// Also, we set permissions, so we can write to the directory:
			$defaultDirectoryTemplatesCompiled->setPermissions(
				M_FsPermissions::constructWithString('drwxrw-rw-')
			);
		}
		
		// Set the compiled directory path
		$this->_template->setCompileDir(
			$defaultDirectoryTemplatesCompiled->getPath()
		);
		
		// Also, add a directory with plugin functions:
		$this->_template->addPluginsDir(
			M_Loader::getResourcesPath() . '/templates-plugins'
		);
		$this->_template->addPluginsDir(
			M_Loader::getAbsolute('core/'.FOLDER_THIRDPARTY.'/smarty-plugins')
		);

		// And check if additional directories have been set
		foreach(self::$_pluginsDirectories AS $path) {
			$this->_template->addPluginsDir($path);
		}

		// Disable caching of the template file:
		//$this->_template-> = 0;
		$this->_template->setCacheDir(
			M_Loader::getAbsolute(M_Loader::getResourcesPath() . '/templates-cached')
		);
	}
	
	/**
	 * Enable/disable caching
	 * 
	 * @param bool $arg
	 * @return M_ViewHtmlResource
	 */
	public function setCaching($arg) {
		$this->_template->caching = (int)$arg;
		return $this;
	}
	
	/**
	 * Set the cache lifetime (in seconds)
	 * 
	 * Defaults to 3600 seconds
	 * 
	 * @param int $arg
	 * @return M_ViewHtmlResource
	 */
	public function setCacheLifetime($arg) {
		$this->_template->cache_lifetime = (int)$arg;
		return $this;
	}
	
	/**
	 * Is this template being cached?
	 * 
	 * @return bool 
	 */
	public function isCached() {
		return $this->_template->caching;
	}
	
	/**
	 * Construct with Module ID and filename
	 * 
	 * This (static) method will construct an instance of {@link M_ViewHtmlResource},
	 * given a Module ID, and template filename.
	 * 
	 * Example 1
	 * <code>
	 *    $resource = M_ViewHtmlResource::constructWithModuleId('contact', 'ThankYou.tpl');
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @see M_ViewHtml::getTemplatesPath();
	 * @param string $moduleId
	 * 		The Module ID; see {@link M_ApplicationModule::getId()}
	 * @param string $filename
	 * 		The template filename
	 * @return M_ViewHtmlResource
	 */
	public static function constructWithModuleId($moduleId, $filename) {
		return new self(strtr(self::getTemplateBasePathModule(), array('@module' => $moduleId)) . '/' . $filename);
	}
	
	/**
	 * Construct with Templates' Base Path
	 * 
	 * This (static) method will construct an instance of {@link M_ViewHtmlResource},
	 * given a filename. It will look for the file in the Templates' Base Path.
	 * For more info, read:
	 * 
	 * - {@link M_ViewHtmlResource::setTemplateBasePath()}
	 * - {@link M_ViewHtmlResource::getTemplateBasePath()}
	 * 
	 * @static
	 * @access public
	 * @see M_ViewHtmlResource::setTemplateBasePath()
	 * @uses M_ViewHtmlResource::getTemplateBasePath()
	 * @param string $filename
	 * 		The template filename
	 * @return M_ViewHtmlResource
	 */
	public static function constructWithTemplateBasePath($filename) {
		return new self(self::getTemplateBasePath() . '/' . $filename);
	}
	
	/**
	 * Set Templates' Base Path
	 * 
	 * Will set the base path, where M_ViewHtmlResource will look for the requested 
	 * template file, if the instance has been constructed with
	 * {@link M_ViewHtmlResource::constructWithTemplateBasePath()}.
	 * Consider the following example:
	 * 
	 * <code>
	 *    // First, set the base path:
	 *    M_ViewHtmlResource::setTemplateBasePath('/theme/default/');
	 *    
	 *    // Now, we request a new resource:
	 *    // The template file will be /theme/default/Page.tpl
	 *    $rs = M_ViewHtmlResource::constructWithTemplateBasePath('Page.tpl');
	 * </code>
	 * 
	 * @access public
	 * @param string $path
	 * @return void
	 */
	public static function setTemplateBasePath($path) {
		self::$_templateBasePath = M_Helper::rtrimCharlist((string) $path, '/');
	}
	
	/**
	 * Get Templates' Base Path
	 * 
	 * Will provide with the base path that may have been set previously with
	 * {@link M_ViewHtmlResource::setTemplateBasePath()}
	 * 
	 * NOTE:
	 * Note that, if no templates base path has been defined previously with
	 * {@link M_ViewHtmlResource::setTemplateBasePath()}, this method will
	 * return the following path by default:
	 * 
	 * - {@link M_ViewHtml::getTemplatesPath()}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getTemplateBasePath() {
		if(self::$_templateBasePath) {
			return self::$_templateBasePath;
		} else {
			return M_Loader::getAbsolute(
				M_ViewHtml::getTemplatesPath()
			);
		}
	}
	
	/**
	 * Set Templates' Base Path, for module-specific templates
	 * 
	 * Will do the same thing as {@link M_ViewHtmlResource::setTemplateBasePath()},
	 * but will set the base path where M_ViewHtmlResource will look for the 
	 * requested template file, when constructed with the constructor
	 * {@link M_ViewHtmlResource::constructWithModuleId()}.
	 * 
	 * In order to set the (variable) Module ID in the base path, you can use
	 * the string @module.
	 * 
	 * Consider the following example:
	 * 
	 * <code>
	 *    // First, set the base path per module:
	 *    M_ViewHtmlResource::setTemplateBasePathModule('/theme/default/@module/');
	 *    
	 *    // Now, we request a new resource:
	 *    // The template file will be /theme/default/contact/Thankyou.tpl
	 *    $rs = M_ViewHtmlResource::constructWithModuleId('contact', 'Thankyou.tpl');
	 * </code>
	 * 
	 * @access public
	 * @param string $path
	 * @return void
	 */
	public static function setTemplateBasePathModule($path) {
		self::$_templateBasePathModule = M_Helper::rtrimCharlist((string) $path, '/');
	}
	
	/**
	 * Get Templates' Base Path, for module-specific templates
	 * 
	 * Will provide with the base path that may have been set previously with
	 * {@link M_ViewHtmlResource::setTemplateBasePathModule()}
	 * 
	 * NOTE:
	 * Note that, if no templates base path has been defined previously with
	 * {@link M_ViewHtmlResource::setTemplatesBasePathModule()}, this method will
	 * return the following path by default:
	 * 
	 * - {@link M_ViewHtml::getTemplatesPath('@module')}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getTemplateBasePathModule() {
		if(self::$_templateBasePathModule) {
			return self::$_templateBasePathModule;
		} else {
			return M_Loader::getAbsolute(
				M_ViewHtml::getTemplatesPath('@module')
			);
		}
	}

	/**
	 * Add a plugins directory
	 *
	 * You can add a directory in which you can use template plugins. By default
	 * the application will search in application/resources/templates-plugins
	 * but you can also add additional directories
	 *
	 * @param M_Directory $directory
	 * @return void
	 * @author Ben Brughmans
	 */
	public static function addPluginsDirectory(M_Directory $directory) {
		self::$_pluginsDirectories[] = $directory->getPath();
	}
	
	/**
	 * Get file
	 * 
	 * Will provide with an instance of {@link M_File}, which represents the
	 * template file
	 * 
	 * @access public
	 * @return M_File
	 */
	public function getFile() {
		return new M_File($this->_filename);
	}
	
	/**
	 * Assign variable
	 * 
	 * This method can be used to assign a presentation variable to the template.
	 * 
	 * @access public
	 * @uses Smarty::assign()
	 * @param string $name
	 * 		The name of the variable
	 * @param mixed $value
	 * 		The value of the presentation variable. Will be made available in the
	 * 		template under the syntaxis {$name}
	 * @return void
	 */
	public function assign($name, $value) {
		$this->_template->assign($name, $value);
	}
	
	/**
	 * Render, and fetch
	 * 
	 * Will render the template, so we get HTML Output. The HTML that has been
	 * rendered by the template will be returned as the output of this method.
	 * 
	 * @access public
	 * @return string
	 */
	public function fetch() {
		// If we cannot locate the template file, we throw an exception!
		if(! is_file($this->_filename)) {
			throw new M_ViewException(sprintf(
				'Cannot fetch the template %s; Cannot locate file %s',
				pathinfo($this->_filename, PATHINFO_BASENAME),
				$this->_filename
			));
		}
		
		// Caching is disabled in debug mode
		if(M_Debug::getDebugMode()) {
			$this->_template->caching = 0;
		}
		
		// Render the template:
		$html = $this->_template->fetch(
			pathinfo($this->_filename, PATHINFO_BASENAME),
			//use the uristring as id for this template, for each uristring a 
			//cache file will be used
			md5(M_Request::getUri()->toString())
		);
		
		// Check if cache busting is enabled:
		if(M_Debug::getCacheBusting()) {
			// If so, we will append a query variable with a timestamp to all 
			// javascript and css files. Note that we do the same for images.
			// We compose an array with file extensions to which the timestamp
			// is to be added:
			$ext = array('css', 'js'); // , 'gif', 'png', 'jpg'
			
			// Get the time
			if(M_Debug::getCacheBustingDate()) {
				$time = M_Debug::getCacheBustingDate()->toString('yyyyMMddhhmmss');
			}else {
				$time = time();
			}
			
			// For each of the extensions:
			foreach($ext as $currentExt) {
				// Add the timestamp:
				$html = str_replace('.'. $currentExt .'"', '.'. $currentExt .'?'.$time.'"', $html);
			}
		}

		return $html;
	}
}