<?php
/**
 * M_FsException class
 * 
 * Used to throw exceptions by file-system-related classes. For more
 * info on file systems, read:
 * 
 * - {@link MI_Fs}
 * - {@link MI_FsItem}
 * - {@link MI_FsItemDirectory}
 * - {@link MI_FsItemFile}
 * 
 * @package Core
 */
class M_FsException extends M_Exception {
}