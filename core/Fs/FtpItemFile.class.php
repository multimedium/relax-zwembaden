<?php
/**
 * M_FtpItemFile class
 * 
 * M_FtpItemFile is an implementation of {@link MI_FsItemFile} that 
 * represents a file on an FTP Server.
 * 
 * @package Core
 */
class M_FtpItemFile extends M_FtpItem implements MI_FsItemFile {
	/**
	 * Get type of filesystem item
	 * 
	 * Will indicate the type of the item on the filesystem. In the case
	 * of {@link M_FtpItemFile}, the return value of this method equals
	 * {@link MI_Fs::TYPE_FILE}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getFsItemType() {
		return MI_Fs::TYPE_FILE;
	}
	
	/**
	 * Get items
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getItems() {
		return new M_IteratorNull;
	}
	
	/**
	 * Get the directory of this item
	 * 
	 * @return M_FtpItemDirectory
	 */
	public function getDirectory() {
		return $this->getParent();
	}
	
	/**
	 * Download this file
	 * 
	 * @access public
	 * @return void
	 * @author b.brughmans
	 */
	public function download() {
		$this->getFs()->download($this);
	}
	
	/**
	 * Set the filname
	 * 
	 * @param string
	 */
	public function setFilename($arg) {
		$this->_filename = $arg;		
	}
	
	/**
	 * Set the file-extension
	 * 
	 * @param string
	 */
	public function setFileExtension($arg) {
		$this->_extension = $arg;
	}
	
	protected $_name;
	protected $_filename;
	protected $_extension;
	
	/**
	 * Get the filename (basename + extension)
	 * 
	 * @return string
	 */
	public function getFilename() {
		return $this->_filename;
	}
	
/**
	 * Set path
	 * 
	 * @see MI_FsItem::setPath()
	 * @access public
	 * @param string $path
	 * 		The new path of the item
	 * @return void
	 */
	public function setPath($path) {
		// Store the path
		$this->_path = $path;
		
		// Parse some information about the file:
		$info = pathinfo($path);
		// - Set the extension
		$this->_extension = isset($info['extension']) ? $info['extension'] : '';
		// - Set the name of the file
		$this->_name = $info['basename'];
		
		// pathinfo provides "filename" since version 5,2,0. If the
		// filename has not been provided, we do it ourselves:
		if(isset($info['filename'])) {
			$this->_filename = $info['filename'];
		} else {
			$this->_filename = substr($this->_name, 0, strrpos($this->_name, '.'));
		}
	}
	
	/**
	 * Get basenamee (filename + extension)
	 * 
	 * @return string
	 */
	public function getBasename() {
		return $this->_filename . '.' . $this->_extension;		
	}
	
	/**
	 * Get the file-extension
	 * 
	 * @return string
	 */
	public function getFileExtension() {
		return $this->_extension;		
	}
	
	/**
	 * @todo implement
	 */
	public function getDirectoryAndFilename() {}
	public function getContents() {}
	public function getContentsInLines() {}
	public function setContents($contents) {}
	public function setContentsInLines(array $contents) {}
}