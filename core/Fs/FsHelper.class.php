<?php
/**
 * M_FsHelper class
 * 
 * M_FsHelper provides with a collection of functions that are related
 * to file systems and items on the file system.
 * 
 * @package Core
 */
class M_FsHelper {
	/**
	 * Get readable file size
	 *
	 * This method will render a readable string out of a given file 
	 * size, expressed in a number of bytes.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_FsHelper::getFileSizeString(1048576); // Will output "1MB"
	 * </code>
	 * 
	 * @access public
	 * @param integer $size
	 * @param string $locale
	 * @return string
	 */
	public static function getFileSizeString($size, $locale = NULL) {
		// Prepare an array of suffixes to indicate size:
		$i = 0;
		$iec = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		
		// While the size is a factor of 1024:
		while(($size / 1024) >= 1) {
			// Then, divide by 1024
			$size /= 1024;
			$i ++;
		}
		
		// Get the final number for display:
		$n = new M_Number(round($size, 2));
		
		// Return the final string, for display
		return $n->toString($locale) . $iec[$i];
	}
	
	/**
	 * Get number of bytes
	 * 
	 * Will do the reverse of {@link M_FsHelper::getFileSizeString()}.
	 * This method can be used to convert a given file size string to 
	 * a number of bytes.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_FsHelper::getNumberOfBytesInString('1MB'); // Will output 1048576
	 * </code>
	 *
	 * @access public
	 * @param string $size
	 * @return integer
	 */
	public static function getNumberOfBytesInString($size) {
		$match = array();
		if(preg_match('/([0-9\.]+)[\s]{0,}(K|M|G|T|P|E|Z|Y){0,1}B?/im', $size, $match)) {
			$bytes  = (float) $match[1];
			$letter = strtoupper(M_Helper::getArrayElement(2, $match, ''));
			if(empty($letter)) {
				return $bytes;
			}
			$iec = array('', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y');
			$i = 0;
			while($letter != $iec[++ $i]) {
				$bytes *= 1024;
			}
			return $bytes * 1024;
		} else {
			throw new M_FsException(sprintf('Could not extract number of bytes in "%s"', $size));
		}
	}
}