<?php
/**
 * MI_FsItemArchive interface
 * 
 * The MI_FsItemArchive interface dictates the public API that must be 
 * implemented by the ARCHIVES on a filesystem. Note that the archive is
 * an element on the file system, so the interface extends the shared
 * {@link MI_FsItem} interface.
 * 
 * @package Core
 */
interface MI_FsItemArchive extends MI_FsItem {
	
	/**
	 * Get uncompressed size
	 * 
	 * Will provide with the uncompressed size of the archive.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getSizeUncompressed();
}