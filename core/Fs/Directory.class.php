<?php
/**
 * M_Directory class
 * 
 * M_Directory can be used to work with directories on the server's
 * file system.
 * 
 * @package Core
 */
class M_Directory extends M_FsItem implements MI_FsItemDirectory {
	/**
	 * Path
	 * 
	 * This property stores the full path to the directory. Note that
	 * ending slashes (directory separators) are not included in this
	 * path.
	 *
	 * @access protected
	 * @var string
	 */
	private $_path;
	
	/**
	 * Get item type
	 * 
	 * @see MI_FsItem::getFsItemType()
	 * @access public
	 * @return string
	 */
	public function getFsItemType() {
		return MI_Fs::TYPE_DIRECTORY;
	}
	
	/**
	 * Check if directory exists
	 * 
	 * @see MI_FsItem::exists()
	 * @access public
	 * @return boolean
	 */
	public function exists() {
		return is_dir($this->_path);
	}
	
	/**
	 * Get path
	 * 
	 * @see MI_FsItem::getPath()
	 * @access public
	 * @return string
	 */
	public function getPath() {
		return M_Helper::rtrimCharlist($this->_path, '/');
	}
	
	/**
	 * Get name
	 * 
	 * @access public
	 * @return string
	 */
	public function getName() {
		return pathinfo($this->_path, PATHINFO_BASENAME);
	}
	
	/**
	 * Get last modified date
	 * 
	 * @see MI_FsItem::getLastModifiedDate()
	 * @access public
	 * @return M_Date
	 */
	public function getLastModifiedDate() {
		if($this->exists()) {
			$date = new M_Date();
			
			foreach($this->getItems() as $item) {
				$itemDate = $item->getLastModifiedDate();
				if($itemDate->isBefore($date)) {
					$date = $itemDate;
				}
			}
			return $date;
		} else {
			throw new M_FsException(sprintf(
				'Cannot get last modified date of %s; cannot locate the directory',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Get creation date
	 * 
	 * @see MI_FsItem::getCreationDate()
	 * @access public
	 * @return M_Date
	 */
	public function getCreationDate() {
		if($this->exists()) {
			$date = new M_Date(0);
			foreach($this->getItems() as $item) {
				$itemDate = $item->getCreationDate();
				if($date->isBefore($itemDate)) {
					$date = $itemDate;
				}
			}
			return $date;
		} else {
			throw new M_FsException(sprintf(
				'Cannot get creation date of %s; cannot locate the directory',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Get last access date
	 * 
	 * @see MI_FsItem::getLastAccessDate()
	 * @access public
	 * @return M_Date
	 */
	public function getLastAccessDate() {
		if($this->exists()) {
			$date = new M_Date(0);
			foreach($this->getItems() as $item) {
				$itemDate = $item->getLastAccessDate();
				if($date->isBefore($itemDate)) {
					$date = $itemDate;
				}
			}
			return $date;
		} else {
			throw new M_FsException(sprintf(
				'Cannot get last modified date of %s; cannot locate the directory',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Get items in the directory
	 *
	 * This method will return the collection of items in the directory.
	 * Note that each of the items in the return value will be presented
	 * either by an instance of {@link M_Directory} or {@link M_File}, 
	 * depending on the type of the item.
	 * 
	 * NOTE:
	 * This method will throw an {@link M_FsException} if the
	 * directory cannot be read.
	 * 
	 * @throws M_FsException
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getItems() {
		// If the directory cannot be found, we throw an exception to
		// notify about the failure:
		if(!$this->exists()) {
			throw new M_FsException(sprintf(
				'%s: %s: Cannot find directory %s',
				__CLASS__,
				__FUNCTION__,
				$this->_path
			));
		}
		
		// Create a resource handle, to work with the directory:
		$handle = opendir($this->_path);
		
		// If the handle could not have been created, we throw an
		// exception to nofity about the failure:
		if(!$handle) {
			throw new M_FsException(sprintf(
				'%s: %s: Could not create directory handle',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// We read the contents of the directory, and we populate the
		// return value with instances of M_Directory and M_File
		$out = array();
		$file = readdir($handle);
		while ($file !== FALSE) {
			if($file != '.' && $file != '..') {
				$temp = $this->_path . '/' . $file;
				if(is_file($temp)) {
					$out[] = new M_File($temp);
				} else {
					$out[] = new M_Directory($temp);
				}
			}
			$file = readdir($handle);
		}
			
		// Close the handle:
		closedir($handle);
		
		// Return an iterator with the items:
		return new M_ArrayIterator($out);
	}
	
	/**
	 * Get size
	 *
	 * This method will return the size of the folder. To do so, it
	 * sums up the filesizes of all the items that are contained by the
	 * folder (recursively). It uses the combination of the following
	 * methods to achieve this sum:
	 * 
	 * - {@link M_Directory::getItems()}
	 * - {@link M_Directory::getSize()}
	 * - {@link M_File::getSize()}
	 * 
	 * @uses M_Directory::getItems()
	 * @uses M_Directory::getSize()
	 * @uses M_File::getSize()
	 * @access public
	 * @return integer
	 */
	public function getSize() {
		// The output size number is:
		$size = 0;
		
		// For each of the items in the directory:
		foreach($this->getItems() as $item) {
			// We ask the item for its size (this item may be either
			// a directory or a file; both implement the same method):
			$size += $item->getSize();
		}
		
		// Return the sum of all sizes:
		return $size;
	}
	
	/**
	 * Set path
	 * 
	 * This method can be used to set the full path to the directory.
	 * This method is used by the constructor to set the initial path
	 * to the directory.
	 *
	 * @access public
	 * @param string $path
	 * 		The path to the directory
	 * @return void
	 */
	public function setPath($path) {
		$this->_path = rtrim($path, " \t\n\r\0\x0B/\\");
	}
	
	/**
	 * Set name
	 * 
	 * @see MI_FsItem::setName()
	 * @access public
	 * @param string $newName
	 * 		The new name of the item
	 * @return bool
	 */
	public function setName($name) {
		// Get the absolute old path:
		$oldPath = M_Loader::getAbsolute($this->_path);
		// Impode the elements to the new path:
		$path = $this->getParent()->getPath() . DIRECTORY_SEPARATOR . $name;
		$newPath = M_Loader::getAbsolute($path);
		// Rename it
		if(@rename($oldPath, $newPath)) {
			$this->_path = $path;
			return TRUE;
		} else {
			throw new M_FsException(sprintf(
				'Could not rename directory %s; permission denied',
				$this->getPath()
			));
		}
	}
	
	/**
	 * Make the directory
	 * 
	 * An instance of {@link M_Directory} may be constructed, even 
	 * though the directory does not "physically" exist. This method
	 * can be used to create the directory structure that is represented
	 * by the {@link M_Directory} instance.
	 * 
	 * Example 1
	 * <code>
	 *    // The 'images' folders does not yet exist in the folder 'tom'
	 *    $dir = new M_Directory('/home/users/tom/images');
	 *    $dir->make(); // now, it does!
	 * </code>
	 * 
	 * NOTE:
	 * This method will also create all of the directories in between,
	 * if not already existing. In other words, it creates the 
	 * directories recursively.
	 * 
	 * NOTE:
	 * This method will throw an {@link M_FsException} if the
	 * directory cannot be created due to permissions.
	 * 
	 * NOTE:
	 * This method will return TRUE on success.
	 *
	 * @access public
	 * @return boolean
	 */
	public function make() {
		// Check if the directory already exists. If the directory
		// already exists, this function will not do anything.
		if(!is_dir($this->_path)) {
			// Note that we tell to create the directory recursively,
			// which will cause all of the intermediate directories to
			// be created as well, if necessary:
			if(@mkdir($this->_path, 0777, TRUE)) {
				return TRUE;
			}
			// If the directory could not have been created, we inform
			// about the error with an exception:
			else {
				throw new M_FsException(sprintf(
					'Could not make directory %s; permission denied',
					$this->getPath()
				));
			}
		}
	}
	
	/**
	 * Check if this is the root-directory
	 * 
	 * @access public
	 * @return boolean
	 * @author b.brughmans
	 * @todo TEST!
	 */
	public function isRoot() {
		return (M_Helper::rtrimCharlist($this->getPath(), '/') == M_Server::getDocumentRoot());
	}
	
	/**
	 * Delete the directory
	 *
	 * This method will delete the directory and all of the items that
	 * are contained in it.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function delete() {
		// rmdir() attempts to remove the directory named by dirname. 
		// The directory must be empty, and the relevant permissions 
		// must permit this. Since the directory must be empty, we 
		// first delete the contents:
		foreach($this->getItems() as $item) {
			// We ask the current item to delete itself (this item may 
			// be either a directory or a file; both implement the same 
			// method)
			$item->delete();
		}
		
		// Remove the directory:
		return rmdir($this->_path);
	}

	/**
	 * Truncate a directory
	 *
	 * @access public
	 * @author Ben Brughmans
	 * @return boolean
	 */
	public function truncate() {
		try {
			/* @var $item M_FsItem */
			foreach($this->getItems() AS $item) {
				//if an item cannot be deleted: stop
				if (!$item->delete()) return false;
			}
			return true;
		}
		//if something went wrong: log and stop
		catch( M_FsException $e) {
			$e->log();
			return false;
		}
	}
	
	/**
	 * Copy the directory
	 * 
	 * @see MI_FsItem::copy()
	 * @access public
	 * @param MI_FsItem $to
	 * 		The location to which the item should be copied
	 * @param string $name
	 *		The (new) name of the copied directory
	 * @return bool
	 */
	public function copy(MI_FsItemDirectory $to, $name = null) {
		throw new M_FsException('Directory copy not yet implemented');
	}
	
	/**
	 * Move the file
	 * 
	 * @see MI_FsItem::move()
	 * @access public
	 * @param MI_FsItem $to
	 * 		The location to which the item should be moved
	 * @return bool
	 */
	public function move(MI_FsItemDirectory $to) {
		throw new M_FsException('Directory move not yet implemented');
	}

	/**
	 * Place an item
	 *
	 * May be used by {@link MI_FsItemFile} in order to copy the file to a target
	 * directory. By using this method, we intend to make the {@link MI_FsItemFile::copy()}
	 * method filesystem-independent.
	 *
	 * @author Tom Bauwens
	 * @access public
	 * @param MI_FsItem $item
	 *		The item to be copied
	 * @param string $name
	 *		The (new) name for the copied item
	 * @return bool $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	public function place(MI_FsItem $item, $name = null) {
		// Check if the new name has been provided:
		if(! $name) {
			// Get default name (same as source file/directory), when not set
			$name = $item->getName();
		}

		// Get the filesystem at which the item is located:
		$fs = $item->getFs();
		
		// If the provided item is an item that is located on a remote FTP server:
		if($fs->getType() == 'FTP' || $fs->getType() == 'SFTP') {
			// Then, we download a copy to this directory:
			/* @var $fs M_Ftp */
			if($fs->copyItem($item, $this)) {
				
				// Check if we need to set a new name
				if($item->isFile()) {
					$newItem = new M_File($this->getPath() . '/' . $item->getName());
				} else {
					$newItem = new M_Directory($this->getPath() . '/' . $item->getName());
				}
				
				$newItem->setName($name);
				
				// Return true, if success
				return true;
			}
			
			// If here:
			return false;
		}
		// If the provided item is a local file:
		else {
			// Try to copy
			if(! @copy($item->getPath(), $this->getPath() . DIRECTORY_SEPARATOR . $name)) {
				// If we failed to copy the item, we throw an exception!
				throw new M_FsException(sprintf(
					'Cannot copy %s to %s; permission denied',
					$item->getPath(),
					$this->getPath()
				));
			}

			// Return TRUE if still here
			return TRUE;
		}
	}
}