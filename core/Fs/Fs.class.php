<?php
/**
 * M_Fs class
 * 
 * M_Fs is an implementation of {@link MI_Fs} that represents the "local"
 * file system, on which the application is running.
 * 
 * @package Core
 */
class M_Fs implements MI_Fs {
	/**
	 * Get type
	 * 
	 * @see MI_Fs::getType()
	 * @access public
	 * @return string
	 */
	public function getType() {
		return 'Local';
	}
	/**
	 * Get OS
	 * 
	 * @see MI_Fs::getOS()
	 * @access public
	 * @return string
	 */
	public function getOS() {
		return PHP_OS; // Predefined by PHP
	}
	
	/**
	 * Get temporary folder
	 * 
	 * Will return an object, that implements the {@link MI_FsItemDirectory} 
	 * interface, where temporary files are located.
	 * 
	 * @access public
	 * @return MI_FsItemDirectory
	 */
	public static function getTemporaryDirectory() {
		$dir = new M_Directory(
			M_Loader::getAbsolute('files' . DIRECTORY_SEPARATOR . 'temp')
		);
		
		if(! $dir->exists()) {
			$dir->make();
		}
		return $dir;
	}
	
	/**
	 * Get unique temporary folder
	 * 
	 * Will return an object, that implements the {@link MI_FsItemDirectory}
	 * interface, where temporary files are located. In addition to what the
	 * {@link M_Fs::getTemporaryDirectory()} method already does, this method
	 * creates a UNIQUE folder inside the temporary folder.
	 * 
	 * @access public
	 * @return M_Directory
	 */
	public static function getUniqueTemporaryDirectory() {
		$temp = self::getTemporaryDirectory();
		$uniq = new M_Directory(
			$temp->getPath() . DIRECTORY_SEPARATOR . M_Helper::getUniqueToken()
		);
		
		if(! $uniq->exists()) {
			$uniq->make();
		}
		return $uniq;
	}
	
	/**
	 * Get an item
	 * 
	 * @see MI_Fs::getItem()
	 * @access public
	 * @return M_FsItem
	 */
	public function getItem($path) {
		if(is_file($path)) {
			return new M_File($path);
		} elseif(is_dir($path)) {
			return new M_Directory($path);
		} else {
			throw new M_FsException(sprintf(
				'Cannot find item %s on local filesystem',
				$path
			));
		}
	}
}