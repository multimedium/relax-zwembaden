<?php
/**
 * M_FtpItemDirectory class
 * 
 * M_FtpItemDirectory is an implementation of {@link MI_FsItemDirectory}
 * that represents a directory on an FTP Server.
 * 
 * @package Core
 */
class M_FtpItemDirectory extends M_FtpItem implements MI_FsItemDirectory {
	/**
	 * Get type of filesystem item
	 * 
	 * Will indicate the type of the item on the filesystem. In the case
	 * of {@link M_FtpItemFile}, the return value of this method equals
	 * {@link MI_Fs::TYPE_FILE}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getFsItemType() {
		return MI_Fs::TYPE_DIRECTORY;
	}
	
	/**
	 * Get items
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getItems() {
		return $this->getFs()->getItemsInDirectory($this);
	}
	
	/**
	 * Make the directory
	 * 
	 * @access public
	 * @return bool
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function make() {
		return $this->getFs()->makeDirectory($this);
	}
	
	/**
	 * Check if this is the root directory
	 * 
	 * @return bool
	 * @author b.brughmans
	 */
	public function isRoot() {
		return (M_Helper::ltrimCharlist($this->getPath(), array('/',".")) == "");
	}
	
	/**
	 * Check if exists
	 * 
	 * @access public
	 * @return bool
	 * @author b.brughmans
	 */
	public function exists() {
		//root directory always exists
		if ($this->isRoot()) return true;
		
		return $this->getFs()->isItemOnServer($this);
	}
	
	/**
	 * Set owner
	 * 
	 * Will set with the item's owner. The owner is represented by an instance 
	 * of {@link M_FsOwner}.
	 * 
	 * Will return TRUE on success, FALSE on failure.
	 * 
	 * @access public
	 * @param M_FsOwner $owner
	 * @return bool
	 */
	public function setOwner(M_FsOwner $owner) {
		return $this->getFs()->setOwnerOfItem($owner, $this);
	}

	/**
	 * Truncate directory
	 *
	 * Delete all files from directory
	 *
	 * @access public
	 * @return bool
	 * @author Ben Brughmans
	 */
	public function truncate() {
		return $this->getFs()->truncate($this);
	}

	/**
	 * Place an item
	 *
	 * May be used by {@link MI_FsItemFile} in order to copy the file to a target
	 * directory. By using this method, we intend to make the {@link MI_FsItemFile::copy()}
	 * method filesystem-independent.
	 *
	 * @author Tom Bauwens
	 * @access public
	 * @param MI_FsItem $item
	 *		The item to be placed
	 * @param string $name
	 *		The (new) name for the copied item
	 * @return bool $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	public function place(MI_FsItem $item, $name = null) {
		return $this->getFs()->upload($this, $item);
	}
}