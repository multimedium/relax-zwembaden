<?php
/**
 * MI_FsItemFile interface
 * 
 * The MI_FsItemFile interface dictates the public API that must be 
 * implemented by the FILES on a filesystem. Note that the file is
 * an element on the file system, so the interface extends the shared
 * {@link MI_FsItem} interface.
 * 
 * @package Core
 */
interface MI_FsItemFile extends MI_FsItem {
	/**
	 * Get contents
	 * 
	 * Will provide with the contents of the file.
	 * 
	 * @access public
	 * @return string
	 */
	public function getContents();
	
	/**
	 * Get contents, in lines
	 * 
	 * Will provide with the contents of the file. Note that the contents
	 * are provided line by line (in the form of an array). The result
	 * of this method is similar to PHP's built-in file() function
	 * 
	 * @access public
	 * @return array
	 */
	public function getContentsInLines();
	
	/**
	 * Get filename
	 * 
	 * Will provide with the filename (no extension included).
	 * 
	 * @access public
	 * @return string
	 */
	public function getFilename();
	
	/**
	 * Get basename
	 * 
	 * Will provide with the basename (filename + extension). The return 
	 * value of this method is exactly the same as the one that is 
	 * provided by {@link MI_FsItem::getName()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getBasename();
	
	/**
	 * Get extension
	 * 
	 * Will provide with the extension of the file.
	 * 
	 * @access public
	 * @return string
	 */
	public function getFileExtension();
	
	/**
	 * Get directory
	 * 
	 * Will provide with the path of the directory that contains the
	 * file. If you want the directory to be represented by an object,
	 * you should use {@link MI_FsItem::getParent()} instead.
	 * 
	 * @access public
	 * @return string
	 */
	public function getDirectory();
	
	/**
	 * Get directory + filename
	 * 
	 * Will provide with the path of the directory and the filename.
	 * The return value of this method is the combined result of 
	 * 
	 * - {@link MI_FsItemFile::getDirectory()}, and
	 * - {@link MI_FsItemFile::getFilename()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getDirectoryAndFilename();
	
	/**
	 * Set filename
	 * 
	 * Will set the filename of the file (not affecting the extension).
	 * Returns TRUE on success, FALSE on failure.
	 * 
	 * @access public
	 * @param string $name
	 * @return bool
	 */
	public function setFilename($name);
	
	/**
	 * Set extension
	 * 
	 * Will set the extension of the file. Returns TRUE on success, 
	 * FALSE on failure.
	 * 
	 * @access public
	 * @param string $extension
	 * @return bool
	 */
	public function setFileExtension($extension);
	
	/**
	 * Set contents
	 * 
	 * Will write the provided contents to the file. Returns TRUE on
	 * success, FALSE on failure.
	 *
	 * @access public
	 * @param string $contents
	 * 		The contents that are to be written to the file
	 * @return bool
	 */
	public function setContents($contents);
	
	/**
	 * Set contents, in lines
	 * 
	 * Will write the contents (provided in lines, in the form of an
	 * array) to the file. Returns TRUE on success, FALSE on failure.
	 * 
	 * @see MI_FsItemFile::setContents()
	 * @access public
	 * @param array $contents
	 * 		The contents, in lines
	 * @return bool
	 */
	public function setContentsInLines(array $contents);
	
	/**
	 * Download this file
	 * 
	 * @access public
	 * @return void
	 * @author b.brughmans
	 */
	public function download();
}