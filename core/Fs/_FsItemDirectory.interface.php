<?php
/**
 * MI_FsItemDirectory interface
 * 
 * The MI_FsItemDirectory interface dictates the public API that must 
 * be implemented by the DIRECTORIES/FOLDERS on a filesystem. Note that 
 * the folder is an element on the file system, so the interface extends 
 * the shared {@link MI_FsItem} interface.
 * 
 * @package Core
 */
interface MI_FsItemDirectory extends MI_FsItem {
	/**
	 * Make the directory
	 * 
	 * If the directory does not yet exist, this method will create it
	 * (recursively). Will return TRUE on success, FALSE on failure.
	 * 
	 * @see MI_FsItem::exists()
	 * @access public
	 * @return bool
	 */
	public function make();
	
	/**
	 * Check if this is the root-directory
	 * 
	 * If this directory is the root-directory, this method will return TRUE.
	 * 
	 * @return bool
	 */
	public function isRoot();

	/**
	 * Empty the directory
	 *
	 * Delete all files from this directory
	 *
	 * @author Ben Brughmans
	 * @return bool
	 */
	public function truncate();

	/**
	 * Place an item
	 *
	 * May be used by {@link MI_FsItemFile} in order to copy the file to a target
	 * directory. By using this method, we intend to make the {@link MI_FsItemFile::copy()}
	 * method filesystem-independent.
	 *
	 * @author Tom Bauwens
	 * @access public
	 * @param MI_FsItem $item
	 *		The item to be placed
	 * @param string $name
	 *		The (new) name for the copied item
	 * @return bool $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	public function place(MI_FsItem $item, $name = null);
}