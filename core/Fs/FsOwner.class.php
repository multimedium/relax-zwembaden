<?php
/**
 * M_FsOwner class
 * 
 * This class can be used to describe the owner of an item on a file
 * system. For more information, read {@link MI_FsItem::getOwner()}.
 * 
 * @package Core
 */
class M_FsOwner {
	/**
	 * Name
	 * 
	 * This property stores the username of the owner.
	 *
	 * @access protected
	 * @var string
	 */
	protected $_name;
	
	/**
	 * Group name
	 * 
	 * This property stores the name of the group to which the user 
	 * belongs. In many cases, for example on a UNIX installation, the
	 * group will be given the same name as the user.
	 *
	 * @access protected
	 * @var string
	 */
	protected $_group;
	
	/**
	 * Constructor
	 * 
	 * @uses M_FsOwner::setName()
	 * @uses M_FsOwner::setGroupName()
	 * @access public
	 * @param string $name
	 * 		The name of the user
	 * @param string $groupName
	 * 		The name of the group
	 * @return M_FsOwner
	 */
	public function __construct($name, $groupName = NULL) {
		$this->setName($name);
		if($groupName) {
			$this->setGroupName($groupName);
		}
	}
	
	/**
	 * Set name
	 * 
	 * Can be used to set the username of the owner.
	 *
	 * @access public
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->_name = (string) $name;
	}
	
	/**
	 * Set group name
	 * 
	 * Can be used to set the name of the group to which the user
	 * belongs.
	 *
	 * @see M_FsOwner::$_group
	 * @access public
	 * @param string $name
	 * @return void
	 */
	public function setGroupName($name) {
		$this->_group = (string) $name;
	}
	
	/**
	 * Get name
	 *
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Get name of group
	 * 
	 * @see M_FsOwner::$_group
	 * @access public
	 * @return string
	 */
	public function getGroupName() {
		return $this->_group;
	}
}