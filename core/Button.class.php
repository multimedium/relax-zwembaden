<?php
/**
 * M_Button
 *
 * @package Core
 */
class M_Button extends M_Object {

	/* -- PROPERTIES -- */

	/**
	 * Title
	 *
	 * @see M_Button::setTitle()
	 * @see M_Button::getTitle()
	 * @access private
	 * @var string
	 */
	private $_title;

	/**
	 * Description
	 *
	 * @see M_Button::setDescription()
	 * @see M_Button::getDescription()
	 * @access private
	 * @var string
	 */
	private $_description;

	/**
	 * ID
	 *
	 * @see M_Button::setId()
	 * @see M_Button::getId()
	 * @access private
	 * @var string
	 */
	private $_id;

	/**
	 * Link URL
	 *
	 * @see M_Button::setHref()
	 * @see M_Button::getHref()
	 * @access private
	 * @var string
	 */
	private $_href;

	/**
	 * CSS Properties
	 *
	 * @see M_Button::setCss()
	 * @see M_Button::removeCss()
	 * @see M_Button::getCss()
	 * @see M_Button::getCssCount()
	 * @see M_Button::hasCss()
	 * @access private
	 * @var string
	 */
	private $_css = array();

	/**
	 * CSS Classes
	 *
	 * @see M_Button::addCssClass()
	 * @see M_Button::removeCssClass()
	 * @see M_Button::hasCssClass()
	 * @see M_Button::getCssClass()
	 * @see M_Button::getCssClassCount()
	 * @access private
	 * @var string
	 */
	private $_cssClasses = array();

	/* -- SETTERS -- */

	/**
	 * Set title
	 *
	 * @access public
	 * @param string $text
	 * @return M_Button $button
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTitle($text) {
		$this->_title = (string) $text;
		return $this;
	}

	/**
	 * Set description
	 *
	 * @access public
	 * @param string $text
	 * @return M_Button $button
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDescription($text) {
		$this->_description = (string) $text;
		return $this;
	}

	/**
	 * Set ID
	 *
	 * @access public
	 * @param string $id
	 * @return M_Button $button
	 *		Returns itself, for a fluent programming interface
	 */
	public function setId($id) {
		$this->_id = (string) $id;
		return $this;
	}

	/**
	 * Set Link URL
	 *
	 * @access public
	 * @param M_Uri $uri
	 * @return M_Button $button
	 *		Returns itself, for a fluent programming interface
	 */
	public function setHref(M_Uri $uri) {
		$this->_href = $uri;
		return $this;
	}

	/**
	 * Add CSS Property (inline)
	 *
	 * @access public
	 * @param string $property
	 *		The name of the property, e.g. 'background-color' or 'backgroundColor'
	 * @param string $value
	 *		The value of the property, e.g. '#fefefe'
	 * @return M_Button $button
	 *		Returns itself, for a fluent programming interface
	 */
	public function addCss($property, $value) {
		$this->_css[$this->_getCssProperty($property)] = M_Helper::trimCharlist($value, ';');
	}

	/**
	 * Remove CSS
	 *
	 * This method can be used to remove a given css property from the object.
	 *
	 * @access public
	 * @param string $property
	 * @return M_Button $button
	 *		Returns itself, for a fluent programming interface
	 */
	public function removeCss($property) {
		unset($this->_css[$this->_getCssProperty($property)]);
		return $this;
	}

	/**
	 * Add CSS class
	 *
	 * @access public
	 * @param string $class
	 * @return M_Button $button
	 *		Returns itself, for a fluent programming interface
	 */
	public function addCssClass($class) {
		$this->_cssClasses[(string) $class] = TRUE;
	}

	/**
	 * Remove CSS class
	 *
	 * @access public
	 * @param string $class
	 * @return M_Button $button
	 *		Returns itself, for a fluent programming interface
	 */
	public function removeCssClass($class) {
		unset($this->_cssClasses[(string) $class]);
	}

	/* -- GETTERS -- */

	/**
	 * Get title
	 *
	 * @access public
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * Get description
	 *
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * Get ID
	 *
	 * @access public
	 * @return string
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * Get Link URL
	 *
	 * @access public
	 * @return M_Uri
	 */
	public function getHref() {
		return $this->_href;
	}

	/**
	 * Get CSS
	 *
	 * @access public
	 * @return array $css
	 *		Returns an associative array, containing the CSS properties of the
	 *		button
	 */
	public function getCss() {
		return $this->_css;
	}

	/**
	 * Get number of applied CSS properties
	 *
	 * @access public
	 * @return integer $count
	 */
	public function getCssCount() {
		return count($this->_css);
	}

	/**
	 * Has CSS?
	 *
	 * Will check whether or not the object already has a given css property
	 * applied to it.
	 *
	 * @access public
	 * @param string $property
	 * @return bool $flag
	 *		Will return TRUE if the CSS property has been assigned, FALSE if not
	 */
	public function hasCss($property) {
		return array_key_exists($this->_getCssProperty($property), $this->_css);
	}

	/**
	 * Get CSS Class(es)
	 *
	 * @access public
	 * @return array $classes
	 *		Returns the collection of classes that has been applied to the object
	 */
	public function getCssClass() {
		return array_keys($this->_cssClasses);
	}

	/**
	 * Get number of applied CSS classes
	 *
	 * @access public
	 * @return integer $count
	 */
	public function getCssClassCount() {
		return count($this->_cssClasses);
	}

	/**
	 * Has CSS Class?
	 *
	 * @access public
	 * @param string $class
	 * @return bool $flag
	 *		Will return TRUE if the CSS Class has been assigned, FALSE if not
	 */
	public function hasCssClass($class) {
		return array_key_exists((string) $class, $this->_cssClasses);
	}

	/**
	 * Get HTML
	 *
	 * ... Alias for {@link M_Button::toString()}
	 *
	 * @uses M_Button::toString()
	 * @access public
	 * @return string $html
	 */
	public function getHtml() {
		return $this->toString();
	}

	/**
	 * Get HTML
	 *
	 * Will convert the button to a string (valid HTML code)
	 *
	 * @access public
	 * @return string $html
	 */
	public function toString() {
		// The output
		$out = '';

		// Start the button tag
		$out .= '<a';

		// Has an ID been assigned to the tag?
		$id = $this->getId();
		if($id) {
			// If so, add the ID:
			$out .= ' id="'. $id .'"';
		}

		// Get the Link URL:
		$url = $this->getHref();

		// If the URL is available, we get the complete URI. If not, we simply
		// do not add the href attribute:
		if($url) {
			$out .= ' href="'. $url->toString() .'"';
		}

		// Apply CSS?
		if($this->getCssCount() > 0) {
			// If so, start the style attribute now:
			$out .= ' style="';

			// For each of the CSS Properties:
			$i = 0; foreach($this->getCss() as $property => $value) {
				// If this is not the first property in the collection
				if($i ++ > 0) {
					// Add space between this property and the previous one:
					$out .= ' ';
				}

				// Add the property to the style attribute
				$out .= $property . ': ' . $value . ';';
			}

			// Finish the style attribute here:
			$out .= '"';
		}

		// CSS Classes?
		if($this->getCssClassCount() > 0) {
			// If so, start the class attribute now:
			$out .= ' class="';

			// For each of the CSS classes:
			$i = 0; foreach($this->getCssClass() as $value) {
				// If this is not the first class in the collection
				if($i ++ > 0) {
					// Add space between this class and the previous one:
					$out .= ' ';
				}

				// Add the class
				$out .= $value;
			}

			// Finish the class attribute here:
			$out .= '"';
		}

		// Get the description of the link:
		$description = new M_Text($this->getDescription());

		// If a description is available
		if($description->getText()) {
			// Then, add it to the "title" attribute of the tag
			$out .= ' title="'. $description->toHtmlAttribute() .'"';
		}

		// Close the opening tag:
		$out .= '>';

		// Add the label/title
		$out .= $this->getTitle();

		// Add the closing tag:
		$out .= '</a>';

		// Return the HTML
		return $out;
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get CSS Property
	 *
	 * This method is used internally, to compose a CSS Property Name. For example,
	 * this method will convert 'backgroundColor' to the correct 'background-color'
	 * name.
	 *
	 * @access protected
	 * @param string $name
	 *		The name of the property, as provided to public methods of this class
	 * @return string $name
	 *		The converted, correct, css property name
	 */
	protected function _getCssProperty($name) {
		return strtolower(implode('-', M_Helper::explodeByCamelCasing((string) $name)));
	}
}