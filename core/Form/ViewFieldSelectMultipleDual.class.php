<?php
class M_ViewFieldSelectMultipleDual extends M_ViewField {
	protected function getHtml() {
		return 'M_ViewFieldSelectMultipleDual; getHtml() function in core. Please
			generate a core-form template file';
	}

	/**
	 * Get the resource
	 *
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldSelectMultipleDual.tpl');
	}
}