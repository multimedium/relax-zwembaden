<?php
class M_ViewFieldVat extends M_ViewFieldText {
	protected function getHtml() {
		$field = $this->getField();
		$html = '<input type="text" name="'. $field->getId() .'"' 
				. ' id="'. $this->getId() .'"' 
				. ' value="'. $field->getValue() .'"' 
				. ' class="field-text"';
		
		if($field->getWidth()) {
			$html .= ' style="width: '. $field->getWidth() .'px;"';
		}
		if($field->getMaxLength()) {
			$html .= ' maxlength="'. $field->getMaxLength() .'"';
		}
		if($field->getReadonly()) {
			$html .= ' readonly="readonly"';
		}
		$html .= ' />';
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldTVat.tpl');
	}
}