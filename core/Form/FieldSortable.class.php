<?php
/**
 * M_FieldSortable class
 * 
 * M_FieldSortable, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldOptions}
 * 
 * handles the input control that allow users to manually sort 
 * the items in an ordered list, with a drag-and-drop interface.
 * 
 * @package Core
 */
class M_FieldSortable extends M_FieldOptions {
	
	/**
	 * @const
	 * 
	 * The string with which the values are seperated
	 */
	const VALUE_SEPARATOR = ',';
	
	/**
	 * @var M_FieldSortable
	 */
	protected $_connectWith;
	
	/**
	 * @var bool Disables the ability to select text in sortable-items
	 */
	protected $_disableSelection;
	
	/**
	 * Flag whether or not you can delete items from a list
	 *
	 * @var bool
	 */
	protected $_itemDeletable = true;
	
	/**
	 * Connect this sortable field with another {@link M_FieldSortable}
	 * 
	 * You can connect this sortable field with another sortable field. This way
	 * you can move items from one sortable field to the connected sortable
	 * field and the other way around
	 * 
	 * @see M_FieldDraggable
	 * @see M_FieldSortable::connectWithSortable()
	 * @param M_FieldSortable
	 * @return void
	 */
	public function connectWithSortable(M_FieldSortable $field) {
		$this->_connectWith = $field;		
	}
	
	/**
	 * Get the field to which this sortable-field is connected
	 * 
	 * @return M_FieldSortable|M_FieldDraggable|null
	 */
	public function getConnectedWith() {
		return $this->_connectWith;
	}
	
	/**
	 * Disable text-selection of sortable items
	 * 
	 * @param $arg bool
	 * @return void
	 */
	public function setDisableSelection($arg) {
		$this->_disableSelection = (bool)$arg;
	}
	
	/**
	 * Is the ability to select text in sortable items disabled?
	 * 
	 * @return bool
	 */
	public function isDisableSelection() {
		return $this->_disableSelection;
	}
	
	/**
	 * Set if the items can be deleted from the list
	 *
	 * @param bool $arg
	 */
	public function setItemDeletable($arg) {
		$this->_itemDeletable = (bool)$arg;
	}
	
	/**
	 * Check if items can be deleted from the list
	 *
	 * @return bool
	 */
	public function isItemDeletable() {
		return $this->_itemDeletable;
	}
	
	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * M_FieldSortable will provide with an array that holds the 
	 * items' values, in the same order they have been provided by
	 * the user.
	 * <code>
	 *    Array (
	 *       [item-value] => [item-label],
	 *       [item-value] => [item-label],
	 *       [item-value] => [item-label],
	 *       [item-value] => [item-label]
	 *       ...
	 *    )
	 * </code>
	 * 
	 * Note that this is also the delivered value by this field (see
	 * {@link M_Field::deliver()}), because we do not override that
	 * method.
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return array
	 */
	public function getValue() {
		$temp = M_Request::getVariable($this->getId(), FALSE);
		$c = ($temp !== FALSE);
		if($c) {
			$values = explode(';', $temp);
			$items  = $this->getItems();
			$value  = array();
			
			$n = count($values);
			if($n == count($items)) {
				for($i = 0; $i < $n && $c; $i ++) {
					$c = (array_key_exists($values[$i], $items));
					if($c) {
						if(!in_array($values[$i], $value)) {
							$value[] = $values[$i];
						}
					}
				}
			}
			
			$c = count($value) == count($items);
		}
		
		if($c) {
			return $value;
		} else {
			return array_keys($this->getItems());
		}
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldSortable} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldSortable
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldSortable($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}