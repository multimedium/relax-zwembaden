<?php
/**
 * M_ViewFieldAddress class
 *
 * Used by the class {@link M_FieldAddress}, in order to render the view of the
 * input control.
 *
 * @package Core
 * @author Ben Brughmans
 */
class M_ViewFieldAddress extends M_ViewField {

	/**
	 * Default HTML source code rendering
	 *
	 * This method overrides {@link M_ViewCoreHtml::getHtml()}. All
	 * subclasses of {@link M_ViewCoreHtml} must implement this method,
	 * to provide with default HTML rendering.
	 *
	 * @access protected
	 * @return string
	 */
	protected function getHtml() {
		// Get the field that is being rendered for display:
		/* @var $field M_FieldAddress */
		$field = $this->getField();
		$out = '';

		// Error message
		// (if the field does not have a general error message)
		if(! $field->getErrorMessage()) {
			// Then, we show the first error message we can find in one of the
			// contained fields:
			$out .= $this->_getErrorMessage();
		}

		// Container
		$out .= '<div id="'.$this->getId().'" class="address-container">';

		// Street field
		$out .=		$this->_getFieldContainerView($field->getFieldStreet(), 'field-address-street');

		// Streetnumber field
		if($field->getShowStreetNumber()) {
			$out .=		$this->_getFieldContainerView($field->getFieldStreetNumber(), 'field-address-street-number');
		}
		$out .= '<div class="m-clear"><!-- --></div></div>';

		$out .= '<div class="address-container">';

		// Postalcode field
		if($field->getShowPostalCode()) {
			$out .=		$this->_getFieldContainerView($field->getFieldPostalCode(), 'field-address-postal-code');
		}
		// City field
		if($field->getShowCity()) {
			$out .=		$this->_getFieldContainerView($field->getFieldCity(), 'field-address-city');
		}

		$out .= '<div class="m-clear"><!-- --></div></div>';

		if($field->getShowRegion()) {
			$out .= '<div class="address-container">';
			$out .=		$this->_getFieldContainerView( $field->getFieldRegion(), 'field-address-region');
			$out .= '</div>';
		}

		if($field->getShowCountry()) {
			$out .= '<div class="address-container">';
			$out .=		$this->_getFieldContainerView($field->getFieldCountry(), 'field-address-country');
			$out .= '</div>';
		}

		if($field->getShowGoogleMaps()) {
			$out .= '<div class="address-container">';
			$out .=		$this->_getFieldContainerView($field->getFieldGoogleMaps(), 'field-address-maps');
			$out .= '</div>';
		}

		// Return the HTML Source code:
		return $out;
	}

	/**
	 * Create a container including a {@link M_Field}
	 *
	 * Each field lives in its own container: this way we can style a field,
	 * or communicate to it more easily with jQuery, ...
	 *
	 * @param M_Field $field
	 * @param string $class
	 * @return string
	 */
	private function _getFieldContainerView(M_Field $field, $class = '') {
		$out =	'<div class="field-address-container '.$class.'" id="container-'.$field->getId().'">';

		//add the title
		if ($field->getTitle()) {
			$out .= '<label for="'.$field->getId().'">'.$field->getTitle().'</label>';
		}

		/* if($field->getErrorMessage()) {
			$out .= '<div class="error-message">';
			$out .=    $field->getErrorMessage();
			$out .= '</div>';
		} */

		//add the field itself
		$out .= $field->getInputView()->fetch();

		//add the description
		if($field->getDescription()) {
			$out .=    '<div class="field-description">';
			$out .= 		'<div class="small note">';
			$out .=				$field->getDescription();
			$out .=			'</div>';
			$out .=    '</div>';
		}

		//add buttons when this is a Google Maps Field
		if (M_Helper::isInstanceOf($field, 'M_FieldGoogleMaps')) {
			$out .=		'<div class="field-buttons">';
			$out .=			'<a href="#" class="left button-submit" id="'.$field->getId().'-button-locate"><span>'.t('locate').'</span></a>';
			$out .=			'<a href="#" class="left button" id="'.$field->getId().'-button-clear"><span>'.t('clear').'</span></a>';
			$out .=		'</div>';
		}

		$out .= '</div>';

		return $out;
	}

	/**
	 * Get (general) error message
	 *
	 * @access private
	 * @return string
	 */
	private function _getErrorMessage() {
		// Create a collection of getters. Each of the getters will be used to
		// retrieve a field, and check if an error message exists for that field.
		$fields = array(
			'getFieldStreet',
			'getFieldStreetNumber',
			'getFieldPostalCode',
			'getFieldCity',
			'getFieldRegion',
			'getFieldCountry',
			'getFieldGoogleMaps'
		);

		// For each of the field getters:
		foreach($fields as $getter) {
			// Get the field:
			$field = $this->getField()->$getter();

			// Check if an error message exists for the field
			/* @var $field M_Field */
			$error = $field->getErrorMessage();
			if($error) {
				// If so, then we render the error message for display
				$out  = '<div class="error-message">';
				$out .=    $error;
				$out .= '</div>';
				return $out;
			}
		}

		// If still here, there is no error
		return '';
	}

	/**
	 * Get template
	 *
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldAddress.tpl');
	}
}