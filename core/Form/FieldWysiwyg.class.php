<?php
/**
 * M_FieldWysiwyg class
 *
 * M_FieldWysiwyg, a subclass of
 *
 * - {@link M_Field}
 * - {@link M_FieldText}
 * - {@link M_FieldTextarea}
 *
 * handles the input control that allows users to introduce a
 * (long) text, via a WYSIWYG editor.
 *
 * @package Core
 */
class M_FieldWysiwyg extends M_FieldTextarea {

	//define which buttons are available
	const BUTTON_ALIGN_LEFT = 'justifyleft';
	const BUTTON_ALIGN_CENTER = 'justifycenter';
	const BUTTON_ALIGN_RIGHT = 'justifyright';
	const BUTTON_ALIGN_JUSTIFY = 'justifyfull';
	const BUTTON_BOLD = 'bold';
	const BUTTON_ITALIC = 'italic';
	const BUTTON_SEPARATOR = '|';
	const BUTTON_UNDERLINE = 'underline';
	const BUTTON_LINK = 'link';
	const BUTTON_ANCHOR = 'anchor';
	const BUTTON_UNLINK = 'unlink';
	const BUTTON_BULLETLIST = 'bullist';
	const BUTTON_NUMLIST = 'numlist';
	const BUTTON_HR = 'hr';
	const BUTTON_REMOVEFORMAT = 'removeformat';
	const BUTTON_UNDO = 'undo';
	const BUTTON_REDO = 'redo';
	const BUTTON_PASTETEXT = 'pastetext';
	const BUTTON_PASTEWORD = 'pasteword';
	const BUTTON_SELECTALL = 'selectall';
	const BUTTON_STYLES = 'styleselect';
	const BUTTON_FORMATS = 'formatselect';
	const BUTTON_TABLES = 'table';
	const BUTTON_HTML = 'code';

	// add some default style constants: these can be used e.g. to position
	// a media-item, or any other kind of element
	const STYLE_DEFAULT_LEFT = 'align_left';
	const STYLE_DEFAULT_RIGHT = 'align_right';

	/**
	 * @var int
	 */
	protected $_width = 400;

	/**
	 * @var int
	 */
	protected $_height = 300;

	/**
	 * @var M_File
	 */
	protected $_contentCss;

	/**
	 * @var Array
	 */
	protected $_styles = array(
		self::STYLE_DEFAULT_LEFT => 'left',
		self::STYLE_DEFAULT_RIGHT => 'right'
	);

	/**
	 * @var bool
	 */
	protected $_resize = true;

	/**
	 * @var Array
	 */
	protected $_buttons = array(
		self::BUTTON_BOLD,
		self::BUTTON_ITALIC,
		self::BUTTON_UNDERLINE,
		self::BUTTON_SEPARATOR,
		self::BUTTON_ANCHOR,
		self::BUTTON_LINK,
		self::BUTTON_UNLINK,
		self::BUTTON_BULLETLIST,
		self::BUTTON_NUMLIST,
		self::BUTTON_HR,
		self::BUTTON_REMOVEFORMAT,
		self::BUTTON_SEPARATOR,
		self::BUTTON_UNDO,
		self::BUTTON_REDO,
		self::BUTTON_PASTETEXT,
		self::BUTTON_PASTEWORD,
		self::BUTTON_TABLES,
		self::BUTTON_HTML
	);

	/**
	 * Define which default formats are available
	 *
	 * These can be overwritten using {@link M_FieldWysiwyg::setFormats()}
	 *
	 * @var Array
	 */
	protected $_formats = array('h1' => 'header 1', 'h2' => 'header 2', 'h3' => 'header 3', 'p' => 'paragraph');

	/**
	 * Define the plugins the user is allowed to use. By default only the
	 * plugins for pasting and using tables are enabled.
	 *
	 * @access protected
	 * @var array
	 */
	protected $_plugins = array('paste', 'table');
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $id
	 * 		The field id. This value will be used to set {@link M_Field::$id}
	 * @return M_Field
	 */
	public function __construct($id) {
		parent::__construct($id);
		$this->setEnableSanitizedValue(FALSE);
	}
	
	/**
	 * @return int
	 */
	public function getHeight() {
		return $this->_height;
	}

	/**
	 * @param int $_height
	 */
	public function setHeight($_height) {
		$this->_height = $_height;
	}

	/**
	 * @return int
	 */
	public function getWidth() {
		return $this->_width;
	}

	/**
	 * @param int $_width
	 */
	public function setWidth($_width) {
		$this->_width = $_width;
	}

	/**
	 * @return M_File
	 */
	public function getContentCss() {
		return $this->_contentCss;
	}

	/**
	 * The stylesheet which will be used to display text in the Wysiwyg editor
	 *
	 * By default this refers to a file in /application/modules/admin/resources/css/tiny-mce.css,
	 * if you wish you can modify this to another location
	 *
	 * @param M_File
	 */
	public function setContentCss(M_File $_contentCss) {
		$this->_contentCss = $_contentCss;
	}

	/**
	 * @return Array
	 */
	public function getStyles() {
		return $this->_styles;
	}

	/**
	 * The styles to display in the editor
	 *
	 * You can specify which styles you want to be displayed in the editor. By
	 * doing so, you can let the user decide which title needs to be emphasised
	 *
	 * @param Array $_styles
	 */
	public function setStyles($_styles) {
		$this->_styles = $_styles;
	}

	/**
	 * @return Array
	 */
	public function getFormats() {
		return $this->_formats;
	}

	/**
	 * The formats to display in the editor
	 *
	 * You can specify which formats you want to be displayed in the editor. By
	 * doing so, you can let the user decide which text is e.g. <h1> or a <p>
	 *
	 * @param Array $_formats
	 */
	public function setFormats($_formats) {
		$this->_formats = $_formats;
	}

	/**
	 * Allow resizing?
	 *
	 * @return bool $_resize
	 */
	public function getResize() {
		return $this->_resize;
	}

	/**
	 * Allow the user to resize the editor (height and with)
	 *
	 * @param bool $_resize
	 */
	public function setResize($_resize) {
		$this->_resize = $_resize;
	}

	/**
	 * Get the buttons to display in the editor
	 * @return Array
	 */
	public function getButtons() {
		return $this->_buttons;
	}

	/**
	 * Set the buttons which are displayed in the editor
	 *
	 * @link http://wiki.moxiecode.com/index.php/TinyMCE:Control_reference
	 *
	 * @param Array $_buttons
	 */
	public function setButtons($_buttons) {
		$this->_buttons = $_buttons;
	}

	/**
	 * Get the plugins that are to be enabled in the editor
	 *
	 * @access public
	 * @return array
	 */
	public function getPlugins() {
		return $this->_plugins;
	}

	/**
	 * Set the plugins that are to be enabled in the editor
	 *
	 * @access public
	 * @param array $plugins
	 */
	public function setPlugins($plugins) {
		$this->_plugins = $plugins;
	}

	/**
	 * Add field definition
	 *
	 * This method is used by {@link M_Field::factory()}, to mount
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 *
	 * <code>styles</code>
	 *
	 * Set the available styles for the wysiwyg. For more information,
	 * read the docs on {@link M_FieldWysiwyg::getStyles()}.
	 *
	 * <code>formats</code>
	 *
	 * Set the available formats for the wysiwyg. For more information,
	 * read the docs on {@link M_FieldWysiwyg::getFormats()}.
	 *
	 * <code>contentcss</code>
	 *
	 * Set the relative location of the content-css file. For more information,
	 * read the docs on {@link M_FieldWysiwyg::setContentCss()}.
	 *
	 * <code>resize</code>
	 *
	 * Enable/disable resizing of the text-editor. For more information,
	 * read the docs on {@link M_FieldWysiwyg::setContentCss()}.
	 *
	 * <code>buttons</code>
	 *
	 * Set the buttons to display in the editor. For more information, read
	 * the docs on {@link M_FieldWysiwyg::setButtons()}
	 *
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		switch($spec) {
			case 'styles':
				$styles = array();
				if (M_Helper::isInstanceOf($definition, 'MI_Config')) {
					foreach($definition AS $style => $label) {
						$styles[$style] = $label;
					}
				}elseif (is_string($definition) && strlen($definition) > 0) {
					$styles[] = $definition;
				}
				$this->setStyles($styles);
				break;
			case 'formats':
				$formats = array();
				if (M_Helper::isInstanceOf($definition, 'MI_Config')) {
					foreach($definition AS $format => $label) {
						$formats[$format] = $label;
					}
				}elseif (is_string($definition) && strlen($definition) > 0) {
					$formats[] = $definition;
				}
				$this->setFormats($formats);
				break;
			case 'contentcss':
				$this->setContentCss(new M_File($definition));
				break;
			case 'resize':
				$this->setResize($definition);
				break;
			case 'buttons':
				if (M_Helper::isInstanceOf($definition, 'MI_Config')) {
					throw new M_Exception('Cannot use list to add buttons: please use a comma-separated string');
				}elseif (is_string($definition) && strlen($definition) > 0) {
					$this->setButtons(explode(',', $definition));
				}
				break;
			case 'plugins':
				if (M_Helper::isInstanceOf($definition, 'MI_Config')) {
					throw new M_Exception('Cannot use list to add plugins: please use a comma-separated string');
				}elseif (is_string($definition) && strlen($definition) > 0) {
					$this->setPlugins(explode(',', $definition));
				}
				break;
			case 'addButtons':
				array_push($this->_buttons, $definition);
				break;
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}

	/**
	 * Get input control view
	 *
	 * This method overrides {@link M_Field::getInputView()}.
	 *
	 * This method provides with an {@link M_ViewFieldWysiwyg}
	 * object, which allows {@link M_Field} to include the input
	 * control in the view that is returned by
	 * {@link M_Field::getView()}.
	 *
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldText
	 * @return M_View
	 */
	public function getInputView() {
		$javascript = M_ViewJavascript::getInstance();

		//only add tinymce-javascript once
		if (!$javascript->isContextStored('tinymce-' . $this->getId())) {

			$tinyMcePath = M_Loader::getResourcesPath('admin') . DIRECTORY_SEPARATOR .
				M_ViewHtml::FOLDER_JAVASCRIPT . DIRECTORY_SEPARATOR .
				'tiny_mce-3.5.6' . DIRECTORY_SEPARATOR;

			//add javascript source file
			$javascriptFileJquery = new M_File(
				$tinyMcePath .
				'jquery.tinymce.js'
			);

			//also add the original file. This is needed to prevent TinyMCE
			//to throw errors when the load of the page takes to long. E.g.
			//when using Google Maps fields
			$javascriptFile = new M_File(
				$tinyMcePath .
				'tiny_mce.js'
			);
			$javascript
				->addFile($javascriptFileJquery, 'tinymce-jquery')
				->addFile($javascriptFile, 'tinymce');

			//apply tinymce to this, and all other, wysiwyg fields
			$javascriptInline = '
				$("#id'. $this->getId() .'").tinymce({
					script_url : "'.M_Request::getLinkWithoutPrefix(M_Loader::getResourcesPath('admin') . '/javascript/tiny_mce/tiny_mce.js').'",
					theme : "advanced",
					plugins : "'.$this->_getPluginsJavascript().'",
					theme_advanced_buttons2 : "",
					theme_advanced_buttons3 : "",
					paste_remove_styles: true,
					theme_advanced_statusbar_location : "bottom",
					extended_valid_elements : "object[type|data|width|height|classid|codebase],embed[src|type|width|height|classid|codebase],img[id|dir|lang|longdesc|usemap|style|class|src|onmouseover|onmouseout|alt|title|hspace|vspace|width|height|align],sub,sup",
					relative_urls : false,
					setup : function(ed) {
							  ed.onInit.add(function() {
								 tinyMCEInitEventDispatcher();
							  });
							},
					theme_advanced_path : false,
					elements: "'.$this->getId().'",
					width : '.$this->getWidth().',
					height : '.$this->getHeight().
					$this->_getButtonJavascript().
					$this->_getContentcssJavascript().
					$this->_getFormatsJavascript().
					$this->_getStylesJavascript().
					$this->_getResizeJavascript().'
				});';

			// Also, dispatch event to notify about new WYSIWYG fields
			$javascriptInline .= '
			function tinyMCEInitEventDispatcher() {
				$(document).trigger("M_Wysiwyg");
			}';

			$javascript->addInline($javascriptInline, 'tinymce-' . $this->getId());
		}

		//create view
		$view = new M_ViewFieldWysiwyg($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}

	/**
	 * Get the javascript for advanced styling
	 *
	 * If no styles are set, there is no need to add javascript to the tinymce
	 * init. In any other case: the styles will be parsed and added to the
	 * init
	 *
	 * @see M_FieldWysiwyg::setStyles()
	 * @return string
	 */
	private function _getStylesJavascript() {
		$styles = $this->getStyles();
		//if no styles are set: do nothing
		if (count($styles) == 0) {
			return '';
		}

		$stylesJs = array();
		foreach($styles AS $style => $label) {
			$stylesJs[] = t($label).'='.$style;
		}

		return ',' . M_CodeHelper::getNewLine() . 'theme_advanced_styles: "' . implode(', ',$stylesJs).'"';
	}

	/**
	 * Get the javascript for advanced formats
	 *
	 * If no formats are set, there is no need to add javascript to the tinymce
	 * init. In any other case: the formats will be parsed and added to the
	 * init
	 *
	 * @see M_FieldWysiwyg::setFormats()
	 * @return string
	 */
	private function _getFormatsJavascript() {
		$formats = $this->getFormats();
		//if no styles are set: do nothing
		if (count($formats) == 0) {
			return '';
		}

		$formatCollection = array();
		foreach($formats AS $format => $label) {
			$formatCollection[] = t($label).'='.$format;
		}

		return ',' . M_CodeHelper::getNewLine() . 'theme_advanced_blockformats: "' . implode(', ',$formatCollection).'"';
	}

	/**
	 * Get the javascript-code which is needed for the content-css attribute
	 *
	 * Check if a content-css file exists, if so return the required javascript
	 * to make this work
	 *
	 * @see M_FieldWysiwyg::setContentcss()
	 * @return string
	 */
	private function _getContentcssJavascript() {
		//content-css
		$contentCssPath = false;
		if ($this->getContentCss()) {
			$contentCssPath = M_Request::getLinkWithoutPrefix(
				$this->getContentCss()->getPath()
			);
		}

		//if not set, we try to load the default tiny-mce css, located
		//in application/modules/admin/resources/css/tiny-mce.css
		else {
			$contentCssFile = new M_File(M_Loader::getRelative(
				M_Loader::getResourcesPath('admin') . DIRECTORY_SEPARATOR .
				M_ViewHtml::FOLDER_CSS . DIRECTORY_SEPARATOR .
				'tiny-mce.css'
			));

			if ($contentCssFile->exists()) {
				$contentCssPath = M_Request::getLinkWithoutPrefix(
					$contentCssFile->getPath()
				);
			}
		}

		if ($contentCssPath) {
			return ',' . M_CodeHelper::getNewLine() . ' content_css : "' . $contentCssPath . '"';
		}else {
			return '';
		}
	}

	/**
	 * Get the javascript-code which is needed for the resize attribute
	 *
	 * @see M_FieldWysiwyg::setResize()
	 * @return string
	 */
	private function _getResizeJavascript() {
		$resize = ($this->getResize()) ? 'true': 'false';
		$js = ',' . M_CodeHelper::getNewLine();
		$js .= 'theme_advanced_resizing : ' . $resize;

		return $js;
	}

	/**
	 * Get the javascript-code which is needed to add the correct buttons
	 *
	 * @see M_FieldWysiwyg::getButtons()
	 * @return string
	 */
	private function _getButtonJavascript() {
		//check if buttons are set
		$buttons = $this->getButtons();
		if (count($buttons) == 0) {
			return '';
		}

		$stylesCount = count((array)$this->getStyles());
		$formatCount = count((array)$this->getFormats());

		if ($stylesCount > 0 || $formatCount > 0) {
			array_push($buttons, self::BUTTON_SEPARATOR);
			if ($stylesCount > 0) array_push($buttons, self::BUTTON_STYLES);
			if ($formatCount > 0) array_push($buttons, self::BUTTON_FORMATS);
		}

		$buttonJs = ',' . M_CodeHelper::getNewLine();
		$buttonJs .= 'theme_advanced_buttons1 : "';
		$buttonJs .= implode(',', $buttons);
		$buttonJs .= '"';

		return $buttonJs;
	}

	/**
	 * Get the javascript code for the plugins
	 *
	 * @access private
	 * @return string
	 */
	private function _getPluginsJavascript() {
		return implode(',', $this->_plugins);
	}
}