<?php
/**
 * M_FieldCron class
 * 
 * M_FieldCron, a subclass of {@link M_Field}, handles the input 
 * control that allows users to introduce a cronjob definition. To
 * learn more about cronjob definitions, read {@link M_Cron::getField()}.
 * 
 * This field allows the user to introduce the first five fields of
 * the cronjob definition: the time period fields. Basically, using
 * this field, the user can describe a time pattern.
 * 
 * @package Core
 */
class M_FieldCron extends M_Field {
	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * M_FieldCron will fetch the introduced value, and return the
	 * value in the form of an array. The keys that are present in 
	 * this array depend on the base pattern that the user has picked.
	 * Possible values for the base pattern are: "never", "monthly", 
	 * "weekly", or "daily".
	 * 
	 * Return value for "never"
	 * <code>
	 *    Array (
	 *       'value' => 'never'
	 *    )
	 * </code>
	 * 
	 * Return value for "monthly"
	 * <code>
	 *    Array (
	 *       'value'   => 'monthly'
	 *       'day'     => (integer|*) 1-31
	 *       'month'   => (integer) 0-12
	 *       'hours'   => (integer|*) 0-23
	 *       'minutes' => (integer|*) 0-59
	 *    )
	 * </code>
	 * 
	 * Return value for "weekly"
	 * <code>
	 *    Array (
	 *       'value'   => 'weekly'
	 *       'wday'    => (integer) 0-6
	 *       'hours'   => (integer|*) 0-23
	 *       'minutes' => (integer|*) 0-59
	 *    )
	 * </code>
	 * 
	 * Return value for "daily"
	 * <code>
	 *    Array (
	 *       'value'   => 'daily'
	 *       'hours'   => (integer|*) 0-23
	 *       'minutes' => (integer|*) 0-59
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return array|boolean
	 */
	public function getValue() {
		// Check if value has been sent:
		$base = M_Request::getVariable($this->id, FALSE, NULL, M_Request::POST);
		
		// No value has been sent, so we check the default value of the
		// field. The default value may contain a cronjob schedule
		// specification
		if($base === FALSE) {
			// If the default value contains a cronjob schedule specification
			$spec = $this->getDefaultValue();
			if($spec) {
				// Construct an M_Cron object, to extract info about 
				// the cronjob scheduling
				$cron = new M_Cron($spec);
				
				// If a day has been given, this cron runs monthly
				$tmp = $cron->getField(M_Cron::DAY);
				if($tmp != '*') {
					return array(
						'value' => 'monthly',
						'day' => $tmp,
						'month' => $cron->getField(M_Cron::MONTH),
						'hours'   => $cron->getField(M_Cron::HOUR),
						'minutes' => $cron->getField(M_Cron::MINUTE)
					);
				}
				
				// If a weekday has been provided, this cron runs
				// weekly
				$tmp = $cron->getField(M_Cron::WEEKDAY);
				if($tmp != '*') {
					return array(
						'value' => 'weekly',
						'wday' => $tmp,
						'hours'   => $cron->getField(M_Cron::HOUR),
						'minutes' => $cron->getField(M_Cron::MINUTE)
					);
				}
				
				// If we're still here, the cron runs daily
				return array(
					'value' => 'daily',
					'hours'   => $cron->getField(M_Cron::HOUR),
					'minutes' => $cron->getField(M_Cron::MINUTE)
				);
			}
		}
		// If value has been sent:
		else {
			// Get the base pattern. Possible values for the base pattern
			// are: "never", "monthly", "weekly", or "daily".
			$base = parent::getValue();
			
			// If the user has picked "never", we return that value:
			if($base == 'never') {
				return array(
					'value' => 'never'
				);
			}
			
			// Get the base name of the field:
			$id = $this->getId();
			
			
			// Time (hour + minutes) is always included in the field
			// interface, whether the user chooses "monthly", "weekly", or 
			// "daily". We get the time fields:
			// - minutes:
			$minutes = M_Request::getVariable($id . '-minutes', '*', M_Request::TYPE_STRING);
			if($minutes != '*') {
				$c = (is_numeric($minutes) && $minutes >= 0 && $minutes < 60);
				if(!$c) {
					$minutes = 0;
				}
			}
				
			// - hours:
			$hours = M_Request::getVariable($id . '-hours', '*', M_Request::TYPE_STRING);
			if($hours != '*') {
				$c = (is_numeric($hours) && $hours >= 0 && $hours < 24);
				if(!$c) {
					$hours = 0;
				}
			}
			
			// Depending on the base pattern, we fetch the other values:
			switch($base) {
				// Monthly:
				case 'monthly':
					// "Day X ..."
					// (Numerical day of the month; in the range of 1 - 31)
					$day = M_Request::getVariable($id . '-day', '*', M_Request::TYPE_STRING);
					if($day != '*') {
						$c = (is_numeric($day) && $day > 0 && $day < 32);
						if(!$c) {
							$day = 1;
						}
					}
					
					// "... of X month"
					// (Numeric number of month; in the range of 0 - 12. 
					// The number 0 means "every month")
					$month = M_Request::getVariable($id . '-month', '*', M_Request::TYPE_INT);
					if($month != '*') {
						$c = ($month >= 0 && $month < 13);
						if(!$c) {
							$month = 0;
						}
					}
					
					// Return value:
					return array(
						'value'   => 'monthly',
						'day'     => $day,
						'month'   => $month,
						'hours'   => $hours,
						'minutes' => $minutes
					);
				
				// Weekly:
				case 'weekly':
					// Every [day-of-week]
					// (Numeric number of weekday. 0 for Sunday, 6 for
					// Saturday)
					$weekday = M_Request::getVariable($id . '-wday', 0, M_Request::TYPE_INT);
					$c = ($weekday >= 0 && $weekday < 7);
					if(!$c) {
						$weekday = 0;
					}
					
					// Return value:
					return array(
						'value'   => 'weekly',
						'wday'    => $weekday,
						'hours'   => $hours,
						'minutes' => $minutes
					);
				
				// Daily (Default):
				case 'daily':
				default:
					// Return value:
					return array(
						'value'   => 'daily',
						'hours'   => $hours,
						'minutes' => $minutes
					);
			}
		}
		
		// If still here, we return "never"
		return array(
			'value' => 'never'
		);
	}
	
	/**
	 * Deliver the final value
	 * 
	 * This method overrides {@link M_Field::deliver()}.
	 * 
	 * In the case of {@link M_FieldCron}, this method will deliver
	 * the final cronjob definition string. To learn more about the
	 * cronjob definition string, read {@link M_Cron::getField()}.
	 * 
	 * NOTE:
	 * If the user has picked "never" as the base pattern, this method
	 * will deliver an empty string (see {@link M_FieldCron::getValue()}.
	 * 
	 * @access public
	 * @see M_Field::deliver()
	 * @see M_Field::getValue()
	 * @see M_FieldCron::getValue()
	 * @return mixed
	 */
	public function deliver() {
		// Get the field value, as delivered by the superclass (will
		// provide with result of getValue()).
		$temp = parent::deliver();
		
		// If the base pattern is "never", return empty string:
		if($temp['value'] == 'never') {
			return '';
		}
		
		// Compose cronjob definition:
		$cronjob  = $temp['minutes'] . ' ';
		$cronjob .= $temp['hours'] . ' ';
		$cronjob .= isset($temp['day']) ? $temp['day'] . ' ' : '* ';
		$cronjob .= isset($temp['month']) ? $temp['month'] . ' ' : '* ';
		$cronjob .= isset($temp['wday']) ? $temp['wday'] : '*';
		
		// return cronjob definition:
		return $cronjob;
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldText} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldText
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldCron($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}
?>