<?php
/**
 * M_FieldGroup class
 * 
 * M_FieldGroup enables the user to add fields to a group, with a title and a
 * description. In doing so, it's possible to give more meaning to a group and
 * display them as a real visible group too.
 * 
 * Each group consists of a set of fields ({@link M_FormField}), a title
 * and a description. It's also possible to specify whether or not the group
 * is foldable.
 * 
 * @package Core
 */
class M_FieldGroup {
	
	/**
	 * The group id
	 * 
	 * Every group has an id, this way you can retrieve the group later on,
	 * and e.g. add fields, change the description, ...
	 * 
	 * @access private
	 * @var string
	 */
	private $_id = '';
	
	/**
	 * Module Owner
	 * 
	 * This property stores the Module Owner, which is used to render
	 * the field's views. For more information, read the docs on:
	 * 
	 * - {@link M_ViewCoreHtml::setModuleOwner()}
	 * - {@link M_ViewCoreHtml::getModuleOwner()}
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_moduleOwnerId;
	
	/**
	 * Collection of {@link M_Field} objects.
	 * 
	 * A form interface consists of a collection of input 
	 * controls. Each of these input controls are represented 
	 * by a {@link M_Field} object.
	 * 
	 * @access private
	 * @var array
	 */
	private $_fields = array();
	
	/**
	 * The title of the group
	 * 
	 * @var string
	 */
	private $_title;
	
	/**
	 * The description of the group
	 * 
	 * @var bool
	 */
	private $_description;
	
	/**
	 * Set if this group is foldable/not
	 * 
	 * @var bool
	 */
	private $_foldable;
	
	/**
	 * Is folded currently?
	 * 
	 * @access private
	 * @var bool
	 */
	private $_folded;

	/**
	 * The form tho which this group is attached
	 * 
	 * @var M_Form
	 */
	private $_form;
	
	/**
	 * View
	 * 
	 * This property holds the name of the {@link M_View} class that 
	 * will be used to render the form interface.
	 * 
	 * @access private
	 * @see M_Form::setView()
	 * @see M_Form::getView()
	 * @var string
	 */
	private $_view = 'M_ViewFieldGroup';
	
	/**
	 * View Object
	 * 
	 * Holds the {@link M_View} we have to use to render the field. Overwrites
	 * the set classname set in {@link M_FieldGroup::_view}, if present. Note
	 * that this view will always implement {@link M_ViewFieldGroup}.
	 * 
	 * @access private
	 * @see M_Form::setViewObject()
	 * @see M_Form::getViewObject()
	 * @var M_ViewFieldGroup
	 */
	private $_viewObject;
	
	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 * @param string $_description
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
	}
	
	/**
	 * @return string
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * @param string $_id
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}
	
	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * @param string $_title
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
	}
	/**
	 * @return bool
	 */
	public function getFoldable() {
		return $this->_foldable;
	}
	
	/**
	 * @param bool $_foldable
	 */
	public function setFoldable($_foldable) {
		$this->_foldable = $_foldable;
	}
	
	/**
	 * Setter: is folded currently?
	 * 
	 * @access public
	 * @param bool $flag
	 * @return void
	 */
	public function setFolded($flag) {
		$this->_folded = (bool) $flag;
	}
	
	/**
	 * Getter: is folded currently?
	 * 
	 * @access public
	 * @return bool|null
	 */
	public function getFolded() {
		return $this->_folded;
	}
	
	/**
	 * Create a new group
	 * 
	 * When creating a group, the ID is mandatory. If you wish you can 
	 * immediately set the title, description and the fields.
	 * 
	 * @param string $id
	 * @param string $title
	 * @param string $description
	 * @param bool $foldable
	 * @param array $fields
	 * 		Array containing the fieldIds
	 * @param bool $folded
	 */
	public function __construct($id, $title = null, $description = null, $foldable = null, $fields = null, $folded = null) {
		$this->setId($id);
		$this->setTitle($title);
		$this->setDescription($description);
		if(!is_null($foldable)) $this->setFoldable($foldable);
		if(!is_null($folded)) $this->setFolded($folded);
		
		//check the fields
		if (!is_null($fields)) {
			if (!is_array($fields)) {
				throw new M_FormException('Fields not of type array. Cannot add fields');
			}
			
			foreach($fields AS $field) {
				$this->addField($field);
			}
		}
	}
	
	/**
	 * Add a field to the field-group
	 * 
	 * This method will add a field - an object of a {@link M_Field}
	 * subclass - to the group.
	 * 
	 * @access public
	 * @param M_Field $field
	 * 		The field to be added to the field-group
	 * @return M_FieldGroup
	 */
	public function addField(M_Field $field) {
		$this->_fields[$field->getId()] = $field;

		//tell the field to which form it's attached
		if($this->_form) {
			$field->__form($this->_form);
		}
		
		return $this;
	}

	/**
	 * Remove a field from the form
	 *
	 * This method will remove the requested field from the form
	 *
	 * @access public
	 * @param string $fieldId
	 * 		The ID of the field
	 * @return M_Form $form
	 *		Returns itself, for a fluent programming interface
	 */
	public function removeField($fieldId) {
		if(isset($this->_fields[$fieldId])) {
			unset($this->_fields[$fieldId]);
		}
		return $this;
	}
	
	/**
	 * Set all fields for this group
	 *
	 * Array of {@link M_Field}-objects
	 * @param array $fields
	 * @return void
	 */
	public function setFields(array $fields) {
		$this->_fields = array();
		foreach($fields AS $field) {
			$this->addField($field);
		}
	}
	
	/**
	 * Get a field
	 * 
	 * This method will fetch the {@link M_Field} object by the
	 * given ID. M_FieldGroup will perform a lookup of the field, with the
	 * given field ID, inside its contained collection of {@link M_Field}
	 * objects.
	 * 
	 * If the requested field has been found in the collection,
	 * M_FieldGroup will return the corresponding {@link M_Field} object.
	 * If not, M_FieldGroup will return FALSE.
	 * 
	 * @access public
	 * @param string $id
	 * 		The field's ID
	 * @return M_Field|FALSE
	 */
	public function getField($id) {
		return isset($this->_fields[$id]) ? $this->_fields[$id] : FALSE;
	}
	
	/**
	 * Get number of contained fields
	 * 
	 * This method will return the total number of fields that
	 * have been added previously with {@link M_Form::addField()}.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfFields() {
		return count($this->_fields);
	}
	
	/**
	 * Get all fields
	 *
	 * @return M_ArrayIterator
	 */
	public function getFields() {
		return new M_ArrayIterator($this->_fields);
	}
	
	/**
	 * Get view
	 * 
	 * This method will construct the {@link M_ViewFieldGroup} object, of
	 * which the class name has been set previously with the method
	 * {@link M_FieldGroup::setView()}.
	 * 
	 * @access public
	 * @see M_Field::setView()
	 * @return M_ViewFieldGroup
	 */
	public function getView() {
		// If a view object is given, use it. Otherwise, construct, a new one
		// with the set class name
		$view = $this->_viewObject ?: new $this->_view($this);

		// Assign the presentation variables to it, and return:
		$view->setModuleOwner($this->getModuleOwner());
		$view->setId($this->_id);
		return $view;
	}
	
	/**
	 * Set the name for the view of this group
	 * 
	 * @access public
	 * @param string
	 * @return void
	 */
	public function setView($view) {
		$this->_view = $view;
	}
	
	/**
	 * Set View Object
	 * 
	 * Allows for specifying the {@link M_ViewFieldGroup} we have to use
	 * for rendering. If not provided, a new instance of the classname
	 * provided to {@link M_FieldGroup::setView()} will be constructed.
	 * 
	 * @access public
	 * @param M_ViewFieldGroup $view
	 * @return void
	 */
	public function setViewObject(M_ViewFieldGroup $view) {
		$this->_viewObject = $view;
	}
	
	/**
	 * Set module owner
	 * 
	 * Will set the Module Owner of the field. {@link M_Field} will use
	 * the Module Owner to render the field's views. For more information, 
	 * read the docs on:
	 * 
	 * - {@link M_ViewCoreHtml::setModuleOwner()}
	 * - {@link M_ViewCoreHtml::getModuleOwner()}
	 * - {@link M_FieldGroup::getView()}
	 * 
	 * @access public
	 * @param string $moduleId
	 * 		The Module Owner of the views
	 * @return void
	 */
	public function setModuleOwner($moduleId) {
		$this->_moduleOwnerId = (string) $moduleId;
	}
	
	/**
	 * Get module owner
	 * 
	 * Will provide with the Module Owner of the field.
	 *
	 * @see M_FieldGroup::setModuleOwner()
	 * @access public
	 * @return string
	 */
	public function getModuleOwner() {
		return $this->_moduleOwnerId;
	}
	
	/**
	 * Get Error Count
	 * 
	 * Will calculate and return the number of error messages present in the
	 * {@link M_Field} objects that are contained within this
	 * {@link M_FieldGroup}
	 * 
	 * @access public
	 * @return int
	 */
	public function getErrorCount() {
		// Initialize the count
		$count = 0;

		// For each field
		foreach($this->getFields() as $field) {
			/* @var $field M_Field */
			
			// If an error message is present, increase the count
			if($field->getErrorMessage()) $count++;
		}
		
		// Return the count
		return $count;
	}

	/**
	 * Magic method: Attach to {@link M_Form}
	 *
	 * This method, optionally implemented by M_Field subclasses,
	 * allows the field to interact with the {@link M_Form} it is
	 * being attached to. For example, an upload field will want
	 * to set some additional form properties ("enctype", for example),
	 * for the file upload to work properly.
	 *
	 * NOTE:
	 * The abstract M_Field class provides with a default implementation,
	 * which does not do anything to the form.
	 *
	 * @access public
	 * @param M_Form $form
	 * 		The form to which the field is being added.
	 * @return void
	 */
	public function __form(M_Form $form) {
		$this->_form = $form;

		// Call the magic __form method for each contained field:
		/* @var $field M_Field */
		foreach($this->getFields() as $field) {
			$field->__form($form);
		}
	}
}