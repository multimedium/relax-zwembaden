<?php

class M_ViewFieldUploadMultiple extends M_ViewField {
		
	/* -- PROTECTED/PRIVATE -- */
	
	protected function getHtml() {
		/* No specific HTML */
		return false;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldUploadMultiple.tpl');
	}
	
}

?>