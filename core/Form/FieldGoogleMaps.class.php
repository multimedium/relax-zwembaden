<?php
/**
 * M_FieldGoogleMaps class
 * 
 * M_FieldGoogleMaps, a subclass of {@link M_FieldText}, handles the
 * input control that allow users to introduce a complete address including
 * lattitude and longitude.
 *
 * The postition will be calculated based on the address, but using interaction
 * with the map the user can decide himself which is the correct position.
 * 
 * @package Core
 */
class M_FieldGoogleMaps extends M_FieldText {

	/**
	 * Width of the field
	 *
	 * @var int
	 */
	protected $_width = 600;

	/**
	 * Height of the field
	 *
	 * @var int
	 */
	protected $_height = 200;

	/**
	 * The type of map
	 * 
	 * @var string
	 */
	protected $_mapType = self::MAP_TYPE_ROAD;

	/**
	 * Indicate the default zoom-level
	 * @var int
	 */
	protected $_zoomLevel = 6;

	/**
	 * The id of {@link M_FieldAddress} to which it is connected
	 *
	 * @var string
	 */
	protected $_addressFieldId = '';

	/**
	 * Holds the title of the marker
	 * 
	 * @var string
	 */
	protected $_markerTitle;

	/**
	 * Set of constants which hold the different kinds of map-types
	 */
	CONST MAP_TYPE_ROAD = 'ROADMAP';
	CONST MAP_TYPE_SATELLITE = 'SATELLITE';
	CONST MAP_TYPE_HYBRID = 'HYBRID';
	CONST MAP_TYPE_TERRAIN = 'TERRAIN';

	/**
	 * Get AddressFieldId
	 *
	 * @return string
	 */
	public function getAddressFieldId() {
		return $this->_addressFieldId;
	}

	/**
	 * Set AddressFieldId
	 *
	 * @param string $arg
	 * @return M_FieldGoogleMaps
	 */
	public function setAddressFieldId($arg) {
		$this->_addressFieldId = $arg;
		return $this;
	}

/**
	 * Get Width
	 *
	 * @return int
	 */
	public function getWidth() {
		return $this->_width;
	}

	/**
	 * Set Width
	 *
	 * @param int $arg
	 * @return M_FieldGoogleMaps
	 */
	public function setWidth($arg) {
		$this->_width = $arg;
		return $this;
	}

	/**
	 * Get Height
	 *
	 * @return int
	 */
	public function getHeight() {
		return $this->_height;
	}

	/**
	 * Set Height
	 *
	 * @param int $arg
	 * @return M_FieldGoogleMaps
	 */
	public function setHeight($arg) {
		$this->_height = $arg;
		return $this;
	}

	/**
	 * Default latitude value
	 *
	 * This property holds the default latitude of the map: we use the middle
	 * of Belgium
	 *
	 * @link http://www.nsesoftware.nl/wiki/maps.asp?params=50_38_28_N_4_40_05_E_region:BE_type:landmark&pagename=Geografisch_middelpunt_van_Belgi%C3%AB
	 * @access public
	 * @return float
	 * @author Ben Brughmans
	 */
	public function getDefaultLatitude() {
		return 50.6411111111111;
	}

	/**
	 * Default longitude value
	 *
	 * This property holds the default longitude of the map: we use the middle
	 * of Belgium
	 *
	 * @link http://www.nsesoftware.nl/wiki/maps.asp?params=50_38_28_N_4_40_05_E_region:BE_type:landmark&pagename=Geografisch_middelpunt_van_Belgi%C3%AB
	 * @access public
	 * @return float
	 * @author Ben Brughmans
	 */
	public function getDefaultLongitude() {
		return 4.66805555555556;
	}

	/**
	 * Get Longitude
	 *
	 * @return float
	 */
	public function getLongitude() {
		$longitude = self::getLongitudeFromDeliveredValue($this->getValue());
		if ($longitude) return $longitude;
		
		return $this->getDefaultLongitude();
	}

	/**
	 * Parse the latitude out of the delivered value
	 *
	 * The final (delivered) value for a Google Maps field will be a concatenated
	 * value holding both latitude and longitude. To get the latitude out of
	 * this value, you can use this method
	 *
	 * @example
	 *
	 * $longitude = M_FieldGoogleMaps::getLatitudeFromDeliveredValue(51.3983569,4.7628241);
	 *
	 * Will return 51.3983569
	 *
	 * @author Ben Brughmans
	 * @access public
	 * @static
	 * @param string $value
	 * @return string|false
	 */
	public static function getLatitudeFromDeliveredValue($value) {
		if(strstr($value, ",")) {
			$val = explode(",", $value);
			return M_Helper::getArrayElement(0, $val);
		}
		
		return false;
	}
	
	/**
	 * Parse the longitude out of the delivered value
	 *
	 * The final (delivered) value for a Google Maps field will be a concatenated
	 * value holding both latitude and longitude. To get the longitude out of
	 * this value, you can use this method
	 *
	 * @example
	 *
	 * $longitude = M_FieldGoogleMaps::getLongitudeFromDeliveredValue(51.3983569,4.7628241);
	 *
	 * Will return 4.7628241
	 *
	 * @author Ben Brughmans
	 * @access public
	 * @static
	 * @param string $value
	 * @return string|false
	 */
	public static function getLongitudeFromDeliveredValue($value) {
		if(strstr($value, ",")) {
			$val = explode(",", $value);
			return M_Helper::getArrayElement(1, $val);
		}

		return false;
	}

	/**
	 * Get Latitude
	 *
	 * @return float
	 */
	public function getLatitude() {
		if(strstr($this->getValue(), ",")) {
			$val = explode(",", $this->getValue());
			return M_Helper::getArrayElement(0, $val);
		}
		return $this->getDefaultLatitude();
	}

	/**
	 * Get MapType
	 *
	 * @return string
	 */
	public function getMapType() {
		//check if a {@link M_Request} variable is available for the maptype
		//the POST-value is the one which the user has chosen in the browser
		$postedMapType = M_Request::getVariable(
			$this->getId().'-map-type',
			FALSE
		);

		//if not: return default value
		if ($postedMapType === FALSE) {
			return $this->_mapType;
		}
		//if yes: return POST-value
		else return $postedMapType;
	}

	/**
	 * Set MapType
	 *
	 * @param string $arg
	 * @return FieldGoogleMaps
	 */
	public function setMapType($arg) {
		$this->_mapType = $arg;
		return $this;
	}

	/**
	 * Get MarkerTitle
	 *
	 * @return string
	 */
	public function getMarkerTitle() {
		//return a default value
		if (is_null($this->_markerTitle)) {
			return t('Click and drag to adjust position');
		}
		return $this->_markerTitle;
	}

	/**
	 * Set MarkerTitle
	 *
	 * @param string $arg
	 * @return M_FieldGoogleMaps
	 */
	public function setMarkerTitle($arg) {
		$this->_markerTitle = $arg;
		return $this;
	}

	/**
	 * Get ZoomLevel
	 *
	 * @return int
	 */
	public function getZoomLevel() {
		//check if a {@link M_Request} variable is available for the zoom-level.
		//the POST-value is the one which the user has chosen in the browser
		$postedZoomLevel = M_Request::getVariable(
			$this->getId().'-zoom-level',
			FALSE
		);

		//if not: return default value
		if ($postedZoomLevel === FALSE) {
			return $this->_zoomLevel;
		}
		//if yes: return POST-value
		else return $postedZoomLevel;
	}

	/**
	 * Set ZoomLevel
	 *
	 * @param int $arg
	 * @return FieldGoogleMaps
	 */
	public function setZoomLevel($arg) {
		$this->_zoomLevel = $arg;
		return $this;
	}


	/**
	 * Whether or not to show the marker
	 *
	 * In most cases we don't want to show the marker initially, but only
	 * when the user has entered an address. By default we don't show the marker,
	 * but only when an address is entered
	 * @var bool
	 */
	public function getShowMarker() {
		//if this field is not linked to an address field: always show the
		//marker by default
		if ($this->getAddressFieldId() == '') {
			return true;
		}

		//if it is linked to an address field: we only want to show it when
		//the user has entered an address before
		else {
			//if a location is known: show the marker
			if ((string)$this->getValue() != '') {
				return true;
			}else {
				return false;
			}
		}
	}

	/**
	 * Add field definition
	 *
	 * This method is used by {@link M_Field::factory()}, to mount
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 *
	 * <code>height</code>
	 *
	 * Set the height of the field
	 *
	 * <code>zoomLevel</code>
	 *
	 * Set default zoom level of the map
	 *
	 * <code>mapType</code>
	 *
	 * Set map-type: terrain, hybrid, ...
	 *
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// Height (expressed in pixels) of the map
			case 'height':
				$this->setHeight($definition);
				break;

			// Zoom level on the map
			case 'zoomLevel':
				$this->setZoomLevel($definition);
				break;

			// Set the type of map
			case 'mapType':
				$this->setMapType($definition);
				break;

			// Address Field ID
			case 'addressFieldId':
				$this->setAddressFieldId($definition);
				break;

			// Marker title
			case 'markerTitle':
				$this->setMarkerTitle($definition);
				break;

			// Other properties
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}

	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldGoogleMaps} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @return M_View
	 */
	public function getInputView() {
		//create javascript
		$script = "$(document).ready(function(){";
		$script .=	"var opts = {";
		$script .=		"latitude:'".$this->getLatitude()."',";
		$script .=		"longitude:'".$this->getLongitude()."',";
		$script .=		"zoom:".$this->getZoomLevel().",";
		$script .=		"maptype:'".$this->getMapType()."',";
		$script .=		"title:'".$this->getMarkerTitle()."',";
		$script .=		"addressFieldId:'".$this->getAddressFieldId()."',";
		$script .=		"fieldId: '".$this->getId()."',";
		$script .=		"showMarker: ".($this->getShowMarker() ? "true" : "false");
		$script .=	"};";

		//create map
		$script .=	"$('#".$this->getId()."-map').gMap(opts);";
		$script .= "});";

		//add javascript
		$js = M_ViewJavascript::getInstance();
		$js->addInline($script, 'google-maps-' . $this->getId());

	    $view = new M_ViewFieldGoogleMaps($this);
		$view->setId($this->getId());
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}