<?php
class M_ViewFieldAutocomplete extends M_ViewField {
	
	/**
	 * @const string
	 * 
	 * This constant holds the suffix which is appended to id and name the field 
	 * which contains the textValue for the autocomplete field.
	 */
	const TEXTVALUE_SUFFIX = '_textValue';
	
	/**
	 * Get html
	 *
	 * @return string
	 */
	protected function getHtml() {
		/* @var $field M_FieldAutocomplete */
		$field = $this->getField();
		// Render input control

		$suffix = '';
		$javascript = '';
		$value = $field->getValue();

		//suffix is only applicable when user forces this
		if ($field->getHasTextValue()) {
			$suffix = self::TEXTVALUE_SUFFIX;
			$value = $field->getTextValue();
		}

		$html = '<input type="text"' 
			. ' class="field-text autocomplete"'
			. ' name="'. $field->getId() . $suffix . '"' 
			. ' style="width:'. $field->getWidth() .'px;"' 
			. ' id="'. $this->getId() . $suffix . '"';

		if(!is_null($field->getHint())) {
			$text = new M_Text($field->getHint());
			$html .= ' title="' . $text->toHtmlAttribute() .'"';
		} else {
			$html .= ' title="' . $field->getTitle() . '"';
		}

		$html.= ' value="'. $value .'" />';
		
		if ($field->getHasTextValue()) {
			$html .= '<input type="hidden"' 
			. ' name="'. $field->getId() . '"' 
			. ' id="'. $this->getId() . '"' 
			. ' value="'. $field->getValue() .'" />';
			
			//when a hidden field is used, we need to clear it whenever
			//the autocomplete field is empty on submit
			$javascriptCheck = '$("#'.$field->getId() . $suffix . '").closest("form").submit(function(){
			if($("#'.$field->getId() . $suffix . '").val() == "") $("#'.$field->getId().'").val("");
			});';
			//@interal I don't know why, but when we append the code above to the $javascript string,
			//and add it only once to M_ViewJavascript: the code doesn't get displayed
			M_ViewJavascript::getInstance()->addInline($javascriptCheck);
		}
		//create javascript
		$javascript .= '$("#'. $this->getId() . $suffix . '").autocomplete("'.$field->getAjaxUrl().'", {
       		width: '.$field->getWidth().',
       		max: '.$field->getLimit().',
       		multiple: '.$field->getMultiple().',
       		minChars: '.$field->getMinChars().'})';

		//store value and textvalue in both fields
		if ($field->getHasTextValue()) {
			$javascript .= '.result(function(event, item) {
				 $("#'. $this->getId() . $suffix . '").val(item[0]);
				 $("#'. $this->getId().'").val(item[1]);
				
				// Trigger a CHANGE event on the hidden field, so others can
				// respond to an update of the value in that field:
				$("#'. $this->getId().'").trigger("change");
			})';
		}
		$javascript .= ';';
		
		//add javascript and add context to prevent loading multiple instances
		$js = M_ViewJavascript::getInstance();
		$file = new M_File(
			M_Loader::getRelative( M_Loader::getResourcesPath('admin') . 
			'/javascript/jquery-autocomplete/jquery.autocomplete.js'
		));
		$js->addFile($file, 'form-autocomplete');
		//add javascript for this field
		$js->addInline($javascript, 'form-autocomplete-' . $this->getId() );
		
		//add css
		$css = M_ViewCss::getInstance();
		$file = new M_File(
		    M_Loader::getRelative(M_Loader::getResourcesPath('admin') . 
		    '/css/jquery.autocomplete.css')
		);
		$css->addFile($file, 'form-autocomplete');
		
		return $html;
	}
	
	/**
	 * Get the resource from the module
	 * 
	 * @return M_ViewHtmlResource 
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner(
			'core-form/FieldAutocomplete.tpl'
		);
	}
}