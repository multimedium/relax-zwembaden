<?php
/**
 * M_FieldSelectMultiple class
 * 
 * M_FieldSelectMultiple, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldOptions}
 * - {@link M_FieldOptionsMultiple}
 * 
 * handles the input control that allow users to choose from a
 * list of options. This specific field allows multiple options
 * to be selected from the menu.
 * 
 * @package Core
 */
class M_FieldSelectMultiple extends M_FieldOptionsMultiple {
	/**
	 * The minimal number of items
	 * 
	 * This property holds the minimal number of items that
	 * the user should select from the list of items. If the
	 * user selects less items, a validation error is generated.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_minSelection;


	/**
	 * Width
	 *
	 * @see FieldSelectMultiple::setWidth()
	 * @access private
	 * @var integer
	 */
	private $_width;


	/**
	 * Height
	 *
	 * @see FieldSelectMultiple::setHeight()
	 * @access private
	 * @var integer
	 */
	private $_height;
	
	/**
	 * The maximal number of items
	 * 
	 * This property holds the maximal number of items that
	 * the user can select from the list of items. If the
	 * user selects more items, a validation error is generated.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_maxSelection;

	/**
	 * Get width
	 *
	 * This method is used to get the field width that has been
	 * set previously with {@link M_FieldMultipleSelect::setWidth()}.
	 *
	 * @access public
	 * @return integer
	 */
	public function getWidth() {
		return $this->_width;
	}

	/**
	 * Get height
	 *
	 * This method is used to get the field height that has been
	 * set previously with {@link M_FieldMultipleSelect::setHeight()}.
	 *
	 * @access public
	 * @return integer
	 */
	public function getHeight() {
		return $this->_height;
	}

	/**
	 * Add field definition
	 *
	 * This method is used by {@link M_Field::factory()}, to mount
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 *
	 * <code>menu</code>
	 * <code>items</code>
	 * <code>options</code>
	 *
	 * Compose the list of items that should be contained in the
	 * field. Read {@link M_FieldOptions::setItem()} for more info.
	 *
	 * <code>deliveryFormat</code>
	 *
	 * Set the delivery format of the field (standard array)
	 *
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @return void
	 */
	public function set($spec, $definition) {
		switch($spec) {
			case 'width':
				$this->setWidth($definition);
				break;
			case 'height':
				$this->setHeight($definition);
				break;
			default:
				parent::set($spec, $definition);
				break;
		}
	}

	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return array
	 */
	public function getValue() {
		$value = parent::getValue();
		return (is_array($value) ? $value : array());
	}

	/**
	 * Set width
	 *
	 * This method will set the width of the field. The width is
	 * expressed in a number of pixels.
	 *
	 * @access public
	 * @param integer $pixels
	 * 		The width of the field, in pixels
	 * @return void
	 */
	public function setWidth($pixels) {
		$this->_width = (int) $pixels;
	}

	/**
	 * Set height
	 *
	 * This method will set the height of the field. The height is
	 * expressed in a number of pixels.
	 *
	 * @access public
	 * @param integer $pixels
	 * 		The height of the field, in pixels
	 * @return void
	 */
	public function setHeight($pixels) {
		$this->_height = (int) $pixels;
	}

	/**
	 * Is empty value?
	 * 
	 * This method overrides {@link M_Field::__empty()}.
	 * 
	 * In the case of M_FieldSelectMultiple, this method makes sure 
	 * that an array has been provided as a value, that has been 
	 * populated with at least 1 element.
	 * 
	 * @access public
	 * @param mixed $value
	 * 		The (temporary) value. Read {@link M_Field::getValue()} 
	 * 		to learn more about the field's temporary value.
	 * @return boolean
	 */
	public function __empty($value) {
		if(is_array($value) && count($value) > 0) {
			return FALSE;
		} else {
			$this->setErrorMessage($this->getDefaultErrorMessage());
			return TRUE;
		}
	}
	
	/**
	 * Field-type-specific validation
	 * 
	 * This method overrides {@link M_Field::__validate()}.
	 * 
	 * @access public
	 * @see M_Field::__validate()
	 * @param string $value
	 * 		The temporary value of the field. This value is 
	 * 		being provided by {@link M_FieldDate::getValue()}
	 * @return boolean
	 * 		Returns TRUE is the provided value is of a valid 
	 * 		format, FALSE if not.
	 */
	public function __validate($value) {
		$n = count($value);
		
		$result = false;
		for($i = 0; $i < $n; $i ++) {
			//check if value is found in items
			if(array_key_exists($value[$i], $this->_items)) {
				$result = true;
			} 
			//check if value is found in a group
			else {
				foreach($this->_groups as $group) {
					if(array_key_exists($value[$i], $group[1])) {
						$result = true;
						break;
					}
				}
			}
		}
			
		if($this->isMandatory() && $result == false) {
			$this->setErrorMessage('Please make your selection.');
			return FALSE;
		}
		
		if($this->_minSelection > 0 && $n < $this->_minSelection) {
			$this->setErrorMessage(sprintf('Please select at least %n options', $this->_minSelection));
			return FALSE;
		}
		if($this->_maxSelection > 0 && $n > $this->_maxSelection) {
			$this->setErrorMessage(sprintf('You can only select %n options', $this->_maxSelection));
			return FALSE;
		}
		return TRUE;
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldSelectMultiple} 
	 * object, which allows {@link M_Field} to include the input 
	 * control in the view that is returned by 
	 * {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldSelectMultiple
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldSelectMultiple($this);
		$view->assign('name', $this->getId());
		$view->assign('value', $this->getValue());
		$view->assign('options', $this->getItems());
		return $view;
	}
}
?>