<?php
/**
 * M_FieldUriPathElement class
 * 
 * M_FieldUriPathElement, a subclass of {@link M_Field}, handles the input
 * control that allows users to introduce a (short) text used as path-element.
 * 
 * @package Core
 */
class M_FieldUriPathElement extends M_FieldText {

	/**
	 * Maximum number of words
	 * 
	 * Shorten the given text to a maximum amount of words. Default value is 7
	 * 
	 * @access private
	 * @var integer
	 */
	private $_maximumNumberOfWords = 7;

	/**
	 * Connect this field with another field
	 *
	 * In most case this field will be connected to another field, using an
	 * AJAX call the value of this will be passed to {@link M_Uri::getPathElementFromString()}
	 * which will return a valid url-value. We store the id of the field which
	 * holds the source-value from which we will create a path-element string
	 *
	 * @var string
	 */
	private $_sourceFieldId = false;
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>maximumAmountOfWords</code>
	 * 
	 * Set maximum amount of words in final value. For more information,
	 * read the docs on {@link M_FieldUriPathElement::setMaximumNumberOfWords()}.
	 *
	 * <code>sourceField</code>
	 * <code>sourceFieldId</code>
	 *
	 * Set the field-id which holds the source value from which we will
	 * create a path-element-string
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		switch($spec) {
			case 'maximumAmountOfWords':
				$this->setMaximumAmountOfWords($definition);
				break;
			case 'sourceFieldId':
			case 'sourceField':
				$this->setSourceFieldId($definition);
				break;
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Set maximum amount of words
	 * 
	 * This method will set the maximum number of words that
	 * will be allowed by the text-url field. If more characters are
	 * introduced into the input control, the value will be shortened
	 * 
	 * @access public
	 * @param integer $number
	 * 		The maximum allowed number of words
	 * @return void
	 */
	public function setMaximumAmountOfWords($number) {
		$this->_maximumNumberOfWords = (int)$number;
	}
	
	/**
	 * Get maximum amount of words
	 * 
	 * This method is used to get the maximum amoiunt of words that has been
	 * set previously with {@link M_FieldText::setMaximumAmountOfWords()}.
	 *
	 * Set to FALSE if you don't want the final value to be shortened
	 * 
	 * @access public
	 * @return integer
	 */
	public function getMaximumAmountOfWords() {
		return $this->_maximumNumberOfWords;
	}
	
	/**
	 * Connect this field to another one
	 *
	 * We can connect this field to another field: the value of this source
	 * field will be used as an argument which will be passed to
	 * {@link M_Uri::getPathElementFromString()}. As a result the user doesn't
	 * need to manually enter an url
	 *
	 * @access public
	 * @param string $fieldId
	 * 		The id of the field which will be used as source
	 * @return void
	 */
	public function setSourceFieldId($fieldId) {
		$this->_sourceFieldId = (string)$fieldId;
	}

	/**
	 * Get id of the field which holds the source value from which we will
	 * create a path element string
	 *
	 * @access public
	 * @return string
	 */
	public function getSourceFieldId() {
		return $this->_sourceFieldId;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		if ($this->description == '') {
			$this->description = t("Be careful when editing an existing url: when this page is already indexed by Google, and a user clicks the link in Google, the page can't be displayed anymore. Instead it will be available on the new url.");
		}
		return $this->description;
	}

	/**
	 * Deliver the final value
	 * 
	 * This method overrides {@link M_Field::deliver()}.
	 * 
	 * This method will undo the changes that have been made by 
	 * {@link M_FieldText::getValue()}, in order to return the
	 * original user-supplied value.
	 * 
	 * @access public
	 * @see M_Field::deliver()
	 * @return mixed
	 */
	public function deliver() {
		return M_Uri::getPathElementFromString(
			parent::deliver(),
			$this->_maximumNumberOfWords
		);
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldText} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldText
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldUriPathElement($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}