<?php
/**
 * M_FieldDateString class
 * 
 * M_FieldDate, a subclass of {@link M_Field}, handles the input control that 
 * allows a user to enter a date with a string.
 * 
 * @package Core
 */
class M_FieldDateString extends M_FieldText {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Expected date format
	 * 
	 * This property stores the date format that is used in order to interpret
	 * the provided date string. For more information about date formats, read
	 * the docs on {@link M_Date}.
	 * 
	 * @access private
	 * @var string
	 */
	private $_format;
	
	/**
	 * Default error message for invalid date
	 * 
	 * This property stores the default error message, which is shown if the 
	 * date string cannot be parsed into an instance of {@link M_Date}.
	 * 
	 * @access private
	 * @var string
	 */
	private $_defaultErrorMessageDate;
		
	/**
	 * Default error message for the minimum date
	 * 
	 * This property stores the default error message, which is shown if the 
	 * date string is before a certain minimum date.
	 * 
	 * @access private
	 * @var string
	 */
	private $_defaultErrorMessageMinimumDate;
	
	/**
	 * Default error message for the maximum date
	 * 
	 * This property stores the default error message, which is shown if the 
	 * date string is after a certain maximum date.
	 * 
	 * @access private
	 * @var string
	 */
	private $_defaultErrorMessageMaximumDate;
	
	/**
	 * MinDate
	 *
	 * See {@link FieldDateString::setMinDate()} for more info.
	 *
	 * @var M_Date
	 */
	private $_minDate;

	/**
	 * MaxDate
	 *
	 * See {@link FieldDateString::setMaxDate()} for more info.
	 *
	 * @var M_Date
	 */
	private $_maxDate;
	
	/**
	 * Date
	 * 
	 * The instances of {@link M_Date} that are parsed with the values provided
	 * to the protected {@link M_FieldDateString::_getDate()} method.
	 * 
	 * @see M_FieldDateString::_getDate()
	 * @access private
	 * @var string
	 */
	private $_date = array();
	
	/* -- M_FIELD -- */
	
	/**
	 * Magic method: Validation specific to field type
	 * 
	 * Unlike {@link M_Field::__empty()}, this method is ALWAYS called, regardless 
	 * of whether or not the field has been marked as mandatory. This method is 
	 * intended to check if the user-supplied value is valid (matching the 
	 * expected type and format).
	 * 
	 * @access public
	 * @param mixed $value
	 * 		The (temporary) value to be validated
	 * @return boolean
	 */
	public function __validate($value) {
		// Clean up the value:
		$clean = is_string($value) 
			? trim($value) 
			: $value;
		
		// If the field has not been left empty:
		if($clean) {
			// If the date cannot be parsed:
			if(! $this->_getDate($value)) {
				// Then, we set the default error message for an invalid date:
				$this->setErrorMessage($this->getDefaultErrorMessageInvalidDate());
				
				// We return FALSE, for failure:
				return FALSE;
			}
				
			// If a minimum date is given
			if($this->getMinDate() && $this->_getDate($value)->isBefore($this->getMinDate())) {
				// Then, we set the default error message for a minimum date:
				$this->setErrorMessage($this->getDefaultErrorMessageMinimumDate());
				
				// We return FALSE, for failure:
				return FALSE;
			}
			
			// If a maximum date is given
			if($this->getMaxDate() && $this->_getDate($value)->isAfter($this->getMaxDate())) {
				// Then, we set the default error message for a maximum date:
				$this->setErrorMessage($this->getDefaultErrorMessageMaximumDate());
				
				// We return FALSE, for failure:
				return FALSE;
			}
		}
		
		// If still here, we return TRUE for success:
		return TRUE;
	}
	
	/**
	 * Deliver
	 * 
	 * The delivered value of this class is an instance of {@link M_Date}. Note 
	 * that, if no date has been entered in the field, this method will return
	 * NULL instead.
	 * 
	 * @see M_Field::deliver()
	 * @uses M_FieldDateString::_getDate()
	 * @access public
	 * @return M_Date
	 */
	public function deliver() {
		// Return the parsed instance of M_Date:
		return $this->_getDate($this->getValue());
	}
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, in order to mount the 
	 * field with a given definition. This method overrides {@link M_Field::set()}, 
	 * to make the following (additional) definition keys available:
	 * 
	 * <code>format</code>
	 * <code>dateFormat</code>
	 * 
	 * Allows you to provide with the expected date format. For more information,
	 * read {@link M_FieldDateString::setDateFormat()}.
	 * 
	 * <code>defaultErrorMessageInvalidDate</code>
	 * 
	 * Allows you to provide with the default error message for invalid dates. 
	 * For more info, read {@link M_FieldDateString::setDefaultErrorMessageInvalidDate()}.
	 * 
	 * @access public
	 * @see M_Field::set()
	 * @param string $spec
	 * @param mixed $definition
	 * @param bool $translateStrings
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key provided:
		switch($spec) {
			// The default error message, for invalid date:
			case 'defaultErrorMessageInvalidDate':
				$this->setDefaultErrorMessageInvalidDate(
					$translateStrings 
						? t((string) $definition)
						: (string) $definition
				);
				break;

			// The date format expected:
			case 'dateFormat':
			case 'format':
				$this->setDateFormat((string) $definition);
				break;

			// Other properties:
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set expected date format
	 * 
	 * This method allows to set the date format that is used in order to 
	 * interpret the provided date string. For more information about date formats, 
	 * read the docs on {@link M_Date}.
	 * 
	 * @access public
	 * @param string $format
	 * @return M_FieldDateString $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDateFormat($format) {
		// Set the date format
		$this->_format = (string) $format;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set default error message, for invalid date
	 * 
	 * This method allows to set the default error message that is shown if the 
	 * date string cannot be parsed into an instance of {@link M_Date}.
	 * 
	 * @access public
	 * @param string $message
	 * @return M_FieldDateString $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDefaultErrorMessageInvalidDate($message) {
		// Set the error message
		$this->_defaultErrorMessageDate = (string) $message;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set default error message, for a minimum date
	 * 
	 * This method allows to set the default error message that is shown if the 
	 * date string is before the minimum date.
	 * 
	 * @access public
	 * @param string $message
	 * @return M_FieldDateString $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDefaultErrorMessageMinimumDate($message) {
		// Set the error message
		$this->_defaultErrorMessageMinimumDate = (string) $message;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set default error message, for a maximum date
	 * 
	 * This method allows to set the default error message that is shown if the 
	 * date string is after the maximum date.
	 * 
	 * @access public
	 * @param string $message
	 * @return M_FieldDateString $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDefaultErrorMessageMaximumDate($message) {
		// Set the error message
		$this->_defaultErrorMessageMaximumDate = (string) $message;
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set default value
	 * 
	 * @access public
	 * @param mixed $value
	 * @return void
	 */
	public function setDefaultValue($value = NULL) {
		// If a date has been provided:
		if(! is_null($value)) {
			// If the provided value is an instance of M_Date:
			if(M_Helper::isInstanceOf($value, 'M_Date')) {
				// Set the date, in the format specified
				parent::setDefaultValue($value->toString($this->getDateFormat()));
			}
			// If not:
			else {
				// Then, set the default value, as a string
				parent::setDefaultValue((string) $value);
			}
		}
		// If not
		else {
			// Reset the date with the NULL value
			parent::setDefaultValue(NULL);
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get expected date format
	 * 
	 * Will return the date format that has been set earlier, with the function
	 * {@link M_FieldDateString::setDateFormat()}. Note that, if no date format 
	 * has been specified, this method will return the default format used by
	 * this class: {@link M_Date::SHORT}.
	 * 
	 * @see M_FieldDateString::setDateFormat()
	 * @access public
	 * @return string
	 */
	public function getDateFormat() {
		// Return...
		return (
			// If no format has been specified
			is_null($this->_format)
				// The default:
				? M_Date::SHORT
				// The provided date format, if specified
				: $this->_format
		);
	}

	
	/**
	 * Get default error message, for invalid date
	 * 
	 * Will return the default error message for invalid dates, set previously 
	 * with {@link M_FieldDateString::setDefaultErrorMessageInvalidDate()}. Note 
	 * that, if no error message has been provided to this class, a default error
	 * message will be returned instead.
	 * 
	 * @see M_FieldDateString::setDefaultErrorMessageInvalidDate()
	 * @access public
	 * @return string
	 */
	public function getDefaultErrorMessageInvalidDate() {
		// Return...
		return (
			// If no error message has been specified
			is_null($this->_defaultErrorMessageDate)
				// The default message:
				? t(
					'Please enter a valid date, in the format "@format"', 
					array(
						'@format' => M_Date::getDateFormatWithPlaceholders($this->getDateFormat())
					)
				)
				// The provided error message, if specified
				: $this->_defaultErrorMessageDate
		);
	}
	
	/**
	 * Get default error message, for the minimum date
	 * 
	 * Will return the default error message for minimum dates, set previously 
	 * with {@link M_FieldDateString::setDefaultErrorMessageMinimumDate()}. Note 
	 * that, if no error message has been provided to this class, a default error
	 * message will be returned instead.
	 * 
	 * @see M_FieldDateString::setDefaultErrorMessageMinimumDate()
	 * @access public
	 * @return string
	 */
	public function getDefaultErrorMessageMinimumDate() {
		// Return...
		return (
			// If no error message has been specified
			is_null($this->_defaultErrorMessageMinimumDate)
				// The default message:
				? t(
					'Please enter a valid date, the minimum date is @date', 
					array(
						'@date' => $this->getMinDate()->toString(M_Date::SHORT)
					)
				)
				// The provided error message, if specified
				: $this->_defaultErrorMessageMinimumDate
		);
	}
	
	/**
	 * Get default error message, for the maximum date
	 * 
	 * Will return the default error message for maximum dates, set previously 
	 * with {@link M_FieldDateString::setDefaultErrorMessageMaximumDate()}. Note 
	 * that, if no error message has been provided to this class, a default error
	 * message will be returned instead.
	 * 
	 * @see M_FieldDateString::setDefaultErrorMessageMaximumDate()
	 * @access public
	 * @return string
	 */
	public function getDefaultErrorMessageMaximumDate() {
		// Return...
		return (
			// If no error message has been specified
			is_null($this->_defaultErrorMessageMaximumDate)
				// The default message:
				? t(
					'Please enter a valid date, the maximum date is @date', 
					array(
						'@date' => $this->getMaxDate()->toString(M_Date::SHORT)
					)
				)
				// The provided error message, if specified
				: $this->_defaultErrorMessageMaximumDate
		);
	}
	
	/**
	 * Get hint
	 * 
	 * This method will provide with the hint (placeholder attribute) that is to
	 * be added to the input control, in HTML. Note that, if no hint has been 
	 * provided, this field will render a default input hint with the date format.
	 * 
	 * @uses M_FieldText::getHint()
	 * @uses M_FieldDateString::getDateFormat()
	 * @uses M_Date::getDateFormatWithPlaceholders()
	 * @access public
	 * @return string
	 */
	public function getHint() {
		// Get the hint provided to the field:
		$hint = parent::getHint();
		
		// If provided
		if($hint) {
			// Then, return the input hint as it was provided earlier, to this
			// field:
			return $hint;
		}
		
		// If no input hint is available, we'll render one with the date format:
		return M_Date::getDateFormatWithPlaceholders($this->getDateFormat());
	}

	
	/**
	 * Set a minimum selectable date
	 *
	 * @access public
	 * @param M_Date $minDate
	 */
	public function setMinDate(M_Date $minDate) {
		$this->_minDate = $minDate;
	}

	/**
	 * Get the minimum selectable date
	 *
	 * @access public
	 * @return M_Date $minDate
	 */
	public function getMinDate() {
		return $this->_minDate;
	}

	/**
	 * Set a maximum selectable date
	 *
	 * @access public
	 * @param M_Date $maxDate
	 */
	public function setMaxDate(M_Date $maxDate) {
		$this->_maxDate = $maxDate;
	}

	/**
	 * Get the maximum selectable date
	 *
	 * @access public
	 * @return M_Date $maxDate
	 */
	public function getMaxDate() {
		return $this->_maxDate;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get date, from a given value
	 * 
	 * This method is used internally in order to parse an instance of {@link M_Date}
	 * out of the provided value. Typically, the provided value is the temporary
	 * value that is retrieved with {@link M_Field::getValue()}. For performance, 
	 * this method will cache the date once it has been parsed. This way, we 
	 * avoid the same string value being parsed into a date more than once.
	 * 
	 * NOTE:
	 * If an instance of {@link M_Date} cannot be parsed with the given value,
	 * this method will return NULL instead.
	 * 
	 * @uses M_Date::constructWithString()
	 * @access protected
	 * @param string $value
	 * @return M_Date
	 */
	protected function _getDate($value) {
		// If already a date:
		// (this might be the case if the default value of the field is a date object)
		if(M_Helper::isInstanceOf($value, 'M_Date')) {
			// Then, leave the date unaffected:
			return $value;
		}
		
		// If the provided value is not a string, or a numerical value:
		if(! (is_string($value) || is_numeric($value))) {
			// Then, the argument is of unexpected format. We throw an exception
			// to inform about the error:
			throw new M_Exception(sprintf(
				'Unexpected argument in %s::%s(); Expecting a string value, ' . 
				'but instead received the value %s',
				__CLASS__,
				__FUNCTION__,
				var_export($value, TRUE)
			));
		}
		
		// If not requested before:
		if(! array_key_exists($value, $this->_date)) {
			// We try to...
			try {
				// ...parse an instance of M_Date
				$this->_date[$value] = M_Date::constructWithString(
					// with the temporary value of the field:
					$value,
					// and the format in which the date string is expected to
					// be provided
					$this->getDateFormat()
				);
			}
			// If we fail to do so:
			catch(Exception $e) {
				// Set the internal property to FALSE. This way, we avoid parsing
				// the date more than once:
				$this->_date[$value] = FALSE;
			}
		}
		
		// Return the date:
		return (
			M_Helper::isInstanceOf($this->_date[$value], 'M_Date') 
				? $this->_date[$value]
				: NULL
		);
	}
}