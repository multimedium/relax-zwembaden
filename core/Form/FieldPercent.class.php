<?php
/**
 * M_FieldPercent class
 * 
 * M_FieldPercent, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldText}
 * - {@link M_FieldNumeric}
 * 
 * handles the input control that allows users to introduce a 
 * percent-number.
 * 
 * @package Core
 */
class M_FieldPercent extends M_FieldNumeric {
	
	/**
	 * Autoformat the value of the field
	 *
	 * @var bool
	 */
	private $_autoFormat = true;
	
	/**
	 * Will the field be formatted automatically?
	 * 
	 * @return bool
	 */
	public function isAutoFormat() {
		return $this->_autoFormat;
	}
	
	/**
	 * Should the field be formatted automatically?
	 * 
	 * @param bool $_autoFormat
	 */
	public function setAutoFormat($_autoFormat) {
		$this->_autoFormat = (bool)$_autoFormat;
	}
	
	/**
	 * Constructor
	 * 
	 * M_FieldPercent overrides the {@link M_FieldNumeric} constructor, in
	 * order to override the default field decorator view. It will use
	 * {@link M_Field::setView()} to change the default field decorator.
	 * 
	 * Also, we will automatically add a validator to ensure we receive a
	 * numeric value
	 * 
	 * @access public
	 * @param string $id
	 * 		The field id. This value will be used to set {@link M_Field::$id}
	 * @return M_Field
	 */
	public final function __construct($id) {
		// Construct the field:
		parent::__construct($id);
		
		// Set the default decoration view:
		$this->setView('M_ViewFieldDecoratorPercent');
		
		//validate if this is a numeric value
		$this->addValidatorObject(
			new M_ValidatorIsNumeric(), 
			t('Please provide a valid percent value')
		);
	}
	
	/**
	 * Holds the field in which the percent value can be calculated to a number
	 * value
	 * 
	 * In some cases we need to display the % value in a numeric value (e.g.
	 * display vat % as a number).
	 *
	 * @var M_FieldNumeric
	 */
	protected $_numericField;
	
	/**
	 * Get the {@link M_FieldNumeric} which is used to display the numeric value
	 * of this percent-field
	 * 
	 * @return M_FieldNumeric
	 */
	public function getNumericField() {
		return $this->_numericField;
	}
	
	/**
	 * Set the field which will display the numeric-value for this percent-field
	 * 
	 * @param M_FieldNumeric $_numberField
	 */
	public function setNumericField($_numberField) {
		$this->_numericField = $_numberField;
	}
	
	/**
	 * Set the default value
	 *
	 * @param string $value
	 */
	public function setDefaultValue($value) {
		//format the number
		$number = new M_Number($value);
		
		//set the value
		$this->defaultValue = $number->toPercentString();
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldPercent} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldPercent
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldPercent($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}