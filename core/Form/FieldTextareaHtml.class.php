<?php
/**
 * M_FieldTextareaHtml class
 *
 * M_FieldTextarea, a subclass of
 *
 * - {@link M_Field}
 * - {@link M_FieldText}
 * - {@link M_FieldTextarea}
 *
 * handles the input control that allows users to introduce a
 * correct html snippet.
 *
 * @package Core
 */
class M_FieldTextareaHtml extends M_FieldTextarea {

	/**
	 * Get the temporary value
	 *
	 * This method overrides {@link M_Field::getValue()}.
	 *
	 * M_FieldText will clean up the temporary value, so it does not
	 * break the html attribute of the input control. For example,
	 * if the user introduces a text that holds a simple or double
	 * quote, it may break the input:
	 *
	 * Example 1
	 * <code>
	 *    <input type="text" value="Text with " breaks the html tag">
	 * </code>
	 *
	 * This method will solve this problem by cleaning up the
	 * temporary value. The value in Example 1 will be converted to
	 * the following:
	 *
	 * <code>
	 *    <input type="text" value="Text with &quot; breaks the html tag">
	 * </code>
	 *
	 * @access public
	 * @see M_Field::getValue()
	 * @return array|boolean
	 */
	public function getValue() {
		if($this->_sanitize) {
			$value = M_Request::getVariable($this->id, FALSE, M_Request::TYPE_STRING_SANITIZED);
		} else {
			$value = M_Request::getVariable($this->id, FALSE);
		}

		if($value === FALSE) {
			return $this->getDefaultValue();
		} else {
			return $value;
		}
	}

	/**
	 * Deliver the final value
	 *
	 * The abstract M_Field class provides with a default
	 * implementation of deliver(), which simply delivers the
	 * temporary value as the final value.
	 *
	 * IMPORTANT NOTE:
	 * Some field types may have interest in not delivering
	 * any final value at all. Such fields should override this
	 * method, so it returns (boolean) FALSE. If this method
	 * returns FALSE, no value will be delivered to M_Form.
	 *
	 * IMPORTANT NOTE:
	 * If this method returns an object that implements the
	 * {@link MI_Filter} interface, {@link M_Field::applyFilters()}
	 * will convert it to the result of this filter (by calling
	 * the apply() function).
	 *
	 * @access public
	 * @return mixed
	 */
	public function deliver() {
		return $this->getValue();
	}
	
		/**
	 * Magic method: Is empty value?
	 * 
	 * This method checks whether or not the field's current value
	 * is considered to be "empty". The implementation of abstract
	 * M_Field checks for a non-empty string (it will first strip all
	 * HTML from the test subject).
	 * 
	 * M_Field provides with a default __empty() check. Subclasses
	 * can override this method to fit their characteristics and meet the
	 * requirements that are specific to the field type. For example, the 
	 * class {@link M_FieldUpload} will override this method, to check
	 * whether or not a file has been uploaded with the POST method.
	 * 
	 * If the value is considered to be empty, this method will return 
	 * (boolean) TRUE. If not considered empty, this method will return
	 * (boolean) FALSE.
	 * 
	 * NOTE:
	 * This method will be called by {@link M_Field::validate()}, only
	 * if the field has been marked as mandatory. To mark a field as
	 * mandatory, use {@link M_Field::setMandatory()}.
	 * 
	 * @access public
	 * @param mixed $value
	 * 		The (temporary) value. Read {@link M_Field::getValue()} to
	 * 		learn more about a field's temporary value.
	 * @return boolean
	 */
	public function __empty($value) {
		//strip and trim only when value is string
		if (is_string($value)) $value = trim($value);
		
		if($value == "") {
			$this->setErrorMessage($this->getDefaultErrorMessage());
			return TRUE;
		}
		return FALSE;
	}
	
}