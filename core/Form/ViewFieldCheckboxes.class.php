<?php
class M_ViewFieldCheckboxes extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		$fieldValue = $field->getValue();
		
		$html  = '<div id="'. $this->getId() .'">';
		
		//render Groups
		foreach($field->getGroups() AS $group) {
			$html .= '<div class="note">' . $group[0] . '</div>';
			$html .= '<div class="field-group-checkboxes">';
			foreach( $group[1] AS $value => $label) {
				$html .= $this->_getCheckbox($value, $label, $field, $fieldValue);
			}
			$html .= '</div>';
		}
		
		foreach($field->getItems() as $value => $label) {
				$html .= $this->_getCheckbox($value, $label, $field, $fieldValue);
		}
		
		$html .= '</div>';
		return $html;
	}
	
	protected function _getCheckbox($value, $label, $field, $fieldValue) {
			$html = '<div class="field-checkboxes-item">';
			$html .=    '<input class="field-checkbox" type="checkbox" name="'. $field->getId() .'[]" value="'. $value .'" id="'. $this->getId() .'-'. M_Helper::getCamelCasedString((string)$value) .'"';
			if(in_array($value, $fieldValue)) {
				$html .= ' checked="checked"';
			}
			if($field->getDisabled()) {
				$html .= ' disabled="disabled"';
			}
			
			$html .=    ' />';
			$html .=    '<label class="checkbox" for="'. $this->getId() .'-'. M_Helper::getCamelCasedString((string)$value) .'">'. $label .'</label>';
			$html .= '</div>';
			
			return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldCheckboxes.tpl');
	}
}