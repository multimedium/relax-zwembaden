<?php
class M_ViewFieldPassword extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		$title = ($field->getHint()) ? 'title="'.$field->getHint().'"' : '';
		return '<input type="password"'
			. ' name="'. $field->getId() .'"'
			. ' id="'. $this->getId() .'"'
			. $title
			. ' class="field-password'. ($field->isShowingStrength() ? ' field-password-strength' : '') .'"'
			. ' value="' . $field->getDefaultValue() . '" />';
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldPassword.tpl');
	}
}
?>