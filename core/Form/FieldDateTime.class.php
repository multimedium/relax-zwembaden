<?php
/**
 * M_FieldDateTime class
 * 
 * M_FieldDateTime, a subclass of {@link M_Field}, handles the input
 * control that allow users to pick a date from a pop-up calendar 
 * interface and choose a time (hour/minutes).
 * 
 * @package Core
 */
class M_FieldDateTime extends M_Field {
	/**
	 * The field which holds the date value
	 *
	 * @var M_FieldDate
	 */
	protected $_fieldDate;

	/**
	 * The field which holds the hour value
	 *
	 * @var M_FieldSelect
	 */
	protected $_fieldHours;

	/**
	 * The field which holds the minutes value
	 *
	 * @var M_FieldSelect
	 */
	protected $_fieldMinutes;
	
	/**
	 * Holds the minute interval for the minutes field, specified through
	 * {@link M_FieldDateTime::setMinuteInterval()}
	 * 
	 * @access protected
	 * @var int
	 */
	protected $_minuteInterval;
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>deliveryFormat</code>
	 * <code>delivery</code>
	 * 
	 * Set the delivery format of the date. Check the documentation 
	 * on {@link M_FieldDate::setDeliveryFormat()}, to learn more 
	 * about date format delivery.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// Minute interval
			case 'minuteInterval':
				$this->setMinuteInterval($definition);
				break;

			// Other properties:
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Set Mandatory
	 * 
	 * We overwrite the default, because we need to set the mandatory flag
	 * of the contained fields as well
	 * 
	 * @access public
	 * @param type $flag
	 * @return void
	 */
	public function setMandatory($flag) {
		parent::setMandatory($flag);
		$this->getFieldDate()->setMandatory($flag);
		$this->getFieldHours()->setMandatory($flag);
		$this->getFieldMinutes()->setMandatory($flag);
	}
	
	/**
	 * Set the default value
	 * 
	 * @param M_Date $date
	 * @return void
	 */
	public function setDefaultValue(M_Date $date = null) {
		//don't do a thing if null
		if(!$date) return;
		
		$this->getFieldDate()->setDefaultValue($date);
		$this->getFieldHours()->setDefaultValue($date->getHour());
		$this->getFieldMinutes()->setDefaultValue($date->getMinutes());
	}

	/**
	 * Get the date field
	 * 
	 * @return M_FieldDate
	 */
	public function getFieldDate() {
		if (is_null($this->_fieldDate)) {
			$this->_fieldDate = new M_FieldDate($this->getId().'-date');
			$this->_fieldDate->setMandatory($this->mandatory);
		}
		return $this->_fieldDate;
	}

	/**
	 * Set the date field
	 *
	 * @param M_FieldDate $field
	 * @return M_FieldDateTime
	 */
	public function setFieldDate(M_FieldDate $field) {
		$this->_fieldDate = $field;
		return $this;
	}

	/**
	 * Get the hour field
	 * 
	 * @return M_FieldSelect
	 */
	public function getFieldHours() {
		if (is_null($this->_fieldHours)) {
			$this->_fieldHours = new M_FieldSelect($this->getId().'-hours');
			for($i = 0 ; $i < 24 ; $i++) {
				$this->_fieldHours->setItem($i, str_pad($i, 2, '0', STR_PAD_LEFT));
			}
		}
		return $this->_fieldHours;
	}

	/**
	 * Set the hours field
	 *
	 * @param M_Field $field
	 * @return M_FieldDateTime
	 */
	public function setFieldHours(M_Field $field) {
		$this->_fieldHours = $field;
		return $this;
	}

	/**
	 * Get the minutes field
	 *
	 * @return M_FieldMinutes
	 */
	public function getFieldMinutes() {
		if (is_null($this->_fieldMinutes)) {
			$this->_fieldMinutes = new M_FieldSelect($this->getId().'-minutes');
			
			// Fetch the minute interval
			$interval = $this->getMinuteInterval();
			for($i = 0 ; $i < 60 ; $i++) {
				
				// Add only if the interval is matched
				if($i % $interval == 0) {
					$this->_fieldMinutes->setItem($i, str_pad($i, 2, '0', STR_PAD_LEFT));
				}
			}
		}
		return $this->_fieldMinutes;
	}

	/**
	 * Set the minutes field
	 * 
	 * @param M_Field $field
	 * @return M_FieldDateTime
	 */
	public function setFieldMinutes(M_Field $field) {
		$this->_fieldMinutes = $field;
		return $this;
	}
	
	/**
	 * Set Minute Interval
	 * 
	 * Allows for specifying a minute interval for the minutes field. For
	 * example, if you set the interval to 15, the minutes select box will
	 * contain the following values:
	 * 
	 * <code>
	 *		array(0, 15, 30, 45);
	 * </code>
	 * 
	 * @access public
	 * @param int $interval
	 *		The requested interval, specified in minutes
	 * @return M_FieldDateTime
	 *		Returns itself, for a fluent programming interface
	 */
	public function setMinuteInterval($interval) {
		$this->_minuteInterval = (int) $interval;
		return $this;
	}
	
	/**
	 * Get Minute Interval
	 * 
	 * Will return the set interval, if any. If no interval is specified
	 * through {@link M_FieldDateTime::setMinuteInterval}, the default values
	 * from 0-60 will be used.
	 * 
	 * @access public
	 * @return int
	 */
	public function getMinuteInterval() {
		return isset($this->_minuteInterval) ? $this->_minuteInterval : 1;
	}

	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return string
	 */
	public function getValue() {
		return $this->getFieldDate()->getValue().', '.$this->getFieldHours()->getValue().':'.$this->getFieldMinutes()->getValue();
	}

	/**
	 * Deliver the final value
	 * 
	 * This method overrides {@link M_Field::deliver()}.
	 * 
	 * M_FieldDate will deliver the final value in the format
	 * previously provided to {@link M_Field::setDeliveryFormat()}.
	 * 
	 * @access public
	 * @see M_Field::deliver()
	 * @return M_Date
	 */
	public function deliver() {
		$date = $this->getFieldDate()->deliver();
		if(!$date) return null;
		$date->setHour($this->getFieldHours()->deliver());
		$date->setMinutes($this->getFieldMinutes()->deliver());
		return $date;
	}

	/**
	 * Set the module owner for this field and all nested fields too
	 *
	 * M_FieldDateTime is a field which holds other fields: date, hours, minutes, ... We need to set the module-owner for all of these fields
	 *
	 * @param string $moduleOwner
	 */
	public function setModuleOwner($moduleOwner) {
		parent::setModuleOwner($moduleOwner);

		$this->getFieldDate()->setModuleOwner($moduleOwner);
		$this->getFieldHours()->setModuleOwner($moduleOwner);
		$this->getFieldMinutes()->setModuleOwner($moduleOwner);
	}

	/**
	 * Validate the date-time field
	 *
	 * @return bool
	 */
	public function __validate() {
		$result = true;

		//invalid streetname or number
		if($this->getFieldDate()->validate() == false) {
			$this->setErrorMessage(
				$this->_defaultErrorMessage
					? $this->_defaultErrorMessage
					: t('Please provide a valid date')
			);
			$result = false;
		}

		return $result;
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldDateTime} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldDate
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldDateTime($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}