<?php
/**
 * M_FieldColor class
 * 
 * M_FieldColor, a subclass of {@link M_Field}, handles the input 
 * control that allow users to introduce a color. Typically, this
 * field generates a color palette interface.
 * 
 * NOTE:
 * To work with colors, check the {@link M_Color} class.
 * 
 * @package Core
 */
class M_FieldColor extends M_Field {

	/* -- PROPERTIES -- */

	/**
	 * Allowed colors
	 *
	 * This property stores the collection of colors that is allowed by this field.
	 *
	 * @see M_FieldColor::setAllowedColors()
	 * @access private
	 * @var array $colors
	 *		The collection of allowed colors; each of them represented by an
	 *		instance of {@link M_Color}
	 */
	private $_colors = array();

	/* -- SETTERS -- */

	/**
	 * Add field definition
	 *
	 * This method is used by {@link M_Field::factory()}, to mount the field with
	 * a given definition. This method overrides {@link M_Field::set()}, to make
	 * the following (additional) definition keys available:
	 *
	 * <code>colors | allowedColors</code>
	 *
	 * Compose the list of colors that should be allowed in the field. Read
	 * {@link M_FieldColor::setAllowedColors()} for more info.
	 *
	 * Example 1
	 * <code>
	 *    $field = M_Field::factory('myColor', new Config(array(
	 *       'type'        => 'color',
	 *       'title'       => 'Choose a color',
	 *       'mandatory'   => TRUE,
	 *       'colors'      => 'ffffff,000000,f3f3f3,cc0000'
	 *    )));
	 * </code>
	 *
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// Allowed colors
			case 'colors':
			case 'allowedColors':
				$this->setAllowedColors(preg_split('/[\-,;\s]+/', (string) $definition));
				break;

			// Default value
			case 'default':
			case 'defaultValue':
				$this->setDefaultValue(new M_Color((string) $definition));
				break;

			// Other properties
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}

	/**
	 * Set default value
	 *
	 * @access public
	 * @param M_Color $color
	 * @return void
	 */
	public function setDefaultValue($color) {
		// If not an instance of color:
		if(! M_Helper::isInstanceOf($color, 'M_Color')) {
			// Then, convert now:
			$color = new M_Color($color);
		}

		// Set default value
		parent::setDefaultValue($color);
	}

	/**
	 * Set allowed colors
	 *
	 * This method can be used to set the collection of allowed colors. If such
	 * a collection is set, then the field should only allow one of the predefined
	 * colors to be introduced.
	 *
	 * @uses M_FieldColor::addAllowedColor()
	 * @access public
	 * @param M_ArrayIterator|array $colors
	 *		The collection of allowed colors. Each of the colors should be represented
	 *		by an array of RGB values, by a hex string, or by an instance of the
	 *		{@link M_Color} class
	 * @return M_FieldColor $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAllowedColors($colors) {
		// We expect an iterator as an argument to this method
		if(! M_Helper::isIterator($colors)) {
			// If that's not the case, we throw an exception to inform about
			// the error
			throw new M_Exception(sprintf(
				'Cannot set allowed colors with variable %s; expecting a collection ' .
				'(array or iterator object) of colors',
				var_export($colors, TRUE)
			));
		}

		// For each of the provided colors:
		foreach($colors as $color) {
			// The color can be represented by an array of RGB values, or by an
			// instance of M_Color. So, we check if an instance of M_Color has
			// already been provided:
			if(M_Helper::isInstanceOf($color, 'M_Color')) {
				// If so, then we can add the color right away :)
				$this->addAllowedColor($color);
			}
			// If the current color is not an instance of M_Color
			else {
				// Then, the current color must not be an object! We will be
				// expecting an array or a string. So, if the current color is
				// an object:
				if(is_object($color)) {
					// We throw an exception, to inform about the error
					throw new M_Exception(sprintf(
						'Cannot add a color to the field with variable %s; expecting ' .
						'an array with RGB values, or a hex string, but not an object ' .
						'of %s',
						var_export($color, TRUE),
						get_class($color)
					));
				}

				// Construct the color, and add it to the field:
				$this->addAllowedColor(new M_Color($color));
			}
		}

		// Return myself:
		return $this;
	}

	/**
	 * Add allowed color
	 *
	 * @access public
	 * @param M_Color $color
	 * @return M_FieldColor $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function addAllowedColor(M_Color $color) {
		// Add the color:
		$this->_colors[$color->getHex(FALSE)] = $color;

		// Return myself:
		return $this;
	}

	/* -- GETTERS -- */

	/**
	 * Has range of allowed colors?
	 *
	 * Will tell whether or not the field allows only a range of colors. For more
	 * info, read
	 *
	 * - {@link M_FieldColor::setAllowedColors()}
	 * - {@link M_FieldColor::addAllowedColor()}
	 *
	 * If all colors are allowed, then this method will FALSE. If a range of
	 * allowed colors has been defined, then this method will return TRUE instead.
	 *
	 * @access public
	 * @return bool $flag
	 */
	public function hasRangeOfAllowedColors() {
		return (count($this->_colors) > 0);
	}

	/**
	 * Get allowed colors
	 *
	 * @see M_FieldColor::addAllowedColor()
	 * @see M_FieldColor::setAllowedColors()
	 * @access public
	 * @return M_ArrayIterator $colors
	 *		The collection of allowed colors. Each of the colors is represented
	 *		by an instance of the {@link M_Color} class
	 */
	public function getAllowedColors() {
		return new M_ArrayIterator($this->_colors);
	}

	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldColor} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldColor
	 * @return M_ViewFieldColor
	 */
	public function getInputView() {
		$view = new M_ViewFieldColor($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}

	/**
	 * Get temporary value
	 *
	 * @access public
	 * @return M_Color
	 */
	public function getValue() {
		$value = M_Request::getVariable($this->getId(), FALSE);
		if($value === FALSE) {
			return $this->getDefaultValue();
		} else {
			try {
				return new M_Color((string) $value);
			}
			// If we caught an error, while constructing the color:
			catch(Exception $e) {
				return new M_Color('#000000');
			}
		}
	}

	/**
	 * Deliver
	 *
	 * @access public
	 * @return M_Color
	 */
	public function deliver() {
		return $this->getValue()->getHex();
	}
}