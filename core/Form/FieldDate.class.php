<?php
/**
 * M_FieldDate class
 * 
 * M_FieldDate, a subclass of {@link M_Field}, handles the input 
 * control that allow users to pick a date from a pop-up calendar 
 * interface.
 * 
 * @package Core
 */
class M_FieldDate extends M_Field {
	/**
	 * The delivery format of the date
	 * 
	 * @access private
	 * @see M_FieldDate::setDeliveryFormat()
	 * @var string
	 */
	private $_deliveryFormat;
	
	/**
	 * The display format of the date
	 * 
	 * @access private
	 * @see M_FieldDate::setDisplayFormat()
	 * @var string
	 */
	private $_displayFormat = M_Date::MEDIUM;
	
	/**
	 * The javascript which will be executed when a date has been selected
	 * 
	 * @var string
	 * @see M_FieldDate::setOnSelect()
	 * @see M_FieldDate::getOnSelect()
	 */
	private $_onSelect;
	
	/**
	 * The javascript which will be executed when the datePicker closes
	 * 
	 * @var string
	 * @see M_FieldDate::setOnClose()
	 * @see M_FieldDate::getOnClose()
	 */
	private $_onClose;

	/**
	 * Hint
	 *
	 * See {@link FieldDate::setHint()} for more info.
	 *
	 * @var string
	 */
	private $_hint;

	/**
	 * MinDate
	 *
	 * See {@link FieldDate::setMinDate()} for more info.
	 *
	 * @var M_Date
	 */
	private $_minDate;

	/**
	 * MaxDate
	 *
	 * See {@link FieldDate::setMaxDate()} for more info.
	 *
	 * @var M_Date
	 */
	private $_maxDate;

	/**
	 * Weekends
	 *
	 * See {@link FieldDate::setWeekends()} for more info.
	 *
	 * @var bool
	 */
	private $_weekends = true;

	/**
	 * Disabled dates
	 *
	 * See {@link FieldDate::setDisabledDates()} for more info.
	 *
	 * @access private
	 * @var M_ArrayIterator
	 */
	private $_disabledDates;
	
	/**
	 * Enabled dates
	 *
	 * See {@link FieldDate::setEnabledDates()} for more info.
	 *
	 * @access private
	 * @var M_ArrayIterator
	 */
	private $_enabledDates;
	
	/**
	 * Year Range
	 *
	 * See {@link FieldDate::setYearRange()} for more info.
	 *
	 * @access private
	 * @var string
	 */
	private $_yearRange;

	/**
	 * Width
	 *
	 * See {@link FieldText::setWidth()} for more info. This property
	 * is defaulted to 200.
	 *
	 * @access private
	 * @var integer
	 */
	private $_width;
	

	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>deliveryFormat</code>
	 * <code>delivery</code>
	 * 
	 * Set the delivery format of the date. Check the documentation 
	 * on {@link M_FieldDate::setDeliveryFormat()}, to learn more 
	 * about date format delivery.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// The format in which the date should be delivered to the form,
			// when user interaction with the field has finalized
			case 'deliveryFormat':
			case 'delivery':
				$this->setDeliveryFormat($definition);
				break;

			// Event listener
			case 'onClose':
				$this->setOnClose($definition);
				break;

			// Event listener
			case 'onSelect':
				$this->setOnSelect($definition);
				break;

			// Disable weekends?
			case 'weekends':
				$this->setWeekends(M_Helper::isBooleanTrue($spec));
				break;

			// The width (in pixels) of the field
			case 'width':
				$this->setWidth($definition);
				break;
			
			// The year range, to be specified with a colon (e.g. 1900:2015)
			// Read the docs on FieldDate::setYearRange() for more info
			case 'yearRange':
				$year = explode(':', $definition);
				if(count($year) != 2) {
					throw new M_Exception(sprintf(
						'Invalid definition for year range: %s; please provide the year range as a dash separated string (e.g. 1900:2015)',
						$definition
					));
				}
				$this->setYearRange($year[0], $year[1]);
				break;

			// Other properties:
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}

	/**
	 * Set width
	 *
	 * This method will set the width of the field. The width is
	 * expressed in a number of pixels. Check the docs on property
	 * {@link FieldText::$_width} for more info about defaults.
	 *
	 * @access public
	 * @param integer $pixels
	 * 		The width of the field, in pixels
	 * @return void
	 */
	public function setWidth($pixels) {
		$this->_width = $pixels;
	}

	/**
	 * Set the delivery format of the date
	 * 
	 * This method expects the same format string you would 
	 * pass in to a date() call. Consult PHP's documentation 
	 * at the following URL:
	 * 
	 * http://be.php.net/manual/en/function.date.php
	 * 
	 * Example 1, deliver a UNIX timestamp
	 * <code>
	 *    $field = new M_FieldDate('myDate');
	 *    $field->setDeliveryFormat('U');
	 * </code>
	 * 
	 * Example 2, deliver a string like YYYY-MM-DD
	 * <code>
	 *    $field = new M_FieldDate('myDate');
	 *    $field->setDeliveryFormat('Y-m-d');
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * If no specific delivery format string has been provided 
	 * to M_FieldDate, it will deliver an {@link M_Date} object 
	 * that represents the date that has been introduced by the 
	 * user.
	 * 
	 * @access public
	 * @see http://be.php.net/manual/en/function.date.php
	 * @see M_FieldDate::deliver()
	 * @param string $format
	 * 		The date format string
	 * @return void
	 */
	public function setDeliveryFormat($format) {
		$this->_deliveryFormat = $format;
	}

	/**
	 * Set the display format of the date
	 * 
	 * This method expects the same format string you would 
	 * pass in to a date() call. Consult PHP's documentation 
	 * at the following URL:
	 * 
	 * http://be.php.net/manual/en/function.date.php
	 * 
	 * Example 1, deliver a UNIX timestamp
	 * <code>
	 *    $field = new M_FieldDate('myDate');
	 *    $field->setDeliveryFormat('U');
	 * </code>
	 * 
	 * Example 2, deliver a string like YYYY-MM-DD
	 * <code>
	 *    $field = new M_FieldDate('myDate');
	 *    $field->setDeliveryFormat('Y-m-d');
	 * </code>
	 * 
	 * @access public
	 * @see http://be.php.net/manual/en/function.date.php
	 * @see M_FieldDate::deliver()
	 * @param string $format
	 * 		The date format string
	 * @return void
	 */
	public function setDisplayFormat($format) {
		$this->_displayFormat = $format;
	}
	
	/**
	 * Get the display format
	 * 
	 * @see M_FieldDate::setDisplayFormat()
	 * @return string $format
	 */
	public function getDisplayFormat() {
		return $this->_displayFormat;
	}
	
	/**
	 * Set the javascript which will be executed on the onClose event
	 * 
	 * This method expects a valid javascript function which will be executed
	 * right after the datePicker is closed.
	 * 
	 * Example 1, trigger alert
	 * 
	 * <code>
	 * 	$field = new M_FieldDate('myDate');
	 * 	$field->setOnClose("function(dateText, inst) { alert("Datepicker closed"); }");
	 * </code>
	 * 
	 * @param string $javascript
	 * @author Ben Brughmans
	 * @return void
	 */
	public function setOnClose($javascript) {
		$this->_onClose = $javascript;
	}
	
	/**
	 * Get the javascript which will be executed when the datePicker is closed
	 * 
	 * @see setOnClose
	 * @return string
	 * @author Ben Brughmans
	 */
	public function getOnClose() {
		return $this->_onClose;
	}
	
	/**
	 * Set the javascript which will be executed on the onSelect event
	 * 
	 * This method expects a valid javascript function which will be executed
	 * right after the a date has been selected in the datePicker.
	 * 
	 * Example 1, trigger alert
	 * 
	 * <code>
	 * 	$field = new M_FieldDate('myDate');
	 * 	$field->setOnSelect("function(dateText, inst) { alert("Datepicker closed"); }");
	 * </code>
	 * 
	 * @param string $javascript
	 * @author Ben Brughmans
	 * @return void
	 */
	public function setOnSelect($javascript) {
		$this->_onSelect = $javascript;
	}
	
	/**
	 * Get the javascript which will be executed when a date is selected
	 * 
	 * @see setOnSelect
	 * @return string
	 * @author Ben Brughmans
	 */
	public function getOnSelect() {
		return $this->_onSelect;
	}

	/**
	 * Set hint
	 *
	 * This method will set the hint of the field.
	 * Check the docs on property {@link M_FieldText::$_hint} for more info.
	 *
	 * A hint will internally be stored as a title property. However, by adding
	 * javascript the hint will be displayed as a default-value. Whenever a
	 * user clicks inside the field, or submits, this value will be removed
	 *
	 * @access public
	 * @param string $hint
	 * @return void
	 */
	public function setHint($hint) {
		$this->_hint = $hint;
	}

	/**
	 * Get hint
	 *
	 * This method is used to get the hint that has been
	 * set previously with {@link M_FieldText::getHint()}.
	 *
	 * @access public
	 * @return string
	 */
	public function getHint() {
		//add javascript library to display hints
		if (!is_null($this->_hint)) {
			$js = M_ViewJavascript::getInstance();

			$file = new M_File(M_Loader::getResourcesPath('admin').'/javascript/jquery.input-hint.js');
			$js->addFile($file, 'form-hint');

			$js->addInline("$('#".$this->getId()."DateButton').inputHint({})", "text-hint-".$this->getId());
		}

		return htmlentities($this->_hint);
	}

	/**
	 * Set a minimum selectable date
	 *
	 * @access public
	 * @param M_Date $minDate
	 */
	public function setMinDate(M_Date $minDate) {
		$this->_minDate = $minDate;
	}

	/**
	 * Get the minimum selectable date
	 *
	 * @access public
	 * @return M_Date $minDate
	 */
	public function getMinDate() {
		return $this->_minDate;
	}

	/**
	 * Set a maximum selectable date
	 *
	 * @access public
	 * @param M_Date $maxDate
	 */
	public function setMaxDate(M_Date $maxDate) {
		$this->_maxDate = $maxDate;
	}

	/**
	 * Get the maximum selectable date
	 *
	 * @access public
	 * @return M_Date $maxDate
	 */
	public function getMaxDate() {
		return $this->_maxDate;
	}

	/**
	 * Enable or disable weekends
	 *
	 * @param bool $weekends
	 *		Set weekends true or false
	 */
	public function setWeekends($weekends) {
		$this->_weekends = $weekends;
	}

	/**
	 * Get the weekends status
	 *		Are weekends ENABLED or DISABLED?
	 *
	 * @return bool $weekends
	 */
	public function getWeekends() {
		return $this->_weekends;
	}

	/**
	 * Set year range
	 *
	 * Allows for setting the year range the dropdown op top of the datepicker
	 * will feature. This can be formatted in a 3 different ways:
	 *
	 * - Relative to the current year, for example:
	 * <code>
	 *		$fieldDate->setYearRange('-25', '+10');
	 * </code>
	 * This will render the dropdown boxes to show 25 years predating the
	 * current year, and 10 years after.
	 *
	 * - Absolute, for example:
	 * <code>
	 *		$fieldDate->setYearRange('1901', '2010');
	 * </code>
	 * This will only show years 1901 - 2010.
	 *
	 * - Mixed, relative and absolute. For example:
	 * <code>
	 *		$fieldDate->setYearRange('1901', '+10');
	 * </code>
	 * This will render all years between 1901 and the current year +10 years.
	 *
	 * IMPORTANT NOTE: as of currently, the use of yearRange with jQuery UI is
	 * discouraged, as it has a bug with the default selected year not
	 * properly adapting when selecting a date.
	 *
	 * NOTE: this function only influences what years are shown in the dropdown
	 * list at the top of the datepicker. To restrict which dates can or can
	 * not be selected, use the following functions:
	 * - {@see M_FieldDate::setDisabledDates()}
	 * - {@see M_FieldDate::setMinDate()}
	 * - {@see M_FieldDate::setMaxDate()}
	 *
	 * For more info on date restrictions, check the docs at
	 * {@link http://jqueryui.com/demos/datepicker/#date-range}
	 *
	 * @access public
	 * @param $firstYear
	 *		The first year that is to be shown on the dropdown
	 * @param $lastYear
	 *		The last year that is to be shown on the dropdown
	 * @return void
	 */
	public function setYearRange($firstYear, $lastYear) {
		$this->_yearRange = (string) $firstYear . ':' . (string) $lastYear;
	}

	/**
	 * Get the year range
	 *
	 * @access public
	 * @return string $yearRange
	 */
	public function getYearRange() {
		return $this->_yearRange;
	}

	/**
	 * Set disabled dates
	 *
	 * Allows for setting a collection of dates that are to be disabled
	 * in the datepicker
	 *
	 * @access public
	 * @param M_ArrayIterator $dates
	 *		The collection of dates that is to be disabled
	 */
	public function setDisabledDates(M_ArrayIterator $dates) {
		if(!is_null($this->_enabledDates)) {
			throw new M_FormException(
				'You cannot set disabled dates when you already have set enabled dates'
			);
		}
		$this->_disabledDates = $dates;
	}

	/**
	 * Set enable dates
	 *
	 * Allows for setting a collection of dates which only be available in the
	 * datepicker
	 *
	 * @param M_ArrayIterator $dates
	 * @author Ben Brughmans
	 */
	public function setEnabledDates(M_ArrayIterator $dates) {
		$this->_enabledDates = $dates;
	}

	/**
	 * Get disabled dates
	 *
	 * Are there dates that have to be disabled in the datepicker?
	 *
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getDisabledDates() {
		return $this->_disabledDates;
	}

	/**
	 * Get enabled dates
	 *
	 * Should we disable alle dates, except this ones in the datepicker?
	 *
	 * @return M_ArrayIterator
	 * @author Ben Brughmans
	 */
	public function getEnabledDates() {
		return $this->_enabledDates;
	}
	
	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * M_FieldDate sends the date in the following format:
	 * <code>Y-m-d</code>
	 * 
	 * Nonetheless, M_FieldDate will provide with an (associative) 
	 * array as its temporary value. This array will have values 
	 * for the following keys:
	 * <code>
	 *    Array (
	 *       [day]       (integer)
	 *       [month]     (integer)
	 *       [year]      (integer)
	 *    )
	 * </code>
	 * 
	 * By doing so, it makes the date elements more accessible to
	 * the {@link M_View} class that renders the date input control 
	 * view ({@link M_FieldDate::getInputView()}). Also, it enables
	 * M_FieldDate to more efficiently validate the provided date
	 * ({@link M_FieldDate::isValidValue()}.
	 * 
	 * IMPORTANT NOTE:
	 * If the date has not been sent in the format that is expected
	 * by M_FieldDate, this method will default the value to the current
	 * date and time. M_FieldDate expects a string, describing the date 
	 * in the following format:
	 * 
	 * <code>Y-m-d</code>
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return array|boolean
	 */
	public function getValue() {
		// get temporary value:
		$value = parent::getValue();
		
		// check if value is null
		if (is_null($value)) return FALSE;
		
		// extract date elements (if string):
		$match = array();
		if(preg_match('/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$/', $value, $match)) {
			return array(
				'day'    => $match[3],
				'month'  => $match[2],
				'year'   => $match[1]
			);
		}
		// Check for UNIX timestamp?
		elseif(is_numeric($value)) {
			$tmp = new M_Date((int) $value);
			return array(
				'day'    => $tmp->day,
				'month'  => $tmp->month,
				'year'   => $tmp->year
			);
		}
		// Check for M_Date
		elseif ($value instanceof M_Date) {
			/* @var $value M_Date */
			return array(
				'day' => $value->getDay(),
				'month' => $value->getMonth(),
				'year' => $value->getYear()
			);
		}
		
		// Cannot extract date:
		else {
			return FALSE;
		}
	}

	/**
	 * Get width
	 *
	 * This method is used to get the field width that has been
	 * set previously with {@link M_FieldText::setWidth()}.
	 *
	 * @access public
	 * @return integer
	 */
	public function getWidth() {
		return $this->_width;
	}
	
	/**
	 * Deliver the final value
	 * 
	 * This method overrides {@link M_Field::deliver()}.
	 * 
	 * M_FieldDate will deliver the final value in the format
	 * previously provided to {@link M_Field::setDeliveryFormat()}.
	 * 
	 * @access public
	 * @see M_Field::deliver()
	 * @return mixed
	 */
	public function deliver() {
		// get the temporary value:
		$value = $this->getValue();
		if($value) {
			// create timestamp:
			$time = mktime(0, 0, 0, $value['month'], $value['day'], $value['year']);
			
			// deliver the date, in the specified delivery format:
			if($this->_deliveryFormat) {
				return date($this->_deliveryFormat, $time);
			} else {
				return new M_Date($time);
			}
		}
		
		// deliver empty value, if temporary value has not been 
		// provided!
		return null;
	}
	
	/**
	 * Field-type-specific validation
	 * 
	 * This method overrides {@link M_Field::isValidValue()}.
	 * 
	 * The specific implementation of this method in M_FieldDate
	 * makes sure that the introduced value is a valid date.
	 * 
	 * @access public
	 * @see M_Field::isValidValue()
	 * @param string $value
	 * 		The temporary value of the field. This value is 
	 * 		being provided by {@link M_FieldDate::getValue()}
	 * @return boolean
	 * 		Returns TRUE is the provided value is of a valid 
	 * 		format, FALSE if not.
	 */
	public function __validate($value) {
		//first we need to check if this field is mandatory
	    if ($value === false && $this->mandatory != 1) return TRUE;
		
        //not empty
	    if(is_array($value)) {
			if(checkdate($value['month'], $value['day'], $value['year'])) {
				return TRUE;
			}
		}
		//not empty and nt a valid date
		$this->setErrorMessage(t('Please introduce a valid date'));
		return FALSE;
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldDate} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldDate
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldDate($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}