<?php
/**
 * M_FieldTabs
 * 
 * @package Core
 */
class M_FieldTabs extends M_Field {

	/* -- PROPERTIES -- */

	/**
	 * Number of Tabs
	 *
	 * Stores the number of tabs that has been specified for the field.
	 *
	 * @access private
	 * @var integer
	 */
	private $_numberOfTabs = 2;

	/**
	 * Tabs
	 *
	 * Stores the information for each of the tabs in the field, such as the tab's
	 * title, the tab's description, and the tab's input field.
	 *
	 * @access private
	 * @var array
	 */
	private $_tabs = array();

	/* -- SETTERS -- */

	/**
	 * Set number of tabs
	 *
	 * @access public
	 * @param integer $numberOfTabs
	 * @return M_FieldTabs $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setNumberOfTabs($numberOfTabs) {
		$this->_numberOfTabs = (int) $numberOfTabs;
		return $this;
	}

	/**
	 * Set Tab Title
	 *
	 * Will set the title (for display) of a tab in the field
	 *
	 * @access public
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @param string $title
	 *		The title of the tab
	 * @return M_FieldTabs $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTabTitle($tabIndex, $title) {
		$this->_setTabInfo($tabIndex, 'title', (string) $title);
		return $this;
	}

	/**
	 * Set Tab Description
	 *
	 * Will set the description (for display) of a tab in the field
	 *
	 * @access public
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @param string $description
	 *		The description of the tab
	 * @return M_FieldTabs $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTabDescription($tabIndex, $description) {
		$this->_setTabInfo($tabIndex, 'description', (string) $description);
		return $this;
	}

	/**
	 * Set Tab Field
	 *
	 * Will set the field (input control) of a tab in the field
	 *
	 * @access public
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @param M_Field $field
	 *		The field
	 * @return M_FieldTabs $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTabField($tabIndex, M_Field $field) {
		// Override the field's ID:
		$field->setId($this->_getTabDefaultFieldId($tabIndex));

		// Set the tab info
		$this->_setTabInfo($tabIndex, 'field', $field);

		// Return myself:
		return $this;
	}

	/* -- GETTERS -- */

	/**
	 * Get number of tabs
	 *
	 * @access public
	 * @return integer
	 */
	public function getNumberOfTabs() {
		return $this->_numberOfTabs;
	}

	/**
	 * Get Tab Title
	 *
	 * @access public
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @return string
	 */
	public function getTabTitle($tabIndex) {
		return $this->_getTabInfo($tabIndex, 'title');
	}

	/**
	 * Get Tab Description
	 *
	 * @access public
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @return string
	 */
	public function getTabDescription($tabIndex) {
		return $this->_getTabInfo($tabIndex, 'description');
	}

	/**
	 * Get Tab Field (Input Control)+
	 *
	 * @access public
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @return string
	 */
	public function getTabField($tabIndex) {
		return $this->_getTabInfo($tabIndex, 'field');
	}

	/**
	 * Get tabs
	 *
	 * Will return the collection of tabs in the field. Each tab is represented
	 * by an object, with the following public properties:
	 *
	 * <code>
	 *    index
	 *    title
	 *    description
	 *    field
	 * </code>
	 *
	 * @access public
	 * @return M_ArrayIterator $tabs
	 *		Returns the collection of tabs in the field
	 */
	public function getTabs() {
		// Output collection:
		$out = array();

		// For each of the tabs:
		for($i = 0, $n = $this->getNumberOfTabs(); $i < $n; $i ++) {
			// We get the object for the current tab:
			$out[] = (object) array(
				'index'       => $i,
				'title'       => $this->getTabTitle($i),
				'description' => $this->getTabDescription($i),
				'field'       => $this->getTabField($i)
			);
		}

		// Return the collection
		return new M_ArrayIterator($out);
	}

	/**
	 * Get input control view
	 *
	 * This method overrides {@link M_Field::getInputView()}.
	 *
	 * This method provides with an {@link M_ViewFieldTabs} object, which is used
	 * as the view of the input control, in the view that is returned by
	 * {@link M_Field::getView()}.
	 *
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldTabs
	 * @return M_ViewFieldTabs
	 */
	public function getInputView() {
		$view = new M_ViewFieldTabs($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Set tab info
	 *
	 * @access protected
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @param string $infoType
	 *		The type of info requested. Possible values are 'title', 'description'
	 *		and 'field'
	 * @param string $infoValue
	 *		The new info value
	 * @return void
	 */
	protected function _setTabInfo($tabIndex, $infoType, $infoValue) {
		// Cast the tab index to an integer, just in case:
		$tabIndex = (int) $tabIndex;

		// If the information about the tab does not yet exist:
		if(! isset($this->_tabs[$tabIndex])) {
			// Then, we initiate the information for the tab:
			$this->_tabs[$tabIndex] = $this->_getTabDefault($tabIndex);
		}

		// Set the info:
		$this->_tabs[$tabIndex][$infoType] = $infoValue;
	}

	/**
	 * Get tab info
	 *
	 * @access protected
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @param string $infoType
	 *		The type of info requested. Possible values are 'title', 'description'
	 *		and 'field'
	 * @return mixed
	 */
	protected function _getTabInfo($tabIndex, $infoType) {
		// Cast the tab index to an integer, just in case:
		$tabIndex = (int) $tabIndex;

		// If the information about the tab does not yet exist:
		if(! isset($this->_tabs[$tabIndex])) {
			// Then, we initiate the information for the tab:
			$this->_tabs[$tabIndex] = $this->_getTabDefault($tabIndex);
		}

		// Return the requested info:
		return $this->_tabs[$tabIndex][$infoType];
	}

	/**
	 * Get default tab entry
	 *
	 * @access protected
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @return array
	 */
	protected function _getTabDefault($tabIndex) {
		return array(
			'title'       => NULL,
			'description' => NULL,
			'field'       => $this->_getTabDefaultField($tabIndex)
		);
	}

	/**
	 * Get default field for a tab
	 *
	 * @access protected
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @return M_FieldText
	 */
	protected function _getTabDefaultField($tabIndex) {
		return new M_FieldText($this->_getTabDefaultFieldId($tabIndex));
	}

	/**
	 * Get default Field ID for a tab
	 *
	 * @access protected
	 * @param integer $tabIndex
	 *		The index of the tab, starting from 0
	 * @return M_FieldText
	 */
	protected function _getTabDefaultFieldId($tabIndex) {
		return $this->getId() . '-tab' . (int) $tabIndex;
	}
}