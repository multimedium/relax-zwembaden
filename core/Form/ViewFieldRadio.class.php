<?php
class M_ViewFieldRadio extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		$fieldValue = $field->getValue();
		
		$html  = '<div id="'. $this->getId() .'" class="field-group-radio">';
		foreach($field->getItems() as $value => $label) {
			$html .= '<div>';
			$html .=    '<input type="radio" name="'. $field->getId() .'" value="'. $value .'" id="'. $this->getId() .'-'. $value .'"';

			//important! Cast to string to prevent NULL values to be compared with 0`
			//E.g. If a value of your radio is 0, it would be checked by default
			if((string)$value == (string)$fieldValue) {
				$html .= ' checked="checked"';
			}
			if($field->getDisabled()) {
				$html .= ' disabled="disabled"';
			}
			$html .=    ' />';
			$html .=    '<label class="radio" for="'. $this->getId() .'-'. $value .'">'. $label .'</label>';
			$html .= '</div>';
		}
		$html .= '</div>';
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldRadio.tpl');
	}
}