<?php
/**
 * M_FieldHtml class
 * 
 * M_FieldHtml, a subclass of {@link M_Field}, handles the input 
 * control that allows users to introduce text in rich (HTML)
 * formatting.
 * 
 * @package Core
 */
class M_FieldHtml extends M_Field {

	/**
	 * The HTML which will be displayed
	 *
	 * @var string
	 */
	protected $_html;
	
	/**
	 * @return string
	 */
	public function getHtml() {
		return $this->_html;
	}
	
	/**
	 * @param string $_html
	 */
	public function setHtml($_html) {
		$this->_html = $_html;
	}
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>html</code>
	 * 
	 * Set the html value of th efield. For more information,
	 * read the docs on {@link M_FieldHtml::setHtml()}.
	 * 
	 * <code>width</code>
	 * 
	 * Set width of the input control. For more information, read the 
	 * docs on {@link M_FieldText::setWidth()}.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// The HTML to be displayed
			case 'html':
				$this->setHtml($definition);
				break;

			// Other properties
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldHtml} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldHtml
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldHtml($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}