<?php
class M_ViewFieldNumeric extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		$html = '<input type="text"'
			. ' name="'. $field->getId() .'"'
			. ' id="'. $this->getId() .'"'
				. ' class="';
		$html .=		'field-text';
		$html .=		' field-numeric';
		
		if($field->getReadonly()) {
			$html .=	' readonly';
		}
		$html .=		'"';
		
		if($field->getWidth()) {
			$html .= ' style="width: '. $field->getWidth() .'px;"';
		}
		if($field->getMaxLength()) {
			$html .= ' maxlength="'. $field->getMaxLength() .'"';
		}
		if ($field->getReadonly()) {
			$html .= ' readonly="readonly"';
		}
		if ($field->getHint()) {
			$html .= ' title="'.$field->getHint().'"';
		}

		$html .= ' value="'.$field->getValue().'" />';
		
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldNumeric.tpl');
	}
}