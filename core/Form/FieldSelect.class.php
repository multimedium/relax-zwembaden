<?php
/**
 * M_FieldSelect class
 * 
 * M_FieldSelect, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldOptions}
 * 
 * handles the input control that allow users to choose from a
 * list of options. This specific field renders the typical
 * drop-down menu.
 * 
 * @package Core
 */
class M_FieldSelect extends M_FieldOptions {
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldSelect} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldSelect
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldSelect($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}
