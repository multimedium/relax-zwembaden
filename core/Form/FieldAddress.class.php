<?php
/**
 * M_FieldAddress class
 * 
 * M_FieldAddress, a subclass of {@link M_Field}, handles the 
 * input control that allow users to introduce a complete address. This way
 * there is no need to create seperate street/city/... fields
 * 
 * @package Core
 */
class M_FieldAddress extends M_Field {

	/**
	 * The field which holds the street value
	 *
	 * @var M_FieldText
	 */
	protected $_fieldStreet;
	
	/**
	 * The field which holds the street-number value
	 *
	 * @var M_FieldText
	 */
	protected $_fieldStreetNumber;
	
	/**
	 * The field which holds the postalcode value
	 *
	 * @var M_FieldText
	 */
	protected $_fieldPostalCode;
	
	/**
	 * The field which holds the city value
	 *
	 * @var M_FieldText
	 */
	protected $_fieldCity;
	
	/**
	 * The field which holds the region value
	 *
	 * @var M_FieldText
	 */
	protected $_fieldRegion;
	
	/**
	 * The field which holds the country value
	 *
	 * @var M_FieldText
	 */
	protected $_fieldCountry;
	
	/**
	 * The field which holds the country select
	 * 
	 * @var M_FieldSelect
	 */
	protected $_fieldCountrySelect;

	/**
	 * The field which holds the coördinates
	 *
	 * @var M_FieldGoogleMaps
	 */
	protected $_fieldGoogleMaps;

	/**
	 * Do we want to show the region-field?
	 * 
	 * In most cases we don't want to show the region-field: a simple address
	 * field mostly does not contain region-data. That's why this field defaults
	 * to false
	 *
	 * @see _fieldRegion
	 * @var bool
	 */
	protected $_showRegion = false;

	/**
	 * Do we want to show the house number field?
	 *
	 * @access protected
	 * @var bool
	 */
	protected $_showStreetNumber = true;
	
	/**
	 * Do we want to show the country-field?
	 * 
	 * In most cases we don't want to show the country-field: a simple address
	 * field mostly does not contain country-data. That's why this field defaults
	 * to false
	 *
	 * @see _fieldCountry
	 * @var bool
	 */
	protected $_showCountry = false;
	
	/**
	 * Do we want to show the postalcode
	 * 
	 * @var bool
	 */
	protected $_showPostalCode = true;
	
	/**
	 * Do we want to show the city
	 * 
	 * @var bool
	 */
	protected $_showCity = true;

	/**
	 * Do we want to show the Google-Maps--field?
	 *
	 * In most cases we don't want to show the Google-Maps-field: a simple address
	 * field mostly does not contain coördinates-data. That's why this field defaults
	 * to false
	 *
	 * @see _fieldGoogleMaps
	 * @var bool
	 */
	protected $_showGoogleMaps = false;

	/**
	 * Default error message
	 *
	 * @access private
	 * @var string
	 */
	private $_defaultErrorMessageStreet;

	/**
	 * Default error message
	 *
	 * @access private
	 * @var string
	 */
	private $_defaultErrorMessageCity;
	
	/**
	 * @return M_FieldText
	 */
	public function getFieldCity() {
		if (!$this->hasFieldCity()) {
			$this->_fieldCity = new M_FieldText($this->getId().'_city');
			$this->_fieldCity->setMandatory($this->mandatory);
			$this->_fieldCity->setHint(t('City'));
			$this->_fieldCity->setMandatory($this->isMandatory());
		}
		return $this->_fieldCity;
	}
	
	/**
	 * Set the city field
	 * 
	 * @param M_Field $_fieldCity
	 */
	public function setFieldCity(M_Field $_fieldCity) {
		$_fieldCity->setId($this->getId().'_city');
		$this->_fieldCity = $_fieldCity;
	}
	
	/**
	 * @return M_FieldText|M_FieldSelect
	 */
	public function getFieldCountry() {
		if (!$this->hasFieldCountry()) {
			$this->_fieldCountry = new M_FieldText($this->getId().'_country');
			$this->_fieldCountry->setWidth(245);
			$this->_fieldCountry->setHint(t('Country'));
			$this->_fieldCountry->setMandatory($this->isMandatory());
		}
		
		if (! is_null($this->_fieldCountrySelect)) {
			return $this->_fieldCountrySelect;
		}
		
		return $this->_fieldCountry;
	}
	
	/**
	 * @param M_FieldText $_fieldCountry
	 */
	public function setFieldCountry($_fieldCountry) {
		$this->_fieldCountry = $_fieldCountry;
	}
	
	/**
	 * set field country as select with iso codes
	 * 
	 * use this setter if you want to display the country field
	 * as a select with iso codes as the value. 
	 * You can provide an array of iso codes, which will be used
	 * to determine which countries to show. If you do not provide
	 * a list of countries, the M_LocaleData::getInstance()->getTerritories()
	 * will be used to display all known countries.
	 * 
	 * @access public
	 * @param array $isoCodes
	 */
	public function setFieldCountryAsSelectWithIsoCodes(array $isoCodes = null) {
		// if no iso codes where given, we want all countries
		if (is_null($isoCodes)) {
			// get all countries
			$countries = M_LocaleData::getInstance()->getTerritories()->getArrayCopy();
			// filter out continents and zz
			foreach ($countries as $iso => $country) {
				if (is_numeric($iso) || $iso == 'ZZ') {
					unset($countries[$iso]);
				}
			}
		} else {
			$countries = array();
			// loop trough the isocodes
			foreach ($isoCodes as $code) {
				// push each iso code to the countries array
				$countries[$code] = M_Locale::getTerritoryDisplayName($code);
			}
		}
		// ad an empty value
		$countries[''] = '-';
		// sort the countries
		asort($countries);
		// store the countrie select field in the property
		$this->_fieldCountrySelect = new M_FieldSelect($this->getId() . '_country_select');
		$this->_fieldCountrySelect->setItems($countries);
	}
	
	/**
	 * @return M_FieldText
	 */
	public function getFieldPostalCode() {
		if (!$this->hasFieldPostalCode()) {
			$this->_fieldPostalCode = new M_FieldText($this->getId().'_postalCode');
			$this->_fieldPostalCode->setWidth(70);
			$this->_fieldPostalCode->setMandatory($this->isMandatory());
			$this->_fieldPostalCode->setHint(t('Postalcode'));
		}
		return $this->_fieldPostalCode;
	}
	
	/**
	 * @param M_FieldText $_fieldPostalCode
	 */
	public function setFieldPostalCode($_fieldPostalCode) {
		$this->_fieldPostalCode = $_fieldPostalCode;
	}
	
	/**
	 * @return M_FieldText
	 */
	public function getFieldRegion() {
		if (!$this->hasFieldRegion()) {
			$this->_fieldRegion = new M_FieldText($this->getId().'_region');
			$this->_fieldRegion->setWidth(245);
			$this->_fieldRegion->setHint(t('Region'));
			$this->_fieldRegion->setMandatory($this->isMandatory());
		}
		return $this->_fieldRegion;
	}
	
	/**
	 * @param M_FieldText $_fieldRegion
	 */
	public function setFieldRegion($_fieldRegion) {
		$this->_fieldRegion = $_fieldRegion;
	}
	
	/**
	 * Get the field which you want to use to display the street-name
	 * 
	 * @see setFieldStreet()
	 * @return M_FieldText
	 */
	public function getFieldStreet() {
		if (!$this->hasFieldStreet()) {
			$this->_fieldStreet = new M_FieldText($this->getId().'_street');
			$this->_fieldStreet->setMandatory($this->isMandatory());
			$this->_fieldStreet->setHint(t('Street'));
		}
		
		return $this->_fieldStreet;
	}
	
	/**
	 * Set the field which you want to use to display the street-name
	 * 
	 * @see setFieldStreet()
	 * @param M_FieldText $_fieldStreet
	 */
	public function setFieldStreet($_fieldStreet) {
		$this->_fieldStreet = $_fieldStreet;
	}
	
	/**
	 * Get the field which will be used to display the streetnumber
	 * 
	 * @see setFieldStreetNumber()
	 * @return M_FieldText
	 */
	public function getFieldStreetNumber() {
		if (!$this->hasFieldStreetNumber()) {
			$this->_fieldStreetNumber = new M_FieldText(
				$this->getId().'_streetNumber'
			);
			$this->_fieldStreetNumber->setMandatory($this->mandatory);
			$this->_fieldStreetNumber->setWidth(70);
			$this->_fieldStreetNumber->setHint(t('Streetnbr.'));
			$this->_fieldStreetNumber->setMandatory($this->isMandatory());
		}
		
		return $this->_fieldStreetNumber;
	}
	
	/**
	 * Add your own field as a street-number field
	 * 
	 * @param M_FieldText $_fieldStreetNumber
	 */
	public function setFieldStreetNumber($_fieldStreetNumber) {
		$this->_fieldStreetNumber = $_fieldStreetNumber;
	}

	/**
	 * Set default error message for street
	 *
	 * @access public
	 * @param string $errorMessage
	 * @return void
	 */
	public function setDefaultErrorMessageStreet($errorMessage) {
		$this->_defaultErrorMessageStreet = (string) $errorMessage;
	}

	/**
	 * Set default error message for street
	 *
	 * @access public
	 * @param string $errorMessage
	 * @return void
	 */
	public function setDefaultErrorMessageCity($errorMessage) {
		$this->_defaultErrorMessageCity = (string) $errorMessage;
	}
	
	/**
	 * When setting the mandatory-property for this address field, we need to 
	 * do this for the form fields (street, country, ...)
	 *
	 * @param type $flag 
	 * @return M_FieldAddress
	 */
	public function setMandatory($flag) {
		$flag = (bool)$flag;
		
		parent::setMandatory($flag);
		
		if($this->hasFieldCity()) {
			$this->getFieldCity()->setMandatory($flag);
		}
		
		if($this->hasFieldCountry()) {
			$this->getFieldCountry()->setMandatory($flag);
		}
		
		if($this->hasFieldPostalCode()) {
			$this->getFieldPostalCode()->setMandatory($flag);
		}
		
		if($this->hasFieldGoogleMaps()) {
			$this->getFieldGoogleMaps()->setMandatory($flag);
		}
		
		if($this->hasFieldRegion()) {
			$this->getFieldRegion()->setMandatory($flag);
		}
		
		if($this->hasFieldStreet()) {
			$this->getFieldStreet()->setMandatory($flag);
		}
		
		if($this->hasFieldStreetNumber()) {
			$this->getFieldStreetNumber()->setMandatory($flag);
		}
		
		return $this;
	}

	/**
	 * Get the Google Maps field which holds the location (longitude and
	 * latitude)
	 *
	 * @return M_FieldGoogleMaps
	 */
	public function getFieldGoogleMaps() {
		if (!$this->hasFieldGoogleMaps()) {
			$this->_fieldGoogleMaps = new M_FieldGoogleMaps(
				$this->getId().'_google-maps'
			);
			//Set the id of this address field so the Google Maps has a link
			//to this field (and can use the data to update location e.g.)
			$this->_fieldGoogleMaps->setAddressFieldId($this->getId());

			//Set a default description if no description has been set
			$this->_fieldGoogleMaps->setDescription(
				t('Use the <strong>locate-button</strong> to update the' .
				' location, or drag the marker to the correct position' .
				' if the position is incorrect.')
			);
		}
		return $this->_fieldGoogleMaps;
	}

	/**
	 * Set the module owner for this field and all nested fields too
	 *
	 * M_FieldAddress is a field which holds other fields: streetname, zip,
	 * country, ... We need to set the module-owner for all of these fields
	 * 
	 * @param string $moduleOwner
	 */
	public function setModuleOwner($moduleOwner) {
		parent::setModuleOwner($moduleOwner);
		$this->getShowCity() && $this->getFieldCity()->setModuleOwner($moduleOwner) ;
		$this->getShowGoogleMaps() && $this->getFieldGoogleMaps()->setModuleOwner($moduleOwner);
		$this->getShowPostalCode() &&  $this->getFieldPostalCode()->setModuleOwner($moduleOwner);
		$this->getShowCountry() &&  $this->getFieldCountry()->setModuleOwner($moduleOwner);
		$this->getShowRegion() &&  $this->getFieldRegion()->setModuleOwner($moduleOwner);
		$this->getShowStreetNumber() &&  $this->getFieldStreetNumber()->setModuleOwner($moduleOwner) ;
		$this->getFieldStreet()->setModuleOwner($moduleOwner);
	}

	/**
	 * Check if the field address has a city-field
	 *
	 * @return boolean
	 */
	public function hasFieldCity() {
		return !is_null($this->_fieldCity);
	}

	/**
	 * Check if the field address has a country-field
	 *
	 * @return boolean
	 */
	public function hasFieldCountry() {
		return !(is_null($this->_fieldCountry) && is_null($this->_fieldCountrySelect));
	}

	/**
	 * Check if the field address has a Google Maps-field
	 *
	 * @return boolean
	 */
	public function hasFieldGoogleMaps() {
		return !is_null($this->_fieldGoogleMaps);
	}

	/**
	 * Check if the field address has a Postalcode-field
	 *
	 * @return boolean
	 */
	public function hasFieldPostalCode() {
		return !is_null($this->_fieldPostalCode);
	}

	/**
	 * Check if the field address has a Region-field
	 *
	 * @return boolean
	 */
	public function hasFieldRegion() {
		return !is_null($this->_fieldRegion);
	}

	/**
	 * Check if the field address has a Street-field
	 *
	 * @return boolean
	 */
	public function hasFieldStreet() {
		return !is_null($this->_fieldStreet);
	}

	/**
	 * Check if the field address has a Street-field
	 *
	 * @return boolean
	 */
	public function hasFieldStreetNumber() {
		return !is_null($this->_fieldStreetNumber);
	}

	/**
	 * Set the Google Maps field
	 *
	 * @param M_FieldGoogleMaps $_fieldGoogleMaps
	 */
	public function setFieldGoogleMaps(M_FieldGoogleMaps $_fieldGoogleMaps = null) {
		$this->_fieldGoogleMaps = $_fieldGoogleMaps;
	}

	/**
	 * Determine whether or not you want to display the house number field
	 *
	 * @return bool
	 */
	public function getShowStreetNumber() {
		return $this->_showStreetNumber;
	}

	/**
	 * Determine whether or not you want to display the house number field
	 *
	 * @param bool $flag
	 */
	public function setShowStreetNumber($flag) {
		$this->_showStreetNumber = (bool) $flag;
	}
	
	/**
	 * Determine whether or not you want to display the postalcode field
	 *
	 * @return bool
	 */
	public function getShowPostalCode() {
		return $this->_showPostalCode;
	}

	/**
	 * Determine whether or not you want to display the postalcode field
	 *
	 * @param bool $flag
	 */
	public function setShowPostalCode($flag) {
		$this->_showPostalCode = (bool) $flag;
	}
	
	/**
	 * Determine whether or not you want to display the city field
	 *
	 * @return bool
	 */
	public function getShowCity() {
		return $this->_showCity;
	}

	/**
	 * Determine whether or not you want to display the city field
	 *
	 * @param bool $flag
	 */
	public function setShowCity($flag) {
		$this->_showCity = (bool) $flag;
	}

	/**
	 * Determine whether or not you want to display the 
	 * @return bool
	 */
	public function getShowCountry() {
		return $this->_showCountry;
	}
	
	/**
	 * Determine whether or not you want to display the country-field
	 * 
	 * @param bool $_showCountry
	 */
	public function setShowCountry($_showCountry) {
		$this->_showCountry = $_showCountry;
		
		//if the field needs to be shown: create the field by getting it.
		//this way {@link hasFieldGoogleMaps()} knows a field exists
		if($_showCountry) {
			$this->getFieldCountry();
		}else {
			$this->setFieldCountry(null);
		}
	}
	
	/**
	 * Do we want to show the region-field?
	 * 
	 * @see _showRegion
	 * @return bool
	 */
	public function getShowRegion() {
		return $this->_showRegion;
	}
	
	/**
	 * Determine whether or not you want to display the region-field
	 * 
	 * @see _showRegion
	 * @param bool $_showRegion
	 */
	public function setShowRegion($_showRegion) {
		$this->_showRegion = $_showRegion;
		
		//if the field needs to be shown: create the field by getting it.
		//this way {@link hasFieldGoogleMaps()} knows a field exists
		if($_showRegion) {
			$this->getFieldRegion();
		}else {
			$this->setFieldRegion(null);
		}
	}

	/**
	 * Get ShowGoogleMaps
	 *
	 * @return bool
	 */
	public function getShowGoogleMaps() {
		return $this->_showGoogleMaps;
	}

	/**
	 * Set ShowGoogleMaps
	 *
	 * @param bool $arg
	 * @return M_FieldAddress
	 */
	public function setShowGoogleMaps($arg) {
		$this->_showGoogleMaps = $arg;
		//if the field needs to be shown: create the field by getting it.
		//this way {@link hasFieldGoogleMaps()} knows a field exists
		if($arg) {
			$this->getFieldGoogleMaps();
		}else {
			$this->setFieldGoogleMaps(null);
		}
		return $this;
	}

	/**
	 * Deliver
	 * 
	 * @access public
	 * @return M_ContactAddress
	 */
	public function deliver() {
		$contactAddress = new M_ContactAddress();
		
		//set contactdata
		$contactAddress->setStreetName($this->getFieldStreet()->deliver());
		$contactAddress->setStreetNumber($this->getFieldStreetNumber()->deliver());
		$contactAddress->setCity($this->getFieldCity()->deliver());
		
		//set the optional fields
		if ($this->hasFieldPostalCode()) {
			$contactAddress->setPostalCode($this->getFieldPostalCode()->deliver());
		}
		if ($this->hasFieldCountry()) {
			if(! is_null($this->_fieldCountrySelect)) {
				$contactAddress->setCountryISO($this->_fieldCountrySelect->deliver());
				$contactAddress->setCountry(M_Locale::getTerritoryDisplayName($this->_fieldCountrySelect->deliver()));
			} else {
				$contactAddress->setCountry($this->getFieldCountry()->deliver());
			}
		}
		if ($this->hasFieldRegion()) {
			$contactAddress->setRegion($this->getFieldRegion()->deliver());
		}

		// if a Google Maps field isset: we check if coördinates are set
		if ($this->hasFieldGoogleMaps()) {
			$coördinates = $this->getFieldGoogleMaps()->deliver();

			//coördinates posted
			if ($coördinates != '') {
				$latitude = M_FieldGoogleMaps::getLatitudeFromDeliveredValue($coördinates);
				if ($latitude) $contactAddress->setLatitude($latitude);

				$longitude = M_FieldGoogleMaps::getLongitudeFromDeliveredValue($coördinates);
				if ($longitude) $contactAddress->setLongitude($longitude);
			}

			//no coördinates given: try to retrieve anyway
			else {
				$googleMapsService = new M_ServiceGoogleMaps();
				try {
					//update address with coödinates using the service
					$googleMapsService->getCoordinatesByAddress(
						$contactAddress
					);
				}catch (M_Exception $e) {
					// not able to get location data
				}
			}
		}
		return $contactAddress;
	}
	
	/**
	 * Update address data using an {@link M_ContactAddress} object
	 * 
	 * This will update
	 * 
	 * -streetName
	 * -streetNumber
	 * -postalCode
	 * -city
	 * 
	 * @param M_ContactAddress $contactAddress
	 * @return void
	 */
	public function setContactAddress(M_ContactAddress $contactAddress) {
		// If a street number and house number has been provided separately
		if($contactAddress->getStreetName() || $contactAddress->getStreetNumber()) {
			// Then, we copy the values separately:
			$this->getFieldStreet()->setDefaultValue($contactAddress->getStreetName());
			$this->getFieldStreetNumber()->setDefaultValue($contactAddress->getStreetNumber());
		}
		// If not:
		else {
			// Then, we copy the street address (should include both the name
			// of the street and the house number):
			$this->getFieldStreet()->setDefaultValue($contactAddress->getStreetAddress());
		}

		$this->getFieldPostalCode()->setDefaultValue($contactAddress->getPostalCode());
		$this->getFieldCity()->setDefaultValue($contactAddress->getCity());

		// If a country field is available
		if($this->hasFieldCountry()) {
			// Then, set the default value as well:
			if (! is_null($this->_fieldCountrySelect)) {
				$this->getFieldCountry()->setDefaultValue($contactAddress->getCountryISO());
			} else {
				$this->getFieldCountry()->setDefaultValue($contactAddress->getCountry());
			}
		}

		//set coördinates, if a Google Maps field is found
		if ($this->hasFieldGoogleMaps()) {
			$fldGoogleMaps = $this->getFieldGoogleMaps();
			
			//if both latitude and longitude are set: update field
			if ($contactAddress->getLatitude() && $contactAddress->getLongitude()) {
				$fldGoogleMaps->setDefaultValue(
					$contactAddress->getLatitude().','.$contactAddress->getLongitude()
				);
			}
		}
		
		if($this->hasFieldRegion()) {
			// Then, set the default value as well:
			$this->getFieldRegion()->setDefaultValue($contactAddress->getRegion());
			
		}
	}
	
	/**
	 * Set default value
	 * 
	 * @param M_ContactAddress $contactAddress
	 * @return void
	 */
	public function setDefaultValue(M_ContactAddress $contactAddress = null) {
		if (!is_null($contactAddress)) $this->setContactAddress($contactAddress);
	}
	
	/**
	 * Get value
	 * 
	 * @return $value
	 */
	public function getValue() {
		return $this->getFieldStreet()->getValue() . ' ' . $this->getFieldStreetNumber()->getValue() . ' - ' .
		$this->getFieldPostalCode()->getValue() . ' ' . $this->getFieldCity()->getValue();
	}
	
	/**
	 * Validate the address field
	 * 
	 * @return bool
	 */
	public function __validate() {
		$result = true;
		
		//invalid streetname or number
		if($this->getFieldStreet()->validate() == false || ($this->getShowStreetNumber() && $this->getFieldStreetNumber()->validate() == false)) {
			$this->setErrorMessage(
				$this->_defaultErrorMessageStreet
					? $this->_defaultErrorMessageStreet
					: t('Please provide a valid streetname and number')
			);
			$result = false;
		}
		//invalid postalcode or city
		if($this->getFieldPostalCode()->validate() == false || $this->getFieldCity()->validate() == false) {
			$this->setErrorMessage(
				$this->_defaultErrorMessageCity
					? $this->_defaultErrorMessageCity
					: t('Please provide a postal code and city')
			);
			$result = false;
		}
		if($this->hasFieldGoogleMaps() && $this->getFieldGoogleMaps()->validate() == false) {
			$this->getFieldGoogleMaps()->setErrorMessage(t('Please set location'));
			$result = false;
		}
		
		if($this->hasFieldCountry() && $this->getFieldCountry()->validate() == false) {
			$this->setErrorMessage(
				$this->getFieldCountry()->getDefaultErrorMessage()
			);
			$result = false;
		}
		
		return $result;
	}

	/**
	 * Add field definition
	 *
	 * This method is used by {@link M_Field::factory()}, to mount
	 * the field with the given form definition. Check out Example 1
	 * to see how exactly this method is being used by
	 * factory():
	 *
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			case 'fieldCountryAsSelectWithIsoCodes':
				$this->setFieldCountryAsSelectWithIsoCodes($definition);
				break;
			
			case 'showStreetNumber':
				$this->setShowStreetNumber(M_Helper::isBooleanTrue($definition));
				break;
			
			case 'showRegion':
				$this->setShowRegion(M_Helper::isBooleanTrue($definition));
				break;

			case 'showCountry':
				$this->setShowCountry(M_Helper::isBooleanTrue($definition));
				break;
			
			case 'showPostalCode':
				$this->setShowPostalCode(M_Helper::isBooleanTrue($definition));
				break;

			case 'showGoogleMaps':
				$this->setShowGoogleMaps(M_Helper::isBooleanTrue($definition));
				break;

			case 'googleMapsWidth':
				$this->getFieldGoogleMaps()->setWidth($definition);
				break;

			case 'googleMapsHeight':
				$this->getFieldGoogleMaps()->setHeight($definition);
				break;

			case 'googleMapsMapType':
				$this->getFieldGoogleMaps()->setMapType($definition);
				break;

			case 'googleMapsMandatory':
				$this->getFieldGoogleMaps()->setMandatory($definition);
				break;
			case 'fieldCountry':
				$this->setFieldCountry(
					M_Field::factory(
						'country', $definition
					)
				);
				break;
			case 'fieldCity':
				$this->setFieldCity(
					M_Field::factory(
						'city', $definition
					)
				);
				break;
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldAddress} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @return M_View
	 */
	public function getInputView() {
	   $view = new M_ViewFieldAddress($this);
		$view->setId($this->getId());
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}