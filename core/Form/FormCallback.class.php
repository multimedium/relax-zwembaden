<?php
/**
 * M_FormCallback
 * 
 * M_FormCallback, a subclass of the abstract {@link M_Form}, is a 
 * form implementation that allows the code to plug user-defined 
 * callback functions into the form. 
 * 
 * Callback functions can not only be simple functions, but also 
 * object methods, including static class methods.
 * 
 * A PHP function is passed by its name as a string. Any built-in or 
 * user-defined function can be used, except language constructs such 
 * as: array(), echo(), empty(), eval(), exit(), isset(), list(), 
 * print(), unset().
 * 
 * A method of an instantiated object is passed as an array containing 
 * an object at index 0 and the method name at index 1.
 * 
 * Static class methods can also be passed without instantiating an 
 * object of that class by passing the class name instead of an object 
 * at index 0.
 * 
 * @package Core
 */
class M_FormCallback extends M_Form {
	/**
	 * Form Actions Callback
	 * 
	 * @access private
	 * @see M_FormCallback::setActionsCallback()
	 * @see M_FormCallback::setActionsCallbackArgs()
	 * @var callback
	 */
	private $_actionsCallback;
	
	/**
	 * Form Validator Callback
	 * 
	 * @access private
	 * @see M_FormCallback::setValidatorCallback()
	 * @see M_FormCallback::setValidatorCallbackArgs()
	 * @var callback
	 */
	private $_validatorCallback;
	
	/**
	 * Add form definition
	 * 
	 * This method is used by {@link M_Form::factory()}, to mount 
	 * the form with a given definition. This method overrides
	 * {@link M_Form::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>validate</code>
	 * 
	 * Sets the Form Validator Callback. Accepts two keys inside:
	 * "callback" (required) and "args" (optional). For more info, 
	 * read {@link M_FormCallback::setValidatorCallback()}.
	 * 
	 * <code>actions</code>
	 * 
	 * Sets the Form Actions Callback. Accepts two keys inside:
	 * "callback" (required) and "args" (optional). For more info, 
	 * read {@link M_FormCallback::setActionsCallback()}.
	 * 
	 * Example 1
	 * <code>
	 *    // Mount the form
	 *    $form = M_Form::factory('myForm', new M_Config(array(
	 *       'type' => 'callback',
	 *       'validate' => array(
	 *          'callback' => 'myValidatorCallback',
	 *          'args' => array()
	 *       )
	 *       'actions' => array(
	 *          'callback' => 'myActionsCallback',
	 *          'args' => array()
	 *       )
	 *    )));
	 * 
	 *    // Callback functions:
	 *    // - Validation:
	 *    function myValidatorCallback(M_Form $form) {
	 *       echo 'Validation';
	 *    }
	 * 
	 *    // - Actions:
	 *    function myActionsCallback(array $values, M_Form $form) {
	 *       echo 'Actions';
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @see M_Form::factory()
	 * @see M_Form::set()
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		switch($spec) {
			case 'validate':
			case 'actions':
				// The "actions" key must be an MI_Config object
				if(in_array('MI_Config', class_implements($definition))) {
					// "callback" is a required variable in the config
					if(isset($definition->callback)) {
						$callback = in_array('MI_Config', class_implements($definition->callback))
							? $definition->callback->toNumericKeyArray()
							: $definition->callback;
						
						$args = (isset($definition->args) && in_array('MI_Config', class_implements($definition->args)))
							? $definition->args->toArray()
							: array();
						
						if($spec == 'actions') {
							$this->setActionsCallbackArgs($callback, $args);
						} else {
							$this->setValidatorCallbackArgs($callback, $args);
						}
					}
					// If "callback" has not been provided
					else {
						throw new M_FormException(sprintf('%s could not find "callback"', __CLASS__));
					}
				}
				// If "actions" is not an MI_Config object, it is
				// a function name (string)
				else {
					if($spec == 'actions') {
						$this->setActionsCallbackArgs($definition);
					} else {
						$this->setValidatorCallbackArgs($definition);
					}
				}
				break;
			
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Set Form Actions Callback
	 * 
	 * This method will set the callback for the Form Actions. Read
	 * {@link M_Form::actions()} for more information about the form
	 * actions.
	 * 
	 * By setting a Form Actions Callback, {@link M_FormCallback} will
	 * delegate the execution of the form actions to the provided 
	 * callback function.
	 * 
	 * Equally to a normal {@link M_Form} specification, the callback
	 * function will receive the values that have been collected by
	 * {@link M_Form::getValues()}. However, the callback also receives
	 * an additional argument: the {@link M_FormCallback} object.
	 * 
	 * So, in order for this to work correctly, you must declare the
	 * callback function with the following signature:
	 * 
	 * Example 1, Form Actions Callback
	 * <code>
	 *    function myCallback(array $values, M_Form $form) {
	 *       // Perform actions with received values here
	 *       // ... 
	 *    }
	 * </code>
	 * 
	 * You can however add additional arguments to the callback function,
	 * by simply adding more arguments to this setActionsCallback().
	 * Check out example 2.
	 * 
	 * Example 2, add arguments to the callback function
	 * <code>
	 *    // Construct the form
	 *    $form = new M_FormCallback('myForm');
	 *    
	 *    // Add the Form Actions Callback, and add an extra argument
	 *    $form->setActionsCallback('myCallback', 1);
	 *    
	 *    function myCallback(array $values, M_Form $form, $extra) {
	 *       // Note that we have added a variable: $extra. Its value 
	 *       // will be 1, as passed in to setActionsCallback()
	 *       // ... 
	 *    }
	 * </code>
	 * 
	 * To add additional arguments in bulk (in the form of an array), 
	 * use {@link M_FormCallback::setActionsCallbackArgs()}.
	 * 
	 * @access public
	 * @throws M_FormException
	 * @see M_FormCallback::setActionsCallbackArgs()
	 * @param callback $callback
	 * 		The callback function. Read the {@link M_FormCallback}
	 * 		intro to learn more about callbacks.
	 * @param ...
	 * 		A variable number of additional arguments
	 * @return void
	 */
	public function setActionsCallback($callback) {
		if(is_callable($callback)) {
			$args = func_get_args();
			$n    = func_num_args();
			$this->_actionsCallback = array($callback, $n > 1 ? array_slice($args, 2) : array());
		} else {
			throw new M_FormException(sprintf('%s expects a valid callback', __CLASS__));
		}
	}
	
	/**
	 * Set Form Actions Callback
	 * 
	 * Read intro at {@link M_FormCallback::setActionsCallback()}.
	 * This method will do exactly the same, but will receive the
	 * additional arguments in the form of an array.
	 * 
	 * @access public
	 * @throws M_FormException
	 * @see M_FormCallback::setActionsCallback()
	 * @param callback $callback
	 * 		The callback function. Read the {@link M_FormCallback}
	 * 		intro to learn more about callbacks.
	 * @param array $args
	 * 		Additional arguments to the callback function
	 * @return void
	 */
	public function setActionsCallbackArgs($callback, array $args = array()) {
		if(is_callable($callback)) {
			$this->_actionsCallback = array($callback, $args);
		} else {
			throw new M_FormException(sprintf('%s expects a valid callback', __CLASS__));
		}
	}
	
	/**
	 * Set Form Validator Callback
	 * 
	 * This method will set the callback for the Form Validation. Read
	 * {@link M_Form::__validate()} for more information about the form
	 * validation.
	 * 
	 * By setting a Form Validator Callback, {@link M_FormCallback} will
	 * delegate the execution of the form validation to the provided 
	 * callback function.
	 * 
	 * In a normal {@link M_Form} implementation, the __validate function 
	 * would not receive any argument. However, the callback function
	 * does receive one additional argument: the {@link M_FormCallback}
	 * object.
	 * 
	 * So, in order for this to work correctly, you must declare the
	 * callback function with the following signature:
	 * 
	 * Example 1, Form Validator Callback
	 * <code>
	 *    function myCallback(M_Form $form) {
	 *       // Perform validation of the form here
	 *       // ... 
	 *    }
	 * </code>
	 * 
	 * You can however add additional arguments to the callback function,
	 * by simply adding more arguments to this setValidatorCallback()
	 * method. Check out example 2.
	 * 
	 * Example 2, add arguments to the callback function
	 * <code>
	 *    // Construct the form
	 *    $form = new M_FormCallback('myForm');
	 *    
	 *    // Add the Form Validator Callback, and add an extra argument
	 *    $form->setValidatorCallback('myCallback', 1);
	 *    
	 *    function myCallback(array $values, M_Form $form, $extra) {
	 *       // Note that we have added a variable: $extra. Its value 
	 *       // will be 1, as passed in to setActionsCallback()
	 *       // ... 
	 *    }
	 * </code>
	 * 
	 * To add additional arguments in bulk (in the form of an array), 
	 * use {@link M_FormCallback::setValidatorCallbackArgs()}.
	 * 
	 * @access public
	 * @throws M_FormException
	 * @see M_FormCallback::setValidatorCallbackArgs()
	 * @param callback $callback
	 * 		The callback function. Read the {@link M_FormCallback}
	 * 		intro to learn more about callbacks.
	 * @param ...
	 * 		A variable number of additional arguments
	 * @return void
	 */
	public function setValidatorCallback($callback) {
		if(is_callable($callback)) {
			$args = func_get_args();
			$n    = func_num_args();
			$this->_validatorCallback = array($callback, $n > 1 ? array_slice($args, 2) : array());
		} else {
			throw new M_FormException(sprintf('%s expects a valid callback', __CLASS__));
		}
	}
	
	/**
	 * Set Form Validator Callback
	 * 
	 * Read intro at {@link M_FormCallback::setValidatorCallback()}.
	 * This method will do exactly the same, but will receive the
	 * additional arguments in the form of an array.
	 * 
	 * @access public
	 * @throws M_FormException
	 * @see M_FormCallback::setValidatorCallback()
	 * @param callback $callback
	 * 		The callback function. Read the {@link M_FormCallback}
	 * 		intro to learn more about callbacks.
	 * @param array $args
	 * 		Additional arguments to the callback function
	 * @return void
	 */
	public function setValidatorCallbackArgs($callback, array $args = array()) {
		if(is_callable($callback)) {
			$this->_validatorCallback = array($callback, $args);
		} else {
			throw new M_FormException(sprintf('%s expects a valid callback', __CLASS__));
		}
	}
	
	/**
	 * Form actions
	 * 
	 * All {@link M_Form} subclasses have to implement this method.
	 * This method overrides {@link M_Form::actions()}. In the case
	 * of {@link M_FormCallback}, this method calls the callback
	 * function that has been defined with:
	 * 
	 * - {@link M_FormCallback::setActionsCallback()}, or
	 * - {@link M_FormCallback::setActionsCallbackArgs()}
	 * 
	 * NOTE:
	 * This method should return TRUE on success, and FALSE
	 * on failure.
	 * 
	 * @access public
	 * @throws M_FormException
	 * @see M_Form::getValues()
	 * @param array $values
	 * 		The values that have been collected by the method
	 * 		{@link M_Form::getValues()}.
	 * @return boolean
	 */
	public function actions(array $values) {
		// echo '<h1>Actions</h1>';
		if($this->_actionsCallback) {
			$args = $this->_actionsCallback[1];
			array_unshift($args, $this);
			array_unshift($args, $values);
			return call_user_func_array($this->_actionsCallback[0], $args);
		} else {
			throw new M_FormException(sprintf('No Form Actions Callback has been set for %s', __CLASS__));
		}
	}
	
	/**
	 * "Magic" validation
	 * 
	 * This method is called by {@link M_Form::validate()}, if no
	 * errors have been generated by any of the contained 
	 * {@link M_Field} objects (see {@link M_Field::validate()}). The 
	 * result of this method is then returned by the method 
	 * {@link M_Form::validate()}.
	 * 
	 * In the case of {@link M_FormCallback}, this method calls the 
	 * callback function that has been defined with:
	 * 
	 * - {@link M_FormCallback::setValidatorCallback()}, or
	 * - {@link M_FormCallback::setValidatorCallbackArgs()}
	 * 
	 * NOTE:
	 * This method should return TRUE on success, or FALSE on failure.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function __validate() {
		if($this->_validatorCallback) {
			$args = $this->_validatorCallback[1];
			array_unshift($args, $this);
			return (bool) call_user_func_array($this->_validatorCallback[0], $args);
		} else {
			return TRUE;
		}
	}
}
?>