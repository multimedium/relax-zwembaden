<?php
class M_ViewFieldDate extends M_ViewField {
	
    /**
     * Get html for this field
     *
     * @return string
     */
	protected function getHtml() {

		$field = $this->getField();
		/* @var $field M_FieldDate */
		$fieldValue = $field->getValue();
		$fieldDisplayValue = '';

		// check if we have received a correct value
		if($fieldValue) {
			$tmp = new M_Date($fieldValue);
			$fieldDisplayValue = $tmp->toString($field->getDisplayFormat());
			$fieldValue = $fieldValue['year'] . '-' . $fieldValue['month'] . '-' . $fieldValue['day'];
		}
		// If not:
		else {
			$fieldValue = '';
		}

		// Render input control
		$html  = '<input type="text" id="'. $this->getId() .'DateButton" name="'. $field->getId() .'DateButton" value="' . $fieldDisplayValue .'" ';
		// Hint
		if($field->getHint()) {
			$text = new M_Text($field->getHint());
			$html .= ' title="'. $text->toHtmlAttribute() .'"';
			$onSelect = $field->getOnSelect();
			$onSelect = 'function removeDateHint() { $("#' . $this->getId() . 'DateButton").removeClass("hint"); }';
			$field->setOnSelect($onSelect);
		}
		if($field->getDisabled()) {
			$html .= ' disabled="disabled"';
		}if($field->getWidth()) {
			$html .= ' style="width:'.$field->getWidth().'px;"';
		}
		
		$html .= ' readonly="readonly" class="field-date left" />';
		$html .= '<input type="hidden" name="'. $field->getId() .'" id="'. $this->getId() .'" value="'. $fieldValue .'" />';

		// We need to add some javascript, in order to add the javascript commands
		// for the calendar interface
		$js = M_ViewJavascript::getInstance();

		// Add icon to empty date when not mandatory
		if ($field->isMandatory() == false) {
			$html .= ' <a href="#" title="'.t('clear this date field').'" class="date-empty left clearvalue" rel="'.$field->getId().'"><span>'.t('clear this date field').'</span></a>';

			$javascript = '$("a.date-empty").click(function(){
				var $fieldId = "#" + $(this).attr("rel");
				$fieldId += ", "+$fieldId+"DateButton";
				$($fieldId).val("");
				$($fieldId).trigger("blur");
				return false;
			});';
			$js->addInline($javascript, 'date-empty');
		}

		// Add jQuery UI i18n file
		$lc = M_Locale::getCategory(M_Locale::LC_TIME);
		$i18nFile = new M_File(
			M_Loader::getResourcesPath('admin').'/'.
			M_ViewHtml::FOLDER_JAVASCRIPT .
			'/jquery-ui/i18n/jquery.ui.datepicker-'.$lc.'.js'
		);

		if($i18nFile->exists() && $js->isContextStored('jquery-ui-datepicker-i18n') == false) {
			$js->addFile($i18nFile, 'jquery-ui-datepicker-i18n');
		}

		// Initialize JavaScript
		$javascript = '';

		// Should we disabled a collection of dates?
		$dateCollection = $field->getDisabledDates();
		if($dateCollection) {
			$dateCollectionType = 'disabled';
		}else {
			//if not: should we disable all dates, and only enable a set of dates?
			$dateCollection = $field->getEnabledDates();
			if ($dateCollection) {
				$dateCollectionType = 'enabled';
			}
		}
		
		// Add javascript to make datepicker work
		// First check if there are dates that are to be disabled/enabled
		if ($dateCollection) :
			$javascript .= 'var dateList = new Array();';
			foreach($dateCollection as $date) {
				$d = $date->toString('yyyy') . $date->toString('MM') . $date->toString('dd');
				$javascript .= 'dateList.push("'.$d.'");';
			}
		endif;

		// Start the datepicker options
		$javascriptOptions = 'if (typeof(datepickerOptions) == "undefined") var datepickerOptions = {};';

		// Store the options for this datepicker in the array
		$javascriptOptions .= 'datepickerOptions["'.$this->getId().'"] = {';
		
		// Disable/enable specific dates?
		if ($dateCollection) :
			$javascriptOptions .=    'beforeShowDay: function (dateToShow){';

			//condition-operator
			$conditionOperator = ($dateCollectionType == 'enabled') ? '>=' : '<';
			
			//create a javascript condition which will check if dateToShow is disabled or not
			$condition = '($.inArray($.datepicker.formatDate("yymmdd", dateToShow),dateList) '.$conditionOperator.' 0)';

			//if we want to disable weekends too, we append the condition
			if (!$field->getWeekends()) {
				$javascriptOptions .= 'var $noWeekend = $.datepicker.noWeekends(dateToShow);';
				$javascriptOptions .= 'var $noWeekend = $noWeekend[0];';
				$condition .= ' && $noWeekend';
			}

			//the function returns an array containing a boolean (disable date
			//or not) and the CSS-class for the disabled date
			$javascriptOptions .=	  'return [('.$condition.'), ""]; ';
			$javascriptOptions .=    '},';

		//no specific dates are disabled, maybe only weekends need to be disabled?
		elseif(!$field->getWeekends()):
			$javascriptOptions .=	  'beforeShowDay: $.datepicker.noWeekends,';
		endif;

		$javascriptOptions .=    'dateFormat: "'. M_Date::getDateFormatTranslated(M_Date::getDateFormat($field->getDisplayFormat()), M_Date::getDateFormatTranslationTableForJQueryUi()) .'"';
		$javascriptOptions .=    ', altField: "#'. $this->getId() .'"';
		$javascriptOptions .=    ', altFormat: "yy-mm-dd"';
		$javascriptOptions .=    ', changeMonth: true';
		$javascriptOptions .=    ', changeYear: true';

		if ($field->getOnClose()) {
			$javascriptOptions .= ', onClose: '.$field->getOnClose();
		}
		if ($field->getOnSelect()) {
			$javascriptOptions .= ', onSelect: '.$field->getOnSelect();
		}
		if ($field->getMinDate()) {
			$minDate = $field->getMinDate();

			/*@var $minDate M_Date*/
			$javascriptOptions .= ', minDate: new Date("'. $minDate->toString(M_Date::FULL_DATE_TIME, 'en') .'")';
		}

		if ($field->getMaxDate()) {
			$maxDate = $field->getMaxDate();
			
			/*@var $maxDate M_Date*/
			$javascriptOptions .= ', maxDate: new Date("'. $maxDate->toString(M_Date::FULL_DATE_TIME, 'en') .'")';
		}

		if ($field->getYearRange()) {
			$yearRange = $field->getYearRange();
			$javascriptOptions .= ', yearRange: "' . $yearRange . '"';
		}

		$javascriptOptions .= '};';

		//instantiate the datepicker and set the options
		$javascript .=    '$("#'. $this->getId() .'DateButton").datepicker(datepickerOptions["'.$this->getId().'"]);';

		//store the option variable, and the datepicker-instantion
		$js->addInline($javascriptOptions, 'datepicker-options' . $this->getId());
		$js->addInline($javascript, 'datepicker-' . $this->getId());

		// Return final HTML Render:
		return $html;
	}

	/**
	 * Get the template
	 * 
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldDate.tpl');
	}
}