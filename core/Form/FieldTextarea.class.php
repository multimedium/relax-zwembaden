<?php
/**
 * M_FieldTextarea class
 * 
 * M_FieldTextarea, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldText}
 * 
 * handles the input control that allows users to introduce a 
 * (long) text.
 * 
 * @package Core
 */
class M_FieldTextarea extends M_FieldText {
	/**
	 * Height
	 * 
	 * See {@link FieldText::setHeight()} for more info. This property
	 * is defaulted to 100
	 * 
	 * @access private
	 * @var integer
	 */
	private $_height;
	
	/**
	 * Rows
	 * 
	 * See {@link M_FieldTextArea::setRows()} for more info. This property
	 * is defaulted to 5
	 * 
	 * @var int
	 */
    protected $_rows = 5;

    /**
	 * Cols
	 * 
	 * See {@link M_FieldTextArea::setCols()} for more info. This property
	 * is defaulted to 20
	 * 
	 * @var int
	 */
	protected $_cols = 20;
	
	/**
	 * Display a charcounter or not
	 *
	 * @var bool|null
	 */
	protected $_charcounter = null;
	
	/**
	 * Let's the textarea grow with the text within
	 * 
	 * @param bool|int
	 * @see setAutogrow()
	 */
	protected $_autogrow = false;
	
	/**
	 * Only start to grow from a certain height
	 * 
	 * @param int
	 */
	protected $_autogrowMinheight;
	
	/**
	 * Stop to grow from a height
	 * 
	 * @param int
	 */
	protected $_autogrowMaxheight;

	public function  __construct($id) {
		// Set FALSE, the value should NOT be escaped by default
		$this->_setEscapeValue(FALSE);
		parent::__construct($id);
	}
	/**
	 * Show charcounter?
	 *
	 * @return bool
	 */
	public function getCharcounter() {
		//user has explicitly set to show the charcounter (or not)
		if (!is_null($this->_charcounter)) $cc = $this->_charcounter;
		//if a maxlength is set: show the charcounter
		elseif ($this->getMaxlength() != null) $cc = true;
		//don't show
		else $cc = false;

		return $cc;
	}
	
	/**
	 * Set whether or not to display a charcounter
	 * 
	 * When a {@link M_FieldText::_maxlength} is set, we can display 
	 * the characters the user has left to fill the field.
	 *
	 * @param bool $arg
	 * @return M_FieldTextarea
	 */
	public function setCharcounter($arg) {
		$this->_charcounter = (bool)$arg;
		return $this;
	}
	
	/**
	 * Checks if this textarea will autogrow (height) with the text within
	 * 
	 * @see setAutogrow()
	 * @return bool|in
	 */
	public function getAutogrow() {
		//let the textarea grow with the text entered
		if ($this->_autogrow !== false) {
			$file = new M_File(M_Loader::getResourcesPath('admin').'/javascript/jquery.elastic.js');
			$jsView = M_ViewJavascript::getInstance();
			$jsView->addFile($file, 'jquery.elastic.js');
			
			//create a javasript string from all the options
			$options = '';
			if (!is_null($this->_autogrowMinheight)) {
				$options[] = 'min-height:'.(int)$this->_autogrowMinheight.'px;';
			}
			if (!is_null($this->_autogrowMaxheight)) {
				$options[] =  'max-height:'.(int)$this->_autogrowMaxheight.'px;';
			}
			if (count($options) > 0) {
				$cssView = M_ViewCss::getInstance();
				$cssView->addInline('#'.$this->getId().' {'.implode('', $options).'}');
			}
			
			$js = '$("#'.$this->getId().'").elastic();';
			$jsView->addInline($js, 'textarea-autogrow-'.$this->getId());
		}
		
		return $this->_autogrow;
	}
	
	/**
	 * Set whether or not to autogrow (height) with the text within
	 * 
	 * Note: if autogrow is enabled, and no minHeight is set: {@link M_FieldTextarea::getHeight()}
	 * as a minimum-height
	 * 
	 * @param bool
	 * @param int|null $maxHeight 
	 * 		Will stop to grow when this height in pixels is reached, set to null
	 * 		to disable
	 * @param int|null $minHeight 
	 * 		Will start to grow when this height in pixels is reached, set to null
	 * 		to disable.
	 * @return M_FieldTextarea
	 */
	public function setAutogrow($arg, $maxHeight = null, $minHeight = null) {
		$this->_autogrow = $arg;
		$this->_autogrowMaxheight = $maxHeight;
		//use the textarea's height as minimum height if no specific height
		//was set
		if (is_null($minHeight)) $minHeight = $this->getHeight();
		$this->_autogrowMinheight = $minHeight;
		return $this;
	}
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()} and {@link M_FieldText::set()}, to make 
	 * the following (additional) definition keys available:
	 * 
	 * <code>height</code>
	 * 
	 * Set height of the input control. For more information, read the 
	 * docs on {@link M_FieldTextarea::setHeight()}.
	 * 
	 * <code>charcounter</code>
	 * 
	 * Set whether or not the a charcounter should be displayed. 
	 * For more information, read the docs on {@link M_FieldTextarea::setCharcounter()}.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @see M_FieldText::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// The height (in pixels) of the textarea:
			case 'height':
				$this->setHeight($definition);
				break;
			
			// The rows:
			case 'rows':
				$this->setRows((int) $definition);
				break;

			// Whether or not a char counter should be shown in the textarea:
			case 'charcounter':
				$this->setCharcounter(M_Helper::isBooleanTrue($definition));
				break;

			// Whether or not the textarea should "auto grow":
			case 'autogrow':
				//since this value can be either boolean or numeric, we perform a 
				//check before we get the boolean value from M_Helper
				$definition = (is_numeric($definition)) ? $definition : M_Helper::isBooleanTrue($definition);
				$this->setAutogrow($definition);
				break;

			// Other properties:
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Set height
	 * 
	 * This method will set the height of the field. The height is
	 * expressed in a number of pixels. Check the docs on property
	 * {@link FieldText::$_height} for more info about defaults.
	 * 
	 * @access public
	 * @param integer $pixels
	 * 		The height of the field, in pixels
	 * @return void
	 */
	public function setHeight($pixels) {
		$this->_height = $pixels;
	}
	
	/**
	 * Get height
	 * 
	 * This method is used to get the field height that has been
	 * set previously with {@link M_FieldText::setHeight()}.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getHeight() {
		return $this->_height;
	}
	
    /**
     * Get cols
     * 
     * This method is used to get the number of columns that has been set
     * previously with {@link M_FieldTextarea::setCols()}
     * 
     * @return integer
     */
    public function getCols ()
    {
        return $this->_cols;
    }

    /**
	 * Set cols
	 * 
	 * This method will set the number of columns of the field. 
	 * Check the docs on property {@link FieldTextArea::$_cols} 
	 * for more info about defaults.
	 * 
	 * @access public
	 * @param integer 
	 * 		The number of columns of the field
	 * @return void
	 */
    public function setCols ($_cols)
    {
        $this->_cols = $_cols;
    }

    /**
     * Get rows
     * 
     * This method is used to get the number of rows that has been set
     * previously with {@link M_FieldTextarea::setRows()}
     * 
     * @return integer
     */
    public function getRows ()
    {
        return $this->_rows;
    }

    /**
	 * Set rows
	 * 
	 * This method will set the number of rows of the field. 
	 * Check the docs on property {@link FieldTextArea::$_rows} 
	 * for more info about defaults.
	 * 
	 * @access public
	 * @param integer 
	 * 		The number of rows of the field
	 * @return void
	 */
    public function setRows ($_rows)
    {
        $this->_rows = $_rows;
    }
    
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldTextarea} 
	 * object, which allows {@link M_Field} to include the input 
	 * control in the view that is returned by 
	 * {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldText
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldTextarea($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}