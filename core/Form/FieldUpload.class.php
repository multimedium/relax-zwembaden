<?php
/**
 * M_FieldUpload class
 * 
 * M_FieldUpload, a subclass of {@link M_Field}, handles the input 
 * control that allows users to choose a file from their computer
 * and upload it to the server.
 * 
 * @package Core
 */
class M_FieldUpload extends M_Field {
	/**
	 * Allowed extensions
	 * 
	 * This property holds the collection of file extensions that
	 * are allowed by M_FieldUpload. For more info, read the docs
	 * on {@link M_FieldUpload::setAllowedExtensions()}.
	 * 
	 * @access private
	 * @var array
	 */
	private $_allowedExtensions = array('jpg', 'jpeg', 'png', 'gif');
	
	/**
	 * Maximum filesize
	 * 
	 * This property holds the maximum filesize that is allowed
	 * by M_FieldUpload. Read {@link M_FieldUpload::setMaxFileSize()}
	 * for more info.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_maxSize = 0;
	
	/**
	 * Upload directory
	 * 
	 * This property holds the directory where the uploaded file 
	 * should be saved. Note that, if not given, this directory 
	 * will be defaulted to the "/files" folder in the application's 
	 * root folder.
	 * 
	 * @access private
	 * @var string
	 */
	private $_directory;
	
	/**
	 * Upload filename
	 * 
	 * The filename of the uploaded file. Note that, if not provided
	 * to M_FieldUpload by using {@link M_FieldUpload::setFilename()},
	 * this property will be defaulted to the current timestamp.
	 * 
	 * @access private
	 * @var string
	 */
	private $_filename;
	
	/**
	 * Upload original filename
	 * 
	 * The original filename of the uploaded file.
	 * 
	 * @access private
	 * @var string
	 */
	private $_filenameOriginal;
	
	/**
	 * Is uploaded?
	 * 
	 * This property is a boolean, that indicates whether or not a
	 * file has been uploaded. For internal use only.
	 * 
	 * @access protected
	 * @var boolean
	 */
	protected $_isUploaded = FALSE;
	
	/**
	 * Uploaded filename
	 * 
	 * This property holds the path to the uploaded file. For internal
	 * use only.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_uploaded = NULL;
	
	/**
	 * Default error message
	 * 
	 * @see M_Field::$_defaultErrorMessage
	 * @access protected
	 * @var string
	 */
	protected $_defaultErrorMessage = 'Please choose a file from your computer';
	
	/**
	 * File Preview
	 * 
	 * Holds the set {@link M_ViewFilePreview} for this upload field, if any
	 * was set
	 * 
	 * @see M_FieldUpload::setFilePreview()
	 * @access protected
	 * @var M_ViewFilePreview
	 */
	protected $_filePreview;
	
	/**
	 * Attach to {@link M_Form}
	 * 
	 * This method overrides {@link M_Field::__form()}.
	 * 
	 * When a M_FieldUpload object is attached to a form, the field
	 * will update the following {@link M_Form} properties:
	 * 
	 * <code>
	 *    Property           | Value
	 *    -------------------|------------------------
	 *    method             | POST
	 *    enctype            | multipart/form-data
	 * </code>
	 * 
	 * These properties are crucial for the upload to work properly.
	 * Read {@link M_Form::setProperty()} for more info about the
	 * form properties.
	 * 
	 * @access public
	 * @see M_Field::__form()
	 * @param M_Form $form
	 * 		The form to which the field is being added.
	 * @return void
	 */
	public function __form(M_Form $form) {
		$form->setProperty('method', 'post');
		$form->setProperty('enctype', 'multipart/form-data');
	}
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>allowedExtensions</code>
	 * <code>extensions</code>
	 * 
	 * Set the allowed upload extensions. For more info, read 
	 * {@link M_FieldUpload::setAllowedExtensions()}.
	 * 
	 * <code>maxFilesize</code>
	 * <code>maxSize</code>
	 * 
	 * The maximum filesize of the uploaded file. To learn more,
	 * read {@link M_FieldUpload::setMaxFileSize()}.
	 * 
	 * <code>directory</code>
	 * 
	 * Sets the directory where the uploaded file should be saved.
	 * Read {@link M_FieldUpload::setDirectory()}.
	 * 
	 * <code>filename</code>
	 * 
	 * Sets the filename under which the uploaded file should be saved.
	 * Read {@link M_FieldUpload::setFilename()}.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return M_FieldUpload
	 *		Returns itself, for a fluent programming interface
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// Allowed extensions
			case 'extensions':
			case 'allowedExtensions':
				$this->setAllowedExtensions($definition);
				break;

			// Maximum size of uploaded files
			case 'maxFilesize':
			case 'maxSize':
				$this->setMaxFileSize($definition);
				break;

			// Directory where uploaded files are saved
			case 'directory':
				$this->setDirectory($definition);
				break;

			// Filename of uploaded file
			case 'filename':
				$this->setFilename($definition);
				break;

			// Other properties
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
		return $this;
	}
	
	/**
	 * Set allowed extensions.
	 * 
	 * This method will set a collection of file extensions that
	 * are allowed by M_FieldUpload. If the user uploads a file
	 * of which the extension does not match one of the "allowed"
	 * extensions, a validation error message will be generated.
	 * 
	 * {@link M_FieldUpload::__validate()} will use this collection
	 * of extensions to evaluate the uploaded file.
	 * 
	 * NOTE:
	 * The collection of extensions can be provided as a string 
	 * (separating extensions by comma), or as an array of extensions.
	 * 
	 * Example 1, pass in extensions with a string
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setAllowedExtensions('jpg,gif,png');
	 * </code>
	 * 
	 * Example 2, pass in extensions with an array
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setAllowedExtensions(array('jpg', 'gif', 'png));
	 * </code>
	 * 
	 * NOTE:
	 * The dot notation is completely ignored by this function,
	 * which makes Example 3 and 4 perfectly valid as well:
	 * 
	 * Example 3, pass in extensions with a string
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setAllowedExtensions('.jpg,.gif,.png');
	 * </code>
	 * 
	 * Example 4, pass in extensions with an array
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setAllowedExtensions(array('.jpg', '.gif', '.png));
	 * </code>
	 * 
	 * NOTE:
	 * The validation of the uploaded file's extension is not
	 * case sensitive!
	 * 
	 * @access public
	 * @param mixed $extensions
	 * 		The allowed extensions
	 * @return M_FieldUpload
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAllowedExtensions($extensions) {
		// If the provided extensions have not been given as an array
		if(! is_array($extensions)) {
			// Then they should be given as a comma separated string. Explode it:
			$extensions = explode(',', $extensions);
		}
		
		// Next, clean up the given extensions:
		foreach($extensions as $i => $extension) {
			$extensions[$i] = trim(strtolower($extension), " \t\n\r\0\x0B.");
		}
		
		// Set the property
		$this->_allowedExtensions = $extensions;
		
		// Return self
		return $this;
	}
	
	/**
	 * Set maximum filesize.
	 * 
	 * This method will set the maximum file size that is allowed 
	 * by M_FieldUpload. If the user uploads a file that is bigger
	 * than the allowed size, a validation error message will be 
	 * generated.
	 * 
	 * {@link M_FieldUpload::__validate()} will use this maximum
	 * filesize to evaluate the uploaded file.
	 * 
	 * NOTE:
	 * The maximum filesize can be expressed in a number of bytes,
	 * or in a string.
	 * 
	 * Example 1, pass in maximum filesize with a string
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setMaxFileSize('1MB');
	 * </code>
	 * 
	 * Example 2, pass in maximum filesize with a number
	 * <code>
	 *    $field = new M_FieldUpload('myUpload');
	 *    $field->setMaxFileSize(1048576); // 1MB
	 * </code>
	 * 
	 * If the maximum filesize is passed in with a string, 
	 * {@link M_FsHelper::getNumberOfBytesInString()} will convert
	 * it to a number of bytes.
	 * 
	 * @access public
	 * @param mixed $size
	 * 		The maximum filesize
	 * @return M_FieldUpload
	 *		Returns itself, for a fluent programming interface
	 */
	public function setMaxFileSize($size) {
		if(is_numeric($size)) {
			$this->_maxSize = $size;
		} else {
			$this->_maxSize = M_FsHelper::getNumberOfBytesInString($size);
		}
		return $this;
	}
	
	/**
	 * Set upload directory
	 * 
	 * This method will set the directory where the uploaded file
	 * is saved. Note that you should provide the absolute path
	 * to the upload directory: {@link M_Loader::getAbsolute()}.
	 * 
	 * @access public
	 * @param string $dir
	 * 		The upload directory (full; absolute path)
	 * @return M_FieldUpload
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDirectory($dir) {
		$this->_directory = rtrim($dir, " \t\n\r\0\x0B\\/" . DIRECTORY_SEPARATOR);
		return $this;
	}

	/**
	 * Set File Preview
	 * 
	 * Allows for setting an {@link M_ViewFilePreview} that is to be used
	 * for this {@link FieldUpload}, which will then be used in
	 * {@link M_FieldUpload::getInputView()}
	 * 
	 * @access public
	 * @param M_ViewFilePreview $view
	 * @return M_FieldUpload
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFilePreview(M_ViewFilePreview $view) {
		$this->_filePreview = $view;
		return $this;
	}
	
	/**
	 * Set upload filename
	 * 
	 * This method will set the filename under which the uploaded 
	 * file is saved. Note that this method will change the filename
	 * of the uploaded file, NOT the uploaded file's basename.
	 * 
	 * @access public
	 * @see M_File::getFilename()
	 * @see M_File::getBasename()
	 * @param string $dir
	 * 		The upload filename
	 * @return M_FieldUpload
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFilename($name) {
		$this->_filename = $name;
		return $this;
	}
	
	/**
	 * Get the original filename
	 * 
	 * This method will get the original filename of the file which has been
	 * posted
	 *
	 * @return string
	 */
	public function getFilenameOriginal() {
		return $this->_filenameOriginal;
	}
	
	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * This method will either return the path to the uploaded
	 * file, or the default value (if no file has been uploaded).
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return array|boolean
	 */
	public function getValue() {
		// If value has not been retrieved before:
		if($this->_uploaded == NULL) {
			// Get the ID of the field:
			$id = $this->getId();
			
			// If the file has been uploaded
			if(isset($_FILES[$id]) && is_uploaded_file($_FILES[$id]['tmp_name'])) {
				// Set internal flag:
				$this->_isUploaded = TRUE;
				
				// return the temporary filename
				$this->_uploaded = $_FILES[$id]['tmp_name'];
				
				// set the original filename
				$this->_filenameOriginal = $_FILES[$id]['name'];
			}
			// If no file has been uploaded, we check if a filename has
			// been sent by a (hidden) field. Once a file has been uploaded
			// previously, it's path is maintained in a hidden field. This
			// is done in case other validation errors were generated.
			else {
				// Set internal flag:
				$this->_isUploaded = FALSE;
				
				// Check for the value of a hidden field
				$this->_uploaded = M_Request::getVariable($id . '_hidden', $this->getDefaultValue(), M_Request::TYPE_STRING, M_Request::POST);
				
				// Check for the original filename, if it is given
				$this->_filenameOriginal = M_Request::getVariable($id . '_filenameOriginal_hidden', null, M_Request::TYPE_STRING, M_Request::POST);
			}
		}
		
		// return value
		return $this->_uploaded;
	}
	
	/**
	 * Is empty value?
	 * 
	 * This method overrides {@link M_Field::__empty()}.
	 * 
	 * In the case of M_FieldUpload, this method makes sure that a file
	 * has been uploaded with the POST method. If no file has been 
	 * uploaded, the value is considered to be "empty", and this method
	 * will return TRUE.
	 * 
	 * @access public
	 * @param mixed $value
	 * 		The (temporary) value. Read {@link M_FieldUpload::getValue()} 
	 * 		to learn more about the field's temporary value.
	 * @return boolean
	 */
	public function __empty($value) {
		if(!$this->_isUploaded) {
			if(!is_file($value)) {
				$this->setErrorMessage($this->getDefaultErrorMessage());
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/**
	 * Field-type-specific validation
	 * 
	 * This method overrides {@link M_Field::__validate()}.
	 * 
	 * The specific implementation of this method in M_FieldUpload
	 * makes sure that the uploaded file:
	 * 
	 * 1) has an allowed extension:
	 * {@link M_FieldUpload::setAllowedExtensions()}
	 * 
	 * 2) is not too big:
	 * {@link M_FieldUpload::setMaxFileSize()}
	 * 
	 * @access public
	 * @see M_Field::__validate()
	 * @param string $value
	 * 		The temporary value of the field. This value is 
	 * 		being provided by {@link M_FieldUpload::getValue()}
	 * @return boolean
	 */
	public function __validate($value) {
		// If a file has been uploaded:
		if(!$this->__empty($value)) {
			if($this->_isUploaded) {
				// Get the ID of the field:
				$id = $this->getId();
				
				// We construct an M_File object out of the uploaded file:
				$file = new M_File($_FILES[$id]['name']);
				
				// We check the uploaded file's extension against the allowed ones
				if(count($this->_allowedExtensions) == 0 || in_array(strtolower($file->getFileExtension()), $this->_allowedExtensions)) {
					// We also check the uploaded file's size against the
					// maximum filesize:
					if($this->_maxSize <= 0 || $_FILES[$id]['size'] <= $this->_maxSize) {
						// If filename has not been provided
						if(! $this->_filename) {
							// default to timestamp
							$this->_filename = time();
						}
						
						// If we got here, both the extension and the filesize
						// are OK. At that point, we can move the uploaded
						// file to its target location!
						$target = $this->_directory . DIRECTORY_SEPARATOR . $this->_filename . '.' . $file->getFileExtension();
						if(move_uploaded_file($value, $target)) {
							// If the file has been moved to its target location
							// successfully, we set the full path to the file's
							// new location. This is then the delivered value:
							$this->_uploaded = $target;
							return TRUE;
						}
						// If the uploaded file could not have been moved, we throw
						// an exception. This is probably due to writing permissions.
						else {
							throw new M_FieldException(sprintf(
								'Could not move the uploaded file "%s" to "%s', 
								$value,
								$target
							));
						}
					}
					// The uploaded file is too big. We set a validation
					// error message:
					else {
						$this->setErrorMessage(
							sprintf(
								t('The uploaded file is too big. The maximum file size is %s'),
								M_FsHelper::getFileSizeString($this->_maxSize)
							)
						);
					}
				}
				// The uploaded file's extension did not match any of the
				// allowed ones. We set a validation error message:
				else {
					$this->setErrorMessage(
						sprintf(
							t('Only the following file formats are accepted: %s'),
							implode(', ', $this->_allowedExtensions)
						)
					);
				}
			
				// Return failure, if we're still here
				return FALSE;
			}
			
			// Return TRUE, if not uploaded
			return TRUE;
		}
		// If no file has been uploaded:
		else {
			return !$this->isMandatory();
		}
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldUpload} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldUpload
	 * @return M_View
	 */
	public function getInputView() {
		// Construct the File Upload view:
		$view = new M_ViewFieldUpload($this);
		
		// If a file has already been made available, we generate a
		// preview of that file:
		$preview = '';
		$fn = $this->getValue();
		if($fn) {
			$file = new M_File($fn);
			if($file->exists()) {
				// Has a custom file preview been set?
				if($this->_filePreview) {
					// If so, use it
					$preview = $this->_filePreview;
				}
				// Otherwise, generate a default one
				else {
					$preview = new M_ViewFilePreview;
					$preview->setModuleOwner($this->getModuleOwner());
					$preview->setFile($file);
					$preview->setId($this->getId() . '-file-preview');
				}
				
				// Set the preview in the view
				$view->setUploadedFilePreview($preview);
			}
		}
		
		// Return the view:
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
	
	/**
	 * Check if the file is uploaded
	 * 
	 * This method tells us if a file was uploaded or not. When this field is 
	 * not required, it's possible the form was posted without a value for this 
	 * field
	 * 
	 * @return bool
	 */
	public function isUploaded() {
		return $this->_uploaded;
	}
}