<?php
/**
 * M_FieldOptions class
 * 
 * M_FieldOptions, a subclass of {@link M_Field}, handles input 
 * controls that allow users to interact with a list of items. 
 * M_FieldOptions is an abstract class, providing with shared 
 * functionality for fields such as:
 * 
 * - {@link M_FieldSelect}
 * - {@link M_FieldSortable}
 * 
 * @package Core
 */
abstract class M_FieldOptions extends M_Field {

	/* -- PROPERTIES -- */

	/**
	 * List of items/options
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_items = array();
	
	/**
	 * List of (option)groups
	 *
	 * Using groups we can collect a set of items and display them with a label,
	 * this way some information can be presented more usefull to the user.
	 * 
	 * A group is an array with 2 elements: the first one is the label, the second
	 * one is again an array for which key is the item-value, and the value is 
	 * the item-label
	 * 
	 * e.g. array(
	 * 		0 => array('group A', array(1 => 'orange', 2 => 'blue')),
	 * 		1 => array('group B', array(3 => 'lemon', 4 => 'apple'))
	 * );
	 *
	 * @access private
	 * @var array
	 */
	protected $_groups = array();

	/**
	 * Flag: should items/options be localized?
	 *
	 * @access private
	 *		Can only be manipulated by {@link M_FieldOptions::set()}
	 * @var bool $flag
	 *		If TRUE, options are translated. If FALSE, then options are not
	 *		localized with the {@link t()} function
	 */
	private $_localizeOptions = TRUE;

	/* -- SETTERS -- */
	
	/**
	 * Add a group to this field
	 * 
	 * @param string $label
	 * @param array $options
	 * @return M_FieldSelect
	 */
	public function addGroup($label, array $options) {
		$this->_groups[] = array($label, $options);
		return $this;
	}
	
	/**
	 * Get a group with a certain label
	 *
	 * @param string $label
	 * @return array|false
	 */
	public function getGroup($label) {
		foreach($this->_groups AS $group) {
			if ($group[0] == $label) return $group;
		}
		return false;
	}
	
	/**
	 * Get all groups
	 *
	 * @return array
	 */
	public function getGroups() {
		return $this->_groups;
	}
	
	/**
	 * Set a group
	 * 
	 * Note: this function differs from {@link M_FieldSelect::addGroup()}, it
	 * will override a group when it already exists, addGroup() will always add
	 * a new group
	 * 
	 * @return M_FieldSelect
	 */
	public function setGroup($label, array $options) {
		//check if the group exists, and override if possible
		foreach($this->_groups AS $key => $group) {
			if ($group[0] == $label) {
				$this->_groups[$key] = array($label,$options);
				return $this;
			}
		}
		
		//add a new group
		$this->addGroup($label, $options);
		return $this;
	}
	
	
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>menu</code>
	 * <code>items</code>
	 * <code>options</code>
	 * <code>groups</code>
	 * 
	 * Compose the list of items that should be contained in the
	 * field. Read {@link M_FieldOptions::setItem()} for more info.
	 * 
	 * <code>deliveryFormat</code>
	 * 
	 * Set the delivery format of the field (standard array)
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// Whether or not the options should be translated?
			case 'localizedOptions':
			case 'localizeOptions':
				$this->_localizeOptions = M_Helper::isBooleanTrue($definition);
				break;
			
			// The available options that are to be contained by the field
			case 'menu':
			case 'items':
			case 'options':
				// The provided value must be an iterator. If so:
				if(M_Helper::isIterator($definition)) {
					// For each of the values in the value:
					foreach($definition as $value => $label) {
						// We add the option to the field
						$this->setItem($value, $this->_localizeOptions ? t((string) $label) : $label);
					}
				}
				// If the provided value is not an iterator
				else {
					// Then, we throw an exception to inform about the error
					throw new M_FieldException(sprintf(
						'Expecting list of items in "%s"',
						$spec
					));
				}
				break;

			// Groups among the available options:
			case 'groups':
			case 'optgroups':
				// The provided value must be an iterator. If so:
				if(M_Helper::isIterator($definition)) {
					/* @var $value M_Config */
					// For each of the values in the value:
					foreach($definition as $id => $value) {
						// if an option and label node are known, use this format
						if(isset($value->options) && isset($value->label)) {
							// Get the options:
							$options = $value->options->toArray();

							// If the options should be localized
							if($this->_localizeOptions) {
								// For each of the options:
								foreach($options as $i => $label) {
									// Localize the label:
									$options[$i] = t($label);
								}
							}

							// Add the group:
							$this->addGroup(
								// ... with label:
								$translateStrings ? t((string) $value->label) : $value->label,
								// ... with options
								$options
							);
						}
						// if not (old way): use the node-name as label and all
						// subnodes as value
						else {
							// Get the options:
							$options = $value->toArray();

							// If the options should be localized
							if($this->_localizeOptions) {
								// For each of the options:
								foreach($options as $i => $label) {
									// Localize the label:
									$options[$i] = t($label);
								}
							}

							// Add the group:
							$this->addGroup(
								// ... with label:
								$translateStrings ? t((string) $id) : $id,
								// ... with options
								$options
							);
						}
					}
				}
				// If the provided value is not an iterator
				else {
					// Then, we throw an exception to inform about the error
					throw new M_FieldException(sprintf('Expecting list of items in "%s"', $spec));
				}
				break;

			// Other properties
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Add/Set an item
	 * 
	 * This method will add/set an item to/in the list. To add an
	 * item to the list of items, M_FieldOptions expects both a
	 * value and a label (for display).
	 * 
	 * Note that the item's label is intended for display in the 
	 * form interface, so you should probably use a translated 
	 * string. To learn more about translated strings, read:
	 * 
	 * - {@link M_Locale}
	 * - {@link M_LocaleMessageCatalog}
	 * - {@link t()}
	 * 
	 * @access public
	 * @param string $value
	 * 		The value of the item
	 * @param string $label
	 * 		The label of the item
	 * @return void
	 */
	public function setItem($value, $label) {
		$this->_items[$value] = $label;
	}
	
	/**
	 * Set options
	 * 
	 * Alias for {M_FieldOptions::setItems()}
	 *
	 * @see setItems()
	 * @param array $options
	 */
	public function setOptions(array $options) {
		$this->_items = $options;
	}
	
	/**
	 * Get items
	 * 
	 * This method will return the list of items that is contained 
	 * in the field. Note that this method will return an associative
	 * array:
	 * 
	 * <code>
	 *    Array (
	 *       [value] => [label],
	 *       [value] => [label],
	 *       [value] => [label],
	 *       ...
	 *    )
	 * </code>
	 * 
	 * Typically, this method is used to assign the list of items
	 * to the {@link M_View} class that renders the field's input 
	 * control view ({@link M_Field::getInputView()})
	 */
	public function getItems() {
		return $this->_items;
	}

	/**
	 * Get number of items
	 *
	 * This method will return the number of items/options that has been defined
	 * for the field
	 *
	 * @access public
	 * @return integer $count
	 */
	public function getNumberOfItems() {
		// Number of ungrouped items
		$n = count($this->_items);

		// For each of the groups
		foreach($this->_groups AS $group) {
			// Add the number of items in this group:
			$n += count($group[1]);
		}

		// Return the total
		return $n;
	}
	
	/**
	 * Set the items
	 * 
	 * This method will set the item-list. M_FieldOptions uses the array-key
	 * as value and the value itself as label for the item.
	 * 
	 * Note that the label is intended for display in the 
	 * form interface, so you should probably use a translated 
	 * string. To learn more about translated strings, read:
	 * 
	 * - {@link M_Locale}
	 * - {@link M_LocaleMessageCatalog}
	 * - {@link t()}
	 * 
	 * @access public
	 * @param array $items
	 * 		The item list
	 * @return void
	 */
	public function setItems(array $items) {
		$this->_items = $items;
	}
	
	/**
	 * Get an item
	 * 
	 * This method will return the label of a given item. The
	 * item must be identified by providing the VALUE that has 
	 * been passed into {@link M_FieldOptions::setItem()}
	 * previously.
	 * 
	 * @access public
	 * @see M_Field::setItem()
	 * @param string $value
	 * 		The value of the item
	 * @return string
	 */
	public function getItem($value) {
		return ($this->hasItem($value) ? $this->_items[$value] : FALSE);
	}
	
	/**
	 * Has an item?
	 * 
	 * This method will check if the field has a specified value in the
	 * items (see {@link M_FieldOptions::setItem()}, {@link M_FieldOptions::getItems()},
	 * and {@link M_FieldOptions::getItem()}).
	 * 
	 * @access public
	 * @param string $value
	 * 		The value of the item
	 * @return bool $flag
	 * 		Returns TRUE if the item is present in the field, FALSE
	 * 		if not
	 */
	public function hasItem($value) {
		return (array_key_exists($value, $this->_items));
	}
}