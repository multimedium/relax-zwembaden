<?php
/**
 * M_FieldVat class
 * 
 * M_FieldVat, a subclass of {@link M_FieldText}, handles the input 
 * control that allows users to introduce a VAT number.
 * 
 * @package Core
 */
class M_FieldVat extends M_FieldText {
	
	/**
	 * Create a new VAT field
	 * 
	 * @param string
	 */
	public function __construct($id) {
		//add the VAT validator by default
		$this->addValidatorObject(
			new M_ValidatorVat(), 
			t('Please provide a valid VAT-number')
		);
		
		parent::__construct($id);
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldVat} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldText
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldVat($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}