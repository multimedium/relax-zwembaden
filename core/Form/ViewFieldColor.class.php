<?php
class M_ViewFieldColor extends M_ViewField {
	protected function getHtml() {
		// Get the field
		$field = $this->getField();
		
		// Build the input control
		$html = '<div id="'. $this->getId() .'ColorWrapper"><input type="text" class="field-text" id="'. $this->getId() .'" name="'. $field->getId() .'" value="'. $field->getValue() .'" /></div>';
		
		// Add some javascript
		$html .= '<script type="text/javascript" language="Javascript">';
		$html .=    '$("#'. $this->getId() .'").change(function() { ';
		$html .=       '$(this).parent().css({ padding: "0px 0px 0px 25px", backgroundColor : "#" + $(this).val().replace(/#/, "") }); ';
		$html .=    '});';
		$html .=    '$("#'. $this->getId() .'").change();';
		$html .= '</script>';
		
		// Return the final render:
		return $html;
	}
	
	public function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldColor.tpl');
	}
}