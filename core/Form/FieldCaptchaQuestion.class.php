<?php
/**
 * Create a mathematical CAPTCHA-field which challenges the user to answer
 * a question. Only if the operation is correct the CAPTCHA-field will allow
 * the user to continue
 *
 * @author Wim Van De Mierop
 */
class M_FieldCaptchaQuestion extends M_FieldCaptcha {

	/**
     * Question
     *
     * This variable holds the possible questions of the captcha
     *
     * @access private
     * @var array
     */
	private $_questions = array();


	/**
	 * Stores the active question
	 *
	 * When the captcha is generated, an active question will be selected from
	 * the possible questions and answers
	 * 
	 * @var string
	 */
	private $_activeQuestion;

	/**
	 * Default error message for wrong answer
	 *
	 * @access private
	 * @var string
	 */
	private $_defaultErrorMsgWrongAnswer;
	
	/**
	 * Constructor
	 * 
	 * @param string $id
	 */
	public function  __construct($id, $defaultQuestions = TRUE) {
		parent::__construct($id);

		// If default questions are to be added to the field:
		if($defaultQuestions) {
			// First, add some logic questions:
			$this->_addQuestions();

			// Set the width of the field:
			$this->setWidth(100);
		}
		
		// Set the default decoration view:
		$this->setView('M_ViewFieldDecoratorCaptchaQuestion');
	}

	/**
	 * Set
	 * 
	 * @param string $key
	 * @param string $definition
	 * @param bool $translateStrings
	 */
	public function set($key, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($key) {
			default:
				parent::set($key, $definition, $translateStrings);
		}
	}

	/**
	 * Add question
	 *
	 * Adds a new question with the answers
	 * The field will select a random question while generating the captcha
	 * 
	 * @param string $question
	 * @param array $answers
	 * @return M_FieldCaptchaQuestion
	 */
	public function addQuestion($question, $answers) {
		if(!is_array($answers)) {
			throw new M_Exception(sprintf('
				Please provide an array for the possible answers'
			));
		}

		// Add the question to the pool
		$this->_questions[] = array(
			'question' => $question,
			'answers' => $answers
		);
		return $this;
	}

	/**
     * Get the answer value
     *
     * This function will retrieve the answer
     * value from this->_answer and return it so
     * we can then display it to the user.
     *
     * @access public
     * @return string The operation answer value.
     */
    public function getAnswer()
    {
       return $this->_getSessionNamespace()->__get('captcha-'.$this->getId());
    }

	/**
	 * Get the active question
	 *
	 * @access public
	 * @return void
	 */
	public function getActiveQuestion() {
		return $this->_activeQuestion;
	}

	/**
	 * Get default error message, for wrong answer
	 *
	 * @access public
	 * @return string
	 */
	public function getDefaultErrorMessageWrongAnswer() {
		if($this->_defaultErrorMsgWrongAnswer) {
			return $this->_defaultErrorMsgWrongAnswer;
		} else {
			return t('Please give the correct answer to the question');
		}
	}

	/**
	 * Set default error message, for wrong answer
	 *
	 * @access public
	 * @param string $errorMessage
	 * @return M_FieldCaptchaQuestion $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDefaultErrorMessageWrongAnswer($errorMessage) {
		// Set the default error message
		$this->_defaultErrorMsgWrongAnswer = $errorMessage;

		// return myself
		return $this;
	}

	/**
	 * Clear (remove) questions
	 *
	 * @access public
	 * @return void
	 */
	public function clearQuestions() {
		$this->_questions = array();
	}

	/**
     * Sets the answer value
     *
     * This function will accept the parameters which is
     * basically the result of the function we have done
     * and it will set $this->_answer with it.
     *
     * @access private
     * @param  integer $_answerValue The answer value
     * @see    $this->_answer
     */
    private function _setAnswer($_answerValue)
    {
		//store in
		$this->_getSessionNamespace()->__set(
			'captcha-'.$this->getId(),
			$_answerValue
		);
        return $this;
    }

	private function _setActiveQuestion($_activeQuestion) {
		$this->_activeQuestion = $_activeQuestion;
	}
	
	/**
	 * Generate the captcha
	 * 
	 * @return string
	 */
	protected function _generateCaptcha() {
		// Get a question
		$questions = $this->_questions;
		shuffle($questions);
		$question = $questions[0];

		// Set the active answers and the active question
		$this->_setAnswer($question['answers']);
		$this->_setActiveQuestion($question['question']);
	}

	/**
	 * Get the namespace in which captcha data is stored
	 *
	 * @return M_SessionNamespace
	 */
	private function _getSessionNamespace() {
		if (!M_Session::getInstance()->isStarted()) {
			throw new M_FieldException("Cannot use captcha when session isn't started");
		}
		return new M_SessionNamespace('captcha');
	}

	/**
	 * Get input control view
	 *
	 * This method overrides {@link M_Field::getInputView()}.
	 *
	 * This method provides with an {@link M_ViewFieldCaptchaMath} object,
	 * which allows {@link M_Field} to include the input control
	 * in the view that is returned by {@link M_Field::getView()}.
	 *
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldCaptchaMath
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldCaptchaMath($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}


	/**
	 * Validate the users's answer
	 *
	 * @return bool
	 */
	public function __validate() {
		// We validate the answer in the array with a strtolower, so the answer is case-insensitive
		$answer = $this->getAnswer();

		// If the correct answer was not found in the session: we cannot validate
		if(!is_array($answer)) return false;

		// Check if the users' answer is equal to the correct answer
		$res = in_array(strtolower($this->deliver()), array_map('strtolower', $answer));

		if (!$res) {
			$this->setErrorMessage($this->getDefaultErrorMessageWrongAnswer());
		}

		return $res;
	}

	/**
	 * Get view
	 *
	 * When the form builds the interface, it will ask each of the
	 * contained fields for a view, to include the field interface
	 * (including input control) in the global form interface.
	 *
	 * This method will construct the {@link M_ViewFieldCaptchaMath} object, of
	 * which the class name has been set previously with the method
	 * {@link M_Field::setView()}. To render the input
	 * control, M_Field relies on {@link M_Field::getInputView()}.
	 *
	 * @access public
	 * @return M_ViewHtml
	 */
	public function getView() {
		//only generate when the captcha is displayed, this to prevent a new
		//captcha is generated upon construct. E.g. when this field would be
		//validated a new captcha will be genrated, resulting in a failed
		//validation (the user has responded to the old question).
		//the only way to prevent this is generating the captcha when a view
		//is displayed
		$this->_generateCaptcha();

		return parent::getView();
	}
	
	/**
	 * Add questions
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _addQuestions() {
		// The array of questions, per locale
		$allQuestions = array(
			// Dutch
			'nl'	=>	array(
				array(
					'q'	=> 'Wat is de laatste letter van het alfabet?',
					'a'	=> array('z', 'Z')
				),
				array(
					'q'	=> 'Wat is de tweede letter van het alfabet?',
					'a'	=> array('b', 'B')
				),
				array(
					'q'	=> 'Wat is de derde letter van het alfabet?',
					'a'	=> array('c', 'C')
				),
				array(
					'q'	=> 'Wat is de tweede maand van het jaar?',
					'a'	=> array('februari')
				),
				array(
					'q'	=> 'Hoeveel seconden zitten er in 1 minuut?',
					'a'	=> array('60', 'zestig')
				),
				array(
					'q'	=> 'Hoeveel dagen zitten er in de maand januari?',
					'a'	=> array('31', 'eenendertig')
				),
				array(
					'q'	=> 'Hoeveel uren zitten er in 1 dag?',
					'a'	=> array('24', 'twenty four', 'twenty-four')
				),
				array(
					'q'	=> 'Welke kleur heeft een geel hotel?',
					'a'	=> array('geel')
				),
				array(
					'q'	=> 'Hoeveel kleuren bevat deze lijst: "zwart", "auto", "groen", "roze", "fles" en "telefoon"?',
					'a'	=> array('3', 'drie')
				),
				array(
					'q'	=> 'Welk van de volgende lijst is de naam van een dag van de week: "handschoen", "kaas", "maandag" of "kist"?',
					'a'	=> array('maandag')
				),
				array(
					'q'	=> 'Welk van de volgende lijst is de naam van een dag van de week: "haar", "donderdag" of "been"?',
					'a'	=> array('donderdag')
				),
				array(
					'q'	=> 'Welk van de volgende 4 getallen is het grootst: "14", "vijvendertig" of "40"?',
					'a'	=> array('40')
				),
				array(
					'q'	=> 'Welk van de volgende getallen is het kleinst: "26", "28" of "vierenzeventig"?',
					'a'	=> array('26')
				),
				array(
					'q'	=> 'Welke kleur heeft een roze voet?',
					'a'	=> array('roze')
				),
				array(
					'q'	=> 'Welk van de volgende lijst is een kleur: "kaas", "regenjas", "aap", "melk", "muis" of "paars"?',
					'a'	=> array('paars')
				),
				array(
					'q'	=> 'Hoeveel is achtenzestig duizend vijfhonderd vijf en vijftig geschreven als 1 getal?',
					'a'	=> array('68551', '68.551', '68,551')
				),
				array(
					'q'	=> 'Welk van de volgende lijst is de naam van een dag van de week: "kat", "bij", "vrijdag" of "haai"?',
					'a'	=> array('vrijdag')
				),
				array(
					'q'	=> 'Wat is het laatste cijfer van het getal 6376121?',
					'a'	=> array('1')
				),
				array(
					'q'	=> 'Wat is het 6de cijfer van het getal 7845129?',
					'a'	=> array('2')
				),
				array(
					'q'	=> 'Wat is het 4de cijfer in volgende lijst: "10", "6", "twenty one", "8" en "39"?',
					'a'	=> array('8')
				),
				array(
					'q'	=> 'Welke kleur heeft een bruine hond?',
					'a'	=> array('bruin')
				)
			),
			// English
			'en'	=>	array(
				array(
					'q'	=> 'What is the last letter of the alphabet?',
					'a'	=> array('z', 'Z')
				),
				array(
					'q'	=> 'What is the second letter of the alphabet?',
					'a'	=> array('b', 'B')
				),
				array(
					'q'	=> 'What is the third letter of the alphabet?',
					'a'	=> array('c', 'C')
				),
				array(
					'q'	=> 'What is the second month of the year?',
					'a'	=> array('February', 'february')
				),
				array(
					'q'	=> 'How many seconds are there in one minute?',
					'a'	=> array('60', 'sixty')
				),
				array(
					'q'	=> 'How many days are there in the month january?',
					'a'	=> array('31', 'thirty one', 'thirty-one')
				),
				array(
					'q'	=> 'How many hours are there in one day?',
					'a'	=> array('24', 'twenty four', 'twenty-four')
				),
				array(
					'q'	=> 'What colour does a yellow hotel have?',
					'a'	=> array('yellow')
				),
				array(
					'q'	=> 'How many colours are there in this list: "black", "car", "green", "pink", "bottle" and "phone"?',
					'a'	=> array('3', 'three')
				),
				array(
					'q'	=> 'Which of the following is the name of a day: "glove", "cheese", "Monday" or "chest"?',
					'a'	=> array('monday', 'Monday')
				),
				array(
					'q'	=> 'Which of the following is the name of a day of the week: "hair", "Thursday" or "leg"?',
					'a'	=> array('Thursday', 'thursday')
				),
				array(
					'q'	=> 'Which is the largest of the following numbers: "14", "thirty five" or "fourty"?',
					'a'	=> array('fourty', '40')
				),
				array(
					'q'	=> 'Which of the following is the smallest number: "26", "28" or "seventy four"?',
					'a'	=> array('26', 'twenty six', 'twenty-six')
				),
				array(
					'q'	=> 'What colour does a pink foot have?',
					'a'	=> array('pink')
				),
				array(
					'q'	=> 'Which of the following list is a color: "cheese", "rainjacket", "monkey", "milk", "mouse" or "purple"?',
					'a'	=> array('purple')
				),
				array(
					'q'	=> 'What is sixty eight thousand five hundred and fifty one as a number?',
					'a'	=> array('68551', '68.551', '68,551')
				),
				array(
					'q'	=> 'Which of the following list is a day of the week: "cat", "bee", "Friday" or "shark"?',
					'a'	=> array('Friday', 'friday')
				),
				array(
					'q'	=> 'What is the last digit of 6376121?',
					'a'	=> array('1')
				),
				array(
					'q'	=> 'What is the 6th digit of 7845129?',
					'a'	=> array('2')
				),
				array(
					'q'	=> 'What is the 4th number in the following list: "10", "6", "twenty one", "8" and "39"?',
					'a'	=> array('8')
				),
				array(
					'q'	=> 'What colour does a brown dog have?',
					'a'	=> array('brown')
				)
			)
		);
		
		// Switch the currently active locale, adding the correct ones
		switch(M_Locale::getCategory(M_Locale::LANG)) {
			case 'nl':
				$questions = $allQuestions['nl'];
				break;
			
			case 'en':
			default:
				$questions = $allQuestions['en'];
				break;
		}
		
		// Add all questions
		foreach($questions as $question) {
			$this->addQuestion($question['q'], $question['a']);
		}
	}
}