<?php
/**
 * M_ViewFieldSortable class
 * 
 * M_ViewFieldSortable, a subclass of {@link M_View}, renders the 
 * input control for {@link M_FieldSortable}. Note that this view 
 * only renders the bare minimum of the field. The field container
 * (decorator) is typically rendered by {@link M_ViewField}.
 * 
 * @package Core
 */
class M_ViewFieldSortable extends M_ViewField  {
	
	/**
	 * Default HTML rendering
	 * 
	 * This method overrides {@link M_View::getHtml()}. All subclasses
	 * of {@link M_View} must implement this method, to provide with
	 * default HTML rendering.
	 * 
	 * @access protected
	 * @return string
	 * 		The rendered view (HTML source code)
	 */
	protected function getHtml() {
		$html  = sprintf('<ol class="sortable" id="%s">', $this->getId());
		$href = '';
		if ($this->getField()->isItemDeletable()) {
			$href = '<a href="#" class="delete" title="'.t('Remove this item').'">';
			$href .= '<span>'.t('Remove this item').'</span>';
			$href .= '</a>';
		}
		foreach($this->getField()->getItems() as $value => $label) {
			$html .= sprintf('<li id="%s_%s"><div>%s</div>%s</li>', $this->getId(), $value, $label, $href);
		}
		$html .= '</ol>';
		return $html;
	}


	/**
	 * Pre-Processing
	 * 
	 * Read introduction at {@link M_View}. Typically, this method 
	 * is used to add predefined behavior to the final HTML source 
	 * code (typically used to enrich the HTML with additional 
	 * javascript code).
	 * 
	 * If the {@link M_View} subclass provides with an implementation
	 * of postProcessing(), it will be called by {@link M_View::render()},
	 * after generating the HTML source code.
	 * 
	 * @access protected
	 * @return string
	 */
	protected function preProcessing() {
		$jsInstance = M_ViewJavascript::getInstance();
		$field = $this->getField();
		/* @var $field M_FieldSortable */
		
		//if this field is a connected-field and items are cloned we don't want 
		//to create a sortable field from it. Instead we create a draggable
		$connectedTo = $field->getConnectedWith();
		
			$js = '$(function() {';
		
		if ($connectedTo && $connectedTo->isItemCloned() ) {
			$js .= 		'$("#'.$this->getField()->getId().' li").draggable({';
			$js .= 			'connectToSortable: "#'.$connectedTo->getId().'",';
			$js .= 			'helper: "clone",';
			$js .= 			'revert: "invalid"';
			$js .= 		'});';
			
		}
		
		//create a sortable
		else {
			$name  = $field->getId();
			
			$js .=    '$("#'. $this->getId() .'").sortable({';
			$js .=     ' cursor: "move"';
			$connectedField = $field->getConnectedWith();
			if ($connectedField) {
				$js .=    ', connectWith: "#'.$connectedField->getId().'"';
				if ($field->isItemCloned() ) {
					$js .=    ', helper: "clone"';
				}
			}
			$js .=    ', revert: true';
			$js .=    ', update: function(e, ui) { ';
			$js .=          'var v = "";';
			$js .=          'var c = $("#'. $this->getId() .'").children();';
			$js .=          'for(var i = 0; i < (c.length); i ++) {';
			$js .=             ' if(i > 0) {';
			$js .=                ' v += ",";';
			$js .=             ' }';
			//get the id of the element and cut off the prefix e.g. id = test_13 (13 = value)
			$js .=			   ' vId = $(c[i]).attr("id");';
			$js .=             ' v += vId.substr(vId.indexOf("_")+1);';
			$js .=          '}';
			$js .=          '$("#_'. $name .'").val(v)';
			$js .=       '} ';
			
			//only when you can delete items from this sortable list
			if($field->isItemDeletable()) :
			$js .=    ', receive: function(e, ui) { ';
			//$js .=    		'alert(ui.item.html());';
			$js .=       '} ';
			endif;
			$js .=    '});';
		}
		$js .= '});';
		$js .= '$("#'.$field->getId().'").disableSelection();
		';
		$jsInstance->addInline($js, 'sortable-'.$field->getId());
		
		
		/**
		 * Add javascript to remove list items
		 */
		$js = '$(".sortable a.delete").click(function(){';
		$js .=	'var $li = $(this).parent("li");';
		//check if there is at least one sibling
		$js .=	'if ($li.siblings().length ==0) {';
		$js .=		'alert("'.t('Oops... there should be at least one item in this list').'");';
		$js .=		'return false;';
		$js .=	'}';
		
		//remove the value from the input type
		//first get the value based on the id of the list item
		$js .= 'var $olId = $li.parent().attr("id");';
		$js .= 'var $liId = $li.attr("id");';
		$js .= 'var $liVal = $liId.substr($olId.length+1,1);';
		
		//create an array from the current values
		$js .= 'var $inputElement = $("input[name="+$olId+"]");';
		$js .= 'var $inputValues = $inputElement.val().split("'.M_FieldSortable::VALUE_SEPARATOR.'");';
		
		//remove the deleted item from the array
		$js .= 'var $valIndex = parseInt(jQuery.inArray($liVal, $inputValues));';
		$js .= '$inputValues.splice($valIndex,1);';
		
		//update the value of the input element
		$js .= '$inputElement.val($inputValues.toString());';
		
		//if everything is ok: fade-out the list item and remove it from the list
		$js .= 	'$li.fadeOut("slow", function(){';
		$js .= 		'$(this).remove();';
		$js .=	'});';
		$js .= 	'return false;';
		$js .= '});';		
		
		
		$jsInstance->addInline($js, 'sortable-delete');
	}
	
	/**
	 * Post-Processing
	 * 
	 * Read introduction at {@link M_View}. Typically, this method 
	 * is used to add predefined behavior to the final HTML source 
	 * code (typically used to enrich the HTML with additional 
	 * javascript code).
	 * 
	 * If the {@link M_View} subclass provides with an implementation
	 * of postProcessing(), it will be called by {@link M_View::render()},
	 * after generating the HTML source code.
	 * 
	 * @access protected
	 * @param string $html
	 * 		The HTML source code. The method is supposed to add to 
	 * 		this variable, and return the final code.
	 * @return string
	 */
	protected function postProcessing($html) {
		$name  = $this->getField()->getId();
		$html .= '<input type="text" name="'. $name .'" id="_'. $name .'" value="'. implode(',',$this->getField()->getValue()) .'" />';
		return $html;
	}
	
	/**
	 * Get resource
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldSortable.tpl');
	}
}