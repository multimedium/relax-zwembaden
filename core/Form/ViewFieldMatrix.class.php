<?php
/**
 * M_ViewFieldMatrix class
 * 
 * Used by the class {@link M_FieldMatrix}, in order to render the view of the
 * input control.
 * 
 * @package Core
 */
class M_ViewFieldMatrix extends M_ViewField {
	/**
	 * Default HTML source code rendering
	 * 
	 * This method overrides {@link M_ViewCoreHtml::getHtml()}. All 
	 * subclasses of {@link M_ViewCoreHtml} must implement this method, 
	 * to provide with default HTML rendering.
	 * 
	 * @access protected
	 * @return string
	 */
	protected function getHtml() {
		// Get the field that is being rendered for display:
		$matrixField = $this->getField();
		
		/* @var $matrixField M_FieldMatrix */
		// We prepare an empty output string:
		$out = '';
		
		// The matrix view is a table. So we start the HTML Table output with
		// the table's header:
		$out .= '<table cellpadding="3" cellspacing="0" border="0" class="field-matrix">';
		$out .=    '<thead>';

		// Start a row:
		$out .= 		'<tr>';
		$out .=            '<th class="row-title"></th>';

		// For each of the columns in the matrix:
		foreach($matrixField->getColumnTitles() as $i => $title) {
			// Add the column's title to the table's header:
			$out .= 		'<th class="column-'. $i .'">'. $title .'</th>';
		}
		
		// We finish the table's header, and we start the table's body:
		$out .= 		'</tr>';
		$out .=    '</thead>';
		$out .=    '<tbody>';
		
		// For each of the rows in the matrix:
		for($i = 0, $nRows = $matrixField->getNumberOfRows(); $i < $nRows; $i ++) {
			// Start a row:
			$out .= '<tr class="'.(($i%2 == 0) ? 'odd' : 'even').'">';

			// Add the title of the current row:
			$out .= '<td class="row-title">';
			$out .=    $matrixField->getRowTitle($i);
			$out .= '</td>';
			
			// And, for each of the columns in the matrix:
			for($j = 0, $nColumns = $matrixField->getNumberOfColumns(); $j < $nColumns; $j ++) {
				// Get the field that goes in the current cell of the matrix:
				$currentField = $matrixField->getCellField($j, $i);
				$currentField->setModuleOwner($this->getModuleOwner());
				$currentFieldWidth = $matrixField->getColumnWidth($j);
				if ($currentFieldWidth && is_callable(array($currentField, 'setWidth'))) {
					$currentField->setWidth($currentFieldWidth);
				}
				
				// Add the field to the table:
				$out .= '<td valign="top" class="column-'. $j;
				
				// First of all, we add the error class to the current cell (if any)
				if($currentField->getErrorMessage()) {
					$errorText = new M_Text($currentField->getErrorMessage());
					$out .= ' field-matrix-cell-error" title="'. $errorText->getText() .'"';
				} else {
					$out .= '"';
				}

				$out .= '>';
				
				// If rendering mode for this cell is the BASIC DECORATION:
				if($matrixField->isCellRenderingModeBasicDecoration($j, $i)) {
					// Also add error message, if any
					if($currentField->getErrorMessage() && $matrixField->getShowCellFieldErrorMessages()) {
						$out .= '<div class="error-message">';
						$out .=    $currentField->getErrorMessage();
						$out .= '</div>';
					}
					
					// The input view:
					$out .=    $currentField->getInputView()->fetch();

					// Also add description, if any
					if($currentField->getDescription()) {
						$out .= '<div class="small note">';
						$out .=    $currentField->getDescription();
						$out .= '</div>';
					}
				}
				// If the rendering mode for this cell is INPUT VIEW ONLY
				elseif($matrixField->isCellRenderingModeInputViewOnly($j, $i)) {
					// Render the input view:
					$out .= $currentField->getInputView()->fetch();
				}
				// If the rendering mode is DEFAULT DECORATION:
				else {
					// Render the decorated view of the field
					$out .= $currentField->getView()->fetch();
				}
				
				$out .= '</td>';
			}
			
			// Finish the row:
			$out .= '</tr>';
		}
		
		// Finish the HTML Table:
		$out .=    '</tbody>';
		$out .= '</table>';
		
		// Return the HTML Source code:
		return $out;
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldMatrix.tpl');
	}
}