<?php
/**
 * M_FieldSelectMultiple class
 * 
 * M_FieldSelectMultiple, a subclass of 
 * 
 * - {@link M_Field}
 * - {@link M_FieldOptions}
 * - {@link M_FieldSelectMultiple}
 * 
 * handles the input control that allows users to choose from a
 * list of options. This specific field allows multiple options
 * to be selected from the menu. Each option is presented to the
 * user as a checkbox (see {@link M_FieldCheckbox}).
 * 
 * @package Core
 */
class M_FieldCheckboxes extends M_FieldSelectMultiple {
	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * M_FieldCheckboxes checks whether or not checkboxes have been 
	 * selected by the user. If so, it will provide with an array as the temporary 
	 * value. If not, it will provide with an empty array as the temporary value.
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return array|boolean
	 */
	public function getValue() {
		if(M_Request::getVariable($this->id, FALSE) === FALSE) {
			if(M_Request::isPost()) {
				return array();
			}
		}
		return parent::getValue();
	}
	
	public function getInputView() {
		$view = new M_ViewFieldCheckboxes($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}