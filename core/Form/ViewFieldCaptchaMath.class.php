<?php
class M_ViewFieldCaptchaMath extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		$html = '<input type="text" name="'. $field->getId() .'"'
				. ' id="'. $this->getId() .'"' 
				. ' class="field-text"';
		
		if($field->getWidth()) {
			$html .= ' style="width: '. $field->getWidth() .'px;"';
		}
		if($field->getMaxLength()) {
			$html .= ' maxlength="'. $field->getMaxLength() .'"';
		}
		if(!is_null($field->getHint())) {
			$text = new M_Text($field->getHint());
			$html .= ' title="'. $text->toHtmlAttribute() .'"';
		}
		if($field->getReadonly()) {
			$html .= ' readonly="readonly"';
		}
		$html .= ' />';
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldCaptchaMath.tpl');
	}
}