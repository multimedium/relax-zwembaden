<?php
/**
 * M_FormEvent class
 *
 * The M_FormEvent class is used to dispatch events from the M_Form
 * class (and subclasses). For more information on events, read the 
 * documentation on the following classes:
 * 
 * - {@link M_EventDispatcher}
 * - {@link M_Event}
 * 
 * @package Core
 */
class M_FormEvent extends M_Event {
	const MOUNT = 'form-mount';
	const VALIDATE = 'form-run';
	const ACTIONS = 'form-actions';
}
?>