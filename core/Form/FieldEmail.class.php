<?php
/**
 * M_FieldEmail class
 * 
 * M_FieldEmail, a subclass of {@link M_FieldText}, handles the input 
 * control that allows users to introduce a e-mailaddress.
 * 
 * @package Core
 */
class M_FieldEmail extends M_FieldText {
	
	/**
	 * @var M_ValidatorIsEmail
	 */
	private $_validator;
	
	/**
	 * Get the validator which will validate if this field is a valid e-mailaddress
	 * 
	 * @return M_ValidatorIsEmail
	 */
	public function getValidator() {
		return $this->_validator;
	}
	
	/**
	 * Create a new e-mailfield
	 * 
	 * @param string $id
	 */
	public function __construct($id) {
		parent::__construct($id);
		$this->_validator = new M_ValidatorIsEmail();
		$this->addValidatorObject(
			$this->getValidator(),
			t('Please introduce a valid e-mailaddress')
		);
	}
}