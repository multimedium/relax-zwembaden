<?php
class M_ViewFieldDecoratorDraggable extends M_ViewField {
	protected function getHtml() {
		// Get field + info
		/* @var $field M_FieldDraggable */
		$field = $this->getField();
		$description = $field->getDescription();
		$errorMessage = $field->getErrorMessage();
		
		//The field to which this draggable is connected
		$connectedField = $field->getConnectToSortable();
		
		
		// Render HTML:
		$html  = '<div class="field-row">';
		$html .=    '<div class="field-container">';
		if($errorMessage) {
			$html .= '<div class="field-error-message">';
			$html .=    $errorMessage;
			$html .= '</div>';
		}
		
		$class = 'draggable-container';
		if ($connectedField) $class .= ' draggable-connected';
		$html .=    '<div class="'.$class.'" id="draggable-container-'.$field->getId().'">';
		if ($field->getTitle()) {
			$html .=    '<label for="'. $this->getId() .'">';
			$html .=       $field->getTitle();
			$html .=    '</label>';
		}
		$html .= $field->getInputView()->fetch();
		if(!empty($description)) {
			$html .=    '<div class="small note field-description">';
			$html .=       $description;
			$html .=    '</div>';
		}
		$html .=	'</div>';
		
		// Render the connected field (if there is one)
		if ($connectedField) {
			$connectedDescription = $connectedField->getDescription();
			$class = $connectedField->isItemDeletable() ? ' sortable-deletable' : '';
			$html .=    '<div class="sortable-container sortable-connected'.$class.'" id="sortable-container-'.$field->getId().'">';
			if ($connectedField->getTitle()) {
				$html .=    '<label for="'. $this->getId() .'">';
				$html .=       $connectedField->getTitle();
				$html .=    '</label>';
			}
			$html .= $connectedField->getInputView()->fetch();
			if(!empty($connectedDescription)) {
				$html .=    '<div class="small note field-description">';
				$html .=       $connectedDescription;
				$html .=    '</div>';
			}
			$html .= '</div>';
		}
		
		$html .= 	'</div>';
		$html .= '<div class="clear"/>';
		$html .= '</div>';
		$html .= '<div class="clear"/>';
		
		// return final render:
		return $html;
	}
	
	/**
	 * Get the resource
	 *
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldDecoratorDraggable.tpl');
	}
}