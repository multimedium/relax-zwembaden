<?php
/**
 * M_ViewFieldHtml
 * 
 * @package Core
 */
class M_ViewFieldHtml extends M_ViewField {
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get HTML
	 * 
	 * @access protected
	 * @return string
	 */
	protected function getHtml() {
		// Fetch the field
		$field = $this->getField();

		// Fetch the CSS element for the field
		$css = $field->getCss();
		
		// Add the proper CSS properties that are required for this field
		$css->addClass('field-html');

		// Return the HTML of the field
		return '<div id="'.$field->getId().'" '. $css->getHtmlAttributesString() .'>'.$field->getHtml().'</div>';
	}
	
	/**
	 * Get Resource
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldHtml.tpl');
	}
}