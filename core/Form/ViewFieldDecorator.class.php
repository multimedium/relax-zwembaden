<?php
class M_ViewFieldDecorator extends M_ViewField {
	protected function getHtml() {
		// Get field + info
		$field = $this->getField();
		$description = $field->getDescription();
		$errorMessage = $field->getErrorMessage();
		
		// CSS Class for the field
		$css = 'field-row';
		if($field->isMandatory()) {
			$css .= ' field-row-mandatory';
		}
		
		// Render HTML:
		$html  = '<div class="'. $css .'" id="' . $this->getId() . '">';
		if ($field->getTitle()):
		$html .=    '<label for="'. $field->getId() .'">';
		$html .=       $field->getTitle();
		$html .=    '</label>';
		endif;

		$classContainer = 'field-container';
		if($errorMessage) $classContainer .= ' field-container-error';
		
		$html .=    '<div class="'.$classContainer.'">';
		if($errorMessage) {
			$html .= '<div class="error-message">';
			$html .=    $errorMessage;
			$html .= '</div>';
		}
		$html .=    $field->getInputView()->fetch();
		if(!empty($description)) {
			$html .=    '<div class="field-description">';
			$html .= 		'<div class="small note">';
			$html .=     	  $description;
			$html .=    	'</div>';
			$html .=    '</div>';
		}
		$html .=    '</div>';
		$html .= '<div class="clear"></div>';
		$html .= '</div>';
		
		// return final render
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldDecorator.tpl');
	}
}