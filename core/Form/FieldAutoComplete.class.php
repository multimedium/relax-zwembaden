<?php
/**
 * M_FieldAutoComplete class
 * 
 * M_FieldAutoComplete, a subclass of {@link M_Field}, handles the 
 * input control that allow users to introduce a text value. As the
 * user types, the input control suggests possible values that are
 * similar to the introduced value.
 * 
 * @package Core
 */
class M_FieldAutoComplete extends M_Field {

    /**
     * The url at which outputs the data in Json format
     * 
     * @var string
     */
    protected $_ajaxUrl;
    
    /**
     * Text value for this field
     *
     * Autocomplete fields can contain a text-value next to the normal value.
     * Text value is displayed where as the value is stored
     * 
     * @var string
     */
    protected $_textValue;
    
    /**
     * Store whether this field has to contain a text-value field
     *
     * @var boolean
     */
    protected $_hasTextValue = false;
    
    /**
     * Specify a custom width for the select box.
     * 
     * @var int
     */
    protected $_width = 600;
    
    /**
     * Whether to allow more than one autocompleted-value to enter
     * 
     * @var boolean
     */
    protected $_multiple = true;
    
    /**
     * The minimum number of characters a user has to type before the 
     * autocompleter activates.
     * 
     * @var int
     */
    protected $_minChars = 1;
    
    /**
     * The maximum number of items to show
     *
     * @var int
     */
    protected $_limit = 10;

	/**
	 * Hint
	 *
	 * See {@link FieldAutoComplete::setHint()} for more info.
	 *
	 * @var string
	 */
	private $_hint;
    
	/**
     * @return string
     */
    public function getAjaxUrl ()
    {
        return $this->_ajaxUrl;
    }

    /**
     * @param string $_ajaxUrl
     */
    public function setAjaxUrl ($_ajaxUrl)
    {
        $this->_ajaxUrl = $_ajaxUrl;
    }
	/**
	 * @return string
	 */
	public function getTextValue() {
		return $this->_textValue;
	}
	
	/**
	 * @param string $_text
	 */
	public function setTextValue($_text) {
		$this->_textValue = $_text;
	}
	
	/**
	 * @return boolean
	 */
	public function getHasTextValue() {
		return $this->_hasTextValue;
	}
	
	/**
	 * @param boolean $_hasTextValue
	 */
	public function setHasTextValue($_hasTextValue) {
		$this->_hasTextValue = $_hasTextValue;
	}

    /**
     * @return int
     */
    public function getMinChars ()
    {
        return $this->_minChars;
    }

    /**
     * @param int $_minChars
     */
    public function setMinChars ($_minChars)
    {
        $this->_minChars = $_minChars;
    }

    /**
     * Will return boolean value as string
     * 
     * @return boolean
     */
    public function getMultiple ()
    {
        return ($this->_multiple === true) ? "true" : "false";
    }

    /**
     * @param boolean $_multiple
     */
    public function setMultiple ($_multiple)
    {
        $this->_multiple = $_multiple;
    }

    /**
     * @return int
     */
    public function getWidth ()
    {
        return $this->_width;
    }

    /**
     * @param int $_width
     */
    public function setWidth ($_width)
    {
        $this->_width = $_width;
    }
    
    /**
     * @return int
     */
    public function getLimit ()
    {
        return $this->_limit;
    }

    /**
     * @param int $_limit
     */
    public function setLimit ($_limit)
    {
        $this->_limit = $_limit;
    }

	/**
	 * Set hint
	 *
	 * This method will set the hint of the field.
	 * Check the docs on property {@link M_FieldAutoComplete::$_hint} for more info.
	 *
	 * A hint will internally be stored as a title property. However, by adding
	 * javascript the hint will be displayed as a default-value. Whenever a
	 * user clicks inside the field, or submits, this value will be removed
	 *
	 * @access public
	 * @param string $hint
	 * @return void
	 */
	public function setHint($hint) {
		$this->_hint = $hint;
	}

	/**
	 * Get hint
	 * 
	 * This method is used to get the hint that has been
	 * set previously with {@link M_FieldAutoComplete::getHint()}.
	 *
	 * @access public
	 * @return string
	 */
	public function getHint() {
		return $this->_hint;
	}
    
    /**
     * Add field definition
     *
     * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
     */
	public function set($key, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($key) {
			// The AJAX URL
			case 'ajaxUrl':
				//in a form-definition the relative url is used
				$this->setAjaxUrl(M_Request::getLink($definition));
				break;

			// The max number of results in autocomplete
			case 'limit':
				$this->setLimit($definition);
				break;

			// Multiple selections possible?
			case 'multiple':
				$this->setMultiple($definition);
				break;

			// Min # characters before autocomplete?
			case 'minChars':
				$this->setMinChars($definition);
				break;

			// Width (pixels)
			case 'width':
				$this->setWidth($definition);
				break;

			// Has text value?
			case 'hasTextValue':
				$this->setHasTextValue($definition);
				break;

			// The field's hint
			case 'hint':
				$this->setHint($translateStrings ? t((string) $definition) : $definition);
				break;

			// Other properties
			default:
				parent::set($key, $definition, $translateStrings);
				break;
		}
	}
    
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldText} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @return M_View
	 */
	public function getInputView() {
	    $view = new M_ViewFieldAutocomplete($this);
		$view->setId($this->getId());
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
}