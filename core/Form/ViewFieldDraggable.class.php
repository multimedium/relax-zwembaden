<?php
/**
 * M_ViewFieldSortable class
 * 
 * M_ViewFieldSortable, a subclass of {@link M_View}, renders the 
 * input control for {@link M_FieldSortable}. Note that this view 
 * only renders the bare minimum of the field. The field container
 * (decorator) is typically rendered by {@link M_ViewField}.
 * 
 * @package Core
 */
class M_ViewFieldDraggable extends M_ViewField  {
	
	/**
	 * Default HTML rendering
	 * 
	 * This method overrides {@link M_View::getHtml()}. All subclasses
	 * of {@link M_View} must implement this method, to provide with
	 * default HTML rendering.
	 * 
	 * @access protected
	 * @return string
	 * 		The rendered view (HTML source code)
	 */
	protected function getHtml() {
		$html  = sprintf('<ol class="draggable" id="%s">', $this->getField()->getId());
		foreach($this->getField()->getItems() as $value => $label) {
			$html .= sprintf('<li id="%s"><div>%s</div></li>', $value, $label);
		}
		$html .= '</ol>';
		return $html;
	}
	
	protected function preProcessing() {
		$jsView = M_ViewJavascript::getInstance();
		/* @var $field M_FieldDraggable */
		$field = $this->getField();
		$js .=    '$("#'. $this->getId() .' li").draggable({';
		$js .=     ' helper: "clone"';
		$js .=    ', revert: "invalid"';

		$connectWith = $field->getConnectToSortable();
		if ($connectWith) {
			$js .= ', connectToSortable: "#'.$connectWith->getId().'"';
			if ($connectWith->isItemDeletable()) {
				//only when you can delete items from this sortable list
				$js .=    ', stop: function(e, ui) { ';
				$js .=    		'ui.helper.append("test");';
				$js .=    '}';
			}
		}
		$js .=    '});';

		//disable selection?
		if ($field->isDisableSelection()) {
			$js .=	'$("#'.$this->getId().', #'.$this->getId().' li").disableSelection();';
		}
		
		$jsView->addInline($js, 'draggable-'.$this->getField()->getId());
	}
	
	/**
	 * Post-Processing
	 * 
	 * Read introduction at {@link M_View}. Typically, this method 
	 * is used to add predefined behavior to the final HTML source 
	 * code (typically used to enrich the HTML with additional 
	 * javascript code).
	 * 
	 * If the {@link M_View} subclass provides with an implementation
	 * of postProcessing(), it will be called by {@link M_View::render()},
	 * after generating the HTML source code.
	 * 
	 * @access protected
	 * @param string $html
	 * 		The HTML source code. The method is supposed to add to 
	 * 		this variable, and return the final code.
	 * @return string
	 */
	protected function postProcessing($html) {
		$name  = $this->getField()->getId();
		$html .= '<input type="hidden" name="'. $name .'" id="_'. $name .'" value="'. implode(';',$this->getField()->getValue()) .'" />';
		return $html;
	}
	
	/**
	 * Get resource
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldDraggable.tpl');
	}
}