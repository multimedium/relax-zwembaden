<?php
/**
 * M_FormMailer class
 * @package Core
 */

/**
 * Load the PHPMailer package
 */
M_Loader::loadRelative('core/PHPMailer.php');

/**
 * M_FormMailer
 * 
 * M_FormMailer, a subclass of the abstract {@link M_Form}, is a form
 * implementation that handles an email form. A typical example of
 * usage would be a contact form.
 * 
 * @package Core
 */
class M_FormMailer extends M_Form {
	/**
	 * Recipients
	 * 
	 * This property holds the recipients that will receive the
	 * email message.
	 * 
	 * @access public
	 * @var array
	 */
	private $_to = array();
	
	/**
	 * CC
	 * 
	 * This property holds the recipients that will receive the
	 * email message, in Carbon Copy.
	 * 
	 * @access public
	 * @var array
	 */
	private $_cc = array();
	
	/**
	 * BCC
	 * 
	 * This property holds the recipients that will receive the
	 * email message, in Blind Carbon Copy.
	 * 
	 * @access public
	 * @var array
	 */
	private $_bcc = array();
	
	/**
	 * Subject
	 * 
	 * This property holds the subject of the email message.
	 * 
	 * @access public
	 * @var string
	 */
	private $_subject;
	
	/**
	 * Message body
	 * 
	 * This property holds the class name of the {@link M_View} that 
	 * renders the email message's body.
	 * 
	 * @access private
	 * @var string
	 */
	private $_body;
	
	/**
	 * Add form definition
	 * 
	 * This method is used by {@link M_Form::factory()}, to mount 
	 * the form with a given definition. This method overrides
	 * {@link M_Form::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * M_FormMailer adds 1 new definition key:
	 * 
	 * <code>mail</code>
	 * 
	 * Inside this new key, you can add the following keys, to specify
	 * the mail message's properties and the recipients of the mail.
	 * 
	 * <code>cc</code>
	 * 
	 * Add a recipient in Carbon Copy. For more information, read
	 * {@link M_FormMailer::addCc()}.
	 * 
	 * <code>bcc</code>
	 * 
	 * Add a recipient in Blind Carbon Copy. For more information, 
	 * read {@link M_FormMailer::addBcc()}.
	 * 
	 * <code>to</code>
	 * 
	 * Sets the recipient(s) of the email. For more information, 
	 * read {@link M_FormMailer::addRecipient()}.
	 * 
	 * <code>subject</code>
	 * 
	 * Sets the subject of the sent email message. For more info, 
	 * read {@link M_FormMailer::setSubject()}.
	 * 
	 * <code>body</code>
	 * 
	 * Sets the body of the email message. For more info, read 
	 * {@link M_FormMailer::setBody()}.
	 * 
	 * Example 1, illustrates how you could mount an instance of 
	 * M_FormMailer
	 * <code>
	 *    $form = M_Form::factory('myForm', new Config(array(
	 *       'type' => 'mailer',
	 *       'mail' => array(
	 *          'to' => 'Tom Bauwens <tom@multimedium.be>',
	 *          'cc' => 'Info <info@multimedium.be>',
	 *          'subject' => 'Subject'
	 *       ),
	 *       'fields' => array(
	 *          'msg' => array(
	 *             'type' => 'textarea',
	 *             'title' => 'Your comments',
	 *             'mandatory' => TRUE
	 *          )
	 *       )
	 *    )));
	 * </code>
	 * 
	 * @access public
	 * @see M_Form::factory()
	 * @see M_Form::set()
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		switch($spec) {
			case 'email':
			case 'mail':
				if(is_object($definition) && in_array('MI_Config', class_implements($definition))) {
					foreach($definition as $name => $value) {
						switch($name) {
							case 'to':
								$this->addRecipient($value);
								break;
							
							case 'cc':
								$this->addCc($value);
								break;
							
							case 'bcc':
								$this->addBcc($value);
								break;
							
							case 'subject':
								$this->setSubject($value);
								break;
							
							case 'body':
								$this->setBody($value);
								break;
						}
					}
				}
				break;
			
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Extract recipients
	 * 
	 * This method will extract the recipient(s) that have been 
	 * provided in a string. The recipients in the string are separated
	 * by either one of the following characters:
	 * 
	 * <code>, ;</code>
	 * 
	 * So, a valid example of a string that provides with multiple
	 * recipients would be:
	 * 
	 * Example 1
	 * <code>tom@multimedium.be; info@multimedium.be</code>
	 * 
	 * Optionally, the string can define both the recipients' names 
	 * and email addresses. To define both the name and email address, 
	 * the string must have the following format:
	 * 
	 * <code>Name <email@address.com></code>
	 * 
	 * So, in order to describe both the names and email addresses of
	 * multiple recipients in a single string, we could rewrite the
	 * string in Example 1 as follows:
	 * 
	 * Example 2
	 * <code>
	 *    Tom Bauwens <tom@multimedium.be>; Info <info@multimedium.be>
	 * </code>
	 * 
	 * This method will return an array with the names and addresses
	 * of each of the recipients. Check out Example 3 to learn more
	 * about the output array's format.
	 * 
	 * Example 3
	 * <code>
	 *    print_r($this->_extractRecipients('Tom Bauwens <tom@multimedium.be>; Info <info@multimedium.be>'));
	 * </code>
	 * 
	 * Example 3 will generate the following output:
	 * 
	 * <code>
	 *    Array (
	 *       0 => Array (
	 *          'name'  => 'Tom Bauwens',
	 *          'email' => 'tom@multimedium.be'
	 *       ),
	 *       1 => Array (
	 *          'name'  => 'Info',
	 *          'email' => 'info@multimedium.be'
	 *       )
	 *    )
	 * </code>
	 * 
	 * @access private
	 * @param string $recipients
	 * 		The string of recipients
	 * @return array
	 */
	private function _extractRecipients($recipient) {
		$output = array();
		$validator = new M_ValidatorIsEmail;
		foreach(preg_split('/[;,]+/', $recipient) as $to) {
			$to = trim($to);
			if(!empty($to)) {
				$match = array();
				if(preg_match('/(.{0,})[^\s]{0,}\<([^\>]+)>/im', $to, $match)) {
					$email = trim($match[2]);
					if($validator->check($email)) {
						$output[] = array(
							'name'  => trim($match[1]),
							'email' => $email
						);
					} else {
						throw new M_FormException(__CLASS__ . ': Invalid email address');
					}
				} else {
					if($validator->check($to)) {
						$output[] = array(
							'name'  => '',
							'email' => $to
						);
					} else {
						throw new M_FormException(__CLASS__ . ': Invalid email address');
					}
				}
			}
		}
		
		return $output;
	}
	
	/**
	 * Add CC
	 * 
	 * This method will add recipient(s) in Carbon Copy. Read
	 * {@link M_FormMailer::_extractRecipients()} to learn more on
	 * how you should provide recipients.
	 * 
	 * @access public
	 * @param string $recipient
	 * 		The recipient(s) in CC
	 * @return void
	 */
	public function addCc($recipient) {
		foreach($this->_extractRecipients($recipient) as $recipient) {
			$this->_cc[$recipient['email']] = $recipient['name'];
		}
	}
	
	/**
	 * Add BCC
	 * 
	 * This method will add recipient(s) in Blind Carbon Copy. Read 
	 * {@link M_FormMailer::_extractRecipients()} to learn more on 
	 * how you should provide recipients.
	 * 
	 * @access public
	 * @param string $recipient
	 * 		The recipient(s) in BCC
	 * @return void
	 */
	public function addBcc($recipient) {
		foreach($this->_extractRecipients($recipient) as $recipient) {
			$this->_bcc[$recipient['email']] = $recipient['name'];
		}
	}
	
	/**
	 * Add recipient
	 * 
	 * This method will add recipient(s). These recipients are 
	 * added to the "To: " header. Read the comments on
	 * {@link M_FormMailer::_extractRecipients()} to learn more on 
	 * how you should provide recipients.
	 * 
	 * @access public
	 * @param string $recipient
	 * 		The recipient(s)
	 * @return void
	 */
	public function addRecipient($recipient) {
		foreach($this->_extractRecipients($recipient) as $recipient) {
			$this->_to[$recipient['email']] = $recipient['name'];
		}
	}
	
	/**
	 * Set the email message's subject
	 * 
	 * IMPORTANT NOTE:
	 * M_FormMailer will strip all HTML from the subject!
	 * 
	 * @access public
	 * @param string $title
	 * 		The email subject
	 * @return void
	 */
	public function setSubject($title) {
		$filter = new M_FilterTextHtml(new M_FilterTextValue($title));
		$this->_subject = $filter->apply();
	}
	
	/**
	 * Set Message Body
	 * 
	 * To render the body of the email message, M_FormMailer uses an
	 * {@link M_View}. This method will set the CLASS NAME of the
	 * {@link M_View} subclass that should be used to render the email
	 * message body.
	 * 
	 * IMPORTANT NOTE:
	 * If no {@link M_View} class name is provided to the form, the 
	 * form will render a simple HTML, displaying the values of all 
	 * variables that have been generated by the form's contained 
	 * collection of {@link M_Field} objects.
	 * 
	 * @access public
	 * @throws M_FormException
	 * @param string $body
	 * 		The name of the {@link M_View} class that renders the
	 * 		email message body.
	 * @return void
	 */
	public function setBody($body) {
		if(in_array('MI_View', class_implements($body))) {
			$this->_body = $body;
		} else {
			throw new M_FormException(sprintf(
				'%s expects an object that implements the MI_View interface to compose the email body',
				__CLASS__
			));
		}
	}
	
	/**
	 * Form actions
	 * 
	 * All {@link M_Form} subclasses have to implement this method.
	 * This method overrides {@link M_Form::actions()}.
	 * 
	 * NOTE:
	 * This method should return TRUE on success, and FALSE
	 * on failure.
	 * 
	 * @access public
	 * @see M_Form::getValues()
	 * @param array $values
	 * 		The values that have been collected by the method
	 * 		{@link M_Form::getValues()}.
	 * @return boolean
	 */
	public function actions(array $values) {
		/*
		// Compose the email message's body:
		if($this->_body) {
			$view = new $this->_body;
			$view->assign('fields', $values);
			$view->assign('subject', $this->_subject);
			$body = $view->fetch();
		} else {
			$body = '';
			foreach($values as $name => $value) {
				$body .= "$name: $value<br>";
			}
		}
		
		// Create a new mailer:
		$mail = new PHPMailer;
		
		// TODO: mail properties
		$mail->From = 'tom@multimedium.be';
		$mail->FromName = 'Tom Bauwens';
		$mail->Host = 'mail.multimedium.be';
		$mail->IsHTML(TRUE);
		
		// Set recipients:
		foreach($this->_to as $email => $name) {
			$mail->AddAddress($email, $name);
		}
		
		foreach($this->_cc as $email => $name) {
			$mail->AddCC($email, $name);
		}
		
		foreach($this->_cc as $email => $name) {
			$mail->AddBCC($email, $name);
		}
		
		// Set subject and body
		$mail->Subject = $this->_subject ? $this->_subject : __CLASS__;
		$mail->Body = $body;
		
		// fire away! :)
		if($mail->Send()) {
			return TRUE;
		} else {
			return FALSE;
		}
		 */
		return TRUE;
	}
}
?>