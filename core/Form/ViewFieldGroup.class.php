<?php
class M_ViewFieldGroup extends M_ViewCoreHtml {
	/**
	 * @var M_FieldGroup
	 */
	private $_fieldGroup;
	
	/**
	 * Construct
	 * 
	 * @param M_FieldGroup $group
	 */
	public function __construct(M_FieldGroup $group) {
		$this->_fieldGroup = $group;
		$this->assign('fieldGroup', $group);
	}
	
	/**
	 * Render this group
	 * 
	 * @return string
	 */
	protected function getHtml() {
		$group = $this->_fieldGroup;
		
		// render html
		$html  = '';
		
		$class = "group-row";
		if ($group->getFoldable()) $class .= " foldable";
		$html .= '<fieldset class="'.$class.'" id="group-' . $group->getId() . '">';
		
		if ($group->getTitle()) {
			$cssClass = 'group-title';
			if($group->getFolded()) $cssClass .= ' folded';
			$html .='<legend class="'. $cssClass .'">'.$group->getTitle().'</legend>';
		}
		if ($group->getDescription()) {
			$html .='<div class="group-description">'.$group->getDescription().'</div>';
		}
		
		//rend the fields
		$html .= '<div class="group-fields">';
		
		foreach($group->getFields() as $field) {
			/*@var $field M_Field*/
			$field->setModuleOwner($group->getModuleOwner());
			$html .= $field->getView()->fetch();
		}
		
		$html .= '</div>';
		$html .= '</fieldset>';
		
		// return the final render
		return $html;
	}
	
	/**
	 * Get the resource
	 *
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/Group.tpl');
	}
}