<?php
/**
 * M_FieldText class
 * 
 * M_FieldText, a subclass of {@link M_Field}, handles the input 
 * control that allows users to introduce a (short) text.
 * 
 * @package Core
 */
class M_FieldText extends M_Field {
	/**
	 * Maximum length
	 * 
	 * See {@link FieldText::setMaxLength()} for more info.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_maxlength;
	
	/**
	 * Width
	 * 
	 * See {@link FieldText::setWidth()} for more info. This property
	 * is defaulted to 200.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_width;
	
	/**
	 * Readonly
	 * 
	 * See {@link FieldText::setReadonly()} for more info. This property
	 * is defaulted to false.
	 *
	 * @var bool
	 */
	private $_readonly;
	
	/**
	 * Hint
	 *
	 * See {@link FieldText::setHint()} for more info.
	 * 
	 * @var string
	 */
	private $_hint;

	/**
	 * Escape value
	 *
	 * Tells whether or not the temporary value of the field should be escaped.
	 *
	 * @access private
	 * @var boolean
	 */
	private $_escapeValue = TRUE;
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>maxlength</code>
	 * 
	 * Set maximum length of the input control. For more information,
	 * read the docs on {@link M_FieldText::setMaxlength()}.
	 * 
	 * <code>width</code>
	 * 
	 * Set width of the input control. For more information, read the 
	 * docs on {@link M_FieldText::setWidth()}.
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// The maximum length of the textual input field (the allowed maximum
			// number of characters in the field)
			case 'maxlength':
				$this->setMaxlength($definition);
				break;

			// The width (in pixels) of the field
			case 'width':
				$this->setWidth($definition);
				break;

			// Whether or not the field should be 'readonly' (not editable)
			case 'readonly':
				$this->setReadonly($definition);
				break;

			// The field's hint
			case 'hint':
				$this->setHint($translateStrings ? t((string) $definition) : $definition);
				break;
			
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Set maximum length
	 * 
	 * This method will set the maximum number of characters that 
	 * will be allowed by the text field. If more characters are
	 * introduced into the input control, a validation error
	 * message will be generated.
	 * 
	 * @access public
	 * @param integer $number
	 * 		The maximum allowed number of characters
	 * @return void
	 */
	public function setMaxlength($number) {
		$this->_maxlength = $number;
	}
	
	/**
	 * Get maximum length
	 * 
	 * This method is used to get the maximum length that has been
	 * set previously with {@link M_FieldText::setMaxlength()}.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getMaxlength() {
		return $this->_maxlength;
	}
	
	/**
	 * Set width
	 * 
	 * This method will set the width of the field. The width is
	 * expressed in a number of pixels. Check the docs on property
	 * {@link FieldText::$_width} for more info about defaults.
	 * 
	 * @access public
	 * @param integer $pixels
	 * 		The width of the field, in pixels
	 * @return void
	 */
	public function setWidth($pixels) {
		$this->_width = $pixels;
	}
	
	/**
	 * Get width
	 * 
	 * This method is used to get the field width that has been
	 * set previously with {@link M_FieldText::setWidth()}.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getWidth() {
		return $this->_width;
	}
	
	/**
	 * Set readonly
	 * 
	 * This method will set the readonly-property of the field. 
	 * Check the docs on property {@link FieldText::$_readonly} for more info 
	 * about defaults.
	 * 
	 * @access public
	 * @param bool $readonly
	 * @return void
	 */
	public function setReadonly($readonly) {
		$this->_readonly = $readonly;
	}
	
	/**
	 * Get readonly
	 * 
	 * This method is used to get the readonly-setting that has been
	 * set previously with {@link M_FieldText::setReadonly()}.
	 * 
	 * @access public
	 * @return bool
	 */
	public function getReadonly() {
		return $this->_readonly;
	}
	
	/**
	 * Set hint
	 * 
	 * This method will set the hint of the field.
	 * Check the docs on property {@link M_FieldText::$_hint} for more info.
	 * 
	 * A hint will internally be stored as a title property. However, by adding
	 * javascript the hint will be displayed as a default-value. Whenever a 
	 * user clicks inside the field, or submits, this value will be removed
	 * 
	 * @access public
	 * @param string $hint
	 * @return void
	 */
	public function setHint($hint) {
		$this->_hint = $hint;
	}
	
	/**
	 * Get hint
	 * 
	 * This method is used to get the hint that has been
	 * set previously with {@link M_FieldText::getHint()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getHint() {
		return $this->_hint;
	}
	
	/**
	 * Get the temporary value
	 * 
	 * This method overrides {@link M_Field::getValue()}.
	 * 
	 * M_FieldText will clean up the temporary value, so it does not
	 * break the html attribute of the input control. For example,
	 * if the user introduces a text that holds a simple or double
	 * quote, it may break the input:
	 * 
	 * Example 1
	 * <code>
	 *    <input type="text" value="Text with " breaks the html tag">
	 * </code>
	 * 
	 * This method will solve this problem by cleaning up the
	 * temporary value. The value in Example 1 will be converted to 
	 * the following:
	 * 
	 * <code>
	 *    <input type="text" value="Text with &quot; breaks the html tag">
	 * </code>
	 * 
	 * @access public
	 * @see M_Field::getValue()
	 * @return array|boolean
	 */
	public function getValue() {
		// return htmlentities(parent::getValue(), ENT_QUOTES);
		$value = parent::getValue();
		
		// We no longer use htmlentities(). Instead, we replace "problematic"
		// characters:
		if($this->_escapeValue) {
			$value = str_replace('<', '&lt;',   $value);
			$value = str_replace('>', '&gt;',   $value);
			$value = str_replace('"', '&quot;', $value);
			$value = str_replace("'", '&#039;', $value);
		}
		
		return $value;
	}
	
	/**
	 * Deliver the final value
	 * 
	 * This method overrides {@link M_Field::deliver()}.
	 * 
	 * This method will undo the changes that have been made by 
	 * {@link M_FieldText::getValue()}, in order to return the
	 * original user-supplied value.
	 * 
	 * @access public
	 * @see M_Field::deliver()
	 * @return mixed
	 */
	public function deliver() {
		// return html_entity_decode($this->getValue(), ENT_QUOTES);
		$value = parent::getValue();
		
		// We no longer use html_entity_decode(). Instead, we replace "problematic"
		// characters:
		if($this->_escapeValue) {
			$value = str_replace('&lt;',   '<', $value);
			$value = str_replace('&gt;',   '>', $value);
			$value = str_replace('&quot;', '"', $value);
			$value = str_replace('&#039;', "'", $value);
		}

		return $value;
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldText} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldText
	 * @return M_View
	 */
	public function getInputView() {
		$view = new M_ViewFieldText($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}

	/* -- PRIVATE -- */

	/**
	 * Escape value?
	 *
	 * @return bool $flag
	 *		Returns TRUE if the value should be escaped, FALSE if not
	 */
	protected function _getEscapeValue() {
		return $this->_escapeValue;
	}

	/**
	 * Escape value?
	 *
	 * @param bool $flag
	 *		Set to TRUE if the value should be escaped, FALSE if not
	 * @return void
	 */
	protected function _setEscapeValue($flag) {
		$this->_escapeValue = (bool) $flag;
	}
}