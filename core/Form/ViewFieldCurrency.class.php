<?php
class M_ViewFieldCurrency extends M_ViewField {
	protected function getHtml() {
		$field = $this->getField();
		/* @var $field M_FieldCurrency */
		$class = array(
			'field-text',
			'field-currency',
			'currency-'.strtolower($field->getCurrency())
		);
		//add extra class when autoformat is enabled
		if ($field->isAutoFormat()) {
			$class[] ='autoformat';
		}
		//add extra class when readaonly is enabled
		if ($field->getReadonly()) {
			$class[] ='readonly';
		}
		
		$html = '<input type="text"'
			. ' name="'. $field->getId() .'"'
			. ' id="'. $this->getId() .'"'
			. ' class="'.implode(' ', $class).'"';
		if ($field->getReadonly()) {
			$html .= ' readonly="readonly"';
		}
		if($field->getWidth()) {
			$html .= ' style="width: '. $field->getWidth() .'px;"';
		}
		if($field->getHint()) {
			$html .= ' title="'.$field->getHint().'"';
		}

		$html .= ' value="'.$field->getValue().'" />';
		
		return $html;
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core-form/FieldCurrency.tpl');
	}
}