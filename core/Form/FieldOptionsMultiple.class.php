<?php
/**
 * M_FieldOptionsMultiple class
 * 
 * M_FieldOptionsMultiple, a subclass of {@link M_FieldOptions}, handles input 
 * controls that allow users to interact with a list of items. In addition to 
 * {@link M_FieldOptions}, this field allows the user to select multiple items.
 * 
 * M_FieldOptionsMultiple is an abstract class, providing with shared functionality 
 * for fields such as:
 * 
 * - {@link M_FieldSelectMultiple}
 * - {@link M_FieldCheckboxes}
 * 
 * @package Core
 */
abstract class M_FieldOptionsMultiple extends M_FieldOptions {
	const DELIVERY_ARRAY = 'array';
	const DELIVERY_STRING = 'string';
	const DEFAULT_VALUE_FORMAT_ARRAY = 'array';
	const DEFAULT_VALUE_FORMAT_STRING = 'string';
	
	/**
	 * Delivery format
	 * 
	 * @access public
	 * @return void
	 */
	protected $_deliveryFormat = self::DELIVERY_ARRAY;
	protected $_defaultValueFormat = self::DEFAULT_VALUE_FORMAT_ARRAY;
	/**
	 * List of items
	 * 
	 * This property holds the list of items.
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_items = array();
	
	/**
	 * Add field definition
	 * 
	 * This method is used by {@link M_Field::factory()}, to mount 
	 * the field with a given definition. This method overrides
	 * {@link M_Field::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>menu</code>
	 * <code>items</code>
	 * <code>options</code>
	 * 
	 * Compose the list of items that should be contained in the
	 * field. Read {@link M_FieldOptions::setItem()} for more info.
	 * 
	 * <code>deliveryFormat</code>
	 * 
	 * Set the delivery format of the field (standard array)
	 * 
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// The format in which the final value should be delivered to the
			// form by which the field is contained:
			case 'deliveryFormat':
				$this->setDeliveryFormat($definition);
				break;

			// The format of the provided default value
			case 'defaultValueFormat':
				$this->setDefaultValueFormat($definition);
				break;

			// Other properties
			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Set default value format
	 * 
	 * @access public
	 * @return void
	 */
	public function setDefaultValueFormat($format) {
		$this->_defaultValueFormat = $format;
	}
	
	/**
	 * Set delivery format of final value
	 * 
	 * @access public
	 * @return void
	 */
	public function setDeliveryFormat($format) {
		$this->_deliveryFormat = $format;
	}
	
	/**
	 * Add/Set an item
	 * 
	 * This method will add/set an item to/in the list. To add an
	 * item to the list of items, M_FieldOptions expects both a
	 * value and a label (for display).
	 * 
	 * Note that the item's label is intended for display in the 
	 * form interface, so you should probably use a translated 
	 * string. To learn more about translated strings, read:
	 * 
	 * - {@link M_Locale}
	 * - {@link M_LocaleMessageCatalog}
	 * - {@link t()}
	 * 
	 * @access public
	 * @param string $value
	 * 		The value of the item
	 * @param string $label
	 * 		The label of the item
	 * @return void
	 */
	public function setItem($value, $label) {
		$this->_items[$value] = $label;
	}
	
	/**
	 * Get items
	 * 
	 * This method will return the list of items that is contained 
	 * in the field. Note that this method will return an associative
	 * array:
	 * 
	 * <code>
	 *    Array (
	 *       [value] => [label],
	 *       [value] => [label],
	 *       [value] => [label],
	 *       ...
	 *    )
	 * </code>
	 * 
	 * Typically, this method is used to assign the list of items
	 * to the {@link M_View} class that renders the field's input 
	 * control view ({@link M_Field::getInputView()})
	 */
	public function getItems() {
		return $this->_items;
	}
	
	/**
	 * Get an item
	 * 
	 * This method will return the label of a given item. The
	 * item must be identified by providing the VALUE that has 
	 * been passed into {@link M_FieldOptions::setItem()}
	 * previously.
	 * 
	 * @access public
	 * @see M_Field::setItem()
	 * @param string $value
	 * 		The value of the item
	 * @return string
	 */
	public function getItem($value) {
		return (isset($this->_items[$value]) ? $this->_items[$value] : FALSE);
	}
	
	public function getValue() {
		$value = M_Request::getVariable($this->id, FALSE);
		if($value === FALSE) {
			$v = $this->defaultValue;
			switch($this->_defaultValueFormat) {
				case self::DEFAULT_VALUE_FORMAT_STRING:
					return explode(',', $v);
				
				default:
					return $v;
			}
		} else {
			return is_array($value) ? $value : array($value);
		}
	}
	
	/**
	 * Deliver the final value
	 * 
	 * This method overrides {@link M_Field::deliver()}.
	 * This method will deliver the set of selected options in the requested format.
	 * See {@link M_FieldOptions::setDeliveryFormat()} for more info.
	 * 
	 * @access public
	 * @see M_Field::deliver()
	 * @return mixed
	 */
	public function deliver() {
		$v = parent::deliver();
		switch($this->_deliveryFormat) {
			case self::DELIVERY_STRING:
				return (is_array($v) ? implode(',', $v) : (string) $v);
			
			case self::DELIVERY_ARRAY:
				return (!is_array($v) ? array($v) : $v);
			
			default:
				return $v;
		}
	}
}
?>