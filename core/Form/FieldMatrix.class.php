<?php
/**
 * M_FieldMatrix class
 * 
 * M_FieldMatrix, a subclass of
 * 
 * - {@link M_Field}
 * - {@link M_FieldOptions}
 * - {@link M_FieldOptionsMultiple}
 * 
 * handles the input control that allows users to input values into a matrix
 * interface. Such a matrix interface is in effect a table, in which the user
 * can introduce a value in each of the table's cells.
 * 
 * @package Core
 */
class M_FieldMatrix extends M_Field {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Rendering mode
	 * 
	 * For each cell in the matrix, you can set the rendering mode of the field
	 * that is contained in that cell. You can choose to only show the input 
	 * field in a cell, to show the basic decoration, or to render the complete 
	 * field (default decoration with title, error message, description, and so on).
	 * 
	 * This particular constant can be used to set the rendering mode of a cell,
	 * or a column, to "basic decoration".
	 * 
	 * @see M_FieldMatrix::setDefaultRenderingMode()
	 * @see M_FieldMatrix::setColumnRenderingMode()
	 * @see M_FieldMatrix::setCellRenderingMode()
	 * @access public
	 * @var string
	 */
	const RENDERING_MODE_BASIC_DECORATION = 'basic-decoration';
	
	/**
	 * Rendering mode
	 * 
	 * For each cell in the matrix, you can set the rendering mode of the field
	 * that is contained in that cell. You can choose to only show the input 
	 * field in a cell, to show the basic decoration, or to render the complete 
	 * field (default decoration with title, error message, description, and so on).
	 * 
	 * This particular constant can be used to set the rendering mode of a cell,
	 * or a column, to "input view only".
	 * 
	 * @see M_FieldMatrix::setDefaultRenderingMode()
	 * @see M_FieldMatrix::setColumnRenderingMode()
	 * @see M_FieldMatrix::setCellRenderingMode()
	 * @access public
	 * @var string
	 */
	const RENDERING_MODE_INPUT_VIEW_ONLY = 'input-view-only';
	
	/**
	 * Rendering mode
	 * 
	 * For each cell in the matrix, you can set the rendering mode of the field
	 * that is contained in that cell. You can choose to only show the input 
	 * field in a cell, to show the basic decoration, or to render the complete 
	 * field (default decoration with title, error message, description, and so on).
	 * 
	 * This particular constant can be used to set the rendering mode of a cell,
	 * or a column, to "default decorated".
	 * 
	 * @see M_FieldMatrix::setDefaultRenderingMode()
	 * @see M_FieldMatrix::setColumnRenderingMode()
	 * @see M_FieldMatrix::setCellRenderingMode()
	 * @access public
	 * @var string
	 */
	const RENDERING_MODE_DEFAULT_DECORATION = 'default-decoration';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Default error message
	 * 
	 * This property stores the "default" error message, which is shown when the
	 * field is left empty. For more information, read the docs on
	 * 
	 * - {@link M_Field::setMandatory()}
	 * - {@link M_Field::validate()}
	 * - {@link M_Field::__empty()}
	 * 
	 * NOTE:
	 * The value of this property is defaulted to "Please do not leave this
	 * field empty" (untranslated). A new value can be assigned with the
	 * method {@link M_Field::setDefaultErrorMessage()}
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_defaultErrorMessage = 'Something is missing. Please make sure to fill out all required fields.';
	
	/**
	 * The number of rows
	 *
	 * @var int
	 */
	protected $_numberOfRows = 1;
	
	/**
	 * Number of columns
	 *
	 * @var int
	 */
	protected $_numberOfColumns = 2;
	
	/**
	 * Column titles
	 *
	 * @var array
	 */
	protected $_columnTitles = array();

	/**
	 * Column titles
	 *
	 * @var array
	 */
	protected $_rowTitles = array();
	
	/**
	 * Mandatory columns
	 *
	 * @var array
	 */
	protected $_columnMandatory = array(false, false);
	
	/**
	 * Minimum number of rows
	 *
	 * @var int
	 */
	protected $_minimumNumberOfRows = 0;
	
	/**
	 * Maximum number of rows
	 *
	 * @var int
	 */
	protected $_maximumNumberOfRows = 0;
	
	/**
	 * Column widths
	 *
	 * @var array
	 */
	protected $_columnWidth = array();
	
	/**
	 * Column fields
	 *
	 * @var array
	 */
	protected $_columnField = array();
	
	/**
	 * Cell fields
	 * 
	 * A cell can hold a different field than the same cell in another row/column
	 * that's why can set the field for one specific cell
	 *
	 * @var array
	 */
	protected $_cellField = array();
	
	/**
	 * Coordinates for specific fields
	 * 
	 * @see M_FieldMatrix::hasDefaultFieldInCell()
	 * @access private
	 * @var array
	 */
	private $_fieldCoords = array();
	
	/**
	 * Full-Width Rows
	 * 
	 * @access private
	 * @var array
	 */
	private $_fullWidthRowOptions = array();
	
	/**
	 * Rendering mode: default
	 * 
	 * @see M_FieldMatrix::setDefaultRenderingMode()
	 * @access private
	 * @var string
	 */
	private $_renderingMode = 'basic-decoration';
	
	/**
	 * Rendering modes, for columns
	 * 
	 * @see M_FieldMatrix::setColumnRenderingMode()
	 * @access private
	 * @var string
	 */
	protected $_renderingModeColumn = array();
	
	/**
	 * Rendering modes, for cells
	 * 
	 * @see M_FieldMatrix::setCellRenderingMode()
	 * @access private
	 * @var string
	 */
	protected $_renderingModeCell = array();

	/**
	 * Show the error messages of all cell fields? Defaults to TRUE
	 *
	 * @var bool
	 */
	protected $_showCellFieldErrorMessages = true;
	
	/**
	 * Holds the number of rows the footer contains
	 * 
	 * @see M_FieldMatrix::hasFooter()
	 * @see M_FieldMatrix::setFooterNumberOfRows()
	 * @see M_FieldMatrix::getFooterNumberOfRows()
	 * @access private
	 * @var int
	 */
	private $_footerNumberOfRows = 0;
	
	/**
	 * Holds the row titles for the footer
	 * 
	 * This is typically an array with the row index as array key and title as
	 * array value
	 * 
	 * @see M_FieldMatrix::setFooterRowTitles()
	 * @see M_FieldMatrix::setFooterRowTitle()
	 * @access private
	 * @var array
	 */
	private $_footerRowTitles = array();
	
	/**
	 * Holds the footer cell values
	 * 
	 * This is typically an array with the combination of row index and column
	 * value as array key, and the cell contents as array value
	 * 
	 * @see M_FieldMatrix::setFooterRowValues()
	 * @see M_FieldMatrix::setFooterCellValue()
	 * @see M_FieldMatrix::getFooterCellValue()
	 * @access private
	 * @var array
	 */
	private $_footerCells = array();
	
	/* -- MAGIC FUNCTIONS -- */
	
	/**
	 * Validate this field
	 *
	 * @param mixed $value
	 * @return bool
	 */
	public function __validate($value) {
		// By default, the result is SUCCESS
		$out = TRUE;
		
		// The matrix field validates in a different way from other fields. Since
		// the matrix is a collection of contained fields, we validate each of the
		// contained fields. If all fields validate correctly, the matrix also
		// does. => For each of rows in the matrix:
		for($i = 0, $nRows = $this->getNumberOfRows(); $i < $nRows; $i ++) {
			// And, for each of the columns in the matrix:
			for($j = 0, $nColumns = $this->getNumberOfColumns(); $j < $nColumns; $j ++) {
				// Get the field that goes in the current cell:
				$cellField = $this->getCellField($j, $i);
				
				// We check if the field validates:
				if(! $cellField->validate()) {
					// If not, we set an error message
					if($out) {
						$this->setErrorMessage($this->getDefaultErrorMessage());
					}
					
					// And we set the outcome:
					$out = FALSE;
				}
			}
		}
		
		// Return the outcome:
		return $out;
	}
	
	/**
	 * Attach to {@link M_Form}
	 * 
	 * This method overrides {@link M_Field::__form()}.
	 * 
	 * When a M_FieldMatrix object is attached to a form, the field
	 * will call the {@link M_Field::__form()} method on each of the contained
	 * fields, in order to update the form properties.
	 * 
	 * @access public
	 * @see M_Field::__form()
	 * @param M_Form $form
	 * 		The form to which the field is being added.
	 * @return void
	 */
	public function __form(M_Form $form) {
		// For each of rows in the matrix:
		for($i = 0, $nRows = $this->getNumberOfRows(); $i < $nRows; $i ++) {
			// And, for each of the columns in the matrix:
			for($j = 0, $nColumns = $this->getNumberOfColumns(); $j < $nColumns; $j ++) {
				// Get the field that goes in the current cell:
				$cellField = $this->getCellField($j, $i);
				
				// Call its __form() method
				$cellField->__form($form);
			}
		}
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Global set function
	 * 
	 * The set function can accept one of following definitions:
	 * - numbersOfRows|rows
	 * - numberofColumns|columns
	 * - columnTitles|titles
	 * - mandatoryColumns
	 * - minimumNumbersOfRows|minimumRows|minRows
	 * - maximumNumbersOfRows|maximumRows|maxRows
	 * 
	 * All of these are also available as seperate setters
	 *
	 * @access public
	 * @see M_Field::factory()
	 * @see M_Field::set()
	 * @throws M_FieldException
	 * @param string $spec
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @param bool $translateStrings
	 *		Set to TRUE if you want the strings to be translated (eg. title,
	 *		description, ...), FALSE if the strings should not be translated
	 * @return void
	 */
	public function set($spec, $definition, $translateStrings = FALSE) {
		// The property that is to be set, depends on the key (property name)
		switch($spec) {
			// The number of rows in the matrix
			case 'numberOfRows':
			case 'rows':
				$this->setNumberOfRows($definition);
				break;

			// The number of columns
			case 'numberOfColumns':
			case 'columns':
				$this->setNumberOfColumns($definition);
				break;

			// The titles of the columns
			case 'columnTitles':
			case 'titles':
				// The provided definition must be an iterator. If that's not
				// the case
				if(! M_Helper::isIterator($definition)) {
					// Then, we throw an exception, to inform about the error
					throw new M_FieldException(sprintf(
						'%s expects an array/iterator from "%s"',
						__CLASS__,
						$spec
					));
				}

				// For each of titles:
				$j = 0; foreach($definition as $title) {
					// Set the column title:
					// (note that we translate this column, if requested)
					$this->setColumnTitle($j ++, $translateStrings ? t((string) $title) : $title);
				}
				break;
			case 'rowTitles':
				// The provided definition must be an iterator. If that's not
				// the case
				if(! M_Helper::isIterator($definition)) {
					// Then, we throw an exception, to inform about the error
					throw new M_FieldException(sprintf(
						'%s expects an array/iterator from "%s"',
						__CLASS__,
						$spec
					));
				}

				// For each of titles:
				$j = 0; foreach($definition as $title) {
					// Set the column title:
					// (note that we translate this column, if requested)
					$this->setRowTitle($j ++, $translateStrings ? t((string) $title) : $title);
				}
				break;
			// Mandatory column(s)
			case 'mandatoryColumns':
				if(is_string($definition)) {
					$definition = explode(',', $definition);
				}
				
				if(! M_Helper::isIterator($definition)) {
					throw new M_FieldException(sprintf(
						'%s expects an array/iterator from "%s"',
						__CLASS__,
						$spec
					));
				}
				
				foreach($definition as $i) {
					$this->setMandatoryColumn($i, TRUE);
				}
				break;
			
			case 'minimumNumberOfRows':
			case 'minimumRows':
			case 'minRows':
				$this->setMinimumNumberOfRows($definition);
				break;
			
			case 'maximumNumberOfRows':
			case 'maximumRows':
			case 'maxRows':
				$this->setMaximumNumberOfRows($definition);
				break;

			case 'showCellFieldErrorMessages':
				$this->setShowCellFieldErrorMessages($definition);
				break;

			// Definition of fields in columns/cells:
			case 'fields':
				// The provided definition must be an instance of MI_Config
				if(! M_Helper::isInterface($definition, 'MI_Config')) {
					// If not the case, then we throw an exception
					throw new M_Exception(sprintf(
						'Cannot mount default input controls for M_FieldMatrix, with definition %s',
						$definition
					));
				}

				// For each of the fields:
				foreach($definition as $identifier => $fieldSpec) {
					// We make sure that we can parse the identifier for the current
					// field. This identifier string must be 'cell-*-*' or 'column-*'
					$c1 = preg_match('/^column-([0-9]{1,})$/i', $identifier, $columnMatch);
					$c2 = preg_match('/^cell-([0-9]{1,})-([0-9]{1,})$/', $identifier, $cellMatch);

					// If none of the patterns match with the identifier
					if(! ($c1 || $c2)) {
						// Then, we throw an exception
						throw new M_Exception(sprintf(
							'Cannot recognize the column or cell for which the field ' .
							'is being defined in the matrix: %s',
							$identifier
						));
					}

					// If the field defines a column:
					if($c1) {
						// Then, we set the field for the column:
						$this->setColumnField($columnMatch[1], M_Field::factory($identifier, $fieldSpec));
					}

					// If the field defines a cell
					if($c2) {
						// Then, we define the field for the cell:
						$this->setCellField($cellMatch[1], $cellMatch[2], M_Field::factory($identifier, $fieldSpec));
					}
				}
				break;

			default:
				parent::set($spec, $definition, $translateStrings);
				break;
		}
	}
	
	/**
	 * Set the column titles
	 *
	 * @param array $arg
	 * @return void
	 */
	public function setColumnTitles(array $arg) {
		return $this->set('columnTitles', $arg);
	}

	/**
	 * Set the row titles
	 *
	 * @param array $arg
	 * @return void
	 */
	public function setRowTitles(array $arg) {
		return $this->set('rowTitles', $arg);
	}
	
	/**
	 * Set whether or not the error messages of the cell fields should be shown
	 *
	 * @param bool $arg
	 * @return void
	 */
	public function setShowCellFieldErrorMessages($arg) {
		$this->_showCellFieldErrorMessages = (bool) $arg;
	}

	/**
	 * Set the number of rows
	 *
	 * @param int $rows
	 */
	public function setNumberOfRows($rows) {
		$this->_numberOfRows = (int) $rows;
	}
	
	/**
	 * Add to number of rows
	 * 
	 * @access public
	 * @param int $number
	 * @return M_FieldMatrix
	 */
	public function addToNumberOfRows($number) {
		$this->_numberOfRows += (int) $number;
	}
	
	/**
	 * Set the number of columns
	 *
	 * @param int $columns
	 */
	public function setNumberOfColumns($columns) {
		$this->_numberOfColumns = (int) $columns;
	}
	
	/**
	 * Set one column title
	 *
	 * @param int $columnIndex
	 * @param string $title
	 */
	public function setColumnTitle($columnIndex, $title) {
		$this->_columnTitles[(int) $columnIndex] = (string) $title;
	}

	/**
	 * Set row title
	 *
	 * @param int $columnIndex
	 * @param string $title
	 */
	public function setRowTitle($rowIndex, $title) {
		$this->_rowTitles[(int) $rowIndex] = (string) $title;
	}
	
	/**
	 * Set the mandatory columns
	 *
	 * Array with column-index as key and flag as value
	 * 
	 * <code>
	 * $mandatory = array(0 => true, 2 => true, 3 => false);
	 * $field->setMandatoryColumns($mandatory);
	 * </code>
	 * 
	 * @param array $columns
	 */
	public function setMandatoryColumns(array $columns) {
		foreach($columns as $i => $flag) {
			$this->setMandatoryColumn($i, $flag);
		}
	}
	
	/**
	 * Mark one column as (not) mandatory 
	 *
	 * @param int $columnIndex
	 * @param bool $flag
	 */
	public function setMandatoryColumn($columnIndex, $flag) {
		$this->_columnMandatory[(int) $columnIndex] = (bool) $flag;
	}
	
	/**
	 * Set the minimum number of rows
	 *
	 * @param int $number
	 */
	public function setMinimumNumberOfRows($number) {
		$this->_minimumNumberOfRows = (int) $number;
	}
	
	/**
	 * Set the maximum number of rows
	 *
	 * @param int $number
	 */
	public function setMaximumNumberOfRows($number) {
		$this->_maximumNumberOfRows = (int) $number;
	}
	
	/**
	 * Set the column widths
	 *
	 * Array with column-index as key and flag as value
	 * 
	 * <code>
	 * $mandatory = array(0 => 100, 1 => 50, 3 => 50);
	 * $field->setColumnWidths($mandatory);
	 * </code>
	 * 
	 * @param array $widths
	 */
	public function setColumnWidths(array $widths) {
		foreach($widths as $i => $width) {
			$this->setColumnWidth($i, $width);
		}
	}
	
	/**
	 * Set the width for one specific column
	 *
	 * @param int $columnIndex
	 * @param int $width
	 */
	public function setColumnWidth($columnIndex, $width) {
		$this->_columnWidth[(int) $columnIndex] = (int) $width;
	}
	
	/**
	 * Set the field for one specific colunm
	 *
	 * @param int $columnIndex
	 * @param M_Field $field
	 */
	public function setColumnField($columnIndex, M_Field $field) {
		$this->_columnField[(int) $columnIndex] = $field;
		$this->_fieldCoords[] = (int) $columnIndex . '-null';
	}
	
	/**
	 * Set the field for one specific cell
	 *
	 * @param int $columnIndex
	 * @param int $rowIndex
	 * @param M_Field $field
	 */
	public function setCellField($columnIndex, $rowIndex, M_Field $field) {
		$fieldKey = (int) $columnIndex . '-' . (int) $rowIndex;
		$this->_cellField[$fieldKey] = $field;
		$this->_fieldCoords[] = $fieldKey;
	}
	
	/**
	 * Set Full-Width Row, Before...
	 * 
	 * This method allows you to insert a full-width row, at a given position
	 * in the matrix. The position is indicated by providing the row index at which
	 * the full-width row is to be prepended. A full-width row is a row that 
	 * contains only one column, spanning the width of all other columns together.
	 * 
	 * @access public
	 * @param int $rowIndex
	 * @param M_ViewHtml|string $html
	 * @return M_FieldMatrix $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function addFullWidthRowBefore($rowIndex, $html) {
		// If no full-width rows have been added previously
		if(! array_key_exists($rowIndex, $this->_fullWidthRowOptions)) {
			// Then, initiate the property now:
			$this->_fullWidthRowOptions[$rowIndex] = array();
		}
		
		// Add the HTML:
		array_push($this->_fullWidthRowOptions[$rowIndex], (string) $html);
		
		// Return myself:
		return $this;
	}
	
	/**
	 * Set default rendering mode
	 * 
	 * With this method, you can set the default rendering mode of fields in the
	 * matrix. For more information about rendering modes, read the docs on
	 * 
	 * - {@link M_FieldMatrix::setDefaultRenderingMode()}
	 * - {@link M_FieldMatrix::setColumnRenderingMode()}
	 * - {@link M_FieldMatrix::setCellRenderingMode()}
	 * 
	 * @see M_FieldMatrix::RENDERING_MODE_BASIC_DECORATION
	 * @see M_FieldMatrix::RENDERING_MODE_INPUT_VIEW_ONLY
	 * @see M_FieldMatrix::RENDERING_MODE_DEFAULT_DECORATION
	 * @access public
	 * @param string $mode
	 * @param M_FieldMatrix $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDefaultRenderingMode($mode) {
		// Make sure the rendering mode is recognized:
		$this->_doCheckRenderingMode($mode);
		
		// Set the rendering mode:
		$this->_renderingMode = $mode;
	}
	
	/**
	 * Set rendering mode for a column
	 * 
	 * For each column in the matrix, you can set the rendering mode of the fields
	 * that are contained in the cells of that column. You can choose to only show 
	 * the input field, to show the basic decoration, or to render the complete 
	 * field (default decoration with title, error message, description, and so on).
	 * 
	 * Note that you can still specify a rendering mode for each individual cell,
	 * by using {@link M_FieldMatrix::setCellRenderingMode()}. If the rendering
	 * mode is set for a specific cell, it will overwrite the rendering mode that
	 * is specified for the corresponding column.
	 * 
	 * @see M_FieldMatrix::RENDERING_MODE_BASIC_DECORATION
	 * @see M_FieldMatrix::RENDERING_MODE_INPUT_VIEW_ONLY
	 * @see M_FieldMatrix::RENDERING_MODE_DEFAULT_DECORATION
	 * @access public
	 * @param int $columnIndex
	 * @param string $mode
	 * @param M_FieldMatrix $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setColumnRenderingMode($columnIndex, $mode) {
		// Make sure the rendering mode is recognized:
		$this->_doCheckRenderingMode($mode);
		
		// Set the rendering mode:
		$this->_renderingModeColumn[(int) $columnIndex] = $mode;
	}
	
	/**
	 * Set rendering mode for a cell
	 * 
	 * For each cell in the matrix, you can set the rendering mode of the field
	 * contained. You can choose to only show the input field, to show the basic 
	 * decoration, or to render the complete field (default decoration with title, 
	 * error message, description, and so on).
	 * 
	 * @see M_FieldMatrix::RENDERING_MODE_BASIC_DECORATION
	 * @see M_FieldMatrix::RENDERING_MODE_INPUT_VIEW_ONLY
	 * @see M_FieldMatrix::RENDERING_MODE_DEFAULT_DECORATION
	 * @access public
	 * @param int $columnIndex
	 * @param int $rowIndex
	 * @param string $mode
	 * @param M_FieldMatrix $field
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCellRenderingMode($columnIndex, $rowIndex, $mode) {
		// Make sure the rendering mode is recognized:
		$this->_doCheckRenderingMode($mode);
		
		// Set the rendering mode:
		$this->_renderingModeCell[(int) $columnIndex . '-' . (int) $rowIndex] = $mode;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get the number of rows
	 * 
	 * @return int
	 */
	public function getNumberOfRows() {
		return $this->_numberOfRows;
	}
	
	/**
	 * Get the number of columns
	 *
	 * @return int
	 */
	public function getNumberOfColumns() {
		return $this->_numberOfColumns;
	}

	/**
	 * Get the column title
	 *
	 * @param int $columnIndex
	 * @return string|null
	 */
	public function getColumnTitle($columnIndex) {
		return (
			isset($this->_columnTitles[(int) $columnIndex])
				? $this->_columnTitles[(int) $columnIndex]
				: NULL
		);
	}

	/**
	 * Get the row title
	 *
	 * @param int $columnIndex
	 * @return string|null
	 */
	public function getRowTitle($columnIndex) {
		return (
			isset($this->_rowTitles[(int) $columnIndex])
				? $this->_rowTitles[(int) $columnIndex]
				: NULL
		);
	}
	
	/**
	 * Get all column titles
	 *
	 * @return ArrayIterator
	 */
	public function getColumnTitles() {
		// Prepare an empty (output) array:
		$out = array();
		
		// For each of the column titles the field is supposed to have:
		for($i = 0, $n = $this->getNumberOfColumns(); $i < $n; $i ++) {
			// Get the current column's title:
			$out[$i] = $this->getColumnTitle($i);
		}
		
		// Return the collection of column titles:
		return new ArrayIterator($out);
	}

	/**
	 * Get all row titles
	 *
	 * @return ArrayIterator
	 */
	public function getRowTitles() {
		// Prepare an empty (output) array:
		$out = array();

		// For each of the column titles the field is supposed to have:
		for($i = 0, $n = $this->getNumberOfRows(); $i < $n; $i ++) {
			// Get the current column's title:
			$out[$i] = $this->getRowTitle($i);
		}

		// Return the collection of column titles:
		return new ArrayIterator($out);
	}
	
	/**
	 * Has row titles?
	 * 
	 * @access public
	 * @return bool $flag
	 *		Returns TRUE if at least one row title has been defined, FALSE if not
	 */
	public function hasRowTitles() {
		return (count($this->_rowTitles) + count($this->_footerRowTitles)) > 0;
	}
	
	/**
	 * Has column titles?
	 * 
	 * @access public
	 * @return bool $flag
	 *		Returns TRUE if at least one column title has been defined, FALSE if not
	 */
	public function hasColumnTitles() {
		return (count($this->_columnTitles) > 0);
	}
	
	/**
	 * Check if a column is mandatory
	 *
	 * @param int $columnIndex
	 * @return bool
	 */
	public function isMandatoryColumn($columnIndex) {
		return (
			isset($this->_columnMandatory[(int) $columnIndex])
				? $this->_columnMandatory[(int) $columnIndex]
				: FALSE
		);
	}
	
	/**
	 * Get the minimum number of rows
	 *
	 * @return int
	 */
	public function getMinimumNumberOfRows() {
		return $this->_minimumNumberOfRows;
	}
	
	/**
	 * Get the maximum number of rows
	 *
	 * @return int
	 */
	public function getMaximumNumberOfRows() {
		return $this->_maximumNumberOfRows;
	}
	
	/**
	 * Get the width for all columns
	 *
	 * @return ArrayIterator
	 */
	public function getColumnWidths() {
		// Prepare an empty (output) array:
		$out = array();
		
		// For each of the column widths the field is supposed to have:
		for($i = 0, $n = $this->getNumberOfColumns(); $i < $n; $i ++) {
			// Get the current column's width:
			$out[$i] = $this->getColumnWidth($i);
		}
		
		// Return the collection of column widths:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get the width for one column
	 *
	 * @param int $columnIndex
	 * @return int|null
	 */
	public function getColumnWidth($columnIndex) {
		return (
			isset($this->_columnWidth[(int) $columnIndex])
				? $this->_columnWidth[(int) $columnIndex]
				: NULL
		);
	}
	
	/**
	 * Get the field for one cell
	 *
	 * @param int $columnIndex
	 * @param int $rowIndex
	 * @return M_Field
	 */
	public function getCellField($columnIndex, $rowIndex) {
		// Get the key to look up the field:
		$fieldKey = (int) $columnIndex . '-' . (int) $rowIndex;
		
		// Check if the requested field exists:
		if(! isset($this->_cellField[$fieldKey])) {
			// If not, we create a field, based on the column:
			$this->_cellField[$fieldKey] = $this->_getCellFieldFromColumn($columnIndex, $rowIndex);
		}
		
		// Return the field:
		return $this->_cellField[$fieldKey];
	}
	
	/**
	 * Has default field in cell?
	 * 
	 * Will tell whether or not a default field is rendered in a given cell. Such
	 * a default field is rendered, when a specific field is NOT assigned to the 
	 * cell with one of the following methods:
	 * 
	 * - {@link M_FieldMatrix::setCellField()}
	 * - {@link M_FieldMatrix::setColumnField()}
	 * 
	 * @access public
	 * @param int $columnIndex
	 * @param int $rowIndex
	 * @return bool $flag
	 *		Returns TRUE if default field is rendered, FALSE if not
	 */
	public function hasDefaultFieldInCell($columnIndex, $rowIndex) {
		// Get the key to look up the field:
		$fieldKey = (int) $columnIndex . '-' . (int) $rowIndex;
		
		// If a field is set with these coordinates:
		if(in_array($fieldKey, $this->_fieldCoords)) {
			// Then, we return FALSE to indicate that no default field is being
			// used for the cell:
			return FALSE;
		}
		
		// If still here, we did not find a field with the coordinates. However,
		// it possible that the field is defined for the entire column instead 
		// of the specific cell
		if(in_array((int) $columnIndex . '-null', $this->_fieldCoords)) {
			// If so, we return FALSE to indicate that no default field is being
			// used for the cell:
			return FALSE;
		}
		
		// Return TRUE, if still here. This indicates a default field in the cell:
		return TRUE;
	}
	
	/**
	 * Get Full-Width Row, Before...
	 * 
	 * @see M_FieldMatrix::addFullWidthRowBefore()
	 * @access public
	 * @param int $rowIndex
	 * @return M_ArrayIterator
	 */
	public function getFullWidthRowBefore($rowIndex) {
		// If existing:
		if(array_key_exists($rowIndex, $this->_fullWidthRowOptions)) {
			// Then, return the full-width rows:
			return new M_ArrayIterator($this->_fullWidthRowOptions[$rowIndex]);
		}
		
		// If not defined, return empty collection
		return new M_ArrayIterator(array());
	}
	
	/**
	 * Get default rendering mode
	 * 
	 * @see M_FieldMatrix::setDefaultRenderingMode()
	 * @access public
	 * @return void
	 */
	public function getDefaultRenderingMode() {
		return $this->_renderingMode;
	}
	
	/**
	 * Get rendering mode for a column
	 * 
	 * Will return the rendering mode that has been set for a specific column in
	 * the matrix. Note that, if no mode has been set for the requested column, 
	 * this method will return the default rendering mode instead.
	 * 
	 * NOTE:
	 * Use {@link M_FieldMatrix::setDefaultRenderingMode()} to set the default
	 * rendering mode.
	 * 
	 * @see M_FieldMatrix::setColumnRenderingMode()
	 * @access public
	 * @param integer $columnIndex
	 * @return void
	 */
	public function getColumnRenderingMode($columnIndex) {
		// Cast the provided index to an integer:
		$columnIndex = (int) $columnIndex;
		
		// Return the rendering mode
		return (
			array_key_exists($columnIndex, $this->_renderingModeColumn)
				? $this->_renderingModeColumn[$columnIndex]
				: $this->getDefaultRenderingMode()
		);
	}
	
	/**
	 * Get rendering mode for a cell
	 * 
	 * Will return the rendering mode that has been set for a specific cell in
	 * the matrix. Note that, if no mode has been set for the requested cell, 
	 * this method will return the rendering mode of the corresponding column
	 * instead.
	 * 
	 * @see M_FieldMatrix::setCellRenderingMode()
	 * @access public
	 * @param integer $columnIndex
	 * @param integer $rowIndex
	 * @return void
	 */
	public function getCellRenderingMode($columnIndex, $rowIndex) {
		// Compose the key to look up the rendering mode:
		$key = (int) $columnIndex . '-' . (int) $rowIndex;
		
		// Return the rendering mode
		return (
			array_key_exists($key, $this->_renderingModeCell)
				? $this->_renderingModeCell[$key]
				: $this->getColumnRenderingMode($columnIndex)
		);
	}
	
	/**
	 * Is rendering mode "basic decoration"?
	 * 
	 * @access public
	 * @param integer $columnIndex
	 * @param integer $rowIndex
	 * @return bool $flag
	 *		Returns TRUE if so, FALSE if not
	 */
	public function isCellRenderingModeBasicDecoration($columnIndex, $rowIndex) {
		return ($this->getCellRenderingMode($columnIndex, $rowIndex) == self::RENDERING_MODE_BASIC_DECORATION);
	}
	
	/**
	 * Is rendering mode "input view only"?
	 * 
	 * @access public
	 * @param integer $columnIndex
	 * @param integer $rowIndex
	 * @return bool $flag
	 *		Returns TRUE if so, FALSE if not
	 */
	public function isCellRenderingModeInputViewOnly($columnIndex, $rowIndex) {
		return ($this->getCellRenderingMode($columnIndex, $rowIndex) == self::RENDERING_MODE_INPUT_VIEW_ONLY);
	}
	
	/**
	 * Is rendering mode "default decoration"?
	 * 
	 * @access public
	 * @param integer $columnIndex
	 * @param integer $rowIndex
	 * @return bool $flag
	 *		Returns TRUE if so, FALSE if not
	 */
	public function isCellRenderingModeDefaultDecoration($columnIndex, $rowIndex) {
		return ($this->getCellRenderingMode($columnIndex, $rowIndex) == self::RENDERING_MODE_DEFAULT_DECORATION);
	}

	/**
	 * Show the error messages of all the cell fields to (if there are any)
	 *
	 * @return bool
	 */
	public function getShowCellFieldErrorMessages() {
		return $this->_showCellFieldErrorMessages;
	}
	
	/**
	 * Does the matrix contain a footer?
	 * 
	 * @access public
	 * @return bool
	 *		Returns TRUE if footer rows are present, FALSE if not
	 */
	public function hasFooter() {
		return ($this->_footerNumberOfRows > 0);
	}
	
	/**
	 * Set the number of footer rows
	 * 
	 * This setter will determine how many rows will be rendered. 
	 * 
	 * NOTE: the number of rows is not linked to whether the rows actually
	 * contain content. For example: if you set the number of footer rows to 5,
	 * the template will always render 5 rows, whether they contain content or
	 * not. The big difference with the regular matrix cells is that empty
	 * footer cells will be rendered as a simply empty <td>, rather than contain
	 * an {@link M_FieldText}. This allows for very flexible use of the footer.
	 * 
	 * @access public
	 * @param int $n
	 *		The number of footer rows you want to render
	 * @return M_FieldMatrix
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFooterNumberOfRows($n) {
		$this->_footerNumberOfRows = (int) $n;
		return $this;
	}
	
	/**
	 * Get the number of footer rows that are to be rendered
	 * 
	 * @see M_FieldMatrix::setFooterNumberOfRows
	 * @access public
	 * @return int
	 */
	public function getFooterNumberOfRows() {
		return $this->_footerNumberOfRows;
	}
	
	/**
	 * Set the row titles for the footer
	 * 
	 * Allows for setting the title for each row of the footer. This is
	 * typically done by providing an array where the array key represents
	 * the row index and the array value contains the title itself. For example:
	 * 
	 * <code>
	 *		array(
	 *			0 => 'Title for the first row',
	 *			3 => 'Title for the 4th row'
	 *		)
	 * </code>
	 * 
	 * Note that you don't have to define a title for every row. A <td> will
	 * be rendered for every row regardless, but will only be filled up if a
	 * title is defined for it.
	 * 
	 * NOTE: because row iterations always start at ZERO, providing array keys
	 * for the row indexes is entirely optional if all rows follow properly
	 * on one another. For example:
	 * 
	 * <code>
	 *		array(
	 *			'Title for the first row',
	 *			'Title for the second row',
	 *			'Title for the third row'
	 *		)
	 * </code>
	 * 
	 * @see M_FieldMatrix::setFooterRowTitle()
	 * @see M_FieldMatrix::getFooterRowTitle()
	 * @access public
	 * @param array $titles
	 *		The array of row titles, with the row index as array key and the
	 *		title for the row as array value
	 * @return M_FieldMatrix
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFooterRowTitles(array $titles) {
		// For each given row index / title combination
		foreach($titles as $rowIndex => $title) {
			// Set the title, using the designated method
			$this->setFooterRowTitle($rowIndex, $title);
		}
		
		// Return self
		return $this;
	}
	
	/**
	 * Set a row title for a footer row
	 * 
	 * Allows for defining the title of a certain row of the footer, defined
	 * by its row index. Additionally, read the docs on
	 * {@link M_FieldMatrix::setFooterRowTitles()} for all possiblities.
	 * 
	 * @see M_FieldMatrix::setFooterRowTitles()
	 * @see M_FieldMatrix::getFooterRowTitle()
	 * @access public
	 * @param int $rowIndex
	 *		The row index, starting at ZERO
	 * @param string $title
	 *		The title of the row
	 * @return M_FieldMatrix
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFooterRowTitle($rowIndex, $title) {
		// Set the title in the property with the row index as array key
		$this->_footerRowTitles[$rowIndex] = $title;
		
		// Return self
		return $this;
	}
	
	/**
	 * Get the row title for a footer row
	 * 
	 * @see M_FieldMatrix::setFooterRowTitles()
	 * @see M_FieldMatrix::setFooterRowTitle()
	 * @access public
	 * @param int $rowIndex
	 *		The row index, starting at ZERO
	 * @return string
	 *		Returns the title if it is given, or an empty string otherwise
	 */
	public function getFooterRowTitle($rowIndex) {
		return M_Helper::getArrayElement($rowIndex, $this->_footerRowTitles, '');
	}
	
	/**
	 * Set the values of a footer row
	 * 
	 * Allows for setting the cell contents a given row of the footer. This is
	 * typically done by providing an array where the array key represents
	 * the column index and the array value contains the content itself.
	 * For example:
	 * 
	 * <code>
	 *		array(
	 *			0 => 'Content of the first column cell of the row',
	 *			3 => '<span id="something">Content of the 4th column cell of the row</span>'
	 *		)
	 * </code>
	 * 
	 * Note that you don't have to define a value for every column. A <td> will
	 * be rendered for every cell regardless, but will only be filled up if
	 * content is defined for it.
	 * 
	 * NOTE: because column iterations always start at ZERO, providing array
	 * keys for the column indexes is entirely optional if all columns follow
	 * properly on one another. For example:
	 * 
	 * <code>
	 *		array(
	 *			'Content of the cell in the first column',
	 *			'Content of the cell in the 2nd column',
	 *			'Content of the cell in the 3rd column'
	 *		)
	 * </code>
	 * 
	 * @see M_FieldMatrix::setFooterCellValue()
	 * @see M_FieldMatrix::getFooterCellValue()
	 * @access public
	 * @param int $rowIndex
	 *		The row index, starting at ZERO
	 * @param array values
	 *		The array of column values, with the column index as array key and
	 *		the content for the cell as array value
	 * @return M_FieldMatrix
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFooterRowValues($rowIndex, array $values) {
		// For each of the column index / value combinations of the row
		foreach($values as $columnIndex => $value) {
			// Add the cell value, using the designated method
			$this->setFooterCellValue($rowIndex, $columnIndex, $value);
		}
		
		// Return self
		return $this;
	}
	
	/**
	 * Set the contents of a single cell of the footer
	 * 
	 * Allows for setting the content of a footer cell using the combination
	 * of the row index / column index. Additionally, read the docs on
	 * {@link M_FieldMatrix::setFooterRowValues()} for all possiblities.
	 * 
	 * @see M_FieldMatrix::setFooterRowValues()
	 * @see M_FieldMatrix::getFooterCellValue()
	 * @access public
	 * @param int $rowIndex
	 *		The row index, starting at ZERO
	 * @param int $columnIndex
	 *		The column index, starting at ZERO
	 * @param string $value
	 *		The content of the cell. This can be a text string, some markup, ...
	 * @return M_FieldMatrix
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFooterCellValue($rowIndex, $columnIndex, $value) {
		// Use the row index / column index combination as array key
		$this->_footerCells[$rowIndex . '-' . $columnIndex] = $value;
		
		// Return self
		return $this;
	}
	
	/**
	 * Get the contents of a single footer cell
	 * 
	 * @see M_FieldMatrix::setFooterRowValues()
	 * @see M_FieldMatrix::setFooterCellValue()
	 * @access public
	 * @param int $rowIndex
	 *		The row index, starting at ZERO
	 * @param int $columnIndex
	 *		The column index, starting at ZERO
	 * @return string
	 *		Returns the cell's content if it is given, or an empty string otherwise
	 */
	public function getFooterCellValue($rowIndex, $columnIndex) {
		return M_Helper::getArrayElement($rowIndex . '-' . $columnIndex, $this->_footerCells, '');
	}
	
	/* -- M_Field IMPLEMENTATIONS -- */
	
	public function getValue() {
		return 'matrix';
	}
	
	public function deliver() {
		// We deliver an array that holds the values of the contained fields. We
		// prepare an empty array, in which to store the values:
		$out = array();
		
		// For each of the rows in the matrix:
		for($i = 0, $nRows = $this->getNumberOfRows(); $i < $nRows; $i ++) {
			// Prepare an entry for the row:
			$out[$i] = array();
			
			// And, for each of the columns in the matrix:
			for($j = 0, $nColumns = $this->getNumberOfColumns(); $j < $nColumns; $j ++) {
				// Get the field that goes in the current cell:
				$cellField = $this->getCellField($j, $i);
				// Get the value from the field, and save it into the output array:
				$out[$i][$j] = $cellField->deliver();
			}
		}
		
		// Finally, deliver the array as final value
		return $out;
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldMatrix} object, which is used
	 * as the view of the input control, in the view that is returned by 
	 * {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @see M_ViewFieldMatrix
	 * @return M_ViewFieldMatrix
	 */
	public function getInputView() {
		$view = new M_ViewFieldMatrix($this);
		$view->setModuleOwner($this->getModuleOwner());
		return $view;
	}
	
	/**
	 * Set the default value
	 *
	 * @param array $value
	 */
	public function setDefaultValue(array $values) {
		foreach($values AS $rowIndex => $row) {
			foreach($row AS $columnIndex => $value) {
				$this->getCellField($columnIndex, $rowIndex)->setDefaultValue($value);
			}
		}
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get field for a column in the matrix
	 * 
	 * @access protected
	 * @param type $columnIndex
	 * @param type $rowIndex
	 * @return M_Field $field
	 */
	protected function _getCellFieldFromColumn($columnIndex, $rowIndex) {
		// Cast the provided column index number to an integer:
		$columnIndex = (int) $columnIndex;
		
		// Prepare the ID of the field:
		$fieldId = $this->getId() . '-' . $columnIndex . '-' . (int) $rowIndex;
		
		// If the requested field exists:
		if(isset($this->_columnField[$columnIndex])) {
			// Get a clone:
			$field = clone($this->_columnField[$columnIndex]);
			$field->setId($fieldId);
		}
		// If the requested field does not exist:
		else {
			// Mount a text field (which is the default field)
			$field = new M_FieldText($fieldId);
		}
		
		// Copy some properties, before returning:
		// - Is Mandatory?
		if (!$field->isMandatory()) {
			$field->setMandatory((
				// if the column is mandatory
				$this->isMandatoryColumn($columnIndex) &&
				// If the row index number is in range of minimum # of rows
				$rowIndex < $this->getMinimumNumberOfRows()
			));
		}
		// - Module Owner
		$field->setModuleOwner($this->getModuleOwner());
		
		// Return the field:
		return $field;
	}
	
	/**
	 * Is valid rendering method
	 * 
	 * Will make sure the provided rendering mode is valid (recognized). If not,
	 * an exception is thrown by this method, to inform about the invalid rendering
	 * mode. This method is used by the setters of rendering modes, in order to
	 * make sure that the provided mode is valid.
	 * 
	 * @throws M_FieldException
	 * @access protected
	 * @param string $mode
	 * @return void
	 */
	protected function _doCheckRenderingMode($mode) {
		// If the mode provided is not recognized
		if(! in_array($mode, array(
			self::RENDERING_MODE_BASIC_DECORATION,
			self::RENDERING_MODE_INPUT_VIEW_ONLY,
			self::RENDERING_MODE_DEFAULT_DECORATION
		))) {
			// Then, we throw an exception
			throw new M_Exception(sprintf(
				'Invalid rendering mode %s; Rendering mode should be either %s, ' . 
				'%s or %s.',
				$mode,
				self::RENDERING_MODE_INPUT_VIEW_ONLY,
				self::RENDERING_MODE_BASIC_DECORATION,
				self::RENDERING_MODE_DEFAULT_DECORATION
			));
		}
	}
}