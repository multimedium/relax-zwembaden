<?php
class M_PagejumperView extends M_ViewCoreHtml {
	protected $_pagejumper;
	
	public function setPagejumper(M_Pagejumper $pagejumper) {
		$this->assign('pagejumper', $pagejumper);
	}
	
	protected function getHtml() {
		return '<div id="'. $this->getId() .'">pagejumper</div>';
	}
	
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core/Pagejumper.tpl');
	}
}