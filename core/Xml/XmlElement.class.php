<?php
/**
 * M_XmlElement
 *
 * M_XmlElement, a subclass of {@link SimpleXmlElement}, represents an element
 * in an XML document.
 *
 * @package Core
 */
class M_XmlElement extends SimpleXMLElement {

	/* -- CONSTRUCTORS -- */

	/**
	 * Construct with file
	 *
	 * @static
	 * @access public
	 * @param MI_FsItemFile $file
	 * @param int $options
	 * @param string $namespace
	 * @return M_XmlElement
	 */
	public static function constructWithFile(MI_FsItemFile $file, $options = NULL, $namespace = NULL) {
		return self::_getXmlFromString($file->getContents(), $options, $namespace);
	}

	/**
	 * Construct with string
	 *
	 * @static
	 * @access public
	 * @param string $string
	 * @param int $options
	 * @param string $namespace
	 * @return M_XmlElement
	 */
	public static function constructWithString($string, $options = NULL, $namespace = NULL) {
		return self::_getXmlFromString($string, $options, $namespace);
	}

	/**
	 * Construct with URI
	 *
	 * @static
	 * @access public
	 * @param M_Uri $uri
	 * @param int $options
	 * @param string $namespace
	 * @return M_XmlElement
	 */
	public static function constructWithUri(M_Uri $uri, $options = NULL, $namespace = NULL) {
		// We get a session, with the provided URI
		$session = $uri->getSession();

		// We make sure that the URI exists:
		if(! $session->uriExists()) {
			// If not, we throw an exception
			throw new M_XmlException(sprintf(
				'Cannot construct an instance of %s with URI %s; URI cannot be ' .
				'found; HTTP Response Code %s',
				__CLASS__,
				$uri->toString(),
				$session->getHttpCode()
			));
		}

		// Parse the XML:
		return self::_getXmlFromString($session->getContents(), $options, $namespace);
	}

	/* -- GETTERS -- */

	/**
	 * Get tag name
	 *
	 * This method will provide with the element's tag name. This method is an alias
	 * for {@link SimpleXmlElement::getName()}
	 *
	 * @access public
	 * @return string
	 */
	public function getTagName() {
		return $this->getName();
	}

	/**
	 * Get attributes
	 *
	 * This method will provide with the element's attributes, in the form of
	 * an associative array
	 *
	 * @access public
	 * @return array
	 */
	public function getAttributes() {
		// The attributes() function returns an instance of SimpleXMLElement, which
		// is quite strange. We will convert it to an array:
		$out = array();

		// For each of the attributes:
		foreach($this->attributes() as $name => $value) {
			// Add the attribute value to the output
			$out[(string) $name] = (string) $value;
		}

		// Return the array
		return $out;
	}

	/**
	 * Get attribute value
	 *
	 * This method will provide with the value of the requested attribute
	 *
	 * @access public
	 * @param string $name
	 * @param mixed $defaultValue
	 * @return array()
	 */
	public function getAttribute($name, $defaultValue = NULL) {
		// Get the attributes:
		$temp = $this->getAttributes();

		// Return the value of the requested attribute:
		return (array_key_exists($name, $temp) ? $temp[$name] : $defaultValue);
	}

	/**
	 * Has attributes?
	 *
	 * @access public
	 * @param string $name
	 * @return boolean 
	 */
	public function hasAttributes() {
		return (count($this->getAttributes()) > 0);
	}

	/**
	 * Has attribute?
	 *
	 * @access public
	 * @param string $name
	 * @return boolean 
	 */
	public function hasAttribute($name) {
		// Get the attributes:
		$temp = $this->getAttributes();

		// Return the value of the requested attribute:
		return array_key_exists($name, $temp);
	}

	/**
	 * Has value?
	 *
	 * This method will tell whether or not the XML element has a (non-empty) value.
	 * Note that, if the element has children elements, the result of this method
	 * will always be (boolean) FALSE.
	 *
	 * @access public
	 * @return boolean $flag
	 *		Returns TRUE if the element has a (non-empty) value, FALSE if not
	 */
	public function hasValue() {
		// Get the value:
		$value = $this->getValue();

		// Is a value available?
		return (! is_null($value) && ! empty($value));
	}

	/**
	 * Get value
	 *
	 * This method will return the value of the XML element/node. Of course, if
	 * the XML element has children, this method will return NULL instead.
	 *
	 * @access public
	 * @return mixed $value
	 */
	public function getValue() {
		return ($this->hasChildren() ? NULL : (string) $this);
	}

	/**
	 * Get value of child element
	 *
	 * This method will return the value of a given child element, which is being
	 * selected by name. If no value could have been found, this method will return
	 * the default value instead.
	 *
	 * @access public
	 * @param string $name
	 *		The name of the child element
	 * @param mixed $defaultValue
	 *		The default value to be returned, if no value could be found for the
	 *		requested xml element.
	 * @return mixed $value
	 */
	public function getValueOfChild($name, $defaultValue = NULL) {
		// If the element does not have children:
		if(! $this->hasChildren()) {
			// Then, we return the default value instead:
			return $defaultValue;
		}

		// Check if the requested child can be found:
		if( isset($this->$name) && M_Helper::isInstanceOf($this->$name, 'M_XmlElement')) {
			// If the requested child has children, then it cannot have a value.
			if($this->$name->hasChildren()) {
				// In that case, we return the default value instead:
				return $defaultValue;
			}
			
			// If still here, we ask for the value of the child element:
			return $this->$name->getValue();
		}

		// Return default value, if still here
		return $defaultValue;
	}

	/**
	 * Get element at ...
	 *
	 * Will select exactly one instance of {@link M_XmlElement} that matches the
	 * provided xpath expression.
	 * 
	 * IMPORTANT NOTE:
	 * If no element could be found, or if multiple elements match the expression,
	 * then this method will return NULL instead.
	 *
	 * @access public
	 * @param xpath $xpath
	 * @return M_XmlElement $element
	 */
	public function getOneWithXpath($xpath) {
		// Run the xpath expression:
		$rs = $this->xpath($xpath);
		
		// If we failed to run the expression:
		if(! $rs) {
			// Then, we throw an exception
			throw new M_Exception(sprintf(
				'Error: Could not run xpath expression "%s" (evaluation failed?)',
				$xpath
			));
		}

		// We expect exactly one element. If our result set contains exactly that:
		if(count($rs) == 1) {
			// Return the element:
			return array_shift($rs);
		}

		// If still here, return NULL
		return NULL;
	}

	/**
	 * Has children?
	 *
	 * This method will tell whether or not the XML element has child elements.
	 *
	 * @access public
	 * @return boolean $flag
	 *		Returns TRUE if child elements exist, FALSE if not
	 */
	public function hasChildren() {
		return (count($this->children()) > 0);
	}

	/**
	 * Convert to (associative) array
	 *
	 * This method will convert the XML Element into an array. Note that this
	 * method will create an array for children that carry the same name, and that
	 * this method will add an "@" entry to the arrays for attributes, if any.
	 *
	 * @uses M_XmlElement::_getArrayFromXml()
	 * @access public
	 * @return array
	 */
	public function toArray() {
		return $this->_getArrayFromXml($this);
	}

	/**
	 * Convert to string
	 *
	 * This method will convert the XML Element into a valid XML source string.
	 *
	 * @uses M_XmlElement::_getSourceCodeStringFromXml()
	 * @access public
	 * @return string
	 */
	public function  toXmlSourceCodeString($includeXmlHeaders = TRUE, $headerCharacterEncoding = 'UTF-8') {
		// Output
		$out = '';

		// Include the XML Header?
		if($includeXmlHeaders) {
			// Then, include the header:
			$out .= '<?xml version="1.0" encoding="'. strtoupper($headerCharacterEncoding) .'"?>';
		}

		// Render the source code:
		$out .= $this->_getSourceCodeStringFromXml($this, 0);

		// Return the output
		return $out;
	}

	/**
	 * Convert to {@link M_Config}
	 *
	 * This method will convert the XML Element to an instance of {@link MI_Config}
	 *
	 * @access public
	 * @return MI_Config
	 */
	public function toConfig() {
		return $this->_getConfigFromXml($this);
	}

	/**
	 * Flush the XML (echo)
	 *
	 * @access public
	 * @return void
	 */
	public function flush() {
		// Send the XML Header:
		M_Header::send(M_Header::CONTENT_TYPE_XML);

		// Print the XML Source Code:
		echo $this->toXmlSourceCodeString();
	}

	/* -- SETTERS -- */
	
	/**
	 * Set value
	 *
	 * This method will set the value of the XML Element. Note that, when setting
	 * a new value, you are deleting the children of the element. An element cannot
	 * have children and a value at the same time.
	 *
	 * @access public
	 * @param string $name
	 * @param string $value
	 * @return M_XmlElement $element
	 *		Returns itself, for a fluent programming interface
	 */
	public function setValueOf($name, $value) {
		// Cast the name to a string, just in case:
		$name = (string) $name;

		// Set the new value
		$this->$name = (string) $value;
	}

	/**
	 * Add a child
	 *
	 * @access public
	 * @param string $name
	 * @param string $value
	 * @param array $attributes
	 * @param string $namespace
	 * @return M_XmlElement $element
	 *		Returns the newly added element
	 */
	public function addChild($name, $value = NULL, array $attributes = array(), $namespace = NULL) {
		// Add the child, thus creating a new element:
		$element = parent::addChild($name, $value, $namespace);

		/* @var $element M_XmlElement */
		// For each of the attributes:
		foreach($attributes as $name => $value) {
			// Add the attribute to the element:
			$element->addAttribute($name, $value);
		}

		// Return the element:
		return $element;
	}

	/**
	 * Add an XML Element as a child
	 *
	 * @access public
	 * @param M_XmlElement $element
	 * @return M_XmlElement $element
	 *		Returns the newly added element
	 */
	public function addChildElement(M_XmlElement $element) {
		// Add the child:
		$newElement = $this->addChild(
			// With (tag) name:
			$element->getName(),
			// With value (may be NULL)
			$element->getValue(),
			// With attributes:
			$element->getAttributes()
		);

		// Then, add the children:
		foreach($element->children() as $child) {
			/* @var $child M_XmlElement */
			// Add the current child to the XML Element:
			$newElement->addChildElement($child);
		}

		// Return the new element:
		return $newElement;
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Parse XML from string
	 *
	 * This method is used by the constructors of {@link M_XmlElement}, in order
	 * to parse XML data into an object of {@link M_XmlElement}. In doing so,
	 * this method will surpress all XML Errors, and convert them to thrown instances
	 * of {@link M_XmlException}
	 *
	 * @static
	 * @access protected
	 * @param string $string
	 * @param int $options
	 * @param string $namespace
	 * @return M_XmlElement
	 */
	protected static function _getXmlFromString($string, $options = NULL, $namespace = NULL) {
		// Surpress XML errors:
		libxml_use_internal_errors(TRUE);

		// Parse the XML:
		$xml = @simplexml_load_string($string, __CLASS__, $options, $namespace);

		// If the XML could not have been parsed:
		if(! $xml) {
			// Get the errors that have occurred during the parsing of XML Data:
			$errorString = '';

			// For each of the errors:
			foreach(libxml_get_errors() as $error) {
				$errorString .= self::_getXmlError($error, $xml);
			}

			// Then, we throw an exception
			throw new M_XmlException(sprintf(
				'Cannot construct an instance of %s with XML Source Code %s; An ' .
				'error has occurred, while parsing the XML data: <xmp>%s</xmp>',
				__CLASS__,
				$string,
				$errorString
			));
			
			// Clear errors
			libxml_clear_errors();
		}

		// Stop surpressing XML errors:
		libxml_use_internal_errors(FALSE);

		// Return the XML:
		return $xml;
	}

	/**
	 * Get formatted XML Error
	 *
	 * While {@link M_XmlElement} parses the XML Source Code, it might encounter
	 * problems in the provided XML string. If it does, errors will be reported,
	 * and formatted by this method. The formatted error will be shown in the
	 * thrown exception
	 *
	 * @access protected
	 * @param mixed $xml
	 * @param object $error
	 * @return string
	 */
	protected function _getXmlError($error, $xml) {
		// New lines in the output string:
		$nl = "\r\n";

		// Start the output of this function, with the indication of where the
		// error occurred in the source code:
		$out  = $nl;
		$out .= str_repeat('-', $error->column) . '^' . $nl;

		// Add a label for the error (warning/error/fatal)
		switch($error->level) {
			// Warning:
			case LIBXML_ERR_WARNING:
				$out .= 'Warning; ';
				break;

			// Fatal error
			case LIBXML_ERR_FATAL:
				$out .= 'Fatal Error; ';
				break;

			// Error:
			case LIBXML_ERR_ERROR:
				$out .= 'Error; ';
				break;
		}

		// Error code:
		$out .= 'Error Code ' . $error->code . '. ';

		// Line + Column
		$out .= trim($error->message) .
			'; # Line ' . $error->line .
			'; # Column ' . $error->column;

		// File
		if($error->file) {
			$out .= '; [File] ' . $error->file;
		}

		// Return output
		return $out;
	}

	/**
	 * Convert given xml element to array
	 *
	 * Used by {@link M_XmlElement::toArray()} to convert the XML Element to
	 * an (associative) array.
	 *
	 * @see M_XmlElement::toArray()
	 * @access protected
	 * @param mixed $xml
	 * @return mixed
	 */
	protected function _getArrayFromXml($xml) {
		// Is the provided argument an XML element?
		if(M_Helper::isInstanceOf($xml, 'SimpleXmlElement')) {
			// If the element does not have children, then we return the contents
			// as the result of this method; not an array!
			if(count($xml) == 0) {
				return (string) $xml; // for CDATA
			}

			// Get the attributes:
			$attributes = $xml->attributes();

			// Prepare a temporary array, in which we will store attributes
			// of the current element
			$a = array();

			// For each of the attributes
			foreach($attributes as $k => $v) {
				// If a value exists for the current attribute
				if($v) {
					// Then, set the value in the temporary array of attributes:
					$a[$k] = (string) $v;
				}
			}

			// We get the accessible non-static properties of the given xml object,
			// according to scope
			$xml = get_object_vars($xml);
		}

		// If we are now dealing with an iterator (either because an array has been
		// provided in the first place, or the xml element has been converted to
		// an array):
		if(M_Helper::isIterator($xml)) {
			// The output array:
			$r = array();

			// For each of the elements in the array:
			foreach($xml as $key => $value) {
				// Note that we call the same function on each element, in order
				// to recursively convert all children to arrays:
				$r[$key] = $this->_getArrayFromXml($value);
			}

			// Have attributes been collected for this element?
			if(isset($a) && count($a) > 0) {
				// Then, we add the attributes to the array, in the "@" key:
				$r['@'] = $a;
			}

			// Return the output array
			return $r;
		}

		// If we are still here, then we're probably dealing with a string-like
		// variable. We return that value, in order to add it to the output
		// array in recursive calls...
		return (string) $xml;
	}

	/**
	 * Convert given XML Element to XML Source Code
	 *
	 * Used by {@link M_XmlElement::toXmlSourceCodeString()} to convert the XML
	 * Element to a string with correctly formatted XML source code.
	 *
	 * @see M_XmlElement::toXmlSourceCodeString()
	 * @access protected
	 * @param M_XmlElement $element
	 * @param integer $indentLevel
	 * @return string
	 */
	protected function _getSourceCodeStringFromXml(M_XmlElement $element, $indentLevel = 0) {
		// The return value (empty for now)
		$output  = '';

		// Prepare the new-line character(s)
		$newline = "\n" . str_repeat("\t", $indentLevel);

		// First of all, we add the name of the provided element, along with
		// its attributes and values:
		$output .= $newline;
		$output .= '<' . $element->getTagName();

		// For each of the attributes:
		foreach($element->getAttributes() as $name => $value) {
			// Add the attribute to the output string:
			$output .= ' ' . $name . '="' . htmlspecialchars($value) . '"';
		}

		// Note that we prepare a boolean, that indicates whether or not we have
		// closed the tag we just opened:
		$isTagClosed = FALSE;

		// If the current xml element does not have children:
		if(! $element->hasChildren()) {
			// And, if the current element does not have a value:
			if(! $element->hasValue()) {
				// Then, we convert the opening tag to a self-closing tag:
				$output .= ' />';

				// And we indicate that we have already closed the tag:
				$isTagClosed = TRUE;
			}
			// If the current element does have a value
			else {
				// Close the opening tag:
				$output .= '>';

				// Then, we add the value to the source code. First, we get the
				// element's value:
				$value = $element->getValue();

				// If the value contains character(s) that would require the
				// value to be wrapped by a CDATA Block:
				if(M_Helper::containsCharactersForXmlCdata($value)) {
					// Then, we wrap the string by a CDATA block:
					$value = '<![CDATA[' . $value . ']]>';
				}

				// We add the value to the source code:
				$output .= $value;
			}
		}
		// If the provided xml element does have children
		else {
			// Close the opening tag:
			$output .= '>';

			// For each of the children:
			foreach($element->children() as $child) {
				/* @var $child M_XmlElement */
				// Add the child to the output:
				$output .= $this->_getSourceCodeStringFromXml($child, $indentLevel + 1);
			}

			// Add a new-line, before the ending tag:
			$output .= $newline;
		}

		// If the opened tag has not yet been closed:
		if(! $isTagClosed) {
			// Then, we do so now:
			$output .= '</' . $element->getTagName() . '>';
		}

		// Return the XML Source Code:
		return $output;
	}

	/**
	 * Convert given XML Element to an instance of {@link MI_Config}
	 *
	 * Used by {@link M_XmlElement::toConfig()} to convert the XML Element
	 * into an instance of the {@link MI_Config} interface.
	 *
	 * @see M_XmlElement::toConfig()
	 * @access protected
	 * @param M_XmlElement $element
	 * @return MI_Config
	 */
	protected function _getConfigFromXml(M_XmlElement $element) {
		// If the provided XML Element has children:
		if($element->hasChildren()) {
			// Then, we construct a new instance of M_Config, to represent the
			// XML Element:
			$config = new M_Config();

			// For each of the children in the provided XML Element:
			foreach($element->children() as $child) {
				/* @var $child M_XmlElement */
				// Get the name of the current child:
				$name = $child->getName();

				// We add the current child to the M_Config object:
				$config->$name = $this->_getConfigFromXml($child);

				// We check if a "title" attribute exists for the current child
				// element:
				$temp = $child->getAttribute('title', NULL);

				// If so:
				if($temp) {
					// Then, we set the name of the entry that we have created
					// in the M_Config object:
					$config->setTitleOf($name, $temp);
				}

				// We check if a "description" attribute exists for the current
				// child element:
				$temp = $child->getAttribute('description', NULL);

				// If so:
				if($temp) {
					// Then, we set the description of the entry that we have
					// created in the M_Config object:
					$config->setDescriptionOf($name, $temp);
				}
			}

			// We return the M_Config object:
			return $config;
		}
		// If the provided XML Element does not have children:
		else {
			// Then, we simply return its value:
			return $element->getValue();
		}
	}
}