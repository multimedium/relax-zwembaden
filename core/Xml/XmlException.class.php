<?php
/**
 * M_XmlException
 *
 * ...Exceptions thrown by the XML API in the core
 *
 * @package Core
 */
class M_XmlException extends M_Exception {
}