<?php
/**
 * M_Array class
 * 
 * @package Core
 * @subpackage Array
 */
class M_Array {
	/**
	 * Sort multidimensional array
	 * 
	 * This method can be used to sort the elements in a multi-dimensional
	 * array by the values of a given key. Consider the following set
	 * of data:
	 * 
	 * <code>
	 *    volume | edition
	 *    -------+--------
	 *    67     |       2
	 *    86     |       1
	 *    85     |       6
	 *    98     |       2
	 *    86     |       6
	 *    67     |       7
	 * </code>
	 * 
	 * As an array, this data could be represented in the following
	 * structure:
	 * 
	 * <code>
	 *    $data[] = array('volume' => 67, 'edition' => 2);
	 *    $data[] = array('volume' => 86, 'edition' => 1);
	 *    $data[] = array('volume' => 85, 'edition' => 6);
	 *    $data[] = array('volume' => 98, 'edition' => 2);
	 *    $data[] = array('volume' => 86, 'edition' => 6);
	 *    $data[] = array('volume' => 67, 'edition' => 7);
	 * </code>
	 * 
	 * In order to sort the values by volume, in descending order, you
	 * would do the following:
	 * 
	 * Example 1
	 * <code>
	 *    M_Array::sortMultiDimensional($data, 'volume', SORT_DESC);
	 * </code>
	 * 
	 * Note however that you can also sort the array by multiple keys.
	 * In Example 2, we will order by volume descending, edition 
	 * ascending. 
	 * 
	 * Example 2
	 * <code>
	 *    M_Array::sortMultiDimensional($data, 'volume', SORT_DESC, 'edition', SORT_ASC);
	 * </code>
	 * 
	 * Example 2 will now have sorted the array, causing the elements
	 * in the array to appear in the following order:
	 * 
	 * <code>
	 *    volume | edition
	 *    -------+--------
	 *    98     |       2
	 *    86     |       1
	 *    86     |       6
	 *    85     |       6
	 *    67     |       2
	 *    67     |       7
	 * </code>
	 * 
	 * @access public
	 * @param array $array
	 * 		A reference to the multidimensional array to be sorted
	 * @param string $key
	 * 		The key to sort the elements in the array by
	 * @param string $flag
	 * 		The (optional) sorting flag. Defaulted to SORT_ASC
	 * @param ...
	 * 		A variable number of additional columns to sort values by
	 * @return void
	 */
	public static function sortMultiDimensional(array &$array) {
		// Get the columns by which we are sorting:
		$columns = array();
		for($i = 1, $args = func_get_args(), $n = count($args); $i < $n; $i ++) {
			$columns[$args[$i]] = isset($args[++ $i]) ? $args[$i] : SORT_ASC;
		}
		
		// Make sure that at least 1 column has been given:
		if($i == 1) {
			throw new M_ArrayException(sprintf(
				'%s: %s, You should provide at least 1 sort column',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// We have an array of rows, but array_multisort() requires 
		// an array of columns, so we use the below code to obtain the 
		// columns, then perform the sorting. 
		$args   = array();
		foreach($columns as $name => $order) {
			$tmp = array();
			foreach($array as $key => $row) {
				$tmp[$key] = $row[$name];
			}
			
			// Add array_multisort arguments:
			$args[] =& $tmp;
			$args[] =& $order;
		}
		
		// Append the array to be sorted, to the array_multisort()
		// function
		$args[] =& $array;
		
		// Sort the array:
		call_user_func_array('array_multisort', $args);
	}
	
	/**
	 * Insert value in array
	 * 
	 * Will insert a value at a given (numerical) position of an array.
	 * The existing values in the array will be shifted to new positions, 
	 * in order to give the new value the position it requests.
	 * 
	 * Example 1
	 * <code>
	 *    $myArray = array('one', 'two', 'four');
	 *    print_r($myArray);
	 *    
	 *    $myArray = M_Array::insertValueAtPosition($myArray, 'three', 2);
	 *    print_r($myArray);
	 * </code>
	 * 
	 * Example 1 will generate the following output:
	 * <code>
	 *    Array (
	 *       0 => 'one',
	 *       1 => 'two',
	 *       2 => 'four',
	 *    )
	 *    Array (
	 *       0 => 'one',
	 *       1 => 'two',
	 *       2 => 'three',
	 *       3 => 'four',
	 *    )
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * The return value of this method is an array that has been indexed
	 * numerically! Also associative arrays will first be converted to
	 * a numericallly indexed array, before inserting the new value.
	 * 
	 * @access public
	 * @param array $array
	 * 		The array into which to insert the new value
	 * @param mixed $value
	 * 		The value to be inserted
	 * @param integer $position
	 * 		The position at which to insert the value
	 * @return array
	 */
	public static function insertValueAtPosition(array $array, $value, $position) {
		// We make sure that the provided array is indexed numerically,
		// by downloading the values into an array with numerical keys
		$array = array_values($array);
		
		// We make sure the position is in de correct range:
		// (not smaller than 0, not greater than the array's length)
		$position = (int) $position;
		$count = count($array);
		if($position < 0) {
			$position = 0;
		} else {
			if($position > $count) {
				$position = $count;
			}
		}
		
		// We shift the existing values in the array:
		for($i = ($count - 1); $i >= $position; $i --) {
			$array[$i + 1] = $array[$i];
		}
		
		// We insert the new value
		$array[$position] = $value;
		
		// Return the final array, which now carries the new value
		return $array;
	}
	
	/**
	 * Search for value, and remove
	 * 
	 * This method will search for the occurrences of a given value, and then
	 * remove the values from the array.
	 * 
	 * @access public
	 * @param array $array
	 * 		The array from which to remove the search value
	 * @param mixed $value
	 * 		The search value
	 * @return array
	 */
	public static function removeValue(array $array, $value) {
		// For each of the keys where the value is being stored:
		foreach(array_keys($array, $value) as $key) {
			// Remove the key from the array:
			unset($array[$key]);
		}
		
		// return the new array:
		return $array;
	}
	
	/**
	 * Trim all array elements (recursive)
	 * 
	 * @param array $array
	 * @return array
	 */
	public static function trimElements(array $array) {
		$output = array();
		
		foreach($array AS $key => $value) {
			
			//trim it, if it's a string
			if(is_string($value)) $value = trim($value);
			
			
			//if it's an array: do recursive
			elseif(is_array($value)) {
				$value = self::trimElements($value);
			}
			
			$output[$key] = $value;
		}
		
		return $output;
	}
}