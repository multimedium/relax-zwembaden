<?php
/**
 * M_FilterImage class
 *
 * Typically, an instance of this class is used as the (first) decorated
 * object in the chain of filters. This filter does not apply any 
 * operation, it simply mounts an image resource and passes it to the 
 * decorator(s). Consider the following example:
 * 
 * Example 1, resize an image:
 * <code>
 *    $filter = new M_FilterImageResize(
 *                 new M_FilterImage(
 *                    'my-image.jpg'
 *                 ),
 *                 150,
 *                 100
 *              );
 *    $resizedImage = $filter->apply();
 * </code>
 * 
 * NOTE:
 * M_FilterImage is a filter and implements the {@link MI_Filter} 
 * interface. For an introduction on filters, read the docs on 
 * {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterImage implements MI_Filter {
	/**
	 * Subject
	 *
	 * This property stores the image subject onto which the filter 
	 * operation(s) are to be applied.
	 * 
	 * @access private
	 * @var string
	 */
	private $_subject;
	
	/**
	 * Constructor
	 *
	 * NOTE:
	 * The constructor of {@link M_FilterImage} accepts:
	 * 
	 * - a path to an image file (string), or 
	 * - an instance of {@link M_Image}.
	 * 
	 * If any other type of data is provided to the constructor, an 
	 * {@link M_FilterException} will be thrown.
	 * 
	 * @throws M_FilterException
	 * @access public
	 * @param string $subject
	 * @return M_FilterImage
	 */
	public function __construct($subject) {
		if(is_string($subject)) {
			$subject = new M_Image($subject);
		} elseif(!($subject instanceof M_Image)) {
			throw new M_FilterException(sprintf(
				'%s expects a path (string) or an instance of M_Image',
				__CLASS__
			));
		}
		if($subject->exists()) {
			$this->_subject = $subject;
		} else {
			throw new M_FilterException(sprintf(
				'%s can not apply filters on "%s" (could not locate image)',
				__CLASS__,
				$subject->getPath()
			));
		}
	}
	
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * Note that image filters are expected to return an image resource.
	 * An image resource should implement the {@link M_ImageResource}
	 * interface.
	 * 
	 * In the case of {@link M_FilterImage}, this method simply 
	 * mounts an image resource (see {@link M_ImageResourceGdFromFile})
	 * out of the image that has been provided to the constructor.
	 * 
	 * @access public
	 * @return M_ImageResource
	 */
	public function apply() {
		return new M_ImageResourceGdFromFile($this->_subject);
	}
}
?>