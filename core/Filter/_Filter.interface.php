<?php
/**
 * MI_Filter interface
 * 
 * The MI_Filter interface describes the public API that must be
 * implemented by M_Filter classes. A filter is an operation that can
 * be applied to a given subject in order to change/mutate it. A good 
 * example of a filter is {@link M_FilterTextHtml}, which removes all
 * HTML markup from a text subject. However, a filter is not necessarily 
 * applied on strings. There are also filters for images, e.g. 
 * {@link M_FilterImageResize}.
 * 
 * Note that the structure of the M_Filter API is strongly inspired by 
 * the Decorator/Decorated pattern. In this pattern, you can create a 
 * chain of filters, each of them represented by an instance of an
 * {@link MI_Filter} class. Consider the following example:
 * 
 * <code>
 *    $filter = new M_FilterTextAccents(
 *               new M_FilterTextHtml(
 *                new M_FilterTextValue(
 *                 'Hello <strong>World</strong>'
 *                )
 *               )
 *              );
 * </code>
 * 
 * In the example illustrated above, we have created a chain of filters.
 * In this chain of filters, the {@link M_FilterTextValue} object is
 * decorated by {@link M_FilterTextHtml}, which in turn is decorated by
 * {@link M_FilterTextAccents}. Now, we could apply the chain of filters 
 * to the text subject, by calling the apply() method:
 * 
 * <code>
 *    $filteredText = $filter->apply();
 * </code>
 * 
 * This single line of code will cause each filter to be invoked and 
 * applied to the original subject:
 * 
 * - M_FilterTextValue::apply() passes the text to M_FilterTextHtml
 * - M_FilterTextHtml::apply() passes the text to M_FilterTextAccents
 * - M_FilterTextAccents::apply() passes the text to $filteredText
 * 
 * The MI_Filter interface is very simple. It defines only one method.
 * Note however that this interface is very important though. Both the
 * decorated and the decorators must implement the same interface, so
 * they can be chained and used interchangeably.
 * 
 * Each of the decorator classes will define the method apply(). This 
 * method does its own magic in each filter, but it is important to note
 * that it does so with the return value of the apply() method of the 
 * decorated object.
 * 
 * @package Core
 */
interface MI_Filter {
	/**
	 * Apply the filter
	 * 
	 * This method is the only method that is being defined in the 
	 * {@link MI_Filter} interface. It uses the return value of the
	 * decorated object's apply() method, and does its own magic with
	 * that value.
	 * 
	 * Once the filter has been applied, this method will return the
	 * changed/mutated value. It needs to do so, in order to make the
	 * filter chainable and interchangeable (The return value of this
	 * method may be used by another decorator object).
	 * 
	 * @access public
	 * @return mixed
	 */
	public function apply();
}
?>