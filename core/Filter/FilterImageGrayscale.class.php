<?php
class M_FilterImageGrayscale extends M_FilterImageDecorator {

	/**
	 * Apply
	 * @return M_ImageResourceGd
	 */
	public function apply() {
		return $this->_decoratedFilter->apply()->grayscale();
	}
}