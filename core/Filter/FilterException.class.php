<?php
/**
 * M_FilterException class
 * 
 * Exceptions thrown by {@link M_Filter} and related classes.
 *
 * @package Core
 */
class M_FilterException extends M_Exception {
}
?>
