<?php
/**
 * M_FilterTextMsWordCharacters class
 *
 * This specific Text Filter can be used to remove all MS Word garbage
 * from the subject. It will remove special characters, such as "smart 
 * quotes".
 * 
 * NOTE:
 * M_FilterTextMsWordCharacters, a subclass of {@link M_FilterTextDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterTextMsWordCharacters extends M_FilterTextDecorator {
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * @access public
	 * @return string
	 */
	public function apply() {
		$map = array(
			// "Smart Quotes"
			chr(226).chr(128).chr(152) => '\'',
			chr(226).chr(128).chr(153) => '\'',
			chr(226).chr(128).chr(154) => '\'',
			chr(226).chr(128).chr(155) => '\'',
			chr(226).chr(128).chr(156) => '"',
			chr(226).chr(128).chr(157) => '"',
			chr(226).chr(128).chr(158) => '"',
			chr(226).chr(128).chr(159) => '"',
			// EM Dash
			chr(226).chr(128).chr(148) => '-',
			// "Horizonal ellipsis" (three grouped dots: "...")
			chr(226).chr(128).chr(166) => '...',
			// Listed item's dot
			chr(226).chr(128).chr(162) => '- '
		);
		return strtr($this->_decoratedFilter->apply(), $map);
	}
}
?>