<?php
/**
 * M_FilterImageCrop class
 *
 * This specific Image Filter can be used to crop a part of the image subject. 
 * Note that this class will use {@link M_ImageRatio} to maintain the
 * original image's aspect ratio (if requested).
 * 
 * NOTE:
 * M_FilterImageCrop, a subclass of {@link M_FilterImageDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterImageCrop extends M_FilterImageDecorator {
	/**
	 * Width
	 * 
	 * This property stores the width to which the image should be 
	 * cropped.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_width;
	
	/**
	 * Height
	 * 
	 * This property stores the height to which the image should be 
	 * cropped.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_height;
	
	/**
	 * X-Position
	 * 
	 * This property stores the x-position where the image should be cropped. 
	 * Note that, the x-position + {M_FilterImageCrop::_width} should not be
	 * smaller as the original image's width
	 *
	 * Besides the height in pixels, it's also possible to specify the X-position
	 * as a string: "left", "center" or "right" are allowed
	 * 
	 * @access private
	 * @var string
	 */
	private $_xPosition;
	
	/**
	 * Y-Position
	 * 
	 * This property stores the y-position where the image should be cropped. 
	 * Note that, the y-position + {M_FilterImageCrop::$_height} should not be
	 * smaller as the original image's height.
	 *
	 * Besides the height in pixels, it's also possible to specify the Y-position
	 * as a string: "top", "center" or "bottom" are allowed
	 * 
	 * @access private
	 * @var integer
	 */
	private $_yPosition;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object
	 * @param integer $width
	 * 		The width to which the image should be cropped.
	 * @param integer $height
	 * 		The height to which the image should be cropped.
	 * @param integer $x
	 * 		The x-position on which we should start to crop.
	 * @param integer $height
	 * 		The y-position on which we should start to crop.
	 * @return M_FilterImageCrop
	 */
	function __construct(MI_Filter $decoratedFilter, $width, $height, $x, $y) {
		$this->_decoratedFilter = $decoratedFilter;
		$this->_width  = (int) $width;
		$this->_height = (int) $height;
		$this->_xPosition = $x;
		$this->_yPosition = $y;
	}
	
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * Note that image filters are expected to return an image resource.
	 * An image resource should implement the {@link M_ImageResource}
	 * interface.
	 * 
	 * @access public
	 * @return M_ImageResourceGd
	 */
	public function apply() {
		/* @var $rs M_ImageResourceGd */
		$rs = $this->_decoratedFilter->apply();
		$rs->crop(
			$this->_getXPositionInPixels($rs->getWidth()),
			$this->_getYPositionInPixels($rs->getHeight()),
			$this->_width,
			$this->_height
		);
		return $rs;
	}
	
	/* -- Additional to MI_Filter interface -- */
	
	/**
	 * Get width
	 * 
	 * This method will return the width to which the image is being 
	 * cropped. For more info, read the documentation on the
	 * property {@link M_FilterImageCrop::$_width}.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getWidth() {
		return $this->_width;
	}
	
	/**
	 * Get height
	 * 
	 * This method will return the height to which the image is being 
	 * cropped. For more info, read the documentation on the
	 * property {@link M_FilterImageCrop::$_height}.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getHeight() {
		return $this->_height;
	}
	
	/**
	 * Get X-position
	 * 
	 * This method will return the x-position on which we will start to crop
	 * the image. For more info, read the documentation on the
	 * property {@link M_FilterImageCrop::$_xPosition}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getXPosition() {
		return $this->_xPosition;
	}
	
	/**
	 * Get Y-position
	 * 
	 * This method will return the y-position on which we will start to crop
	 * the image. For more info, read the documentation on the
	 * property {@link M_FilterImageCrop::$_yPosition}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getYPosition() {
		return $this->_yPosition;
	}
	
	
	/**
	 * Set width
	 * 
	 * This method can be used to set the width to which the image
	 * should be cropped. For more info, read the documentation on the
	 * property {@link M_FilterImageCrop::$_width}.
	 * 
	 * @access public
	 * @param integer $width
	 * 		The width to crop to (expressed in pixels)
	 * @return void
	 */
	public function setWidth($width) {
		$this->_width = (int) $width;
	}
	
	/**
	 * Set height
	 * 
	 * This method can be used to set the height to which the image
	 * should be cropped. For more info, read the documentation on the
	 * property {@link M_FilterImageCrop::$_height}.
	 * 
	 * @access public
	 * @param integer $height
	 * 		The height to crop to (expressed in pixels)
	 * @return void
	 */
	public function setHeight($height) {
		$this->_height = (int) $height;
	}
	
	/**
	 * Set X-position
	 * 
	 * This method can be used to set the x-position on which we will start
	 * the image to crop. For more info, read the documentation on the
	 * property {@link M_FilterImageCrop::$x}.
	 * 
	 * @access public
	 * @param integer $height
	 * 		The x-position on which we will start to crop
	 * @return void
	 */
	public function setXPosition($x) {
		$this->_xPosition = (int) $x;
	}
	
	/**
	 * Set Y-position
	 * 
	 * This method can be used to set the y-position on which we will start
	 * the image to crop. For more info, read the documentation on the
	 * property {@link M_FilterImageCrop::$_yPosition}.
	 * 
	 * @access public
	 * @param integer $height
	 * 		The y-position on which we will start to crop
	 * @return void
	 */
	public function setYPosition($x) {
		$this->_yPosition = (int) $x;
	}

	/**
	 * Get the x-position from which we need to start cropping in pixels.
	 *
	 * Since the position can be defined as integer (pixels) or
	 * string (left, center) we need to check this before we start cropping
	 *
	 * @see FilterImageResizeSquare::getCropY()
	 * @param int $size The width of the resource we are cropping
	 * @return int
	 */
	protected function _getXPositionInPixels($size) {
		$crop = $this->getXPosition();

		//start cropping on the left or top-side?
		//@internal: something weird happens if we math against == 0. If $crop
		//is "center" this equation will return true :-|
		if ($crop == self::POSITION_LEFT || (string)$crop === '0') {
			return 0;
		}

		//use the center to start cropping
		elseif ($crop == self::POSITION_CENTER) {
			return ($size - $this->getWidth()) / 2;
		}

		//start cropping from the right
		elseif($crop == self::POSITION_RIGHT) {
			return ($size - $this->getWidth());
		}

		//use a numerical value to start cropping, this needs to be smaller
		//as the size of the resized image
		elseif (is_numeric($crop) && $crop < $size) {
			return $crop;
		}

		//oops...
		else {
			throw new M_FilterException(sprintf(
				'Cannot use %s as value to crop thumbnail',
				$crop)
			);
		}
	}

	/**
	 * Get the y-position from which we need to start cropping in pixels.
	 *
	 * Since the position can be defined as integer (pixels) or
	 * string (top, center) we need to check this before we start cropping
	 *
	 * @see FilterImageResizeSquare::getCropY()
	 * @param int $size The height of the resource we are cropping
	 * @return int
	 */
	protected function _getYPositionInPixels($size) {
		$crop = $this->getYPosition();

		//start cropping on the left or top-side?
		//@internal: something weird happens if we math against == 0. If $crop
		//is "center" this equation will return true :-|
		if ($crop == self::POSITION_TOP || (string)$crop === '0') {
			return 0;
		}

		//use the center to start cropping
		elseif ($crop == self::POSITION_CENTER) {
			return ($size - $this->getHeight()) / 2;
		}

		//start cropping from the bottom
		elseif($crop == self::POSITION_BOTTOM) {
			return ($size - $this->getHeight());
		}

		//use a numerical value to start cropping, this needs to be smaller
		//as the size of the resized image
		elseif (is_numeric($crop) && $crop < $size) {
			return $crop;
		}

		//oops...
		else {
			throw new M_FilterException(sprintf(
				'Cannot use %s as value to crop thumbnail at Y-position',
				$crop)
			);
		}
	}
}