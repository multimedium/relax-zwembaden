<?php
/**
 * M_FilterTextUtf8Bom class
 *
 * This specific Text Filter can be used to remove the UTF-8 BOM from the
 * provided text
 *
 * @package Core
 */
class M_FilterTextUtf8Bom extends M_FilterTextDecorator {
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * @access public
	 * @return string
	 */
	public function apply() {
		// Get the decorated string:
		$string = $this->_decoratedFilter->apply();

		// If the string starts with the UTF8 BOM:
		if(substr($string, 0, 3) == pack('CCC', 0xef, 0xbb, 0xbf)) {
			// Then, remove it:
			return substr($string, 3);
		}

		// If not, return the original string
		return $string;
	}
}