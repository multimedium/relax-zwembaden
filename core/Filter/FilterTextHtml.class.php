<?php
/**
 * M_FilterTextHtml class
 *
 * This specific Text Filter can be used to remove all HTML from the 
 * subject. A filter is made available for this purpose, because the 
 * results of PHP's built-in strip_tags() are not always satisfactory.
 * 
 * NOTE:
 * M_FilterTextAccents, a subclass of {@link M_FilterTextDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterTextHtml extends M_FilterTextDecorator {
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * @access public
	 * @return string
	 */
	public function apply() {
		// Define the patterns for replacement:
		$patterns = array(
			// - Strip out javascript blocks
			'@<script[^>]*?>.*?</script>@si',
			// - Strip style tags
			'@<style[^>]*?>.*?</style>@siU',
			// - Strip multi-line comments (including CDATA)
			'@<![\s\S]*?--[ \t\n\r]*>@',
			// - Strip out HTML tags
			'@<[\/\!]*?[^<>]*?>@si',
		);
		
		// Decode HTML Entities:
		return html_entity_decode(
			// We replace the non-breaking space by a regular space. If we don't
			// do this, the trim() function will not work on the resulting string,
			// since non-breaking spaces are replaced by ASCII code 160 (0xa0)
			// by html_entity_decode().
			str_replace(
				'&nbsp;',
				' ',
				// Replace HTML tags:
				preg_replace($patterns, '', $this->_decoratedFilter->apply())
			),
			ENT_QUOTES, 
			'UTF-8'
		);
	}
}