<?php
class M_FilterTextStopwords extends M_FilterTextDecorator {
	
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * @access public
	 * @return string
	 */
	public function apply() {
		//Get the stopwords which we want to remove
		$catalog = M_LocaleStopwordsCatalog::getInstance();
		$iterator = $catalog->getIterator()->getArrayCopy();
		
		//Now split the text in words and punctuation, if the found pattern is
		//a stopword: remove it
		$text = preg_replace(
			'/([\w]+)/me', 
			'in_array("$1", $iterator) ? "" : "$1";', 
			$this->_decoratedFilter->apply()
		);
		
		//stopwords are removed, but as a result the output string could contain
		//multiple spaces between words
		return preg_replace('/\s+/', ' ', $text);;
	}
}