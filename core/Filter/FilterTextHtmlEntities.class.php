<?php
/**
 * M_FilterTextHtmlEntities class
 *
 * This specific Text Filter can be used to safely use a string as
 * HTML-text.
 *
 * A filter is made available for this purpose, because the
 * results of PHP's built-in htmlentities() are not always satisfactory.
 *
 * NOTE:
 * M_FilterTextHtmlEntities, a subclass of {@link M_FilterTextDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 *
 * @package Core
 */
class M_FilterTextHtmlEntities extends M_FilterTextDecorator {
	/**
	 * Apply the filter
	 *
	 * This method is a required implementation in any filter class.
	 * For more information, read the documentation on:
	 *
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 *
	 * @access public
	 * @return string
	 */
	public function apply() {
        // Remove quotes and other special characters that do not belong in HTML
		// attribute value:

		// - &
        $string = str_replace("&", '&amp;', $this->_decoratedFilter->apply());
        $string = str_replace("&amp;amp;", '&amp;', $string);

		// - Greater and lower than
        $string = str_replace('>', '&gt;',  $string);
		$string = str_replace("<", '&lt;', $string);

		return $string;
	}
}