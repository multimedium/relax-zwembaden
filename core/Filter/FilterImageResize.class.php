<?php
/**
 * M_FilterImageResize class
 *
 * This specific Image Filter can be used to resize the image subject. 
 * Note that this class will use {@link M_ImageRatio} to maintain the
 * original image's aspect ratio (if requested).
 * 
 * NOTE:
 * M_FilterImageResize, a subclass of {@link M_FilterImageDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterImageResize extends M_FilterImageDecorator {
	/**
	 * Width
	 * 
	 * This property stores the width to which the image should be 
	 * resized. Note that, if the original image's aspect ratio is to
	 * be maintained, this width will be interpreted as the maximum
	 * width.
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_width;
	
	/**
	 * Height
	 * 
	 * This property stores the height to which the image should be 
	 * resized. Note that, if the original image's aspect ratio is to
	 * be maintained, this height will be interpreted as the maximum
	 * height.
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_height;
	
	/**
	 * Flag: maintain aspect ratio
	 * 
	 * This property stores a boolean flag that indicates whether or
	 * not the image's aspect ratio should be maintained when resizing.
	 * 
	 * @access protected
	 * @var boolean
	 */
	protected $_keepAspectRatio;

	/**
	 * Constructor
	 * 
	 * @access public
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object
	 * @param integer $width
	 * 		The width to which the image should be resized. Note that, 
	 * 		if the original image's aspect ratio is to be maintained, 
	 * 		this width will be interpreted as the maximum width.
	 * @param integer $height
	 * 		The height to which the image should be resized. Note that, 
	 * 		if the original image's aspect ratio is to be maintained, 
	 * 		this height will be interpreted as the maximum height.
	 * @param boolean $keepAspectRatio
	 * 		Set to TRUE (default) to maintain aspect ratio, FALSE to
	 * 		force to given width and height.
	 * @return M_FilterImageResize
	 */
	public function __construct(MI_Filter $decoratedFilter, $width, $height, $keepAspectRatio = TRUE) {
		$this->_decoratedFilter = $decoratedFilter;
		$this->_width  = (int) $width;
		$this->_height = (int) $height;
		$this->_keepAspectRatio = (bool) $keepAspectRatio;
	}
	
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * Note that image filters are expected to return an image resource.
	 * An image resource should implement the {@link M_ImageResource}
	 * interface.
	 * 
	 * @access public
	 * @return M_ImageResourceGd
	 */
	public function apply() {
		$rs = $this->_decoratedFilter->apply();
		$size = M_ImageRatio::getMaxSize($rs->getWidth(), $rs->getHeight(), $this->_width, $this->_height);
		$rs->resize($size[0], $size[1]);
		return $rs;
	}
	
	/* -- Additional to MI_Filter interface -- */
	
	/**
	 * Get width
	 * 
	 * This method will return the width to which the image is being 
	 * resized. For more info, read the documentation on the
	 * property {@link M_FilterImageResize::$_width}.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getWidth() {
		return $this->_width;
	}
	
	/**
	 * Get height
	 * 
	 * This method will return the height to which the image is being 
	 * resized. For more info, read the documentation on the
	 * property {@link M_FilterImageResize::$_height}.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getHeight() {
		return $this->_height;
	}
	
	/**
	 * Get flag: maintain aspect ratio?
	 * 
	 * Will return TRUE if filter is maintaining the aspect ratio, 
	 * FALSE if not.
	 *
	 * @access public
	 * @return boolean
	 */
	public function getKeepAspectRatio() {
		return $this->_keepAspectRatio;
	}

	/**
	 * Set width
	 * 
	 * This method can be used to set the width to which the image
	 * should be resized. For more info, read the documentation on the
	 * property {@link M_FilterImageResize::$_width}.
	 * 
	 * @access public
	 * @param integer $width
	 * 		The width to resize to (expressed in pixels)
	 * @return void
	 */
	public function setWidth($width) {
		$this->_width = (int) $width;
	}
	
	/**
	 * Set height
	 * 
	 * This method can be used to set the height to which the image
	 * should be resized. For more info, read the documentation on the
	 * property {@link M_FilterImageResize::$_height}.
	 * 
	 * @access public
	 * @param integer $height
	 * 		The height to resize to (expressed in pixels)
	 * @return void
	 */
	public function setHeight($height) {
		$this->_height = (int) $height;
	}

	/**
	 * Set flag: maintain aspect ratio?
	 * 
	 * This method can be used to indicate whether or not the filter
	 * should maintain the original image's aspect ratio. Pass in TRUE
	 * to keep the aspect ratio, FALSE to force the image to the 
	 * provided width and height.
	 *
	 * @access public
	 * @param boolean $flag
	 * @return void
	 */
	public function setKeepAspectRatio($flag) {
		$this->_keepAspectRatio = (bool) $flag;
	}

	/**
	 * X-position from where we should start to crop, if the resized image
	 * exceeds the width and/or height defined in the definition
	 *
	 * This can be either a integer, which represents the amount of pixels. Or
	 * you can use one of "left", "right" or "center" to automatically
	 * calculate the beginposition. Use the constants defined in
	 * {@link M_FilterImageDecorator} to set a textual-crop position.
	 *
	 * @var int|string
	 */
	private $_cropX;

	/**
	 * Y-position from where we should start to crop, if the resized image
	 * exceeds the width and/or height defined in the definition
	 *
	 * This can be either a integer, which represents the amount of pixels. Or
	 * you can use one of "left", "right" or "center" to automatically
	 * calculate the beginposition. Use the constants defined in
	 * {@link M_FilterImageDecorator} to set a textual-crop position.
	 *
	 * @var int|string
	 */
	private $_cropY;

	/**
	 * Get cropX
	 *
	 * This method will return the x-start position from which the image will be
	 * cropped if the resized width exceeds {@M_FilterImageResize::getWidth()}.
	 * For more info, read the documentation on the
	 * property {@link M_FilterImageResize::$_cropX}.
	 *
	 * @access public
	 * @return integer|string
	 */
	public function getCropX() {
		return $this->_cropX;
	}

	/**
	 * Get cropY
	 *
	 * This method will return the y-start position from which the image will be
	 * cropped if the resized height exceeds {@M_FilterImageResize::getHeight()}.
	 * For more info, read the documentation on the
	 * property {@link M_FilterImageResize::$_cropY}.
	 *
	 * @access public
	 * @return integer|string
	 */
	public function getCropY() {
		return $this->_cropY;
	}

	/**
	 * Set cropX: where to start to crop on position?
	 *
	 * This method can be used to determine the the x-start position from which
	 * the image will be cropped if the resized height exceeds
	 * {@M_FilterImageResize::getHeight()}.
	 *
	 * For more info, read the documentation on the
	 * property {@link M_FilterImageResize::$_cropX}.
	 *
	 * Note: use {@link M_FilterImageDecorator} constants if you want to use
	 * a string like "left", "right" or "center".
	 *
	 * @access public
	 * @return integer|string
	 */
	public function setCropX($cropX) {
		return $this->_cropX = $cropX;
	}

	/**
	 * Set cropY: where to start to crop on position?
	 *
	 * This method can be used to determine the the y-start position from which
	 * the image will be cropped if the resized height exceeds
	 * {@M_FilterImageResize::getHeight()}.
	 *
	 * For more info, read the documentation on the
	 * property {@link M_FilterImageResize::$_cropY}.
	 *
	 * Note: use {@link M_FilterImageDecorator} constants if you want to use
	 * a string like "top" or "center".
	 *
	 * @access public
	 * @return integer|string
	 */
	public function setCropY($cropY) {
		return $this->_cropY = $cropY;
	}

	/**
	 * Get the x-position from which we need to start cropping in pixels.
	 *
	 * Since the position can be defined as integer (pixels) or
	 * string (left, center) we need to check this before we start cropping
	 *
	 * @see FilterImageResizeSquare::getCropY()
	 * @param int $size
	 * @return int
	 */
	protected function _getCropXPosition($size) {
		$crop = $this->getCropX();

		//don't calculate cropX if it is not set
		if (is_null($crop)) {
			return $crop;
		}

		//start cropping on the left or top-side?
		//@internal: something weird happens if we math against == 0. If $crop
		//is "center" this equation will return true :-|
		elseif ($crop == self::POSITION_LEFT || (string)$crop === '0') {
			return 0;
		}

		//use the center to start cropping
		elseif ($crop == self::POSITION_CENTER) {
			return ($size - $this->getWidth()) / 2;
		}

		//start cropping from the right
		elseif($crop == self::POSITION_RIGHT) {
			return ($size - $this->getWidth());
		}

		//use a numerical value to start cropping, this needs to be smaller
		//as the size of the resized image
		elseif (is_numeric($crop) && $crop < $size) {
			return $crop;
		}

		//oops...
		else {
			throw new M_FilterException(sprintf(
				'Cannot use %s as value to crop thumbnail',
				$crop)
			);
		}
	}

	/**
	 * Get the y-position from which we need to start cropping in pixels.
	 *
	 * Since the position can be defined as integer (pixels) or
	 * string (top, center) we need to check this before we start cropping
	 *
	 * @see FilterImageResizeSquare::getCropY()
	 * @param int $size
	 * @return int
	 */
	protected function _getCropYPosition($size) {
		$crop = $this->getCropY();

		//don't calculate cropY if it is not set
		if (is_null($crop)) {
			return $crop;
		}
		
		//start cropping on the left or top-side?
		//@internal: something weird happens if we math against == 0. If $crop
		//is "center" this equation will return true :-|
		elseif ($crop == self::POSITION_TOP || (string)$crop === '0') {
			return 0;
		}

		//use the center to start cropping
		elseif ($crop == self::POSITION_CENTER) {
			return ($size - $this->getHeight()) / 2;
		}

		//start cropping from the bottom
		elseif($crop == self::POSITION_BOTTOM) {
			return ($size - $this->getHeight());
		}

		//use a numerical value to start cropping, this needs to be smaller
		//as the size of the resized image
		elseif (is_numeric($crop) && $crop < $size) {
			return $crop;
		}

		//oops...
		else {
			throw new M_FilterException(sprintf(
				'Cannot use %s as value to crop thumbnail at Y-position',
				$crop)
			);
		}
	}
}