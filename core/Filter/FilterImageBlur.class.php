<?php
class M_FilterImageBlur extends M_FilterImageDecorator {

	public function apply() {
		return $this->_decoratedFilter->apply()->blur();
	}
}