<?php
/**
 * M_FilterImageRGBAMatrix class
 *
 * This specific Image Filter can be used to apply an RGBA Matrix to 
 * an image resource. This filter relies heavily on the following:
 * 
 * - {@link M_ValidatorRGBAMatrix}
 * - {@link M_ImageResourcePixelIterator}
 * - {@link MI_ImageResource::getPixelIterator()}
 * - {@link M_Color}
 * 
 * For an introduction on RGBA matrixes, read the documentation on
 * {@link M_Color::applyRGBAMatrix()}.
 * 
 * NOTE:
 * M_FilterImageRGBAMatrix, a subclass of {@link M_FilterImageDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterImageRGBAMatrix extends M_FilterImageDecorator {
	/**
	 * The RGBA Matrix
	 * 
	 * This property stores the RGBA Matrix that is to be applied on
	 * the image subject.
	 *
	 * @access private
	 * @var array
	 */
	private $_matrix = array(
		1, 0, 0, 0, 0,
		0, 1, 0, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 1, 0
	);
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object
	 * @param array $matrix
	 * 		The RGBA Matrix
	 * @return M_FilterImageRGBAMatrix
	 */
	function __construct(MI_Filter $decoratedFilter, array $matrix) {
		$this->_decoratedFilter = $decoratedFilter;
		$this->setRGBAMatrix($matrix);
	}
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object
	 * @param array $matrix
	 * 		The RGBA Matrix
	 * @return M_FilterImageRGBAMatrix
	 */
	public function apply() {
		$rs = $this->_decoratedFilter->apply();
		foreach($rs->getPixelIterator() as $pixel) {
			$color = $pixel->getColor();
			$color->applyRGBAMatrix($this->_matrix);
			$pixel->setColor($color);
			$rs->setPixel($pixel);
		}
		return $rs;
	}
	
	/* -- Additional to MI_Filter interface -- */
	
	/**
	 * Set RGBA Matrix
	 * 
	 * This method can be used to set the RGBA Matrix that is to be
	 * applied on the image subject. Note that this method will use
	 * {@link M_ValidatorRGBAMatrix} to check if the provided matrix
	 * is valid.
	 * 
	 * NOTE:
	 * An {@link M_FilterException} is thrown if the matrix is not 
	 * considered valid.
	 *
	 * @access public
	 * @param array $matrix
	 * 		The RGBA Matrix
	 * @return void
	 */
	public function setRGBAMatrix(array $matrix) {
		$validator = new M_ValidatorRGBAMatrix;
		if(! $validator->check($matrix)) {
			throw new M_FilterException(sprintf('%s: Invalid RGBA Matrix.', __CLASS__));
		} else {
			$this->_matrix = $matrix;
		}
	}
	
	/**
	 * Get RGBA Matrix
	 * 
	 * This method will return the RGBA Matrix that is being applied
	 * on the image.
	 *
	 * @access public
	 * @return array $matrix
	 * 		The RGBA Matrix
	 */
	public function getRGBAMatrix() {
		return $this->_matrix;
	}
}
?>