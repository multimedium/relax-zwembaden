<?php
/**
 * M_FilterTextValue class
 *
 * Typically, an instance of this class is used as the (first) decorated
 * object in the chain of filters. This filter does not apply any 
 * operation, it simply passes on the original text subject to the 
 * decorator. Consider the following example:
 * 
 * Example 1, filter HTML out of a string:
 * <code>
 *    $filter = new M_FilterTextHtml(
 *                 new M_FilterTextValue(
 *                    'Hello <strong>World</strong>'
 *                 )
 *              );
 *    $stringWithoutHtml = $filter->apply();
 * </code>
 * 
 * NOTE:
 * M_FilterTextValue is a filter and implements the {@link MI_Filter} 
 * interface. For an introduction on filters, read the docs on 
 * {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterTextValue implements MI_Filter {
	/**
	 * Subject
	 *
	 * This property stores the subject onto which the filter 
	 * operation(s) are to be applied.
	 * 
	 * @access private
	 * @var string
	 */
	private $_subject = '';
	
	/**
	 * Constructor
	 *
	 * NOTE:
	 * The constructor of {@link M_FilterTextValue} expects a string
	 * value. If any other type of data is provided to the constructor,
	 * it will throw an {@link M_FilterException}.
	 * 
	 * @throws M_FilterException
	 * @access public
	 * @param string $subject
	 * @return M_FilterTextValue
	 */
	public function __construct($subject) {
		if(is_string($subject)) {
			$this->_subject = $subject;
		} else {
			throw new M_FilterException('M_FilterTextValue expects a string');
		}
	}
	
	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * In the case of {@link M_FilterTextValue}, this method simply 
	 * returns the original text subject, as provided to the 
	 * constructor.
	 * 
	 * @access public
	 * @return string
	 */
	public function apply() {
		return $this->_subject;
	}
}
?>