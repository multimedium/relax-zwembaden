<?php
/**
 * M_FilterImageResizeForceFit class
 *
 * This specific Image Filter can be used to resize the image subject to a
 * forced width and height. This way an image will always have the given width
 * and height. Opposed to {@link M_FilterImageResize} which will resize untill
 * both dimensions are met, we now resize untill one dimension meets the
 * requirements. In addition we crop the image so it meets both requirements.
 *
 * Note that this class will use {@link M_ImageRatio} to maintain the
 * original image's aspect ratio (if requested).
 *
 * NOTE:
 * M_FilterImageResizeForced, a subclass of {@link M_FilterImageDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 *
 * @package Core
 */
class M_FilterImageResizeForcedFit extends M_FilterImageResize {

	/**
	 * This color is used as default fill color for the remaining space
	 * 
	 * @var string
	 */
	protected $_fillColor;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param MI_Filter $decoratedFilter
	 * 		The decorated object
	 * @param integer $width
	 * 		The width to which the image should be resized. Note that,
	 * 		if the original image's aspect ratio is to be maintained,
	 * 		this width will be interpreted as the maximum width.
	 * @param integer $height
	 * 		The height to which the image should be resized. Note that,
	 * 		if the original image's aspect ratio is to be maintained,
	 * 		this height will be interpreted as the maximum height.
	 * @param boolean $autoGrow
	 * 		Set to TRUE (default) to let the image grow
	 * @param int|string
	 *		The x-position where we start to crop when the resized image exceeds
	 *		the defined width
	 * @param int|string
	 *		The y-position where we start to crop when the resized image exceeds
	 *		the defined height
	 * @param string $fillColor
	 *		The color to fill the background
	 *
	 * @return M_FilterImageResize
	 */
	function __construct(MI_Filter $decoratedFilter, $width, $height, $keepAspectRatio = TRUE, $fillColor = null) {
		parent::__construct($decoratedFilter, $width, $height, $keepAspectRatio);

		// Set the fillcolor
		$this->_fillColor = $fillColor;
	}

	/**
	 * Apply the filter
	 *
	 * This method is a required implementation in any filter class.
	 * For more information, read the documentation on:
	 *
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 *
	 * Note that image filters are expected to return an image resource.
	 * An image resource should implement the {@link M_ImageResource}
	 * interface.
	 *
	 * @access public
	 * @return M_ImageResourceGd
	 */
	public function apply() {
		/* @var $rs M_ImageResourceGd */
		$rs = parent::apply();
		$resource = new M_ImageResourceGd($this->_width, $this->_height);

		//don't use a background:
		$opacity = 100;

		//a background color is given: do not make the background transparent
		if(!is_null($this->_fillColor)) {
			$opacity = 0;
		}
		$resource->drawFill(new M_Color($this->_fillColor), $opacity);
		$resource->insertResource($rs, $this->_getCropXPosition($rs->getWidth()), $this->_getCropYPosition($rs->getHeight()));
		return $resource;
	}


	/**
	 * Get the x-position from which we need to start cropping in pixels.
	 *
	 * Since the position can be defined as integer (pixels) or
	 * string (left, center) we need to check this before we start cropping
	 *
	 * @see FilterImageResizeSquare::getCropY()
	 * @param int $size
	 * @return int
	 */
	protected function _getCropXPosition($size) {
		$crop = $this->getCropX();

		//don't calculate cropX if it is not set
		if (is_null($crop)) {
			return $crop;
		}

		//start cropping on the left or top-side?
		//@internal: something weird happens if we math against == 0. If $crop
		//is "center" this equation will return true :-|
		elseif ($crop == self::POSITION_LEFT || (string)$crop === '0') {
			return 0;
		}

		//use the center to start cropping
		elseif ($crop == self::POSITION_CENTER) {
			return ($this->getWidth() - $size) / 2;
		}

		//start cropping from the right
		elseif($crop == self::POSITION_RIGHT) {
			return ($this->getWidth() - $size);
		}

		//use a numerical value to start cropping, this needs to be smaller
		//as the size of the resized image
		elseif (is_numeric($crop) && $crop < $size) {
			return $crop;
		}

		//oops...
		else {
			throw new M_FilterException(sprintf(
				'Cannot use %s as value to crop thumbnail',
				$crop)
			);
		}
	}

	/**
	 * Get the y-position from which we need to start cropping in pixels.
	 *
	 * Since the position can be defined as integer (pixels) or
	 * string (top, center) we need to check this before we start cropping
	 *
	 * @see FilterImageResizeSquare::getCropY()
	 * @param int $size
	 * @return int
	 */
	protected function _getCropYPosition($size) {
		$crop = $this->getCropY();

		//don't calculate cropY if it is not set
		if (is_null($crop)) {
			return $crop;
		}

		//start cropping on the left or top-side?
		//@internal: something weird happens if we math against == 0. If $crop
		//is "center" this equation will return true :-|
		elseif ($crop == self::POSITION_TOP || (string)$crop === '0') {
			return 0;
		}

		//use the center to start cropping
		elseif ($crop == self::POSITION_CENTER) {
			return ($this->getHeight() - $size) / 2;
		}

		//start cropping from the bottom
		elseif($crop == self::POSITION_BOTTOM) {
			return ($this->getHeight() - $size);
		}

		//use a numerical value to start cropping, this needs to be smaller
		//as the size of the resized image
		elseif (is_numeric($crop) && $crop < $size) {
			return $crop;
		}

		//oops...
		else {
			throw new M_FilterException(sprintf(
				'Cannot use %s as value to crop thumbnail at Y-position',
				$crop)
			);
		}
	}
}