<?php
/**
 * M_FilterTextXss class
 *
 * @package Core
 */

/**
 * Include the HTML Purifier class
 */
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . FOLDER_THIRDPARTY . DIRECTORY_SEPARATOR . 'htmlpurifier/HTMLPurifier.auto.php';

/**
 * M_FilterTextXss class
 *
 * The FilterTextXss is used to detect (and remove) XSS attacks. It 
 * extensively investigates the provided text subject in order to find 
 * common hacking tricks.
 * 
 * Intelligence to be taken into account:
 * 
 * - http://ha.ckers.org/xss.html
 * - http://htmlpurifier.org
 * 
 * NOTE:
 * M_FilterTextXss, a subclass of {@link M_FilterTextDecorator},
 * is a filter and implements the {@link MI_Filter} interface. For
 * an introduction on filters, read the docs on {@link MI_Filter}.
 * 
 * @package Core
 */
class M_FilterTextXss extends M_FilterTextDecorator {


	/* -- PROPERTIES -- */

	/**
	 * Apply the filter
	 * 
	 * This method is a required implementation in any filter class. 
	 * For more information, read the documentation on:
	 * 
	 * - {@link MI_Filter}
	 * - {@link MI_Filter::apply()}
	 * 
	 * @access public
	 * @return string
	 */
	public function apply() {
		$dirty = $this->_decoratedFilter->apply();

		// Apply the HTML Purifier
		$config = HTMLPurifier_Config::createDefault();
		$config->set('Cache.SerializerPath', M_Loader::getAbsolute('files/temp'));
		$purifier = new HTMLPurifier($config);
		return $purifier->purify($dirty);
	}
}
?>