<?php
/**
 * M_ServiceYouTubeHelper
 *
 * @author Tim Segers
 * @package Core
 */

class M_ServiceYouTubeHelper extends M_Object {

    /**
     * Stores Google Developer API Key
     */
    const DEVELOPER_KEY = 'AIzaSyDjLRtx8KcRtOfH3OqYo6A8F4RE0fFCgOk';

    /**
     * Contains the Google API Client
     *
     * @access private
     * @var Google_Client
     */
    private $_googleClient;

     /**
     * Contains the YouTube Service
     *
     * @access private
     * @var Google_Service_YouTube
     */
    private $_youtube;


    /**
     * Construct
     *
     * Constructs Google Client and YouTube Service for usage throughout
     * the class.
     *
     * @throws M_Exception
     */
    public function __construct(){

        // Autoload Google
        M_Loader::loadRelative('core/_thirdparty/Google/autoload.php');
        // TODO: Check if there is a cleaner solution to do this
        M_Loader::loadRelative('core/_thirdparty/Google/Model.php');

        $this->_googleClient = new Google_Client();

        // Set Developer Key
        // TODO: Allow to be overwritten by config
        $this->_googleClient->setDeveloperKey(self::DEVELOPER_KEY);

        // Load YouTube Service and use Google Client to construct instanct
        $this->_youtube = new Google_Service_YouTube($this->_googleClient);

    }

    /**
     * Will properly format and return the date string from the xml
     *
     * @access public static
     * @param string $string
     *        This is the date string provided by the xml file
     * @return M_Date
     * @throws M_Exception
     */
	public static function getDateFromString($string) {
		// 2010-04-15T06:47:00.000Z
		$matches = array();
		if(preg_match('/([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2}).*/', $string, $matches) == 1) {
			return new M_Date(array(
				'year'   => $matches[1],
				'month'  => $matches[2],
				'day'    => $matches[3],
				'hour'   => $matches[4],
				'minute' => $matches[5],
				'second' => $matches[6]
			));
		} else {
			throw new M_Exception(sprintf(
				'Cannot parse date from string %s',
				$string
			));
		}
	}

    /**
     * Will retrieve the video id from the given video uri
     *
     * @access public static
     * @param string $string
     *        The video uri
     * @return string
     * @throws M_Exception
     */
	public static function getVideoIdFromString($string) {
		// http://gdata.youtube.com/feeds/api/videos/z_BshfWGQos
		if($string) {
			$id = substr(strrchr($string, "/"), 1);
			return $id;
		} else {
			throw new M_Exception(sprintf(
				'Cannot parse id from string %s',
				$string
			));
		}
	}

    /**
     * Will create the direct link to a video from the given video id
     *
     * @access public static
     * @param int $id
     *        The video id
     * @return M_Uri $videoLink
     * @throws M_Exception
     */
	public static function getVideoLinkById($id) {
		// http://www.youtube.com/watch?v=EisxFvVVJXo
		if($id) {
			$videolink = new M_Uri('http://www.youtube.com/watch');
			$videolink->setQueryVariable('v', $id);
			return $videolink;
		} else {
			throw new M_Exception(sprintf(
				'Cannot parse url from string %s',
				$id
			));
		}
	}

    /**
     * Get the user's channel URL
     *
     * @access public
     * @param string $username
     *        The username. For more info, read {@link M_ServiceYouTube::getUser()}
     * @return M_Uri $channel
     * @throws M_Exception
     */
	public static function getChannelByUsername($username) {
		// http://www.youtube.com/username
		if($username) {
			$channel = new M_Uri('http://www.youtube.com/' . $username);
			return $channel;
		} else {
			throw new M_Exception(sprintf(
				'Cannot compose the channel URL from string $s',
				$username
			));
		}
	}


    /**
     * Get Video By Id
     *
     * Does an API call and constructs a
     * M_ServiceYouTubeVideo object with the result
     *
     * @param $videoId
     * @return M_ServiceYouTubeVideo
     * @throws M_Exception
     */
    public function getVideoById($videoId) {

        // Get the video snippet
        $snippet = $this->_getVideoSnippetByVideoId($videoId);

        if(! $snippet) {
            throw new M_Exception(sprintf(
                'Could not load video with id [%s].',
                $videoId)
            );
        }

        // Get the thumbnails from the snippet
        $thumbnails = $snippet->getThumbnails();
        /* @var $thumbnails Google_Service_YouTube_ThumbnailDetails */

        // Create a new M_ServiceYouTubeVideo object and insert all values
        $video = new M_ServiceYouTubeVideo((string) $videoId);
        $video
            ->setTitle((string) $snippet->getTitle())
            ->setDescription((string) $snippet->getDescription())
            ->setPublicationDate(self::getDateFromString($snippet->getPublishedAt()))
            ->setVideoUri(self::getVideoLinkById((string) $videoId))
            ->setBigThumbnailUri(new M_Uri($thumbnails->getHigh()->getUrl())) // 480 x 360
            ->setSmallThumbUriOne(new M_Uri($thumbnails->getDefault()->getUrl())) // 120 x 90
            ->setSmallThumbUriTwo(new M_Uri($thumbnails->getMedium()->getUrl()))  // 320 x 180
            ->setSmallThumbUriThree(new M_Uri($thumbnails->getHigh()->getUrl())); // 480 x 360

        return $video;
    }

    /**
     * Get Videos By Username
     *
     * Returns an M_ArrayIterator with M_ServiceYouTubeVideo objects for all
     * videos in the Uploads Playlist of a specific YouTube user.
     *
     * NOTE: Use with caution! This function loops through ALL videos and can get quite heavy.
     *
     * @param $username
     * @return M_ArrayIterator
     * @throws M_Exception
     */
    public function getVideosByUsername($username) {

        // Get the Id of the user's Upload Channel
        $uploadsChannelId = $this->getUploadsChannelIdByUsername($username);

        // New empty array to store our videos
        $videos = array();

        // Define page token
        $nextPageToken = null;

        // Keep looping to get all results from the playlist (50 at a time)
        do {
            // Do a List call
            try {
                $listResponse = $this->_youtube->playlistItems->listPlaylistItems(
                    "snippet",
                    array(
                        'playlistId' => $uploadsChannelId,
                        'maxResults' => 50,
                        'pageToken' => $nextPageToken
                    )
                );
            } catch (Exception $e) {
                throw new M_Exception(sprintf(
                    'Could not load user upload playlist. User: %s, PlaylistId: %s',
                    $username,
                    $uploadsChannelId)
                );
            }

            $items = $listResponse->getItems();

            // The first item contains our snippet
            $snippet = $items[0]->getSnippet();
            /* @var $snippet Google_Service_YouTube_PlaylistItemSnippet */

            // Get the thumbnails from the snippet
            $thumbnails = $snippet->getThumbnails();
            /* @var $thumbnails Google_Service_YouTube_ThumbnailDetails */

            $videoId = $snippet->getResourceId()->getVideoId();

            // Create a new M_ServiceYouTubeVideo object and insert all values
            $video = new M_ServiceYouTubeVideo((string) $videoId);
            $video
                ->setTitle((string) $snippet->getTitle())
                ->setDescription((string) $snippet->getDescription())
                ->setPublicationDate(self::getDateFromString($snippet->getPublishedAt()))
                ->setVideoUri(self::getVideoLinkById((string) $videoId))
                ->setBigThumbnailUri(new M_Uri($thumbnails->getHigh()->getUrl())) // 480 x 360
                ->setSmallThumbUriOne(new M_Uri($thumbnails->getDefault()->getUrl())) // 120 x 90
                ->setSmallThumbUriTwo(new M_Uri($thumbnails->getMedium()->getUrl()))  // 320 x 180
                ->setSmallThumbUriThree(new M_Uri($thumbnails->getHigh()->getUrl())); // 480 x 360

            array_push($videos, $listResponse->getItems());

            $nextPageToken = $listResponse->getNextPageToken();

        } while(! is_null($nextPageToken));

        return new M_ArrayIterator($videos);
    }

    /**
     * Get Uploads Channel Id By Username
     *
     * Returns the id of the channel with the video uploads
     * by a specific user.
     *
     * @param $username
     * @return string
     */
    public function getUploadsChannelIdByUsername($username) {

        // Do a List call
        try {
            $listResponse = $this->_youtube->channels->listChannels(
                "contentDetails",
                array(
                    'forUsername' => $username
                )
            );
        } catch (Exception $e) {
            throw new M_Exception(sprintf(
            'Could not load user upload playlist. User: %s',
            $username)
            );
        }

        // Extract the channels from our response
        $channels = $listResponse->getItems();

        $channel = $channels[0];
        /* @var $channel Google_Service_YouTube_Channel */

        // Get the Id of the 'Uploads' Channel of this user
        $uploadsChannelId = $channel
            ->getContentDetails()
            ->getRelatedPlaylists()
            ->getUploads();

        return $uploadsChannelId;
    }


    /**
     * Get Snippet By VideoId
     *
     * Does a call to the YouTube API and returns a 'Snippet'
     * For a full list of all properties in a Snippet, see:
     * https://developers.google.com/youtube/v3/docs/videos#resource
     *
     * @param $videoId
     * @return Google_Service_YouTube_VideoSnippet
     */
    protected function _getVideoSnippetByVideoId($videoId) {

        // Do a List call
        try {
            $listResponse = $this->_youtube->videos->listVideos(
                "snippet",
                array(
                    'id' => $videoId
                )
            );
        } catch (Exception $e) {
            throw new M_Exception(sprintf(
                'Could not load video snippet. Id: %s',
                $videoId)
            );
        }

        // Extract the video items from our reponse
        $items = $listResponse->getItems();

        // No results found
        if(! $listResponse->getPageInfo()->getTotalResults()) {
            return null;
        }

        // Take the first item (there should only be 1, since we provide a video id)
        $video = $items[0];
        /* @var $video Google_Service_YouTube_Video */

        // From our video, take the snippet, which is all we need
        $snippet = $video->getSnippet();

        return $snippet;
    }
}