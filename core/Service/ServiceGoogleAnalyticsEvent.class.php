<?php
/**
 * M_ServiceGoogleAnalyticsEvent
 *
 * This class can be used to create objects that represent an event on the website,
 * in Google Analytics. Typically, an object of this class is used to track a new
 * event on the website, such as a click on a predetermined call-to-action button.
 * For example:
 *
 * @package Core
 */
class M_ServiceGoogleAnalyticsEvent extends M_Object {

	/* -- PROPERTIES -- */

	/**
	 * The general event category (eg "Videos")
	 *
	 * @access private
	 * @var string
	 */
	private $_category;

	/**
	 * The action for the event (eg "Play")
	 *
	 * @access private
	 * @var string
	 */
	private $_action;

	/**
	 * An optional descriptor for the event
	 *
	 * @access private
	 * @var string
	 */
	private $_description;

	/**
	 * A value associated with the event
	 *
	 * @access private
	 * @var string
	 */
	private $_value;

	/* -- CONSTRUCTOR -- */

	/**
	 * Constructor
	 *
	 * @access public
	 * @param string  $category
	 * @param string  $action
	 * @param string  $description
	 * @param integer $value
	 * @return M_ServiceGoogleAnalyticsEvent
	 */
	public function __construct($category, $action, $description = NULL, $value = NULL) {
		// Set (required) category and action
		$this->setCategory($category);
		$this->setAction($action);

		// Set (optional) description
		if($description) {
			$this->setDescription($description);
		}

		// Set (optional) value
		if($value) {
			$this->setValue($value);
		}
	}

	/* -- GETTERS -- */

	/**
	 * Get the event category
	 *
	 * @see M_ServiceGoogleAnalytics::setCategory()
	 * @access public
	 * @return string
	 */
	public function getCategory() {
		return $this->_category;
	}

	/**
	 * Get the action
	 *
	 * @see M_ServiceGoogleAnalytics::setAction()
	 * @access public
	 * @return string
	 */
	public function getAction() {
		return $this->_action;
	}

	/**
	 * Get description
	 *
	 * @see M_ServiceGoogleAnalytics::setDescription()
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * Get the event's value
	 *
	 * @see M_ServiceGoogleAnalytics::setValue()
	 * @access public
	 * @return integer
	 */
	public function getValue() {
		return $this->_value;
	}

	/* -- SETTERS -- */

	/**
	 * Set category
	 *
	 * For Event Tracking, a category is a name that you supply as a way to group
	 * objects that you want to track.
	 *
	 * @access public
	 * @param string $description
	 * @return M_ServiceGoogleAnalyticsEvent $event
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCategory($category) {
		$this->_category = (string) $category;
		return $this;
	}

	/**
	 * Set action
	 *
	 * Typically, you will use the action parameter to name the type of event or
	 * interaction you want to track for a particular web object. For example,
	 * with a single "Videos" category, you can track a number of specific events
	 * with this parameter, such as:
	 *
	 * - Time when the video completes load
	 * - "Play" button clicks
	 * - "Stop" button clicks
	 * - "Pause" button clicks
	 *
	 * @access public
	 * @param string $description
	 * @return M_ServiceGoogleAnalyticsEvent $event
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAction($action) {
		$this->_action = (string) $action;
		return $this;
	}

	/**
	 * Set description
	 *
	 * @access public
	 * @param string $description
	 * @return M_ServiceGoogleAnalyticsEvent $event
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
		return $this;
	}

	/**
	 * Set Value
	 *
	 * This parameter differs from the others in that it is of type integer rather 
	 * than string, so use it to assign a numerical value to a tracked page object. 
	 * For example, you could use it to provide the time in seconds for an player
	 * to load, or you might trigger a dollar value when a specific playback
	 * marker is reached on a video player.
	 *
	 * The value is interpreted as a number and the report adds the total values
	 * based on each event count (see Implicit Count below). The report also
	 * determines the average value for the category. In our video example, an
	 * event is tracked for the "Video Load Time" action when video load completes.
	 * The name of the video is provided as a label, and the computed load time
	 * is accrued for each video download. You could then determine average load
	 * time for all "Video Load Time" actions for the "Videos" category.
	 *
	 * @access public
	 * @param integer $value
	 * @return M_ServiceGoogleAnalyticsEvent $event
	 *		Returns itself, for a fluent programming interface
	 */
	public function setValue($value) {
		$this->_value = (int) $value;
		return $this;
	}
}