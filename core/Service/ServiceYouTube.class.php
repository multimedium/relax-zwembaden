<?php
/**
 * M_ServiceYouTube
 * 
 * ... contains functions to communicate with YouTube
 *
 * @author Tim Segers
 * @see http://code.google.com/intl/nl/apis/youtube/getting_started.html
 * @package Core
 */

class M_ServiceYouTube extends M_Service {

	/**
	 * Get videos from a given user
	 *
	 * This function will return an M_ArrayIterator filled with
	 * M_ServiceYouTubeVideo objects, including all videos from the specified
	 * user. As of yet this function expects a string as username directly,
	 * rather then the entire M_ServiceYouTubeUser object. To get all videos
	 * directly from an M_ServiceYouTubeUser, simply call the getVideos()
	 * function on the M_ServiceYouTubeUser object itself.
	 *
	 * @access public
	 * @param string $username
	 * @return M_ArrayIterator $out
	 *		The requested videos of the indicated user
	 */
	public function getVideosByUsername($username) {

        $serviceYouTubeHelper = new M_ServiceYouTubeHelper();

        $videos = $serviceYouTubeHelper->getVideosByUsername($username);

        return $videos;
	}

	/**
	 * Get video metadata by a given video Id
	 *
	 * This function will collect the metadata of a certain video, starting
	 * from a given video ID. This video ID is usually something like
	 * L5NjeNUVkYE or 4lQ1Pz_O-j0, e.g. a random looking string unique for
	 * every video on YouTube.
	 *
	 * @access public
	 * @param string $videoId
	 * @return M_ServiceYouTubeVideo
	 */
	public function getVideoByVideoId($videoId) {

        $serviceYouTubeHelper = new M_ServiceYouTubeHelper();

        $video = $serviceYouTubeHelper->getVideoById($videoId);

        return $video;
	}
}