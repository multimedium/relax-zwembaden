<?php
/**
 * M_ServiceFlickrPhoto
 * 
 * ... contains functions to handle Flickr Photos
 * 
 * @package Core
 */
class M_ServiceFlickrPhoto extends M_Object {
	
	/* -- PROPERTIES -- */
	
	private $_flickrService;
	private $_photoId;
	private $_userId;
	private $_title;
	private $_isPublic;
	private $_dateUploaded;
	private $_dateTaken;
	private $_latitude;
	private $_longitude;
	private $_tags;
	private $_numberOfViews;
	private $_mediaType;
	private $_mediaStatus;
	private $_square = array();
	private $_thumbnail = array();
	private $_medium = array();
	private $_image = array();
	private $_large = array();
	
	/* -- CONSTRUCTORS -- */
	
	public function __construct(M_ServiceFlickr $flickr, $photoId) {
		$this->_flickrService = $flickr;
		$this->setPhotoId($photoId);
	}
	
	/* -- GETTERS -- */
	
	public function getPhotoId() {
		return $this->_photoId;
	}
	
	public function getUserId() {
		return $this->_userId;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function isPublic() {
		return $this->_isPublic;
	}
	
	public function getDateUploaded() {
		return $this->_dateUploaded;
	}
	
	public function getDateTaken() {
		return $this->_dateTaken;
	}
	
	public function getLatitude() {
		return $this->_latitude;
	}
	
	public function getLongitude() {
		return $this->_longitude;
	}
	
	public function getTags() {
		return $this->_tags;
	}
	
	public function getNumberOfViews() {
		return $this->_numberOfViews;
	}
	
	public function getMediaType() {
		return $this->_mediaType;
	}
	
	public function getMediaStatus() {
		return $this->_mediaStatus;
	}
	
	public function getUriOfThumbnailSquare() {
		return (isset($this->_square[0]) ? $this->_square[0] : NULL);
	}
	
	public function getWidthOfThumbnailSquare() {
		return (isset($this->_square[1]) ? $this->_square[1] : NULL);
	}
	
	public function getHeightOfThumbnailSquare() {
		return (isset($this->_square[2]) ? $this->_square[2] : NULL);
	}
	
	public function getUriOfThumbnail() {
		return (isset($this->_thumbnail[0]) ? $this->_thumbnail[0] : NULL);
	}
	
	public function getWidthOfThumbnail() {
		return (isset($this->_thumbnail[1]) ? $this->_thumbnail[1] : NULL);
	}
	
	public function getHeightOfThumbnail() {
		return (isset($this->_thumbnail[2]) ? $this->_thumbnail[2] : NULL);
	}
	
	public function getUriOfMediumImage() {
		return (isset($this->_medium[0]) ? $this->_medium[0] : NULL);
	}
	
	public function getWidthOfMediumImage() {
		return (isset($this->_medium[1]) ? $this->_medium[1] : NULL);
	}
	
	public function getHeightOfMediumImage() {
		return (isset($this->_medium[2]) ? $this->_medium[2] : NULL);
	}
	
	public function getUriOfImage() {
		return (isset($this->_image[0]) ? $this->_image[0] : NULL);
	}
	
	public function getWidthOfImage() {
		return (isset($this->_image[1]) ? $this->_image[1] : NULL);
	}
	
	public function getHeightOfImage() {
		return (isset($this->_image[2]) ? $this->_image[2] : NULL);
	}
	
	public function getUriOfLargeImage() {
		return (isset($this->_large[0]) ? $this->_large[0] : NULL);
	}
	
	public function getWidthOfLargeImage() {
		return (isset($this->_large[1]) ? $this->_large[1] : NULL);
	}
	
	public function getHeightOfLargeImage() {
		return (isset($this->_large[2]) ? $this->_large[2] : NULL);
	}
	
	/* -- SETTERS -- */
	
	public function setPhotoId($photoId) {
		$this->_photoId = (string) $photoId;
	}
	
	public function setUserId($userId) {
		$this->_userId = (string) $userId;
	}
	
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	public function setIsPublic($flag) {
		$this->_isPublic = (bool) $flag;
	}
	
	public function setDateUploaded(M_Date $date) {
		$this->_dateUploaded = $date;
	}
	
	public function setDateTaken(M_Date $date) {
		$this->_dateTaken = $date;
	}
	
	public function setLatitude($latitude) {
		$this->_latitude = (float) $latitude;
	}
	
	public function setLongitude($longitude) {
		$this->_longitude = (float) $longitude;
	}
	
	public function setTags($tags) {
		$this->_tags = (string) $tags;
	}
	
	public function setNumberOfViews($numberOfViews) {
		$this->_numberOfViews = (int) $numberOfViews;
	}
	
	public function setMediaType($type) {
		$this->_mediaType = (string) $type;
	}
	
	public function setMediaStatus($status) {
		$this->_mediaStatus = (string) $status;
	}
	
	public function setUriOfThumbnailSquare(M_Uri $uri) {
		$this->_square[0] = $uri;
	}
	
	public function setWidthOfThumbnailSquare($width) {
		$this->_square[1] = $width;
	}
	
	public function setHeightOfThumbnailSquare($height) {
		$this->_square[2] = $height;
	}
	
	public function setUriOfThumbnail(M_Uri $uri) {
		$this->_thumbnail[0] = $uri;
	}
	
	public function setWidthOfThumbnail($width) {
		$this->_thumbnail[1] = $width;
	}
	
	public function setHeightOfThumbnail($height) {
		$this->_thumbnail[2] = $height;
	}
	
	public function setUriOfMediumImage(M_Uri $uri) {
		$this->_medium[0] = $uri;
	}
	
	public function setWidthOfMediumImage($width) {
		$this->_medium[1] = $width;
	}
	
	public function setHeightOfMediumImage($height) {
		$this->_medium[2] = $height;
	}
	
	public function setUriOfImage(M_Uri $uri) {
		$this->_image[0] = $uri;
	}
	
	public function setWidthOfImage($width) {
		$this->_image[1] = $width;
	}
	
	public function setHeightOfImage($height) {
		$this->_image[2] = $height;
	}
	
	public function setUriOfLargeImage(M_Uri $uri) {
		$this->_large[0] = $uri;
	}
	
	public function setWidthOfLargeImage($width) {
		$this->_large[1] = $width;
	}
	
	public function setHeightOfLargeImage($height) {
		$this->_large[2] = $height;
	}
}