<?php
/**
 * M_ServiceYouTubeVideo
 *
 * ... contains functions to interact with a YouTube video
 *
 * @author Tim Segers
 * @package Core
 */
class M_ServiceYouTubeVideo extends M_Object {

	/* -- PROPERTIES -- */

	/**
	 * Video title
	 *
	 * Contains the video title
	 *
	 * @access private
	 * @var string
	 */
	private $_title;

	/**
	 * Video ID
	 *
	 * Contains the video ID. This is a random looking string which uniquely
	 * identifies a youtube video. Typically this ID looks something like these:
	 *		- XQtWmp-V3cI
	 *		- CElwYE1nkMc
	 *		- lAl28d6tbko
	 *
	 * @access private
	 * @var string
	 */
	private $_videoId;

	/**
	 * Video Description
	 *
	 * Contains the video description
	 *
	 * @access private
	 * @var string
	 */
	private $_description;

	/**
	 * Video Category
	 *
	 * Contains the category the video belongs in
	 *
	 * @acces private
	 * @var string
	 */
	private $_category;

	/**
	 * Video Author
	 *
	 * Contains the author of the video
	 *
	 * NOTE: the author isn't necessarily the same as the username of the one
	 * who uploaded the video.
	 *
	 * @access private
	 * @var string
	 */
	private $_author;

	/**
	 * Video Publication Date
	 *
	 * Contains the date the video was published on youtube
	 *
	 * @access private
	 * @var M_Date
	 */
	private $_publicationDate;

	/**
	 * Video Last Updated Date
	 *
	 * Contains the date the video was updated for the last time (e.g. the user
	 * deleted or added comments or changed the description etc)
	 *
	 * @access private
	 * @var M_Date
	 */
	private $_lastUpdatedDate;

	/**
	 * Video Tagline
	 *
	 * Contains a string with tags relevant to the video
	 *
	 * @access private
	 * @var string
	 */
	private $_tagline;

	/**
	 * Video Rating
	 *
	 * Contains the rating of the video. This is a float value between
	 * 0 and 5.
	 *
	 * @access private
	 * @var float
	 */
	private $_rating;

	/**
	 * Video Number of Times Favorited
	 *
	 * Contains the number of times the video has been favorited by other
	 * users browsing youtube
	 *
	 * @access private
	 * @var int
	 */
	private $_timesFavorited;

	/**
	 * Number of pageviews
	 *
	 * Contains the number of times the video has been viewed
	 *
	 * @access private
	 * @var int
	 */
	private $_numberOfPageviews;

	/**
	 * Number of ratings
	 *
	 * Contains the number of times the video has been rated
	 *
	 * @access private
	 * @var int
	 */
	private $_numberOfRatings;

	/**
	 * Number of comments
	 *
	 * Contains the number of comments posted for this video
	 *
	 * @access private
	 * @var int
	 */
	private $_numberOfComments;

	/**
	 * Video Duration
	 *
	 * Contains the length of the video in seconds
	 *
	 * @access private
	 * @var int
	 */
	private $_videoDuration;

	/**
	 * Big Thumbnail URI
	 *
	 * Contains the direct URI to the big thumbnail of this video
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_bigThumbnailUri;

	/**
	 * Small Thumbnail URI #1
	 *
	 * Contains the direct URI to the fist small thumbnail of the video
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_smallThumbUriOne;

	/**
	 * Small Thumbnail URI #2
	 *
	 * Contains the direct URI to the second small thumbnail of the video
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_smallThumbUriTwo;

	/**
	 * Small Thumbnail URI #3
	 *
	 * Contains the direct URI to the third small thumbnail of the video
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_smallThumbUriThree;

	/**
	 * Video URI
	 *
	 * Contains the direct URI to the video itself. This link brings us directly
	 * to the youtube website
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_videoUri;

	/**
	 * Video Comments Feed URI
	 *
	 * Contains the URI to the feed of comments posted for this video
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_commentsFeedUri;

	/**
	 * Related Videos Feed URI
	 *
	 * Contains the URI to the feed with videos related to this video
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_relatedVideosFeedUri;

	/**
	 * Video Responses Feed URI
	 *
	 * Contains the URI to the feed with video responses for tis videp
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_videoResponsesFeedUri;

	/**
	 * Video Mobile Feed URI
	 *
	 * Contains the link to the mobile page of this video, which
	 * is typically accessed through movile devices such as phones or smartphones.
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_videoMobileFeedUri;

	/* -- CONSTRUCTORS -- */

	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $videoId
	 * @return M_ServiceYouTubeVideo
	 */
	public function  __construct($videoId = null) {
		if(!is_null($videoId)) {
			// Set the ID:
			$this->setVideoId($videoId);
		}
	}

	/* -- STATIC CONSTRUCTORS -- */

	/**
	 * Construct with URI
	 * 
	 * @access public
	 * @param M_Uri $uri
	 *		The Youtube Video URI
	 * @return M_ServiceYouTubeVideo $video
	 *		The video object
	 */
	public static function constructWithUri(M_Uri $uri) {
		// We get the "v" parameter from the URI:
		$v = $uri->getQueryVariable('v');

		$path = str_replace('/', '', $uri->getPath());
		if($path != "watch") {
		    $v = $path;
        }

		// If the parameter could not have been found:
		if(! $v) {
			// Then, we throw an exception to inform about the error$
			throw new M_Exception(sprintf(
				'Cannot extract the Youtube Video ID from URL %s',
				$uri->toString()
			));
		}

		// We construct a Youtube Video object, with the Video ID:
		$service = new M_ServiceYouTube();
		return $service->getVideoByVideoId($v);
	}

	/* -- GETTERS -- */

	/**
	 * Get title
	 *
	 * Get the title of the video
	 *
	 * @access public
	 * @return string $title
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * Get the video id
	 *
	 * Get the ID of the video
	 *
	 * @access
	 * return string $videoId
	 */
	public function getVideoId() {
		return $this->_videoId;
	}

	/**
	 * Get description
	 *
	 * Get the description of the video
	 *
	 * @access public
	 * @return string $description
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * Get category
	 *
	 * Get the category the video belongs in
	 *
	 * @access public
	 * @return string $category
	 */
	public function getCategory() {
		return $this->_category;
	}

	/**
	 * Get author
	 *
	 * Get the name of the author of this video
	 *
	 * NOTE: This value doesn't necessarily reflect the youtube user who
	 * uploaded the video.
	 *
	 * @access public
	 * @return string $author
	 */
	public function getAuthor() {
		return $this->_author;
	}

	/**
	 * Get publication date
	 *
	 * Get the video's publication date
	 *
	 * @access public
	 * @return M_Date $publicationDate
	 */
	public function getPublicationDate() {
		return $this->_publicationDate;
	}

	/**
	 * Get last date updated
	 *
	 * Get the last date this video (or its metadata) was updated
	 * 
	 * @access public
	 * @return M_Date $lastUpdated
	 */
	public function getLastUpdatedDate() {
		return $this->_lastUpdatedDate;
	}

	/**
	 * Get tagline
	 *
	 * Get the video's tagline
	 *
	 * @access public
	 * @return string $tagline
	 */
	public function getTagline() {
		return $this->_tagline;
	}

	/**
	 * Get number of pageviews
	 *
	 * Get the number of times this video has been viewed on youtube
	 *
	 * @access public
	 * @return int $numberOfPageviews
	 */
	public function getNumberOfPageViews() {
		return $this->_numberOfPageviews;
	}

	/**
	 * Get the video rating
	 *
	 * Get the rating this video received through user voting on youtube. This
	 * returns a value between 0 and 5 as a float.
	 *
	 * @access public
	 * @return float $rating
	 */
	public function getRating() {
		return $this->_rating;
	}

	/**
	 * Get number of ratings
	 *
	 * Get the number of times this video has been rated
	 *
	 * @access public
	 * @return integer $numberOfRatings
	 */
	public function getNumberOfRatings() {
		return $this->_numberOfRatings;
	}

	/**
	 * Get the number of comments
	 *
	 * Get the number of comments that were posted for this video so far
	 *
	 * @access public
	 * @return int $numberOfComments
	 */
	public function getNumberOfComments() {
		return $this->_numberOfComments;
	}

	/**
	 * Get Video Times Favorites
	 *
	 * Get the number of times this video has been favorited
	 *
	 * @access public
	 * @return int $timesFavorited
	 */
	public function getTimesFavorited() {
		return $this->_timesFavorited;
	}

	/**
	 * Get the big thumbnail uri
	 *
	 * Will provide the big thumbnail image, which is typically the first
	 * frame in the video
	 *
	 * @access public
	 * @return M_Uri $bigThumbnailUri
	 */
	public function getBigThumbnailUri() {
		return $this->_bigThumbnailUri;
	}

	/**
	 * Get the first small thumbnail of the video
	 *
	 * @access public
	 * @return M_Uri $smallThumbUriOne
	 */
	public function getSmallThumbUriOne() {
		return $this->_smallThumbUriOne;
	}

	/**
	 * Get the second small thumbnail of the video
	 *
	 * @access public
	 * @return M_Uri $smallThumbUriTwo
	 */
	public function getSmallThumbUriTwo() {
		return $this->_smallThumbUriTwo;
	}

	/**
	 * Get the third small thumbnail of the video
	 *
	 * @access public
	 * @return M_Uri $smallThumbUriThree
	 */
	public function getSmallThumbUriThree() {
		return $this->_smallThumbUriThree;
	}
	
	/**
	 * Get the uri to the comments feed related to this video
	 * 
	 * @access public
	 * @return M_Uri $commentsFeedUri
	 */
	public function getCommentsFeedUri() {
		return $this->_commentsFeedUri;
	}

	/**
	 * Get the uri to the feed of videos related to this video
	 *
	 * @access public
	 * @return M_Uri $relatedVideosFeedUri
	 */
	public function getRelatedVideosFeedUri() {
		return $this->_relatedVideosFeedUri;
	}

	/**
	 * Get the uri to the feed of videoresponses related to this video
	 *
	 * @access public
	 * @return M_Uri $videoResponsesFeedUri
	 */
	public function getVideoResponsesFeedUri() {
		return $this->_videoResponsesFeedUri;
	}

	/**
	 * Get the uri to the feed of mobile versions of this video
	 *
	 * @access public
	 * @return M_Uri $videoMobileFeedUri
	 */
	public function getVideoMobileFeedUri() {
		return $this->_videoMobileFeedUri;
	}

	/**
	 * Get the direct video Uri
	 *
	 * This URI typically leads to the direct youtube page of this video
	 * 
	 * @access public
	 * @return M_Uri $videoUri
	 */
	public function getVideoUri() {
		return $this->_videoUri;
	}

	/**
	 * Get the duration of the video in seconds
	 *
	 * @access public
	 * @return int $videoDuration
	 */
	public function getVideoDuration() {
		return $this->_videoDuration;
	}

	/* -- SETTERS -- */

	/**
	 * Set the video title
	 *
	 * @access public
	 * @param string $title
	 * @return M_ServiceYouTubeVideo
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
		return $this;
	}

	/**
	 * Set the video id
	 *
	 * @access public
	 * @param string $videoId
	 * @return M_ServiceYouTubeVideo
	 */
	public function setVideoId($videoId) {
		$this->_videoId = (string) $videoId;
		return $this;
	}

	/**
	 * Set the video description
	 *
	 * @access public
	 * @param string $description
	 * @return M_ServiceYouTubeVideo
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
		return $this;
	}

	/**
	 * Set the video category
	 *
	 * @access public
	 * @param string $category
	 * @return M_ServiceYouTubeVideo
	 */
	public function setCategory($category) {
		$this->_category = (string) $category;
		return $this;
	}

	/**
	 * Set the author of the video
	 *
	 * @access public
	 * @param string $author
	 * @return M_ServiceYouTubeVideo
	 */
	public function setAuthor($author) {
		$this-> _author = (string) $author;
		return $this;
	}

	/**
	 * Set the video publication date
	 *
	 * @access public
	 * @param M_Date $publicationDate
	 * @return M_ServiceYouTubeVideo
	 */
	public function setPublicationDate(M_Date $publicationDate) {
		$this-> _publicationDate = $publicationDate;
		return $this;
	}

	/**
	 * Set the date the video was last update
	 *
	 * @access public
	 * @param M_Date $lastUpdatedDate
	 * @return M_ServiceYouTubeVideo
	 */
	public function setLastUpdatedDate(M_Date $lastUpdatedDate) {
		$this-> _lastUpdatedDate = $lastUpdatedDate;
		return $this;
	}

	/**
	 * Set the video tagline
	 *
	 * @access public
	 * @param string $tagline
	 * @return M_ServiceYouTubeVideo
	 */
	public function setTagline($tagline) {
		$this-> _tagline = (string )$tagline;
		return $this;
	}

	/**
	 * Set the number of pageviews
	 *
	 * @access public
	 * @param string $numberOfPageviews
	 * @return M_ServiceYouTubeVideo
	 */
	public function setNumberOfPageviews($numberOfPageviews) {
		$this-> _numberOfPageviews = (int) $numberOfPageviews;
		return $this;
	}

	/**
	 * Set the rating
	 *
	 * @access public
	 * @param float $rating
	 * @return M_ServiceYouTubeVideo
	 */
	public function setRating($rating) {
		$this-> _rating = (float) $rating;
		return $this;
	}

	/**
	 * Set the number of ratings
	 *
	 * @access public
	 * @param int $numberOfRatings
	 * @return M_ServiceYouTubeVideo
	 */
	public function setNumberOfRatings($numberOfRatings) {
		$this-> _numberOfRatings = (int) $numberOfRatings;
		return $this;
	}

	/**
	 * Set the number of comments
	 *
	 * @access public
	 * @param int $numberOfComments
	 * @return M_ServiceYouTubeVideo
	 */
	public function setNumberOfComments($numberOfComments) {
		$this->_numberOfComments = (int) $numberOfComments;
		return $this;
	}

	/**
	 * Set the number of times the video has been favorited
	 *
	 * @access public
	 * @param int $timesFavorited
	 * @return M_ServiceYouTubeVideo
	 */
	public function setTimesFavorited($timesFavorited) {
		$this-> _timesFavorited = (int) $timesFavorited;
		return $this;
	}

	/**
	 * Set the big thumbnail Uri
	 *
	 * @access public
	 * @param M_Uri $bigThumbnailUri
	 * @return M_ServiceYouTubeVideo
	 */
	public function setBigThumbnailUri(M_Uri $bigThumbnailUri) {
		$this->_bigThumbnailUri = $bigThumbnailUri;
		return $this;
	}

	/**
	 * Set the uri of the first small thumb
	 *
	 * @access public
	 * @param M_Uri $smallThumbOne
	 * @return M_ServiceYouTubeVideo
	 */
	public function setSmallThumbUriOne(M_Uri $smallThumbUriOne) {
		$this->_smallThumbUriOne = $smallThumbUriOne;
		return $this;
	}

	/**
	 * Set the uri of the second small thumb
	 *
	 * @access public
	 * @param M_Uri $smallThumbTwo
	 * @return M_ServiceYouTubeVideo
	 */
	public function setSmallThumbUriTwo(M_Uri $smallThumbUriTwo) {
		$this->_smallThumbUriTwo = $smallThumbUriTwo;
		return $this;
	}
	
	/**
	 * Set the uri of the third small thumb
	 *
	 * @access public
	 * @param M_Uri $smallThumbThree
	 * @return M_ServiceYouTubeVideo
	 */
	public function setSmallThumbUriThree(M_Uri $smallThumbUriThree) {
		$this->_smallThumbUriThree = $smallThumbUriThree;
		return $this;
	}

	/**
	 * Set the uri to the video comments feed
	 *
	 * @access public
	 * @param M_Uri $commentsFeeduri
	 * @return M_ServiceYouTubeVideo
	 */
	public function setCommentsFeedUri(M_Uri $commentsFeedUri) {
		$this->_commentsFeedUri = $commentsFeedUri;
		return $this;
	}

	/**
	 * Set the uri to the related video feed
	 *
	 * @access public
	 * @param M_Uri $relatedVideosFeedUri
	 * @return M_ServiceYouTubeVideo
	 */
	public function setRelatedVideosFeedUri(M_Uri $relatedVideosFeedUri) {
		$this->_relatedVideosFeedUri = $relatedVideosFeedUri;
		return $this;
	}

	/**
	 * Set the uri to the videoresponses feed uri
	 *
	 * @access public
	 * @param M_Uri $videoResponsesFeedUri
	 * @return M_ServiceYouTubeVideo
	 */
	public function setVideoResponsesFeedUri(M_Uri $videoResponsesFeedUri) {
		$this->_videoResponsesFeedUri = $videoResponsesFeedUri;
		return $this;
	}

	/**
	 * Set the uri to the mobile page of the video
	 *
	 * @access public
	 * @param M_Uri $videoMobileFeedUri
	 * @return M_ServiceYouTubeVideo
	 */
	public function setVideoMobileFeedUri(M_Uri $videoMobileFeedUri) {
		$this->_videoMobileFeedUri = $videoMobileFeedUri;
		return $this;
	}

	/**
	 * Set the direct Uri to the video
	 *
	 * @access public
	 * @param M_Uri $videoUri
	 * @return M_ServiceYouTubeVideo
	 */
	public function setVideoUri(M_Uri $videoUri) {
		$this->_videoUri = $videoUri;
		return $this;	
	}

	/**
	 * Set the duration of the video in seconds
	 *
	 * @access public
	 * @param int $videoDuration
	 * @return M_ServiceYouTubeVideo
	 */
	public function setVideoDuration($videoDuration) {
		$this->_videoDuration = (int) $videoDuration;
		return $this;
	}

	/* -- OTHER FUNCTIONS -- */

	/**
	 * Get Video View
	 *
	 * This function will create a video view for a given video object
	 * and return it.
	 *
	 * @access public
	 * @return M_ServiceYouTubeVideoView
	 */
	public function getView() {
		$videoView = new M_ServiceYouTubeVideoView();
		$videoView->setVideo($this);

		return $videoView;
	}
}