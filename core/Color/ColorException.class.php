<?php
/**
 * M_ColorException class
 *
 * Exceptions thrown by the {@link M_Color} class
 * 
 * @package Core
 */
class M_ColorException extends Exception {
}
?>