<?php
/**
 * Registry of (global) data
 *
 * The Registry Pattern allows the storing and retrieval of data and 
 * objects which need to be accessed globally in a single centralised 
 * object. In order to achieve this, the Registry is implemented as a 
 * singleton object.
 * 
 * M_Registry is a simple class that implements a private array to 
 * store objects and four methods for registering, unregistering, 
 * retrieving and checking the existence of objects.
 * 
 * Think of it as a basket; using a Registry you can add data (both 
 * values and objects) to the basket, and retrieve them as required 
 * from other parts of an application. Since all such data is handled 
 * by a single Registry object, it makes passing data and objects 
 * around an application far more simple than passing all such values 
 * as individual parameters to a constructor or setup method.
 * 
 * NOTE:
 * Registry documentation extracted and summarized from the original 
 * text at:
 * 
 * http://www.patternsforphp.com/wiki/Registry
 * 
 * @package Core
 */
class M_Registry extends M_ObjectSingleton implements Countable, Iterator {
	
	/**
	 * The singleton object
	 * 
	 * @static
	 * @access private
	 * @var M_Messenger
	 */
	private static $_instance;
	
	/**
	 * The data store
	 * 
	 * The data store contains all of the data that has been 
	 * registered in M_Registry.
	 * 
	 * @access private
	 * @var array
	 */
	private $_store = array(array());
	
	/**
	 * Autoload definitions
	 * 
	 * Read the documentation on {@link M_Registry::setAutoLoad()} 
	 * to learn more about autoloading data.
	 * 
	 * @access private
	 * @var array
	 */
	private $_autoload = array();
	
	/**
	 * Countable support
	 * 
	 * This is an internal variable to support the Countable interface.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_count;
	
	/**
	 * Iterator support
	 * 
	 * This is an internal variable to support the Iterator interface. 
	 * More specifically, this is the current cursor position.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_index;
	
	/**
	 * (Protected) constructor
	 * 
	 * The signature on the M_Registry class constructor is PROTECTED.
	 * This way, we force the M_Registry object to be a singleton, as
	 * objects can only be instantiated by the class itself.
	 * 
	 * @see M_Registry::getInstance()
	 * @access protected
	 * @return M_Registry
	 */
	protected function __construct() {
		$this->_count = 0;
		$this->_index = 0;
	}
	
	/**
	 * Singleton constructor
	 * 
	 * @static
	 * @access public
	 * @return M_Registry $registry
	 * 		The singleton object
	 */
	public static function getInstance() {
		// If not requested before
		if(! self::$_instance) {
			// Then, construct now:
			self::$_instance = new self;
		}
		
		// Return the singleton object
		return self::$_instance;
	}
	
	/**
	 * Register data in the Registry
	 * 
	 * This method will register the provided data, under the provided
	 * name, in the M_Registry object. This way, it can be retrieved 
	 * later on by using the {@link M_Registry::get()} method.
	 * 
	 * @access public
	 * @param string $label
	 * 		The name that is being given to the registered data/object. 
	 * 		This name can be any name you wish. However, you should 
	 * 		look for short and descriptive names. 
	 * @param mixed $object
	 * 		The data/object to be registered in the M_Registry, under 
	 * 		the provided name.
	 * @return void
	 */
	public function register($label, $object) {
		$this->_store[0][$label] = $object;
		$this->_count = count($this->_store[0]);
	}
	
	/**
	 * Unregister data from the Registry
	 * 
	 * This method will remove a previously registered data/object 
	 * from the global Registry. By doing so, the data can no longer 
	 * be retrieved with the {@link M_Registry::get()} method.
	 * 
	 * @access public
	 * @param string $label
	 * 		The label under which the data/object has been registered 
	 * 		previously with the {@link M_Registry::register()} method.
	 * @return void
	 */
	public function unregister($label) {
		if(isset($this->_store[0][$label])) {
			unset($this->_store[0][$label]);
			$this->_count = count($this->_store[0]);
		}
	}
	
	/**
	 * Set an autoloading
	 * 
	 * In some cases, you might want to register an object that has 
	 * not yet been constructed. This is specially handy if you want 
	 * to register singleton objects in the registry, that have not 
	 * yet been constructed.
	 * 
	 * This method allows you to store autoloading information. When 
	 * the object is being retrieved from the Registry, with the 
	 * {@link M_Registry::get()} method, and the object has not yet 
	 * been constructed, the Registry will construct the object 
	 * automatically.
	 * 
	 * @access public
	 * @param string $label
	 * 		The name under which to store the autoloader. This is the 
	 * 		name with which you will later on retrieve the data/object 
	 * 		with the {@link M_Registry::get()} method. Think of this 
	 * 		parameter as exactly the same name you pass in the 
	 * 		{@link M_Registry::get()} method.
	 * @param string $class
	 * 		The class name. This is the class of which an object will 
	 * 		be constructed.
	 * @param boolean $singleton
	 * 		If set to TRUE, M_Registry will use the typical 
	 * 		::getInstance() method to construct the object. If set to 
	 * 		FALSE, it will construct the object with the "new" keyword.
	 * @return void
	 */
	public function setAutoLoader($label, $class, $singleton = TRUE) {
		$this->_autoload[$label] = array((string) $class, (boolean) $singleton);
	}
	
	/**
	 * Retrieve an object from the registry
	 * 
	 * This method will check if a data/object has been registered 
	 * under the provided name. If so, it will return that data/object. 
	 * If no data/object exists under the provided name, M_Registry 
	 * will check if an autoloader has been defined for the name.
	 * ({@link M_Register::setAutoLoader()})
	 * 
	 * Note that, if no data/object is available for the requested 
	 * name (and no auto-loader has been defined for that name), this 
	 * method will return the provided default value (second argument) 
	 * instead.
	 * 
	 * @access public
	 * @param string $label
	 * 		The name under which the data/object has been registered 
	 * 		before, either with {@link M_Registry::register()} or 
	 * 		{@link M_Registry::setAutoLoader()}.
	 * @param mixed $defaultValue
	 * 		The default value to be returned
	 * @return mixed
	 */
	public function get($label, $defaultValue = NULL) {
		// If the object has been constructed, and registered:
		if(isset($this->_store[0][$label])) {
			return $this->_store[0][$label];
		}
		// If the requested object has not been registered before, we check
		// if an autoload has been defined for the label
		elseif(isset($this->_autoload[$label])) {
			if($this->_autoload[$label][1]) {
				$this->register($label, call_user_func(array($this->_autoload[$label][0], 'getInstance')));
				return $this->_store[0][$label];
			} else {
				$this->register($label, new $this->_autoload[$label][0]);
				return $this->_store[0][$label];
			}
		}
		
		// if we're still here, return FAILURE!
		return $defaultValue;
	}
	
	/**
	 * Backup registry data
	 * 
	 * Test-friendly isolation methods for original data.
	 * 
	 * @access public
	 * @return void
	 */
	public function backup() {
		array_unshift($this->store, array());
	}
	
	/**
	 * Restore registry data
	 * 
	 * Test-friendly isolation methods for original data.
	 * 
	 * @access public
	 * @return void
	 */
	public function restore() {
		array_shift($this->store);
	}

	/**
	 * Magic Setter
	 * 
	 * Typically, you will access the variables in the Registry by 
	 * directly addressing them by their name. PHP will automatically
	 * call the "magic method" __set(), to set the value of the
	 * variable. For example:
	 * 
	 * @example
	 * <?php
	 * $registry = M_Registry::getInstance();
	 * $registry->myVariable = 'Hello World';
	 * ?>
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the variable, as you would provide to the 
	 * 		{@link M_Registry::register()} method.
	 * @param mixed $value
	 * 		The value of the variable, as you would provide to the 
	 * 		{@link M_Registry::register()} method.
	 * @return void
	 */
	public function __set($name, $value) {
		$this->register($name, $value);
	}
	
	/**
	 * Magic Getter
	 * 
	 * Typically, you will access the variables in the registry by 
	 * directly addressing them by their name. PHP will automatically
	 * call the "magic method" __get(), to retrieve the value of the
	 * requested variable. For example:
	 * 
	 * @example
	 * <?php
	 * $registry = new M_Registry('myNamespace');
	 * echo $registry->myVariable;
	 * ?>
	 *
	 * @access public
	 * @param name $name
	 * 		The name of the variable
	 * @return mixed
	 */
	public function __get($name) {
		return $this->get($name);
	}
	
	/**
	 * Overloading of isset()
	 *
	 * This method will handle the isset() call on the registry's
	 * variables.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the variable
	 * @return boolean
	 * 		Will return TRUE if the variable has been set, or FALSE
	 * 		if the variable does not exist.
	 */
	public function __isset($name) {
		// We use array_key_exists() to avoid a NULL value from being ignored
		return array_key_exists($name, $this->_store[0]);
	}
	
	/**
	 * Overloading of unset()
	 * 
	 * This method will handle the unset() call on the registry's
	 * variables.
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the variable
	 * @return void
	 */
	public function __unset($name) {
		$this->unregister($name);
	}
	
	/**
	 * Support for the Countable interface
	 *
	 * @access public
	 * @return integer
	 * 		The item count
	 */
	public function count() {
		return $this->_count;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return mixed
	 * 		The current item in stored data
	 */
	public function current() {
		return current($this->_store[0]);
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return string
	 * 		The key of the current item in stored data
	 */
	public function key() {
		return key($this->_store[0]);
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Moves the internal cursor to the next item in stored data
	 * 
	 * @access public
	 * @return void
	 */
	public function next() {
		next($this->_store[0]);
		$this->_index += 1;
	}
	
	/**
	 * Support for the Iterator interface
	 * 
	 * Resets the internal cursor for iteration. foreach() calls on
	 * an instance of the M_Registry class will call the rewind()
	 * method, to start looping at the beginning.
	 *
	 * @access public
	 * @return void
	 */
	public function rewind() {
		reset($this->_store[0]);
		$this->_index = 0;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Will indicate if the end of the iterator has been reached.
	 * 
	 * @return boolean
	 * 		Will return TRUE if not reached the end of the iterator,
	 * 		or FALSE if all items in the iterator have been accessed
	 * 		in the loop.
	 */
	public function valid() {
		return $this->_index < $this->_count;
	}
}
?>