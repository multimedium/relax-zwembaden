<?php
// Load ADODB
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . FOLDER_THIRDPARTY . DIRECTORY_SEPARATOR . 'adodb' . DIRECTORY_SEPARATOR . 'adodb.inc.php';

// Since we're using ADODB under the hood, we write a generic driver,
// instead of writing a driver per database brand.
class M_DbDriverAdo extends M_DbDriver {
	/**
	 * This property holds the (fetch) mode in which result sets return
	 * record. By default, the fetch mode is set to M_Db::FETCH_BOTH
	 *
	 * @var string
	 */
	private $_fetchMode = M_Db::FETCH_BOTH;
	
	/**
	 * @var ADOConnection
	 */
	private $_connection;
	
	/**
	 * Connection host, user, pass, database name
	 * 
	 * @see M_DbDriverAdo::connect()
	 * @access private
	 * @var array
	 */
	private $_connectionDetails;
	
	/**
	 * Connection driver
	 * 
	 * @see M_DbDriverAdo::__construct()
	 * @access private
	 * @var string
	 */
	private $_driver;

	/**
	 * Construct a new driver
	 * 
	 * @param string $driver Id of connection
	 */
	public function __construct($driver) {
		$this->_driver = $driver;
		$conn = ADONewConnection($driver);
		$this->_connection =& $conn;
	}

	/**
	 * Connect to the database
	 *
	 * Throws exception when connection fails
	 * 
	 * @param string $host
	 * @param string $user
	 * @param string $pass
	 * @param string $dbname
	 * @return void
	 */
	public function connect($host, $user, $pass, $dbname) {
		// Note that we use NConnect() in order to force a new connection in each
		// separate driver. We do this to make sure that different instances
		// of M_DbDriverAdo do not confuse connections, by re-using Resource IDs
		if(! $this->_connection->NConnect($host, $user, $pass, $dbname)) {
			// Throw an exception to inform about the error, if we fail to
			// connect to the database:
			throw new M_DbException('Could not connect to database.');
		}
		
		// Store connection details:
		$this->_connectionDetails = array(
			'host' => $host,
			'user' => $user,
			'pass' => $pass,
			'name' => $dbname
		);
		
		// Set charset to UTF-8
		$this->_connection->Execute("set names 'utf8'");
	}
	
	/**
	 * Used to retrieve a property from the connection. Note that, if the
	 * requested property could not have been found, this method will
	 * return FALSE!
	 * 
	 * @param <type> $property
	 * @return <type>
	 */
	public function getConnectionProperty($property) {
		if(isset($this->_connection->$property)) {
			return $this->_connection->$property;
		}
		return FALSE;
	}

	/**
	 *
	 * @param <type> $sequence
	 * @param <type> $startAt
	 * @return
	 */
	public function getId($sequence = 'dbseq', $startAt = 1) {
		return $this->_connection->GenID($sequence, $startAt);
	}

	/**
	 * Start a Database-transaction
	 *
	 * NOTE: transactions for MySQL are only supported if you use MySQLT
	 * as db-driver and use InnoDb as engine
	 *
	 * @link http://dev.mysql.com/doc/refman/5.0/en/commit.html
	 * @see M_DbDriverAdo::commit();
	 * @see M_DbDriverAdo::rollback();
	 * @throws M_Exception
	 * @return void
	 */
	public function beginTransaction() {
		//check if a transaction is already started
		if($this->getTransactionCount() > 0) {
			throw new M_Exception(
				'Another transaction has already begun, please use '.
				'M_DbDriverAdo::beginSmartTransaction() if you want to use '.
				'nested transactions');
		}
		
		$this->_connection->BeginTrans();
	}
	
	/**
	 * Get the amount of open transaction
	 * 
	 * @return int 
	 */
	public function getTransactionCount() {
		return $this->_connection->transCnt;
	}
	
	/**
	 * Start a smart database-transaction
	 * 
	 * Smart transactions differ to traditional transactions in a couple of ways:
	 * - they will auto-rollback when sql-errors occur
	 * - are nestable (only the outermost block will be executed)
	 * 
	 * When you want to end a smart transaction, this can be done by calling
	 * {@link M_DbDriverAdo::completeSmartTransaction()}
	 * 
	 * @link http://phplens.com/lens/adodb/docs-adodb.htm#ex11
	 * @see M_DbDriverAdo::completeSmartTransaction(); 
	 * @return void
	 */
	public function beginSmartTransaction() {
		$this->_connection->StartTrans();
	}
	
	/**
	 * Complete a smart transaction
	 * 
	 * When one completes a smart transaction, the driver will automaticcally
	 * rollback or commit depending if an sql-error occurred.
	 * 
	 * @return bool
	 */
	public function completeSmartTransaction() {
		return $this->_connection->CompleteTrans();
	}
	
	/**
	 * Will force a smart transaction to fail
	 * 
	 * Note: rollback will only occur when 
	 * {@link M_DbDriverAdo::completeSmartTransaction()} is called
	 * 
	 * @return void 
	 */
	public function failSmartTransaction() {
		$this->_connection->FailTrans();
	}
	
	/**
	 * Check whether smart transaction has failed
	 * 
	 * Returns true if there was an error in SQL execution or FailTrans() 
	 * was called. If not within smart transaction, returns false.
	 * 
	 * @return bool 
	 */
	public function hasFailedSmartTransaction() {
		return $this->_connection->HasFailedTrans();
	}
	
	/**
	 * Commit a Database-transaction
	 *
	 * NOTE: transactions for MySQL are only supported if you use MySQLT
	 * as db-driver
	 *
	 * @link http://dev.mysql.com/doc/refman/5.0/en/commit.html
	 * @see M_DbDriverAdo::beginTransaction();
	 * @see M_DbDriverAdo::rollback();
	 * @return void
	 */
	public function commit() {
		$this->_connection->CommitTrans();
	}
	
	/**
	 * Do a rollback on a database-transaction
	 *
	 * NOTE: transactions for MySQL are only supported if you use MySQLT
	 * as db-driver
	 *
	 * @link http://dev.mysql.com/doc/refman/5.0/en/commit.html
	 * @see M_DbDriverAdo::commit();
	 * @see M_DbDriverAdo::rollback();
	 * @return void
	 */
	public function rollback() {
		$this->_connection->RollbackTrans();
	}
	
	/**
	 * Get tables
	 * 
	 * This method will return an ArrayIterator that has been populated
	 * with objects of {@link M_DbTableAdo}, each of them representing
	 * a table in the database.
	 * 
	 * NOTE:
	 * The keys of the iterator are the names of the tables.
	 *
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getTables() {
		$out = array();
		foreach($this->_connection->MetaTables() as $tableName) {
			array_push($out, $this->getTable($tableName));

		}
		return new M_ArrayIterator($out);
	}
	
	/**
	 * Returns a table object
	 *
	 * @param str $name
	 * @return M_DbTableAdo
	 */
	public function getTable($name) {
		return new M_DbTableAdo($this, $name);
	}
	
	/**
	 * Returns a new table object
	 *
	 * @param str $name
	 * @return M_DbTableAdo
	 */
	public function getNewTable() {
		return new M_DbTableAdo($this, 'untitled-table');
	}
	
	/**
	 * Construct a M_DbSelect for a given table
	 * 
	 * @param string $from
	 * @return M_DbSelect
	 */
	public function select($from = NULL) {
		if($from) {
			return $this->getTable($from)->select();
		} else {
			return new M_DbSelect($this);
		}
	}
	
	/**
	 * Runs a query, and returns the result set
	 * 
	 * @param mixed $select
	 * @return bool
	 */ 
	public function query($select) {
		if(is_object($select)) {
			if(!in_array('MI_DbSelect', class_implements($select))) {
				throw new M_DbException(sprintf('Cannot execute a database query from a "%s" object', get_class($select)));
			}
			$rs = $select->execute();
		} else {
			$args = func_get_args();
			$sql = $this->quoteIn(array_shift($args), $args);

			//in debug mode: log query
			if (M_Debug::getLogQueries(true)) {
				M_Stopwatch::start('query');
			}
			
			// If connections are to be kept alive:
			if($this->isKeepingAlive()) {
				// Then, check connection and attempt to reconnect if necessary
				$this->_checkAndKeepConnectionAlive();
			}
			
			//do we use cache?
			if ($this->getCacheMode()) {
				$rs = $this->_connection->CacheExecute(
					$this->getCacheLifetime(),
					$sql
				);
			}
			//nope: execute query
			else {
				$rs = $this->_connection->Execute($sql);
			}

			//in debug mode: log query
			if (M_Debug::getLogQueries(true)) {
				M_Stopwatch::stop('query');
				M_Debug::addToQueryLog(
					$sql,
					M_Stopwatch::getTime('query'),
					debug_backtrace()
				);
				M_Stopwatch::remove('query');
			}

			//sql could not be executed: error
			if ($rs === FALSE) {
				throw new M_DbException(sprintf(
					'Query error "%s" in query: %s',
					$this->getError(),
					$select)
				);
			}
			
			return ($rs === FALSE ? FALSE : new M_DbResultAdo($rs));
		}
	}

	/**
	 * Execute a query with a range
	 *
	 * @param MI_DbSelect|string $select
	 * @param int $offset
	 * @param int $numberOfRows
	 * @return M_DbResultAdo
	 */
	public function queryRange($select, $offset, $numberOfRows) {
		if(is_object($select)) {
			if(!in_array('MI_DbSelect', class_implements($select))) {
				throw new M_DbException(sprintf('Cannot execute a database query from a "%s" object', get_class($select)));
			}
			$select->limit($offset, $numberOfRows);
			return $select->execute();
		} else {
			//in debug mode: log query
			if (M_Debug::getLogQueries(true)) {
				M_Stopwatch::start('query');
			}
			
			// If connections are to be kept alive:
			if($this->isKeepingAlive()) {
				// Then, check connection and attempt to reconnect if necessary
				$this->_checkAndKeepConnectionAlive();
			}

			//use cache?
			if ($this->getCacheMode()) {
				$rs = $this->_connection->CacheSelectLimit(
					$this->getCacheLifetime(),
					$select,
					$numberOfRows,
					$offset
				);
			}

			//no: execute query
			else {
				$rs = $this->_connection->SelectLimit($select, $numberOfRows, $offset);
			}

			//in debug mode: log query
			if (M_Debug::getLogQueries(true)) {
				M_Stopwatch::stop('query');
				M_Debug::addToQueryLog(
					$select,
					M_Stopwatch::getTime('query'),
					debug_backtrace()
				);
			}

			//query error?
			if ($rs == false) {
				throw new M_DbException(sprintf(
					'Query error "%s" in query: %s',
					$this->getError(),
					$select)
				);
			}
		
			return ($rs === FALSE ? FALSE : new M_DbResultAdo($rs));
		}
	}
	
	/**
	 * Run insert, update or delete queries
	 *
	 * Note: provide optional placeholders as parameters
	 * 
	 * Will return TRUE on success, FALSE on failure
	 * 
	 * @param string $sql
	 * @return bool
	 */
	public function queryOp($sql) {
		$args = array();
		for($i = 1, $n = func_num_args(); $i < $n; $i ++) {
			$args[] = func_get_arg($i);
		}
		
		$sql = $this->quoteIn($sql, $args);

		//in debug mode: log query
		if (M_Debug::getLogQueries(true)) {
			M_Stopwatch::start('query');
		}
		
		// If connections are to be kept alive:
		if($this->isKeepingAlive()) {
			// Then, check connection and attempt to reconnect if necessary
			$this->_checkAndKeepConnectionAlive();
		}

		//execute this query
		$rs = $this->_connection->_Execute($sql);

		//in debug mode: log query
		if (M_Debug::getLogQueries(true)) {
			M_Stopwatch::stop('query');
			M_Debug::addToQueryLog(
				$sql,
				M_Stopwatch::getTime('query'),
				debug_backtrace()
			);
		}
		
		//query error?
		if ($rs == false) {
			throw new M_DbException(sprintf(
				'Query error "%s" in query: %s',
				$this->getError(),
				$sql)
			);
		}
		return ($rs !== FALSE);
	}

	/**
	 * Get the last insert-id
	 * 
	 * @return int
	 */
	public function getLastInsertId() {
		return $this->_connection->Insert_ID();
	}

	/**
	 * Get the number of affected rows
	 * 
	 * @return int
	 */
	public function getAffectedRows() {
		return $this->_connection->Affected_Rows();
	}

	/**
	 * Get the error message
	 * 
	 * @return string
	 */
	public function getError() {
		return $this->_connection->ErrorMsg();
	}

	/**
	 * Quote an identifier 
	 * @param string $name
	 * @return string
	 */
	public static function quoteIdentifier($name) {
		//check if first and last character are already `
		//if so: this is already quoted
		if($name{0} == '`' && substr($name, -1) == '`') return $name;

		//check if user passed table.column value
		if (strpos($name, '.')) {
			$nameArray = explode('.',$name);
			if (count($nameArray) > 2) throw new M_DbException(
				sprintf(
					'$name parameter %s should contain table.column, or column',
					$name
				)
			);
			if ($nameArray[0]!= '*') $nameArray[0] = '`'.$nameArray[0].'`';
			if ($nameArray[1]!= '*') $nameArray[1] = '`'.$nameArray[1].'`';
			$name = implode('.',$nameArray);
		}
		//if not escape whole value
		else $name = '`'. $name .'`';
		
		return $name;
	}
	
	/**
	 * Set the type of fetch-mode
	 *
	 * Different types of fetching:
	 * - assoc
	 * - num
	 * - both
	 * - obj
	 * - iter
	 * 
	 * @param string $mode
	 */
	public function setFetchMode($mode) {
		$this->_fetchMode = $mode;
	}
	
	/**
	 * Get the fetch mode
	 * 
	 * @return string
	 */
	public function getFetchMode() {
		return $this->_fetchMode;
	}
	
	/**
	 * Create table
	 * 
	 * Since {@link M_DbDriverAdo} is an implementation that is based
	 * on ADODB, this method uses ADODB's Data Dictionary. For more
	 * information, read the documentation on ADODB.
	 *
	 * @access public
	 * @param MI_DbTable $table
	 * 		The database table to be created in the database
	 * @return success
	 */
	public function createTable(MI_DbTable $table) {
		// We prepare an array with (string) field definitions
		$fields = array();
		
		// Get the primary key of the table:
		$primary = $table->getPrimaryKey();
		
		// Get the indexes in the table:
		$indexes = $table->getIndexes();
		
		// For each of the columns in the table:
		
		/* @var $column MI_DbColumn */
		foreach($table->getColumns() as $column) {
			// Start the definition of the field with its name:
			$spec = $this->quoteIdentifier($column->getName()) . ' ';
			
			// Add the type to the definition. Note that we use 
			// ADODB's data type definitions:
			switch($column->getType()) {
				case M_DbColumn::TYPE_FLOAT:
					$spec .= 'F';
					break;
				
				case M_DbColumn::TYPE_BLOB:
					$spec .= 'B';
					break;
				
				case M_DbColumn::TYPE_TEXT:
					$spec .= 'XL';
					break;
				
				case M_DbColumn::TYPE_VARCHAR:
					$spec .= 'C';
					break;
				
				case M_DbColumn::TYPE_INTEGER:
					$spec .= 'I';
					break;
					
				case M_DbColumn::TYPE_TINY_INTEGER:
					$spec .= 'I1';
					break;
					
				case M_DbColumn::TYPE_DATETIME:
					$spec .= 'T';
					break;
					
				case M_DbColumn::TYPE_DATE:
					$spec .= 'D';
					break;
			}
			
			// Add the length to the definition:
			// (if available)
			$tmp = $column->getLength();
			if($tmp) {
				$spec .= '('. $tmp .')';
			}
			
			// Add the default value to the definition:
			// (if available)
			$tmp = $column->getDefaultValue();
			if($tmp) {
				if(is_numeric($tmp)) {
					$spec .= ' DEFAULT '. $tmp;
				} else {
					$spec .= ' DEFAULT \''. str_replace('\'', '\\\'', $tmp) . '\'';
				}
			}
			
			// If the option UNSIGNED has been enabled for the column,
			// we add the option to the definition string:
			if($column->isUnsigned()) {
				$spec .= ' UNSIGNED';
			}
			
			// If the option NOT-NULL has been enabled for the column,
			// we add the option to the definition string:
			if($column->isNotNull()) {
				$spec .= ' NOTNULL ';
			}
			
			// If the option AUTO-INCREMENT has been enabled for the 
			// column, we add the option to the definition string:
			if($column->isAutoIncrement()) {
				$spec .= ' AUTOINCREMENT';
			}
			
			// For each of the indexes in the table:
			foreach($indexes as $index) {
				if($index->containsColumn($column)) {
					$spec .= ' '. ($index->isUnique() ? 'UNIQUE INDEX' : 'INDEX') . ' ' . $index->getName();
				}
			}
			
			// Add the primary-key signature to the definition, if
			// this field is part of the primary key:
			if($primary && $primary->containsColumn($column)) {
				$spec .= ' PRIMARY';
			}
			
			// Add the charset for this column
			//@todo: this gets removed by adodb, is this valid SQL?
			if ($column->getCharset()) {
			    $spec .= ' CHARACTER SET = ' . $column->getCharset();
			}
			
			// Add the collation for this column
			//@todo: this gets removed by adodb, is this valid SQL?
			if ($column->getCollate()) {
			    $spec .= ' COLLATE ' . $column->getCollate();
			}
			
			// Add the comments for this column
			//@todo: this gets removed by adodb, is this valid SQL?
		    if ($column->getComments()) {
			    $spec .= ' COMMENT ' . $this->quote($column->getComments());
			}

			// Add field definition to the collection:
			$fields[] = $spec;
			
		}
		
		// Construct ADODB Data Dictionary object:
		$dictionary = NewDataDictionary($this->_connection);
		
		// Set the table options
		$mysqlOptions = ' ENGINE = '.(string)$table->getEngine();
		if ($table->getCharset()) $mysqlOptions .= ' CHARACTER SET '.(string)$table->getCharset();
		if ($table->getCollate()) $mysqlOptions .= ' COLLATE '.(string)$table->getCollate();
		if ($table->getComments()) $mysqlOptions .= ' COMMENT = '.(string)$this->quote($table->getComments());
		$tableoptions = array(
			'mysql' => $mysqlOptions
		);
		
		// If connections are to be kept alive:
		if($this->isKeepingAlive()) {
			// Then, check connection and attempt to reconnect if necessary
			$this->_checkAndKeepConnectionAlive();
		}
		
		// Parse the definition strings into an SQL. The resulting
		// SQL can be used to create the table.
		$sqlArray = $dictionary->CreateTableSQL($table->getName(), implode(', ', $fields) , $tableoptions);

		// Run the SQL:
		$dictionary->ExecuteSQLArray($sqlArray);
	}
	
	/**
	 * Alter a table
	 *
	 * @internal Not implemented yet
	 * @param MI_DbTable $table
	 */
	public function alterTable(MI_DbTable $table) {
		throw new M_DbException('Alter table not implemented yet');
	}

	/**
	 * Drop a table
	 * 
	 * @param MI_DbTable $table
	 * @return bool
	 */
	public function dropTable(MI_DbTable $table) {
		// If connections are to be kept alive:
		if($this->isKeepingAlive()) {
			// Then, check connection and attempt to reconnect if necessary
			$this->_checkAndKeepConnectionAlive();
		}
		
		// Drop the table
		return $this->_connection->_Execute(sprintf(
			$this->_connection->_dropSeqSQL,
			$table->getName())
		);
	}
	
	/**
	 * Get indexes
	 *
	 * This method will return the collection of column names that are
	 * used as an index in a given table. The return value of this method
	 * is an array:
	 * 
	 * <code>
	 *    Array (
	 *       [name-of-index-1] => [array-of-column-names],
	 *       [name-of-index-2] => [array-of-column-names],
	 *       ...
	 *    )
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * This method does NOT form part of the public {@link MI_Db} interface.
	 * {@link M_DbDriverAdo} implements this method, so it can be called
	 * by the class {@link M_DbTableAdo}. 
	 * 
	 * In other words, this method is a driver-specific method, and you
	 * should never call it directly. Instead, you should use the
	 * methods
	 * 
	 * - {@link M_DbTableAdo::getIndexes()}
	 * - {@link M_DbTableAdo::getPrimaryKey()}
	 * 
	 * For example:
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $table = $db->getTable('news');
	 *    $index = $table->getPrimaryKey();
	 * </code>
	 * 
	 * @access public
	 * @param string $table
	 * 		The table of which you wish to fetch the 
	 * @param boolean $primary
	 * 		Set to TRUE if you want to fetch primary keys only
	 * @return array
	 */
	public function getIndexes($table, $primary = FALSE) {
		// Output array
		$out = array();
			
		// If connections are to be kept alive:
		if($this->isKeepingAlive()) {
			// Then, check connection and attempt to reconnect if necessary
			$this->_checkAndKeepConnectionAlive();
		}
		
		// Get the indexes:
		$ind = $this->_connection->MetaIndexes((string) $table, $primary);
		
		// If an array is retrieved:
		if(is_array($ind) && is_array($ind)) {
			// For each of entries:
			foreach($ind as $name => $index) {
				// Add to output:
				$out[$name] = $index['columns'];
			}
		}
		
		// Return output
		return $out;
	}

	/**
	 * Close the db connection
	 *
	 * @return void
	 */
	public function close() {
		$this->_connection->Close();
		// should this method also unregister() the connection from the
		// M_Db class?
	}

	/**
	 * Set the directory to which the cache files should be saved
	 * 
	 * @param M_Directory $directory
	 * @return M_DbDriverAdo
	 */
	public function setCacheDirectory(M_Directory $directory) {
		//unfortunately ADODB uses global variables to store the cache-directory
		$GLOBALS['ADODB_CACHE_DIR'] = M_Loader::getAbsolute($directory->getPath());
		return $this;
	}

	/**
	 * Delete all cache from the cache-directory
	 *
	 * @return void
	 */
	public function flushCache() {
		$this->_connection->CacheFlush();
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Attempt reconnection
	 * 
	 * When connections are kept alive, the database driver checks whether or 
	 * not the connection to the server is working on each query executed. If it 
	 * has gone down, an automatic reconnection is attempted by this function.
	 * For more information, read the docs on {@link M_DbDriver::setKeepAlive()}.
	 * 
	 * @access protected
	 * @return bool $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	protected function _checkAndKeepConnectionAlive() {
		// Is the connection still working?
		$connected = FALSE;
		
		// If the driver used is MySQL:
		if(! strncmp($this->_driver, 'mysql', 5)) {
			// Then, use mysql_ping() to check the connection. We use this
			// function because the ADODB Library does not provide with an 
			// abstraction for this:
			$connected = mysql_ping($this->_connection->_connectionID);
		}
		// If using another driver:
		else {
			// Then, we reconnect
			throw new M_Exception(sprintf(
				'Keeping alive a connection is currently not supported for ' . 
				'driver %s'
			));
		}
		
		// If the connection is not working:
		if(! $connected) {
			// We have to close the connection (even though its not currently 
			// working) for it to recreate properly:
			$this->_connection->Disconnect();
			
			// And, recreate the connection:
			$this->connect(
				$this->_connectionDetails['host'], 
				$this->_connectionDetails['user'], 
				$this->_connectionDetails['pass'], 
				$this->_connectionDetails['name']
			);
		}
	}
}