<?php
interface MI_DbColumn {
	public function __construct($name = NULL);
	
	// Getters of column properties:
	public function getName();
	public function getType();
	public function getLength();
	public function getCollate();
	public function getCharset();
	public function getComments();
	public function getDefaultValue();
	public function isAutoIncrement();
	public function isNotNull();
	public function isUnsigned();
	
	// Setters:
	public function setName($name);
	public function setType($type);
	public function setLength($length);
	public function setCollate($collate);
	public function setCharset($charset);
	public function setComments($comments);
	public function setDefaultValue($value);
	public function setIsAutoIncrement($flag);
	public function setIsNotNull($flag);
	public function setIsUnsigned($flag);
}