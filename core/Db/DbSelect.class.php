<?php
/**
 * M_DbSelect class
 * 
 * The SELECT statement has been standardized to a great degree. Nearly
 * every database supports the following:
 * 
 * <code>
 *    SELECT [cols] FROM [tables]
 *    [WHERE conditions]
 *    [GROUP BY cols]
 *    [HAVING conditions] 
 *    [ORDER BY cols]
 * </code>
 * 
 * Given the standardization of the SELECT statement, we use 
 * M_DbSelect - an implementation of {@link MI_DbSelect} - as a 
 * shared implementation for all database drivers.
 * 
 * @package Core
 */
class M_DbSelect extends M_DbQuery implements MI_DbSelect {
	/**
	 * Constant to indicate merging mode
	 * 
	 * This constant is used to indicate the mode in which 2 M_DbSelect
	 * objects should be merged. This constant will cause the merged
	 * select object to overwrite the original object, where values
	 * overlap.
	 * 
	 * To lean more about merging selects, read the documentation on
	 * {@link M_DbSelect::merge()}
	 */
	const OVERWRITE = 1;
	
	/**
	 * Constant to indicate merging mode
	 * 
	 * This constant is used to indicate the mode in which 2 M_DbSelect
	 * objects should be merged. This constant will cause the merged
	 * select object to add to the original object.
	 * 
	 * To lean more about merging selects, read the documentation on
	 * {@link M_DbSelect::merge()}
	 */
	const COPY = 2;

	/**
	 * Columns
	 *
	 * This property stores the collection of columns that is to be
	 * fetched by the SELECT statement.
	 *
	 * NOTE:
	 * This property defaults to *
	 *
	 * @see M_DbQuery::columns()
	 * @see M_DbQuery::getColumns()
	 * @access private
	 * @var string
	 */
	protected $_columns = '*';
	
	/**
	 * FROM
	 * 
	 * This property stores the FROM expression of the SELECT
	 * statement.
	 * 
	 * @see M_DbSelect::from()
	 * @see M_DbSelect::getFrom()
	 * @access private
	 * @var string
	 */
	protected $_from = '';

	/**
	 * UNION
	 *
	 * This property stores the select statement with which a union should be
	 * created in the SQL
	 *
	 * @see M_DbSelect::union()
	 * @see M_DbSelect::getUnion()
	 * @access private
	 * @var array
	 */
	protected $_union = array();

	/**
	 * JOIN
	 *
	 * This property stores the INNER JOIN, LEFT JOIN, and RIGHT JOIN
	 * expressions of the SELECT statement.
	 *
	 * @see M_DbQuery::joinInner()
	 * @see M_DbQuery::joinLeft()
	 * @see M_DbQuery::joinRight()
	 * @see M_DbQuery::getJoin()
	 * @access private
	 * @var string
	 */
	protected $_join = '';

	/**
	 * This property stores every join of the SELECT statement
	 *
	 * e.g. array(
	 * 			'left' => array('table3','table4','table2'),
	 * 			'right' => array('table2')
	 * 	);
	 *
	 * This makes it possible to check whether a JOIN has been set to a table
	 *
	 * @var array
	 */
	protected $_joins = array(
		'INNER' => array(),
		'LEFT' => array(),
		'RIGHT' => array()
	);

	/**
	 * GROUP BY
	 *
	 * This property stores the GROUP BY expression of the SELECT
	 * statement.
	 *
	 * @see M_DbQuery::group()
	 * @see M_DbQuery::getGroup()
	 * @access private
	 * @var string
	 */
	protected $_group = '';

	/**
	 * HAVING
	 *
	 * This property stores the HAVING expression of the SELECT
	 * statement.
	 *
	 * @see M_DbQuery::having()
	 * @see M_DbQuery::getHaving()
	 * @access private
	 * @var string
	 */
	protected $_having = '';
	
	/* -- SQL syntax -- */

	/**
	 * Set UNION with other SELECT
	 *
	 * NOTE:
	 * This method will set the union, and overwrite any union that may have been
	 * set previously. To add a union to the current selection of unions, please
	 * check {@link M_DbSelect::addUnion()}
	 * 
	 * @access public
	 * @param M_DbSelect $select
	 *		The SELECT statement with which a UNION should be created
	 * @return M_DbSelect $select
	 *		Returns itself, for a fluent programming interface
	 */
	public function union(M_DbSelect $select) {
		$this->_union = array($select);
		return $this;
	}
	
	/**
	 * Add UNION with other SELECT
	 * 
	 * @access public
	 * @param M_DbSelect $select
	 *		The SELECT statement with which a UNION should be created
	 * @return M_DbSelect $select
	 *		Returns itself, for a fluent programming interface
	 */
	public function addUnion(M_DbSelect $select) {
		array_push($this->_union, $select);
		return $this;
	}
	
	/**
	 * Get SQL Syntax: DISTINCT
	 * 
	 * This method is used to compose the DISTINCT expression. This 
	 * method will produce the following syntax:
	 * 
	 * <code>
	 *    DISTINCT table.column
	 * </code>
	 * 
	 * Example 1, get DISTINCT syntax for the column 'headline' of 
	 * the database table 'news'
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    echo $select->distinct('headline', 'news');
	 * </code>
	 * 
	 * Example 1 will produce the following SQL expression:
	 * 
	 * <code>
	 *    DISTINCT `news`.`headline`
	 * </code>
	 * 
	 * @access public
	 * @param string $column
	 * 		The column to be used in the DISTINCT expression
	 * @param string $table
	 * 		The database table in which the column lives (optional)
	 * @return string
	 */
	public function distinct($column, $table = NULL) {
		$output = 'DISTINCT ';
		if($table) {
			$output .= $this->_db->quoteIdentifier($table) . '.';
		}
		return $output . $this->_db->quoteIdentifier($column);
	}
	
	/**
	 * Get SQL Syntax: COUNT
	 * 
	 * This method is used to compose an SQL's COUNT function call. 
	 * The following syntax is produced by this method:
	 * 
	 * <code>
	 *    COUNT( field )
	 * </code>
	 * 
	 * Example 1, get COUNT syntax
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    echo $select->count('*');
	 * </code>
	 * 
	 * Example 1 will produce the following SQL expression:
	 * 
	 * <code>
	 *    COUNT(*)
	 * </code>
	 * 
	 * @access public
	 * @param string $field
	 * 		The selection of column(s) to be counted
	 * @return string
	 */
	public function count($field) {
		return 'COUNT( ' . $field . ' )';
	}
	
	/**
	 * Get SQL Syntax: SUBSTRING
	 * 
	 * This method is used to compose an SQL's SUBSTRING function call. 
	 * The following syntax is produced by this method:
	 * 
	 * <code>
	 *    SUBSTR( field, x, x )
	 * </code>
	 * 
	 * Example 1, get SUBSTRING syntax
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    echo $select->substr('field', 0, 1);
	 * </code>
	 * 
	 * @access public
	 * @param string $field
	 * 		The column from which to extract a substring
	 * @param integer $position
	 * 		The position at which to start the substring
	 * @param integer $length
	 * 		The length of the substring
	 * @param string $table
	 * 		The table in which the column is located
	 * @param bool $literal
	 * 		Set to TRUE if you do not want to quote the identifiers in the SQL Syntax
	 * @return string
	 */
	public function substr($field, $position, $length = NULL, $table = NULL, $literal = FALSE) {
		// Compose the field name
		if(! $literal) {
			$field = $this->_db->quoteIdentifier($field);
		}
		
		if($table) {
			$field = $literal
				? $table . '.' . $field
				: $this->_db->quoteIdentifier($table) . '.' . $field;
		}
		
		// If a length has been provided
		if($length) {
			return 'SUBSTRING( ' . $field . ', '. ($position + 1) .', '. $length .' )';
		}
		// If not provided
		else {
			return 'SUBSTRING( ' . $field . ', '. ($position + 1) .' )';
		}
	}
	
	/**
	 * Get SQL Syntax: SUM
	 * 
	 * This method is used to compose an SQL's SUM() function call. 
	 * The following syntax is produced by this method (example for MySQL):
	 * 
	 * <code>
	 *    SUM( field )
	 * </code>
	 * 
	 * @access public
	 * @param string $field
	 * 		The column/value of which values are to be summed up
	 * @param bool $literal
	 * 		Set to TRUE if you do not want to quote the identifiers in the SQL Syntax
	 * @return string
	 */
	public function sum($field, $literal = TRUE) {
		// Compose the field name
		if(! $literal) {
			// Quote it, if not being used literally:
			$field = $this->_db->quoteIdentifier($field);
		}
		
		// Return the syntax
		return 'SUM( ' . $field . ' )';
	}
	
	/**
	 * Get SQL Syntax: AVERAGE
	 * 
	 * This method is used to compose an SQL's AVG() function call. 
	 * The following syntax is produced by this method (example for MySQL):
	 * 
	 * <code>
	 *    AVG( field )
	 * </code>
	 * 
	 * @access public
	 * @param string $field
	 * 		The column/value we have to fetch the average for
	 * @param bool $literal
	 * 		Set to TRUE if you do not want to quote the identifiers in the SQL Syntax
	 * @return string
	 */
	public function average($field, $literal = TRUE) {
		// Compose the field name
		if(! $literal) {
			// Quote it, if not being used literally:
			$field = $this->_db->quoteIdentifier($field);
		}
		
		// Return the syntax
		return 'AVG( ' . $field . ' )';
	}
	
	/**
	 * Get SQL Syntax: UPPERCASE
	 * 
	 * This method is used to compose an SQL's UPPERCASE function call. 
	 * The following syntax is produced by this method (example for MySQL):
	 * 
	 * <code>
	 *    UPPER( field )
	 * </code>
	 * 
	 * @access public
	 * @param string $field
	 * 		The column/value to be uppercased
	 * @param bool $literal
	 * 		Set to TRUE if you do not want to quote the identifiers in the SQL Syntax
	 * @return string
	 */
	public function uppercase($field, $literal = TRUE) {
		// Compose the field name
		if(! $literal) {
			$field = $this->_db->quoteIdentifier($field);
		}
		
		// Return the syntax
		return 'UPPER( ' . $field . ' )';
	}

	/**
	 * Get SQL Syntax: CONCAT()
	 *
	 * This method is used to compose an SQL's CONCAT function call.
	 * The following syntax is produced by this method (example for MySQL):
	 *
	 * <code>
	 *    CONCAT( 'string', field )
	 * </code>
	 *
	 * This method expects an array of pieces to be provided for concatenation.
	 * Each piece should define a string. This string can either be a simple
	 * string, or the name of a field. A second value is then expected for each
	 * piece, which describes the type of the string: a boolean. This value should
	 * be set to TRUE, if the value is a name of a field, and FALSE if it is
	 * a string.
	 *
	 * Consider this example:
	 *
	 * <code>
	 *    $select = M_Db::getInstance()->select();
	 *    $select->concat(array(
	 *       array('my_field', TRUE),
	 *       array('string',   FALSE)
	 *    ));
	 * </code>
	 *
	 * @access public
	 * @param array $values
	 * @return string
	 */
	public function concat(array $values) {
		// If no values are available:
		if(count($values) == 0) {
			// Throw an exception, to inform about the error
			throw new M_Exception(sprintf(
				'Cannot create SQL CONCAT() syntax. Empty array of values ' .
				'provided'
			));
		}
		
		// Output string
		$out = '';
		$i   = 0;

		// For each argument:
		foreach($values as $i => $value) {
			// The current argument should also be an array, with two elements:
			$isArray = (is_array($value) && count($value) == 2);

			// If that's not the case
			if(! $isArray) {
				// Throw an exception, to inform about the error
				throw new M_Exception(sprintf(
					'Cannot create SQL CONCAT() syntax. Unexpected format for ' .
					'a piece of the concatenation. Expecting an array with two ' .
					'elements: the string at index 0, the type indicator (boolean) ' .
					'at index 1. For example: <pre>%s</pre>',
					var_export(array(array('my_field', TRUE), array('string', FALSE)), TRUE)
				));
			}

			// Get the name and the type indicator:
			$name    = array_shift($value);
			$isField = array_shift($value);

			// Add to the output string:
			$out .= $i ++ == 0 ? ' CONCAT(' : ', ';
			$out .= $isField
				? $this->getDb()->quoteIdentifier($name)
				: '"' . $this->getDb()->quoteString($name) . '"';
		}

		// Finish the function call:
		$out .= ') ';

		// Return the final syntax
		return $out;
	}
	
	/* -- Setters -- */
	
	/**
	 * Set FROM
	 * 
	 * This method is employed to set the FROM expression of the
	 * SELECT statement. By doing so, it sets the database table(s)
	 * where data is being retrieved from.
	 * 
	 * NOTE:
	 * To pass in a selection of tables, you can either pass in a
	 * string (single database table), or an array (collection of
	 * tables).
	 * 
	 * NOTE:
	 * Every call of from() will reset the collection of tables that
	 * may have been set before, in order to set the new selection.
	 * 
	 * @access public
	 * @param string|array $tables
	 * 		The selection of database table(s)
	 * @return M_DbSelect
	 */
	public function from($tables) {
		if(is_array($tables)) {
			$i = 0;
			$this->_from = '';
			foreach($tables as $table) {
				if($i ++ > 0) {
					$this->_from .= ', ';
				}
				$this->_from .= $this->_db->quoteIdentifier($table);
			}
		} else {
			$this->_from = $this->_db->quoteIdentifier($tables);
		}
		
		return $this;
	}
	
	/**
	 * Add random sorting
	 * 
	 * This method will add random sorting to the SELECT statement.
	 * Random sorting can't be combined with the standard sort, it will overwrite
	 * the current sorting-order
	 * 
 	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->orderRand();
	 * </code>
	 * 
	 * @access public
	 * @return M_DbSelect
	 */
	public function orderRandom()
	{
		$this->_order .= empty($this->_order) ? 'ORDER BY ' : ', ';
		$this->_order .= ' RAND()';
		return $this;
	}
	
	/**
	 * Merge select
	 * 
	 * This method will merge 2 {@link MI_DbSelect} objects into one.
	 * Also, you can specify in which mode the objects should be 
	 * merged together:
	 * 
	 * {@link M_DbSelect::OVERWRITE}
	 * {@link M_DbSelect::COPY}
	 * 
	 * The merging mode defaults to the COPY mode.
	 * 
	 * Example 1
	 * <code>
	 *    // Create the first select
	 *    $db = M_Db::getInstance();
	 *    $select1 = $db->select();
	 *    $select1->from('news');
	 *    
	 *    // Create the second one
	 *    $db = M_Db::getInstance();
	 *    $select2 = $db->select();
	 *    $select2->where('date_expired < ?', time());
	 *    
	 *    // Merge the selects
	 *    $select1->merge($select2, M_DbSelect::COPY);
	 *    echo $select1->toString();
	 * </code>
	 * 
	 * Example 1 will output the following:
	 * 
	 * <code>
	 *    SELECT * FROM news WHERE ( date_expired < 1221422321 );
	 * </code>
	 * 
	 * @access public
	 * @param MI_DbSelect $select
	 * 		The select object to be merged into the current select
	 * 		object.
	 * @param string $mode
	 * 		The mode in which the merge of the select objects should 
	 * 		be completed.
	 * @return void
	 */
	public function merge(MI_DbSelect $select, $mode = self::COPY) {
		// Add from:
		switch($mode) {
			// COPY; add to the original object
			case self::COPY:
				// Add from
				$tmp = $select->getFrom();
				if(!empty($tmp) && !empty($this->_from)) {
					$this->_from .= ', ';
				}
				
				$this->_from .= $tmp;
				
				// Add join(s)
				$this->_join .= $select->getJoin();
				
				// Add columns:
				$tmp = $select->getColumns();
				if($tmp != '*') {
					if(!empty($tmp) && !empty($this->_columns)) {
						$this->_columns .= ', ';
					}
					
					$this->_columns .= $tmp;
				}
				
				// Add conditions:
				$tmp = $select->getWhere();
				if(!empty($tmp) && !empty($this->_where)) {
					$this->_where .= ' AND ';
					$this->_where .= !strncmp($tmp, 'WHERE', 5) ? substr($tmp, 5) : $tmp;
				} else {
					$this->_where = $tmp;
				}
				
				// Add order:
				$tmp = $select->getOrder();
				if(!empty($tmp) && !empty($this->_order)) {
					$this->_order .= ', ';
				}
				
				$this->_order .= $tmp;
				
				// Add grouping
				$tmp = $select->getGroup();
				if(!empty($tmp) && !empty($this->_group)) {
					$this->_group .= ', ';
					$this->_group .= !strncmp($tmp, 'GROUP BY', 8) ? substr($tmp, 8) : $tmp;
				} else {
					$this->_group .= $tmp;
				}
				
				// Add having
				$tmp = $select->getHaving();
				if(!empty($tmp) && !empty($this->_having)) {
					$this->_having .= ' AND ';
					$this->_having .= !strncmp($tmp, 'HAVING', 6) ? substr($tmp, 6) : $tmp;
				} else {
					$this->_having .= $tmp;
				}
				break;
			
			// Overwrite "overlapping" properties
			case self::OVERWRITE:
				$tmp = $this->getFrom();
				if(!empty($tmp)) {
					$this->_from = $tmp;
				}
				
				$tmp = $this->getJoin();
				if(!empty($tmp)) {
					$this->_join = $tmp;
				}
				
				$tmp = $this->getColumns();
				if(!empty($tmp)) {
					$this->_columns = $tmp;
				}
				
				$tmp = $this->getWhere();
				if(!empty($tmp)) {
					$this->_where = $tmp;
				}
				
				$tmp = $this->getOrder();
				if(!empty($tmp)) {
					$this->_order = $tmp;
				}
				
				$tmp = $this->getGroup();
				if(!empty($tmp)) {
					$this->_group = $tmp;
				}
				
				$tmp = $this->getHaving();
				if(!empty($tmp)) {
					$this->_having = $tmp;
				}
				break;
		}
	}

	/**
	 * Set columns
	 *
	 * This method will set the collection of column(s) that is to be
	 * fetched by the SELECT statement. The collection that is passed
	 * into this method will affect the elements that are contained
	 * in the final result set.
	 *
	 * Read documentation on {@link M_DbQuery::$_columns} to learn
	 * more about the default selection of columns.
	 *
	 * NOTE:
	 * Every call of columns() will reset the collection of columns
	 * that may have been set before, in order to set the new
	 * selection.
	 *
	 * @access public
	 * @param array $columns
	 * 		The columns that will be selected
	 * @param bool $literal Use quoteIdentifier on every column, this can only
	 * be used when $columns is an array
	 * @return M_DbQuery
	 */
	public function columns( array $columns, $literal = false) {
		$i = 0;
		$this->_columns = '';
		foreach($columns as $column) {
			if($i ++ > 0) {
				$this->_columns .= ', ';
			}

			// if we don't want to add a literal column we use the
			// quoteIdentifier method
			if ($literal == false) $column = $this->_db->quoteIdentifier($column);

			$this->_columns .= $column;
		}

		// Return myself;
		return $this;
	}

	/**
	 * This method will set the collection of columns litteraly to
	 * $columns
	 *
	 * NOTE:
	 * Every call of columns() will reset the collection of columns
	 * that may have been set before, in order to set the new
	 * selection.
	 *
	 * @param str $columns
	 * @return M_DbQuery
	 */
	public function columnsLiteral( $columns) {
		// Set columns
		$this->_columns = (string)$columns;

		// Return myself
		return $this;
	}

	/**
	 *
	 * Add a new column without resetting the existing ones
	 *
	 * @acces public
	 * @param string $column
	 * @param bool $literal
	 * @return M_DbQuery
	 */
	public function addColumn($column, $literal = false) {
		// use quotes when colunm isn't added literally
		if ($literal == false) {
			$column = $this->_db->quoteIdentifier($column);
		}

		// if no column has been selected yet
		if ($this->_columns == '*') {
			// Then we set this column as the column string:
			$this->_columns = $column;
		}
		// otherwise
		else {
			// add column to existing ones
			$this->_columns .= ', '.$column;
		}

		// Return myself
		return $this;
	}

	/**
	 * Add INNER JOIN
	 *
	 * The FROM clause indicates the table or tables from which to
	 * retrieve rows. If you name more than one table, you are
	 * performing a join. This method will add an INNER JOIN to the
	 * SELECT statement.
	 *
	 * @access public
	 * @param string $table
	 * 		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 * 		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $on
	 * 		The column on which to join the table
	 * @param string $condition
	 * 		This condition is applied to the $on parameter, to find
	 * 		matches where the table can be joined on. Typically, this
	 * 		condition is the name of a column in the joined table.
	 * @return M_DbQuery
	 */
	public function joinInner($table, $tableAlias = null, $on, $condition) {
		return $this->_join('INNER',$table,$tableAlias,$on,$condition);
	}
	
	/**
	 * Add INNER JOIN Literal
	 * 
	 * Allows for adding a literal inner join. This allows for adding joins
	 * with a complicated condition, rather than the standard "ON a = b"
	 * condition. For example:
	 * 
	 * <code>
	 *	SELECT product.* FROM product
	 *	INNER JOIN category ON 
	 *		(product.product_category_id = category.category_id) AND
	 *		(category.category_active = 1)
	 *	;
	 * </code>
	 * 
	 * Note that the condition you are providing is appended literally to the
	 * query, so make sure the query is properly quoted where necessary!
	 * 
	 * @access public
	 * @param string $table
	 *		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 *		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $condition
	 *		The full condition, as it is to be applied after the ON statement
	 * @return M_DbQuery
	 */
	public function joinInnerLiteral($table, $tableAlias = null, $condition) {
		return $this->_join('INNER', $table, $tableAlias, $condition);
	}

	/**
	 * Add LEFT JOIN
	 *
	 * The FROM clause indicates the table or tables from which to
	 * retrieve rows. If you name more than one table, you are
	 * performing a join. This method will add an LEFT JOIN to the
	 * SELECT statement.
	 *
	 * @access public
	 * @param string $table
	 * 		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 * 		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $on
	 * 		The column on which to join the table
	 * @param string $condition
	 * 		This condition is applied to the $on parameter, to find
	 * 		matches where the table can be joined on. Typically, this
	 * 		condition is the name of a column in the joined table.
	 * @return M_DbQuery
	 */
	public function joinLeft($table, $tableAlias = null, $on, $condition) {
		return $this->_join('LEFT',$table,$tableAlias,$on,$condition);
	}
	
	/**
	 * Add LEFT JOIN Literal
	 * 
	 * Allows for adding a literal left join. This allows for adding joins
	 * with a complicated condition, rather than the standard "ON a = b"
	 * condition. For example:
	 * 
	 * <code>
	 *	SELECT product.* FROM product
	 *	LEFT JOIN category ON 
	 *		(product.product_category_id = category.category_id) AND
	 *		(category.category_active = 1)
	 *	;
	 * </code>
	 * 
	 * Note that the condition you are providing is appended literally to the
	 * query, so make sure the query is properly quoted where necessary!
	 * 
	 * @access public
	 * @param string $table
	 *		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 *		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $condition
	 *		The full condition, as it is to be applied after the ON statement
	 * @return M_DbQuery
	 */
	public function joinLeftLiteral($table, $tableAlias = null, $condition) {
		return $this->_join('LEFT', $table, $tableAlias, $condition);
	}

	/**
	 * Add RIGHT JOIN
	 *
	 * The FROM clause indicates the table or tables from which to
	 * retrieve rows. If you name more than one table, you are
	 * performing a join. This method will add an RIGHT JOIN to the
	 * SELECT statement.
	 *
	 * @access public
	 * @param string $table
	 * 		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 * 		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $on
	 * 		The column on which to join the table
	 * @param string $condition
	 * 		This condition is applied to the $on parameter, to find
	 * 		matches where the table can be joined on. Typically, this
	 * 		condition is the name of a column in the joined table.
	 * @return M_DbQuery
	 */
	public function joinRight($table, $tableAlias = null, $on, $condition) {
		return $this->_join('RIGHT',$table,$tableAlias,$on,$condition);
	}
	
	/**
	 * Add RIGHT JOIN Literal
	 * 
	 * Allows for adding a literal right join. This allows for adding joins
	 * with a complicated condition, rather than the standard "ON a = b"
	 * condition. For example:
	 * 
	 * <code>
	 *	SELECT product.* FROM product
	 *	RIGHT JOIN category ON 
	 *		(product.product_category_id = category.category_id) AND
	 *		(category.category_active = 1)
	 *	;
	 * </code>
	 * 
	 * Note that the condition you are providing is appended literally to the
	 * query, so make sure the query is properly quoted where necessary!
	 * 
	 * @access public
	 * @param string $table
	 *		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 *		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $condition
	 *		The full condition, as it is to be applied after the ON statement
	 * @return M_DbQuery
	 */
	public function joinRightLiteral($table, $tableAlias = null, $condition) {
		return $this->_join('RIGHT', $table, $tableAlias, $condition);
	}
	
	/**
	 * Add JOIN
	 *
	 * The FROM clause indicates the table or tables from which to
	 * retrieve rows. If you name more than one table, you are
	 * performing a join. This method will add an JOIN to the
	 * SELECT statement.
	 *
	 * IMPORTANT NOTE: the condition is optional. If left empty, we assume
	 * the $on parameter contains the full condition, which is typically
	 * provided by either of the following functions:
	 * - {@link DbSelect::joinInnerLiteral()}
	 * - {@link DbSelect::joinLeftLiteral()}
	 * - {@link DbSelect::joinRightLiteral()}
	 * 
	 * This rather obscure methodology is implemented to support backwards
	 * compatibility with older projects.
	 * 
	 * @access public
	 * @param string $table
	 * 		The table to be joined in the SELECT statement
	 * @param string $tableAlias
	 * 		The alias for the tablename, if left null this is automatically set
	 * 		to $table
	 * @param string $on
	 * 		The column on which to join the table
	 * @param string $condition
	 * 		This condition is applied to the $on parameter, to find
	 * 		matches where the table can be joined on. Typically, this
	 * 		condition is the name of a column in the joined table.
	 * @return M_DbQuery
	 */
	private function _join($type, $table, $tableAlias, $on, $condition = null) {
		$this->_join .= ' ' . $type . ' JOIN ';
		$this->_join .= $this->_db->quoteIdentifier($table);

		if (!is_null($tableAlias)) {
			$this->_join.= ' ' . $this->_db->quoteIdentifier($tableAlias);
		}

		// If the condition is given:
		if($condition) {
			// Then apply the condition as usual
			$this->_join .= ' ON ' . $this->_db->quoteIdentifier($on);
			$this->_join .= ' = ' . $condition;
		}
		// Otherwise, if the condition is not given, it means the $on parameter
		// contains a literal condition
		else {
			$this->_join .= ' ON ' . $on;
		}

		//store for later use
		$this->_joins[$type][$tableAlias] = $table;

		return $this;
	}

	/**
	 * Check if a join has already been set
	 *
	 * @param str $table
	 * 		The table to which has been joined, if left empty the total amount
	 * 		of joins is returned
	 * @return int
	 * 		Number of joins to this table
	 */
	public function hasJoin($table = null) {
		$cnt = $this->hasJoinLeft($table);
		$cnt += $this->hasJoinRight($table);
		$cnt += $this->hasJoinInner($table);

		return $cnt;
	}

	/**
	 * Count the number of left joins to a table
	 *
	 * @param str $table
	 * 		The table name
	 * @return int
	 */
	public function hasJoinLeft($table = null) {
		return $this->_hasJoin($table,'LEFT');
	}

	/**
	 * Count the number of right joins to a table
	 *
	 * @param str $table
	 * 		The table name
	 * @return int
	 */
	public function hasJoinRight($table = null) {
		return $this->_hasJoin($table,'RIGHT');
	}

	/**
	 * Count the number of inner joins to a table
	 *
	 * @param str $table
	 * 		The table name
	 * @return int
	 */
	public function hasJoinInner($table = null) {
		return $this->_hasJoin($table,'INNER');
	}

	/**
	 * Count the occurrences of $table in $this->_joins
	 * Private implementation so we don't need to copy paste this code
	 * for every type of join
	 *
	 * @param str $table
	 * 		If left empty the total amount of joins will be calculated
	 * @param str $type
	 * 		The join-type inner,left,right
	 * @return int
	 * 		The number of occurences
	 *
	 */
	private function _hasJoin($table = null,$type) {
		if (is_null($table)) $cnt = count($this->_joins[$type]);
		else {
			//we check if the table is found in the joins array, if
			//yes we can return 1, if not we return 0
			$cnt = (int)in_array($table, $this->_joins[$type]);
		}

		return $cnt;
	}

	/**
	 * Add GROUP BY
	 *
	 * This method will add a GROUP BY expression to the SELECT
	 * statement. By doing so, the results of the select query will
	 * be grouped by the provided condition.
	 *
	 * NOTE:
	 * To pass in a grouping condition, you can either pass in a
	 * string (single database column), or an array (collection of
	 * columns).
	 *
	 * Example 1, group news items by 'id'
	 *
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->group('id');
	 * </code>
	 *
	 * Example 2, group news items by 'id', and 'category'
	 *
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    $select->group(array('id', 'category'));
	 * </code>
	 *
	 * @access public
	 * @param string|array $columns
	 * 		The columns by which to group results
	 * @return M_DbQuery
	 */
	public function group($columns) {
		if(is_array($columns)) {
			$this->_group = 'GROUP BY ';
			for($i = 0, $n = count($columns); $i < $n; $i ++) {
				if($i > 0) {
					$this->_group .= ', ';
				}
				$this->_group .= $this->_db->quoteIdentifier($columns[$i]);
			}
		} else {
			$this->_group .= empty($this->_group) ? 'GROUP BY ' : ', ';
			$this->_group .= $this->_db->quoteIdentifier($columns);
		}
		return $this;
	}

	/**
	 * Check if grouping
	 *
	 * Will check if grouping has been defined in the query, with the method
	 * {@link M_DbQuery::group()}
	 *
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if grouping exists in the query, FALSE if not
	 */
	public function hasGroup() {
		return (! empty($this->_group));
	}

	/**
	 * Add HAVING clause
	 *
	 * This method is used in exactly the same way(s) as the methods
	 *
	 * - {@link M_DbQuery::where()}
	 * - {@link M_DbQuery::orWhere()}
	 *
	 * @access public
	 * @param string $conditions
	 * 		The HAVING expression/clause
	 * @param ...
	 * 		A variable number of arguments, which is used to do
	 * 		replacements of placeholders in the HAVING clause.
	 * @return M_DbQuery
	 */
	public function having($conditions) {
		$this->_having .= empty($this->_having) ? 'HAVING ( ' : ' '.self::OPERATOR_AND.' ( ';
		if(is_array($conditions)) {
			// if the conditions argument is an array, we add the conditions
			// in bulk, using the AND operator.
			$i = 0;
			foreach($conditions as $condition) {
				// if the current condition is also an array, we use the
				// elements of the array to set placeholder values:
				$this->_having .= ($i ++ > 0) ? ' '.self::OPERATOR_AND.' ' : ' ';
				$this->_having .= $this->_db->quoteIn(array_shift($condition), $condition);
			}
		} else {
			// if the conditions argument is not an array, we add the string
			// to the collection of where clauses. Note that we also do
			// placeholder values, before adding the clause
			$conditions = func_get_args();
			$this->_having .= $this->_db->quoteIn(array_shift($conditions), $conditions);
		}

		// close the WHERE clause
		$this->_having .= ' )';
		return $this;
	}
	
	/* -- Getters -- */
	
	/**
	 * Get the SELECT statement
	 * 
	 * This method will export the SELECT statement to the correct SQL
	 * expression. The result of this method will be used by 
	 * 
	 * - {@link MI_Db::query()}
	 * - {@link MI_Db::queryRange()}
	 * 
	 * to obtain the result set.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// Compose the SELECT statement:
		$sql = 
			'SELECT ' . $this->_columns . ' FROM ' . $this->_from . ' ' . 
			$this->_join . ' ' . $this->_where . ' ' . $this->_group . ' ' . 
			$this->_having;
		
		// Get the count of unions:
		$unionCount = count($this->_union);
		
		// If at least 1 union has been set, then add an opening bracket:
		if($unionCount > 0) {
			// Copy the ORDER BY clause of subselects to the main query?
			$doOrder = TRUE;
			
			// Initiate a temporary array of queries:
			$tmp = array($sql);
			
			// For each of the SELECT statements in the UNION:
			for($i = 0, $j = 1; $i < $unionCount; $i ++, $j ++) {
				// We render the SELECT statement, and add it to the temporary
				// array of queries:
				$tmp[$j] = $this->_union[$i];
				
				// Re-check if we should copy the ORDER BY clause:
				$doOrder = ($doOrder && $this->_union[$i]->getOrder() == $this->_order);
			}
			
			// If we should copy the ORDER BY clause to the main query:
			if($doOrder) {
				// We'll reset the order in all SUBSELECT statements:
				for($i = 1; $i <= $unionCount; $i ++) {
					$tmp[$i]->_order = '';
				}
			}
			// If we should not copy:
			else {
				// We add the original ORDER BY statement to this query. In this case,
				// each subselect orders its own results:
				$tmp[0] .= ' ' . $this->_order;
			}
			
			// Join the SELECT statements in a UNION syntax:
			$out = '('. implode(') UNION (', $tmp) .')';
			
			// If the ORDER BY clause should be maintained, we add the ORDER BY clause
			// that will order the UNIFIED collection. 
			if($doOrder) {
				$out .= ' ' . $this->_order;
			}
			
			// Return the resulting query
			return $out;
		}
		// If no unions have been added:
		else {
			// Then, we simply return the query:
			return $sql . ' ' . $this->_order;
		}
	}

	/**
	 * Get FROM
	 *
	 * This method will return the FROM expression that has been set
	 * previously by {@link M_DbSelect::from()}.
	 *
	 * @access public
	 * @return string
	 */
	public function getFrom() {
		return $this->_from;
	}

	/**
	 * Get UNIONS
	 *
	 * This method will return the SELECT Statements that are being used for a UNION
	 *
	 * @author Tom Bauwens
	 * @see M_DbSelect::union()
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getUnions() {
		return new M_ArrayIterator($this->_union);
	}

	/**
	 * Get GROUP
	 *
	 * This method will return the gouping conditions that has been
	 * set previously with {@link M_DbQuery::group()}
	 *
	 * @access public
	 * @return string
	 */
	public function getGroup() {
		return $this->_group;
	}

	/**
	 * Get HAVING
	 *
	 * This method will return the HAVING expression that has been
	 * set previously with {@link M_DbQuery::group()}
	 *
	 * @access public
	 * @return string
	 */
	public function getHaving() {
		return $this->_having;
	}

	/**
	 * Get columns
	 *
	 * This method will return the columns that has been set previously
	 * with {@link M_DbQuery::columns()}.
	 *
	 * @access public
	 * @return string
	 */
	public function getColumns() {
		return $this->_columns;
	}

	/**
	 * Get JOINS
	 *
	 * This method will return the joins that has been set previously
	 * with:
	 *
	 * - {@link M_DbQuery::joinInner()}
	 * - {@link M_DbQuery::joinLeft()}
	 * - {@link M_DbQuery::joinRight()}
	 *
	 * @access public
	 * @return string
	 */
	public function getJoin() {
		return $this->_join;
	}
	
	/* -- Resetters -- */
	
	/**
	 * Reset from
	 * 
	 * Will reset back to empty value. For more info, read the 
	 * following documentation:
	 * 
	 * - {@link M_DbSelect::from()}
	 * - {@link M_DbSelect::getFrom()}
	 * 
	 * @access public
	 * @return void
	 */
	public function resetFrom() {
		$this->_from = '';
	}
	
	/**
	 * Reset join
	 *
	 * Will reset back to empty value. For more info, read the
	 * following documentation:
	 *
	 * - {@link M_DbQuery::joinInner()}
	 * - {@link M_DbQuery::joinLeft()}
	 * - {@link M_DbQuery::joinRight()}
	 *
	 * @access public
	 * @return void
	 */
	public function resetJoin() {
		$this->_join = '';
		$this->_joins = array(
			'left' => array(),
			'right' => array(),
			'inner' => array()
		);
	}

	/**
	 * Reset columns
	 *
	 * Will reset back to empty value. For more info, read the
	 * following documentation:
	 *
	 * - {@link M_DbQuery::columns()}
	 * - {@link M_DbQuery::getColumns()}
	 *
	 * @access public
	 * @return void
	 */
	public function resetColumns() {
		$this->_columns = '*';
	}

	/**
	 * Reset GROUP BY
	 *
	 * Will reset back to empty value. For more info, read the
	 * following documentation:
	 *
	 * - {@link M_DbQuery::group()}
	 * - {@link M_DbQuery::getGroup()}
	 *
	 * @access public
	 * @return void
	 */
	public function resetGroup() {
		$this->_group = '';
	}

	/**
	 * Reset HAVING
	 *
	 * Will reset back to empty value. For more info, read the
	 * following documentation:
	 *
	 * - {@link M_DbQuery::having()}
	 * - {@link M_DbQuery::getHaving()}
	 *
	 * @access public
	 * @return void
	 */
	public function resetHaving() {
		$this->_having = '';
	}

	/* -- Fetchers -- */

	/**
	 * Get first value of first result
	 *
	 * Shortcut to {@link MI_DbResult::getOne()}.
	 *
	 * This method will execute the SELECT statement, and return the
	 * value of the first column of the first record in the result set.
	 * This method is particularly handy if you're looking for just
	 * one value
	 *
	 * Example 1
	 *
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $select = $db->select();
	 *    $select->from('news');
	 *    echo $select;
	 * </code>
	 *
	 * Note that, if no results have been fetched by the select, this
	 * method will return FALSE!
	 *
	 * @uses MI_DbResult::getOne()
	 * @access public
	 * @return string|integer
	 */
	public function getOne() {
		$rs = $this->execute();
		if($rs !== FALSE && count($rs) > 0) {
			return $rs->getOne();
		} else {
			return FALSE;
		}
	}

	/**
	 * Get All results
	 *
	 * Shortcut to {@link MI_DbResult::getAll()}.
	 *
	 * This method will execute the SELECT statement, and return all
	 * results in an array.
	 *
	 * @uses MI_DbResult::getAll()
	 * @access public
	 * @return array
	 */
	public function getAll() {
		$rs = $this->execute();
		if($rs !== FALSE && count($rs) > 0) {
			return $rs->getAll();
		} else {
			return FALSE;
		}
	}

	/**
	 * Execute the SELECT statement
	 *
	 * This method will execute the SELECT statement. This method will
	 * return the same database query result set, as returned by
	 *
	 * - {@link M_DbQuery::query()}, or
	 * - {@link M_DbQuery::queryRange()}
	 *
	 * @access public
	 * @return M_DbResultAdo
	 */
	public function execute() {

		//check if every where block has been closed
		if ($this->_whereBlockCount !== 0) {
			throw new M_DbException('Not every WHERE block has been closed,
				still ' . $this->_whereBlockCount . ' is open');
		}

		//generate sql string
		$sql = $this->toString();

		//execute with limit
		if($this->_limit) {
			return $this->_db->queryRange($sql, $this->_limit[0], $this->_limit[1]);
		}
		//execute query
		else {
			return $this->_db->query($sql);
		}
	}
}