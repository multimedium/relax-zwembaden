<?php
/**
 * DbQueryFilter Interface
 * 
 * Filters can be used in combination with DBSelect, DBUpdate, DBInsert or
 * DBDelete objects. It simplifies programming data-models because only a base
 * query needs to be written. Subsequently filters can modify this query and
 * providing different results for each situation
 *
 */
interface MI_DbQueryFilter {
	
	/**
	 * Filter the table
	 * 
	 * This method adds conditions to the provided instance of {@link M_DbQuery}.
	 *
	 * @access public
	 * @param M_DbQuery $query
	 * 		The query on which to apply the filter
	 * @return M_DbQuery $query
	 * 		The query with the filters applied to it
	 */
	public function filter(M_DbQuery $query);
	
	/**
	 * Returns all columns on which is filtered on
	 * 
	 * @return ArrayIterator
	 */
	public function getFields();
	
	/**
	 * Change the field name of a certain field in a condition
	 * 
	 * Can be used when filtering on e.g. joined tables
	 * The column name of the joined table is unknown for this filter
	 * That's why we need to change the name of the column that is used in the 
	 * filters, the old name will be replaced in all conditions by the new column
	 * name
	 * 
	 * @param string $old
	 * @param string $new
	 * 
	 * @return M_DbQueryFilterWhere $this
	 */
	public function changeFieldName($old, $new);
}