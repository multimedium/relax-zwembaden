<?php
// Used to represent primary keys, indexes, etc...
interface MI_DbIndex {
	public function __construct($name = NULL);
	
	// Get name of the index
	public function getName();
	
	// Get type of index
	public function getType();
	
	// Get columns in index
	public function getColumns();
	
	// Set name of the index:
	public function setName($name);
	
	// Set type of index
	public function setType($type);
	
	// Add database table column to the index:
	public function addColumn(MI_DbColumn $column);
	
	// Check if column is in the index:
	public function containsColumn(MI_DbColumn $column);
}
?>