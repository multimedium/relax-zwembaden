<?php
/**
 * M_DbQueryFilterOrder class
 * 
 * adjust the query-order
 *
 * @package Core
 * @access public
 */
class M_DbQueryFilterOrder extends M_DbQueryFilter 
{
	/**
	 * @var array
	 */
	protected $_order = array();

	/**
	 * Construct
	 *
	 * @param str $column 
	 * @param str $order (ASC/DESC)
	 * @param bool $literal
	 * @return void
	 */
	public function __construct( $column, $order = 'ASC', $literal = false) {
		$this->_order[] = array($column, strtoupper($order), $literal);
	}
	
	/**
	 * Order
	 *
	 * @param str $column
	 * @param str $order (ASC/DESC)
	 * @param bool $literal
	 * @return M_DbQueryFilterOrder
	 */
	public function addOrder( $column, $order, $literal = false) {
		$this->_order[] = array($column, strtoupper($order), $literal);
		
		return $this;
	}
	
	/**
	 * Reset the order
	 *
	 * @return M_DbQueryFilterOrder
	 */
	public function resetOrder() {
		$this->_order = array();
		
		return $this;
	}
	
	/**
	 * Apply the filter
	 * 
	 * apply the added column-order.
	 * 
	 * @example 
	 * 
	 * $filter = new M_DbQueryFilterOrder('column1', 'ASC');
	 * $filter->filter($select);
	 * 
	 * Final result: 
	 * ORDER BY `column1` ASC
	 *
	 * @param M_DbSelect $query
	 * @param M_DataObjectMapper $mapper
	 * @return M_DbSelect $query
	 */
	public function filter( M_DbQuery $query, M_DataObjectMapper $mapper = null) {
		$orderIterator = new ArrayIterator($this->_order);
		while( $orderIterator->valid())
		{
			$order = $orderIterator->current();
			$column = $order[0];
			$literal = $order[2];

			//use default order
			if (!$literal) {
				//if a mapper is provided: ask him for the localized field name
				if (!is_null($mapper)) {
					$column = $mapper->getFieldDbNameLocalized($column);
				}
				$query->order($column,$order[1]);
			}

			//apply literal order
			else {
				$query->orderLiteral($column, $order[1]);
			}
			
			$orderIterator->next();
		}
		return $query;
	}
	
	/**
	 * Get fields for this filter, based on every field on which we want
	 * to order
	 * 
	 * @return ArrayIterator
	 */
	public function getFields() {
		$fields = array();
		foreach($this->_order AS $order) {
			$fields[] = $order[0];
		}
		
		return new ArrayIterator($fields);
	}
}