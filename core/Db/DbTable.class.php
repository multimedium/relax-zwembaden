<?php
// Abstract class to support tables in a driver. This class provides with
// the basic implementation of MI_DbTable, shared by all driver 
// implementations
abstract class M_DbTable implements MI_DbTable {
	
    /**
     * @const str
     */
    const ENGINE_MYSQL_MYISAM = 'MYISAM';
    /**
     * @const str
	 * 
	 * don't use!! no longer supported by Tigron
     */
    const ENGINE_MYSQL_MEMORY = 'MEMORY';
    /**
     * @const str
     */
    const ENGINE_MYSQL_INNODB = 'INNODB';

    /**
     * @const str
     */
    const CHARSET_UTF8 = 'UTF8';
    /**
     * @const str
     */
    const COLLATE_UTF8_GENERAL_CI = 'utf8_general_ci';
    
	/**
	 * The database driver
	 *
	 * @access protected
	 * @var MI_Db
	 */
	protected $_db;
	
	/**
	 * Table name
	 * 
	 * This property stores the name of the database table.
	 *
	 * @access protected
	 * @var string
	 */
	protected $_name;
	
	/**
	 * Table comments
	 * 
	 * This property stores additional commens for this database table
	 *
	 * @var str
	 */
	protected $_comments;
	
	/**
	 * Table engine
	 *
	 * @var str
	 */
	protected $_engine = self::ENGINE_MYSQL_INNODB;
	
	/**
	 * Table charset
	 *
	 * @var str
	 */
	protected $_charset = self::CHARSET_UTF8;
	
	/**
	 * Table collate
	 */
	protected $_collate = self::COLLATE_UTF8_GENERAL_CI;
	
	
	public function __construct(MI_Db $db, $name) {
		$this->_db = $db;
		$this->_name = $name;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function setName($name) {
		$this->_name = (string) $name;
	}
	
	/**
	 * Get the table comments
	 * 
	 * @return str
	 */
    public function getComments() {
        if ($this->getName() && $this->_db->hasTable($this->getName())) {
            $status = $this->getStatus();
            $this->_comments =  $status->getColumnValue('Comments');
        }
        return $this->_comments;
	}
	
	/**
	 * Set table comments
	 *
	 * @param str $comments
	 * @return void
	 */
	public function setComments($comments) {
	    $this->_comments = (string) $comments;
	}

    /**
     * @return str
     */
    public function getCharset ()
    {
        return $this->_charset;
    }

    /**
     * @param str $_charset
     */
    public function setCharset ($_charset)
    {
        $this->_charset = $_charset;
    }

    /**
     * @return str
     * @todo if this is an existing table we could retrieve the collation by
     * executing SHOW TABLE status WHERE name = $this->getName()
     */
    public function getCollate ()
    {
        return $this->_collate;
    }

    /**
     * @param str $_collate
     */
    public function setCollate ($_collate)
    {
        if ($this->getName() && $this->_db->hasTable($this->getName())) {
            $status = $this->getStatus();
            $this->_engine =  $status->getColumnValue('Collate');
        }
        $this->_collate = $_collate;
    }

    /**
     * @return str
     * @todo if this is an existing table we could retrieve the engine by
     * executing SHOW TABLE status WHERE name = $this->getName()
     */
    public function getEngine ()
    {
        if ($this->getName() && $this->_db->hasTable($this->getName())) {
            $status = $this->getStatus();
            $this->_engine =  $status->getColumnValue('Engine');
        }
        return $this->_engine;
    }

    /**
     * @param str $_engine
     */
    public function setEngine ($_engine)
    {
        $this->_engine = $_engine;
    }
    
    /**
     * Get status for this table
     *
     * @return M_DbResultAdo
     */
    public function getStatus() {
        if (is_null($this->getName()) || $this->_db->hasTable($this->getName()) == false) {
            throw new M_DbException('Cannot retrieve status for inexistant table');
        }
        return $this->_db->query('SHOW TABLE STATUS WHERE name = '.$this->_db->quote($this->_name));
    }
	
	// Empty the table. Returns success/failure
	public function truncate() {
		return $this->_db->queryOp('TRUNCATE TABLE ' . $this->_db->quoteIdentifier($this->_name));
	}
	
	// We provide a default implementation of insert(). The insert SQL
	// should be in standard SQL anyway
	public function insert(array $data) {
		if(count($data) == 0) {
			throw new M_DbException('Cannot insert an empty array');
		}
		
		$sql  = 'INSERT INTO ';
		$sql .=    $this->_db->quoteIdentifier($this->_name);
		$val  = ' VALUES';
		
		$i = 0;
		foreach($data as $column => $value) {
			$sql .= ($i == 0) ? ' ( ' : ', ';
			$sql .= $this->_db->quoteIdentifier($column);
			$val .= ($i ++ == 0) ? ' ( ' : ', ';
			$val .= $this->_db->quote($value);
		}
		
		$sql .= ' )' . $val . ' )';
		
		return $this->_db->queryOp($sql);
	}
	
	// Insert multiple records
	public function insertMultiple(array $data) {
		if(count($data) == 0) {
			throw new M_DbException('Cannot insert an empty array');
		}
		
		$sql  = 'INSERT INTO ';
		$sql .=    $this->_db->quoteIdentifier($this->_name);
		
		$i = 0;
		$values = array();
		$columns = array();
		foreach($data as $record) {
			$val = '';
			$j = 0;
			foreach($record as $column => $value) {
				//only add columns once
				if ($i == 0) {
					$columns[] = $this->_db->quoteIdentifier($column);;
				}

				$val .= ($j ++ == 0) ? ' ( ' : ', ';
				$val .= $this->_db->quote($value);
			}
			$val .= (')');
			$i++;
			
			$values[] = $val;
			
		}
		//add columns
		$sql .= '('.implode(',',$columns ) .') ';
		
		//add values
		$sql.= 'VALUES '.implode(',',$values);

		return $this->_db->queryOp($sql);
	}
	
	/**
	 * Insert multiple records, and UPDATE column(s) if necessary
	 * 
	 * Note: Assumption support is provided by DB!
	 * 
	 * @example 
	 * 
	 * $date = array(
	 * 		array(
	 * 			'id' => 1,
	 * 			'productName' => 'test',
	 * 			'productOrder' => 2
	 * 			'productDate' => '2009-11-23'
	 * 		),
	 * 		array(
	 * 			'id' => 2,
	 * 			'productName' => 'another test',
	 * 			'productOrder' => 1
	 * 			'productDate' => '2009-11-21'
	 * 		),
	 * );
	 * 
	 * $updateColumns = array(
	 * 		'productOrder',
	 * 		'productDate'
	 * );
	 * 
	 * $table->insertOrUpdateMultiple($data, $updateColumns);
	 * 
	 * This will INSERT both product, and update the productOrder and/or 
	 * productDate if the product allready exists in the database
	 * 
	 * @param array $data 
	 * 			Insert-data
	 * @param array $updateColumns 
	 * 			Columns which will be update ON DUPLICATE KEY
	 * @return bool 
	 * 			Success/failure
	 */
	public function insertOrUpdateMultiple(array $data, array $updateColumns = array()) {
		if(count($data) == 0) {
			throw new M_DbException('Cannot insert an empty array');
		}
		
		//if no updatecolumns are provided, we assume all columns need to be
		//updated: get the columns from the first record
		if(count($updateColumns) == 0) {
			$updateData = $data;
			$updateData = array_shift($updateData);
			$updateData = array_keys((array)$updateData);
		}
		
		$sql  = 'INSERT INTO ';
		$sql .=    $this->_db->quoteIdentifier($this->_name);
		$val  = ' VALUES';
		
		$i = 0;
		$values = array();
		$columns = array();
		foreach($data as $record) {
			$val = '';
			$j = 0;
			foreach($record as $column => $value) {
				//only add columns once
				if ($i == 0) {
					$columns[] = $this->_db->quoteIdentifier($column);;
				}

				$val .= ($j ++ == 0) ? ' ( ' : ', ';
				$val .= $this->_db->quote($value);
			}
			$val .= (')');
			$i++;
			
			$values[] = $val;
			
		}
		//add columns
		$sql .= '('.implode(',',$columns ) .') ';
		
		//add values
		$sql.= 'VALUES '.implode(',',$values).' ';
		
		//on duplicate key		
		$sql .= 'ON DUPLICATE KEY ';
		$sql .= 'UPDATE ';
		
		$i = 0;
		foreach($updateColumns as $column) {
			$sql .= ($i ++ == 0) ? ' ' : ', ';
			$sql .= $this->_db->quoteIdentifier($column);
			$sql .= ' = ';
			$sql .= 'VALUES('.$this->_db->quoteIdentifier($column).')';
		}
		
		return $this->_db->queryOp($sql);
	}
	
	// $conditions should be string-only? Also, additional arguments are
	// fetched to populate placeholders in the conditions string
	public function update(array $data, $conditions) {
		if(count($data) == 0) {
			throw new M_DbException('Cannot update with an empty array');
		}
		
		$sql  = 'UPDATE ';
		$sql .=    $this->_db->quoteIdentifier($this->_name);
		
		$i = 0;
		foreach($data as $column => $value) {
			$sql .= ($i ++ == 0) ? ' SET ' : ', ';
			$sql .= $this->_db->quoteIdentifier($column);
			$sql .= ' = ';
			$sql .= $this->_db->quote($value);
		}
		
		if(is_array($conditions)) {
			$i = 0;
			foreach($conditions as $condition) {
				// if the current condition is also an array, we use the
				// elements of the array to set placeholder values:
				$sql .= ($i ++ > 0) ? ' AND ' : ' WHERE ';
				$sql .= $this->_db->quoteIn(array_shift($condition), $condition);
			}
		} else {
			$output = array();
			for($i = 1, $n = func_num_args(); $i < $n; $i ++) {
				$output[] = func_get_arg($i);
			}
			
			$sql .= ' WHERE ' . $this->_db->quoteIn(array_shift($output), $output);
		}
		
		return $this->_db->queryOp($sql);
	}
	
	// @TODO
	// Assumption support is provided by DB!!!
	public function insertOrUpdate(array $data, array $updateData = array()) {
		if(count($data) == 0) {
			throw new M_DbException('insertOnDuplicateUpdate: Cannot insert an empty array');
		}
		
		if(count($updateData) == 0) {
			$updateData = $data;
		}
		
		$sql  = 'INSERT INTO ';
		$sql .=    $this->_db->quoteIdentifier($this->_name);
		$val  = ' VALUES';
		
		$i = 0;
		foreach($data as $column => $value) {
			$sql .= ($i == 0) ? ' ( ' : ', ';
			$sql .= $this->_db->quoteIdentifier($column);
			$val .= ($i ++ == 0) ? ' ( ' : ', ';
			$val .= $this->_db->quote($value);
		}
		
		$sql .= ' )' . $val . ' ) ON DUPLICATE KEY ';
		$sql .= 'UPDATE';
		
		$i = 0;
		foreach($updateData as $column => $value) {
			$sql .= ($i ++ == 0) ? ' ' : ', ';
			$sql .= $this->_db->quoteIdentifier($column);
			$sql .= ' = ';
			$sql .= $this->_db->quote($value);
		}
		
		return $this->_db->queryOp($sql);
	}
	
	public function delete($conditions) {
		$sql  = 'DELETE FROM ';
		$sql .=    $this->_db->quoteIdentifier($this->_name);

		if(is_array($conditions)) {
			for($i = 0, $n = count($conditions); $i < $n; $i ++) {
				$sql .= ($i == 0) ? ' WHERE ' : ' AND ';
				$sql .= $conditions[$i];
			}
		} else {
			$output = array();
			for($i = 0, $n = func_num_args(); $i < $n; $i ++) {
				$output[] = func_get_arg($i);
			}
			$sql .= ' WHERE ' . $this->_db->quoteIn(array_shift($output), $output);
		}
		
		return $this->_db->queryOp($sql);
	}
	
	/**
	 * Create a new Select query for this table
	 *
	 * @return M_DbSelect
	 */
	public function select() {
		$select = new M_DbSelect($this->_db);
		//by default select all columns of this table
		//@internal: this was added to prevent selecting every column of every
		//join. If we should select all, it's possible to override colunm-values
		//since not all colunmnames are unique, and thus losing row-data
		$select->columnsLiteral($this->_name.'.*');
		$select->from($this->_name);
		return $select;
	}
	
	public function hasColumn($name) {
		foreach($this->getColumns() as $column) {
			if($column->getName() == $name) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function create() {
		$this->_db->createTable($this);
	}
	
	public function alter() {
		$this->_db->alterTable($this);
	}
	
	public function drop() {
		$this->_db->dropTable($this);
	}
}
?>