<?php
/**
 * M_DbQueryFilterLimit class
 *
 * @package Core
 * @access public
 */
class M_DbQueryFilterLimit extends M_DbQueryFilter 
{
	/**
	 * @var int
	 */
	protected $_offset;

	/**
	 * @var int
	 */
	protected $_numberofRows;
	
	/**
	 * Construct
	 *
	 * @param int offset 
	 * @param int numberofrows 
	 * @return void
	 */
	public function __construct( $offset, $numberofRows ) {
		$this->_offset = (int)$offset;
		$this->_numberofRows = (int)$numberofRows;
	}
	
	/**
	 * Apply the filter
	 *
	 * @param M_DbSelect $query
	 * @return M_DbSelect $query
	 */
	public function filter( M_DbQuery $query) {
		
		$query->limit($this->_offset, $this->_numberofRows);
		return $query;
	}
	
	/**
	 * Get fields
	 * 
	 * Will always return an empty iterator as a LIMIT filter doesn't filter
	 * on fields
	 * 
	 * @return ArrayIterator
	 */
	public function getFields() {
		return new ArrayIterator();
	}
}