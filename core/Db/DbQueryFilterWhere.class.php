<?php
/**
 * M_DbQueryFilterWhere class
 *
 * @package Core
 * @access public
 */
class M_DbQueryFilterWhere extends M_DbQueryFilter 
{
	/**
	 * Comparison operator constants
	 */
	const EQ = 'EQ';
	const NEQ = 'NEQ';
	const LT = 'LT';
	const LTEQ = 'LTEQ';
	const GT = 'GT';
	const GTEQ = 'GTEQ';
	const BEGINSWITH = 'BEGINSWITH';
	const ENDSWITH = 'ENDSWITH';
	const CONTAINS = 'CONTAINS';
	const IN = 'IN';
	const NIN = 'NOT IN';
	const IS = 'IS';
	const ISN = 'IS NOT';
	
	/**
	 * Subfilters
	 * 
	 * Every filter can contain subfilters, this is a flexible way to create a complex
	 * set of conditions. This array stores the add subfilters and the used sql operator
	 * 
	 * @var array
	 * @example $_subfilters = array( 
	 * 		array( 'subfilter' => $subfilter, 'logical' => 'OR' ) , 
	 * 		array( 'subfilter' => $subfilter2, 'logical' => 'AND' ) 
	 * );
	 */
	protected $_subfilters = array();
	
	/**
	 * Conditions
	 * 
	 * The conditions which will be applied to the DbQuery
	 * 
	 * NOTE: the first condition will always have null as logical operator
	 * 
	 * @var array
	 * @example $_conditions = array( 
	 * 		array( 'field' => 'id', 'value' => 2, 'comparison' => '=', 'logical' => null),
	 * 		array( 'field' => 'price', 'value' => 186, 'comparison' => '<=', 'logical' => 'AND')
	 * );
	 */
	protected $_conditions = array();
	
	/**
	 * Construct
	 *
	 * @param str $field
	 * @param mixed $value
	 * @param str $comparison
	 * @return void
	 */
	public function __construct($field, $value, $comparison = self::EQ, $binary = FALSE) {
		$this->addCondition($field, $value, $comparison, null, $binary);
	}
	
	/**
	 * Add a condition
	 * 
	 * @param str $field
	 * @param mixed $value
	 * @param str $comparison
	 * @param str $logical
	 * @return M_DbQueryFilterWhere
	 */
	public function addCondition($field, $value, $comparison = self::EQ, $logical = M_DbQuery::OPERATOR_AND, $binary = FALSE) {
		// automatically replace EQ and NEQ operators for NULL values
		if(is_null($value)) {
			if($comparison == self::EQ) {
				$comparison = self::IS;
			}
			elseif ($comparison == self::NEQ) {
				$comparison = self::ISN;
			}
		}
		
		$this->_conditions[] = array(
			'field'      => $field,
			'value'      => $value,
			'comparison' => $comparison,
			'logical'    => $logical,
			'binary'     => (bool) $binary
		);
		
		return $this;
	}
	
	
	/**
	 * Get conditions of a specific field
	 * 
	 * @param str $field
	 * @return ArrayIterator $result
	 */
	public function getConditions($field) {
		$result = array();
		foreach($this->_conditions AS $value) {
			if($value['field'] == $field) {
				$result[] = $value;
			}
		}
		
		return new ArrayIterator($result);
	}
	
	/**
	 * Reset conditions
	 * 
	 * Reset all extra conditions (added by using {@link M_DbQueryFilterWhere::addCondition()}. 
	 * This will only keep the first applied condition (specified in construct)
	 * 
	 * @return M_DbQueryFilterWhere
	 */
	public function resetConditions() {
		$this->_conditions = array_shift($this->_conditions);
		return $this;
	}
	
	/**
	 * Reset ALL conditions
	 * 
	 * @return M_DbQueryFilterWhere
	 */
	public function resetAllConditions() {
		$this->_conditions = array();
		return $this;
	}
	
	/**
	 * Add a subfilter
	 * 
	 * After applying the base conditions, it's possible to apply subfilters. This provides
	 * a flexible way to create complex sql conditions.
	 * 
	 * @example 
	 * 
	 * //We commence by specifying the base conditions
	 * $filter = new M_DbQueryFilterWhere('product_id', 5, '=');
	 * $filter->addCondition('productName', 'phone', '=', 'OR');
	 * 
	 * //Afterwards we can add a subfilter containing extra conditions which will
	 * //be grouped in a separate condition string
	 * $subfilter = new M_DbQueryFilterWhere('product_price', 100, '<=');
	 * $subfilter->addCondition('product_price', 200, '>=', 'OR');
	 * 
	 * //Finally we apply the subfilter
	 * $filter->addSubfilter($subfilter, 'OR');
	 * 
	 * @param $subfilter M_DbQueryFilterWhere
	 * @param $logical str
	 * 			The logical operator
	 * @return M_DbQueryFilterWhere
	 */
	public function addSubfilter(M_DbQueryFilterWhere $subfilter, $logical = M_DbQuery::OPERATOR_AND) {
		$this->_subfilters[] = array('subfilter' => $subfilter, 'logical' => $logical);
		return $this;
	}
	
	/**
	 * Reset subfilters
	 * 
	 * Remove all subfilters and prevent the filter from applying them when calling the filter method
	 * 
	 * @return M_DbQueryFilterWhere
	 */
	public function resetSubfilters() {
		$this->_subfilters = array();
		return $this;
	}
	
	/**
	 * Apply the filter
	 *
	 * @param M_DbSelect $query
	 * @param M_DataObjectMapper $mapper
	 * 		Provide the dataobjectmapper which is used to translate field-names
	 * 		and values
	 * @return M_DbSelect $query
	 */
	public function filter( M_DbQuery $query, M_DataObjectMapper $mapper = null) {
		// If at least 1 condition has been added to the filter:
		if(count($this->_conditions) > 0) {
			// loop through conditions
			foreach( $this->_conditions AS $condition) {
				$field = $condition['field'];
				$value = $condition['value'];
				$comparison = $condition['comparison'];
				$logical = $condition['logical'];
				$binary = $condition['binary'];

				//if a mapper exists, we use it to translate field name and value
				//to the corresponding db-name and db-value
				//e.g M_Date to MySQL-DATETIME
				if (!is_null($mapper)) {
					$value = $this->_getMapperValue($value, $field, $mapper);
					$field = $mapper->getFieldDbNameLocalized($field);
				}
				
				// WHERE IN ('x','y','z')
				if ($comparison == self::IN ) {
					if ($logical == M_DbQuery::OPERATOR_OR) {
						$query->orWhereIn($field, $value, $binary);
					} else {
						$query->whereIn($field, $value, M_DbQuery::OPERATOR_AND, $binary);
					}
				}
				// WHERE NOT IN ('x','y','z')
				elseif ($comparison == self::NIN ) {
					if ($logical == M_DbQuery::OPERATOR_OR) {
						$query->orWhereNotIn($field, $value, $binary);
					} else {
						$query->whereNotIn($field, $value, M_DbQuery::OPERATOR_AND, $binary);
					}
				}
				// LIKE 'x%' / LIKE '%x' / LIKE '%x%'
				elseif (in_array($comparison, array(self::BEGINSWITH,self::ENDSWITH,self::CONTAINS))) {
					$like = null;
					switch( $comparison)
					{
						case self::BEGINSWITH:
							$like = 'left';
							break;
						case self::ENDSWITH:
							$like = 'right';
							break;
						default:
							$like = 'middle';
							break;
					}
					
					if ($logical == M_DbQuery::OPERATOR_OR) {
						$query->orWhereContains($field, $value, $like, $binary);
					} else {
						$query->whereContains($field, $value, $like, M_DbQuery::OPERATOR_AND, $binary);
					}
				}
				//OR
				elseif ($logical == M_DbQuery::OPERATOR_OR) {
					$query->orWhere(($binary ? 'BINARY ' : '' ) . $field . ' ' . self::getSqlComparisonSyntax($comparison) . ' ?', $value);
				}
				
				//AND
				else {
					$query->where(($binary ? 'BINARY ' : '' ) . $field . ' ' . self::getSqlComparisonSyntax($comparison) . ' ?', $value);
				}
			}
		}
		
		// If at least 1 subfilter has been added to the filter:
		if(count($this->_subfilters) > 0) {
			// Loop through the subfilters:
			foreach ($this->_subfilters AS $subfilterHolder) {
				//open a block,this way all subfilter conditions will be grouped
				//in a separate block
				$query->openWhereBlock($subfilterHolder['logical']);
				
				//apply filter
				$subfilter = $subfilterHolder['subfilter'];
				/* @var $subfilter M_DbQueryFilterWhere */
				
				//apply subfilter
				$subfilter->filter($query, $mapper);
				
				//close block
				$query->closeWhereBlock();
			}
		}
		
		// We're done! Return the query:
		return $query;
	}
	
	/**
	 * Get the sql syntax for the given operator
	 * 
	 * @static
	 * @access public
	 * @param string $operator
	 *		For example: {@link M_DbQueryFilterWhere::GTEQ}
	 * @return string
	 */
	public static function getSqlComparisonSyntax($operator) {
		switch($operator) {
			case self::EQ:
				return '=';
			
			case self::NEQ:
				return '!=';
			
			case self::LT:
				return '<';
			
			case self::LTEQ:
				return '<=';
			
			case self::GT:
				return '>';
			
			case self::GTEQ:
				return '>=';
				
			case self::CONTAINS:
				return 'LIKE';
				
			default:
				return $operator;
		}
	}
	
	/**
	 * Get the value which should be used in a query using an {@link M_DataObjectMapper}
	 *
	 * @param mixed $value
	 * @param string $fieldName
	 * @param M_DataObjectMapper $mapper
	 * @return mixed
	 */
	private function _getMapperValue($value, $fieldName, M_DataObjectMapper $mapper) {
		if (is_array($value)) {
			$values = array();
			foreach($value AS $key => $val) {
				$values[$key] = $mapper->getDbValueByFieldName($fieldName, $val);
			}
			$value = $values;
		}else {
			$value = $mapper->getDbValueByFieldName($fieldName, $value);
		}
		
		return $value;
	}
	
	/**
	 * Get all fields for this filter, including the ones of the subfilters
	 * 
	 * @return ArrayIterator
	 */
	public function getFields() {
		$fields = array();
		foreach($this->_conditions AS $condition) {
			$fields[] = $condition['field'];
		}
		
		foreach ($this->_subfilters AS $filterHolder) {
			/* @var $filter M_DbQueryFilterWhere */
			$filter = M_Helper::getArrayElement('subfilter',$filterHolder);
			$fields = array_merge(
				$fields,
				$filter->getFields()->getArrayCopy()
			);
		}
		
		return new ArrayIterator($fields);
	}
}