<?php
// Used to register and dispatch database connections
class M_Db {

	/**
	 * @var string name of default db-connection id
	 */
	const _DEFAULT = 'default';
	
	/**
	 *  Constants to set the fetch mode . This will affect how the
	 *  code interacts with database query result sets.
	 */
	const FETCH_ASSOC    = 'assoc';
	const FETCH_NUMERIC  = 'num';
	const FETCH_BOTH     = 'both';
	const FETCH_OBJECT   = 'obj';
	const FETCH_ITERATOR = 'iter';
	
	/**
	 *  Constants to get properties from a connection
	 */
	const SQL_TABLES = 'metaTablesSQL';
	const SQL_COLUMS = 'metaColumnsSQL';

	/**
	 * Info about the connections. Per connection, we store a:
	 * - connection id (custom name)
	 * - connection url
	 * - connection driver
	 *
	 * @var array
	 */
	private static $_connections = array();
	
	/**
	 * Singleton instances
	 * 
	 * @var array
	 */
	private static $_instances = array();
	
	/**
	 * Singleton constuctor
	 *
	 * @static 
	 * @access public
	 * @param string $id
	 * 		The Connection ID
	 * @return MI_Db
	 */
	public static function getInstance($id = self::_DEFAULT) {
		if(!isset(self::$_connections[$id])) {
			throw new M_DbException(sprintf('Unregistered connection "%s"', $id));
		}
		if(!isset(self::$_instances[$id])) {
			// Construct the driver (we construct a generic driver, because
			// we're using ADODB under the hood)
			self::$_instances[$id] = new M_DbDriverAdo(strtolower(self::$_connections[$id][0]));
			
			// establish the connection
			$url = new M_Uri(self::$_connections[$id][1]);
			self::$_instances[$id]->connect(
				$url->getHostWithPort(), 
				$url->getUsername(), 
				$url->getPassword(), 
				trim($url->getPath(), '/')
			);
		}
		return self::$_instances[$id];
	}
	
	/**
	 * Register a new connection
	 *
	 * @param string $driver driver-type (e.g. mysql)
	 * @param string $url connection-string
	 * @param string $id  id of the connection
	 * @return void
	 */
	public static function registerConnectionId($driver, $url, $id = self::_DEFAULT) {
		if(isset(self::$_connections[$id])) {
			throw new M_DbException(sprintf('The database connection "%s" has already been registered', $id));
		}
		self::$_connections[$id] = array($driver, $url);
	}
	
	/**
	 *  Check if connection has been registered
	 *
	 * @param string $id
	 * @return bool
	 */
	public static function isRegisteredConnection($id) {
		return (isset(self::$_connections[$id]) ? TRUE : FALSE);
	}
}