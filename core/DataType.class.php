<?php
/**
 * M_DataType
 * 
 * Because typecasting in php is so bad, it is sometimes a pain to check whether
 * a provided value is a proper integer, floated value or string, for example
 * because all values received from the database are strings. This class allows
 * for proper data type checking.
 * 
 * @author Tim
 * @package Core
 */
class M_DataType extends M_Object {
	
	/* -- GETTERS -- */
	
	/**
	 * Is integer?
	 * 
	 * @access public
	 * @param mixed $input
	 * @return bool
	 */
	public static function isInteger($input) {
		// Because is_int() does not consider a string '10' as being a proper
		// integer, we write our own validation: if the provided input is
		// numeric (which does validate strings in php) and the string
		// comparison of the (int) casted string value vs the regular string
		// version is equal, then the provided input is a valid integer. This
		// makes sure provided floated values (wheter a string or not) do not
		// validate
		return (is_numeric($input) && (string) (int) $input == (string) $input);
	}
	
	/**
	 * Is floating number?
	 * 
	 * @access public
	 * @param mixed $input
	 * @return bool
	 */
	public static function isFloat($input) {
		return (
			is_numeric($input) && 
			(string) (float) (string) $input == (string) $input &&
			(int) $input != (float) $input
		);
	}
	
	/**
	 * Get floating number
	 * 
	 * This method will cast the provided (string) input to a floating number. Note that
	 * you should first make sure that the value represents a floating number, with the
	 * method {@link M_DataType::isFloat()}.
	 * 
	 * @access public
	 * @param mixed $input
	 * @return float
	 */
	public static function getFloat($input) {
		return ((float) (string) $input);
	}
}