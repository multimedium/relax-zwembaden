<?php
/**
 * M_AjaxResponse
 * 
 * @package Core
 */
class M_AjaxResponse extends M_Object {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Response Format: XML
	 * 
	 * This constant can be used to describe the format in which the AJAX response 
	 * needs to be outputted. With this particular constant, you describe the 
	 * format XML.
	 * 
	 * @access public
	 * @var string
	 */
	const FORMAT_XML = 'xml';
	
	/**
	 * Response Format: JSON
	 * 
	 * This constant can be used to describe the format in which the AJAX response 
	 * needs to be outputted. With this particular constant, you describe the 
	 * format JSON.
	 * 
	 * @access public
	 * @var string
	 */
	const FORMAT_JSON = 'json';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Outcome flag
	 * 
	 * This property stores a boolean that describes the outcome of the AJAX
	 * request: TRUE if the response needs to describe success, FALSE if failure
	 * is the answer to the request.
	 * 
	 * @see M_AjaxResponse::setOutcomeFlag()
	 * @access private
	 * @var bool
	 */
	private $_outcome;
	
	/**
	 * Error Message
	 * 
	 * This property stores a string, containing the error message to be included
	 * in the response. Typically, this error message is set only if the outcome
	 * is set to FALSE.
	 * 
	 * @see M_AjaxResponse::setOutcomeFlag()
	 * @see M_AjaxResponse::setErrorMessage()
	 * @access private
	 * @var string
	 */
	private $_errorMessage;
	
	/**
	 * Error Code
	 * 
	 * This property stores a string, containing the error code to be included
	 * in the response. Typically, this error code is set only if the outcome
	 * is set to FALSE.
	 * 
	 * @see M_AjaxResponse::setOutcomeFlag()
	 * @see M_AjaxResponse::setErrorCode()
	 * @access private
	 * @var string
	 */
	private $_errorCode;
	
	/**
	 * Format
	 * 
	 * This property stores an integer that describes the format in which the 
	 * AJAX response needs to be outputted.
	 * 
	 * @see M_AjaxResponse::setOutputFormat()
	 * @access private
	 * @var bool
	 */
	private $_format;
	
	/* -- SETTERS -- */
	
	/**
	 * Set outcome flag
	 * 
	 * This method allows to describe the outcome of the AJAX request. If you
	 * want to use this view to describe a successful outcome of the AJAX response,
	 * then you should provide (boolean) TRUE to this method. If a failure needs 
	 * to be outputted, you should provide (boolean) FALSE instead.
	 * 
	 * @access public
	 * @param bool $flag
	 * @return M_AjaxResponse $response
	 *		Returns itself, for a fluent programming interface
	 */
	public function setOutcomeFlag($flag) {
		$this->_outcome = (bool) $flag;
		return $this;
	}
	
	/**
	 * Error Message
	 * 
	 * This method allows to set a string, representing the error message to be 
	 * included in the AJAX response. Typically, this error message is set only 
	 * if the outcome is set to FALSE.
	 * 
	 * @see M_AjaxResponse::setOutcomeFlag()
	 * @access public
	 * @param string $message
	 * @return M_AjaxResponse $response
	 *		Returns itself, for a fluent programming interface
	 */
	public function setErrorMessage($message) {
		$this->_errorMessage = $message ? (string) $message : NULL;
		return $this;
	}
	
	/**
	 * Error Code
	 * 
	 * This method allows to set a string, representing the error code to be 
	 * included in the AJAX response. Typically, this error code is set only 
	 * if the outcome is set to FALSE.
	 * 
	 * @see M_AjaxResponse::setOutcomeFlag()
	 * @access public
	 * @param string $code
	 * @return M_AjaxResponse $response
	 *		Returns itself, for a fluent programming interface
	 */
	public function setErrorCode($code) {
		$this->_errorCode = $code ? (string) $code : NULL;
		return $this;
	}
	
	/**
	 * Set format
	 * 
	 * This class is used to send a response to an AJAX request. Typically, these
	 * responses are formatted in either XML or JSON. With this method, you can
	 * specify the format in which to output the AJAX response.
	 * 
	 * @see M_AjaxResponse::FORMAT_XML
	 * @see M_AjaxResponse::FORMAT_JSON
	 * @access public
	 * @param integer $format
	 * @return M_AjaxResponse $response
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFormat($format) {
		// Cast to lowercase string:
		$format = strtolower((string) $format);
		
		// Make sure the provided format is recognized:
		if(! in_array($format, array(self::FORMAT_JSON, self::FORMAT_XML))) {
			// If not, we throw an exception:
			throw new M_AjaxException(sprintf(
				'Cannot set format of AJAX Response to "%s"; Unrecognized format. ' . 
				'The format should be either "%s" or "%s"',
				$format,
				self::FORMAT_JSON,
				self::FORMAT_XML
			));
		}
		
		// Set the format:
		$this->_format = $format;
		
		// Return myself:
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get outcome flag
	 * 
	 * @see M_AjaxResponse::setOutcomeFlag()
	 * @access public
	 * @return bool
	 */
	public function getOutcomeFlag() {
		return $this->_outcome;
	}
	
	/**
	 * Get error message
	 * 
	 * @see M_AjaxResponse::setErrorMessage()
	 * @access public
	 * @return string
	 */
	public function getErrorMessage() {
		return $this->_errorMessage;
	}
	
	/**
	 * Get error code
	 * 
	 * @see M_AjaxResponse::setErrorCode()
	 * @access public
	 * @return string
	 */
	public function getErrorCode() {
		return $this->_errorCode;
	}
	
	/**
	 * Get format
	 * 
	 * This method will return the format in which the AJAX Response is being 
	 * rendered. This format can be set with {@link M_AjaxResponse::setFormat()}.
	 * 
	 * If no format has been defined earlier, this method will try to look up
	 * the desired format by looking at the page request variable with name
	 * "format". The value of this variable must be either "xml" or "json".
	 * 
	 * If still no format can be determined with the page request variable, then
	 * this method will default the format to "json".
	 * 
	 * @access public
	 * @return integer
	 */
	public function getFormat() {
		// If no format has been set
		if(! $this->_format) {
			// Then, we define it now, by checking whether or not a request 
			// variable has been provided to set the format. Note that we default 
			// to JSON:
			$this->setFormat(M_Request::getVariable('format', self::FORMAT_JSON, M_Request::TYPE_STRING_SANITIZED, M_Request::GET));
		}

		// Return the view mode:
		return $this->_format;
	}
	
	/**
	 * Is format XML?
	 * 
	 * @access public
	 * @return bool $flag
	 *		Returns TRUE if the format of the response is XML, FALSE if not
	 */
	public function isFormatXml() {
		return ($this->getFormat() == self::FORMAT_XML);
	}
	
	/**
	 * Is format JSON?
	 * 
	 * @access public
	 * @return bool $flag
	 *		Returns TRUE if the format of the response is JSON, FALSE if not
	 */
	public function isFormatJson() {
		return ($this->getFormat() == self::FORMAT_JSON);
	}
	
	/**
	 * Send...
	 * 
	 * This method will send the AJAX response back to the client that initiated
	 * the request.
	 * 
	 * @access public
	 * @uses M_View::_render()
	 * @see M_View::fetch()
	 * @return void
	 */
	public function send() {
		// If the response is JSON
		if($this->isFormatJson()) {
			// Output the encoded string:
			echo $this->fetch();
		}
		// If the response is XML
		elseif($this->isFormatXml()) {
			// Then, first, we send the XML headers:
			M_Header::send(M_Header::CONTENT_TYPE_XML);
			
			// And, finally, we send the XML string:
			echo $this->fetch();
		}
		// If the response format is not known:
		else {
			// Then, we throw an exception to inform about the error:
			throw new M_AjaxException(sprintf(
				'Cannot send AJAX Response. The output format has not been ' . 
				'specified!'
			));
		}
	}
	
	/**
	 * Render the output
	 * 
	 * This method will render the contents of the AJAX Response. Typically, this
	 * method will be overwritten by subclasses, in order to include the specifics
	 * of the response.
	 * 
	 * @uses M_AjaxResponse::_getFromArray()
	 * @access public
	 * @return string
	 */
	public function fetch() {
		// Parse an array with the parameters in the view:
		return $this->_getFromArray(array(
			'outcome' => $this->getOutcomeFlag(),
			'errorMessage' => $this->getErrorMessage(),
			'errorCode' => $this->getErrorCode()
		));
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get JSON string from associative array
	 * 
	 * This method will render the output string that serves as the AJAX response.
	 * The result of this method will depend on the format that has been specified
	 * for the AJAX response.
	 * 
	 * @access protected
	 * @param array $array
	 * @return string
	 */
	protected function _getFromArray(array $array) {
		// If the response is JSON:
		if($this->isFormatJson()) {
			// Then, we output the array as JSON:
			return $this->_getJsonFromArray($array);
		}
		// If the response is XML
		elseif($this->isFormatXml()) {
			// Then, we output the array as XML
			$out  = '<?xml version="1.0" encoding="UTF-8"?>';
			$out .= '<response>';
			$out .=    $this->_getXmlFromArray($array);
			$out .= '</response>';
			return $out;
		}
		// If the response format is not known:
		else {
			// Then, we throw an exception to inform about the error:
			throw new M_AjaxException(sprintf(
				'Cannot output AJAX Response Values (Array). The output ' . 
				'format has not been specified!'
			));
		}
	}
	
	/**
	 * Get JSON string from associative array
	 * 
	 * This method will render the output string that serves as the AJAX response.
	 * The result of this method will be in JSON format.
	 * 
	 * @access protected
	 * @param array $array
	 * @return string
	 */
	protected function _getJsonFromArray(array $array) {
		return M_Helper::jsonEncode($array);
	}
	
	/**
	 * Get XML string from associative array
	 * 
	 * This method will render the output string that serves as the AJAX response.
	 * The result of this method will be in XML format.
	 * 
	 * @access protected
	 * @param array $array
	 * @return M_XmlElement
	 */
	protected function _getXmlFromArray(array $array) {
		// The output string
		$out = '';
		
		// For each of the elements in the array:
		foreach($array as $name => $value) {
			// We add a node to the XML output string:
			$out .= '<' . $name;
			
			// If a value is not available:
			if(! $value) {
				// Then, we create a self-closing tag:
				$out .= ' />';
			}
			// If a value is available:
			else {
				// Then, we close the opening tag:
				$out .= '>';
				
				// We add the value to the output string:
				$out .= $this->_getXmlValue($value);
				
				// And we close the tag:
				$out .= '</' . $name . '>';
			}
		}
		
		// Return the XML Output String
		return $out;
	}
	
	/**
	 * Get XML value
	 * 
	 * This method will convert the provided value into a string that may be
	 * used as a value of an XML tag.
	 * 
	 * @access protected
	 * @param mixed $value
	 * @return string
	 */
	protected function _getXmlValue($value) {
		// If the provided value is numeric:
		if(is_numeric($value)) {
			// Then, we return the value unaffected:
			return $value;
		}
		// If the provided value is a string:
		elseif(is_string($value)) {
			// Then, first, we check if the value contains characters that need
			// to be wrapped by a CDATA block:
			if(M_Helper::containsCharactersForXmlCdata($value)) {
				// If so, we wrap the value and return:
				return '<![CDATA[' . $value . ']]>';
			}
			// If not to be wrapped:
			else {
				// Then, return the value as-is:
				return $value;
			}
		}
		// If the provided value is an array
		elseif(is_array($value)) {
			// Then, we convert the array to XML
			return $this->_getXmlFromArray($value);
		}
		// If the provided value is NULL
		elseif(is_null($value)) {
			// Then, we return an empty value:
			return '';
		}
		// If the provided value is a boolean flag:
		elseif(is_bool($value)) {
			// Then, we return either 0 or 1
			return ($value ? 1 : 0);
		}
		// Any other value cannot be translated into XML for now:
		else {
			// We throw an exception if we cannot translate the value:
			throw new M_AjaxException(sprintf(
				'Cannot output AJAX Response Value to XML: %s',
				var_export($value, TRUE)
			));
		}
	}
	
	/**
	 * Get HTML Output from view
	 * 
	 * This method will accept a view as argument, and will render the HTML source
	 * code that is to be outputted as part of the AJAX response. Among others,
	 * this method will remove new lines and tabs from the source code.
	 * 
	 * @access protected
	 * @param M_ViewHtml $view
	 * @return string
	 */
	protected function _getHtml(M_ViewHtml $view) {
		// Render the HTML:
		$html = $view->fetch();
		
		// Remove new lines and tabs
		$html = str_replace("\r", '', $html);
		$html = str_replace("\n", '', $html);
		$html = str_replace("\t", '', $html);
		
		// Return the HTML:
		return $html;
	}
}