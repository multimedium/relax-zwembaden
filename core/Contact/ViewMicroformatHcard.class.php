<?php
/**
 * M_ViewMicroformatHcard class
 * 
 * M_ViewMicroformatHcard, a subclass of {@link M_View}, renders the 
 * view of the hCard Microformat. This microformat is a semantic 
 * markup of a person's or entity's contact data.
 * 
 * See {@link http://microformats.org} for more information on
 * microformats.
 * 
 * @package Core
 */
class M_ViewMicroformatHcard extends M_ViewCoreHtml {
	/**
	 * Contact
	 * 
	 * This property stores the contact person/entity. Note that this data is
	 * being represented by an instance of {@link M_Contact}
	 * 
	 * @access protected
	 * @var M_Contact
	 */
	protected $_contact = array();
	
	/* -- SETTERS -- */
	
	/**
	 * Set Contact Person/Entity
	 * 
	 * @access public
	 * @param M_Contact $contact
	 * 		The contact person/entity
	 * @return void
	 */
	public function setContact(M_Contact $contact) {
		$this->_contact = $contact;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Pre-Processing
	 * 
	 * @access protected
	 * @throws M_ViewException
	 * @return void
	 */
	protected function preProcessing() {
		if(! $this->_contact) {
			throw new M_ViewException('Cannot render hCard Microformat; missing Contact Data!');
		}
	}
	
	/**
	 * Default HTML rendering
	 * 
	 * @access protected
	 * @return string
	 */
	public function getHtml() {
		// Start hCard container
		$html  = '<div class="vcard" id="'. $this->getId() .'">';
		
		// Picture
		$temp = $this->_contact->getPicture();
		if($temp) {
			$html .= '<img class="photo" src="'. $temp->getPath() .'" alt="' . htmlentities($this->_contact->getName()) . '" />';
		}
		
		// Name
		$html .=    '<div class="fn">' . $this->_contact->getName() . '</div>';
		
		// Address
		$address = $this->_contact->getAddress();
		if($address) {
			$html .= '<div class="adr">';
			
			$temp = $address->getStreetAddress();
			if($temp) {
				$html .= '<div class="street-address">';
				$html .=    $temp;
				$html .= '</div>';
			}
			
			$temp = $address->getPostalCode();
			if($temp) {
				$html .= '<span class="postal-code">';
				$html .=    $temp;
				$html .= '</span>';
			}
			
			$temp = $address->getCity();
			if($temp) {
				if($address->getCity()) {
					$html .= ' ';
				}
				$html .= '<span class="locality">';
				$html .=    $temp;
				$html .= '</span>';
			}
			
			$temp = $address->getRegion();
			if($temp) {
				$html .= '<div class="region">';
				$html .=    $temp;
				$html .= '</div>';
			}
			
			$temp = $address->getCountry();
			if($temp) {
				$html .= '<div class="country-name">';
				$html .=    $temp;
				$html .= '</div>';
			}
			
			$html .= '</div>';
		}
		
		// Email(s)
		foreach($this->_contact->getEmails() as $temp) {
			$html .=    '<div>';
			$html .=       '<a href="mailto:'. $temp->getEmail() .'">';
			$html .=          '<span class="email">'. $temp->getEmail() .'</span>';
			$html .=       '</a>';
			$html .=    '</div>';
		}
		
		// Website(s)
		foreach($this->_contact->getWebsites() as $temp) {
			$html .=    '<a class="url" href="'. $temp->getUri() .'">';
			$html .=       $temp->getUri();
			$html .=    '</a>';
		}
		
		// Phone number(s)
		foreach($this->_contact->getPhoneNumbers() as $temp) {
			$html .=    '<div class="tel">';
			$html .=       '<span class="type">'.  $temp->getType() .'</span>: ';
			$html .=       '<span class="value">'. $temp->getCompleteNumber() .'</span>';
			$html .=    '</div>';
		}
		
		// Finish hCard container
		$html .= '</div>';
		return $html;
	}
	
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath() . '/MicroformatHcard.tpl');
	}
}
?>