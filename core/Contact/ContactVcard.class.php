<?php
/**
 * M_ContactVcard class
 * 
 * M_ContactVcard can be used to generate the contents of a vCard file. For more
 * information about vCard files, read the documentation that is available at
 * wikipedia: {@link http://en.wikipedia.org/wiki/Vcard}
 * 
 * @package Core
 */
class M_ContactVcard {
	/**
	 * vCard File Extension
	 * 
	 * This constants holds the file extension that is used to store a vCard file.
	 * This is also used by {@link M_ContactVcard::getSuggestedFilename()}, to
	 * compose the full filename.
	 */
	const FILE_EXTENSION = 'vcf';
	
	/**
	 * Contact
	 * 
	 * This property stores the contact person/entity, out of which a vcard should
	 * be generated.
	 * 
	 * @access protected
	 * @var M_Contact
	 */
	protected $_contact;
	
	/**
	 * Note
	 * 
	 * Holds the note that is to be added to the vCard
	 * 
	 * @see ContactVcard::setNote()
	 * @access private
	 * @var string
	 */
	private $_note;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param M_Contact $contact
	 * 		The contact person/entity from which to extract info for the vCard,
	 * 		represented by an instance of {@link M_Contact}
	 * @return M_ContactVcard
	 */
	public function __construct(M_Contact $contact = NULL) {
		if($contact) {
			$this->setContact($contact);
		}
	}
	
	/**
	 * Set contact person/entity
	 * 
	 * @access public
	 * @param M_Contact $contact
	 * 		The contact person/entity from which to extract info for the vCard,
	 * 		represented by an instance of {@link M_Contact}
	 * @return void
	 */
	public function setContact(M_Contact $contact) {
		$this->_contact = $contact;
	}
	
	/**
	 * Get contact
	 * 
	 * Will provide with the {@link M_Contact} instance that may have been provided
	 * earlier to {@link M_ContactVcard::setContact()}.
	 * 
	 * @access public
	 * @return M_Contact
	 */
	public function getContact() {
		return $this->_contact;
	}
	
	/**
	 * Set Note
	 * 
	 * Holds the note that is to be added to the vCard
	 * 
	 * @access public
	 * @param string $note
	 * @return ContactVcard
	 *		Returns itself, for a fluent programming interface
	 */
	public function setNote($note) {
		$this->_note = (string) $note;
		return $this;
	}
	
	/**
	 * Get Note
	 * 
	 * @access public
	 * @return string
	 */
	public function getNote() {
		return $this->_note;
	}
	
	/**
	 * Render the vcard contents
	 * 
	 * Will render the contents of a vCard file. Check the documentation on
	 * {@link http://en.wikipedia.org/wiki/Vcard}, for more information about 
	 * a vCard's format.
	 * 
	 * NOTE:
	 * Currently, this method will render the 3.0 version.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// If no contact person/entity is available, throw an exception:
		if(! $this->_contact) {
			throw new M_ContactException(
				'Cannot generate vCard; Missing contact person/entity'
			);
		}
		
		// New lines in the string output:
		$nl = "\n";
		
		// Start the vcard:
		$out  = 'BEGIN:VCARD' . $nl;
		$out .= 'VERSION:3.0' . $nl;
		
		// Add first name and surnames, if available separately
		$f = $this->_contact->getFirstname();
		$s = $this->_contact->getSurnames();
		if($f && $s) {
			$out .= 'N:' . $s . ';' . $f . $nl;
		}
		
		// Add logo, if any:
		$logo = $this->_contact->getPicture();
		if($logo && $logo->exists()) {
			$out .= 'PHOTO;ENCODING=BASE64;TYPE=' . strtoupper($logo->getFileExtension()) . ':' . $logo->getBase64Encoded() . $nl;
		}
		
		// Add full name:
		$out .= 'FN:' . $this->_contact->getName() . $nl;
		
		// Add name of organisation (if any)
		$f = $this->_contact->getOrganisation();
		if($f) {
			$out .= 'ORG:' . $f . $nl;
		}
		
		// Add nickname (if any)
		$f = $this->_contact->getNickname();
		if($f) {
			$out .= 'TITLE:' . $f . $nl;
		}
		
		// Add phone number(s) (if any)
		foreach($this->_contact->getPhoneNumbers() as $f) {
			$out .= 'TEL;TYPE='. $f->getType() .',VOICE:' . $f->getCompleteNumber() . $nl;
		}
		
		// Add address(es) (if any)
		foreach($this->_contact->getAddresses() as $f) {
			$out .= 'ADR;TYPE='. $f->getType() .':;;'. $f->getStreetAddress() .';'. $f->getCity() .';'. $f->getRegion() .';'. $f->getPostalCode() .';' . $f->getCountry() . $nl;
			$out .= 'LABEL;TYPE='. $f->getType() .':'. $f->getStreetAddress() .'\n'. $f->getPostalCode() .' '. $f->getCity() .'\n' . $f->getCountry() . $nl;
		}
		
		// Add email(s) (if any)
		foreach($this->_contact->getEmails() as $f) {
			$out .= 'EMAIL;TYPE='. $f->getType() .',INTERNET:' . $f->getEmail() . $nl;
		}
		
		// If we are to add a note
		if($this->getNote()) {
			$out .= 'NOTE;CHARSET=utf-8;ENCODING=QUOTED-PRINTABLE:' . $this->getNote() . $nl;
		}
		
		// Finish the vcard string:
		$out .= 'END:VCARD';
		
		// Return the rendered string:
		return $out;
	}
	
	/**
	 * Get suggested filename
	 * 
	 * Will suggest a filename for the vCard contents, based on the contact
	 * person/entity's data.
	 * 
	 * @access public
	 * @return string
	 */
	public function getSuggestedFilename() {
		// If no contact person/entity is available, throw an exception:
		if(! $this->_contact) {
			throw new M_ContactException(
				'Cannot generate vCard; Missing contact person/entity'
			);
		}
		
		// Compose the filename:
		$fn = new M_FilterTextAccents(new M_FilterTextValue($this->_contact->getName()));
		return M_Uri::getPathElementFromString($fn->apply()) . '.' . self::FILE_EXTENSION;
	}
}