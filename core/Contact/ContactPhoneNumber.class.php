<?php
class M_ContactPhoneNumber {
	const TYPE_PHONE = 'phone';
	const TYPE_MOBILE = 'mobile';
	const TYPE_FAX   = 'fax';
	
	protected $_number;
	protected $_countryCode;
	protected $_areaCode;
	protected $_type;
	
	public function __construct($number, $type = self::TYPE_PHONE) {
		$this->setNumber($number);
		$this->setType($type);
	}
	
	public function setNumber($number) {
		$this->_number = (string) $number;
	}
	
	public function setCountryCode($code) {
		$code = (string) trim($code);
		if(!strncmp($code, '00', 2)) {
			$code = '+' . substr($code, 2);
		}
		$this->_countryCode = (string) trim($code);
	}
	
	public function setAreaCode($code) {
		$this->_areaCode = (string) $code;
	}
	
	public function setType($type) {
		$this->_type = (string) $type;
	}
	
	public function getNumber() {
		return $this->_number;
	}
	
	public function getCountryCode() {
		return $this->_countryCode;
	}
	
	public function getAreaCode() {
		return $this->_areaCode;
	}
	
	public function getCompleteNumber() {
		$n = array();
		$c = $this->getCountryCode();
		if($c) {
			$n[] = $c;
		}
		
		$a = $this->getAreaCode();
		if($c == '+32') {
			$a = '(0)' . ltrim($a, '0');
		}
		if($a) {
			$n[] = $a;
		}
		
		$p = $this->getNumber();
		if($p) {
			$n[] = $p;
		}
		return implode(' ', $n);
	}
	
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Return this phoneNumber as a string
	 * 
	 * @see M_ContactPhoneNumber
	 * @return string
	 * @author b.brughmans
	 */
	public function toString() {
		return $this->getCompleteNumber();
	}
	
	/**
	 * Return this phoneNumber as a string (magic function)
	 * 
	 * @see toString
	 * @return string
	 */
	public function __toString() {
		return $this->toString();
	}
}