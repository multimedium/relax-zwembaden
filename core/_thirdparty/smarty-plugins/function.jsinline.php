<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty "inline javascript" plugin
 *
 * - Type:     function
 * - Purpose:  Add inline js to page
 * 
 * This function can be used in a smarty template, to add javascript code to the
 * page. This plugin will use {@link M_ViewJavascript} to do so...
 *
 * @example
 *
 * {capture name="inlineJavascript"}
 *		$('#div').hide();
 * {/capture}
 *
 * {jsinline script=$smarty.capture.inlineJavascript}
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_jsinline($params, $smarty) {
	// Get the Javascript singleton
	$js = M_ViewJavascript::getInstance();
	$js->addInline(
		isset($params['script']) ? $params['script'] : '',
		isset($params['context']) ? $params['context'] : NULL
	);
}