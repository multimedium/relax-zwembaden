<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty number plugin
 *
 * - Type:     function
 * - Name:     date
 * - Purpose:  Format a given date
 * 
 * This function can be used in a smarty template, in order to format 
 * a given date in the currently selected locale.
 * 
 * Usage Example 1
 * <code>
 *    {date}
 * </code>
 * 
 * Note that the desired format of the date can be provided as an extra arg. For
 * example:
 * 
 * Usage Example 2
 * <code>
 *    {date format="short"}
 * </code>
 * 
 * Usage Example 3; Assuming $date is a timestamp (integer)
 * <code>
 *    {date date=$date format="full"}
 * </code>
 * 
 * Usage Example 4; Assuming $date is an {@link M_Date} instance
 * <code>
 *    {date date=$date format="full" locale="en"}
 * </code>
 * 
 * For an available list of formats, please check the documentation on the core
 * {@link M_Date} package.
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_date($params, $smarty) {
	// Has date been provided?
	$isDate = (isset($params['date']));
	if(! $isDate) {
		// Default to today, if not given a date
		$date = new M_Date();
	}
	// If date has been provided:
	else {
		// If an instance of M_Date:
		if(M_Helper::isInstanceOf($params['date'], 'M_Date')) {
			$date = $params['date'];
		}
		// If not, we use the value to construct an M_Date instance:
		else {
			$date = new M_Date($params['date']);
		}
	}
	
	// Get requested format:
	$isFormat = (isset($params['format']) && ! empty($params['format']));
	if(! $isFormat) {
		$params['format'] = 'short-date-time';
	}
	
	// Get requested locale:
	$isLocale = (isset($params['locale']) && ! empty($params['locale']));
	if(! $isLocale) {
		$params['locale'] = M_Locale::getCategory(M_Locale::LANG);
	}
	
	// Format the date:
	return M_smarty_plugin_result($date->toString($params['format'], $params['locale']), $params, $smarty);
}