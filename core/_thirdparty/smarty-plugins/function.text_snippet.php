<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty text_snippet plugin
 *
 * - Type:     function
 * - Purpose:  Create a textsnippet
 * 
 * This function can be used in a smarty template, to limit the length of textual
 * value, sometimes called 'a snippet'
 * 
 * Usage Example 1
 * <code>
 *    {text_snippet text="This is my text" length=4}
 * </code>
 * Will produce: This...
 * 
 * Usage Example 2
 * <code>
 *    {text_snippet type="This is my text" lenght=4 etc=""}
 * </code>
 * Will produce: This
 * 
 * 
 * Note that arguments for placeholders can be passed to the smarty 
 * function. Consider the following example:
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_text_snippet($params, $smarty) {
	
	$etc = false;

	// Has etc parameter been provided?
	if(isset($params['etc'])) {
		// Get etc
		$etc = $params['etc'];
	}
	
	// Has text parameter been provided?
	if(isset($params['text']) && !empty($params['text'])) {
		// Get etc
		$textString = (string) $params['text'];
	}
	else
	{
		throw new M_Exception(
			'No text parameter provided in text_snippet'
		);
	}
	
	// Has length parameter been provided?
	if(isset($params['length']) && !empty($params['length'])) {
		// Get etc
		$length = $params['length'];
	}
	else
	{
		throw new M_Exception(
			'No length parameter provided in text_snippet'
		);
	}
	
	$text = new M_Text($textString);
	
	if ($etc)
	{
		$snippet = $text->getSnippet($length, $etc);
	}
	else 
	{
		$snippet = $text->getSnippet($length);
	}
	
	return M_smarty_plugin_result($snippet, $params, $smarty);
}