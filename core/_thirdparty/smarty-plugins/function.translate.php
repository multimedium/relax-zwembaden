<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty translate plugin
 *
 * - Type:     function
 * - Name:     translate
 * - Purpose:  Translate a given text
 * 
 * This function can be used in a smarty template, to translate a given
 * text in the currently selected locale.
 * 
 * Usage Example 1
 * <code>
 *    {translate text="Hello world"}
 * </code>
 * 
 * Note that arguments for placeholders can be passed to the smarty 
 * function. Consider the following example:
 * 
 * Usage Example 2
 * <code>
 *    {translate text="Hello @username" username="Tom"}
 * </code>
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_translate($params, $smarty) {
	// Has untranslated string been provided?
	if(isset($params['text']) && !empty($params['text'])) {
		// Get untranslated string
		$text = $params['text'];
		
		// For each of the parameters:
		foreach($params as $name => $value) {
			// If not reserved keyword:
			if($name != 'text') {
				// We prepare placeholder value
				$params['@' . $name] = $value;
			}
			
			// Remove original key from array (we have already added
			// placeholder key)
			unset($params[$name]);
		}
		
		// Return the translated text:
		return M_smarty_plugin_result(t($text, $params), $params, $smarty);
	}
	// Throw exception, if not provided
	else {
		return '';
	}
	
	// Return empty string, if still here
	return '';
}
?>