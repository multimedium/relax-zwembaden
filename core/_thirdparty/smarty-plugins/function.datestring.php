<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty datestring plugin
 *
 * The datestring plugin allows you to format {@link M_Date} objects in a template
 * 
 * @example {datestring}
 * @example {datestring format="medium"}
 * @example {datestring format="full" date=$date}
 * @example {datestring format="full" date=$date assignto="datestring"}
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_datestring($params, $smarty) {
	// Check if a date has been provided:
	$isDate = (isset($params['date']) && $params['date'] instanceof M_Date);
	
	// If not, we default to a date object, with the current time
	if(! $isDate) {
		$params['date'] = new M_Date;
	}
	
	// Check if a format has been provided:
	$isFormat = (isset($params['format']) && !empty($params['format']));
	
	// If not, we default to the SHORT format
	if(! $isFormat) {
		$params['format'] = M_Date::SHORT;
	}
	
	// Return formatted date
	return M_smarty_plugin_result($params['date']->toString($params['format']), $params, $smarty);
} 