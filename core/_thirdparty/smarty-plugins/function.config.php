<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty config plugin
 *
 * - Type:     function
 * - Purpose:  Get configuration value
 * 
 * This function can be used in a smarty template, to get the value of a config
 * variable.
 * 
 * Examples
 * <code>
 *    {config module="contact" var="recipients" default="Tom Bauwens <tom@multimedium.be>"}
 * </code>
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_config($params, $smarty) {
	// Make sure a variable name has been specified
	$c = (isset($params['var']) && ! empty($params['var']));
	
	// If not
	if(! $c) {
		// We throw an exception
		throw new M_Exception(sprintf(
			'Cannot retrieve config variable; %s expects a variable name: {config var=[path] [ module=[module-id] default=[default-value] ]}',
			'smarty.config'
		));
	}
	
	// Get the default value:
	$defaultValue = isset($params['default'])
		? $params['default']
		: FALSE;
	
	// If an Application Module ID has been provided:
	if(isset($params['module']) && ! empty($params['module'])) {
		// Get the configuration variables from the module:
		$config = M_Application::getConfigOfModule((string) $params['module']);
	}
	// If no module has been specified
	else {
		$config = M_Application::getConfig();
	}

	// If a required variable has been requested:
	if(isset($params['required']) && M_Helper::isBooleanTrue($params['required'])) {
		// Then, return the required variable:
		return M_smarty_plugin_result($config->getRequired($params['var']), $params, $smarty);
	}
	
	// Get the path elements:
	$path = explode('/', $params['var']);
	$root = clone $config;
	
	// For each of the elements in the path:
	foreach($path as $element) {
		// If the current element exists in the configuration:
		if(isset($root->$element)) {
			// Then, navigate to the element
			$root = $root->$element;
		}
		// If the current element does not exist:
		else {
			// Return the default value
			return M_smarty_plugin_result($defaultValue, $params, $smarty);
		}
	}
	
	// Return the value
	return M_smarty_plugin_result($root, $params, $smarty);
}