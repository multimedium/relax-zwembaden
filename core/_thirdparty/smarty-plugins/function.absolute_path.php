<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty absolute path plugin
 *
 * - Type:     function
 * - Purpose:  Get the absolute path for a file
 *
 * This function can be used in a smarty template, get the absolute path
 * of a file
 *
 * Usage Example 1
 * <code>
 *    {absolute_path path="files/config/Application.xml"}
 * </code>
 *
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_absolute_path($params, $smarty) {
	// Has type string been provided?
	if(isset($params['path'])) {
		$path = M_Loader::getAbsolute($params['path']);
	}else {
		throw new M_Exception('Missing "path" parameter when calling absolute-path smarty-plugin');
	}

	return M_smarty_plugin_result($path, $params, $smarty);
}