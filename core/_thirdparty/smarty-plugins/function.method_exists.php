<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty method_exists plugin
 *
 * The method_exists plugin checks if a method exists in an object
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_method_exists($params, $smarty) {
	// Check params
	if (!array_key_exists('object', $params)) {
		throw new M_Exception('Missing object param');
	}
	if (!array_key_exists('method', $params)) {
		throw new M_Exception('Missing method param');
	}
	
	$result = method_exists(
		M_Helper::getArrayElement('object', $params),
		M_Helper::getArrayElement('method', $params)
	);
	
	return M_smarty_plugin_result($result , $params, $smarty);
}