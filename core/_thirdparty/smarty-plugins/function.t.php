<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

// Load the {translate} plugin
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'function.translate.php';

/**
 * Smarty t plugin
 *
 * The t plugin is an alias for the {translate} plugin. For more info,
 * read the docs on {@link smarty_function_translate()}
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_t($params, $smarty) {
	return M_smarty_plugin_result(smarty_function_translate($params, $smarty), $params, $smarty);
}