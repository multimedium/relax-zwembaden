<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty crop plugin
 *
 * - Type:     function
 * - Purpose:  Create a link to an image crop
 * 
 * This function can be used in a smarty template, to link to a cropped image to the page.
 * This function checks the last modified date of the crop to add a timestamp
 * to prevent the browser from loading a cached file that is outdated
 * 
 * @example {crop imageId=1 thumb=thumb}
 * 
 * 
 * Params:
 * 	image Media
 * 	thumb string Thumbnail-definition used to display image
 * 
 * @author Willem Cornelissen
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_crop($params, $smarty) {
	// Get the image id and the thumbnail definition
	if(isset($params['thumb']) && M_Helper::isInstanceOf($params['image'] , 'Media')) {
		// If we are here the 2 parameters are given
		$image = $params['image'];
		$definition = $params['thumb'];
		
		// Then we get the crop
		$crop = M_Loader::getDataObjectMapper('Crop', 'crop')
			->getCropByDefinition($image->getId(), $definition);
		
		// Create the url
		/* @var $image Media */
		$url = 'crop/' . $definition . '/' . $image->getId() . '/' . $image->getBasenameFromTitle();
		
		// If a crop is found, add the timestamp
		/* @var $crop Crop */
		if($crop) {
			$url .= '?t=' . $crop->getDateModified()->toString('timestamp');
		}
		
		// Return the link
		return M_smarty_plugin_result(M_Request::getLink($url), $params, $smarty);
	}
	else {
		throw new M_ViewException(sprintf(
			'No image or thumbnail defined; %s expects 2 parameters;',
			'smarty.crop'
		));
	}
}
