<?php
/**
 * Smarty Skype plugin
 *
 * - Type:	function
 * - Name:	skype
 * - Purpose: may be used to display a Skype button on the page
 * 
 * This plugin has the following standard parameters:
 * - account (mandatory): this is the account name we want to link the Skype button to
 * - live (optional): boolean determining whether we want to display the online status (online, busy, away, offline) live on the
 *   button. This is TRUE by default. Note that you may pass any form of boolean supported by
 *   {@link M_Helper::isBooleanTrue()} to this parameter.
 * 
 * For example:
 * <code>
 *	{skype account='MyAccount'}
 *	{skype account='MyAccount' live='false'}
 * </code>
 * 
 * Next to the standard parameters, this plugin supports full button customization. You can either use one of the standard
 * button styles (which may be found at {@link http://www.skype.com/go/skypebuttons}), or generate your own button,
 * through use of the Skype button generator (found at {@link http://www.skype.com/intl/en/tell-a-friend/wizard}).
 * 
 * To use a custom button, the following parameters are required:
 * - button: the name of the button you want to use. For live display, the button name will not link directly to an image,
 *   but will contain a button type (e.g. "bigclassic"). For non-live display, the button name will include a full image basename
 *   including an extension (e.g. "call_blue_white_124x52.png")
 * - width: the width of the button, as integer (e.g. 150)
 * - height: the height of the button, as integer (e.g. 60)
 * 
 * The possible values for these parameters can be found at the links mentioned above, typically in the generated code
 * snippet on the page.
 * 
 * For example:
 * <code>
 *	{skype account='MyAccount' button='bigclassic' width='182' height='44'}
 *	{skype account='MyAccount' button='call_blue_white_124x52.png' width='124' height='52' live='false'}
 * </code>
 * 
 * Additionally, you may define which Skype action you want the button to perform. By default we use the "Call me"
 * functionality. The following actions are supported:
 * - Call me (action name: call)
 * - Add me to skype (action name: add)
 * - Chat with me (action name: chat)
 * - Leave me a voice message (action name: voicemail)
 * - Send me a file (action name: sendfile)
 * 
 * Note that if you choose a custom action, you might have to define a custom button as well, depending on
 * the action you chose, and whether you want to display the live status or not. Typically, most live status buttons don't
 * need a custom button, while the non-live buttons do. The custom button may be generated using the Skype button
 * wizard mentioned above. For example, imagine we want to generate a non-live "Chat with me" button. This may be
 * achieved as follows:
 * 
 * <code>
 *	{skype account='MyAccount' live='false' action='chat' button='chat_green_white_164x52.png' width='164' height='52' alt="{t|att text='Chat with me'}"}
 * </code>
 * 
 * Finally, like displayed in the previous example, you may modify the alt attribute the Skype button image will get,
 * which is "Skype us!" by default. For example:
 * 
 * <code>
 *	{skype account='MyAccount' alt="{t|att text='Contact us through Skype'}"}
 * </code>
 * 
 * NOTE: before your online status can be displayed live, you must enable this feature in Skype. From the Skype
 * support website:
 * 
 *	"By default, your Skype status will NOT be shown on the web. To show your Skype status on the web,
 *	you must explicitly choose to do so. Publishing your Skype online status will allow all internet users to see it
 *	and potentially contact you.
 * 
 *	To show your Skype status on the web:
 *	
 *	1. Sign in to Skype.
 *	2. In the menu bar, click Skype > Privacy...  (on earlier versions, you can find this on Skype > Options >
 *	    Privacy > Show Advanced Options).
 *	3. Check "Allow my online status to be shown on the web."
 * 
 * @author Tim Segers
 * @param array $params
 * @param Smarty_Internal_Template $smarty
 * @return html
 */
function smarty_function_skype($params, Smarty_Internal_Template $smarty) {
	// Fetch the account name we have to use for display
	if(isset($params['account']) && ! empty($params['account'])) {
		$account = $params['account'];
	}
	// If not given, throw an error
	else {
		throw new M_Exception(sprintf(
			'Cannot display smarty Skype plugin; missing required parameter account. For example: ' .
			'{skype account="MyAccount"}'
		));
	}
	
	// Fetch whether we have to display the Skype status live or not. Note that we default the live status to TRUE:
	$live = isset($params['live'])
		? M_Helper::isBooleanTrue($params['live'])
		: true;
	
	// Determine the correct image base link, based on the live status
	$baseHref = $live
		? 'http://mystatus.skype.com'
		: 'http://download.skype.com/share/skypebuttons/buttons';
	
	// If a button style, width and height are all set:
	if(isset($params['button']) && isset($params['width']) && isset($params['height'])) {
		// Use them
		$button = $params['button'];
		$width = (int) $params['width'];
		$height = (int) $params['height'];
	}
	// Otherwise, default the button, based on the live status
	else {
		// For the live status, default to the live balloon type button
		if($live) {
			$button = 'balloon';
			$width = 150;
			$height = 60;
		// For the non-live status, default to the "Call me" balloon type button
		} else {
			$button = 'call_green_white_153x63.png';
			$width = 153;
			$height = 63;
		}
	}

	// Define the alt for the image
	$alt = isset($params['alt'])
		? (string) $params['alt']
		: t('Skype us!');

	// Define the Skype action we have to use
	$action = isset($params['action'])
		? (string) $params['action']
		: 'call';
	
	// Parse the output html, returning it
	return '<div class="skype-button">' .
			'<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>' .
			'<a href="skype:'. $account .'?'. $action .'">' .
				'<img ' .
					'src="'. $baseHref . '/' . $button . ($live ? ('/'. $account) : '') .'" ' . // Only add the account name if we are displaying the status live
					'style="border: none;" ' .
					'width="'. $width .'" ' .
					'height="'. $height .'" ' .
					'alt="'. $alt .'" ' .
				'/>' .
			'</a>' .
		'</div>';
}