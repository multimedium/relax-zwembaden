<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty dataobjects plugin
 *
 * - Type:     function
 * - Purpose:  Get data object(s)
 * 
 * This function can be used in a smarty template, to fetch data objects.
 * This plugin will use {@link M_DataObjectMapper} to do so...
 * 
 * Examples:
 * <code>
 *    {dataobjects module="text" class="Text" id=1}
 *    {dataobjects module="text" class="Text" realm="/contact"}
 *    {dataobjects module="text" class="Text" sortby="realm" order="ASC"}
 *    {dataobjects module="text" class="Text" sortby="realm" order="ASC" limit="0,1"}
 * </code>
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_dataobjects($params, $smarty) {
	// Check if the data object module has been defined:
	if(isset($params['module']) && is_string($params['module']) && ! empty($params['module'])) {
		// If so, set the variable:
		$module = $params['module'];
		
		// Remove the variable from the parameters array
		unset($params['module']);
	}
	// If not defined:
	else {
		// Set the module to NULL
		$module = NULL;
	}
	
	// Check if the data object class has been defined:
	$c = (isset($params['class']) && is_string($params['class']) && ! empty($params['class']));
	if($c) {
		// Set the class name:
		$class = $params['class'];
		
		// We remove the variable from the parameters array
		unset($params['class']);
		
		// Construct the data object mapper:
		$mapper = M_Loader::getDataObjectMapper($class, $module);
		
		// Check if sorting:
		if(isset($params['sortby']) && is_string($params['sortby']) && ! empty($params['sortby'])) {
			// If so, add the sorting filter to the mapper:
			$mapper->addFilter(
				new M_DbQueryFilterOrder(
					$params['sortby'],
					(isset($params['order']) && in_array(strtoupper($params['order']), array('ASC', 'DESC')))
						? $params['order']
						: 'ASC'
				)
			);
			
			// Remove the variable from the parameters array
			unset($params['sortby']);
			unset($params['order']);
		}
		
		// Check if limit has been applied:
		if(isset($params['limit'])) {
			// Get offset and count:
			$limit = explode(',', $params['limit']);
			
			// Remove the variable from the parameters array
			unset($params['limit']);
			
			// Make sure that both the offset and count has been provided.
			// If only count is provided, we default the offset to 0 (ZERO)
			if(count($limit) < 2) {
				$limit = array(0, (int) $limit[0]);
			}
			
			// Apply the filter:
			$mapper->addFilter(new M_DbQueryFilterLimit($limit[0], $limit[1]));
		}
		
		// Now, we prepare an array with filter values. All the remaining parameters 
		// to the smarty plugin will be used to apply filters:
		foreach($params as $name => $value) {
			// Some (reserved) parameters are excluded though:
			if(! in_array($name, array('assignto'))) {
				// Add the filter:
				$mapper->addFilter(new M_DbQueryFilterWhere($name, $value));
			}
		}
		
		// Ask the mapper to fetch the object, and return the result
		return M_smarty_plugin_result($mapper->getAll(), $params, $smarty);
	}
	// If the data object class name has not been defined
	else {
		// Throw an exception
		throw new M_Exception(sprintf(
			'Cannot fetch data object; %s expects a data object class name: {dataobject class="string" [module="string" ...]}',
			'smarty.dataobject'
		));
	}
}