<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty messenger plugin
 *
 * - Type:     function
 * - Name:     locale
 * - Purpose:  Get all messages from {@link M_Messenger}
 * 
 * This function can be used in a smarty template, to get all the messages from
 * M_Messenger
 * 
 * @example {messenger assignto=$messages}
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_messenger($params, $smarty) {
	// Make sure a locale category has been provided
	$flag = M_Helper::getArrayElement('flag', $params);
	
	$messages = M_Messenger::getInstance()->getMessages();
	// Return the requested locale category:
	return M_smarty_plugin_result($messages, $params, $smarty);
}