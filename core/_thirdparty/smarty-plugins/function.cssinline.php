<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty "inline css" plugin
 *
 * - Type:     function
 * - Purpose:  Add inline css to page
 * 
 * This function can be used in a smarty template, to add css code to the
 * page. This plugin will use {@link M_ViewCss} to do so...
 *
 * @example
 *
 * {capture name="inlineCss"}
 *		#myDiv {
 *			border: 1px red solid;
 *		}
 * {/capture}
 *
 * {cssinline css=$smarty.capture.inlineCss}
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_cssinline($params, $smarty) {
	// Get the Javascript singleton
	$css = M_ViewCss::getInstance();
	$css->addInline(
		isset($params['css']) ? $params['css'] : '',
		isset($params['context']) ? $params['context'] : NULL
	);
}