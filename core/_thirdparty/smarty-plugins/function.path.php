<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';


/**
 * Smarty path plugin
 *
 * - Type:     function
 * - Purpose:  Format a string to an url-safe string
 * 
 * This function can be used in a smarty template, to parse a path element 
 * out of string
 * 
 * Usage Example 1
 * <code>
 *    {path text="Product name"}
 * </code>
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_path($params, $smarty) {
	// Has type string been provided?
	if(isset($params['text'])) {
		$text = M_Uri::getPathElementFromString($params['text']);
	}else {
		throw new M_Exception('Missing "text" parameter when calling url smarty-plugin');
	}

	return M_smarty_plugin_result($text, $params, $smarty);
}