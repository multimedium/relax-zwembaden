<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty Facebook plugin
 *
 * - Type:     function
 * - Name:     facebook
 * - Purpose:  include facebook plugins in your website
 * 
 * This function can be used in a smarty template, to include facebook plugins
 *
 * Available options:
 *
 * # Like
 *		- url (the url to like, default to current url)
 *		- width (width of iframe in pixels)
 *		- layout (standard/button_count)
 *		- colorscheme (light/dark)
 *		- locale (default to active-locale and Belgium)
 *    - appId (make one on the developer website, not required)
 * 
 * @link http://developers.facebook.com/plugins
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_facebook($params, $smarty) {
    $plugin = M_Helper::getArrayElement('plugin', $params);
    if ($plugin === null) {
        throw new M_Exception('No plugin-type specified');
    }
	
	$url = M_Helper::getArrayElement('url', $params, null);
	if (is_null($url)) {
		$url = M_ControllerDispatcher::getInstance()->getUri();
	}

	$width = M_Helper::getArrayElement('width', $params, 450);
	$height = M_Helper::getArrayElement('height', $params, 24);
	$colorScheme = M_Helper::getArrayElement('colorscheme', $params, 'light');
	$showFaces = M_Helper::getArrayElement('showFaces', $params, 'false');
	$localeCode = M_Helper::getArrayElement('locale', $params, M_Locale::getLocaleCode('NL'));
	$appId = M_Helper::getArrayElement('appId', $params, false);
	if ($appId) {
		$appId = '&amp;appId='.$appId;
	}
	switch ($plugin) {
		case 'like':
			//like params
			$layout = M_Helper::getArrayElement('layout', $params, 'standard'); // standard | button_count
			
			$protocol = M_Request::isHttps() ? 'https://' : 'http://';
			$result = '<iframe src="' .$protocol . 'www.facebook.com/plugins/like.php?';
			$result .=	'href='.urlencode($url);
			$result .=		'&amp;layout=' . $layout;
			$result .=		'&amp;show_faces=' . $showFaces;
			$result .=		'&amp;width='.(int)$width;
			$result .=		'&amp;action=like';
			$result .=		'&amp;height='.(int)$height;
			$result .=     $appId;
			$result .=		'&amp;locale='.$localeCode.'" ';
			$result .=		'scrolling="no" ';
			$result .=		'frameborder="0" ';
			$result .=	'style="border:none; overflow:hidden; width:'.(int)$width.'px; height:'.(int)$height.'px;" ';
			$result .=	'allowTransparency="true">';
			$result .= '</iframe>';
			break;
		case 'likebox':
			
			//likebox params
			$borderColor = M_Helper::getArrayElement('borderColor', $params, '#000000');
			$stream = M_Helper::getArrayElement('stream', $params, 'true');
			$header = M_Helper::getArrayElement('header', $params, 'true');
			
			$result = '<iframe src="//www.facebook.com/plugins/likebox.php?';
			$result .=	'href='.urlencode($url);
			$result .=		'&amp;width='.(int)$width;
			$result .=		'&amp;height='.(int)$height;
			$result .=		'&amp;show_faces=' . $showFaces;
			$result .=		'&amp;colorscheme=' . $colorScheme;
			$result .=		'&amp;stream='.$stream;
			$result .=		'&amp;border_color='.urlencode($borderColor);
			$result .=		'&amp;header='.$header;
			$result .=     $appId;
			$result .=		'" scrolling="no" ';
			$result .=		'frameborder="0" ';
			$result .=	'style="border:none; overflow:hidden; width:'.(int)$width.'px; height:'.(int)$height.'px;" ';
			$result .=	'allowTransparency="true">';
			$result .= '</iframe>';
			break;
		default:
			throw new M_Exception(sprintf('Plugin %s not yet implemented', $plugin));
			break;
	}
	
	
	// Return result:
	return M_smarty_plugin_result($result, $params, $smarty);
}