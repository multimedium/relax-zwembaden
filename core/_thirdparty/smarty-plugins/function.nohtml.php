<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty nohtml plugin
 * 
 * Will filter all html tags from a given text, returning it.
 * 
 * Example:
 * {nohtml value="<strong>Test<strong>"}
 * 
 * @author Tom, Tim
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_nohtml($params, $smarty) {
	$filter = new M_FilterTextHtml(new M_FilterTextValue(array_key_exists('value', $params) && $params['value'] ? $params['value'] : ''));
	return M_smarty_plugin_result($filter->apply(), $params, $smarty);
}