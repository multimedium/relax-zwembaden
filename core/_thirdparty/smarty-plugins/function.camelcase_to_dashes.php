<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty plural plugin
 *
 * - Type:     function
 * - Name:     camelcase_to_dashes
 * - Purpose:  Will convert a camelcased string (like a classname) to a string
 *			   seperated by dashes
 * 
 * For example:
 * 
 * <code>
 *    {camelcase_to_dashes string="MyString"}
 * </code>
 * 
 * Will output:
 * 
 * <code>
 *	my-string
 * </code>
 * 
 * @author Tim Segers
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_camelcase_to_dashes($params, $smarty) {
	// Fetch the string
	$string = M_Helper::getArrayElement('string', $params);
	
	// If not given, we cannot proceed
	if(! $string) {
		throw new M_Exception(sprintf(
			'Cannot create path element; string must be given! ' .
			'{camelcase_to_dashes string="MyString"}'
		));
	}
	
	// Return the result:
	return M_smarty_plugin_result(
		// With the string, concatinated by dashes
		strtolower(implode('-', M_Helper::explodeByCamelCasing($string))),
		// The parameters
		$params,
		// And the Smarty object
		$smarty
	);
}