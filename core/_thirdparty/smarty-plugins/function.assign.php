<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {assign} function plugin
 *
 * Type:     function<br>
 * Name:     assign<br>
 * Purpose:  Assigns a new variable to the template
 * 
 * @author Tom Bauwens
 * @param array $params
 * @param Smarty
 * @return string|null
 */
function smarty_function_counter($params, &$smarty) {
	if(isset($params['var']) && is_string($params['var']) && isset($params['value'])) {
		$smarty->assign($params['var'], $params['value']);
	}
}