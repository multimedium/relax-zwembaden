<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty clickable plugin
 *
 * - Type:     function
 * - Purpose:  Create URLs and Emails clickable
 *
 * This function can be used in a smarty template, to make URLs and Emails
 * clickable
 *
 * Usage Example 1
 * <code>
 *    {clickable text="This is an url: http://www.multimedium.be" url="true" email="true"}
 * </code>
 * Will produce clickable URLs and e-mail addresses
 *
 * Note that arguments for placeholders can be passed to the smarty
 * function.
 *
 * @author Wim Van De Mierop
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_clickable($params, $smarty) {

	// Has text parameter been provided?
	if(isset($params['text']) && !empty($params['text'])) {
		// Get etc
		$textString = $params['text'];
	}
	else
	{
		throw new M_Exception(
			'No text parameter provided in clickable'
		);
	}

	// Has url parameter been provided?
	if(isset($params['url'])) {
		// Get etc
		$url = $params['url'];
	}
	else
	{
		throw new M_Exception(
			'No url parameter provided in clickable'
		);
	}

	// Has url parameter been provided?
	if(isset($params['email'])) {
		// Get etc
		$email = $params['email'];
	}
	else
	{
		throw new M_Exception(
			'No email parameter provided in clickable'
		);
	}

	$text = new M_Text($textString);

	// If $url is TRUE, make URLs clickable
	if (M_Helper::isBooleanTrue($url)) {
		$text->makeUrlClickable();
	}

	// If $email is TRUE, make Emails clickable
	if (M_Helper::isBooleanTrue($email)) {
		$text->makeEmailClickable();
	}

	return M_smarty_plugin_result($text->getText(), $params, $smarty);
}