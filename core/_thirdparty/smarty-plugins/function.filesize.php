<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty filesize plugin
 *
 * - Type:     function
 * - Purpose:  Get readable file size
 * 
 * This function can be used in a smarty template, to get a readable string for 
 * a given filesize (expressed in a number of bits)
 * 
 * Examples
 * <code>
 *    {filesize n=$file->getSize()}
 * </code>
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_filesize($params, $smarty) {
	// Has number been provided?
	if(isset($params['size']) && is_numeric($params['size'])) {
		
		// Format the number, and return the result
		return M_smarty_plugin_result(M_FsHelper::getFileSizeString($params['size']), $params, $smarty);
		
	}
	// Throw exception, if not provided
	else {
		throw new M_ViewException(sprintf(
			'Cannot format filesize "%s"; %s expects a numeric value size; {filesize size=[numeric-value]}',
			isset($params['size']) ? $params['size'] : 'undefined',
			'smarty.filesize'
		));
	}
	
	// Return empty string, if still here
	return '';
}