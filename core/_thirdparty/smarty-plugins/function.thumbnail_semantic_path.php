<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty thumbnail semantic path
 *
 * - Type:     function
 * - Purpose:  get a semantic path to a media item
 * 
 * Note that this requires the 'use semantic filenames' flag to be enabled in
 * the thumbnail definition.
 * 
 * Params:
 * 	image Media
 * 	thumb string Thumbnail-definition used to display image
 * 	name string  The name you want to use for the image, optional
 * 
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_thumbnail_semantic_path($params, $smarty) {
	// Check if an image has been provided:
	if(isset($params['image']) && M_Helper::isInstanceOf($params['image'] , 'Media')) {
		/* @var $image Media */
		$image = $params['image'];
		//use the parameters to determine the definitions we want to use, or
		//use default values if they are not provided
		$thumbDef = M_Helper::getArrayElement('thumb', $params, 'thumb');
		
		// we determine the name of the image
		// 1. check if provided as argument
		// 2. try to use the media item's title
		// 3. use the actual filename of the image
		$name = M_Helper::getArrayElement(
			'name', 
			$params, 
			M_Uri::getPathElementFromString(
				$image->getTitle() ? : $image->getFile()->getFilename()
			)
		);
		
		// convert the name to a valid path element
		$name = M_Uri::getPathElementFromString($name);
		
		// we compose the path
		$path = M_Request::getLink('thumbnail');
		// add the definition name
		$path .= DIRECTORY_SEPARATOR . $thumbDef;
		// add the basename without extension
		$basenameParts = explode('.', $image->getBasename());
		array_pop($basenameParts);
		$path .= DIRECTORY_SEPARATOR . implode('.', $basenameParts);
		// add the semantic name, and the extension
		$path .= DIRECTORY_SEPARATOR . $name . '.' . $image->getFileExtension(); 
		
	}
	//cannot add image
	else $path = '';
	
	return M_smarty_plugin_result($path, $params, $smarty);
}

