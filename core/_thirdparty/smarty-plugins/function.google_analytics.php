<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty Google Analytics plugin
 *
 * - Type:     function
 * - Name:     google_analytics
 * - Purpose:  Track pageviews, events, ... in Google Analytics
 * 
 * This function can be used in a smarty template, to create the javascript
 * code which is used by Google Analytics to track the traffic on your website
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters:
 *		- trackPageView boolean default TRUE
 *		- ua string
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_google_analytics($params, $smarty) {
	// Construct the Google Analytics Singleton Instance
	$service = M_ServiceGoogleAnalytics::getInstance();

	// Track the page view? (enabled by default)
	$trackPageView = M_Helper::getArrayElement('trackPageView', $params, true);
	if($trackPageView) $service->addTrackingOfPageView();

	// Has the UA been provided to the smarty plugin?
	if(isset($params['ua']) && is_string($params['ua'])) {
		// Then, we provide the singleton with the account id that is contained
		// in the ua variable:
		$service->setAccount($params['ua']);
	}
	
	// Has a domain name been provided?
	if(isset($params['domainName']) && is_string($params['domainName'])) {
		// Then, we provide the singleton with the account id that is contained
		// in the ua variable:
		$service->setDomainName($params['domainName']);
	}
	
	// If we have to support direct advertising
	if(isset($params['directAdvertising']) && M_Helper::isBooleanTrue($params['directAdvertising'])) {
		// Specify so
		$service->setSupportDisplayAdvertising(TRUE);
	}
	
	// Return result:
	return M_smarty_plugin_result($service->getJavascriptCode(), $params, $smarty);
}