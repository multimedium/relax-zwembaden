<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty varinfo plugin
 *
 * - Type:     function
 * - Name:     varinfo
 * - Purpose:  Get variable info
 * 
 * This function can be used in a smarty template, to get information about the
 * available presentation variables. The plugin will layout a clear structure,
 * in which the properties of the variables are illustrated.
 * 
 * Usage Example 1
 * <code>
 *    {varinfo}
 * </code>
 * 
 * Note that you may specify a variable name, to get more information about
 * a specific variable
 * 
 * Usage Example 2
 * <code>
 *    {varinfo name="test"}
 * </code>
 * 
 * Example 2 would provide information about the smarty variable $test
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_varinfo($params, Smarty $smarty) {
	// We compose the range of variables, for which we will dump information.
	// By default, we dump the information about all variables, but we check if
	// a specific variable has been selected:
	if(isset($params['name']) && ! empty($params['name'])) {
		// If a variable has been selected, we do an additional check to see
		// whether or not the variable exists!
		if(! isset($smarty->_tpl_vars[$params['name']])) {
			// If it doesn't exist, we return "undefined" as result
			return M_smarty_plugin_result('<pre>' . $params['name'] . ' is undefined</pre>', $params, $smarty);
		}
		
		// If the variable does exist, we define the range of variables to 
		// export
		$vars = array($params['name'] => $smarty->_tpl_vars[$params['name']]);
	}
	// If no specific variables have been selected:
	else {
		// Set the range of variables to export:
		$vars = $smarty->_tpl_vars;
	}
	
	// For each of the variables to export:
	foreach($vars as $name => $value) {
		
	}
}