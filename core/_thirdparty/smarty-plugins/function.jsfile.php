<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty css plugin
 *
 * - Type:     function
 * - Purpose:  Add js file to page
 * 
 * This function can be used in a smarty template, to add a js file to the page.
 * This plugin will use {@link M_ViewJavascript} to do so...
 * 
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_jsfile($params, $smarty) {
	// Get the Javascript singleton
	$js = M_ViewJavascript::getInstance();
	
	// Check if a file has been provided:
	if(isset($params['file']) && ! empty($params['file'])) {
		
		// Get the file
		$file = new M_File($params['file']);
		
		// Add the file:
		$js->addFile(
			M_Debug::getDebugMode() ? M_ViewJavascript::getOriginalFileByMinifiedFile($file) : $file,
			// Context
			isset($params['context']) && ! empty($params['context']) ? $params['context'] : NULL,
			// Conditions
			isset($params['conditions']) && ! empty($params['conditions']) ? $params['conditions'] : NULL
		);
	}
	// If the path to the file has not been provided:
	else {
		// Throw an exception
		throw new M_Exception(sprintf(
			'Cannot add JS file; %s expects a path to the file; {jsfile file=[path] [context=... condition=...]}',
			__FUNCTION__
		));
	}
}