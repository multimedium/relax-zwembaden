<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty Twitter plugin
 *
 * - Type:     function
 * - Name:     twitter
 * - Purpose:  include twitter plugins in your website
 * 
 * This function can be used in a smarty template, to include twitter plugins
 * 
 * @example {twitter} {twitter plugin="tweet"}
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_twitter($params, $smarty) {
	// Get requested plugin
    $plugin = M_Helper::getArrayElement('plugin', $params);

	// If not provided:
    if($plugin === null) {
		// Then, default to "tweet"
        $plugin = 'tweet';
    }

	// use text?
	$text = M_Helper::getArrayElement('text', $params);
	if($text) {
		$data['data-text'] = $text;
	}

	// user url?
	$url = M_Helper::getArrayElement('url', $params);
	if ($url) {
		// use url?
		$data['data-url'] = $url;
	}

	// get data-count (number of tweets)
	$dataCount = M_Helper::getArrayElement('dataCount', $params, 'horizontal');
	$data['data-count'] = $dataCount;

	$attributes = '';
	foreach($data AS $key => $value) {
		$attributes .= ' ' . $key .= ' = "'.$value.'"';
	}
	
	// Render the plugin:
	switch ($plugin) {
		case 'tweet':
			$protocol = M_Request::isHttps() ? 'https://' : 'http://';
			$result  = '<a href="' . $protocol . 'twitter.com/share" class="twitter-share-button"'.$attributes.'>';
			$result .=    t('Tweet');
			$result .= '</a>';
			$result .= '<script type="text/javascript" src="' . $protocol . 'platform.twitter.com/widgets.js"></script>';
			break;


		default:
			throw new M_Exception(sprintf('Plugin %s not yet implemented', $plugin));
			break;
	}
	
	// Return result:
	return M_smarty_plugin_result($result, $params, $smarty);
}