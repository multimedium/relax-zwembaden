<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';


/**
 * Smarty link plugin
 *
 * - Type:     function
 * - Purpose:  Get the link (for a type of resource, for a module)
 * 
 * This function can be used in a smarty template, to get the link from the active
 * page or to a resource type
 * 
 * Usage Example 1
 * <code>
 *    {link href="products/product-name"}
 * </code>
 * 
 * Usage Example 2
 * <code>
 *    {link type="images" href="nav.jpg"}
 * </code>
 * 
 * Usage Example 3
 * <code>
 *    {link type="css" href="style.css" module="products"}
 * </code>
 * 
 * 
 * 
 * Note that arguments for placeholders can be passed to the smarty 
 * function. Consider the following example:
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_link($params, $smarty) {
	$type = false;
	$href = false;
	$module = false;
	$prefix = false;

	// Has type string been provided?
	if(isset($params['type']) && !empty($params['type'])) {
		// Get type
		$type = strtolower($params['type']);
	}
	
	// Has module string been provided?
	if(isset($params['module']) && !empty($params['module'])) {
		// Get module
		$module = $params['module'];
	}
	
	// Has href string been provided?
	if(isset($params['href']) && !empty($params['href'])) {
		// Get href
		$href = $params['href'];
	}
	
	// Has prefix string been provided?
	if(isset($params['prefix']) && !empty($params['prefix'])) {
		// Get href
		$prefix = strtolower($params['prefix']);
	}
	
	// Return the correct link-type:
	$appendHref = true;
	switch($type) {
		case 'images':
			$link = M_ViewHtml::getImagesHref($module);
			break;
		
		case 'javascript':
			$link = M_ViewHtml::getJavascriptHref($module);
			
			// Check if we need to provide the original file
			if(M_Debug::getDebugMode()) {
				$javascriptPath = M_Loader::getResourcesPath($module) . '/javascript/';
				$file = new M_File($javascriptPath . M_Helper::ltrimCharlist((string) $href, '/'));
				$href = str_replace($javascriptPath, '', M_ViewJavascript::getOriginalFileByMinifiedFile($file)->getPath());
			}
			break;
		
		case 'flash':
			$link = M_ViewHtml::getFlashHref($module);
			break;
		
		case 'css':
			$link = M_ViewHtml::getCssHref($module);
			break;
		
		case 'xml':
			$link = M_ViewHtml::getXmlHref($module);
			break;
		
		default:
			$appendHref = false;
			
			if($href) {
				$arg = M_Helper::ltrimCharlist((string) $href, '/');
				$add = substr($href, -1) == '/' ? '/' : '';
			} else {
				$arg = NULL;
				$add = '';
			}
			
			$link  = ($prefix == "false") ? M_Request::getLinkWithoutPrefix($arg) : M_Request::getLink($arg);
			$link .= $add;
			
			$isSchemeProvided = (isset($params['scheme']) && !empty($params['scheme']));
			$isHostProvided   = (isset($params['host']) && !empty($params['host']));
			if($isSchemeProvided || $isHostProvided) {
				$uri = new M_Uri($link);
				if($isSchemeProvided) $uri->setScheme($params['scheme']);
				if($isHostProvided) $uri->setHost($params['host']);
				$link = $uri->toString();
			}
			break;
	}
	
	if($href && $appendHref) {
		$link .= '/' . M_Helper::ltrimCharlist((string) $href, '/');
	}
	
	// If a href was provided, append it to the link variable
	return M_smarty_plugin_result($link, $params, $smarty);
}
?>