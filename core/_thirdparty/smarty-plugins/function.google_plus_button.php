<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty Google Plus Button Plugin
 *
 * - Type:     function
 * - Name:     google_plus_one
 * - Purpose:  Displaying the Google +1 button on the page
 * 
 * @author Tim Segers
 * @param array parameters
 * 		The function's parameters:
 *		- count: set to false to disable the current count being displayed
 *		- url: the url to share
 *		- size: small, medium, tall (leave empty for default)
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_google_plus_button($params, $smarty) {
	// Parse the JavaScript
	$out = '';
	$out .= '<script type="text/javascript" src="https://apis.google.com/js/plusone.js">';
	$out .=		"{literal}{lang: 'nl'}{/literal}";
	$out .= '</script>';
	
	// Add the html
	$out .= '<g:plusone';
	
	// If we are to hide the count
	if(M_Helper::getArrayElement('count', $params, null) === false) {
		$out .= ' count="false"';
	}
	// If we want to specify a specific href
	$href = M_Helper::getArrayElement('url', $params);
	if($href) {
		$out .= ' href="'.$href.'"';
	}

	//Size?
	$size = M_Helper::getArrayElement('size', $params);
	if($size) {
		$out .= ' size="'.$size.'"';
	}
	
	$out .= '></g:plusone>';

	// Return result:
	return M_smarty_plugin_result($out, $params, $smarty);
}