<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty media plugin
 *
 * The media plugin parses texts and replaces images which have been dropped
 * into a wysiwyg editor with the correct src attribute. 
 * 
 * By passing the correct variables, we can use the thumbnail module to create
 * the correct thumbnails
 * 
 * Params:
 * 
 * thumbnail/thumb: thumbnail definition or "square"|"resize"
 * href:thumbnail definition used in href
 * width (optional): width of thumbnail
 * height (optional: height of thumbnail
 * 
 * @author Ben Brughmans
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_media($params, $smarty) {
	
	// Check params
	if (!array_key_exists('text', $params)) {
		throw new M_Exception('Missing text param');
	}
	
	// Set default values
	$text = (string) $params['text'];
	
	//check both for "thumb" an "thumbnail" key, these both will work
	$thumbnail = M_Helper::getArrayElement('thumb', $params);
	if (is_null($thumbnail)) $thumbnail = M_Helper::getArrayElement('thumbnail', $params, 'thumb');
	
	//check for other values
	$width = M_Helper::getArrayElement('width', $params);
	$height = M_Helper::getArrayElement('height', $params);
	$href = M_Helper::getArrayElement('href', $params, 'full');
	
	M_Loader::loadView('MediaViewHelper', 'media');
	//no definition is used
	if ($thumbnail == 'resize' || $thumbnail == 'square') {
		$text = MediaViewHelper::prepareTextForMedia(
			$params['text'], $thumbnail, $width,$height
		);
	}
	//definition is used
	else {
		$text = MediaViewHelper::prepareTextForMediaDefinition(
			$params['text'], $thumbnail, $href
		);
	}
	return M_smarty_plugin_result($text , $params, $smarty);
}