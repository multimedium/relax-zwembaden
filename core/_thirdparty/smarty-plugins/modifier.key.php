<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty key modifier plugin
 *
 * Type:     modifier<br>
 * Name:     key<br>
 * Purpose:  Get the value of a key in array, if existing
 * @author Tom Bauwens
 * @param array
 * @param string
 * @return string
 */
function smarty_modifier_key($array, $key) {
    // If the array is not an array:
    if(! is_array($array)) {
    	// return empty string:
    	return '';
    }
    
    // If the key does not exist:
    if(! array_key_exists($key, $array)) {
    	// return empty string
    	return '';
    }
    
    // Return the value
    return $array[$key];
}