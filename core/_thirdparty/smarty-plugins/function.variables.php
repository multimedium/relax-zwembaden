<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty variables plugin
 *
 * - Type:     function
 * - Name:     counter
 * - Purpose:  Dump assigned presentation variables
 * 
 * This function can be used in a smarty template, to output the 
 * presentation variables that have been assigned to it. It will create
 * with a clear overview of all variables' values.
 * 
 * Usage Example 1
 * <code>
 *    {variables}
 * </code>
 * 
 * Note that you can select a variable in specific, in order to output
 * it's value, by calling the method as following:
 * 
 * Usage Example 2
 * <code>
 *    {variables name="myVariable"}
 * </code>
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_variables($params, $smarty) {
	// Create boolean flag: Has variable name been provided?
	$c = (isset($params['name']) && !empty($params['name']));
	
	// If TRUE (selected variable name)
	if($c) {
		// If the requested variable does not exist, we output the
		// "undefined" data type
		$vars = _smarty_function_variable_get($params['name'], $smarty);
		if($vars == NULL) {
			return sprintf('<pre>%s = UNDEFINED</pre>', $params['name']);
		}
		
		// If the variable does exist, we set the variable to be formatted
		$name = $params['name'];
	}
	// If FALSE (all variables)
	else {
		// Variable to be formatted:
		$vars = _smarty_function_variable_get('variables', $smarty);
		$name = 'variables';
	}
	
	// Format the variable:
	return sprintf('<pre>%s</pre>', _smarty_function_variable($name, $vars));
}

function _smarty_function_variable_get($name, $smarty) {
	$vars = $smarty->_tpl_vars;
	foreach(explode('.', $name) as $key) {
		if(strtolower($key) != 'variables') {
			if(isset($vars[$key])) {
				$vars = $vars[$key];
			} else {
				return NULL;
			}
		}
	}
	
	return $vars;
}

function _smarty_function_variable($name, $variable) {
	$out = '';
	if(is_array($variable)) {
		foreach($variable as $_name => $_value) {
			$out .= _smarty_function_variable($name . '.' . $_name, $_value);
		}
	} else {
		$out .= $name . ' = ' . str_replace('<', '&lt;', str_replace('>', '&gt;', var_export($variable, TRUE))) . "\n";
	}
	return $out;
}
?>