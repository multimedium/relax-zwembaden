<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_plugin.shared.php';

/**
 * Smarty number plugin
 *
 * - Type:     function
 * - Name:     number
 * - Purpose:  Format a given number
 * 
 * This function can be used in a smarty template, in order to format 
 * a given number in the currently selected locale.
 * 
 * Usage Example 1
 * <code>
 *    {number n="12.35"}
 * </code>
 * 
 * Note that the type of the number can be provided as an extra arg. For
 * example:
 * 
 * Usage Example 2
 * <code>
 *    {number n="12.35" format="percent"}
 * </code>
 * 
 * Usage Example 3
 * <code>
 *    {number n="12.35" format="currency" currency="EUR"}
 * </code>
 * 
 * @author Tom Bauwens
 * @param array parameters
 * 		The function's parameters
 * @param Smarty
 * 		The Smarty template where the function is being called
 * @return string|null
 */
function smarty_function_number($params, $smarty) {
	// Has number been provided?
	if(isset($params['n']) && is_numeric($params['n'])) {
		// Get number:
		$n = new M_Number($params['n']);
		
		// Format the number:
		if(! isset($params['format'])) {
			$params['format'] = 'none';
		}
		switch(strtolower($params['format'])) {
			// CURRENCY
			case 'currency':
				// We check if a currency has been provided (defaulted
				// to EUR)
				$currency = isset($params['currency']) ? (string) $params['currency'] : 'EUR';
				$precision = isset($params['precision']) ? (string) $params['precision'] : NULL;
				return M_smarty_plugin_result($n->toCurrencyString($currency, null, $precision), $params, $smarty);
			
			// PERCENT
			case 'percent':
				return M_smarty_plugin_result($n->toPercentString(), $params, $smarty);
			
			// DEFAULT:
			default:
				return M_smarty_plugin_result($n->toString(), $params, $smarty);
		}
	}
	// Throw exception, if not provided
	else {
		throw new M_ViewException(sprintf(
			'Cannot format number "%s"; %s expects a numeric value n; {number n=[numeric-value]}',
			isset($params['n']) ? $params['n'] : 'undefined',
			'smarty.number'
		));
	}
	
	// Return empty string, if still here
	return '';
}
?>