<?php
/**
 * M_Cron class
 * 
 * M_Cron's job is to assign a schedule to a given tasks. The schedule, 
 * similarly to Linux's crontab, is contained in a string. That string
 * defines when the task should be run. For more info, read the docs
 * on {@link M_Cron::getField()}.
 * 
 * M_Cron provides with tools to extract information out of a cronjob 
 * definition, calculate the next run of the scheduled time of a task, 
 * etc...
 * 
 * @package Core
 */
class M_Cron extends M_Object {
	/**
	 * Cronjob field
	 * 
	 * This constant is used to address a field in the cronjob's
	 * definition string. Read {@link M_Cron::getField()} for more
	 * information
	 */
	const MONTH       = 'month';
	
	/**
	 * Cronjob field
	 * 
	 * This constant is used to address a field in the cronjob's
	 * definition string. Read {@link M_Cron::getField()} for more
	 * information
	 */
	const DAY         = 'day';
	
	/**
	 * Cronjob field
	 * 
	 * This constant is used to address a field in the cronjob's
	 * definition string. Read {@link M_Cron::getField()} for more
	 * information
	 */
	const WEEKDAY     = 'weekday';
	
	/**
	 * Cronjob field
	 * 
	 * This constant is used to address a field in the cronjob's
	 * definition string. Read {@link M_Cron::getField()} for more
	 * information
	 */
	const HOUR        = 'hour';
	
	/**
	 * Cronjob field
	 * 
	 * This constant is used to address a field in the cronjob's
	 * definition string. Read {@link M_Cron::getField()} for more
	 * information
	 */
	const MINUTE      = 'minute';
	
	/**
	 * Cronjob
	 * 
	 * This property stores the cronjob's definition string. For an
	 * introduction on cron definition strings, read the documentation
	 * on {@link M_Cron::getField()}. To set the value of this 
	 * property, {@link M_Cron::setSchedule()} is used.
	 * 
	 * @access private
	 * @var array
	 */
	private $_schedule;
	
	/**
	 * Cronjob
	 * 
	 * This property stores the cronjob's task. To set the value of 
	 * this property, {@link M_Cron::setTask()} is used.
	 * 
	 * @access private
	 * @var callback
	 */
	private $_task;
	
	/**
	 * Constructor
	 * 
	 * If you are not familiar with what M_Cron does, or what a cronjob
	 * is, you should first read the docs 
	 * 
	 * @access public
	 * @see M_Cron::getField() 
	 * @see M_Cron::setSchedule() 
	 * @see M_Cron::setTask() 
	 * @param string $schedule
	 * 		The cronjob schedule definition. For more information on
	 * 		how to format the schedule, read {@link M_Cron::getField()}
	 * @param callback $task
	 * 		The cronjob task. For more information on how to format
	 * 		the cronjob task, read {@link M_Cron::setTask()}
	 * @return M_Cron
	 */
	public function __construct($schedule, $task = NULL) {
		$this->setSchedule($schedule);
		if($task) {
			$this->setTask($task);
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get cronjob field
	 * 
	 * This method will accept a cronjob definition (string), and 
	 * return the fields in that definition.
	 * 
	 * A cronjob is a line that contains six fields, separated by 
	 * spaces. The values in the first five fields determine when the 
	 * task will be run and the sixth field defines the task itself. 
	 * The first five fields correspond to minutes, hours, days, 
	 * months, and weekdays.
	 * 
	 * Table 1. Crontab time period values
	 * <code>
	 *    Time Period          | Values
	 *    ---------------------|----------------
	 *    Minutes              | 0 - 59
	 *    Hour                 | 0 - 23 (0 = Midnight)
	 *    Day                  | 1 - 31
	 *    Month                | 1 - 12
	 *    Weekday              | 0 - 6 (0 = Sunday)
	 * </code>
	 * 
	 * You can also put an asterisk in a field to indicate that every 
	 * possible value for that field (such as every minute, every day 
	 * of the week, etc.) will be used. For example,
	 * 
	 * <code>30 1 * * * task</code>
	 * 
	 * would run 'task' every day at 1:30 in the morning. There are a 
	 * number of permutations of what can go in the fields to fine 
	 * tune when a cronjob is run.
	 * 
	 * If you want to use more than one instance of a value, separate 
	 * the values by commas. For example, if you wanted a job to run 
	 * at 15 and 45 minutes past the hour of every day, you'd use 15,45 
	 * in the first field, like so: 
	 * 
	 * <code>15,45 * * * * task</code>
	 * 
	 * Similarly, 
	 * 
	 * <code>30 22 1,11,21 * * task</code>
	 * 
	 * would run 'task' at 10:30 pm on the 1st, 11th and 21st of every 
	 * month.  You can use a range of values to span a number of values 
	 * without typing each of them in. For example, 
	 * 
	 * <code>15 5 1-15 * * 'task'</code>
	 * 
	 * would run a job at 5:15 am on the first 15 days of the month. 
	 * You can combine multiple instances and ranges together. For 
	 * example, if you wanted to run a job every minute for the first 
	 * quarter of the hour, and then every minute during the third 
	 * quarter of the hour, you would use 1-15, 31-45 in the Minutes 
	 * field, like so:
	 * 
	 * <code>1-15,31-45 * * * * task</code>
	 * 
	 * This method can be used to get the values of a selected field
	 * in the cronjob definition. For example:
	 * 
	 * Example 1. Get minutes from definition
	 * <code>
	 *    $cron = new M_Cron('30 1 * * *');
	 *    echo $cron->getField(M_Cron::MINUTE);
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * This method will return (boolean) FALSE, if no value could have
	 * been found for the requested cronjob field.
	 * 
	 * @access public
	 * @param string $part
	 * 		The cronjob field to be retrieved from M_Cron
	 * @return array
	 */
	public function getField($part) {
		return isset($this->_schedule[$part]) ? $this->_schedule[$part] : FALSE;
	}
	
	/**
	 * Get cronjob schedule
	 * 
	 * This method will return the complete cronjob schedule definition.
	 * For an intro on the definition of cronjob schedules, read the
	 * docs on {@link M_Cron::getField()}.
	 * 
	 * This return value of this method will be affected by:
	 * 
	 * - {@link M_Cron::__construct()}
	 * - {@link M_Cron::setSchedule()}
	 * - {@link M_Cron::setField()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getSchedule() {
		return implode(' ', $this->_schedule);
	}
	
	/**
	 * Is due?
	 * 
	 * This method will tell whether or not the cronjob that is being
	 * contained by the {@link M_Cron} object is expected to be ran at 
	 * or planned for a given time.
	 * 
	 * Optionally, you can provide this method with an {@link M_Date}
	 * object, to check if the cronjob is due at the time that is 
	 * being represented by that {@link M_Date} object. If you do not
	 * provide this object, {@link M_Cron} will check against the 
	 * current time.
	 * 
	 * @access public
	 * @param M_Date $date
	 * 		The time to be checked against
	 * @return boolean
	 */
	public function isDue(M_Date $date = NULL) {
		// If no date has been provided, we default the date to the
		// current time:
		if($date == NULL) {
			$date = new M_Date;
		}
		
		// The parts to be compared (in order of priority):
		$parts = array(
			// Compare the following cronjob fields
			'cron' => array(self::WEEKDAY, self::MONTH, self::DAY, self::HOUR, self::MINUTE),
			// with the following parts of the provided date:
			'date' => array(M_Date::WEEKDAY, M_Date::MONTH, M_Date::DAY, M_Date::HOUR, M_Date::MINUTE)
		);
		
		// For each of the parts to be compared:
		for($i = 0; $i < 5; $i ++) {
			// Get the current cronjob field's value:
			$tmp = $this->getField($parts['cron'][$i]);
			
			// If the value does not equal the asterisk
			// (Any date part value will match an asterisk; it's a 
			// wildcard character!)
			if($tmp != '*') {
				// Now, we have to compare date parts. The default 
				// comparison result is FALSE
				$c = FALSE;
				
				// If more than one instance of a value has been used
				// (separated by comma), we also want to evaluate that.
				// So, we parse an array out of the cronjob field value.
				$ar1 = explode(',', $tmp);
				
				// For each of the value(s) that have been parsed out
				// of the cronjob field:
				for($j = 0, $n = count($ar1); $j < $n && !$c; $j ++) {
					// Each of the values may be a range too (separated
					// by a dash character). So, again, we parse an
					// array out of the current value
					$ar2 = explode('-', $ar1[$j]);
					
					// If the current value is a range:
					if(count($ar2) == 2) {
						// Check if the date part falls in the range:
						$c = (
							// start
							($date->equals($ar2[0], $parts['date'][$i]) || $date->isAfter($ar2[0], $parts['date'][$i])) &&
							// end
							($date->equals($ar2[1], $parts['date'][$i]) || $date->isBefore($ar2[1], $parts['date'][$i]))
						);
					}
					// If the current value is NOT a range:
					else {
						// Check if the date part matches with the value:
						$c = $date->equals($ar2[0], $parts['date'][$i]);
					}
				}
				
				// Return FALSE, if the part did not match
				if($c === FALSE) {
					return FALSE;
				}
			}
		}
		
		// If we're still here, the cronjob is due!
		return TRUE;
	}
	
	/**
	 * TODO: Get next scheduled time
	 * 
	 * This method will calculate the next time the cronjob task 
	 * should be executed again. It will calculate that time, based
	 * on a given {@link M_Date} object as start date. If you do not
	 * provide this {@link M_Date} object, {@link M_Cron} will use 
	 * the current time.
	 * 
	 * The return value of this method is an {@link M_Date} object
	 * that contains the next scheduled time.
	 * 
	 * Example 1. Calculate the next scheduled time, after 2 AM,
	 * Christmas 2007
	 * 
	 * <code>
	 *    $cron = new M_Cron('15 1 * * *');
	 *    $date = $cron->getNextDate(new M_Date(array(
	 *       'year'   => 2007,
	 *       'month'  => 12,
	 *       'day'    => 25,
	 *       'hour'   => 1,
	 *       'minute' => 30
	 *    )));
	 *    
	 *    // show date:
	 *    echo date('d M Y H:i', $date->get(M_Date::TIMESTAMP));
	 * </code>
	 * 
	 * Example 1 will produce the following output:
	 * 
	 * <code>
	 *    26 Dec 2007 1:15
	 * </code>
	 * 
	 * @access public
	 * @param M_Date $date
	 * 		The time to be base the result date on
	 * @return boolean
	 */
	public function getNextDate(M_Date $date = NULL) {
		// If no date has been provided, we default the date to the
		// current time:
		if($date == NULL) {
			$date = new M_Date;
		}
		
		// Get the month field:
		// (Notice that we do this BEFORE checking day and time, as 
		// adding months may affect day and time of the month)
		$v = $this->getField(self::MONTH);
		
		// If the value of the month field is not equal to the date's
		// number of months, then we add the required number of months
		// to the date to make it match
		if($v != '*') {
			$diff = $v < $date->month ? 12 - $date->month + $v : $v - $date->month;
			$date->addTime($diff, M_Date::MONTH);
		}
		
		// Get the minutes field:
		$v = $this->getField(self::MINUTE);
		
		// If the value of the minutes field is not equal to the date's 
		// number of seconds, then we add the required number of seconds
		// to the date to make it match
		if($v != '*') {
			$diff = $v < $date->minute ? 60 - $date->minute + $v : $v - $date->minute;
			$date->addTime($diff, M_Date::MINUTE);
		}
		
		// Get the hour field:
		$v = $this->getField(self::HOUR);
		
		// If the value of the hour field is not equal to the date's
		// number of hours, then we add the required number of hours
		// to the date to make it match
		if($v != '*') {
			$diff = $v < $date->hour ? 24 - $date->hour + $v : $v - $date->hour;
			$date->addTime($diff, M_Date::HOUR);
		}
		
		// Get the day field:
		$v = $this->getField(self::DAY);
		
		// If the value of the day field is not equal to the date's
		// number of days, then we add the required number of days
		// to the date to make it match
		if($v != '*') {
			// Calculate the difference (number of days to be added):
			if($v < $date->day) {
				$calendar = new M_Calendar($date->year, $date->month);
				$n = $calendar->getNumberOfDays();
				$diff = $n - $date->day + $v;
			} else {
				$diff = $v - $date->day;
			}
			
			// add the required number of days:
			$date->addTime($diff, M_Date::DAY);
		}
		
		// Return the final date:
		return $date;
	}
	
	/**
	 * Get interval
	 * 
	 * This method will calculate the time interval that exists between
	 * each execution of the cronjob task. For example, if you set up
	 * the cronjob as following:
	 * 
	 * <code>
	 *    
	 * </code>
	 */
	
	/* -- SETTERS -- */
	
	/**
	 * Set schedule
	 * 
	 * As layed out in {@link M_Cron::getField()}, a cronjob spec
	 * consists of six fields. This method will set the value of the
	 * first five fields in the cronjob definition. This collection
	 * of the first five fields define the schedule of the cronjob.
	 * 
	 * @access public
	 * @uses M_Cron::setField()
	 * @param string $definition
	 * 		The cronjob schedule definition
	 * @return void
	 */
	public function setSchedule($definition) {
		$tmp = preg_split('/[\s]+/m', $definition);
		if(count($tmp) == 5) {
			$this->_schedule = array();
			$this->setField(self::MINUTE,  $tmp[0]);
			$this->setField(self::HOUR,    $tmp[1]);
			$this->setField(self::DAY,     $tmp[2]);
			$this->setField(self::MONTH,   $tmp[3]);
			$this->setField(self::WEEKDAY, $tmp[4]);
		} else {
			throw new M_CronException('Expecting five fields for cronjob schedule');
		}
	}
	
	/**
	 * Set cronjob field
	 * 
	 * This method can be used to set the value of a cronjob field.
	 * For an introduction on cronjob fields, read the documentation
	 * on {@link M_Cron::getField()}.
	 *
	 * This function makes use of 2 additional validate functions.
	 * - {@link M_Cron::_validateValue()}
	 * - {@link M_Cron::_validatePartValue()}
	 * 
	 * @access public
	 * @throws M_CronException
	 * @param string $part
	 * 		The cronjob field to be set a new value for
	 * @param mixed $value
	 * 		The new value of the cronjob field
	 * @return void
	 */
	public function setField($part, $value) {
		$ok = FALSE;

		// Validate each part. If we get a TRUE in return, validation succeeded
		if($this->_validateValue($part, $value)) {
			$this->_schedule[$part] = $value;

		// If validation failed, throw an error
		} else {
			throw new M_CronException('Could not set cronjob field; unrecognized field or unexpected value');
		}
	}

	/**
	 * Validate Value
	 *
	 * This function will validate a given cron job definition part
	 * (minute, hour, day, month, weekday), returning
	 * TRUE on success or FALSE on fail. These parts can consist of 3 types:
	 *		- with commas, describing a number of values (1,3,4)
	 *		- with dashes, describing a range of values (3-14)
	 *		- single values
	 *
	 * @access private
	 * @param string $part
	 *		The cronjob field to be set a new value for
	 * @param string $value
	 *		The new value of the cronjob field
	 * @return <type>
	 */
	private function _validateValue($part, $value) {

		// If the string contains commas, e.g. the value of the part describes
		// a number of values (for example: 1,3,5,6)
		if (strpos($value, ',')) $value = explode(',',$value);

		// If the string contains dashes, e.g. the value of the part describes
		// a range of values (for example: 3-6 or 10-15)
		elseif (strpos($value, '-')) $value = explode('-',$value);

		// If the value consists of only one value, we can validate right now
		else {
			return $this->_validatePartValue($part, $value);
		}

		// Validate each part. If one of the parts fails validation, return FALSE
		foreach($value AS $partValue) {
			if (!$this->_validatePartValue($part, $partValue)) return false;
		}

		// If we are still here validation succeeded, so we return TRUE
		return true;
	}

	/**
	 * Validate Part Value
	 *
	 * Will validate a single part of a cron definition part. For example, if
	 * the cron definition part for minutes is (1, 3, 4), this function will
	 * be called for each single entity of this part (once for 1, once for 3
	 * and once for 4).
	 *
	 * @access private
	 * @param string $part
	 *		The cronjob field to be set a new value for
	 * @param string $value
	 *		The new value of the cronjob field
	 * @return bool
	 *		Returns TRUE if validation succeeded, FALSE if validation failed
	 */
	private function _validatePartValue($part, $value) {
		switch($part) {
			case self::MINUTE:
				$ok = ($value == '*' || (is_numeric($value) && $value >= 0 && $value <= 59));
				break;

			case self::HOUR:
				$ok = ($value == '*' || (is_numeric($value) && $value >= 0 && $value <= 23));
				break;

			case self::DAY:
				$ok = ($value == '*' || (is_numeric($value) && $value >= 1 && $value <= 31));
				break;

			case self::MONTH:
				$ok = ($value == '*' || (is_numeric($value) && $value >= 1 && $value <= 12));
				break;

			case self::WEEKDAY:
				$ok = ($value == '*' || (is_numeric($value) && $value >= 0 && $value <= 6));
				break;
		}

		// Return the validation result
		return $ok;
	}
	
	/**
	 * Set task
	 * 
	 * As layed out in {@link M_Cron::getField()}, a cronjob spec
	 * consists of six fields. This method will set the value of the
	 * 6th field in the cronjob definition. This field defines the
	 * task to be run by the cronjob.
	 * 
	 * The task is described in the form of a callback function.
	 * Callback functions can not only be simple functions, but also 
	 * object methods, including static class methods.
	 * 
	 * A PHP function is passed by its name as a string. Any built-in 
	 * or user-defined function can be used, except language constructs 
	 * such as: array(), echo(), empty(), eval(), exit(), isset(), 
	 * list(), print(), unset().
	 * 
	 * A method of an instantiated object is passed as an array 
	 * containing an object at index 0 and the method name at index 1.
	 * 
	 * Static class methods can also be passed without instantiating 
	 * an object of that class by passing the class name instead of an 
	 * object at index 0.
	 * 
	 * @access public
	 * @throws M_CronException
	 * @param callback $task
	 * 		The cronjob task
	 * @return void
	 */
	public function setTask($task) {
		if(is_callable($task)) {
			$this->_task = $task;
		} else {
			throw new M_CronException('The cronjob task is not a valid callback');
		}
	}
}

/**
 * M_CronException class
 * 
 * Exceptions thrown by the M_Cron class
 * 
 * @package Core
 */
class M_CronException extends M_Exception {
}
?>