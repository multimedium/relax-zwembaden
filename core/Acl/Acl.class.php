<?php
/**
 * M_Acl
 * 
 * @package Core
 */
class M_Acl extends M_Object {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Rule type
	 * 
	 * This constant can be used to indicate a type of rule. Typically, this
	 * constant is used in combination with {@link M_Acl::setRule()}
	 * 
	 * @see M_Acl::setRule()
	 * @see M_Acl::allow()
	 * @var string
	 */
	const RULE_ALLOW = 1;
	
	/**
	 * Rule type
	 * 
	 * This constant can be used to indicate a type of rule. Typically, this
	 * constant is used in combination with {@link M_Acl::setRule()}
	 * 
	 * @see M_Acl::setRule()
	 * @see M_Acl::deny()
	 * @var string
	 */
	const RULE_DENY  = 0;
	
	/* -- PROPERTIES -- */
	
	/**
	 * Singleton instance
	 * 
	 * @static
	 * @see M_Acl::getInstance()
	 * @access private
	 * @var M_Acl
	 */
	private static $_instance;
	
	/**
	 * Rules
	 * 
	 * This property stores the list of rules that are set by 
	 * 
	 * - {@link M_Acl::addAllowRule()},
	 * - {@link M_Acl::addDenyRule()}, and
	 * - {@link M_Acl::addRule()}
	 *
	 * @access private
	 * @var array
	 */
	private $_rules = array();
	
	/**
	 * Roles
	 * 
	 * @access private
	 * @var array
	 */
	private $_roles = array();
	
	/**
	 * Resources
	 * 
	 * @access private
	 * @var array
	 */
	private $_resources = array();
	
	/**
	 * Permissions
	 * 
	 * @access private
	 * @var array
	 */
	private $_permissions = array();
	
	/**
	 * Storage
	 * 
	 * This property holds the instance of {@link M_AclStorage} (if any) that 
	 * is in charge of storing the ACL Data.
	 * 
	 * @access private
	 * @var M_AclStorage
	 */
	private $_storage;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access private
	 * @return M_Acl $acl
	 * 		The constructed ACL Instance
	 */
	private function __construct() {
	}
	
	/**
	 * Singleton constructor
	 * 
	 * @access public
	 * @return M_Acl
	 */
	public static function getInstance() {
		if(! self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Singleton constructor which will use a storage
	 *
	 * Setting the storage in this construct will apply all the data set
	 * in teh storage to the ACL
	 * 
	 * @param M_AclStorage $storage
	 * @return M_Acl
	 */
	public static function getInstanceWithStorage(M_AclStorage $storage) {
		return $storage->getAcl();
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Add Role
	 * 
	 * Will add a role to the ACL.
	 * 
	 * @access public
	 * @param MI_AclRole $role
	 * 		The role to be added to the ACL
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function addRole(MI_AclRole $role) {
		// Set the role in internal variable:
		$this->_roles[$role->getRoleId()] = $role;
		
		// Has a storage object been defined in the ACL?
		if($this->_storage) {
			// Then, add the role to the storage:
			$this->_storage->addRole($role);
		}
		
		// Return myself
		return $this;
	}

	/**
	 * Remove Role
	 *
	 * Will remove a role from the ACL.
	 *
	 * @access public
	 * @param MI_AclRole $role
	 * 		The role to be removed from the ACL
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function removeRole(MI_AclRole $role) {
		if (!isset($this->_roles[$role->getRoleId()])) return $this;

		//Unset the role in internal variable
		unset($this->_roles[$role->getRoleId()]);

		//Remove from db
		if($this->_storage) {
			$this->_storage->removeRole($role);
		}

		return $this;
	}

	/**
	 * Add Resource
	 * 
	 * Will add a role to the ACL.
	 * 
	 * @access public
	 * @param MI_AclResource $resource
	 * 		The resource to be added to the ACL
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function addResource(MI_AclResource $resource) {
		// Set the resource in internal variable:
		$this->_resources[$resource->getResourceId()] = $resource;
		
		// Has a storage object been defined in the ACL?
		if($this->_storage) {
			// Then, add the resource to the storage:
			$this->_storage->addResource($resource);
		}
		
		// Return myself
		return $this;
	}
	
	/**
	 * Add Permission
	 * 
	 * Will add a permission to the ACL.
	 * 
	 * @access public
	 * @param MI_AclPermission $permission
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function addPermission(MI_AclPermission $permission) {
		// Set the permission in internal variable:
		$this->_permissions[$permission->getPermissionId()] = $permission;
		
		// Has a storage object been defined in the ACL?
		if($this->_storage) {
			// Then, add the permission to the storage:
			$this->_storage->addPermission($permission);
		}
		
		// Return myself
		return $this;
	}
	
	/**
	 * Add "Allow" rule to ACL
	 * 
	 * NOTE:
	 * In order to grant access (add allow rule) to all permissions, you need to
	 * omit the permissions argument.
	 * 
	 * NOTE:
	 * In order to grant access (add allow rule) to all resources, you need to
	 * omit the resources argument.
	 * 
	 * In conclusion, in order to allow all permissions to all resources to a
	 * given role, you could do the following:
	 * 
	 * <code>
	 *    // The administrator has all permissions, on all resources
	 *    $acl->allow('administrator');
	 * </code>
	 * 
	 * @access public
	 * @uses M_Acl::addRule()
	 * @param MI_AclRole|string|array $role
	 * 		The role(s) onto which you wish to apply the "Allow" rule.
	 * @param MI_AclResource|string|array $resource
	 * 		The resource(s), if any, for which you wish to apply the "Allow" rule
	 * @param MI_AclPermission|string|array $permission
	 * 		The permission(s), if any, for which you wish to apply the "Allow" rule
	 * @param MI_AclAssertion $assert
	 * 		Additional conditions to be taken into account when applying the 
	 * 		"Allow" rule
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function addAllowRule($role, $resource = NULL, $permission = NULL, MI_AclAssertion $assert = NULL) {
		return $this->addRule(self::RULE_ALLOW, $role, $resource, $permission, $assert);
	}
	
	/**
	 * Add "Deny" rule to ACL
	 * 
	 * @access public
	 * @uses M_Acl::addRule()
	 * @param MI_AclRole|string|array $role
	 * 		The role(s) onto which you wish to apply the "Deny" rule.
	 * @param MI_AclResource|string|array $resource
	 * 		The resource(s), if any, for which you wish to apply the "Deny" rule
	 * @param string|array $permission
	 * 		The permission(s), if any, for which you wish to apply the "Deny" rule
	 * @param MI_AclAssertion $assert
	 * 		Additional conditions to be taken into account when applying the 
	 * 		"Deny" rule
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function addDenyRule($role, $resource = NULL, $permission = NULL, MI_AclAssertion $assert = NULL) {
		return $this->addRule(self::RULE_DENY, $role, $resource, $permission, $assert);
	}
	
	/**
	 * Add rule
	 * 
	 * This method can be used publicly, and is also used by the class to add
	 * allow rules - {@link M_Acl::addAllowRule()} - as well as deny rules - 
	 * {@link M_Acl::addDenyRule()} to the ACL.
	 * 
	 * @access public
	 * @param integer $type
	 * 		The type of rule: {@link M_Acl::RULE_ALLOW} or {@link M_Acl::RULE_DENY}
	 * @param MI_AclRole|string|array $role
	 * 		The role(s) onto which you wish to apply the rule.
	 * @param MI_AclResource|string|array $resource
	 * 		The resource(s), if any, for which you wish to apply the rule
	 * @param MI_AclPermission|string|array $permission
	 * 		The permission(s), if any, for which you wish to apply the rule
	 * @param MI_AclAssertion $assert
	 * 		Additional conditions to be taken into account when applying the rule
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function addRule($type, $role, $resource = NULL, $permission = NULL, MI_AclAssertion $assert = NULL) {
		// For each of the rules that can be extracted from the provided vars:
		foreach($this->_getRulesFromVariables($type, $role, $resource, $permission, $assert) as $rule) {
			// Add the rule to the ACL:
			$this->_addRule($rule['type'], $rule['role'], $rule['resource'], $rule['permission']);
		}
	}
	
	/**
	 * Remove "Allow" rule from ACL
	 * 
	 * To remove one or more access rules from the ACL, simply use the available 
	 * {@link M_Acl::removeAllowRule()} or {@link M_Acl::removeDenyRule()} methods. 
	 * As with {@link M_Acl::addAllowRule()} and {@link M_Acl::addDenyRule()}, 
	 * you may provide a NULL value to indicate application to all roles, 
	 * resources, and/or permissions.
	 * 
	 * @access public
	 * @param MI_AclRole|string|array $role
	 * 		The role(s) for which you wish to remove the "Allow" rule.
	 * @param MI_AclResource|string|array $resource
	 * 		The resource(s), if any, for which you wish to remove the "Allow" rule
	 * @param MI_AclPermission|string|array $permission
	 * 		The permission(s), if any, for which you wish to remove the "Allow" rule
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function removeAllowRule($role, $resource = NULL, $permission = NULL) {
		return $this->removeRule(self::RULE_ALLOW, $role, $resource, $permission);
	}
	
	/**
	 * Remove "Deny" rule from ACL
	 * 
	 * To remove one or more access rules from the ACL, simply use the available 
	 * {@link M_Acl::removeAllowRule()} or {@link M_Acl::removeDenyRule()} methods. 
	 * As with {@link M_Acl::addAllowRule()} and {@link M_Acl::addDenyRule()}, 
	 * you may provide a NULL value to indicate application to all roles, 
	 * resources, and/or permissions.
	 * 
	 * @see M_Acl::deny()
	 * @uses M_Acl::setRule()
	 * @access public
	 * @param MI_AclRole|string|array $roleId
	 * 		The role(s) for which you wish to remove the "Deny" rule.
	 * @param MI_AclResource|string|array $resourceId
	 * 		The resource(s), if any, for which you wish to remove the "Deny" rule
	 * @param MI_AclPermission|string|array $permission
	 * 		The permission(s), if any, for which you wish to remove the "Deny" rule
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function removeDenyRule($role, $resource = NULL, $permission = NULL) {
		return $this->removeRule(self::RULE_DENY, $role, $resource, $permission);
	}
	
	/**
	 * Remove rule
	 * 
	 * This method can be used publicly, and is also used by the class itself to 
	 * remove allow rules - {@link M_Acl::removeAllowRule()} - as well as deny 
	 * rules - {@link M_Acl::removeDenyRule()} from the ACL.
	 * 
	 * @access public
	 * @param integer $type
	 * 		The type of rule: {@link M_Acl::RULE_ALLOW} or {@link M_Acl::RULE_DENY}
	 * @param MI_AclRole|string|array $role
	 * @param MI_AclResource|string|array $resource
	 * @param MI_AclPermission|string|array $permission
	 * @param MI_AclAssertion $assert
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function removeRule($type, $role, $resource = NULL, $permission = NULL, MI_AclAssertion $assert = NULL) {
		// For each of the rules that can be extracted from the provided vars:
		foreach($this->_getRulesFromVariables($type, $role, $resource, $permission, $assert) as $rule) {
			// Add the rule to the ACL:
			$this->_removeRule($rule['type'], $rule['role'], $rule['resource'], $rule['permission']);
		}
	}

	/**
	 * Remove all
	 * 
	 * Will remove all roles, resources and permissions from the ACL.
	 * 
	 * @access public
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function removeAll() {
		// Remove the rules from the ACL Object
		$this->_rules = array();
		
		// Has a storage object been defined in the ACL?
		if($this->_storage) {
			// Then, also remove from storage:
			$this->_storage->removeAll();
		}
		
		// Return myself
		return $this;
	}
	
	/**
	 * Set storage, for persistence
	 * 
	 * @access public
	 * @param M_AclStorage $storage
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	public function setStorage(M_AclStorage $storage) {
		$this->_storage = $storage;
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get roles
	 * 
	 * Will provide with the collection of roles - {@link MI_AclRole} instances - 
	 * that have been added to the ACL.
	 * 
	 * @see M_Acl::addRole()
	 * @access public
	 * @return ArrayIterator $roles
	 * 		The collection of {@link MI_AclRole} instances in the ACL
	 */
	public function getRoles() {
		return new M_ArrayIterator($this->_roles);
	}
	
	/**
	 * Get role
	 * 
	 * Will provide with the requested {@link MI_AclRole} instance.
	 * 
	 * IMPORTANT NOTE:
	 * If the requested role could not have been found with the provided ID, this
	 * method will return NULL instead.
	 * 
	 * @access public
	 * @param string $roleId
	 * 		The Role ID, with which to look up the instance
	 * @return MI_AclRole
	 */
	public function getRole($roleId) {
		// If the requested role does not exist:
		if(! isset($this->_roles[$roleId])) {
			// Then, return NULL
			return NULL;
		}
		
		// Return the role:
		return $this->_roles[$roleId];
	}
	
	/**
	 * Get resources
	 * 
	 * Will provide with the collection of resources - {@link MI_AclResource} 
	 * instances - that have been added to the ACL
	 * 
	 * @access public
	 * @return ArrayIterator $resources
	 * 		The collection of {@link MI_AclResource} instances in the ACL
	 */
	public function getResources() {
		return new M_ArrayIterator($this->_resources);
	}
	
	/**
	 * Get resource
	 * 
	 * Will provide with the requested instance of {@link MI_AclResource}.
	 * 
	 * IMPORTANT NOTE:
	 * If the requested resource could not have been found with the provided ID, 
	 * this method will return NULL instead.
	 * 
	 * @access public
	 * @param string $resourceId
	 * 		The Resource ID, with which to look up the instance
	 * @return MI_AclResource
	 */
	public function getResource($resourceId) {
		// If the requested resource does not exist:
		if(! isset($this->_resources[$resourceId])) {
			// Then, return NULL
			return NULL;
		}
		
		// Return the resource:
		return $this->_resources[$resourceId];
	}
	
	/**
	 * Get permissions
	 * 
	 * Will provide with the collection of permissions - {@link MI_AclPermission} 
	 * instances - that has been added
	 * to the ACL
	 * 
	 * @access public
	 * @return ArrayIterator $permissions
	 * 		The collection of {@link MI_AclPermission} instances in the ACL
	 */
	public function getPermissions() {
		return new M_ArrayIterator($this->_permissions);
	}
	
	/**
	 * Get permission
	 * 
	 * Will provide with the requested instance of {@link MI_AclPermission}.
	 * 
	 * IMPORTANT NOTE:
	 * If the requested permission could not have been found with the provided 
	 * ID, this method will return NULL instead.
	 * 
	 * @access public
	 * @param string $permissionId
	 * 		The Permission ID, with which to look up the instance
	 * @return MI_AclPermission
	 */
	public function getPermission($permissionId) {
		// If the requested permission does not exist:
		if(! isset($this->_permissions[$permissionId])) {
			// Then, return NULL
			return NULL;
		}
		
		// Return the permission:
		return $this->_permissions[$permissionId];
	}
	
	/**
	 * Is allowed?
	 * 
	 * Will check whether or not a given role has been assigned an "Allow" rule,
	 * for a given resource (if any) and a given permission (if any).
	 * 
	 * @access public
	 * @param MI_AclRole|string $role
	 * 		The role for which you wish to check whether or not it has been allowed.
	 * @param MI_AclResource|string $resource
	 * 		The resource for which you wish to check
	 * @param MI_AclPermission|string $permission
	 * 		The permission for which you wish to check
	 * @param boolean $autoCreate
	 *		Creates the resource/permission if they doesn't exist yet
	 * @return bool $flag
	 * 		Returns TRUE if the role has been granted permission, FALSE if not
	 */
	public function isAllowed($role, $resource, $permission, $autoCreate = true) {
		if ($autoCreate) {
			// Check if rule exists, if not add it to ACL
			try {
				$resource = $this->_getResourceFromVariable($resource);
			}catch(M_AclException $e) {
				if (!M_Helper::isInstanceOf($resource, 'M_AclResource')) {
					$resource = new M_AclResource($resource);
				}
				$this->addResource($resource);
			}

			// Check if permission exists, if not add it to ACL
			try {
				$permission = $this->_getPermissionFromVariable($permission);
			}catch(M_AclException $e) {
				if (!M_Helper::isInstanceOf($permission, 'M_AclPermission')) {
					$permission = new M_AclPermission($permission);
				}
				$this->addPermission($permission);
			}
		}

		// Get the boolean result from the defined set of rules:
		$b = $this->_getBooleanResult($role, $resource, $permission);
		
		// If access is allowed
		if($b === TRUE) {
			// Return TRUE
			return TRUE;
		}
		
		// If the outcome is FALSE, then access is denied. Note that this will 
		// also be applied for a NULL outcome. All access is denied, until a
		// rule is added to specify otherwise
		return FALSE;
	}

	/**
	 * Get the status (allow/disallow/notset) for a rule
	 *
	 * Tells you if the rule-status for the combination
	 * of a role/resource/permission.
	 *
	 * E.g. has a specific rule been set for the role "multimedium" on resource
	 * "news" for permission "edit". If TRUE is been returned, a rule has been
	 * set to "allow". If FALSE is been returned, a rule has been
	 * set to "deny". If NULL is been returned, no specific rule has been set.
	 *
	 * The resource and permission params arent mandatory. It's possible a rule
	 * is set for all permissions on resource "news". Or even more: a rule
	 * can be appliced only to a role ("multimedium" has access to all resources
	 * for every permission).
	 *
	 *
	 * NOTE: the result of this does not imply the user also has access to the
	 * given resource. It only tells us more if a rule is know, and if yes what
	 * it's status is.
	 *
	 * @param MI_AclRole|string $role
	 * @param MI_AclResource|string $resource
	 * @param MI_AclPermission|string $permission
	 * @author Ben Brughmans
	 */
	public function getRuleStatus($role, $resource = null, $permission = null) {
		$role = $this->_getRoleFromVariable($role);

		/* get the resource */
		if (is_null($resource)) {
			$resourceId = '*null';
		}else {
			$resource = $this->_getResourceFromVariable($resource);
			$resourceId = $resource->getResourceId();
		}

		/* get the permission */
		if (is_null($permission)) {
			$permissionId = '*null';
		}else {
			$permission = $this->_getPermissionFromVariable($permission);
			$permissionId = $permission->getPermissionId();
		}
		
		//check if any rules are set for this role
		$roleData = M_Helper::getArrayElement($role->getRoleId(), $this->_rules);
		if (is_null($roleData)) return NULL;
		
		//get the data for the given resource
		$resourceData = M_Helper::getArrayElement($resourceId, $roleData);

		//if no data is found: no rule has been set for this role+resource
		if (is_null($resourceData)) return NULL;

		//get the data for the given permission, if the permission could not
		//be found: NULL will be returned. Otherwise, the correct status (TRUE/
		//FALSE) will be returned
		return M_Helper::getArrayElement($permissionId, $resourceData);
	}

	/**
	 * Get the data to which a rule applies
	 *
	 * Returns an array which tells us to which resource (or all) and/or
	 * permissions (or all) this rule applies
	 * 
	 * @param int $databaseId
	 * @return array
	 */
	public function getRuleData($databaseId) {

		if (!$this->_storage) return false;

		return $this->_storage->getRuleByDatabaseId($databaseId);
	}

	/**
	 * Remove a rule by it's database-id
	 *
	 * @param int $databaseId
	 * @return bool
	 */
	public function removeRuleByDatabaseId($databaseId) {
		if (!M_Helper::isInstanceOf($this->_storage, 'M_AclStorageDb')) {
			throw new M_AclException('Cannot remove a rule from the databse, no M_AclStorageDb object found');
		}

		$rule = $this->getRuleData($databaseId);

		//if a rule is found in the database, remove it
		if ($rule) {
			$this->removeRule($rule['type'], $rule['role'], $rule['resource'], $rule['permission']);
			return true;
		}else {
			return false;
		}
	}

	/**
	 * Get the set of rules
	 * 
	 * @return M_ArrayIterator
	 */
	public function getRules() {
		$storage = $this->_storage;
		/* @var $storage M_AclStorageDb */
		return new M_ArrayIterator($storage->getRules());
	}

	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Add a rule
	 * 
	 * Used by {@link M_Acl::addRule()} to add a rule to the ACL
	 * 
	 * @access protected
	 * @param integer $type
	 * @param MI_AclRole $role
	 * @param MI_AclResource $resource
	 * @param MI_AclPermission $permission
	 * @return void
	 */
	//protected function _addRule($type, MI_AclRole $role, MI_AclResource $resource = NULL, MI_AclPermission $permission = NULL, MI_AclAssertion $assert = NULL) {
	protected function _addRule($type, MI_AclRole $role, MI_AclResource $resource = NULL, MI_AclPermission $permission = NULL) {
		// The value to be added to the internal tree of rules:
		// TRUE  = Access allowed
		// FALSE = Access denied
		$value = $type == self::RULE_ALLOW ? TRUE : FALSE;
		
		// The key to be used for the resource:
		$keyResource = $resource ? $resource->getResourceId() : '*null';
		
		// The key to be used for the permission:
		$keyPermission = $permission ? $permission->getPermissionId() : '*null';
		
		// Add the value to the tree of rules:
		$this->_rules[$role->getRoleId()][$keyResource][$keyPermission] = $value;
		
		// Has a storage object been defined in the ACL?
		if($this->_storage) {
			// Then, add the rule to the storage:
			$this->_storage->addRule($type, $role, $resource, $permission);
		}
	}
	
	/**
	 * Remove a rule
	 * 
	 * Used by {@link M_Acl::removeRule()} to add a rule to the ACL
	 * 
	 * @access protected
	 * @param integer $type
	 * @param MI_AclRole $role
	 * @param MI_AclResource $resource
	 * @param MI_AclPermission $permission
	 * @return void
	 */
	//protected function _removeRule($type, MI_AclRole $role, MI_AclResource $resource = NULL, MI_AclPermission $permission = NULL, MI_AclAssertion $assert = NULL) {
	protected function _removeRule($type, MI_AclRole $role, MI_AclResource $resource = NULL, MI_AclPermission $permission = NULL) {
		// The value to be added to the internal tree of rules:
		// TRUE  = Access allowed
		// FALSE = Access denied
		$value = $type == self::RULE_ALLOW ? TRUE : FALSE;
		
		// The key to be used for the resource:
		$keyResource = $resource ? $resource->getResourceId() : '*null';
		
		// The key to be used for the permission:
		$keyPermission = $permission ? $permission->getPermissionId() : '*null';
		
		// Check if the value exists:
		if(
			isset($this->_rules[$role->getRoleId()][$keyResource][$keyPermission]) &&
			$this->_rules[$role->getRoleId()][$keyResource][$keyPermission] === $value
		) {
			// If so, we delete the value
			unset($this->_rules[$role->getRoleId()][$keyResource][$keyPermission]);
		
			// Has a storage object been defined in the ACL?
			if($this->_storage) {
				// Then, remove the rule from the storage:
				$this->_storage->removeRule($type, $role, $resource, $permission);
			}
		}
	}
	
	/**
	 * Get rules from variable
	 * 
	 * Will provide with an array of RULES from the provided set of variables. In
	 * order to define the collection of RULES, this method will create all possible
	 * combinations, also with the help of
	 * 
	 * - {@link M_Acl::_getRolesFromVariable()}
	 * - {@link M_Acl::_getResourcesFromVariable()}
	 * - {@link M_Acl::_getPermissionsFromVariable()}
	 * 
	 * The result of this method is an array that has been populated with rules.
	 * 
	 * @throws M_AclException
	 * @access protected
	 * @param integer $type
	 * 		The type of rule: {@link M_Acl::RULE_ALLOW} or {@link M_Acl::RULE_DENY}
	 * @param MI_AclRole|string|array $role
	 * 		The role(s) onto which you wish to apply the rule.
	 * @param MI_AclResource|string|array $resource
	 * 		The resource(s), if any, for which you wish to apply the rule
	 * @param MI_AclPermission|string|array $permission
	 * 		The permission(s), if any, for which you wish to apply the rule
	 * @return array $rules
	 */
	protected function _getRulesFromVariables($type, $role, $resource, $permission) {
		// Output variable
		$out = array();
		
		// Make sure that the rule type is recognized by M_Acl
		if(! in_array($type, array(self::RULE_ALLOW, self::RULE_DENY))) {
			// If not, we throw an exception
			throw new M_AclException(sprintf(
				'Unrecognized rule of type "%s"; must be of type "%s" or "%s"',
				$type,
				self::RULE_ALLOW,
				self::RULE_DENY
			));
		}
		
		// Get the resource(s) to which the current rule is to be applied:
		$resources = $this->_getResourcesFromVariable($resource);
		
		// If no resources:
		if(count($resources) == 0) {
			// then we define the collection of resources to an array that 
			// contains only one element: NULL
			$resources   = array();
			$resources[] = NULL;
		}
		
		// Get the permission(s) to which the rule is to be applied
		$permissions = $this->_getPermissionsFromVariable($permission);
		
		// If no permissions:
		if(count($permissions) == 0) {
			// then we define the collection of permissions to an array that 
			// contains only one element: NULL
			$permissions   = array();
			$permissions[] = NULL;
		}
		
		// For each of the roles to which the rule is to be applied:
		/* @var $currentRole MI_AclRole */
		foreach($this->_getRolesFromVariable($role) as $currentRole) {
			// For each of the resources:
			/* @var $currentResource MI_AclResource */
			foreach($resources as $currentResource) {
				// For each of the permissions:
				/* @var $currentPermission MI_AclPermission */
				foreach($permissions as $currentPermission) {
					// We add the rule to the output variable
					array_push($out, array(
						'type'        => $type,
						'role'        => $currentRole,
						'resource'    => $currentResource,
						'permission'  => $currentPermission
					));
				}
			}
		}
		
		// Return the output variable
		return $out;
	}
	
	/**
	 * Get boolean result from rules
	 * 
	 * Used by {@link M_Acl::isAllowed()}.
	 * 
	 * NOTE:
	 * Will return NULL, if no boolean could have been found in the defined set
	 * of rules in the ACL.
	 * 
	 * @access protected
	 * @param MI_AclRole|string $role
	 * 		The role for which you wish to check whether or not it has been allowed.
	 * @param MI_AclResource|string $resource
	 * 		The resource for which you wish to check
	 * @param MI_AclPermission|string $permission
	 * 		The permission for which you wish to check
	 * @return boolean $flag
	 */
	protected function _getBooleanResult($role, $resource, $permission) {
		// Prepare the ACL Objects with which we are going to work in order to
		// check whether or not the role has access:
		$oRole        = $this->_getRoleFromVariable($role);
		$roleId       = $oRole->getRoleId();
		
		// There should be at least one rule for the requested role. We check
		// if that is the case:
		if(! isset($this->_rules[$roleId])) {
			// If no rules are available for this role, then we get the parent 
			// of the role:
			$parentRole = $oRole->getParentRole();
			
			// If a parent is NOT available:
			if(! $parentRole) {
				// Return NULL. No boolean result could have been extracted
				// from the defined rules:
				return NULL;
			}
			
			// If the parent is available, then we do the check with the 
			// parent role
			return $this->_getBooleanResult($parentRole, $resource, $permission);
		}
		// Prepare some more work variables (ACL Objects):
		$oPermission  = $this->_getPermissionFromVariable($permission);
		$permissionId = $oPermission->getPermissionId();
		
		// Step 1:
		// Has a rule been set for this specific resource, or for the 
		// parent resource?
		$rs = $this->_getBooleanResultForResource($oRole, $this->_getResourceFromVariable($resource), $oPermission);
		
		// If a boolean has been found:
		if($rs !== NULL) {
			// Then, return the boolean
			return (bool) $rs;
		}
		
		// Step 2:
		// Has a rule been set for the role + permission?
		$c = isset($this->_rules[$roleId]['*null']);
		if($c && isset($this->_rules[$roleId]['*null'][$permissionId])) {
			// If so, we return the result of that rule:
			return (bool) $this->_rules[$roleId]['*null'][$permissionId];
		}
		
		// Step 3:
		// Has a rule been set for the role?
		if($c && isset($this->_rules[$roleId]['*null']['*null'])) {
			// If so, we return the result of that rule:
			return (bool) $this->_rules[$roleId]['*null']['*null'];
		}
		
		// Step 4:
		// Check parent role, if any
		$parentRole = $oRole->getParentRole();
		
		// If a parent is NOT available:
		if(! $parentRole) {
			// Return NULL. No boolean result could have been extracted
			// from the defined rules:
			return NULL;
		}
		
		// If the parent is available, then we do the check with the 
		// parent role
		return $this->_getBooleanResult($parentRole, $resource, $permission);
	}
	
	/**
	 * Get boolean result from rules for a specific role + resource
	 * 
	 * Used by {@link M_Acl::_getBooleanResult()}.
	 * 
	 * NOTE:
	 * Will return NULL, if no boolean could have been found in the defined set
	 * of rules in the ACL.
	 * 
	 * @access protected
	 * @param MI_AclRole $role
	 * @param MI_AclResource $resource
	 * @param MI_AclPermission $permission
	 * @return boolean $flag
	 */
	protected function _getBooleanResultForResource(MI_AclRole $role, MI_AclResource $resource, MI_AclPermission $permission) {
		// Prepare some more work variables:
		$roleId       = $role->getRoleId();
		$resourceId   = $resource->getResourceId();
		$permissionId = $permission->getPermissionId();
		
		// Step 1: Has a rule been set for the role + resource + permission?
		$c = isset($this->_rules[$roleId][$resourceId]);
		if($c && isset($this->_rules[$roleId][$resourceId][$permissionId])) {
			// If so, we return the result of that rule:
			return (bool) $this->_rules[$roleId][$resourceId][$permissionId];
		}
		
		// Step 2: Has a rule been set for the role + resource?
		if($c && isset($this->_rules[$roleId][$resourceId]['*null'])) {
			// If so, we return the result of that rule:
			return (bool) $this->_rules[$roleId][$resourceId]['*null'];
		}
		
		// If we are still here, then no applicable rule has been found for the
		// requested resource. The next step is to try with the parent of the
		// resource (if any):
		$parentResource = $resource->getParentResource();
		
		// If a parent resource is available:
		if($parentResource) {
			// Then we do the check again, with the parent resource:
			return $this->_getBooleanResultForResource($role, $parentResource, $permission);
		}
		
		// If none of the above has been set, we return NULL
		return NULL;
	}
	
	/**
	 * Get the roles in a variable
	 * 
	 * Will provide with an array of Roles from the provided variable. The 
	 * provided variable can either be:
	 * 
	 * - an instance of {@link MI_AclRole}
	 * - a string, containing the Role ID
	 * - an array|iterator, containing instances of {@link MI_AclRole}
	 * - an array|iterator, containing strings with the Role ID
	 * - an array|iterator, combining both the above
	 * 
	 * The result of this method is an array that has been populated with the 
	 * corresponding instances of {@link MI_AclRole}. If the variable has been 
	 * corrupted or is malformed, an exception will be thrown.
	 * 
	 * @throws M_AclException
	 * @access protected
	 * @param mixed $var
	 * @return array $roles
	 * 		The collection of {@link MI_AclRole} instances
	 */
	protected function _getRolesFromVariable($var) {
		// Output array
		$out = array();
		
		// If the provided variable is an iterator (object or array)
		if(M_Helper::isIterator($var)) {
			// Then, we loop the contents of the iterator. For each of the elements
			// in the provided iterator:
			foreach($var as $current) {
				// Get the role from the current item in the iterator, and
				// add to the output array:
				array_push($out, $this->_getRoleFromVariable($current));
			}
		}
		// If the provided variable is not an iterator:
		else {
			// In that case, the variable should simply contain the Role. We
			// get the Role and add it to the output array:
			array_push($out, $this->_getRoleFromVariable($var));
		}
		
		// If the output is empty:
		if(count($out) == 0) {
			// Then, we throw an exception
			throw new M_AclException(sprintf(
				'Cannot find ACL Roles in %s',
				var_export($var, TRUE)
			));
		}
		
		// Return the collection of roles:
		return $out;
	}
	
	/**
	 * Get role from variable
	 * 
	 * Will provide with an instance of {@link MI_AclRole} for the provided 
	 * variable. The provided variable can either be:
	 * 
	 * - an instance of {@link MI_AclRole}
	 * - a string, containing the Role ID
	 * 
	 * @throws M_AclException
	 * @access protected
	 * @param mixed $var
	 * @return MI_AclRole
	 */
	protected function _getRoleFromVariable($var) {
		// If the provided variable is an instance of MI_AclRole
		if(is_object($var) && M_Helper::isInterface($var, 'MI_AclRole')) {
			// If so, we simply return the object
			return $var;
		}
		// If the variable is not an object:
		else {
			// Then, it must be a string!
			if(! is_string($var)) {
				// If not so, we throw an exception
				throw new M_AclException(sprintf(
					'Cannot find role with variable %s!',
					var_export($var, TRUE)
				));
			}
			
			// We check whether or not the requested role exists:
			if(! isset($this->_roles[(string) $var])) {
				// If not existing, we throw an exception, because we cannot
				// recognize the variable:
				throw new M_AclException(sprintf(
					'Cannot find role %s in ACL',
					var_export($var, TRUE)
				));
			}
			
			// Return the Role
			return $this->_roles[(string) $var];
		}
	}
	
	/**
	 * Get the resources in a variable
	 * 
	 * Will provide with an array of Resources from the provided variable. The 
	 * provided variable can either be:
	 * 
	 * - an instance of {@link MI_AclResource}
	 * - a string, containing the Resource ID
	 * - an array|iterator, containing instances of {@link MI_AclResource}
	 * - an array|iterator, containing strings with the Resource ID
	 * - an array|iterator, combining both the above
	 * 
	 * The result of this method is an array that has been populated with the 
	 * corresponding instances of {@link MI_AclResource}. If the variable has 
	 * been corrupted or is malformed, an exception will be thrown.
	 * 
	 * @throws M_AclException
	 * @access protected
	 * @param mixed $var
	 * @return array $resources
	 * 		The collection of {@link MI_AclResource} instances
	 */
	protected function _getResourcesFromVariable($var) {
		// Output array
		$out = array();
		
		// If a variable has been provided
		if($var) {
			// If the provided variable is an iterator (object or array)
			if(M_Helper::isIterator($var)) {
				// Then, we loop the contents of the iterator. For each of the elements
				// in the provided iterator:
				foreach($var as $current) {
					// Get the resource from the current item in the iterator, and
					// add to the output array:
					array_push($out, $this->_getResourceFromVariable($current));
				}
			}
			// If the provided variable is not an iterator:
			else {
				// In that case, the variable should simply contain the Resource. We
				// get the Resource and add it to the output array:
				array_push($out, $this->_getResourceFromVariable($var));
			}
		}
		
		// Return the collection of resources:
		return $out;
	}
	
	/**
	 * Get resource from variable
	 * 
	 * Will provide with an instance of {@link MI_AclResource} for the provided 
	 * variable. The provided variable can either be:
	 * 
	 * - an instance of {@link MI_AclResource}
	 * - a string, containing the Resource ID
	 * 
	 * @throws M_AclException
	 * @access protected
	 * @param mixed $var
	 * @return MI_AclResource
	 */
	protected function _getResourceFromVariable($var) {
		// If the provided variable is an instance of MI_AclResource
		if(is_object($var) && M_Helper::isInterface($var, 'MI_AclResource')) {
			// If so, we simply return the object
			return $var;
		}
		// If the variable is not an object:
		else {
			// Then, it must be a string!
			if(! is_string($var)) {
				// If not so, we throw an exception
				throw new M_AclException(sprintf(
					'Cannot find resource with variable %s!',
					var_export($var, TRUE)
				));
			}
			
			// We check whether or not the requested resource exists:
			if(! isset($this->_resources[(string) $var])) {
				// If not existing, we throw an exception, because we cannot
				// recognize the variable:
				throw new M_AclException(sprintf(
					'Cannot find resource %s in ACL',
					var_export($var, TRUE)
				));
			}
			
			// Return the Resource
			return $this->_resources[(string) $var];
		}
	}
	
	/**
	 * Get the permissions in a variable
	 * 
	 * Will provide with an array of Permissions from the provided variable. The 
	 * provided variable can either be:
	 * 
	 * - an instance of {@link MI_AclPermission}
	 * - a string, containing the Permission ID
	 * - an array|iterator, containing instances of {@link MI_AclPermission}
	 * - an array|iterator, containing strings with the Permission ID
	 * - an array|iterator, combining both the above
	 * 
	 * The result of this method is an array that has been populated with the 
	 * corresponding instances of {@link MI_AclPermission}. If the variable has 
	 * been corrupted or is malformed, an exception will be thrown.
	 * 
	 * @throws M_AclException
	 * @access protected
	 * @param mixed $var
	 * @return array $permissions
	 * 		The collection of {@link MI_AclPermission} instances
	 */
	protected function _getPermissionsFromVariable($var) {
		// Output array
		$out = array();
		
		// If a variable has been provided
		if($var) {
			// If the provided variable is an iterator (object or array)
			if(M_Helper::isIterator($var)) {
				// Then, we loop the contents of the iterator. For each of the elements
				// in the provided iterator:
				foreach($var as $current) {
					// Get the permission from the current item in the iterator, and
					// add to the output array:
					array_push($out, $this->_getPermissionFromVariable($current));
				}
			}
			// If the provided variable is not an iterator:
			else {
				// In that case, the variable should simply contain the Permission. We
				// get the Permission and add it to the output array:
				array_push($out, $this->_getPermissionFromVariable($var));
			}
		}
		
		// Return the collection of permissions:
		return $out;
	}
	
	/**
	 * Get permission from variable
	 * 
	 * Will provide with an instance of {@link MI_AclPermission} for the provided 
	 * variable. The provided variable can either be:
	 * 
	 * - an instance of {@link MI_AclPermission}
	 * - a string, containing the Permission ID
	 * 
	 * @throws M_AclException
	 * @access protected
	 * @param mixed $var
	 * @return MI_AclPermission
	 */
	protected function _getPermissionFromVariable($var) {
		// If the provided variable is an instance of MI_AclPermission
		if(is_object($var) && M_Helper::isInterface($var, 'MI_AclPermission')) {
			// If so, we simply return the object
			return $var;
		}
		// If the variable is not an object:
		else {
			// Then, it must be a string!
			if(! is_string($var)) {
				// If not so, we throw an exception
				throw new M_AclException(sprintf(
					'Cannot find permission with variable %s!',
					var_export($var, TRUE)
				));
			}
			
			// We check whether or not the requested permission exists:
			if(! isset($this->_permissions[(string) $var])) {
				// If not existing, we throw an exception, because we cannot
				// recognize the variable:
				throw new M_AclException(sprintf(
					'Cannot find permission %s in ACL',
					var_export($var, TRUE)
				));
			}
			
			// Return the Permission
			return $this->_permissions[(string) $var];
		}
	}
}