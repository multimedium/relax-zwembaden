<?php
/**
 * M_AclStorage
 * 
 * Provides with an abstract implementation, and can be
 * used as superclass to your specific implementation of an ACL Storage Object.
 * Also, this class is the motherclass to already existing storage objects, such
 * as {@link M_AclStorageDb}
 * 
 * @abstract 
 * @package Core
 */
abstract class M_AclStorage {

	/**
	 * Get the ACL from the storage
	 *
	 * @access public
	 * @return M_Acl
	 */
	abstract public function getAcl();

	/**
	 * Get roles
	 *
	 * Will provide with the collection of roles - {@link MI_AclRole} instances -
	 * that have been stored in the ACL Storage Object.
	 *
	 * @see M_AclStorage::addRole()
	 * @access public
	 * @return M_ArrayIterator $roles
	 * 		The collection of {@link MI_AclRole} instances
	 */
	abstract public function getRoles();

	/**
	 * Get rules
	 *
	 * Will provide with the collection of rules that have been stored in the ACL
	 * Storage Object.
	 *
	 * @access public
	 * @return M_ArrayIterator $rules
	 *		The collection of rules
	 */
	abstract public function getRules();

	/**
	 * Get resources
	 *
	 * Will provide with the collection of resources - {@link MI_AclResource}
	 * instances - that have been stored in the ACL Storage Object.
	 *
	 * @see M_AclStorage::addResource()
	 * @access public
	 * @return M_ArrayIterator $resources
	 * 		The collection of {@link MI_AclResource} instances
	 */
	abstract public function getResources();

	/**
	 * Get permissions
	 *
	 * Will provide with the collection of permissions - {@link MI_AclPermission}
	 * instances - that have been stored in the ACL Storage Object.
	 *
	 * @see M_AclStorage::addPermission()
	 * @access public
	 * @return M_ArrayIterator $permissions
	 * 		The collection of {@link MI_AclPermission} instances
	 */
	abstract public function getPermissions();

	/**
	 * Add a role
	 *
	 * This method will add a role to the storage object. Typically, this will
	 * cause the storage object to save the role to whatever medium that is being
	 * used by the storage object.
	 *
	 * NOTE:
	 * {@link M_Acl} will call this method, when a role is added to the ACL. See
	 * {@link M_Acl::addRole()} for more information.
	 *
	 * @access public
	 * @param MI_AclRole $role
	 * @return void
	 */
	abstract public function addRole(MI_AclRole $role);

	/**
	 * Add a resource
	 *
	 * This method will add a resource to the storage object. Typically, this will
	 * cause the storage object to save the resource to whatever medium that is
	 * being used by the storage object.
	 *
	 * NOTE:
	 * {@link M_Acl} will call this method, when a resource is added to the ACL.
	 * See {@link M_Acl::addResource()} for more information.
	 *
	 * @access public
	 * @param MI_AclResource $resource
	 * @return void
	 */
	abstract public function addResource(MI_AclResource $resource);

	/**
	 * Add a permission
	 *
	 * This method will add a permission to the storage object. Typically, this
	 * will cause the storage object to save the permission to whatever medium
	 * that is being used by the storage object.
	 *
	 * NOTE:
	 * {@link M_Acl} will call this method, when a permission is added to the ACL.
	 * See {@link M_Acl::addPermission()} for more information.
	 *
	 * @access public
	 * @param MI_AclPermission $permission
	 * @return void
	 */
	abstract public function addPermission(MI_AclPermission $permission);

	/**
	 * Add rule
	 *
	 * This method will add a rule to the storage object. Typically, this
	 * will cause the storage object to save the rule to whatever medium
	 * that is being used by the storage object (eg. database).
	 *
	 * As with {@link M_Acl::addAllowRule()} and {@link M_Acl::addDenyRule()},
	 * you may provide a NULL value to indicate application to all roles,
	 * resources, and/or permissions.
	 *
	 * @access public
	 * @param string $type
	 * 		The type of rule: {@link M_Acl::RULE_ALLOW} or {@link M_Acl::RULE_DENY}
	 * @param MI_AclRole $role
	 * 		The role(s) onto which you wish to apply the rule.
	 * @param MI_AclResource $resource
	 * 		The resource(s), if any, for which you wish to apply the rule
	 * @param MI_AclPermission $permission
	 * 		The permission(s), if any, for which you wish to apply the rule
	 * @param MI_AclAssertion $assert
	 * 		Additional conditions to be taken into account when applying the rule
	 * @return M_Acl $acl
	 * 		Returns the instance of M_Acl, for a fluent programming interface
	 */
	abstract public function addRule($type, MI_AclRole $role, MI_AclResource $resource = NULL, MI_AclPermission $permission = NULL);

	/**
	 * Remove all
	 *
	 * Will completely clear the ACL Storage object, by removing all of the
	 * elements mentioned below:
	 *
	 * - Roles
	 * - Resources
	 * - Permissions
	 *
	 * @access public
	 * @return void
	 */
	abstract public function removeAll();

	/**
	 * Remove role
	 *
	 * Will remove a role from the ACL Storage Object. Typically, this will cause
	 * the storage object to remove the role from whatever medium (e.g. database)
	 * that is being used by the storage object.
	 *
	 * @access public
	 * @param MI_AclRole $role
	 * 		The role to be removed from the storage object
	 * @return void
	 */
	abstract public function removeRole(MI_AclRole $role);

	/**
	 * Remove resource
	 *
	 * Will remove a resource from the ACL Storage Object. Typically, this will
	 * cause the storage object to remove the resource from whatever medium
	 * (e.g. database) that is being used by the storage object.
	 *
	 * @access public
	 * @param MI_AclResource $resource
	 * 		The resource to be removed from the storage object
	 * @return void
	 */
	abstract public function removeResource(MI_AclResource $resource);

	/**
	 * Remove permission
	 *
	 * Will remove a permission from the ACL Storage Object. Typically, this will
	 * cause the storage object to remove the permission from whatever medium
	 * (e.g. database) that is being used by the storage object.
	 *
	 * @access public
	 * @param MI_AclPermission $permission
	 * 		The permission to be removed from the storage object
	 * @return void
	 */
	abstract public function removePermission(MI_AclPermission $permission);

	/**
	 * Remove (all) rules
	 *
	 * Will remove all rules from the ACL Storage Object. Typically, this will
	 * cause the storage object to remove the rules from whatever medium
	 * (e.g. database) that is being used by the storage object.
	 *
	 * In order to remove a specific rule from the ACL Storage Object, you should
	 * use {@link M_AclStorage::removeRule()} instead.
	 *
	 * @access public
	 * @return void
	 */
	abstract public function removeRules();

	/**
	 * Remove rule
	 *
	 * Will remove a specific rule from the ACL Storage Object. As with
	 * {@link M_Acl::addAllowRule()} and {@link M_Acl::addDenyRule()},
	 * you may provide a NULL value to indicate application to all roles,
	 * resources, and/or permissions.
	 *
	 * @access public
	 * @param integer $type
	 * 		The type of rule: {@link M_Acl::RULE_ALLOW} or {@link M_Acl::RULE_DENY}
	 * @param M_AclRole|string $role
	 * 		The role(s) to be removed
	 * @param M_AclResource|string $resource
	 * 		The resource(s) to be removed
	 * @param M_AclPermission|string $permission
	 * 		The permission(s) to be removed
	 * @return void
	 */
	abstract public function removeRule($type, $role, $resource = NULL, $permission = NULL);
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get ACL
	 * 
	 * Will construct an ACL, and add the ACL Objects to it:
	 * 
	 * - Roles: {@link MI_AclRole}
	 * - Resources: {@link MI_AclResource}
	 * - Permissions: {@link MI_AclPermission}
	 * 
	 * @access protected
	 * @return M_Acl
	 */
	protected function _getAclWithObjects() {
		// Construct a new ACL instance
		$acl = M_Acl::getInstance();
		
		// For each of the roles in the storage object:
		/* @var $role MI_AclRole */
		foreach($this->getRoles() as $role) {
			// Add the role to the ACL:
			$acl->addRole($role, $role->getParentRole());
		}
		
		// For each of the resources in the storage object:
		/* @var $resource MI_AclResource */
		foreach($this->getResources() as $resource) {
			// Add the resource to the ACL:
			$acl->addResource($resource, $resource->getParentResource());
		}
		
		// For each of the permissions in the storage object:
		/* @var $permission MI_AclPermission */
		foreach($this->getPermissions() as $permission) {
			// Add the resource to the ACL:
			$acl->addPermission($permission);
		}
		
		// Return the ACL:
		return $acl;
	}
}