<?php
/**
 * M_AclResource
 * 
 * ... The default implementation of {@link MI_AclResource}.
 * 
 * @package Core
 */
class M_AclResource extends M_AclObject implements MI_AclResource {
	
	/* -- PROPERTIES -- */
	
	/**
	 * ID
	 * 
	 * Stores the ID of the resource.
	 * 
	 * @access private
	 * @var string
	 */
	private $_id;
	
	/**
	 * Parent
	 * 
	 * Stores the parent object of the resource.
	 * 
	 * @see M_AclResource::getParentResource()
	 * @access private
	 * @var MI_AclResource
	 */
	private $_parent;

	/**
	 * This resources has custom permissions
	 * 
	 * @var M_ArrayIterator
	 */
	private $_permissions;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $id
	 * 		The ID of the resource
	 * @param MI_AclResource $parent
	 * 		To parent resource of the object
	 * @return M_AclResource
	 */
	public function __construct($id, MI_AclResource $parent = NULL) {
		$this->_id = (string) $id;
		$this->_parent = $parent;
	}

	/**
	 * Get the id of the module
	 *
	 * The code of the resource is a concatenation of the moduleId and the
	 * dataObjectId
	 *
	 * E.g. For resource-code "pages-text" the moduleId is "pages" and the
	 * dataObjectId is "text".
	 *
	 * @see getDataObjectId()
	 * @return string
	 */
	public function getModuleId() {
		return M_Helper::getArrayElement(0, explode('-',$this->getResourceId()));
	}

	/**
	 * Get the id of the dataObject
	 *
	 * The code of the resource is a concatenation of the moduleId and the
	 * dataObjectId
	 *
	 * E.g. For resource-code "pages-text" the moduleId is "pages" and the
	 * dataObjectId is "text".
	 *
	 * @see getModuleId()
	 * @return string
	 */
	public function getDataObjectId() {
		return M_Helper::getArrayElement(1, explode('-',$this->getResourceId()));
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get ID
	 *
	 * @access public
	 * @return string
	 */
	public function getResourceId() {
		return $this->_id;
	}
	
	/**
	 * Get parent
	 * 
	 * In an ACL, you may define a hierarchical structure of resources. This way,
	 * you can apply rules by inheritance to a defined set of resources, without 
	 * having to apply the rule to each of the resources separately.
	 * 
	 * This method will provide with the parent resource, if any. Will return NULL
	 * instead, if no parent resource exists for this role.
	 * 
	 * @access public
	 * @return MI_AclResource $resource
	 * 		The parent resource
	 */
	public function getParentResource() {
		return $this->_parent;
	}


	/**
	 * Bind resources to this permission
	 *
	 * @see getPermissions()
	 * @param M_ArrayIterator $permissions
	 * @return M_AclResource
	 * @author Ben Brughmans
	 */
	public function setPermissions(M_ArrayIterator $permissions) {
		$this->_permissions = $permissions;
		return $this;
	}

	/**
	 * Get the permissions for a resource
	 *
	 * Custom permissions apply mostly to a specific resource. If this is the
	 * case, the permissions for one resource can be set through this setter.
	 *
	 * If no permissions are set, we assume all permissions apply.
	 *
	 * @return M_ArrayIterator
	 */
	public function getPermissions() {
		if (is_null($this->_permissions)) return new M_ArrayIterator(array());
		return $this->_permissions;
	}

	/**
	 * Add a permission to this resource
	 *
	 * If this resource has custom permission, we can add a permission using
	 * this method
	 *
	 * @see getPermissions()
	 * @param M_AclPermission $permission
	 * @return M_AclResource
	 */
	public function addPermission(M_AclPermission $permission) {
		$p = $this->getPermissions();
		$p->append($permission);
		$this->setPermissions($p);
		return $this;
	}
}