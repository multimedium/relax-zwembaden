<?php
/**
 * MI_AclRole interface
 * 
 * This interface describes the public API that must be implemented by all 
 * roles that are added to the ACL.
 * 
 * @package Core
 */
interface MI_AclRole {
	/**
	 * Get ID
	 *
	 * @access public
	 * @return string
	 */
	public function getRoleId();
	
	/**
	 * Get parent
	 * 
	 * In an ACL, you may define a hierarchical structure of roles. This way,
	 * you can apply rules by inheritance to a defined set of roles, without 
	 * having to apply the rule to each of the roles separately.
	 * 
	 * This method will provide with the parent role, if any. Will return NULL
	 * instead, if no parent role exists for this role.
	 * 
	 * @access public
	 * @return MI_AclRole $role
	 * 		The parent role
	 */
	public function getParentRole();
	
	/**
	 * Get title
	 * 
	 * This method will provide with the title of the instance. Typically, this
	 * title is used for display. A title can be assigned to the instance with 
	 * {@link MI_AclRole::setTitle()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getTitle();
	
	/**
	 * Get description
	 * 
	 * This method will provide with the description of the instance. Typically, 
	 * this description is used for display. A description can be assigned to the 
	 * instance with {@link MI_AclRole::setDescription()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription();
	
	/**
	 * Set title
	 * 
	 * @see MI_AclRole::getTitle()
	 * @access public
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title);
	
	/**
	 * Set description
	 * 
	 * @see MI_AclRole::getDescription()
	 * @access public
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description);
}