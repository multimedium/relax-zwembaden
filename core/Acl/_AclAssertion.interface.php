<?php
/**
 * MI_AclAssertion interface
 * 
 * Sometimes a rule for allowing or denying a role access to a resource should 
 * not be absolute but dependent upon various criteria. For example, suppose that 
 * certain access should be allowed, but only between the hours of 8:00am and 5:00pm.
 * 
 * Another example would be denying access because a request comes from an IP 
 * address that has been flagged as a source of abuse. {@link M_Acl} has built-in 
 * support for implementing rules based on whatever conditions the developer 
 * needs.
 * 
 * {@link M_Acl} provides support for conditional rules with {@link MI_AclAssertion}.
 * In order to use the rule assertion interface, a developer writes a class that 
 * implements the assert() method of this interface.
 * 
 * @package Core
 */
interface MI_AclAssertion {
	/**
	 * Assert
	 * 
	 * This method is to be implemented, in order to state a fact or belief 
	 * confidently and forcefully that a given condition is being met.
	 * 
	 * In other words, this method is to be implemented by all {@link MI_AclAssertion}
	 * classes, in order to evaluate whether or not a given condition is being met.
	 * This method should return TRUE (in order to indicate that the condition
	 * is being met) or FALSE (if not being met).
	 * 
	 * @access public
	 * @param M_AclRole $role
	 * 		The ACL Role for which the condition is being evaluated. For more 
	 * 		information, read {@link M_AclRole}.
	 * @param M_AclResource $resource
	 * 		The ACL Resource for which the condition is being evaluated. For more 
	 * 		information, read {@link M_AclResource}.
	 * @param string $permission
	 * 		The permission string for which the condition is being evaluated.
	 * @return bool $flag
	 * 		This method should return TRUE (in order to indicate that the condition
	 * 		is being met) or FALSE (if not being met).
	 */
	public function assert(M_Acl $acl, M_AclRole $role = NULL, M_AclResource $resource = NULL, M_AclPermission $permission = NULL);
}