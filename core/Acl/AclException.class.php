<?php
/**
 * M_AclException
 * 
 * Used to throw exceptions in {@link M_Acl} and related classes
 * 
 * @package Core
 */
class M_AclException extends M_Exception {
}