<?php
/**
 * M_AclRole
 * 
 * ... The default implementation of {@link MI_AclRole}.
 * 
 * @package Core
 */
class M_AclRole extends M_AclObject implements MI_AclRole {
	
	/* -- PROPERTIES -- */
	
	/**
	 * ID
	 * 
	 * Stores the ID of the role.
	 * 
	 * @access private
	 * @var string
	 */
	private $_id;
	
	/**
	 * Parent
	 * 
	 * Stores the parent object of the role.
	 * 
	 * @see M_AclRole::getParentRole()
	 * @access private
	 * @var MI_AclRole
	 */
	private $_parent;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $id
	 * 		The ID of the role
	 * @param MI_AclRole $parent
	 * 		To parent role of the object
	 * @return M_AclResource
	 */
	public function __construct($id, MI_AclRole $parent = NULL) {
		$this->_id = (string) $id;
		$this->_parent = $parent;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get ID
	 *
	 * @access public
	 * @return string
	 */
	public function getRoleId() {
		return $this->_id;
	}
	
	/**
	 * Get parent
	 * 
	 * In an ACL, you may define a hierarchical structure of roles. This way,
	 * you can apply rules by inheritance to a defined set of roles, without 
	 * having to apply the rule to each of the roles separately.
	 * 
	 * This method will provide with the parent role, if any. Will return NULL
	 * instead, if no parent role exists.
	 * 
	 * @access public
	 * @return MI_AclRole $role
	 * 		The parent role
	 */
	public function getParentRole() {
		return $this->_parent;
	}

	/**
	 * Get the status of a rule
	 *
	 * Note: this will not tell you if a certain role has access to a resource
	 * (for a permission). Instead it will check if a specific rule has been
	 * set for this combination
	 *
	 * For more information please see {@link M_Acl::getRuleStatus()}
	 * 
	 * @param M_AclResource|string $resource
	 * @param M_AclPermission|string $permission
	 * @see M_Acl::getRuleStatus()
	 * @author Ben Brughmans
	 */
	public function getRuleStatus($resource = null, $permission = null) {
		return M_Acl::getInstance()->getRuleStatus($this, $resource, $permission);
	}
}