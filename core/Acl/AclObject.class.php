<?php
/**
 * M_AclObject
 * 
 * Is the abstract implementation of an ACL Object, and is motherclass to:
 * 
 * - {@link M_AclRole}
 * - {@link M_AclResource}
 * - {@link M_AclPermission}
 * 
 * @package Core
 */
abstract class M_AclObject extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Title
	 * 
	 * Stores the title of the object.
	 * 
	 * @see M_AclObject::getTitle()
	 * @see M_AclObject::setTitle()
	 * @access private
	 * @var string
	 */
	private $_title;
	
	/**
	 * Description
	 * 
	 * Stores the description of the object.
	 * 
	 * @see M_AclObject::getDescription()
	 * @see M_AclObject::setDescription()
	 * @access private
	 * @var string
	 */
	private $_description;
	
	/* -- SETTERS -- */
	
	/**
	 * Get title
	 * 
	 * This method will provide with the title of the instance. Typically, this
	 * title is used for display. A title can be assigned to the instance with 
	 * {@link M_AclObject::setTitle()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * Get description
	 * 
	 * This method will provide with the description of the instance. Typically, 
	 * this description is used for display. A description can be assigned to the
	 * instance with {@link M_AclObject::setDescription()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set title
	 * 
	 * @see M_AclObject::getTitle()
	 * @access public
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	/**
	 * Set description
	 * 
	 * @see M_AclObject::getDescription()
	 * @access public
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
	}
}