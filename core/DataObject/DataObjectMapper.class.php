<?php
/*
 * @todo document goal and specifications of DataObjectMapper
 */
abstract class M_DataObjectMapper {
	
	/**
	 * File and object suffix
	 * 
	 * This suffix is used to construct the filename and objectname for every
	 * mapper used. The mapper classname consists of the object name appended
	 * with the suffix constant
	 * 
	 * @var str
	 */
	const MAPPER_SUFFIX = 'Mapper';
	
	/**
	 * Timezone
	 * 
	 * Stores the timezone of the mapper, set by
	 * {@link DataObjectMapper::setTimezone()}
	 * 
	 * This property can ensure that dates are stored in the correct timezone
	 * If the property is empty, dates will be saved in the default timezone
	 * of the application
	 * 
	 * @access private
	 * @var DateTimeZone
	 */
	private static $_timezone;
	
	/**
	 * Set Timezone
	 * 
	 * Sets the timezone that is used by the mapper for dates
	 * 
	 * @author Wim Van De Mierop
	 * @see M_DataObjectMapper::getTimezone()
	 * @static
	 * @access public
	 * @param string $timezoneIdentifier
	 * @return void
	 */
	public static function setTimezone($timezoneIdentifier) {
		// Store internal reference
		self::$_timezone = (string) $timezoneIdentifier;
	}
	
	/**
	 * Get Timezone
	 * 
	 * Gets the timezone that is used by the mapper for date/time values
	 * 
	 * @author Wim Van De Mierop
	 * @see M_DataObjectMapper::setTimezone()
	 * @static
	 * @access public
	 * @return string
	 */
	public static function getTimezone() {
		// Store internal reference
		return self::$_timezone;
	}
	
	/**
	 * @var MI_Db
	 */
	protected $db;
	
	/**
	 * Xml object which contains the data-definition for the data-object
	 * 
	 * @var SimpleXmlElement
	 */
	protected $_xml;
	
	/**
	 * Database table-object
	 *
	 * @var MI_DbTable
	 */
	protected $_table;
	
	/**
	 * Database table-object for localized texts
	 *
	 * @var MI_DbTable
	 */
	protected $_tableLocalizedTexts;
	
	/**
	 * The classname from the data-object with which this mapper interacts
	 * 
	 * @var str
	 */
	protected $_dataObjectClassName;

	/**
	 * The filters which need to be applied before we execute the query
	 *
	 * @var array
	 */
	protected $_filters = array();
	
	/**
	 * The active locale
	 * 
	 * @var string
	 * @see M_DataObjectMapper::getLocale()
	 * @see M_DataObjectMapper::setLocale()
	 * @see M_DataObjectMapper::resetLocale()
	 * @author Ben Brughmans
	 */
	protected $_locale;
	
	/**
	 * Store all localized joins
	 * 
	 * This attribute is used to store all localized joins for all 
	 * {@link M_DbQuery} objects which are being processed by this mapper. We
	 * need to store these joins so we know they are allready being added to 
	 * the query
	 * 
	 * @see _addLocalizedFieldJoin()
	 * @param array
	 */
	private $_localizedJoins = array();

	/**
	 * Union
	 *
	 * This attribute is used to store the union with another data object mapper.
	 * For more information about unions between data object mappers, read the
	 * docs on 
	 * 
	 * - {@link M_DataObjectMapper::setUnion()}
	 * - {@link M_DataObjectMapper::addUnion()}
	 *
	 * @author Tom Bauwens
	 * @access private
	 * @see M_DataObjectMapper::setUnion()
	 * @see M_DataObjectMapper::addUnion()
	 * @var array
	 */
	private $_union = array();

	/**
	 * No results?
	 *
	 * @author Tom Bauwens
	 * @see M_DataObjectMapper::setFlagEmptyResultSet()
	 * @access private
	 * @var boolean
	 */
	private $_emptyResultSet = FALSE;
	
	/**
	 * Get the active locale
	 * 
	 * The mapper will join localized texts in function of this locale. If no
	 * locale has been set the application locale will be used
	 *
	 * @return string
	 * @author Ben Brughmans
	 */
	public function getLocale() {
		if (is_null($this->_locale)) {
			return M_Locale::getCategory(M_Locale::LANG);
		}
		
		return $this->_locale;
	}
	
	/**
	 * Set the locale for this mapper
	 * 
	 * Note: this locale does only affect the data retrieved via this mapper.
	 * More specificcally it will force 
	 * {@link M_DataObjectMapper::_addLocalizedFieldJoin} as well as any other
	 * method which is locale sensitive to use this locale. Furthermore also
	 * {@link M_DbQueryFilter} will use this locale
	 *
	 * @param string $locale
	 * @return void
	 * @author Ben Brughmans
	 */
	public function setLocale($locale) {
		$this->_locale = $locale;
	}
	
	/**
	 * Reset the locale
	 * 
	 * After resetting the locale for this mapper, the default application
	 * locale will be used again.
	 * 
	 * Note: this locale does only affect the data retrieved via this mapper.
	 * More specificcally it will force 
	 * {@link M_DataObjectMapper::_addLocalizedFieldJoin} to use this locale
	 * 
	 * @return string Active locale after reset
	 * @author Ben Brughmans
	 */
	public function resetLocale() {
		$this->_locale = null;
		return $this->getLocale();
	}
	
	/**
	 * Construct
	 *
	 * @param MI_Db $db
	 */	
	public function __construct( MI_Db $db = NULL)
	{
		$this->db = $db;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get the database object
	 *
	 * @return MI_Db
	 */
	public function getDb() {
		if($this->db) {
			return $this->db;
		} else {
			return M_Db::getInstance();
		}
	}
	
	/**
	 * Get the query-filters
	 *
	 * @return array
	 */
	public function getFilters()
	{
		return $this->_filters;
	}
	
	/**
	 * Get the database fields for this object
	 * 
	 * @return array $fields Array containing SimpleXMLElements
	 */
	public function getFields()
	{
		return $this->_getXml()->fields->children();
	}
	
	/**
	 * Get one field element for this object
	 *
	 * The field where node "name" = $field will be retreived, if none found
	 * it will return false
	 * 
	 * @param str $field
	 * @return str field node
	 * @example 
	 * 
	 * <field>
	 *		<name></name>
	 *		<db_name></db_name>
	 *		<db_type></db_type>
	 *		<length></length>
	 *		<getter></getter>
	 *		<setter></setter>
	 *	 </field>
	 */
	public function getField($fieldName)
	{
		foreach ($this->getFields() AS $field)
		{
			if (strcmp($fieldName, $field->name) == 0) return $field;
		}
		
		return false;
	}

	/**
	 * Get one field element for this object, by db_name
	 *
	 * @author Tom Bauwens
	 * @access public
	 * @return SimpleXmlElement $field
	 *		The requested field, or (boolean) FALSE if not found
	 */
	public function getFieldByDbName($dbFieldName) {
		foreach ($this->getFields() AS $field) {
			if(strcmp($dbFieldName, $field->db_name) == 0) {
				return $field;
			}
		}
		return false;
	}
	
	/**
	 * Get the db_name property for a field element
	 *
	 * The field where node "name" = $field will be retreived, if none found
	 * it will return false. 
	 * 
	 * After retreiving this node, we will return the db_name property
	 * 
	 * @param str $field
	 * @return str $dbName
	 */
	public function getFieldDbName($fieldName)
	{
		$field = $this->getField($fieldName);
		
		//field found
		if ($field)	return (string)$field->db_name;
		//field not found
		else return $fieldName;
	}
	
	/**
	 * TEMP-TOM
	 * Get localized database fields for this object
	 * 
	 * @return array $fields
	 * 		Array containing SimpleXMLElement objects
	 */
	public function getLocalizedFields() {
		$out = array();
		foreach($this->_getXml()->fields->children() as $child) {
			if($this->isLocalizedField($child)) {
				$out[] = $child;
			}
		}
		return $out;
	}
	
	/**
	 * Get the table name for this object mapper
	 * 
	 * @return str
	 */
	public function getTableName() {
		return (string)$this->_getXml()->table;
	}

	/**
	 * Get the table engine for this object mapper
	 *
	 * @return str
	 */
	public function getTableEngine() {
		return (string)$this->_getXml()->tableEngine;
	}
	
	/**
	 * Get the module for this object mapper
	 * 
	 * @return M_ApplicationModule
	 */
	public function getModule() {
		return new M_ApplicationModule($this->_getModuleName());
	}
	
	/**
	 * Is localized?
	 * 
	 * Will tell whether or not the data objects that are being managed by the 
	 * mapper contain localized fields.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if localized, FALSE if not
	 */
	public function isLocalized() {
		// For each of the fields:
		foreach($this->getFields() as $field) {
			// If the field is localized:
			if($this->isLocalizedField($field)) {
				// We stop looping the fields, and return TRUE as result:
				return TRUE;
			}
		}
		
		// Return FALSE, if still here:
		return FALSE;
	}
	
	/**
	 * Is field localized?
	 * 
	 * Will tell whether or not the given field is a localized text field
	 * 
	 * @access public
	 * @param SimpleXMLElement $field
	 * 		The Field
	 * @return bool $flag
	 * 		Returns TRUE if localized, FALSE if not
	 */
	public function isLocalizedField(SimpleXMLElement $field) {
		return (isset($field['localized']) && strtolower($field['localized']) == 'true');
	}
	
	/**
	 * Is field localized?
	 * 
	 * Will tell whether or not the given field (string ID) is a localized text
	 * field
	 * 
	 * @access public
	 * @param string $field
	 * 		The Field ID
	 * @return bool $flag
	 * 		Returns TRUE if localized, FALSE if not
	 */
	public function isLocalizedFieldId($fieldId) {
		$field = $this->getField($fieldId);
		if($field) {
			return $this->isLocalizedField($field);
		} else {
			return NULL;
		}
	}
	
	/**
	 * Is a NULL value for this field allowed?
	 *
	 * Will tell whether or not the given field (stringID) allows NULL values
	 * 
	 * @author b.brughmans
	 * @param SimpleXMLElement $field
	 * @return bool
	 */
	public function isNullFieldId($fieldId) {
		$field = $this->getField($fieldId);
		if ($field) {
			return $this->isNullField($field);
		}else {
			return null;
		}
	}
	
	/**
	 * Is a NULL value for this field allowed?
	 *
	 * @author b.brughmans
	 * @param SimpleXMLElement $field
	 * @return bool
	 */
	public function isNullField(SimpleXMLElement $field) {
		return (isset($field['null']) && strtolower($field['null']) == 'true');
	}
	
	/**
	 * Get available locales
	 * 
	 * Will provide with the collection of locales for which the data object has
	 * stored localized texts.
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @param M_DataObject $object
	 * 		The data object for which to check the available locales
	 * @return M_ArrayIterator $iterator
	 * 		A collection of locales (strings, as provided by the method
	 * 		{@link M_LocaleMessageCatalog::getInstalled()}
	 */
	public function getLocalesOfLocalizedTexts(M_DataObject $object) {
		return $object->getLocalesOfLocalizedTexts();
	}
	
	/**
	 * Get the object by it's id
	 * 
	 * @return object
	 */
	public function getById($arg) {
		// Compose the SELECT statement:
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return FALSE
			return FALSE;
		}
		
		// Return the result:
		return $this->_fetchOne(
			$select
				->where($this->getTableName() . '.' . $this->_getPrimaryKey()->db_name . ' = ?', (int) $arg)
				->execute()
		);
	}
	
	/**
	 * Return an array containing only the id's (primary keys)
	 *
	 * @return M_ArrayIterator
	 */
	public function getIds() {
		// Compose the SELECT statement:
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return an empty collection
			return new M_ArrayIterator(array());
		}
		
		// Return the results
		return new M_ArrayIterator(
			$select
				->columns(array($this->getTableName() . '.' . $this->_getPrimaryKey()->db_name))
				->execute()
				->getColumnValues(0)
		);
	}
	
	/**
	 * Get multiple objects by their id
	 *
	 * @param array $arg
	 * @example 
	 * <?php
	 * $ids = array(88,3,43,12);
	 * $mapper->getByIds($ids);
	 * ?>
	 * @return M_ArrayIterator
	 */
	public function getByIds(array $arg) {
		// Compose the SELECT statement
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return an empty collection
			return new M_ArrayIterator(array());
		}
		
		// Return the results
		return $this->_fetchAll(
			$select
				->whereIn($this->getTableName() . '.' . $this->_getPrimaryKey()->db_name, $arg)
				->execute()
		);
	}
	
	/**
	 * Get object(s) by a certain field
	 *
	 * @param str $fieldName
	 * @param mixed $fieldValue
	 * @return M_ArrayIterator
	 */
	public function getByField($fieldName, $fieldValue) {
		// Compose the SELECT statement
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return an empty collection
			return new M_ArrayIterator(array());
		}
		
		// Add the where-clause to the select statement:
		$this->_addWhere($select,$fieldName,$fieldValue);

		// And execute:
		return $this->_fetchAll($select->execute());
	}
	
	/**
	 * Get objects by multiple fields
	 * 
	 * Example 1
	 * <code>
	 *    $mapper->getByFields(array(
	 *       'fieldName1' => 'fieldValue1',
	 *       'fieldName2' => 'fieldValue2'
	 *    ));
	 * </code>
	 *
	 * @access public
	 * @param array $values
	 * 		The values to fetch objects with
	 * @return M_ArrayIterator
	 */
	public function getByFields(array $values) {
		// Construct a select statement:
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return an empty collection
			return new M_ArrayIterator(array());
		}
		
		// For each of the fields:
		foreach($values as $fieldName => $fieldValue) {
			// Add a where-clause for the field:
			$this->_addWhere($select, $fieldName, $fieldValue);
		}
		
		// Return the collection of objects:
		return $this->_fetchAll($select->execute());
	}
	
	/**
	 * Add a WHERE condition for a field to a query. It automatically checks
	 * if this field is localized and will do the necessary things in order
	 * to filter on the localized-value
	 * 
	 * @param M_DbQuery $query
	 * @param string $fieldName
	 * @param mixed $fieldValue
	 * @return M_DbQuery $query
	 */
	private function _addWhere($query, $fieldName, $fieldValue) {
		// add the condition to the select statement: we first try to add
		// a localized join, this will return the localized-dbname to query
		// on.
		$localizedFieldName = $this->_addLocalizedFieldJoin($query,$fieldName);
		if ($localizedFieldName !== false) $dbName = $localizedFieldName;
		//If it isn't localized we simply filter on the original dbname
		else $dbName = $this->getFieldDbName($fieldName);
		
		//add the condition
		if(is_array($fieldValue)) {
		    $query->whereIn($dbName, $fieldValue);
		} elseif(is_null($fieldValue)) {
    		$query->where($dbName.' IS NULL');
		} else {
    		$query->where($dbName.' = ?', $fieldValue);
		}
		
		return $query;
	}
	
	/**
	 * Returns the number of records present in the database for the active object
	 *
	 * @return int
	 */
	public function getCount() {
		// Get the select statement, with all filters applied to it, and make a
		// SELECT COUNT() statement out of it:
		$select = $this->_getSelectCount($this->_getFilteredSelect());
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return 0 (ZERO)
			return 0;
		}
		
		// For each of the selects in UNION:
		foreach($select->getUnions() as $union) {
			/* @var $union M_DbSelect */
			// Make a count() statement of the union query:
			$this->_getSelectCount($union);
		}
		
		// total count
		$count = 0;

		// Add together all counts:
		foreach($select->execute() as $rs) {
			// Add to total
			$count += $rs[0];
		}

		// return count
		return $count;
	}

	/**
	 * Compose SELECT COUNT() statement
	 *
	 * @access public
	 * @param MI_DbSelect $select
	 * @return MI_DbSelect $select
	 */
	protected function _getSelectCount(MI_DbSelect $select) {
		// If the select statement has a GROUP BY statement:
		if($select->hasGroup()) {
			// Then, we count with that selection:
			$select->columnsLiteral($select->count(str_replace('GROUP BY', 'DISTINCT', $select->getGroup())));
			$select->resetGroup();
		}
		// If not
		else {
			// Then, we use the COUNT(0) statement:
			$select->columnsLiteral('COUNT(0)');
		}

		//if we are counting, and we have only one result: order never
		//matters... it can only slow down the query that's why we reset it
		$select->resetOrder();

		// If a limit has been applied to the select:
		if($select->getLimitCount()) {
			// @todo: WIE WIL DIT OPTIMALISEREN? :)
			// Then, we need to fetch all records in order to make the sum
			// ourselves (for now)
			$select->group($this->getFieldDbName('id'));
		}

		// Return the select
		return $select;
	}
	
	/**
	 * Returns the maximum value of a field
	 * 
	 * @access public
	 * @param string $field
	 * 		The field of which to get the maximum value, in the database
	 * @return mixed
	 */
	public function getMaximum($field) {
		// Compose the SELECT statement
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return NULL
			return NULL;
		}
		
		// Get the maximum
		$rs = $select
			->columnsLiteral('MAX( ' . $this->getDb()->quoteIdentifier($this->getFieldDbName($field)) . ' )')
			->execute();

		// If a result has been found
		if($rs && count($rs) == 1) {
			// then, return the value
			return $rs->getFirstValue();
		}

		// Return NULL, if still here
		return NULL;
	}
	
	/**
	 * Get the latest items
	 *
	 * @param int $limit default this is 1
	 * @return M_ArrayIterator
	 */
	public function getLatest($limit = 1) {
		// Compose the SELECT statement
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return an empty collection
			return new M_ArrayIterator(array());
		}
		
		// Add conditions to the query
		$select
			// To limit the collection
			->limit(0,(int)$limit)
			// To sorty by the ID
			->order($this->getTableName().'.'.$this->getFieldDbName('id'),'desc');

		// Get the objects:
		return $this->_fetchAll($select->execute());
	}
	
	/**
	 * Get random items
	 * 
	 * @access public
	 * @param integer $count
	 * 		THe number of items to be fetched randomly
	 * @return M_ArrayIterator
	 */
	public function getRandom($count = 1) {
		// Compose the SELECT statement
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return an empty collection
			return new M_ArrayIterator(array());
		}

		// Run the SELECT statement, and return the objects:
		return $this->_fetchAll(
			$select
				->limit(0,(int) $count)
				->orderRandom()
				->execute()
		);
	}
	
	/**
	 * Get random object
	 * 
	 * @access public
	 * @return M_DataObject
	 */
	public function getRandomObject() {
		// Compose the SELECT statement
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return FALSE
			return FALSE;
		}

		// Run the SELECT, and return the object:
		return $this->_fetchOne(
			$select
				->limit(0, 1)
				->orderRandom()
				->execute()
		);
	}
	
	/**
	 * Get all items
	 * 
	 * @access public
	 * @return M_DataObjectRecordset
	 */
	public function getAll() {
		// Compose the SELECT statement:
		$select = $this->_getFilteredSelect();
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return an empty collection
			return new M_ArrayIterator(array());
		}
		
		// Return the objects:
		return $this->_fetchAll($select->execute());
	}

	/**
	 * Get a limited amount of items
	 * 
	 * @param int $numberofRows
	 * @param int $offset
	 * @return M_DataObjectRecordset
	 */
	public function getLimited($numberofRows, $offset = null) {
		return $this->addFilter(new M_DbQueryFilterLimit($offset, $numberofRows))->getAll();
	}

	/**
	 * Get one item
	 * 
	 * @access public
	 * @return Object
	 */
	public function getOne() {
		// Compose the SELECT statement
		$select = $this->_getFilteredSelect(array(
			new M_DbQueryFilterLimit(0, 1)
		));
		
		// If an empty result set is forced:
		// (Note that we check this after having composed the query with filters.
		// While composing the query, subclasses may want to indicate an empty 
		// result set!)
		if($this->_emptyResultSet) {
			// Then, we return FALSE
			return FALSE;
		}

		// Return the object
		return $this->_fetchOne($select->execute());
	}
	
	/**
	 * Get one data object, by field value comparison
	 * 
	 * NOTE:
	 * Will return FALSE if the data object could not have been found
	 * 
	 * @param str $fieldName
	 * @param mixed $fieldValue
	 * @return M_DataObject
	 */
	public function getOneByFieldWithRegistryCache($fieldName, $fieldValue, $comparison = M_DbQueryFilterWhere::EQ, $binary = FALSE) {
		// If an empty result set is forced:
		if($this->_emptyResultSet) {
			// Then, we return FALSE
			return FALSE;
		}

		// Get the registry:
		$registry = M_Registry::getInstance();
		
		// We compose the key for the object:
		$key = $this->_getDataObjectClassName() . '-' . $fieldName . '-' . $fieldValue;
		
		// Check if the requested object has already been stored in the registry.
		// If not so:
		if(! isset($registry->$key)) {
			// Debugging
			// echo 'Not in cache, fetch with ' . $fieldName . ' = ' . $fieldValue . '<br />';
			
			// Get the result of the SELECT statement, with all of the filters 
			// applied to it:
			$rs = $this->_getFilteredSelect(
				// ... add filter to look up by code:
				new M_DbQueryFilterWhere((string) $fieldName, $fieldValue, $comparison, $binary)
			)
				// Only fetch one object:
				->limit(0, 1)
				// Execute:
				->execute();
			
			// If the result set contains the record we are looking for:
			if($rs && $rs->count() == 1) {
				// Return the object:
				$registry->$key = $this->_createObjectFromRecord($rs->current());
			}
			// If the result set is empty:
			else {
				$registry->$key = FALSE;
			}
		}
		// If the object has already been stored in registry
		else {
			// Debugging
			// echo 'From registry cache; field ' . $fieldName . ' = ' . $fieldValue . '<br />';
		}
		
		// We return the stored object:
		return $registry->$key;
	}
	
	/**
	 * Get new data object
	 * 
	 * @access public
	 * @return M_DataObject
	 */
	public function getNewDataObject() {
		$className = $this->_getDataObjectClassName();
		M_Loader::loadDataObject($className, $this->_getModuleName());
		return new $className;
	}
	
	/**
	 * Is new?
	 * 
	 * Returns TRUE if the object is not stored in database, FALSE if
	 * already stored in database
	 * 
	 * @param M_DataObject $object
	 * @return bool
	 */
	public function isNew($object) {
		$primaryKey = call_user_func(array(
			$object,
			(string) $this->_getPrimaryKey()->getter
		));
		
		return !((bool) $primaryKey);
	}
	
	/**
	 * Is updated?
	 * 
	 * Compares the object's data against the data that is stored in the
	 * database. Returns TRUE if the object has been updated, FALSE if
	 * not.
	 * 
	 * @param M_DataObject $object
	 * @return bool
	 */
	public function isUpdated($object) {
		return count($this->getUpdatedFieldsIn($object) > 0);
	}
	
	/**
	 * Is Valid Array Of ID's?
	 * 
	 * Will check whether or not the provided array of ID's can be considered as 
	 * a valid collection of Data Object ID's. Returns TRUE if so, FALSE if not.
	 * 
	 * @access public
	 * @param array $ids
	 * @return bool
	 */
	public function isValidArrayOfIds(array $ids) {
		// The provided array must contain at least one ID. If that is not the
		// case, then we return FALSE to indicate an invalid array of ID's:
		if(count($ids) == 0) {
			return FALSE;
		}
		
		// The array provided must be an array of integer values (greater than
		// zero). We will make sure that is the case, so we'll loop through the
		// array provided:
		foreach($ids as $id) {
			// The current ID must be an integer, greater than ZERO. If that is
			// not the case, we return FALSE to indicate an invalid array of 
			// ID's:
			if(! $this->isValidId($id)) {
				return FALSE;
			}
		}
		
		// Return TRUE if still here
		return TRUE;
	}
	
	/**
	 * Is Valid ID?
	 * 
	 * Will check for the given ID if it is a value database ID, returning
	 * TRUE if that is the case, or FALSE if not.
	 * 
	 * @access public
	 * @param int $id
	 * @return boolean
	 */
	public function isValidId($id) {
		// The ID must be an integer, greater than ZERO. If that is not the
		// case, we return FALSE to indicate an invalid ID. Otherwise we
		// return TRUE:
		return (M_DataType::isInteger($id) && $id > 0);
	}
	
	/**
	 * Get updated fields
	 *
	 * Will return an array with the updated fields in a given object.
	 * 
	 * @access public
	 * @return array $out
	 * 		The collection of fields, each of them represented by
	 * 		a SimpleXMLElement
	 */
	public function getUpdatedFieldsIn(M_DataObject $object) {
		// Array of update field names:
		$out = array();
		
		// If an ID is available for the object:
		if($object->getId()) {
			// Get the copy for this object from the database
			$dbObject = $this->getById($object->getId());
			
			// Compare every field of this object with the database data
			foreach ($this->getFields() AS $field) {
				// Get data in database:
				$getter = (string) $field->getter;
				if (!method_exists($dbObject, $getter)) {
					throw new M_Exception(sprintf(
						'Cannot call method "%s" on Object "%s"', 
						$getter, 
						get_class($object)
					));
				}
				$dbValue = call_user_func(array($dbObject, $getter));
				
				// Use the getter to get the object's data:
				$value = call_user_func(array($object, (string) $field->getter));
				
				// if there is a difference, the field has been updated!
				if(strcmp($dbValue, $value) !== 0) {
					// Add to output array:
					$out[] = $field;
				}
			}
		}
		
		// Return collection of updated fields:
		return $out;
	}
	
	/**
	 * TEMP-TOM
	 * Get localized texts, in all languages
	 * 
	 * @access public
	 * @param M_DataObject $object
	 * 		The object of which to set localized texts
	 * @return void
	 */
	public function setAllLocalizedTextsIn(M_DataObject $object) {
		// For each of the installed locales:
		foreach(M_LocaleMessageCatalog::getInstalled() as $locale) {
			// Set the localized texts in the current language
			$this->setLocalizedTextsIn($object, $locale);
		}
	}
	
	/**
	 * Get localized texts
	 * 
	 * @todo add additional comments
	 *
	 * @access public
	 * @param M_DataObject $object
	 * 		The object of which to set localized texts
	 * @param string $locale
	 * 		The language in which to set texts
	 * @return void
	 */
	public function setLocalizedTextsIn(M_DataObject $object, $locale = NULL) {
		// If no locale has been provided, we fetch the locale that
		// has been set in the locale category LC_MESSAGES
		if(! $locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
			if(! $locale) {
				// do not continue, if no locale can be found at all
				return;
			}
		}
		
		// Info of the fields of which we fetch the translations:
		$fields = array();

		// For each of the localized fields:
		foreach($this->getLocalizedFields() as $field) {
			// Get the numeric value. This value is the ID of the translated
			// text, which is stored in the auxiliary table
			$id = call_user_func(array($object, (string) $field->getter . 'Id'));
			
			// If the numeric value is available, we will have to fetch
			// the corresponding localized text. We store this value,
			// for later reference:
			if($id) {
				$fields[$id] = (string) $field->name;
			}
		}

		// If at least 1 localized field should be fetched from database:
		if(count($fields) > 0) {
			// Compose SELECT statement to fetch the localized fields
			$select = $this->_getLocalizedTextTable()->select();
			$select->columns(array('text_id', 'text_locale', 'text_value'));
			$select->whereIn('text_id', array_keys($fields));
			$select->where('text_locale = ?', $locale);
			
			// Run the select:
			$rs = $select->execute();

			// if the result set is valid, and it contains at least 
			// 1 record:
			if($rs !== FALSE && count($rs) > 0) {
				// for each of the records:
				foreach($rs as $row) {
					// Get the corresponding object property
					$property = $fields[$row['text_id']];

					//if this property is still empty, we set the value from the database
					if($object->getLocalizedProperty($property, $row['text_locale']) === null) {
						$object->setLocalizedProperty($property, $row['text_value'], $row['text_locale']);
					}
				}
			}
		}
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set the db-connection
	 *
	 * @param MI_Db $db
	 */
	public function setDb(MI_Db $db) {
		$this->db = $db;
	}
	
	/**
	 * Set the query-filters
	 *
	 * @param array
	 * @return void
	 */
	public function setFilters($filters) {
		$this->_filters = $filters;
	}
	
	/**
	 * Add one query filter
	 *
	 * @param M_DbQueryFilter $filter
	 * @return M_DataObjectMapper
	 */
	public function addFilter( M_DbQueryFilter $filter) {
		$this->_filters[] = $filter;
		return $this;
	}
	
	/**
	 * Reset the existing query filters
	 * 
	 * @return M_DataObjectMapper
	 */
	public function resetFilters() {
		$this->_filters = array();
        $this->_emptyResultSet = false;

		return $this;
	}

	/**
	 * Set flag: No Results
	 *
	 * This method can be used to set the flag "No Results". Once this flag is
	 * set (to TRUE), then the mapper's getter/fetcher methods, such as the following
	 * ones, will no longer return results:
	 *
	 * - {@link M_DataObjectMapper::getAll()}
	 * - {@link M_DataObjectMapper::getOne()}
	 * - {@link M_DataObjectMapper::getByField()}
	 * - {@link M_DataObjectMapper::getById()}
	 * - etc...
	 *
	 * NOTE:
	 * If this flag is set to TRUE, then no database query will be executed in
	 * order to fetch objects. In that case, an empty result set is forced by the
	 * data object mapper.
	 *
	 * @access public
	 * @return M_DataObjectMapper $mapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFlagEmptyResultSet($flag = TRUE) {
		// Set the boolean
		$this->_emptyResultSet = (bool) $flag;

		// Return myself
		return $this;
	}

	/**
	 * Get flag: No Results
	 *
	 * @see M_DataObjectMapper::setFlagEmptyResultSet()
	 * @access public
	 * @return M_DataObjectMapper $mapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function getFlagEmptyResultSet() {
		return $this->_emptyResultSet;
	}

	/**
	 * Set Union with Data Object Mapper
	 *
	 * Will set a union with another data object mapper. By creating such a union,
	 * you can fetch a collection of two different (types of) data objects in one
	 * single query.
	 *
	 * It is important to note that, when retrieving data objects with a union of
	 * two separate and different data object mappers, the result set (collection)
	 * will contain a mixture of data objects. The instantiated class for each
	 * object will depend on the origin of the corresponding data object.
	 *
	 * For example, if I were to execute the following code:
	 *
	 * <code>
	 *    // Construct the mapper of News instances
	 *    $newsMapper  = M_Loader::getDataObjectMapper('News', 'news');
	 *
	 *    // Construct the mapper of Press instances
	 *    $pressMapper = M_Loader::getDataObjectMapper('Press', 'press');
	 *
	 *    // Fetch both News and Press instances in a single request/query:
	 *    $newsMapper->setUnion($pressMapper);
	 *    $result = $newsMapper->getAll();
	 *
	 *    // Now the $result variable contains a collection of both News and
	 *    // Press objects:
	 *    foreach($result as $object) {
	 *       if(M_Helper::isInstanceOf($object, 'News')) {
	 *          // The current object is a News object
	 *       } else {
	 *          // The current object is a Press object
	 *       }
	 *    }
	 * </code>
	 * 
	 * NOTE:
	 * This method will set the union, and overwrite any union that may have been
	 * set previously. To add a union to the current selection of unions, please
	 * check {@link M_DataObjectMapper::addUnion()}
	 *
	 * @access public
	 * @param M_DataObjectMapper $mapper
	 * @return M_DataObjectMapper $mapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function setUnion(M_DataObjectMapper $mapper) {
		$this->_union = array($mapper);
		return $this;
	}
	
	/**
	 * ADD Union with Data Object Mapper
	 *
	 * Will add a union with another data object mapper. By creating such a union,
	 * you can fetch a collection of different (types of) data objects in one
	 * single query.
	 *
	 * It is important to note that, when retrieving data objects with a union of
	 * separate and different data object mappers, the result set (collection)
	 * will contain a mixture of data objects. The instantiated class for each
	 * object will depend on the origin of the corresponding data object.
	 *
	 * For example, if I were to execute the following code:
	 *
	 * <code>
	 *    // Construct the mapper of News instances
	 *    $newsMapper  = M_Loader::getDataObjectMapper('News', 'news');
	 *
	 *    // Construct the mapper of Press instances
	 *    $pressMapper = M_Loader::getDataObjectMapper('Press', 'press');
	 *
	 *    // Construct the mapper of Document instances
	 *    $documentMapper = M_Loader::getDataObjectMapper('Document', 'document');
	 *
	 *    // Fetch both News and Press instances in a single request/query:
	 *    $newsMapper
	 *       ->addUnion($pressMapper)
	 *       ->addUnion($documentMapper);
	 *    
	 *    $result = $newsMapper->getAll();
	 *
	 *    // Now the $result variable contains a collection of News, Press and 
	 *    // Document objects:
	 *    foreach($result as $object) {
	 *       if(M_Helper::isInstanceOf($object, 'News')) {
	 *          // The current object is a News object
	 *       } elseif(M_Helper::isInstanceOf($object, 'Press')) {
	 *          // The current object is a Press object
	 *       } elseif(M_Helper::isInstanceOf($object, 'Document')) {
	 *          // The current object is a Document object
	 *       }
	 *    }
	 * </code>
	 *
	 * @access public
	 * @param M_DataObjectMapper $mapper
	 * @return M_DataObjectMapper $mapper
	 *		Returns itself, for a fluent programming interface
	 */
	public function addUnion(M_DataObjectMapper $mapper) {
		array_push($this->_union, $mapper);
		return $this;
	}

	/**
	 * Get unions
	 *
	 * Will provide with the data object mappers that have been connected to this
	 * object previously, with {@link M_DataObjectMapper::setUnion()} and/or
	 * {@link M_DataObjectMapper::addUnion()}
	 *
	 * @access public
	 * @return M_ArrayIterator $unions
	 *		The collection of {@link M_DataObjectMapper} instances in the union
	 */
	public function getUnions() {
		return new M_ArrayIterator($this->_union);
	}
	
	/**
	 * Get Union, by index
	 * 
	 * @access public
	 * @return M_DataObjectMapper
	 */
	public function getUnion($index = 0) {
		return (array_key_exists($index, $this->_union) ? $this->_union[$index] : NULL);
	}
	
	/**
	 * Set the class-name for the dataObect with which this DataObjectMapper
	 * interacts
	 *
	 * @param str $arg
	 * @return bool
	 */
	public function _setDataObjectClassName( $arg ) {
		return $this->_dataObjectClassName = $arg;
	}
	
	/**
	 * Get ID of object
	 *
	 * @param M_DataObject $object
	 * @return integer $id
	 * 		The ID of the object
	 */
	public function getIdOf(M_DataObject $object) {
		return call_user_func(array(
			$object,
			(string) $this->_getPrimaryKey()->getter
		));
	}
	
	/**
	 * Update object
	 *
	 * @param M_DataObject $object
	 * @return bool
	 */
	public function update(M_DataObject $object) {
		// Dispatch an event, to notify that we are about to update the object:
		$this->_dispatchDataObjectEvent(M_DataObjectMapperEvent::DATA_OBJECT_BEFORE_UPDATE, $object);

		// Store the data in array
		$data = array();

		// Iterate through the fields:
		foreach($this->_getXml()->fields->children() AS $field) {
			// Get the database column name:
			$dbName = (string) $field->db_name;

			// store only when dbname is known
			if(!empty($dbName)) {
				// If the current field is a localized field:
				if($this->isLocalizedField($field)) {
					// Call the getterId, to get the current field's numeric
					// reference to the text.
					$id = call_user_func(array($object, (string) $field->getter . 'Id'));

					// We create a temporary array, in which we store information
					// about translated texts. This array will tell us about:
					$tmp = array(
						// The number of translated texts available, for the
						// current field:
						0,
						// The locales in which NO translated text is available:
						// (this will be an SQL condition, that is used to delete
						// localized text entries from the database)
						array('', $id)
					);

					// For each of the installed locales:
					$i = 0;
					foreach(M_LocaleMessageCatalog::getInstalled() as $locale) {
						// Get the localized text in the current locale
						$localized = call_user_func(array($object, (string) $field->getter), $locale);

						// If a localized text is available in this language:
						if($localized !== NULL) {
							// Then, also increment the number of translated texts
							// available, for this field:
							$tmp[0] += 1;

							// If texts were already available in the db:
							if($id > 0) {
								// Insert the text:
								// (we use the ID of previously created text)
								$result = $this->_getLocalizedTextTable()->insertOrUpdate(array(
									'text_id'     => $id,
									'text_locale' => $locale,
									'text_value'  => $localized
								));

								// Store numeric reference to text in data to be updated:
								$data[$dbName] = $id;
							}
							// If this is the first available text:
							else {
								if($i ++ == 0) {
									// Insert the text:
									// (TODO: We are relying on auto-increment! Is this a safe assumption?)
									$result = $this->_getLocalizedTextTable()->insert(array(
										'text_locale' => $locale,
										'text_value'  => $localized
									));

									// if we failed to do the insert:
									if(! $result) {
										// return FAILURE as result of this method!
										return false;
									}

									// Get the ID of the text in the db:
									$n = $this->getDb()->getLastInsertId();

									// Set the ID of the localized text, in the object:
									call_user_func(array($object, (string)$field->setter . 'Id'), $n);

									// Store numeric reference to text in data to be updated:
									$data[$dbName] = $n;
								}
								else {
									// Insert the text:
									// (we use the ID of previously created text)
									$result = $this->_getLocalizedTextTable()->insert(array(
										'text_id'     => $n,
										'text_locale' => $locale,
										'text_value'  => (string)$localized //we don't accept anything but strings for localized fields: no NULL, ...
									));
								}
							}
						}
						// If a localized text is NOT available for the current
						// field, in the current locale:
						else {
							// Then, we add the locale to the SQL condition, in
							// order to delete localized text entries afterwards:
							$tmp[1][0] .= ! empty($tmp[1][0]) ? ' OR ' : '';
							$tmp[1][0] .= 'text_locale = ?';
							$tmp[1][]   = $locale;
						}
					}

					// Now, we have looped through all of the locales in the
					// application. If a localized value was stored previously:
					// (we know that a value was stored previously, if the
					// numeric reference to the translated text is a positive
					// number)
					if($id > 0) {
						// If no translated texts are available AT ALL for
						// the current field:
						if($tmp[0] == 0) {
							// Then, we need to delete ALL entries from the
							// localized _text table:
							$this
								->_getLocalizedTextTable()
								->delete('text_id = ?', $id);

							// Also, we delete the numeric reference in the field:
							$data[$dbName] = $this->_getDbValue($field, NULL);
						}
						// If (some of the) translated texts are not available,
						// or no longer available, then we make sure that the
						// entries in the localized _text table are deleted as
						// well...
						elseif(count($tmp[1]) > 2) {
							// We delete the entries, for those locales for which
							// no translated texts exist:
							$tmp[1][0] = 'text_id = ? AND ( '. $tmp[1][0] .' )';
							call_user_func_array(
								array($this->_getLocalizedTextTable(), 'delete'),
								$tmp[1]
							);
						}
					}
				}
				// If the field is not localized:
				else {
					$dbValue = call_user_func(array($object, (string) $field->getter));

					//check if we want to convert to a db-value and store it
					$data[$dbName]  = $this->_getDbValue($field,$dbValue);
				}
			}
		}

		// get primary key database name and value
		$primaryKey = (string) $this->_getPrimaryKey()->db_name;
		$primaryKeyValue = call_user_func(array(
			$object,
			(string) $this->_getPrimaryKey()->getter
		));

		// update database
		$result = $this->_getTable()->update(
			$data ,
			$primaryKey . ' = ?',
			$primaryKeyValue
		);

		// If successfully updated:
		if($result) {
			// Dispatch an event, to notify that we have updated the object:
			$this->_dispatchDataObjectEvent(M_DataObjectMapperEvent::DATA_OBJECT_UPDATE, $object);
		}

		// Return result
		return $result;
	}
	
	/**
	 * Insert object into database
	 *
	 * @param M_DataObject $object
	 * @return bool
	 * @author Ben Brughmans, Tom Bauwens
	 */
	public function insert(M_DataObject $object) {
		// Dispatch an event, to notify that we are about to insert the object:
		$this->_dispatchDataObjectEvent(M_DataObjectMapperEvent::DATA_OBJECT_BEFORE_INSERT, $object);
		
		// store the data in array
		$data = array();
		
		// iterate through every field in the definition of the model
		foreach( $this->_getXml()->fields->children() AS $field ) {
			// Get the name of the current field's database column
			$dbName = (string) $field->db_name;
			
			// store only when that name is known!
			if (!empty($dbName)) {
				// The getter of the of the current field's value is
				// stored in $getter.
				$getter = (string)$field->getter;
				
				// If the current field is a localized field:
				if($this->isLocalizedField($field)) {
					// For each of the installed locales:
					$i = 0;
					$n = 0;
					foreach(M_LocaleMessageCatalog::getInstalled() as $locale) {
						// Get the localized text in this language:
						$localized = call_user_func(array($object, $getter), $locale);
						
						// If a localized text is available in this 
						// language:
						if($localized <> NULL) {
							// If this is the first available text:
							if($i ++ == 0) {
								// Insert the text:
								// (@TODO: We are relying on auto-increment! Is this a safe assumption?)
								//@todo (added by Ben for Tom ;) ) When we have a localized field, and we
								//set the field without a locale, and no locale is set in the system: no error is thrown.
								//this will result in unusable data in the database, no?
								$result = $this->_getLocalizedTextTable()->insert(array(
									'text_locale' => $locale,
									'text_value'  => $localized
								));
								
								// if we failed to do the insert:
								if(! $result) {
									// return FAILURE as result of this method!
									return false;
								}
								
								// Get the ID of the text in the db:
								$n = $this->getDb()->getLastInsertId();
								
								// Set the ID of the localized text, in 
								// the object:
								call_user_func(array($object, (string) $field->setter . 'Id'), $n);
							}
							// If this is not the first available text:
							else {
								// Insert the text:
								// (we use the ID of previously created text)
								$result = $this->_getLocalizedTextTable()->insert(array(
									'text_id'     => $n,
									'text_locale' => $locale,
									'text_value'  => (string)$localized //we don't accept anything but strings for localized fields: no NULL, ...
								));
							}
						}
					}
					
					// The getter of the reference to localized texts
					// is the same getter, but with 'Id' appended to it:
					$getter .= 'Id';
				}
				
				//the value which will be stored in the database
				$dbValue = call_user_func(array($object, $getter));
				
				//check if we want to convert to a db-value and store it
				$data[$dbName]  = $this->_getDbValue($field,$dbValue);
			}
		}
		
		// Save data into table
		$result = $this->_getTable()->insert( $data );

		// update id
		if($result) {
			// Get the object's ID:
			$idField       = $this->_getPrimaryKey();
			$idFieldName   = (string) $idField->name;
			
			// If the ID has been provided in the dataset
			$idValue = (isset($data[$idFieldName]) && $data[$idFieldName] != NULL)
				// then we simply copy the value as the new ID:
				? $data[$idFieldName]
				// If not, we get the last insert-id from the database
				: $this->getDb()->getLastInsertId();
			
			// Set the ID
			call_user_func( 
				array(
					$object,
					(string) $idField->setter
				),
				$idValue
			);
			
			// Dispatch an event, to notify that we have successfully inserted the object
			$this->_dispatchDataObjectEvent(M_DataObjectMapperEvent::DATA_OBJECT_INSERT, $object);
		}
		
		// Return the result:
		return $result;
	}
	
	/**
	 * Delete the object from the database
	 *
	 * @param M_DataObject
	 * @return bool
	 */
	public function delete(M_DataObject $object) {
		// Dispatch an event, to notify that we are about to delete the object:
		$this->_dispatchDataObjectEvent(M_DataObjectMapperEvent::DATA_OBJECT_BEFORE_DELETE, $object);
		
		// For each of the fields in the object:
		foreach($this->_getXml()->fields->children() AS $field) {
			// If the current field is localized:
			if($this->isLocalizedField($field)) {
				// We delete the localized texts:
				$id = call_user_func(array($object, (string) $field->getter . 'Id'));
				if($id > 0) {
					$this->_getLocalizedTextTable()->delete('text_id = ?', $id);
				}
			}
		}
		
		// Get the primary key db-name
		$pk = (string) $this->_getPrimaryKey()->db_name;
		
		// Extract the primary key value out of the object
		$pkValue = call_user_func(array(
			$object,
			(string) $this->_getPrimaryKey()->getter
		));
		
		// delete it from the database
		$res = $this->_getTable()->delete($pk.' = ?', $pkValue);
		
		// when an object is successfully deleted:
		if($res) {
			// We empty the primary key:
			$pkValue = call_user_func(
				array(
					$object,
					(string) $this->_getPrimaryKey()->setter
				),
				null
			);
			
			// We dispatch an event, to notify that the data object has been deleted
			$this->_dispatchDataObjectEvent(M_DataObjectMapperEvent::DATA_OBJECT_DELETE, $object);
		}
		
		// Return the result (boolean)
		return $res;
	}
	
	/**
	 * Delete an object by it's id
	 *
	 * @param int $id
	 * @return bool
	 */
	public function deleteById($id) {
		/*
		// For each of the fields in the object:
		foreach($this->_getXml()->fields->children() AS $field) {
			// If the current field is localized:
			if($this->isLocalizedField($field)) {
				// First, we need to construct the object (in order to
				// delete the attached translations)
				$object = $this->getById($id);
				if($object) {
					return $object->delete();
				}
				
				// stop iterating the fields:
				break;
			}
		}
		
		// If we're still here, the object is not localizable, and
		// we simply remove the database entry.
		return $this->_getTable()->delete('id = ?', (int) $id);
		 */
		
		// We construct the object first. We do this, so events can be dispatched
		// with the data-object:
		$object = $this->getById($id);
		if($object) {
			return $object->delete();
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Delete a objects by their id
	 *
	 * @param array $ids
	 * @return bool
	 * @todo Implement this method: it would be nice to use an array of id's
	 * and delete all records with one query
	public function deleteByIds( array $ids) {
		
	}
	 */
	
	/**
	 * Truncate all objects from the database
	 *
	 * @param object
	 * @return bool
	 */
	public function truncate() {
		// We dispatch an event, to notify that the mapper is about to be truncated:
		$this->_dispatchDataObjectMapperEvent(M_DataObjectMapperEvent::DATA_OBJECT_MAPPER_BEFORE_TRUNCATE);
		
		// For each of the fields in the object:
		foreach($this->_getXml()->fields->children() AS $field) {
			// If the current field is localized:
			if($this->isLocalizedField($field)) {
				// Also truncate localized texts table:
				$this->_getLocalizedTextTable()->truncate();
				break;
			}
		}
		
		// If the mapper's table has been truncated successfully:
		if($this->_getTable()->truncate()) {
			// We dispatch an event, to notify that the mapper has been truncated
			$this->_dispatchDataObjectMapperEvent(M_DataObjectMapperEvent::DATA_OBJECT_MAPPER_TRUNCATE);
			
			// return success
			return TRUE;
		}
		
		// If we're still here, we return FALSE
		return FALSE;
	}
	
	/**
	 * Save the object into the database. The mapper checks whether this 
	 * should be an update or insert
	 *
	 * @param M_DataObject $object
	 * @return bool
	 */
	public function save( M_DataObject $object )
	{
		if ($this->isNew($object))
		{
			$res = $this->insert($object);
		}
		else
		{
			$res = $this->update($object);
		}
		
		return $res;
	}
	
	/**
	 * Get the database value for a field by it's name
	 *
	 * @param string $fieldName
	 * @param mixed $value
	 * @return mixed
	 */
	public function getDbValueByFieldName($fieldName, $value) {
		return $this->_getDbValueByFieldName($fieldName, $value);
	}
	
	/**
	 * Get Private Key For Alpha ID's
	 * 
	 * Will provide the private key that is to be used for encoding and
	 * decoding alpha ID's.
	 * 
	 * @access public
	 * @return string
	 */
	public function getPrivateKeyForAlphaIds() {
		// Make sure the first hash string is defined as php constant:
		if(! (defined('APPLICATION_HASH1') && APPLICATION_HASH1)) {
			throw new M_Exception(sprintf(
				'Cannot create Alpha ID private key in %s::%s(); the ' .
				'required constant APPLICATION_HASH1 is not defined yet!',
				__CLASS__,
				__FUNCTION__
			));
		}

		// Return the private key, which consists of the defined variable
		// with the data object name of the mapper concatinated to it
		return APPLICATION_HASH1 . $this->_getDataObjectClassName();
	}
	
	/**
	 * Get by Alpha ID
	 * 
	 * Will try to fetch a unique object that matches the provided Alpha ID. 
	 * Returns an instance of {@link M_DataObject}, or NULL if no matching object 
	 * could have been found.
	 * 
	 * @access public
	 * @param string $alphaId
	 * @return M_DataObject $dataObject
	 * 		The matching object, or NULL if not found
	 */
	public function getByAlphaId($alphaId) {
		return $this->getById(
			M_Helper::decodeAlphaId((string) $alphaId, $this->getPrivateKeyForAlphaIds())
		);
	}
	
	/**
	 * Get by Alpha IDs
	 * 
	 * Will try to fetch multiple objects that match the provided Alpha IDs. 
	 * Returns an instance of {@link M_ArrayIterator}, or NULL if no matching object 
	 * could have been found.
	 * 
	 * @access public
	 * @param array $alphaIds
	 * @return M_ArrayIterator $result
	 * 		The matching object, or NULL if not found
	 */
	public function getByAlphaIds(array $alphaIds) {
		// Prepare an array to store the actual database IDs
		$ids = array();
		
		// For each alpha id we received
		foreach($alphaIds as $alphaId) {
			// Decode it to its ID, adding it to the working array
			$ids[] = M_Helper::decodeAlphaId((string) $alphaId, $this->getPrivateKeyForAlphaIds());
		}
		
		// Fetch the objects, using the collection of IDs
		return $this->getByIds($ids);
	}
	
	/**
	 * Get AlphaId For ID
	 * 
	 * Will convert the given {@link M_DataObject} id to its proper alpha id,
	 * using the correct mapper. This is especially useful in forms, where you
	 * often have to convert id's to alpha id's and the other way around.
	 * 
	 * @static
	 * @access public
	 * @param int $id
	 *		The numeric {@link M_DataObject} id
	 * @param string $dataObjectClassName
	 *		The name of the data object, e.g. "ProductCategory"
	 * @param string $module
	 *		The name of the module, e.g. "product"
	 * @return string
	 *		The encoded alpha id
	 */
	public static function getAlphaIdFor($id, $dataObjectClassName, $module) {
		// Construct the mapper we need to use
		$mapper = M_Loader::getDataObjectMapper($dataObjectClassName, $module);
		
		// Encode the alpha ID, returning it
		return M_Helper::encodeAlphaId($id, $mapper->getPrivateKeyForAlphaIds());
	}
	
	/**
	 * Get ID for AlphaId
	 * 
	 * Will convert the given alpha id to its proper {@link M_DataObject} id,
	 * using the correct mapper. This is especially useful in forms, where you
	 * often have to convert id's to alpha id's and the other way around.
	 * 
	 * @static
	 * @access public
	 * @param string $alphaId
	 *		The {@link M_DataObject} alpha id
	 * @param string $dataObjectClassName
	 *		The name of the data object, e.g. "ProductCategory"
	 * @param string $module
	 *		The name of the module, e.g. "product"
	 * @return int
	 *		The numeric {@link M_DataObject} id
	 */
	public static function getIdFor($alphaId, $dataObjectClassName, $module) {
		// Construct the mapper we need to use
		$mapper = M_Loader::getDataObjectMapper($dataObjectClassName, $module);
		
		// Encode the alpha ID, returning it
		return M_Helper::decodeAlphaId($alphaId, $mapper->getPrivateKeyForAlphaIds());
	}
	
	/**
	 * Get Alpha ID's
	 * 
	 * Will convert all ID's provided by {@link M_DataObjectMapper::getIds()}
	 * to alpha ID's, returning the collection anew.
	 * 
	 * @see M_DataObjectMapper::getIds()
	 * @access public
	 * @return M_ArrayIterator
	 *		The collection of {@link M_DataObject} alpha ID's
	 */
	public function getAlphaIds() {
		// Output array
		$out = array();
		
		// For each of the ID's
		foreach($this->getIds() as $id) {
			// Convert it to an alpha ID, adding it to the output collection
			$out[] = M_Helper::encodeAlphaId((int) $id, $this->getPrivateKeyForAlphaIds());
		}
		
		// Return the output collection
		return new M_ArrayIterator($out);
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Apply filters
	 * 
	 * This method will process additional filters to a query.
	 * These additional filters are defined by methods such as
	 * {@link M_DataObjectMapper::setFilters()}.
	 * {@link M_DataObjectMapper::addFilter()}.
	 * {@link M_DataObjectMapper::resetFilters()}.
	 * 
	 * @access protected
	 * @param MI_DbQuery $query
	 * 		The query to append the additional filters to
	 * @param array $filters
	 * 		Additional filters
	 * @return MI_DbQuery $query
	 * 		The filtered query; the query with additional filters
	 * 		appended to it
	 */
	protected function _getFilteredQuery(MI_DbQuery $query, array $filters) {
		$filters = array_merge($this->getFilters(), $filters);
		foreach ($filters AS $filter) {
			if($filter instanceof MI_DbQueryFilter == false) {
				throw new M_Exception('cannot add filter to mapper: $filter does not implements filter interface');
			}
			/* @var $filter M_DbQueryFilter */
			//provide filter with this mapper so filter can use mapper-functionality
			//to translate object-names to db-names, and object-values to db-values
			//e.g. M_Date to Mysql DATETIME
			$filter->filter($query, $this);
			
			//check if the user wants to filter on a localized field, if so
			//we need to select data from a localized table too
			foreach($filter->getFields() AS $fieldName) {
				$this->_addLocalizedFieldJoin($query,$fieldName);
			}
		}
		
		// return the final query:
		return $query;
	}
	
	/**
	 * Add a localized field join to a query, this way you are able to search
	 * on the localized field
	 * 
	 * The column name of the localized field is returned, the query will get 
	 * updated with the join automatically
	 * 
	 * @param M_DbQuery $query
	 * @param string $fieldName
	 * @param string $locale
	 * 			The language locale, if left empty we will try to get the default
	 * 			value
	 * @param string $tableName
	 *			The name (or alias) of the table on which the join should be
	 *			applied. If left empty, no tablename will be used
	 * @return string
	 * 			The db-fieldname on which you can filter
	 */
	protected function _addLocalizedFieldJoin(M_DbQuery $query, $fieldName, $locale = null, $tableName = null) {
	    $field = $this->getField($fieldName);
		$fieldDbTranslation = false;
		
		//check if this is a localized field
		if ($field && $this->isLocalizedField($field)) {
			$fieldDbName = $this->getFieldDbName($fieldName);

			//get the default locale if none is given
			if (is_null($locale)) $locale = $this->getLocale();
		    $localizedTable = $this->_getLocalizedTextTable()->getName();
					
			//add alias when user already filters on a localized field 
			//we create an unique alias for every table, this way we
			//can search for different values on different columns
			//if we wouldn't do this, every locale table would be 
			//referenced to as "$localizedTable" and it would be
			//impossible to refer to it in the WHERE clause when there
			//are multiple filters on localized fields
			//NOTE: the same combination is made in 
			//{@link _getFieldDbTranslation()}
			$localizedTableAlias = $this->_getLocalizedJoinTableName(
				$localizedTable, 
				$fieldDbName, 
				$locale
			);

			//check if this object has already been used
			$objectHash = $query->getCoreObjectId();
			if (!array_key_exists($objectHash, $this->_localizedJoins)) {
				$this->_localizedJoins[$objectHash] = array();
			}
			
            //check if this joins doesn't already exists			
			if (!in_array($localizedTableAlias, $this->_localizedJoins[$objectHash])) {
    			//we store this join in a static array so we don't make multiple 
    			//joins to one text-field for this object
    			$this->_localizedJoins[$objectHash][] = $localizedTableAlias;
    			
    			$condition = $this->getDb()->quoteIdentifier($localizedTableAlias . '.text_id');
    			$condition .= ' AND ' . $this->getDb()->quoteIdentifier($localizedTableAlias . '.text_locale').' = ' . $this->getDb()->quote($locale);
    			
    			$query->joinLeft(
    				$localizedTable,
    				$localizedTableAlias,
    				($tableName) ? $tableName.'.'.$fieldDbName : $fieldDbName, //if a tablename is given use this one to apply the join on
    				$condition
    			);
    			$fieldDbTranslation = $this->getFieldDbNameLocalized($fieldName, $locale);
			}
		}
		return $fieldDbTranslation;
	}
	
	/**
	 * Get the localized table name for a localized field
	 *
	 * @param string $localizedTableName
	 * @param string $dbName
	 * @param string $locale
	 * @return string
	 */
	protected function _getLocalizedJoinTableName($localizedTableName, $dbName, $locale) {
		return $localizedTableName.'_'.$dbName.'_'.$locale;
	}
	
	/**
	 * Get the localized field name for a localized field
	 *
	 * @param string $localizedTableName
	 * @param string $dbName
	 * @param string $locale
	 * @return string
	 */
	protected function _getLocalizedJoinFieldName($localizedTableName, $dbName, $locale) {
		$table = $this->_getLocalizedJoinTableName(
			$localizedTableName, 
			$dbName, 
			$locale
		);
		return $table.'.text_value';
	}
	
	/**
	 * Get the translation from object-name to db-name. This will return an
	 * a string containing join name (if applicable) and the db-name
	 * 
	 * @example 
	 * 
	 * $this->getFieldDbNameLocalized('title', 'nl');
	 * 
	 * will produce:
	 * 
	 * news_title.text_value
	 * 
	 * @param string $fieldName
	 * @param string $locale
	 * 		Optional: a specific locale
	 * @return string
	 * 		False on failure (only when a $fieldName is provided)
	 */
	public function getFieldDbNameLocalized($fieldName, $locale = null) {
		//get the default locale if none is given
		if (is_null($locale)) $locale = $this->getLocale();
		
		//get the column-name for this field
		$dbName = $this->getFieldDbName($fieldName);
		
		//get the field-definition to check if it's a localized field
		$field = $this->getField($fieldName);
		
		//if this is a localized field, we don't wan't to filter on the 
		//db_name, (this would be e.g. title_id) but on the localized
		//column e.g. product_text.text_value
		if ($field && $this->isLocalizedField($field)) {
        	return $this->_getLocalizedJoinFieldName(
        		$this->_getLocalizedTextTable()->getName(), 
        		$dbName, 
        		$locale
        	);
		} 
		//field exists: add table name
		elseif($field) {
			return $this->getTableName() . '.' . $dbName;
		}
		//field does not exist: simply add the dbname... most probably this
		//field is of a joined table
		else {
			return $dbName;
		}
	}
	
	/**
	 * Get the classname with which dataObject this mapper interacts
	 * 
	 * @return string
	 */
	public function getDataObjectClassName() {
		return $this->_getDataObjectClassName();
	}

	/**
	 * Create an instance of {@link M_DataObject} from a record (array)
	 *
	 * We need to be able to create {@link M_DataObject}-instances from a
	 * record so we can use a {@link M_DataObjectRecordset} for optimal
	 * performance
	 * 
	 * @param array $record
	 * @param M_DataObject $object
	 * @return M_DataObject
	 * @author Ben Brughmans
	 */
	public function createObjectFromRecord($record, M_DataObject $object = null) {
		return $this->_createObjectFromRecord($record, $object);
	}
	
	
	/**
	 * Get the base select query with possible filters applied to it
	 *
	 * @author Ben Brughmans, Tom Bauwens
	 * @param M_DbQueryFilter|array
	 *		Additional filter(s), empty by default
	 * @return M_DbSelect
	 */
	protected function _getFilteredSelect( $filters = null ) {
		//when $filters not empty -> check if $filters is of type array 
		//or M_DbQueryFilter
		if (!is_null($filters) && !is_array($filters) && $filters instanceof M_DbQueryFilter == false) {
			// Then, we throw an exception to inform about the error
			throw new M_Exception(sprintf(
				'$filters not of type array or M_DbQueryFilter'
			));
		}
		
		// Prepare additional filters:
		if($filters instanceof MI_DbQueryFilter) { $filters = array($filters); }
		elseif(is_null($filters)) { $filters = array(); }

		// First, get the SELECT statements with all of the filters applied to it:
		/* @var $query M_DbSelect */
		$query = $this->_getFilteredQuery(
			// Use a basic SELECT statement to start with:
			$this->_getTable()->select(),
			// And add the additional filters to it:
			$filters 
		);

		// If unions have been defined with other data object mappers:
		$unionCount = count($this->_union);
		if($unionCount > 0) {
			// Get the mappers that have been added to the union:
			$allMappers = $this->_union;
			
			// Then, we add this mapper to the collection of data object mappers:
			array_unshift($allMappers, $this);
			$unionCount += 1;
			
			// We initiate an array with fields. This keys of this array will be
			// the field names, while the values in turn will be other arrays that
			// inform about the data object mappers making use of that field.
			$fields  = array();
			
			// We initiate an array with database queries. This array will be
			// populate with a select statement for each of the data object mappers
			// in the union:
			$selects = array($query);
			
			// Extra columns added to each SELECT statement in the UNION will be maintained
			// where possible. We initiate an array where we store these extra columns:
			$addedColumns = array();
			
			// For each of the mappers in the union:
			for($i = 0; $i < $unionCount; $i ++) {
				// Get the select statement from the current mapper, with all
				// additional filters applied to it, and add it to the array of
				// queries:
				if($i > 0) {
					$query = $allMappers[$i]->_getFilteredSelect();
				}
				
				// Trim out the "table.*" from the SELECT:
				$tempExtraColumns = M_Helper::trimCharlist(strtr(
					$query->getColumns(),
					array(
						$allMappers[$i]->getTableName() . '.*' => '',
						$allMappers[$i]->getDb()->quoteIdentifier($allMappers[$i]->getTableName() . '.*') => ''
					)
				), ',');
				
				// Have specific columns been added to the SELECT statement?
				if($tempExtraColumns) {
					// Then, we extract the column selected (the alias):
					$tempColumnMatches = array();
					
					// If we can find the ALIAS syntax:
					if(preg_match_all('/(.*) AS `?([a-z0-9]+)`?/i', $tempExtraColumns, $tempColumnMatches)) {
						// For each of the matches:
						foreach($tempColumnMatches[2] as $matchIndex => $matchAlias) {
							// Add the column, or the expression, to the array of extra columns
							// found in the SELECT statements:
							$addedColumns[$matchAlias][$i] = $tempColumnMatches[1][$matchIndex];
						}
					}
					// If not, we assume the column name is being used instead:
					else {
						// Note however that multiple columns may have been added. So, for
						// each of these columns:
						foreach(explode(',', $tempExtraColumns) as $matchAlias) {
							$matchAlias = M_Helper::trimCharlist($matchAlias);
							$addedColumns[$matchAlias][$i] = $matchAlias;
						}
					}
				}
				
				// A UNION syntax in a SELECT statement requires both tables to 
				// have exactly the same (number of) columns. This is why we add 
				// missing columns, to make sure that we have an exactly matching 
				// number of columns! Before we start with all of this, we set 
				// a column to the select statement that is rendered by THIS 
				// mapper, to recognize the type of data objects that are being 
				// fetched with the select statement:
				
				// Note that, in doing so, we reset the columns in the SELECT statement:
				$query->columnsLiteral(
					'"' . $i . '" AS ' .
					$this->getDb()->quoteIdentifier('union_mapper_index')
				);
				
				// Add the select statement to the array of database queries:
				$selects[$i] = $query;
				
				// For each of the fields in the current data object mapper of 
				// the union:
				foreach($allMappers[$i]->getFields() as $field) {
					// Get the dbname:
					$dbname = (string) $field->db_name;
					
					// Has the current field already been added to the array of 
					// fields?
					if(! array_key_exists($dbname, $fields)) {
						// If not, we initiate the entry now, with the current
						// index of the data object mapper:
						$fields[$dbname] = array($i);
					}
					// If already added:
					else {
						// Then, we add the index of the current data object mapper
						array_push($fields[$dbname], $i);
					}
				}
			}
			
			// For each of the mappers in the union:
			for($i = 0; $i < $unionCount; $i ++) {
				// For the current mapper in the union, we'll iterate through 
				// each of the fields we have collected before:
				foreach($fields as $dbname => $indexes) {
					// If the field is not present in the current data object
					// mapper of the union:
					if(! in_array($i, $indexes)) {
						// Then, we add a NULL replacer for its value, to the 
						// select statement:
						$selects[$i]->addColumn(
							'NULL AS ' . $this->getDb()->quoteIdentifier($dbname), 
							TRUE
						);
					}
					// If the field is recognized by the current data object mapper:
					else {
						// Then, we simply add the column to the select statement
						$selects[$i]->addColumn($this->getDb()->quoteIdentifier($dbname), TRUE);
					}
				}
			}
			
			// For each of the extra columns we have detected earlier:
			foreach($addedColumns as $column => $columnExpressions) {
				// If the current column was detected in ALL select statements:
				if(count($columnExpressions) == count($selects)) {
					// Then, we'll restore the column in each of the SELECT statements. So,
					// for each of the UNIONS:
					for($i = 0; $i < $unionCount; $i ++) {
						// Add the column:
						$selects[$i]->addColumn(
							$column == $columnExpressions[$i]
								? $column
								: $columnExpressions[$i] . ' AS ' . $column,
							
							TRUE
						);
					}
				}
			}
			
			// Now, all SELECT statements have been given a set of columns, to
			// make sure they all return the same number of columns. Now, we get
			// the SELECT statement for THIS mapper:
			$query = array_shift($selects);
			
			// And, we add the other statements as union to that SELECT. For each
			// statement:
			foreach($selects as $select) {
				// Add a union:
				$query->addUnion($select);
			}
		}
		
		// Return the SELECT statement:
		return $query;
	}
	
	/**
	 * Add filter with data object
	 * 
	 * Can be used to add a filter to the mapper using the id of the provided
	 * {@link M_DataObject}. If the provided {@link M_DataObject} is not
	 * stored in the database yet, an empty result set will be forced.
	 * 
	 * @access protected
	 * @param string $fieldId
	 *		The field we have to apply the ID filter to, e.g. 'productId'
	 * @param M_DataObject $dataObject
	 *		The data object whose ID we have to use
	 * @param string $comparison
	 *		The comparison operator we have to use, which is EQ by default
	 * @return M_DataObjectMapper
	 *		Return self, for a fluent programming interface
	 */
	protected function _addFilterWithDataObject($fieldId, M_DataObject $dataObject, $comparison = M_DbQueryFilterWhere::EQ) {
		// Make sure the data object is present in the database. If not
		// the case, we cannot filter on its ID, and have to force an empty
		// resultset.
		if($dataObject->isNew()) {
			return $this->setFlagEmptyResultSet();
		}
		
		// Apply the filter, returning ourselves
		return $this->addFilter(new M_DbQueryFilterWhere((string) $fieldId, $dataObject->getId(), $comparison));
	}
	
	/**
	 * Add filter with data object ids
	 * 
	 * Can be used to add a filter to the mapper using a collection of provided
	 * {@link M_DataObject} ids. If an invalid (or empty) collection
	 * of ids is provided, an empty result set will be forced, or an
	 * {@link M_Exception} will be thrown, if you choose to.
	 * 
	 * @access protected
	 * @param string $fieldId
	 *		The field we have to apply the ID filter to, e.g. 'productId'
	 * @param M_DataObject $dataObject
	 *		The data object whose ID we have to use
	 * @param string $comparison
	 *		The comparison operator we have to use, which is IN by default
	 * @param bool $throwException
	 *		Set to TRUE if you want to throw an error if the provided collection
	 *		of ids is invalid, rather than simply forcing an empty result set
	 * @return M_DataObjectMapper
	 *		Return self, for a fluent programming interface
	 */
	protected function _addFilterWithDataObjectIds($fieldId, array $ids, $comparison = M_DbQueryFilterWhere::IN, $throwException = false) {
		// If the received collection of ids is invalid
		if(! $this->isValidArrayOfIds($ids)) {
			// Then stop here. If we have to throw an exception:
			if($throwException) {
				throw new M_Exception(sprintf(
					'Cannot apply filter; invalid collection of M_DataObject ' .
					'ids specified: "%s"',
					implode(', ', $ids)
				));
			}
			// Otherwise, simply force an empty result set
			else {
				return $this->setFlagEmptyResultSet();
			}
		}
		
		// Apply the filter
		return $this->addFilter(new M_DbQueryFilterWhere((string) $fieldId, $ids, $comparison));
	}
	
	/**
	 * Fetch all records from the result set
	 *
	 * @param MI_DbResult $result
	 * @return M_DataObjectRecordset
	 */
	protected function _fetchAll(MI_DbResult $result)
	{
		return new M_DataObjectRecordset($result, $this);
	}
	
	/**
	 * Fetch one record from the resultset
	 *
	 * @param MI_DbResult $result
	 * @todo before this method is called we need to catch exceptions: howto? 
	 * @return object
	 */
	protected function _fetchOne(MI_DbResult $result)
	{
		//no records found
		if (count($result) == 0) return false;
		
		//fetch record
		$record = $result->getOne();
		
		//create object
		return $this->_createObjectFromRecord( $record );
	}
	
	/**
	 * Creates an object from a single dimension array, like a database row, 
	 * using the xml data defintion
	 *
	 * @param array $row
	 * @return object
	 */
	protected function _createObjectFromRecord($record, $object = null) {
		// If a union has been created with another mapper:
		// (we know this, if the column "union_mapper_index" has been added to 
		// the result set. This column serves to identify the mapper that is 
		// responsible for instantiating the corresponding data object)
		if(isset($record['union_mapper_index']) && is_numeric($record['union_mapper_index'])) {
			// Cast the index to an integer. Also, lower the index by one:
			$mapperIndex = (int) $record['union_mapper_index'] - 1;
			
			// If another mapper is responsible:
			if(array_key_exists($mapperIndex, $this->_union)) {
				// Then, we ask that mapper for a new object:
				return $this->_union[$mapperIndex]->_createObjectFromRecord($record);
			}
		}
		
		// If we are still here, then THIS data object mapper is responsible
		// of instantiating the data object. First, load the data object:
		M_Loader::loadDataObject(
			$this->_getDataObjectClassName(),
			$this->_getModuleName()
		);

		// Instantiate the object:
		$className = $this->_getDataObjectClassName();
		$object = new $className;

		// Update/Populate it with the provided values:
		return $this->_updateObjectFromRecord($record,$object);
	}
	
	/**
	 * Update a object from a data-record and parse all fields from xml data
	 * definition file
	 *
	 * @author Ben Brughmans, Tom Bauwens
	 * @param $record
	 * @param $object
	 */
	protected function _updateObjectFromRecord($record, $object) {
		// for every variable of this object: try to set the value
		foreach($this->_getXml()->fields->children() as $field) {
			// set the db-value through the setter. To do so, we prepare the
			// name of the setter function: (if the current field is localized,
			// we will append "Id" to the setter name)
			if($this->isLocalizedField($field)) {
				$setter = (string) $field->setter . 'Id';
			} else {
				$setter = (string) $field->setter;
			}
			
			// The database name of the current field is:
			$dbName = (string) $field->db_name;
			
			// if the property is not present in the db-row the value 
			// will be null
			$value = isset($record[(string) $dbName]) 
				? $record[(string) $dbName]
				: null;

			// Get the object's value: possibly we need to convert the db-value
			// to an object, ...
			$value = $this->_getObjectValue($field,$value);

			// only try to set if the mutator is known and the name is known
			if($setter && $dbName !== '') {
				call_user_func(array($object, $setter), $value);
			}
		}

		// Return the populated object:
		return $object;
	}
	
	/**
	 * Get xml data definition
	 *
	 * @return SimpleXMLElement
	 */
	protected function _getXml()
	{
		if (is_null($this->_xml))
		{
			$this->_xml = $this->_getDefinitionFromFile();
		}
		return $this->_xml;
	}
	
	/**
	 * Get the classname with which dataObject this mapper interacts
	 *
	 * @return str
	 */
	protected function _getDataObjectClassName()
	{
		//if the classname isn't set yet, we try to define the object
		//for which this mapper is used. This is done by removing the mapper
		//suffix from the className
		if (!$this->_dataObjectClassName)
		{
			$this->_dataObjectClassName = str_replace(
				self::MAPPER_SUFFIX, 
				'', 
				get_class($this) 
			);
		}
		
		return $this->_dataObjectClassName;
	}
	
	/**
	 * Get the active module folder name
	 *
	 * @internal when creating a new DataObjectMapper and the object's filename
	 * differs from the module name, you can overwrite this method and specify
	 * the modulename. 
	 * 
	 * By default we assume the module name is the same as the
	 * classname
	 * 
	 * @return str
	 */
	protected function _getModuleName()
	{
		return $this->_getDataObjectClassName();
	}
	
	/**
	 * Get the object-definition from the given filename
	 *
	 * @return SimpleXMLElement
	 */
	protected function _getDefinitionFromFile() {
		//definition file: label stored in registry
		$definitionFileLabel = 'DataDefinitionFile_' . $this->_getDataObjectClassName();
		
		//load from registry
		/* @var $definitionFile M_File */
		$definitionFile = M_Registry::getInstance()->get( $definitionFileLabel );
		
		//if not loaded yet, load and store in registry
		if ( is_a($definitionFile, 'M_File') == false)
		{
			$definitionFile = $this->_getDefinitionFile();
			M_Registry::getInstance()->register( $definitionFileLabel, $definitionFile );
		}
		//the label stored in registry
		$label = 'DataDefinition_' . $this->_getDataObjectClassName();
		
		//load xml from registry
		$xml = M_Registry::getInstance()->get($label);
		
		//xml not yet loaded, load and store in registry
		if (is_null($xml)) 
		{
			$xml = @simplexml_load_file( $definitionFile->getPath() );
			if (!$xml) {
				throw new M_Exception(sprintf('Cannot load definition-file from %s', $definitionFile->getPath()));
			}
			M_Registry::getInstance()->register( $label, $xml );
		}
		
		return $xml;
	}
	
	/**
	 * Get the data-object's primary key and return the xml element
	 * 
	 * @internal used for interal mapper use
	 * @return SimpleXMLElement
	 */
	protected function _getPrimaryKey()
	{
		$label = 'DataObjectPrimaryKey_' . $this->_getDataObjectClassName();
		$primaryKey = M_Registry::getInstance()->get($label);
		
		if ( is_null($primaryKey))
		{
			//search in data definition for the node which has an attribute
			//"isPrimary" with value "true"
			$primaryKey = $this->_getXml()->xpath(
				"//definition/fields/field[@primary_key='true']"
			);
			
			//no primary key found
			if (count($primaryKey) == 0)
			{
				throw new Exception(
					'No primary key defined for object ' . $this->_getDataObjectClassName() 
				);
			}
			
			//get the first element from the array
			$primaryKey = array_shift($primaryKey);
			
			//store in registry
			M_Registry::getInstance()->register($label, $primaryKey);
		}
		
		return $primaryKey;
	}
	
	/**
	 * Get the data-object's primary key
	 * 
	 * @return M_DbIndex
	 */
	public function getPrimaryKey() {
		$xmlPrimaryKey = $this->_getPrimaryKey();

		$column = new M_DbColumn((string)$xmlPrimaryKey->db_name);
		$column->setType((string)$xmlPrimaryKey->db_type);
		$column->setLength((string)$xmlPrimaryKey->length);
		
		$index = new M_DbIndex((string)$xmlPrimaryKey->name);
		$index->setType(M_DbIndex::PRIMARY);
		$index->addColumn($column);
		
		return $index;
	}
	
	/**
	 * Get the definition file for this object
	 *
	 * @return M_File
	 */
	protected function _getDefinitionFile()
	{
		$path = array();
		$path[] = M_Loader::getModulePath($this->_getModuleName());
		$path[] = M_Loader::getResourcesFolder();
		$path[] = M_Loader::getDefinitionsFolder();
		$path[] = $this->_getDataObjectClassName() . '.xml';
		
		return new M_File(M_Loader::getAbsolute(implode('/',$path)));
	}
	
	/**
	 * Get the db-table for this object
	 *
	 * @return M_DbTable
	 */
	protected function _getTable()
	{
		if (is_null($this->_table))
		{
			//get the table from the xml-definition
			$this->_table  = $this->getDb()->getTable( 
				(string) $this->_getXml()->table 
			);
		}
		
		return $this->_table;
	}
	
	/**
	 * Get the db-table for localized texts
	 *
	 * @return M_DbTableAdo
	 */
	protected function _getLocalizedTextTable()
	{
		if(! $this->_tableLocalizedTexts) {
			$this->_tableLocalizedTexts  = $this->getDb()->getTable((string) $this->_getXml()->table . '_text');
		}
		return $this->_tableLocalizedTexts;
	}
	
	/**
	 * Check if field is of type date
	 * 
	 * @param SimpleXMLElement $field
	 * 		The field to be evaluated
	 * @return boolean
	 */
	protected function _isFieldDate($field) {
		return (isset($field['type']) && strtolower((string)$field['type']) == 'date');
	}
	
	/**
	 * Check if field is of type encrypted
	 * 
	 * @param SimpleXMLElement $field
	 * 		The field to be evaluated
	 * @return boolean
	 */
	protected function _isFieldEncrypted($field) {
		return (isset($field['type']) && strtolower((string)$field['type']) == 'encrypted');
	}
	
	/**
	 * Check if this field is a text field
	 * 
	 * For this we rely on the db-type
	 *
	 * @param SimpleXMLElement $field
	 * @return boolean
	 */
	protected function _isFieldText($field) {
		return in_array(
			$this->_getFieldDbType($field), 
			M_DbColumn::getTextTypes()
		);
	}
	
	/**
	 * Check if this field is a numeric field
	 * 
	 * For this we rely on the db-type
	 *
	 * @param SimpleXMLElement $field
	 * @return boolean
	 */
	protected function _isFieldNumeric($field) {
		return in_array(
			$this->_getFieldDbType($field),
			M_DbColumn::getNumericTypes()
		);
	}
	
	/**
	 * Get the type of a field
	 * 
	 * Note: the type is only set for specific types e.g. file, ... If
	 * no type is set NULL will be returned
	 *
	 * @param SimpleXMLElement $field
	 * @return mixed
	 */
	private function _getFieldType($field) {
		$type = null;
		if (isset($field['type'])) {
			$type = strtolower($field['type']);
		}
		return $type;
	}
	
	/**
	 * Get the db-type of a field
	 *
	 * @param SimpleXMLElement $field
	 * @return string
	 */
	private function _getFieldDbType($field) {
		return (string)$field->db_type;
	}
	
	/**
	 * Get the database value for a field by it's name
	 *
	 * @param string $fieldName
	 * @param mixed $value
	 * @return mixed
	 */
	protected function _getDbValueByFieldName($fieldName, $value) {
		$field = $this->getField($fieldName);
		//if the field is not known in this object, we cannot get the representative
		//db-value. This could be the case when we want to filter on joined
		//tables
		if ($field) {
			return $this->_getDbValue($field, $value);
		}else return $value;
	}
	
	/**
	 * Get the database value for a field
	 * 
	 * We can auto-adjust the value to the correct database-value according to 
	 * it's type. 
	 * 
	 * E.g. When the field is of type date, the value is of type M_Date but the
	 * database expects a string YYYY-MM-DD HH:MM:SS
	 * 
	 * @param SimpleXMLElement $field
	 * @param mixed $value
	 * @see _getDbValueFromDate
	 * @author b.brughmans
	 */
	protected function _getDbValue(SimpleXMLElement $field,$value) {
		$nullField = $this->isNullField($field);
		
		// check if this is a date field and the value isn't empty
		if ($this->_isFieldDate($field)) {
		    //create a db-string from M_Date based on the type. Do only
			//if the field is not null/empty string or false.
			if ($value) {
				$value = $this->_getDbValueFromDate(
					$value,
					(string)$field->db_type
				);
			}elseif($nullField) $value = null;
			
			return $value;
		}
		
		//check if this is an encrypted field
		elseif ($this->_isFieldEncrypted($field)) {
			
			if ($value) {
				$value = $this->_getDbValueEncrypted(
					$value
				);
			}elseif($nullField) $value = null;
			
			return $value;
		}
		
		//check if this is a text field
		elseif ($this->_isFieldText($field)) {
			if (!is_null($value) || $this->isNullField($field) == false) $value = (string)$value;
			return $value;
		}
		
		//check if this is a numeric field
		elseif ($this->_isFieldNumeric($field)) {
			if (!is_null($value) || $this->isNullField($field) == false) $value = floatval($value);
			return $value;
		}

		//nothing special? Simply return the value
		return $value;
	}
	
	/**
	 * Convert the database value to the object's value
	 *
	 * @param SimpleXMLElement $field
	 * @param mixed $value
	 * @return mixed
	 * @author b.brughmans
	 */
	protected function _getObjectValue($field,$value) {
		// if this is a date, convert the date to M_Date object
		// maybe date can be null in database
		if($this->_isFieldDate($field) && !is_null($value)) {
			try{
				$value = new M_Date($value);
				
				// If a mapper timezone is given, we need to do a conversion
				if(self::getTimezone()) {
					// Ask for a converted {@link Date} object
					$value = $value->getNewDateInTimezone(
						// The default timezone of the application
						M_DateTimezone::getDefaultTimezone(),
						// The timezone that dates are stored in
						self::getTimezone()
					);
					
				}
				
			}catch(M_Exception $e) {
				if ($this->isNullField($field)) $value = null;
				else {
					throw new M_Exception(sprintf(
						'Cannot get object value for field "%s" using value "%s"', 
						(string)$field->name, 
						$value)
					);
				}
			}
		}
		
		// if this is an encrypted value, we try to decrypt it
		if($this->_isFieldEncrypted($field) && !is_null($value)) {
			// Decrypt the value
			$value = $this->_getObjectValueDecrypted($value);
		}
		
		// Return value
		return $value;
	}
	
	/**
	 * Get the database value for a M_Date object for a given type
	 * 
	 * NOTE: using 'timestamp' as type will not result in a UNIX timestamp but
	 * mysql timestamp (same as DATETIME but with a range from 
	 * 1970-01-01 00:00:01 to 2038-01-09 03:14:07).
	 * 
	 * In order to use a unix timestamp simply use INT as db_type
	 * 
	 * @example 
	 * 
	 * $this->_getDbValueFromDate($object, 'datetime');
	 * will result in 2009-12-08 10:59
	 * 
	 * $this->_getDbValueFromDate($object, 'int');
	 * will result in 1260266340
	 * 
	 * @param M_Date $date
	 * @param str $type
	 * 		Can be one of date, timestamp,datetime,time,year,int
	 */
	private function _getDbValueFromDate(M_Date $date, $type) {
		
		// If a mapper timezone is given, we need to do a conversion
		if(self::getTimezone()) {
			// We calculate the timezone differences
			$date = $date->getNewDateInTimezone(self::getTimezone());
		}
		
		/*@var $dbValue M_Date */
		switch(strtolower($type)) {
			case 'date':
			case 'timestamp':
				$dbValue = $date->getMysqlDate();
				break;
			case 'datetime':
				$dbValue = $date->getMysqlDateTime();
				break;
			case 'time':
				$dbValue = $date->getMysqlTime();
				break;
			case 'year':
				$dbValue = $date->getMysqlYear();
				break;
			//integer value (unix-timestamp)
			case 'int':
				$dbValue = $date->getTimestamp();
				break;
			default:
				throw new M_Exception(sprintf(
					'Unsupport database type %s, cannot convert M_Date object 
					to a matching value',$type)
				);
		}
		
		return $dbValue;
	}
	
	/**
	 * Get db value encrypted
	 * 
	 * @param string $value
	 * @return string
	 */
	private function _getDbValueEncrypted($value) {
		// If the value is empty: stop encryption and decryption
		if(!$value) {
			return $value;
		}
		
		return 
			trim(base64_encode(
				// We use mcrypt for encryption
				// Note that we use the standard PHP functions to do the crypt
				mcrypt_encrypt(
					// Encryption with rijndael, the AES standard
					MCRYPT_RIJNDAEL_256,
					// We provide the application hash
					$this->_getApplicationHashEncryption(),
					// We provide the value that needs to be encrypted
					$value, 
					// The simplest of the encryption modes is the electronic codebook (ECB) mode.
					// The message is divided into blocks and each block is encrypted separately.
					MCRYPT_MODE_ECB, 
					// The initialization vector (IV) is a fixed-size input to a cryptographic primitive
					// that is typically required to be random or pseudorandom
					mcrypt_create_iv(
						mcrypt_get_iv_size(
							MCRYPT_RIJNDAEL_256, 
							MCRYPT_MODE_ECB
						), 
						MCRYPT_RAND
					)
				)
			));
	}
	
	/**
	 * Get db value decrypted
	 * 
	 * @param string $value
	 * @return string
	 */
	private function _getObjectValueDecrypted($value) {
		// If the value is empty: stop encryption and decryption
		if(!$value) {
			return $value;
		}
		
		return
			// We use mcrypt for decryption
			// Note that we use the standard PHP functions to do the crypt
			trim(mcrypt_decrypt(
				// Decrypt with rijndael, the AES standard
				MCRYPT_RIJNDAEL_256, 
				// We provide the application hash
				$this->_getApplicationHashEncryption(), 
				// We provide the value that needs to be decrypted
				base64_decode($value), 
				// The simplest of the encryption modes is the electronic codebook (ECB) mode.
				// The message is divided into blocks and each block is encrypted separately.
				MCRYPT_MODE_ECB,
				// The initialization vector (IV) is a fixed-size input to a cryptographic primitive
				// that is typically required to be random or pseudorandom
				mcrypt_create_iv(
					mcrypt_get_iv_size(
						MCRYPT_RIJNDAEL_256,
						MCRYPT_MODE_ECB
					), 
					MCRYPT_RAND
				)
            ));
	}
	
	/**
	 * Get application hash encryption
	 * 
	 * @return string
	 * @throws M_Exception
	 */
	private function _getApplicationHashEncryption() {
		if(! defined('APPLICATION_HASH_ENCRYPTION')) {
			throw new M_Exception(sprintf(
				'Could not find the APPLICATION_HASH_ENCRYPTION to encrypt and decrypt database values'
			));
		}
		
		// Return the encryption hash
		return APPLICATION_HASH_ENCRYPTION;
	}
	
	/* -- PRIVATE / PROTECTED EVENT DISPATCHERS -- */

	/**
	 * Dispatch Object Event
	 *
	 * Will dispatch an {@link M_DataObjectMapperEvent} event, using the singleton
	 * {@link M_DataObjectMapperEventDispatcher} to do so.
	 *
	 * @access protected
	 * @param string $type
	 * 		The event type; see {@link M_DataObjectMapperEvent::__construct()}
	 * @param M_DataObject $object
	 * 		The data object; see {@link M_DataObjectMapperEvent::setDataObject()}
	 * @return void
	 */
	protected function _dispatchDataObjectEvent($type, M_DataObject $object) {
		// Construct the event:
		$event = new M_DataObjectMapperEvent($type, $this);
		$event->setDataObject($object);

		// Dispatch the event:
		$this->_getDataObjectMapperEventDispatcher()->dispatchEvent($event);
	}
	
	/**
	 * Dispatch Mapper Event
	 * 
	 * Will dispatch an {@link M_DataObjectMapperEvent} event, using the singleton
	 * {@link M_DataObjectMapperEventDispatcher} to do so.
	 * 
	 * @access private
	 * @param string $type
	 * 		The event type; see {@link M_DataObjectMapperEvent::__construct()}
	 * @return void
	 */
	private function _dispatchDataObjectMapperEvent($type) {
		// Construct the event:
		$event = new M_DataObjectMapperEvent($type, $this);
		
		// Dispatch the event:
		$this->_getDataObjectMapperEventDispatcher()->dispatchEvent($event);
	}
	
	/**
	 * Get event dispatcher
	 * 
	 * @access private
	 * @return M_DataObjectMapperEventDispatcher
	 */
	private function _getDataObjectMapperEventDispatcher() {
		return M_DataObjectMapperEventDispatcher::getInstance();
	}
}