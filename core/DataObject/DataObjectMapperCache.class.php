<?php
abstract class M_DataObjectMapperCache extends M_DataObjectMapper {
	/**
	 *
	 * @var M_CachePhp
	 */
	private $_cacheLocalizedData;

	/**
	 * Get a cache object
	 *
	 * @return M_CachePHP
	 */
	protected function _getCacheLocalizedData() {
		if ($this->_cacheLocalizedData == null) {
			$this->_cacheLocalizedData = new M_CachePHP(
				M_Loader::getAbsolute('files/cache-localized')
			);
		}
		return $this->_cacheLocalizedData;
	}

	/**
	 * Get the localized data from the database for a set of text-ids
	 * 
	 * @param array $ids
	 * @param string $locale
	 * @param array $localizedData
	 * @return array
	 */
	protected function _getDbLocalizedObjectData(array $ids, $locale, $localizedData = null) {
		//create if cache did not yet exist
		if(!$localizedData) {
			$localizedData = array($locale => array());
		}
		//create if no cache for this locale exists yets
		if(!isset($localizedData[$locale])) {
			$localizedData[$locale] = array();
		}

		// Compose SELECT statement to fetch the localized fields
		$select = $this->_getLocalizedTextTable()->select();
		$select->columns(array('text_id', 'text_locale', 'text_value'));
		$select->whereIn('text_id', array_keys($ids));
		$select->where('text_locale = ?', $locale);

		foreach($select->execute()->getAll() AS $data) {
			//use the setter as identification for this field
			$setter = (string) $ids[$data['text_id']];

			//store this data for this locale and this field (identified by the setter)
			$localizedData[$locale][$setter] = array(
				'text_id' => (int) $data['text_id'],
				'text_value' => is_null($data['text_value']) ? NULL : (string) $data['text_value'],
			);
		}

		return $localizedData;
	}

	/**
	 * Apply a set of localized data into an object
	 *
	 * When we have a set of localized data (from cache or from db) we still
	 * need to apply it into the object, so all localized data is stored into
	 * the object
	 *
	 * @param M_DataObject $object
	 * @param array $localizedData
	 * @param array $ids The id's which refer to the localized-table (..._text). Array(2 => name, 3 => description, ...)
	 * @param string $locale
	 * @return array
	 */
	protected function _setLocalizedDataInObject(M_DataObject $object, array $localizedData, array $ids, $locale) {
		// if the result set is valid, and it contains at least 1 record:
		if(isset($localizedData[$locale]) && $localizedData[$locale] !== FALSE && count($localizedData[$locale]) > 0) {
			// for each of the records:
			foreach($localizedData[$locale] as $row) {
				
				// Set the text in the object:
				$fieldName = isset($ids[$row['text_id']]) ? $ids[$row['text_id']] : NULL;

				//check if the object allready contains data: if not: set the cache data into the object
				$objectValue = call_user_func(array($object, 'getLocalizedProperty'), $fieldName, $locale);
				if($objectValue === null) {
					call_user_func(array($object,'setLocalizedProperty'), $fieldName, $row['text_value'], $locale);
				}
				//it seems like the object has other values than the ones
				//which we have received from cache or database. We update
				//this data ($localizedData)
				else {
					$localizedData[$locale][(string)$fieldName]['text_value'] = is_null($objectValue) ? NULL : (string) $objectValue;
				}
			}
		}

		return $localizedData;
	}

	/**
	 * Get all cache-data for an object
	 * 
	 * @param M_DataObject $object
	 * @return array
	 */
	protected function _getCacheLocalizedObjectData(M_DataObject $object) {
		return $this->_getCacheLocalizedData()->read(
			$this->_getCacheObjectHash($object)
		);
	}

	/**
	 * Remove all cache-data for an object
	 *
	 * @param M_DataObject $object
	 * @return bool
	 */
	protected function _removeCacheLocalizedObjectData(M_DataObject $object) {
		return $this->_getCacheLocalizedData()->remove(
			$this->_getCacheObjectHash($object)
		);
	}

	/**
	 * Set all cache-data for an object
	 *
	 * @param M_DataObject $object
	 * @param array
	 * @return M_DataObjectMapperCache
	 */
	protected function _setCacheLocalizedObjectData(M_DataObject $object, array $localizedData) {
		$this->_getCacheLocalizedData()->write(
			$this->_getCacheObjectHash($object),
			$localizedData
		);

		return $this;
	}
	
	/**
	 * Get localized texts
	 *
	 * @todo add additional comments
	 *
	 * @access public
	 * @param M_DataObject $object
	 * 		The object of which to set localized texts
	 * @param string $locale
	 * 		The language in which to set texts
	 * @return void
	 */
	public function setLocalizedTextsIn(M_DataObject $object, $locale = NULL) {
		// If no locale has been provided, we fetch the locale that
		// has been set in the locale category LC_MESSAGES
		if(! $locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
			if(! $locale) {
				// do not continue, if no locale can be found at all
				return;
			}
		}

		// Info of the fields of which we fetch the translations:
		// [id] => setter
		$ids = array();
		// For each of the localized fields:
		foreach($this->getLocalizedFields() as $field) {
			// Get the numeric value. This value is the ID of the translated
			// text, which is stored in the auxiliary table
			$id = call_user_func(array($object, (string) $field->getter.'Id'), $locale);

			// If the numeric value is available, we will have to fetch
			// the corresponding localized text. We store this value,
			// for later reference:
			if($id) {
				$ids[$id] = (string) $field->name;
			}
		}
		
		// If at least 1 localized field should be fetched from database:
		if(count($ids) > 0) {
			// Use cache?
			$localizedData = $this->_getCacheLocalizedObjectData($object);

			//cache-file exists and cache-data for this locale is available
			$cacheExists = ($localizedData && isset($localizedData[$locale]));

			//if cache was removed (on the server), we need to save it again
			if(!$cacheExists) {
				$localizedData = $this->_getDbLocalizedObjectData($ids, $locale, $localizedData);

				//store the cache on the filesystem
				$this->_setCacheLocalizedObjectData(
					$object,
					$localizedData
				);
			}
			
			//apply the localized-data into the object
			$this->_setLocalizedDataInObject(
				$object,
				$localizedData,
				$ids,
				$locale
			);
		}
	}

	/**
	 * Update object and store cache
	 *
	 * @param M_DataObject $object
	 * @return bool
	 */
	public function update(M_DataObject $object) {
		//update fails?
		if(!parent::update($object)) return false;

		//if this is a localized object: save the cache
		if($object->isLocalized()) {
			$this->_setCacheLocalizedObjectData(
				$object,
				$this->_getLocalizedDataFromObject($object)
			);
		}
		
		return true;
	}

	/**
	 * Insert a new object into the database, and create cache
	 * 
	 * @param M_DataObject $object
	 * @return bool
	 */
	public function insert(M_DataObject $object) {
		//insert fails?
		if(!parent::insert($object)) return false;

		//if this is a localized object: save the cache
		if($object->isLocalized()) {
			$this->_setCacheLocalizedObjectData(
				$object,
				$this->_getLocalizedDataFromObject($object)
			);
		}

		return true;
	}

	/**
	 * Get an array containing all localized-data
	 *
	 * This data is used for cache
	 *
	 * @param M_DataObject $object
	 * @return bool
	 */
	public function _getLocalizedDataFromObject(M_DataObject $object) {
		$localizedData = array();
		
		//for every locale
		foreach(M_LocaleMessageCatalog::getInstalled() as $locale) {
			
			$localizedData[$locale] = array();

			//iterate throug all localized fields
			foreach($this->getLocalizedFields() AS $field) {
				//get the value for this field out of the object
				$value = call_user_func(array($object, "getLocalizedProperty"), (string)$field->name, $locale);
				$id = call_user_func(array($object, (string) $field->getter . 'Id'), $locale);
				if ($id) {
					$localizedData[$locale][(string) $field->name] = array(
						'text_value' => is_null($value) ? NULL : (string) $value,
						'text_id' => (int) $id
					);
				}
			}
		}

		return $localizedData;
	}

	/**
	 * Delete object and cache
	 * 
	 * @param M_DataObject $object
	 * @return bool
	 */
	public function delete(M_DataObject $object) {
		$this->_removeCacheLocalizedObjectData($object);
		return parent::delete($object);
	}

	/**
	 * Create a hash which identifies the object in the cache-data
	 *
	 * @param M_DataObject $object
	 * @return string
	 */
	protected function _getCacheObjectHash(M_DataObject $object) {
		return $object->getClassName() . '-' . $object->getId();
	}
}