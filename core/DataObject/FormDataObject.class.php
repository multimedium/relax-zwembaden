<?php
/**
 * M_FormDataObject
 *
 * @package Core
 */
class M_FormDataObject extends M_Form {
	
	/* -- PROPERTIES -- */
	
	/**
	 * DataObject
	 * 
	 * This property stores the {@link M_DataObject} instance that is
	 * being edited by the form.
	 *
	 * @see M_FormDataObject::setDataObject()
	 * @access protected
	 * @var M_DataObject
	 */
	protected $_dataObject;
	
	/**
	 * Flag: Is DataObject localized?
	 * 
	 * This property stores a (boolean) flag that tells whether or not the data
	 * object is localized.
	 * 
	 * @access protected
	 * @var bool
	 */
	protected $_dataObjectLocalized = FALSE;
	
	/**
	 * Locale
	 * 
	 * This property stores the locale in which the {@link M_DataObject} instance
	 * is to be edited
	 * 
	 * @access private
	 * @var string
	 */
	private $_locale;
	
	/**
	 * Reference Locale
	 * 
	 * This property stores the reference locale. For more info, read the docs
	 * on {@link M_FormDataObject::setLocaleReference()}
	 * 
	 * @access private
	 * @var string
	 */
	private $_localeReference;
	
	/**
	 * Getters and setters
	 * 
	 * This method stores the getter and setter methods that need to be invoked
	 * on the data object, to set and get the values of the contained form fields.
	 * For more info, read
	 * 
	 * - {@link M_FormDataObject::setDataObjectGetter()}
	 * - {@link M_FormDataObject::setDataObjectSetter()}
	 * 
	 * @access private
	 * @var array
	 */
	private $_getterSetter = array(
		'setters' => array(),
		'getters' => array()
	);
	
	/**
	 * Are values updated?
	 * 
	 * @see M_FormDataObject::setUpdatedValuesInDataObject()
	 * @access private
	 * @var boolean
	 */
	private $_valuesUpdated;
	
	/* -- SETTERS -- */
	
	/**
	 * Add form definition
	 * 
	 * This method is used by {@link M_Form::factory()}, to mount 
	 * the form with a given definition. This method overrides
	 * {@link M_Form::set()}, to make the following (additional)
	 * definition keys available:
	 * 
	 * <code>dataObject</code>
	 * 
	 * Sets the Data Object that is to be edited by the form. For more info,
	 * read {@link M_FormDataObject::setDataObject()}
	 * 
	 * <code>dataObjectGetters</code>
	 * 
	 * Sets the Data Object Getter(s). The value of this definition will be 
	 * treated as an iterator, of which the keys are the Field ID's and the 
	 * values are the getter method names. For more info, read the docs on 
	 * {@link M_FormDataObject::setDataObjectGetter()}.
	 * 
	 * <code>dataObjectSetters</code>
	 * 
	 * Sets the Data Object Setter(s). The value of this definition will be 
	 * treated as an iterator, of which the keys are the Field ID's and the 
	 * values are the setter method names. For more info, read the docs on 
	 * {@link M_FormDataObject::setDataObjectSetter()}.
	 * 
	 * Example 1
	 * <code>
	 *    // Mount the form
	 *    $form = M_Form::factory('myForm', new M_Config(array(
	 *       'type' => 'dataObject',
	 *       'dataObject' => new NewsItem(), // Assuming NewsItem is a subclass of M_DataObject
	 *       'dataObjectGetter' => array(
	 *          'title' => 'getTitle',
	 *          'description' => 'getDescription'
	 *       ),
	 *       'dataObjectSetter' => array(
	 *          'title' => 'setTitle',
	 *          'description' => 'setDescription'
	 *       )
	 *    )));
	 * </code>
	 * 
	 * @access public
	 * @see M_Form::factory()
	 * @see M_Form::set()
	 * @param string $key
	 * 		The field definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @return void
	 */
	public function set($spec, $definition) {
		switch($spec) {
			// Set data object
			case 'dataObject':
				// Make sure we're dealing with an instance of M_DataObject here!
				if(! M_Helper::isInstanceOf($definition, 'M_DataObject')) {
					// If not, we throw an exception:
					throw new M_FormException(sprintf(
						'Expecting an instance of M_DataObject from "%s" in Form Definition',
						$spec
					));
				}
				
				// Set the data object:
				$this->setDataObject($definition);
				break;
			
			// data object setters
			case 'dataObjectGetters':
				// Make sure we're dealing with an iterator of getters!
				if(! M_Helper::isIterator($definition)) {
					// If not, we throw an exception:
					throw new M_FormException(sprintf(
						'Expecting an array/iterator from "%s" in Form Definition',
						$spec
					));
				}
				
				// For each of the setters:
				foreach($definition as $fieldId => $methodName) {
					// Add the setter to the form:
					$this->setDataObjectGetter($fieldId, $methodName);
				}
				break;
			
			// data object setters
			case 'dataObjectSetters':
				// Make sure we're dealing with an iterator of setters!
				if(! M_Helper::isIterator($definition)) {
					// If not, we throw an exception:
					throw new M_FormException(sprintf(
						'Expecting an array/iterator from "%s" in Form Definition',
						$spec
					));
				}
				
				// For each of the setters:
				foreach($definition as $fieldId => $methodName) {
					// Add the setter to the form:
					$this->setDataObjectSetter($fieldId, $methodName);
				}
				break;
			
			default:
				parent::set($spec, $definition);
				break;
		}
	}
	
	/**
	 * Add field
	 * 
	 * @see M_Form::addField()
	 * @param M_Field $field
	 */
	public function addField(M_Field $field) {
		// When a field is added, we set the default value to the 
		// value we get from the attached data object
		if($this->_dataObject) {
			$this->_setFieldDefaultValueFromDataObject($field, $this->_dataObject);
		}
		
		// Add the field:
		parent::addField($field);
	}
	
	/**
	 * Add group of fields
	 * 
	 * @see M_Form::addFieldGroup()
	 * @param M_FieldGroup $group
	 */
	public function addFieldGroup(M_FieldGroup $group) {
		// When a group is added, we set the default value of the contained field(s)
		// to the value we get from the attached data object
		if($this->_dataObject) {
			// For each of the contained fields:
			foreach($group->getFields() as $field) {
				// Set the default value:
				$this->_setFieldDefaultValueFromDataObject($field, $this->_dataObject);
			}
		}
		
		// Add the field:
		parent::addFieldGroup($group);
	}
	
	/**
	 * Set DataObject
	 * 
	 * Will set the {@link M_DataObject} that is being edited/created 
	 * by the form.
	 *
	 * NOTE:
	 * The data object will overwrite the default values of the fields in the form
	 * with its properties.
	 *
	 * @access public
	 * @param M_DataObject $object
	 * 		The data object that is being edited by the form
	 * @return void
	 */
	public function setDataObject(M_DataObject $object) {
		// Set the data object:
		$this->_dataObject = $object;
		$this->_dataObjectLocalized = $object->isLocalized();
		
		// If fields have already been attached to the form, we override
		// the default values of these fields:
		foreach($this->getFields() as $field) {
			// Set the default value of the current field:
			$this->_setFieldDefaultValueFromDataObject($field, $this->_dataObject);
		}
	}
	
	/**
	 * Setters
	 * 
	 * Defines the object's Setter method that is to be used to save a given 
	 * field's value to the data object.
	 * 
	 * @access public
	 * @param string $fieldId
	 * 		The field's ID; see {@link M_Field::getId()}
	 * @param string $setterFunctionName
	 * 		The name of the Setter method
	 * @return void
	 */
	public function setDataObjectSetter($fieldId, $setterFunctionName) {
		$this->_getterSetter['setters'][(string) $fieldId] = (string) $setterFunctionName;
	}
	
	/**
	 * Getters
	 * 
	 * Defines the object's Getter method that is to be invoked, in order to 
	 * populate a given field in the form with a default value.
	 * 
	 * @access public
	 * @param string $fieldId
	 * 		The field's ID; see {@link M_Field::getId()}
	 * @param string $getterFunctionName
	 * 		The name of the Setter method
	 * @return void
	 */
	public function setDataObjectGetter($fieldId, $getterFunctionName) {
		$this->_getterSetter['getters'][(string) $fieldId] = (string) $getterFunctionName;
	}
	
	/**
	 * Set locale
	 * 
	 * This method can be used to set the locale in which the {@link M_DataObject}
	 * instance is to be edited.
	 * 
	 * @access public
	 * @param string $locale
	 * 		The locale in which the data object is to be edited
	 * @return void
	 */
	public function setLocale($locale) {
		$this->_locale = (string) $locale;
	}
	
	/**
	 * Set locale reference
	 * 
	 * If the data object is being edited in a given locale, you can also set a
	 * reference locale. This reference locale is then used to fetch values from
	 * the data object. These values are then used as reference values with each
	 * field in the form. This way, the user can compare the text with the text 
	 * in the reference locale.
	 * 
	 * @access public
	 * @param string $locale
	 * 		The reference locale
	 * @return void
	 */
	public function setLocaleReference($locale) {
		$this->_localeReference = (string) $locale;
	}
	
	/**
	 * Set updated values in Data Object
	 * 
	 * Will copy the Form Values to the Data Object. Typically, this method is 
	 * used in {@link M_FormDataObject::actions()}, in order to apply the changes 
	 * made in the form, to the data object. 
	 * 
	 * Note however that this method will *not* store the changes to the database!
	 * It does enable you though to apply the changes to the 
	 * 
	 * @see M_FormDataObject::actions()
	 * @access public
	 * @return void
	 */
	public function setUpdatedValuesInDataObject() {
		// If already updated:
		if($this->_valuesUpdated) {
			// We do not do anything
			return;
		}
		
		// Get the Form Values:
		$values = $this->getValues();
		
		// For each of the fields in the form:
		foreach($this->getFields() as $field) {
			// We set the new value in the data object. If the field is
			// featured in the received values:
			if(array_key_exists($field->getId(), $values)) {
				// Then use that value
				$this->_setDataObjectValue($field->getId(), $values[$field->getId()], $this->_dataObject);
			}
			// Otherwise, if not featured
			else {
				// Use the deliver value of the field
				$this->_setDataObjectValueFromField($field, $this->_dataObject);
			}
		}
		
		// For each of the hidden variables in the form:
		foreach($this->getVariables() as $key => $value) {
			// We set the new value in the data object:
			$this->_setDataObjectValue($key, $value, $this->_dataObject);
		}
		
		// Values have been copied:
		$this->_valuesUpdated = TRUE;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get DataObject
	 *
	 * @see M_FormDataObject::setDataObject()
	 * @access public
	 * @return M_DataObject
	 */
	public function getDataObject() {
		return $this->_dataObject;
	}
	
	/**
	 * Get locale
	 * 
	 * Will provide with the locale in which the data object is to be edited by 
	 * the form. Note that this method will return the locale that has been provided
	 * to {@link M_FormDataObject::setLocale()}. 
	 * 
	 * However, if no locale has been provided to that method, this method will 
	 * return the locale that has been set in the locale category 
	 * {@link M_Locale::LC_MESSAGES}. For more information about locale
	 * categories, read the docs on {@link M_Locale::getCategory()}
	 * 
	 * NOTE:
	 * This method will throw an exception, if no locale could have been found
	 * in the locale category either.
	 * 
	 * @throws M_FormException
	 * @access public
	 * @return string
	 */
	public function getLocale() {
		// If a locale has been provided to the form:
		if($this->_locale) {
			// provide that value:
			return $this->_locale;
		}
		
		// If no locale has been provided, we default to the one that has been
		// set in the locale category LC_MESSAGES
		$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
		
		// If we cannot find a locale in that category, we throw an exception
		if(! $locale) {
			throw new M_FormException(sprintf(
				'Could not determine the locale to be used in %s; please use %s::%s()',
				__CLASS__,
				__CLASS__,
				'setLocale'
			));
		}
		
		// Return the locale:
		return $locale;
	}
	
	/**
	 * Get locale reference
	 * 
	 * Will provide with the locale that has been set as the reference locale.
	 * For more info, read {@link M_FormDataObject::setLocaleReference()}.
	 *
	 * However, if no locale has been provided as a reference, this method will
	 * return the currently selected locale: {@link M_LocaleMessageCatalog::getCategory()}
	 * 
	 * If no locale has been selected, this method will return the first (installed)
	 * locale it can find. For more info, read the docs on
	 * {@link M_LocaleMessageCatalog::getInstalled()}
	 * 
	 * NOTE:
	 * This method will throw an exception, if no locale could have been found
	 * whatsoever as a reference.
	 * 
	 * @throws M_FormException
	 * @access public
	 * @return string
	 */
	public function getLocaleReference() {
		// If a locale has been provided to the form:
		if($this->_localeReference) {
			// provide that value:
			return $this->_localeReference;
		}

		// Get the currently selected locale
		$locale = M_Locale::getCategory(M_Locale::LANG);

		// If not the same as the reference locale:
		if($locale && $locale != $this->getLocale()) {
			// Then, provide that locale
			return $locale;
		}
		
		// If no locale has been provided, we default to the first installed locale
		// we can find:
		$locales = M_LocaleMessageCatalog::getInstalled();
		
		// If no installed locales have been found, we throw an exception:
		if(count($locales) > 0) {
			// Get the first locale:
			$locale = array_shift($locales);
			
			// If the locale is the same as the locale that is being used to 
			// edit the data object, we try to fetch another one:
			if($locale == $this->getLocale()) {
				// Fetch another one:
				$another = array_shift($locales);
				
				// If another one has been found:
				if($another) {
					// return that one:
					return $another;
				}
			}
			
			// Return the first locale, if still here:
			return $locale;
		}
		
		// Throw an exception, if still here:
		throw new M_FormException(sprintf(
			'Could not determine the reference locale to be used in %s; please use %s::%s()',
			__CLASS__,
			__CLASS__,
			'setLocaleReference'
		));
	}
	
	/**
	 * Is locale-aware?
	 * 
	 * Will tell whether or not a locale is being used to edit the data object.
	 * For more info, read the docs on {@link M_FormDataObject::getLocale()}
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the data object is being edited in a locale, FALSE
	 * 		if not.
	 */
	public function hasLocale() {
		// If a locale has been provided to the form:
		if($this->_locale) {
			// Then, we return TRUE
			return TRUE;
		}
		
		// If no locale has been provided, we default to the one that has been
		// set in the locale category LC_MESSAGES
		if(! M_Locale::getCategory(M_Locale::LC_MESSAGES)) {
			// If we cannot find a locale in that category, we return FALSE
			return FALSE;
		}
		
		// If still here, return TRUE
		return TRUE;
	}
	
	/* -- SUPPORT FOR M_Form -- */
	
	/**
	 * Form actions
	 *
	 * @see M_Form::actions()
	 * @access public
	 * @param array $values
	 * 		The values that have been collected by the method
	 * 		{@link M_Form::getValues()}.
	 * @return unknown
	 */
	public function actions(array $values) {
		// Set updated values:
		$this->setUpdatedValuesInDataObject();
		
		// Save the changes, and return the result:
		return $this->_dataObject->save();
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Set value in data object, based on a field in the form
	 * 
	 * @access private
	 * @param M_Field $field
	 * 		The field from which to get the value
	 * @param M_DataObject $object
	 * 		The data object in which to set the value
	 * @return void
	 */
	private function _setDataObjectValueFromField(M_Field $field, M_DataObject $object) {
		$this->_setDataObjectValue($field->getId(), $field->deliver(), $object);
	}
	
	/**
	 * Set a value in a data object
	 * 
	 * Using the id of the field, which corresponds with the property of the
	 * dataobject, and the value itself we can set a value in the dataobject
	 * 
	 * @param string $fieldId
	 * @param mixed $fieldValue
	 * @param M_DataObject $object
	 */
	private function _setDataObjectValue($fieldId, $fieldValue, M_DataObject $object) {
		// Normally, we would set the new value with the object's setter method 
		// (which is provided to us by the corresponding mapper). First though, 
		// we check whether or not a different setter has been specified by
		// M_FormDataObject::setDataObjectSetter()
		if(isset($this->_getterSetter['setters'][$fieldId])) {
			// Yes, so we compose the setter's name:
			$objectSetter = (string) $this->_getterSetter['setters'][$fieldId];
			
			// And then invoke it on the object:
			$object->$objectSetter($fieldValue, $this->getLocale());
		}
		// If no specific setter has been specified, we use the normal
		// object setter method:
		else {
			// If the data object is localized:
			if($object->isLocalized()) {
				// We ask the data object for its mapper:
				$mapper = $object->getMapper();
				
				// Get the field definition from the data object mapper:
				$objectField  = $mapper->getField((string) $fieldId);
				
				// If found:
				if($objectField) {
					// Get the setter:
					$objectSetter = (string) $objectField->setter;
					
					// If the field is localized
					if($mapper->isLocalizedField($objectField)) {
						// Get the locale that has been assigned to the form. We need to make
						// the changes in this locale:
						$locale = $this->getLocale();
						
						// We add the locale to the mix, in order to set the new value
						// of the field:
						$object->$objectSetter($fieldValue, $locale);
					}
					// If the field is not localized:
					else {
						// We simply set the new value:
						$object->$objectSetter($fieldValue);
					}
				}
			}
			// If the data object is not localized:
			else {
				// Set the value with the magic setter:
				$object->$fieldId = $fieldValue;
			}
		}
	}
	
	/**
	 * Set default value in field, based on data object
	 * 
	 * @access private
	 * @param M_Field $field
	 * 		The field in which to set the default value
	 * @param M_DataObject $object
	 * 		The data object from which to fetch the default value
	 * @return void
	 */
	private function _setFieldDefaultValueFromDataObject(M_Field $field, M_DataObject $object) {
		// We set the default value to the value we get from the attached data object
		// First, we get the ID of the field:
		$id = $field->getId();
		
		// We only continue with setting the default value, if 
		// - the form has not yet been sent, and
		if(! $this->isSent()) {
			// Yes, we need to set the default value! 
			// Normally, we would do this with the object's getter method (which
			// is provided to us by the corresponding mapper). First though, we
			// check whether or not a different getter has been specified by
			// M_FormDataObject::setDataObjectGetter()
			if(isset($this->_getterSetter['getters'][$id])) {
				// Yes, so we compose the getter's name:
				$objectGetter = (string) $this->_getterSetter['getters'][$id];
				
				// And then invoke it on the object:
				$fieldValue = $object->$objectGetter($this->getLocale());

				// If the field's value is not NULL, then we set it as the default
				// value of the corresponding form field
				if(! is_null($fieldValue) || ! $object->isNew()) {
					$field->setDefaultValue($fieldValue);
				}
			}
			// If no specific getter has been specified, we use the normal
			// object getter method:
			else {
				// Get the mapper of the object:
				$mapper = $object->getMapper();
				
				// Get the data object field:
				$objectField  = $mapper->getField($id);
				
				// If the field has been found:
				if($objectField) {
					// Get the field's getter method:
					$objectGetter = (string) $objectField->getter;
					
					// If the field is localized:
					if($mapper->isLocalizedField($objectField)) {
						// In that case, we use the locale to fetch the value:
						$fieldValue = $object->$objectGetter($this->getLocale());
					}
					// If the field is not localized
					else {
						// we use the magic getter
						$fieldValue =  $object->$id;
					}

					// If the field's value is not NULL, then we set it as the default
					// value of the corresponding form field
					if(! is_null($fieldValue) || ! $object->isNew()) {
						$field->setDefaultValue($fieldValue);
					}
				}
			}
		}
	}
}