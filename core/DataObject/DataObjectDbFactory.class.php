<?php
/**
 * M_DataObjectDbFactory class
 *
 * @package Core
 */
class M_DataObjectDbFactory {
	/**
	 * M_DataObjectMapper
	 * 
	 * This property stores the mapper that interprets the way in which
	 * the {@link M_DataObject} is stored and retrieved.
	 * 
	 * @access private
	 * @var M_DataObjectMapper
	 */
	private $_mapper;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param M_DataObjectMapper $mapper
	 * 		The mapper that will be used, in order to extract info
	 * 		about the Object/Mapper/Model
	 * @return M_DataObjectClassFactory
	 */
	public function __construct(M_DataObjectMapper $mapper = NULL) {
		if($mapper) {
			$this->setDataObjectMapper($mapper);
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get table(s)
	 * 
	 * This method will return objects that implement {@link MI_DbTable},
	 * representing the database tables that need to be installed for
	 * the model that is described by the mapper.
	 * 
	 * The return value is an ArrayIterator object, that contains a
	 * collection of {@link MI_DbTable} objects.
	 * 
	 * @access public
	 * @param string $name
	 * 		The base-name of the tabel(s). If localized attributes are
	 * 		available, a second table will be included (which appends
	 * 		"_text" to the table name
	 * 
	 * 		By default this parameter will be set to the tablename retrieved by
	 * 		the mapper. This tablename is set in the object's xml file
	 * @return ArrayIterator
	 */
	public function getTables($name = null) {
		// Collection of tables is stored in $out
		$out = array();
		
		// If no name is specified, we use the mapper table name
		if (is_null($name)) $name = $this->_mapper->getTableName();
		
		// Get the localized fields in the object:
		$localized = array();
		foreach($this->_mapper->getLocalizedFields() as $field) {
			$localized[(string) $field->name] = $field;
		}
		
		// Construct a new table for the model:
		$table = $this->_mapper->getDb()->getNewTable();
		$table->setName($name);

		//Set the egine if set
		$tableEngine = $this->_mapper->getTableEngine();
		if ($tableEngine) $table->setEngine ($tableEngine);

		// Prepare the primary key of the table:
		$primary = new M_DbIndex('PRIMARY');
		$primary->setType(M_DbIndex::PRIMARY);
		
		// For each of the fields in the mapper:
		foreach($this->_mapper->getFields() as $field) {
			// Get the name of the database table column:
			$dbName = (string) $field->db_name;
			
			// if a name has been provided:
			if($dbName) {
				// if the field is not localized:
				if(!isset($localized[(string) $field->name])) {
					// We create a new column object, to represent the 
					// current field:
					$column = new M_DbColumn((string) $field->db_name);
					
					// Required attributes for additional properties:
					$required = array();
					
					// Set the type of the column:
					switch(strtolower((string) $field->db_type)) {
						case 'varchar':
							$column->setType(M_DbColumn::TYPE_VARCHAR);
							$required['length'] = 'setLength';
							break;
						
						case 'text':
							$column->setType(M_DbColumn::TYPE_TEXT);
							break;
						
						case 'blob':
							$column->setType(M_DbColumn::TYPE_BLOB);
							break;
						
						case 'int':
						case 'integer':
							$column->setType(M_DbColumn::TYPE_INTEGER);
							$required['length'] = 'setLength';
							break;
						
						case 'tinyint':
						case 'tinyinteger':
							$column->setType(M_DbColumn::TYPE_TINY_INTEGER);
							$required['length'] = 'setLength';
							break;
						
						case 'float':
							$column->setType(M_DbColumn::TYPE_FLOAT);
							break;
							
						case 'date':
							$column->setType(M_DbColumn::TYPE_DATE);
							break;
							
						case 'datetime':
							$column->setType(M_DbColumn::TYPE_DATETIME);
							break;
							
						default:
							throw new M_DbException(sprintf(
								'%s: %s: Unrecognized data type "%s"',
								 __CLASS__, 
								 __METHOD__, 
								 $field->db_type)
							);
							break;
					}
					
					// For each of the required attributes:
					foreach($required as $attribute => $setter) {
						// Clean up the attribute value:
						$cleanAttributeValue = isset($field->$attribute)
							? trim((string) $field->$attribute)
							: NULL;
						
						// If not provided:
						if(! $cleanAttributeValue) {
							// Then, we throw an exception to inform about the
							// malformed definition:
							throw new M_Exception(sprintf(
								'Cannot create column `%s`; Missing a value for ' . 
								'attribute "%s", and can therefore not call ' . 
								'the setter %s::%s()',
								$column->getName(),
								$attribute,
								get_class($column),
								$setter
							));
						}
						
						// Set the attribute value, with the column's setter:
						$column->$setter($cleanAttributeValue);
					}
				}
				// If the field is localized:
				else {
					// We create a new column object, to represent the 
					// current field:
					$column = new M_DbColumn((string) $field->db_name);
					
					// We force the data type of the column to INTEGER.
					// This column will be storing a reference to translated
					// texts in other table
					$column->setType(M_DbColumn::TYPE_INTEGER);
				}
				
				// If the current field is defined as part of primary
				// key, we add it to the key definition:
				if(isset($field['primary_key']) && strtolower($field['primary_key']) == 'true') {
					$primary->addColumn($column);
					
					// Also, we enable the AUTO-INCREMENT option on
					// the column:
					$column->setIsAutoIncrement(TRUE);
				}
				
				// For every not-primary key field check if the current field can be NULL
				elseif (isset($field['null']) && strtolower($field['null'] == 'true')) {
					$column->setIsNull(true);
				}
				
				// If the current field is defined as index
				if(isset($field['index']) && strtolower($field['index'] == 'true')) {
					$index = new M_DbIndex($column->getName());
					$index->addColumn($column);
					$table->addIndex($index);
				}
				
				// If the current field is defined as index
				if(isset($field['unique']) && strtolower($field['unique'] == 'true')) {
					$index = new M_DbIndex($column->getName());
					$index->setType(M_DbIndex::UNIQUE);
					$index->addColumn($column);
					$table->addIndex($index);
				}
				
				// Add the column to the table:
				$table->addColumn($column);
			}
		}
		
		// Set the primary key in the table:
		$table->setPrimaryKey($primary);
		
		// Add the table to the collection:
		$out[] = $table;
		
		// If at least 1 localized field is available:
		if(count($localized) > 0) {
			// Construct a new table for the translated texts:
			$table = $this->_mapper->getDb()->getNewTable();
			$table->setName($name . '_text');
			
			// Set up the fields of the table:
			$id = new M_DbColumn('text_id');
			$id->setType(M_DbColumn::TYPE_INTEGER);
			$id->setIsAutoIncrement(TRUE);
			$table->addColumn($id);
			
			$loc = new M_DbColumn('text_locale');
			$loc->setType(M_DbColumn::TYPE_VARCHAR);
			$loc->setLength(8);
			$table->addColumn($loc);
			
			$val = new M_DbColumn('text_value');
			$val->setType(M_DbColumn::TYPE_TEXT);
			$table->addColumn($val);

			// Add an index for localized fields
			/* @todo test if seperate index on localized field is faster
			$index = new M_DbIndex('text_locale');
			$index->setType(M_DbIndex::INDEX);
			$index->addColumn($loc);
			$table->addIndex($index);
			*/
			// Prepare the primary key of the table:
			$primary = new M_DbIndex('PRIMARY');
			$primary->setType(M_DbIndex::PRIMARY);
			$primary->addColumn($id);
			$primary->addColumn($loc);
			
			// Set the primary key in the table:
			$table->setPrimaryKey($primary);
			
			// Add the second table to the output:
			$out[] = $table;
		}
		
		// Return the table:
		return new M_ArrayIterator($out);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set DataObjectMapper
	 * 
	 * This method is used to set the mapper. The mapper provides
	 * {@link M_DataObjectDbFactory} with the information about the
	 * model, so the database tables can be installed.
	 * 
	 * @access public
	 * @param M_DataObjectMapper $mapper
	 * 		The mapper that will be used, in order to extract info
	 * 		about the Object/Mapper/Model
	 * @return void
	 */
	public function setDataObjectMapper(M_DataObjectMapper $mapper) {
		$this->_mapper = $mapper;
	}
}
?>