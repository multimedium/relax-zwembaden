<?php
/**
 * M_DataObjectClassFactory class
 *
 * @package Core
 */
class M_DataObjectClassFactory {
	/**
	 * M_DataObjectMapper
	 * 
	 * This property stores the mapper that interprets the way in which
	 * the {@link M_DataObject} is stored and retrieved.
	 * 
	 * @access private
	 * @var M_DataObjectMapper
	 */
	private $_mapper;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param M_DataObjectMapper $mapper
	 * 		The mapper that will be used, in order to extract info
	 * 		about the Object/Mapper/Model
	 * @return M_DataObjectClassFactory
	 */
	public function __construct(M_DataObjectMapper $mapper = NULL) {
		if($mapper) {
			$this->setDataObjectMapper($mapper);
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get source code of Object class
	 * 
	 * This method can be used to generate the source code of the Object
	 * class in the Object/Mapper/Model pattern.
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the Object class
	 * @return string
	 * @todo use M_Code to generate code
	 */
	public function getSourceOfObjectClass($name) {
		// Module name
		$module = ucfirst($this->_mapper->getModule()->getId());
		
		// DataObject name without DataObject
		$tmpName = str_replace("DataObject", "", $name); //class name without DataObject
		
		// Get the localized fields in the object:
		$localized = array();
		foreach($this->_mapper->getLocalizedFields() as $field) {
			$localized[(string) $field->name] = $field;
		}
		
		// Prepare some white-spaces for the formatting of the source
		// code:
		$nl   = $this->_getNewline(1);
		$nlt1 = $this->_getNewlineWithTabs(1, 1);
		$nlt2 = $this->_getNewlineWithTabs(1, 2);
		
		// We start the source code of the class:
		$out  = '<?php';
		$out .= "$nl/** $nl";
		$out .= " * $name$nl";
		$out .= " * $nl";
		$out .= " * @see $tmpName". 'Mapper' ."$nl";
		$out .= " * @see $tmpName$nl";
		$out .= " * @package App$nl";
		$out .= " * @subpackage $module$nl";
		$out .= " */";
		$out .= $nl;
		$out .= 'class ' . $name . ' extends M_DataObject {';
		$out .= $nlt1;
		
		// First of all, we add the class' properties to the source code.
		// For each of the fields in the object:
		$types = array();
		foreach($this->_mapper->getFields() as $field) {
			// Get the field's name
			$tmpName   = (string) $field->name;
			$tmpSetter = (string) $field->setter;
			$tmpGetter = (string) $field->getter;

			//remember the type of field, and for some type of fields the strongly
			//typed params
			switch ((string)$field->attributes()->type) {
				case 'date':
					$type = 'M_Date';
					$strongType = 'M_Date';
					break;
				default:
					$type = 'mixed';
					$strongType = false;
					break;
			}
			//store for later use
			$types[$tmpName] = array('type' => $type , 'strongtype' => $strongType);
			
			// If the field is a localized field:
			if(isset($localized[$tmpName])) {
				// We prepare the comments on the localized field:
				$title  = "$tmpName ID";
				$desc   = 
						"This property stores the numeric reference to the translated text " .
						"of $tmpName. To get/set the value of the $tmpName, read the documentation on \n\n" . 
						"- {@link $name::$tmpGetter()}\n" . 
						"- {@link $name::$tmpGetter". 'Id' ."()}\n" . 
						"- {@link $name::$tmpSetter()}\n" .
						"- {@link $name::$tmpSetter". 'Id' ."()}";
				$access = 'protected';
				$var    = 'integer';
				
				// Add the comments to the source code:
				$out .= $this->_getPropertyComments($title, $desc, $access, $var);
				
				// Add the property:
				$out .= 	$nlt1;
				$out .= 'protected $_' . ((string) $field->name) . 'Id;';
				$out .= $nlt1;
			}
			// If the field is not localized:
			else {
				// We prepare the comments on the localized field:
				$title  = "$tmpName";
				$desc   = 
						"This property stores the $tmpName. To get/set the value of the " . 
						"$tmpName, read the documentation on \n\n" . 
						"- {@link $name::$tmpGetter()}\n" . 
						"- {@link $name::$tmpSetter()}";
				$access = 'protected';
				$var    = $type;
				
				// Add the comments to the source code:
				$out .= $this->_getPropertyComments($title, $desc, $access, $var);
				
				// Add the property:
				$out .= 	$nlt1;
				$out .= 'protected $_' . ((string) $field->name) . ';';
				$out .= $nlt1;
			}
		}
		
		// We add the getters and setters of localized fields. For each
		// of the localized fields:
		foreach($localized as $fieldName => $field) {
			// We prepare the comments on the getter of the localized field:
			$title  = "Get $fieldName";
			$desc   = 
					"Get $fieldName in the requested language\n\n" .
					"NOTE:\n" . 
					"If no language is provided to this method, this method will use " . 
					"the language that has been set in the locale category LANG. See " . 
					"{@link M_Locale::getCategory()} for more information about locale " .
					"categories.";
			$access = 'public';
			$params = array(
				'locale' => array('string', 'The language in which to get the ' . $fieldName)
			);
			
			// We add the comments on the getter method:
			$out .= 	$this->_getMethodComments($title, $desc, $access, $params, 'string|NULL');
			
			// Add the getter:
			$out .= 	$nlt1;
			$out .= 	'public function '. ((string) $field->getter) .'($locale = NULL) {';
			$out .= 		$nlt2;
			$out .= 		'return $this->_getLocalizedText(\''. $fieldName .'\', $locale);';
			$out .= 		$nlt1;
			$out .= 	'}';
			$out .= 	$nlt1;
			
			// We prepare the comments on the getterId of the localized field:
			$title  = "Get $fieldName ID";
			$desc   = "Get numeric reference to the translated $fieldName .";
			$access = "public";
			$params = array();
			
			// We add the comments on the getterId method:
			$out .= 	$this->_getMethodComments($title, $desc, $access, $params, 'integer');
			
			// Add the getterId:
			$out .= 	$nlt1;
			$out .= 	'public function '. ((string) $field->getter) .'Id() {';
			$out .= 		$nlt2;
			$out .= 		'return $this->_' . $fieldName . 'Id;';
			$out .= 		$nlt1;
			$out .= 	'}';
			$out .= 	$nlt1;
			
			// We prepare the comments on the setter of the localized field:
			$title = "Set $fieldName";
			$desc  = 
					"Set $fieldName, in a given language\n\n" . 
					"NOTE:\n" . 
					"If no language is provided to this method, this method will use " .
					"the language that has been set in the locale category LANG. See " .
					"{@link M_Locale::getCategory()} for more information about locale  " .
					"categories.";
			$access = 'public';
			$params = array(
				'text' => array('string', 'The translated ' . $fieldName),
				'locale' => array('string', 'The language in which to set the ' . $fieldName)
			);
			
			// We add the comments on the setter method:
			$out .= 	$this->_getMethodComments($title, $desc, $access, $params, 'void');
			
			// Add the setter:
			$out .= 	$nlt1;
			$out .= 	'public function '. ((string) $field->setter) .'($text, $locale = NULL) {';
			$out .= 		$nlt2;
			$out .= 		'return $this->_setLocalizedText(\''. $fieldName .'\', $text, $locale);';
			$out .= 		$nlt1;
			$out .= 	'}';
			$out .= 	$nlt1;
			
			// We prepare the comments on the setterId of the localized field:
			$title = "Set $fieldName ID";
			$desc  = "Set numeric reference to the translated $fieldName .";
			$access = 'public';
			$params = array(
				'id' => array('integer', 'The numeric reference value to translations of ' . $fieldName),
			);
			
			// We add the comments on the setterId method:
			$out .= 	$this->_getMethodComments($title, $desc, $access, $params, 'void');
			
			// Add the setterId
			$out .= 	$nlt1;
			$out .= 	'public function '. ((string) $field->setter) .'Id($id) {';
			$out .= 		$nlt2;
			$out .= 		'$this->_' . $fieldName . 'Id = $id;';
			$out .= 		$nlt2;
			$out .=			'return $this;';
			$out .= 		$nlt1;
			$out .= 	'}';
			$out .= 	$nlt1;
		}
		
		// Now, we add all other getters and setters to the class.
		// (Non-localized fields)
		foreach($this->_mapper->getFields() as $field) {
			if(isset($localized[(string) $field->name])) continue;

			//get the type of this field
			$fieldType = $types[(string)$field->name];
			
			// We prepare the comments on the getter of the field:
			$title  = 'Get ' . ((string) $field->name);
			$access = 'public';
			$params = array();

			// We add the comments on the getter method:
			$out .= 	$this->_getMethodComments($title, '', $access, $params, $fieldType['type']);

			// Add the getter:
			$out .= 	$nlt1;
			$out .= 	'public function '. ((string) $field->getter) .'() {';
			$out .= 		$nlt2;
			$out .= 		'return $this->_'. ((string) $field->name) .';';
			$out .= 		$nlt1;
			$out .= 	'}';
			$out .= 	$nlt1;

			// We prepare the comments on the setter of the field:
			$title  = 'Set ' . ((string) $field->name);
			$access = 'public';

			$params = array(
				((string) $field->name) => $fieldType['type']
			);

			// We add the comments on the setter method:
			$out .= 	$this->_getMethodComments($title, '', $access, $params, $name);

			$null = '';
			// Add stronly type parameters
			if ($fieldType['strongtype']) {
				$fieldType['strongtype'] = ' ' . $fieldType['strongtype'] . ' ';
				if((string)$field->attributes()->null == 'true') {
					$null = ' = null';
				}
			}

			// Add the setter:
			$out .= 	$nlt1;
			$out .= 	'public function '. ((string) $field->setter) .'('.$fieldType['strongtype'].'$'. ((string) $field->name) . $null .') {';
			$out .= 		$nlt2;
			$out .= 		'$this->_'. ((string) $field->name) .' = $'. ((string) $field->name) .';';
			$out .= 		$nlt2;
			$out .=			'return $this;';
			$out .= 		$nlt1;
			$out .= 	'}';
			$out .= 	$nlt1;
		}
		
		// Add a getMapper method
		$codeVar = new M_CodeVariable('mapper', get_class($this->_mapper));
		$codeComment = new M_CodeComment();
		$codeComment->setTitle('Get mapper');
		$codeComment->setText('The mapper is the connection between the object and database or other storage platform');
		$codeComment->setReturn($codeVar);
		
		
		$code = new M_CodeFunction('getMapper');
		
		//check if module-id should be passed as 2nd argument
		$moduleId = '';
		if ($this->_mapper->getModule()->getId() != '') {
			$moduleId = ", '".$this->_mapper->getModule()->getId()."'";
		}
		// Parse the data object name
		$tmpName = str_replace("DataObject", "", $name); //class name without DataObject
		
		$code->setBody("return M_Loader::getDataObjectMapper('".$tmpName."'" . $moduleId.");");
		$code->setAccess(M_CodeAccess::TYPE_PUBLIC);
		$code->setComment($codeComment);
		
		$out .= $nl;
		$out .= $code->toString();
		
		// Class' bracket delimiter:
		$out .= $nl;
		$out .= '}';
		
		// Return rendered source
		return $out;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set DataObjectMapper
	 * 
	 * This method is used to set the mapper. The mapper provides
	 * {@link M_DataObjectClassFactory} with the information about the
	 * model, so the source code of the class(es) can be parsed.
	 * 
	 * @access public
	 * @param M_DataObjectMapper $mapper
	 * 		The mapper that will be used, in order to extract info
	 * 		about the Object/Mapper/Model
	 * @return void
	 */
	public function setDataObjectMapper(M_DataObjectMapper $mapper) {
		$this->_mapper = $mapper;
	}
	
	/* -- PRIVATE -- */

	/**
	 * Get syntax of comments on class property
	 * 
	 * This method generates the (phpdocumentor) comments on a class
	 * property.
	 * 
	 * @access private
	 * @param string $title
	 * 		The title of the property
	 * @param string $comments
	 * 		The description of the property
	 * @param string $access
	 * 		The access signature of the property (private|protected|public)
	 * @param array $var
	 * 		The data type of the property (array|string|...)
	 * @return string
	 */
	private function _getPropertyComments($title, $comments, $access, $var) {
		// Prepare some white-spaces for the formatting of the source
		// code:
		$nlt1 = $this->_getNewlineWithTabs(1, 1);
		
		// Parse the comments into the phpdoc format. Also, we max the
		// width to 70 chars, for readability in Zend :)
		$comments = wordwrap($comments, 70, "\n");
		$comments = str_replace("\r", '', $comments);
		$comments = str_replace("\n", $nlt1 . ' * ', $comments);
		
		// Render the comments block:
		$out  = $nlt1 . '/**';
		$out .= $nlt1 . ' * ' . $title;
		
		if(!empty($comments)) {
			$out .= $nlt1 . ' * ';
			$out .= $nlt1 . ' * ';
			$out .= $comments;
		}
		
		$out .= $nlt1 . ' * ';
		$out .= $nlt1 . ' * @access ' . $access;
		$out .= $nlt1 . ' * @var ' . $var;
		$out .= $nlt1;
		$out .= ' */';
		return $out;
	}

	/**
	 * Get syntax of comments on method/function
	 * 
	 * This method generates the (phpdocumentor) comments on a class
	 * method.
	 * 
	 * @access private
	 * @param string $title
	 * 		The title of the method
	 * @param string $comments
	 * 		The description of the method
	 * @param string $access
	 * 		The access signature of the method (private|protected|public)
	 * @param array $params
	 * 		The parameters of the method. The keys of this array are
	 * 		the (variable) names, the values are the data types. If the
	 * 		value of the parameter is an array, it will be used to set
	 * 		both data type and description of the parameter.
	 * @param string $return
	 * 		The return value of the method
	 * @return string
	 */
	private function _getMethodComments($title, $comments, $access, array $params, $return = 'void') {
		// Prepare some white-spaces for the formatting of the source
		// code:
		$nlt1 = $this->_getNewlineWithTabs(1, 1);
		
		// Parse the comments into the phpdoc format. Also, we max the
		// width to 70 chars, for readability in Zend :)
		$comments = wordwrap($comments, 70, "\n");
		$comments = str_replace("\r", '', $comments);
		$comments = str_replace("\n", $nlt1 . ' * ', $comments);
		
		// Render the comments block:
		$out  = $nlt1 . '/**';
		$out .= $nlt1 . ' * ' . $title;
		
		if(!empty($comments)) {
			$out .= $nlt1 . ' * ';
			$out .= $nlt1 . ' * ';
			$out .= $comments;
		}
			
		$out .= $nlt1 . ' * ';
		$out .= $nlt1 . ' * @access ' . $access;
		foreach($params as $name => $type) {
			if(is_array($type)) {
				$out .= $nlt1 . ' * @param ' . $type[0] . ' $' . $name;
				$out .= $nlt1 . ' * ' . "\t" . $type[1];				
			} else {
				$out .= $nlt1 . ' * @param ' . $type . ' $' . $name;
			}
		}
		
		$out .= $nlt1 . ' * @return ' . $return;
		$out .= $nlt1;
		$out .= ' */';
		return $out;
	}
	
	/**
	 * Get tab(s)
	 * 
	 * @access private
	 * @param integer $number
	 * 		The number of tabs
	 * @return string
	 */
	private function _getTabs($number = 1) {
		return str_repeat("\t", $number);
	}
	
	/**
	 * Get new line(s)
	 * 
	 * @access private
	 * @param integer $number
	 * 		The number of new lines
	 * @return string
	 */
	private function _getNewline($number = 1) {
		return str_repeat("\n", $number);
	}
	
	/**
	 * Get line(s), with tab(s)
	 * 
	 * @access private
	 * @param integer $numberOfLines
	 * 		The number of new lines
	 * @param integer $numberOfTabs
	 * 		The number of tabs in each new line
	 * @return string
	 */
	private function _getNewlineWithTabs($numberOfLines = 1, $numberOfTabs = 1) {
		return str_repeat("\n" . str_repeat("\t", $numberOfTabs), $numberOfLines);
	}
}
?>