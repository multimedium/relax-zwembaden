<?php
/**
 * M_DataObjectMapperEventDispatcher
 * 
 * All significant events that occur in instances of {@link M_DataObjectMapper}
 * are dispatched by {@link M_DataObjectMapperEventDispatcher}. This way, it is 
 * possible to listen to events that occur throughout all data objects, instead
 * of being limited to one instance of the {@link M_DataObjectMapper} class.
 * 
 * The events that are dispatched by this class are instances of the Event class
 * {@link M_DataObjectMapperEvent}.
 * 
 * @package Core
 */
class M_DataObjectMapperEventDispatcher extends M_EventDispatcher implements MI_EventDispatcher {
	/**
	 * Singleton object
	 * 
	 * This property stores the singleton object, which is dispatched by the
	 * singleton constructor: {@link M_DataObjectMapperEventDispatcher::getInstance()}
	 * 
	 * @static
	 * @access private
	 * @var M_DataObjectMapperEventDispatcher
	 */
	private static $_instance;
	
	/**
	 * PRIVATE Constructor
	 * 
	 * M_DataObjectMapperEventDispatcher is a singleton object, and can therefore
	 * not be instantiated with the "new" keyword. Instead, you should use 
	 * {@link M_DataObjectMapperEventDispatcher::getInstance()}
	 * 
	 * @access private
	 * @return M_DataObjectMapperEventDispatcher
	 */
	public function __construct() {
	}
	
	/**
	 * Singleton Constructor
	 * 
	 * @static 
	 * @access public
	 * @see M_DataObjectMapperEventDispatcher::__construct()
	 * @return M_DataObjectMapperEventDispatcher
	 */
	public static function getInstance() {
		if(! self::$_instance) {
			self::$_instance = new self;
		}
		return self::$_instance;
	}
}