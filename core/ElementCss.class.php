<?php
/**
 * M_ElementCss
 * 
 * This class allows you to create an object with CSS properties. These properties
 * can then be applied elements in the interface, for styling.
 * 
 * @package Core
 */
class M_ElementCss extends M_Object {
	
	/* -- PROPERTIES -- */

	/**
	 * CSS Properties
	 *
	 * @access private
	 * @var array
	 */
	private $_css = array();

	/**
	 * CSS Classes
	 *
	 * @access private
	 * @var array
	 */
	private $_class = array();
	
	/* -- GETTERS -- */
	
	/**
	 * Has CSS Class?
	 * 
	 * @access public
	 * @param string $class
	 * @return bool $flag
	 *		Will return TRUE if the CSS class has been added to this object,
	 *		FALSE if not
	 */
	public function hasClass($class) {
		return in_array($this->_getClassNameClean($class), $this->_class);
	}
	
	/**
	 * Get CSS Classes
	 * 
	 * @access public
	 * @return M_ArrayIterator $classes
	 *		The collection of CSS classes added
	 */
	public function getClasses() {
		return new M_ArrayIterator($this->_class);
	}
	
	/**
	 * Has CSS Property?
	 * 
	 * @access public
	 * @param string $name
	 * @return bool $flag
	 *		Will return TRUE if the CSS property has been added to this object,
	 *		FALSE if not
	 */
	public function hasProperty($name) {
		return in_array($this->_getPropertyNameClean($name), $this->_css);
	}
	
	/**
	 * Get CSS Properties
	 * 
	 * @access public
	 * @return M_ArrayIterator $properties
	 *		The collection of properties added. The keys of the collection are
	 *		the names of the properties, while the value is the corresponding
	 *		property value
	 */
	public function getProperties() {
		return new M_ArrayIterator($this->_css);
	}
	
	/**
	 * Get value of CSS Property
	 * 
	 * NOTE:
	 * Will return NULL instead, if the CSS Property has not been added previously.
	 * 
	 * @access public
	 * @param string $name
	 * @return string
	 */
	public function getProperty($name) {
		// Clean up the property name:
		$name = $this->_getPropertyNameClean($name);
		
		// Return its value:
		return (array_key_exists($name, $this->_css) ? $this->_css[$name] : NULL);
	}
	
	/**
	 * Get HTML Attributes String
	 * 
	 * This method can be used to render the object as HTML attributes. Consider
	 * the following examples:
	 * 
	 * <code>
	 *    $styling = new M_ElementCss();
	 *    $styling->addClass('button');
	 *    $styling->addProperty('float', 'left');
	 *    echo $styling->getHtmlAttributesString();
	 * </code>
	 * 
	 * The example illustrated above would create the following output:
	 * 
	 * <code>
	 *    class="button" style="float: left;"
	 * </code>
	 * 
	 * Note that the output created by this function will always be prepended by
	 * a space character. If you do not want to include that character, you should
	 * provide the argument (boolean) FALSE to this function.
	 * 
	 * @access public
	 * @param boolean $prependSpace
	 * @return string
	 */
	public function getHtmlAttributesString($prependSpace = TRUE) {
		// Output:
		$out = array();
		
		// If classes have been added to this object:
		if(count($this->_class) > 0) {
			// Then, we add the attribute to specify classes:
			array_push($out, 'class="'. implode(' ', $this->_class) .'"');
		}
		
		// If CSS Properties have been added:
		if(count($this->_css) > 0) {
			// Create a temporary array:
			$temp = array();
			
			// For each of the properties:
			foreach($this->_css as $name => $value) {
				// Add to the temporary array:
				array_push($temp, $name . ': ' . $value . ';');
			}
			
			// Add the attribute for inline styling
			array_push($out, 'style="'. implode(' ', $temp) .'"');
		}
		
		// If no attributes have been generated for output
		if(count($out) == 0) {
			// Then, we return an empty string:
			return '';
		}
		
		// Return the output:
		return (($prependSpace ? ' ' : '') . implode(' ', $out));
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Add CSS Class
	 * 
	 * @access public
	 * @param string $class
	 * @return M_ElementCss $styling
	 *		Returns itself, for a fluent programming interface
	 */
	public function addClass($class) {
		// Clean up the class name:
		$clean = $this->_getClassNameClean($class);
		
		// The class must not be empty. If the provided class name is empty
		if(! $clean) {
			// Then, we throw an exception to inform about the error:
			throw new M_Exception(sprintf(
				'Cannot add class "%s" to %s; The class name must not be empty!',
				$class,
				__CLASS__
			));
		}
		
		// If the class has not yet been added:
		if(! in_array($clean, $this->_class)) {
			// Add the class
			array_push($this->_class, $clean);
		}
		
		// Return myself
		return $this;
	}
	
	/**
	 * Add CSS Property
	 * 
	 * @access public
	 * @param string $name
	 * @param string $value
	 * @return M_ElementCss $styling
	 *		Returns itself, for a fluent programming interface
	 */
	public function addProperty($name, $value) {
		// Clean up the property name:
		$cleanName = $this->_getPropertyNameClean($name);
		
		// Clean up the value:
		$cleanValue = M_Helper::trimCharlist((string) $value);
		
		// The class must not be empty. If the provided class name is empty
		if(! $cleanName || ! $cleanValue) {
			// Then, we throw an exception to inform about the error:
			throw new M_Exception(sprintf(
				'Cannot set CSS property "%s" to %s; The name of the property ' . 
				'must not be empty, and its value cannot be left empty!',
				$cleanName,
				__CLASS__
			));
		}
		
		// Add the css property
		$this->_css[$cleanName] = $cleanValue;
		
		// Return myself
		return $this;
	}
	
	/**
	 * To String
	 * 
	 * We overwrite this method, because we often have to add CSS properties
	 * on template level. When doing so and not printing the CSS immediately,
	 * the __toString() method is called, printing the CSS as an object in
	 * the template. To prevent this, we always return an empty string when
	 * this happens.
	 * 
	 * @access public
	 * @return string
	 */
	public function __toString() {
		return '';
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Clean CSS Class
	 * 
	 * @access protected
	 * @param string $class
	 * @return string
	 */
	protected function _getClassNameClean($class) {
		// Clean up the class name:
		$temp = M_Helper::trimCharlist((string) $class, '.');
		
		// Return the cleaned up name:
		return $temp;
	}
	
	/**
	 * Get Clean CSS Property Name
	 * 
	 * @access protected
	 * @param string $name
	 * @return string
	 */
	protected function _getPropertyNameClean($name) {
		// Clean up the property name:
		$temp = M_Helper::trimCharlist((string) $name);
		
		// Return the cleaned up name:
		return $temp;
	}
}