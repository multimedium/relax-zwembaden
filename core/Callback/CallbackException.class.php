<?php
/**
 * M_CallbackException class
 *
 * Exceptions thrown by {@link M_Callback}
 * 
 * @package Core
 */
class M_CallbackException extends M_Exception {
}