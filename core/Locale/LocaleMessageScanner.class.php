<?php
/**
 * M_LocaleMessageScanner class
 * 
 * Throughout the application, {@link t()} is used to translate 
 * messages in the interface. M_LocaleMessageScanner can be used to
 * scan the application's code for messages that are being used in the 
 * interface.
 * 
 * @package Core
 */
class M_LocaleMessageScanner {
	private $_dirs = array();
	private $_ignoreDirs = array();
	private $_extensions = array();
	private $_recursive = TRUE;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_LocaleMessage
	 */
	public function __construct() {
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Scan application code
	 * 
	 * TODO: This application should also add reference comments to
	 * each of the found matches!! Maybe this method could return an 
	 * iterator on a collection of M_LocaleMessage objects?
	 */
	public function getStrings() {
		// We will save all strings to $out
		// (this will be the final return value)
		$out = array();
		
		// For each of the scan directories
		foreach($this->_dirs as $dir) {
			// For each of the matching files in the current directory:
			foreach($this->getFiles($dir) as $file) {
				// Read the contents of the file:
				$code = file_get_contents($file);
				
				// Special characters are temporarily changed to other
				// sequences:
				$code = str_replace('\\"',  '#@dquote@#', $code);
				$code = str_replace('\\\'', '#@squote@#', $code);
				
				// Search untranslated strings, called by t()
				// - with double quotes:
				$matches = array();
				preg_match_all('/[^a-z]+t\("(.*)"[,\)]{1}/im', $code, $matches);
				
				// For each of the matches:
				foreach($matches[1] as $match) {
					// Add the string to the collection of strings
					$pos = strpos($match, '")');
					if($pos !== FALSE) {
						$out[] = substr($match, 0, $pos);
						$pos = strpos($match, 't(');
						if($pos !== FALSE) {
							$out[] = substr($match, $pos + 3);
						}
					} else {
						$out[] = $match;
					}
				}
				
				// - with single quotes:
				$matches = array();
				preg_match_all('/[^a-z]+t\(\'(.*)\'[,\)]{1}/im', $code, $matches);
				
				// For each of the matches:
				foreach($matches[1] as $match) {
					// Add the string to the collection of strings
					$pos = strpos($match, '\')');
					if($pos !== FALSE) {
						$out[] = substr($match, 0, $pos);
						$pos = strpos($match, 't(');
						if($pos !== FALSE) {
							$out[] = substr($match, $pos + 3);
						}
					} else {
						$out[] = $match;
					}
				}
				
				// Search untranslated strings, called by {translate text=""}
				// - without arguments:
				$matches = array();
				preg_match_all('/\{translate text="([^"]+)"\}/m', $code, $matches);
				
				// Add the strings to the collection of strings
				foreach($matches[1] as $match) {
					$out[] = $match;
				}
				
				// Search untranslated strings, called by {translate text=""}
				// - with arguments:
				$matches = array();
				preg_match_all('/\{translate text="([^"]+)"[^\}]{1,}\}/m', $code, $matches);
				
				// Add the strings to the collection of strings
				foreach($matches[1] as $match) {
					$out[] = $match;
				}
			}
		}
		
		// Return final collection of found strings:
		return array_unique($out);
	}
	
	/**
	 * Get files
	 * 
	 * This method is used to get the collection of files that match
	 * the scanned extensions, in a given directory.
	 */
	public function getFiles($directory) {
		// If no search extensions have been defined, we return an
		// empty collection:
		if(count($this->_extensions) == 0) {
			return array();
		}
		
		// The collection of files is stored in $out:
		// (this will be the final return value)
		$out = array();
		
		// Create a directory handle
		$dirHandle = @opendir($directory);
		
		// If directory could not have been read:
		if(!$dirHandle) {
			// We throw an exception, to inform about error
			throw new M_LocaleException(sprintf(
				'%s: Could not open directory "%s"',
				__CLASS__,
				$directory
			));
		}
		
		// For each of the files in the directory:
		$file = readdir($dirHandle);
		while($file !== FALSE) {
			// If a directory or file:
			if($file != '.' && $file != '..') {
				// $path is the full path to the current file:
				$path = $directory . '/' . $file;
				
				// if the current item is a directory:
				if(is_dir($path)) {
					// We also scan it, if the scanner has been set to
					// search recursively:
					if($this->_recursive) {
						// Note that we will not scan ignored directories!
						if(!in_array($file, $this->_ignoreDirs)) {
							foreach($this->getFiles($path) as $file) {
								$out[] = $file;
							}
						}
					}
				}
				// If the current item is a file:
				else {
					// If the current file's extension matches one of the
					// specified search extensions:
					if(in_array(pathinfo($directory . $file, PATHINFO_EXTENSION), $this->_extensions)) {
						$out[] = $path;
					}
				}
			}
			
			// Get next file:
			$file = readdir($dirHandle);
		}
		
		// Close directory handle
		closedir($dirHandle);
		
		// return collection of files:
		return $out;
	}
	
	/* -- SETTERS -- */
	
	public function addDirectory($dir) {
		$this->_dirs[] = rtrim(trim($dir), DIRECTORY_SEPARATOR . '/');
	}
	
	public function addIgnoreDirectory($dir) {
		$this->_ignoreDirs[] = rtrim(trim($dir), DIRECTORY_SEPARATOR . '/');
	}
	
	public function addExtension($dir) {
		$this->_extensions[] = trim($dir, " \t\n\r\0\x0B.");
	}
	
	public function setDoRecursiveScan($flag) {
		$this->_recursive = (bool) $flag;
	}
}
?>