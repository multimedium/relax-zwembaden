<?php
/**
 * M_LocaleDataLDML class
 * 
 * This class is used by the {@link M_LocaleData} class to load LDML files. A 
 * M_LocaleData may be using various M_LocaleDataLDML objects to load the 
 * data of the requested locale. Read the documentation on the method
 * {@link M_LocaleData::_loadLDML()} to learn more.
 * 
 * IMPORTANT NOTE:
 * The M_LocaleData class expects the source (this class) to provide the locale
 * data in UTF-8 encoding
 * 
 * @package Core
 */
class M_LocaleDataLDML {
	/**
	 * LDML handler
	 * 
	 * This is the handler that reads out the LDML source file. This property
	 * is a SimpleXMLElement object.
	 * 
	 * @access private
	 * @var SimpleXMLElement
	 */
	private $_ldml = NULL;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $path
	 * 		The full path to the LDML file
	 * @return M_LocaleDataLDML
	 */
	public function __construct($path) {
		if(!is_file($path)) {
			throw new M_LocaleException('Cannot find LDML file "' . $path . '"', E_USER_WARNING);
		}
		
		$this->_ldml = simplexml_load_file($path);
	}
	
	/**
	 * Get display names
	 * 
	 * Note that you can request different types of display names:
	 * 
	 * <code>
	 *    language
	 *    script
	 *    territory
	 *    variant
	 * </code>
	 * 
	 * For example, to get the display names of languages, in the selected locale,
	 * you would do the following:
	 * 
	 * <code>
	 *    $ldml = new M_LocaleDataLDML('data/nl.xml');
	 *    $ldml->getDisplayNames('language');
	 * </code>
	 * 
	 * @access public
	 * @throws M_LocaleException
	 * @param string $element
	 * 		The requested element's display names. Possible values: "language",
	 * 		"script", "territory", "variant"
	 * @return array
	 */
	public function getDisplayNames($element) {
		$elements = array(
			'language'  => 'languages/language',
			'script'    => 'scripts/script',
			'territory' => 'territories/territory',
			'variant'   => 'variants/variant'
		);
		
		if(!isset($elements[$element])) {
			throw new M_LocaleException('Unrecognized locale element "' . $element . '"', E_USER_WARNING);
		}
		
		$rs = $this->_ldml->xpath('/ldml/localeDisplayNames/' . $elements[$element]);
		$output = array();
		foreach($rs as $XMLElement) {
			$output[(string) $XMLElement->attributes()] = (string) $XMLElement;
		}
		return $output;
	}
	
	/**
	 * Get Currency names
	 * 
	 * This method will return an associative array with display names of the 
	 * currencies that are supported in the locale. The keys of the resulting array 
	 * are the currency codes, of which the value contains the currency display 
	 * name and symbol. The result array should look like this:
	 * 
	 * <code>
	 *    Array (
	 *       ...
	 *       'EUR' => array (
	 *          'displayName' => 'Euro',
	 *          'symbol' => '€'
	 *       ),
	 *       ...
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function getCurrencyDisplayNames() {
		$output = array();
		$rs = $this->_ldml->xpath('/ldml/numbers/currencies/currency');
		foreach($rs as $XMLElement) {
			$code = (string) $XMLElement->attributes();
			$children = $XMLElement->children();
			if(count($children) > 1) {
				$output[$code] = array();
				foreach($children as $child) {
					$output[$code][(string) $child->getName()] = (string) $child;
				}
			} else {
				$output[$code] = (string) $children;
			}
		}
		return $output;
	}
	
	/**
	 * Get number formatting symbols
	 * 
	 * This method will return the symbols that are used to format numbers. 
	 * The result of this method is an associative array, with values for the 
	 * following keys (example array):
	 * 
	 * <code>
	 *    Array (
	 *       'decimal'         => '.',
	 *       'group'           => ',',
	 *       'list'            => ';',
	 *       'percentSign'     => '%',
	 *       'nativeZeroDigit' => '0',
	 *       'patternDigit'    => '#',
	 *       'plusSign'        => '+',
	 *       'minusSign'       => '-',
	 *       'exponential'     => 'E',
	 *       'perMille'        => chr(226).chr(128).chr(176),
	 *       'infinity'        => chr(226).chr(136).chr(158),
	 *       'nan'             => 'NaN'
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function getNumberFormattingSymbols() {
		// default number-formatting symbols (represented in UTF-8):
		// (will be overwritten if values are available in the LDML file)
		$output = array(
			'decimal'         => '.',
			'group'           => ',',
			'list'            => ';',
			'percentSign'     => '%',
			'nativeZeroDigit' => '0',
			'patternDigit'    => '#',
			'plusSign'        => '+',
			'minusSign'       => '-',
			'exponential'     => 'E',
			'perMille'        => chr(226).chr(128).chr(176),
			'infinity'        => chr(226).chr(136).chr(158),
			'nan'             => 'NaN'
		);
		
		$rs = $this->_ldml->xpath('/ldml/numbers/symbols');
		foreach($rs as $XMLElement) {
			foreach($XMLElement->children() as $child) {
				$output[(string) $child->getName()] = (string) $child;
			}
		}
		return $output;
	}
	
	/**
	 * Get number format
	 * 
	 * This method will return the format in which numbers are to be rendered.
	 * Note that numbers can be used in different contexts. For example, if a
	 * number is a price, you should render the number in the "currency" context.
	 * 
	 * You can request the following contexts:
	 * 
	 * - "scientific"
	 * - "percent"
	 * - "currency"
	 * 
	 * @access public
	 * @param string $context
	 * 		The context in which the number format is being requested
	 * @return string
	 */
	public function getNumberFormat($context) {
		$rs = $this->_ldml->xpath('/ldml/numbers/' . $context . 'Formats/' . $context . 'FormatLength');
		$output = array();
		foreach($rs as $XMLElement) {
			$format = (string) $XMLElement->attributes();
			if(empty($format)) {
				$format = 'default';
			}
			$output[$format] = (string) $XMLElement->children()->children();
		}
		return $output;
	}
	
	/**
	 * Get format of date/time
	 * 
	 * This method will return the format in which date and time should be rendered
	 * for display in the locale. As in {@link M_LocaleDataLDML::getNumberFormat()},
	 * you can request the format strings of different elements.
	 * 
	 * You can request the following elements:
	 * 
	 * - "date"
	 * - "time"
	 * 
	 * Note that you can also specify the calendar type from which you want to retrieve
	 * the format strings. The M_LocaleData requests all format strings with the calendar 
	 * type set to "gregorian".
	 * 
	 * The returned value of this method will be an associative array, of which the keys
	 * are the names of the formats.
	 * 
	 * Example 1, get Dutch date format(s) from gregorian calendar
	 * <code>
	 *    $ldml = new M_LocaleDataLDML('data/nl.xml');
	 *    print_r($ldml->getDateFormats('gregorian', 'date'));
	 * </code>
	 * 
	 * The above example would, for example, output the following:
	 * 
	 * <code>
	 *    Array (
	 *       'full'   => 'EEEE d MMMM yyyy',
	 *       'long'   => 'd MMMM yyyy',
	 *       'medium' => 'd MMM yyyy',
	 *       'short'  => 'dd-MM-yy'
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @param string $calendarType
	 * 		The calendar type. The M_LocaleData asks for format strings with the 
	 * 		calendar type set to "gregorian"
	 * @param string $element
	 * 		The element (date|time)
	 * @return array
	 */
	public function getDateFormats($calendarType, $element) {
		$rs = $this->_ldml->xpath('/ldml/dates/calendars/calendar[@type="' . $calendarType . '"]/' . $element . 'Formats/' . $element . 'FormatLength');
		if(count($rs) > 0) {
			$output = array();
			foreach($rs as $XMLElement) {
				$output[(string) $XMLElement->attributes()] = (string) $XMLElement->children()->children();
			}
			return $output;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get calendar item
	 * 
	 * This method will return the names of the requested calendar element. You
	 * can request the following elements:
	 * 
	 * - "month"
	 * - "day"
	 * - "quarter"
	 * 
	 * Note that you can also specify the calendar type from which you want to 
	 * retrieve the items. The M_LocaleData requests all format strings with the 
	 * calendar type set to "gregorian".
	 * 
	 * Example 1, get the Dutch names for the gregorian calendar's months
	 * <code>
	 *    $ldml = new M_LocaleDataLDML('data/nl.xml');
	 *    print_r($ldml->calendarItem('gregorian', 'month'));
	 * </code>
	 * 
	 * The above example would, for example, output the following:
	 * 
	 * <code>
	 *    Array (
	 *       'format' => Array (
	 *          'abbreviated' => Array (
	 *             1 => 'jan',
	 *             2 => 'feb',
	 *             3 => 'mrt',
	 *             4 => 'apr',
	 *             5 => 'mei',
	 *             6 => 'jun',
	 *             7 => 'jul',
	 *             8 => 'aug',
	 *             9 => 'sep',
	 *             10 => 'okt',
	 *             11 => 'nov',
	 *             12 => 'dec'
	 *          ),
	 *          'wide' => Array (
	 *             1 => 'januari',
	 *             2 => 'februari',
	 *             3 => 'maart',
	 *             4 => 'april',
	 *             5 => 'mei',
	 *             6 => 'juni',
	 *             7 => 'juli',
	 *             8 => 'augustus',
	 *             9 => 'september',
	 *             10 => 'oktober',
	 *             11 => 'november',
	 *             12 => 'december'
	 *          )
	 *       ),
	 *       'stand-alone' => Array (
	 *          'narrow' => Array (
	 *             1 => 'J',
	 *             2 => 'F',
	 *             3 => 'M',
	 *             4 => 'A',
	 *             5 => 'M',
	 *             6 => 'J',
	 *             7 => 'J',
	 *             8 => 'A',
	 *             9 => 'S',
	 *             10 => 'O',
	 *             11 => 'N',
	 *             12 => 'D'
	 *          )
	 *       )
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @param string $calendarType
	 * 		The calendar type. The M_LocaleData always sends messages with the 
	 * 		calendar type set to "gregorian"
	 * @param string $element
	 * 		The calendar element
	 * @return array
	 */
	public function getCalendarItemNames($calendarType, $element) {
		$rs = $this->_ldml->xpath('/ldml/dates/calendars/calendar[@type="' . $calendarType . '"]/' . $element . 's/' . $element . 'Context');
		if(count($rs) > 0) {
			$output = array();
			for($i = 0, $n = count($rs); $i < $n; $i ++) {
				$context = (string) $rs[$i]->attributes();
				$output[$context] = array();
				foreach($rs[$i]->children() as $monthWidth) {
					$format = (string) $monthWidth->attributes();
					$output[$context][$format] = array();
					foreach($monthWidth->children() as $month) {
						$output[$context][$format][(string) $month->attributes()] = (string) $month;
					}
				}
			}
			return $output;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get calendar field names
	 * 
	 * This method will return the names of the calendar fields. Note that you 
	 * can also specify the calendar type from which you want to retrieve the 
	 * names. The M_LocaleData requests all names with the calendar type set to 
	 * "gregorian".
	 * 
	 * Example 1, get the Dutch names for the gregorian calendar fields
	 * <code>
	 *    $ldml = new M_LocaleDataLDML('data/nl.xml');
	 *    print_r($ldml->getCalendarFieldNames('gregorian'));
	 * </code>
	 * 
	 * The above example would, for example, output the following:
	 * 
	 * <code>
	 *    Array (
	 *       'era' => 'Tijdperk',
	 *       'year' => 'Jaar',
	 *       'month' => 'Maand',
	 *       'week' => 'Week',
	 *       'day' => Array (
	 *          'displayName' => 'Dag',
	 *          'relative' => Array (
	 *             0 => 'Vandaag',
	 *             1 => 'Morgen',
	 *             2 => 'Overmorgen',
	 *             3 => 'Over drie dagen',
	 *             -1 => 'Gisteren',
	 *             -2 => 'Eergisteren',
	 *             -3 => 'Drie dagen geleden'
	 *          )
	 *       ),
	 *       'weekday' => 'Dag van de week',
	 *       'dayperiod' => 'AM/PM',
	 *       'hour' => 'Uur',
	 *       'minute' => 'Minuut',
	 *       'second' => 'Seconde',
	 *       'zone' => 'Zone',
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @param string $calendarType
	 * 		The calendar type. The M_LocaleData always sends messages with the 
	 * 		calendar type set to "gregorian"
	 * @return array
	 */
	public function getCalendarFieldNames($calendarType) {
		$rs = $this->_ldml->xpath('/ldml/dates/calendars/calendar[@type="' . $calendarType . '"]/fields/field');
		if(count($rs) > 0) {
			$output = array();
			foreach($rs as $XMLElement) {
				$type = (string) $XMLElement->attributes();
				$children = $XMLElement->children();
				$n = count($children);
				if($n > 1) {
					$output[$type] = array(
						'displayName' => (string) $children,
						'relative' => array()
					);
					for($i = 1; $i < $n; $i ++) {
						$output[$type]['relative'][(string) $children[$i]->attributes()] = (string) $children[$i];
					}
				} else {
					$output[$type] = (string) $children;
				}
			}
			return $output;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get AM/PM label
	 * 
	 * Example 1, get the Dutch AM/PM labels
	 * <code>
	 *    $ldml = new M_LocaleDataLDML('data/nl.xml');
	 *    print_r($ldml->getAmPm());
	 * </code>
	 * 
	 * The above example would, for example, output the following:
	 * 
	 * <code>
	 *    Array (
	 *       'era' => 'Tijdperk',
	 *       'year' => 'Jaar',
	 *       'month' => 'Maand',
	 *       'week' => 'Week',
	 *       'day' => Array (
	 *          'displayName' => 'Dag',
	 *          'relative' => Array (
	 *             0 => 'Vandaag',
	 *             1 => 'Morgen',
	 *             2 => 'Overmorgen',
	 *             3 => 'Over drie dagen',
	 *             -1 => 'Gisteren',
	 *             -2 => 'Eergisteren',
	 *             -3 => 'Drie dagen geleden'
	 *          )
	 *       ),
	 *       'weekday' => 'Dag van de week',
	 *       'dayperiod' => 'AM/PM',
	 *       'hour' => 'Uur',
	 *       'minute' => 'Minuut',
	 *       'second' => 'Seconde',
	 *       'zone' => 'Zone',
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function getAmPm() {
		// Default AM PM
		$output = array(
			'AM' => 'AM',
			'PM' => 'PM'
		);
		
		$rs = $this->_ldml->xpath('/ldml/dates/calendars/calendar/am');
		if(count($rs) == 1) {
			$output['AM'] = (string) $rs[0];
		}
		
		$rs = $this->_ldml->xpath('/ldml/dates/calendars/calendar/pm');
		if(count($rs) == 1) {
			$output['PM'] = (string) $rs[0];
		}
		
		return $output;
	}
	
	/**
	 * Get delimiters (quotes)
	 * 
	 * When quotations are nested, the quotation marks and alternate 
	 * marks are used in an alternating fashion:
	 * 
	 * <code>
	 * He said, "Remember what the Mad Hatter said: 'Not the same thing a 
	 * bit! Why you might just as well say that "I see what I eat" is the 
	 * same thing as "I eat what I see"!'"
	 * 
	 *    <quotationStart>"</quotationStart>
	 *    <quotationEnd>"</quotationEnd>
	 *    <alternateQuotationStart>'</alternateQuotationStart>
	 *    <alternateQuotationEnd>'</alternateQuotationEnd>
	 * </code>
	 * 
	 * This method will return an array, with values for the following keys:
	 * (the example below illustrates the default values)
	 * 
	 * <code>
	 *    Array (
	 *       'quotationStart'          => chr(226).chr(128).chr(156),
	 *       'quotationEnd'            => chr(226).chr(128).chr(157),
	 *       'alternateQuotationStart' => chr(226).chr(128).chr(152),
	 *       'alternateQuotationEnd'   => chr(226).chr(128).chr(153)
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function getDelimiters() {
		// default quotation marks (represented in UTF-8) will be
		// overwritten if quotation marks are available in the LDML file
		$output = array(
			'quotationStart'          => chr(226).chr(128).chr(156),
			'quotationEnd'            => chr(226).chr(128).chr(157),
			'alternateQuotationStart' => chr(226).chr(128).chr(152),
			'alternateQuotationEnd'   => chr(226).chr(128).chr(153)
		);
		
		$rs = $this->_ldml->xpath('/ldml/delimiters');
		if(count($rs) == 1) {
			foreach($rs as $XMLElement) {
				$type = (string) $XMLElement->attributes();
				if(!empty($type)) {
					$output[(string) $XMLElement->attributes()] = (string) $XMLElement;
				}
			}
		}
		
		return $output;
	}
}
?>