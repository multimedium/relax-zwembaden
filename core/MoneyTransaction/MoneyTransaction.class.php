<?php
/**
 * M_MoneyTransaction class
 * 
 * M_MoneyTransaction is used to handle online payment, with the 
 * involvement of a financial entity. It provides with tools to set
 * the merchant code, terminal code, total price, etc. Also, it
 * provides the {@link M_Form} that is used to start the payment.
 * 
 * Note that M_MoneyTransaction is an abstract class. The specific
 * technical parameters of a transaction may vary between different 
 * service providers. To create a specific implementation, a subclass 
 * of M_MoneyTransaction should be created.
 * 
 * @package Core
 */
abstract class M_MoneyTransaction extends M_Object {
	/**
	 * Payment result
	 * 
	 * This constant is used to address a possible outcome of the 
	 * payment process. For example, this constant may be used to set 
	 * a redirect URL. Check {@link M_MoneyTransaction::setRedirectUrl()} 
	 * for more info.
	 * 
	 * This specific constant addresses the success of a payment.
	 */
	const SUCCESS = 'ok';
	
	/**
	 * Payment result
	 * 
	 * This constant is used to address a possible outcome of the 
	 * payment process. For example, this constant may be used to set 
	 * a redirect URL. Check {@link M_MoneyTransaction::setRedirectUrl()} 
	 * for more info.
	 * 
	 * This specific constant addresses the failure of a payment.
	 */
	const FAILURE = 'ko';
	
	/**
	 * Payment result
	 * 
	 * This constant is used to address a possible outcome of the 
	 * payment process. For example, this constant may be used to set 
	 * a redirect URL. Check {@link M_MoneyTransaction::setRedirectUrl()} 
	 * for more info.
	 * 
	 * This specific constant addresses the payment having been
	 * cancelled by the client.
	 */
	const CANCEL = 'cancel';
	
	/**
	 * Merchant Code
	 * 
	 * This property stores the merchant (beneficiary) code. Read the 
	 * docs on {@link M_MoneyTransaction::setMerchantCode()} for more 
	 * info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_merchantCode;
	
	/**
	 * Customer name
	 * 
	 * This property stores the name of the paying customer. Read the
	 * docs on {@link M_MoneyTransaction::setCustomerName()} for more
	 * info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_customerName;
	
	/**
	 * Online payment title
	 * 
	 * This property stores the (page) title of the online payment.
	 * Read {@link M_MoneyTransaction::setTitle()} for more info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_pageTitle;
	
	/**
	 * Currency
	 * 
	 * This property stores the currency in which the payed amount is 
	 * being expressed. Read {@link M_MoneyTransaction::setCurrency()}
	 * for more info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_currency;
	
	/**
	 * Amount
	 * 
	 * This property stores the amount (price) that is being paid.
	 * Read {@link M_MoneyTransaction::setAmount()} for more info.
	 * 
	 * @access protected
	 * @var float
	 */
	protected $_amount;
	
	/**
	 * Payment Method
	 * 
	 * This property stores the payment method used to complete the payment
	 * (e.g. "CreditCard"). Read {@link M_MoneyTransaction::setPaymentMethod()}
	 * for more info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_paymentMethod;
	
	/**
	 * Payment Method Brand
	 * 
	 * This property stores the brand of the payment method used to complete
	 * the payment (e.g. MasterCard, VISA, ...). Read
	 * {@link M_MoneyTransaction::setPaymentMethodBrand()} for more info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_paymentMethodBrand;
	
	/**
	 * Payment Card Number
	 * 
	 * This property stores the card number of the card used to complete the
	 * payment. Read {@link M_MoneyTransaction::setPaymentCardNumber()} for
	 * more info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_paymentCardNumber;
	
	/**
	 * Payment Card Holder Name
	 * 
	 * This property stores the name of the card holder. Read
	 * {@link M_MoneyTransaction::setPaymentCardHolderName()} for more info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_paymentCardHolderName;
	
	/**
	 * Payment Card Expiration Month And Year
	 * 
	 * This property stores the expiration date of the card used to complete
	 * the payment, as month and year. Read
	 * {@link M_MoneyTransaction::setPaymentCardExpirationMonthAndYear()} for
	 * more info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_paymentCardExpirationMonthAndYear;
	
	/**
	 * Private Key In
	 * 
	 * This property stores the Merchant's Private Key In. For more 
	 * info, read {@link M_MoneyTransaction::setPrivateKeyIn()}.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_privateKeyIn;
	
	/**
	 * Private Key Out
	 * 
	 * This property stores the Merchant's Private Key Out. For more 
	 * info, read {@link M_MoneyTransaction::setPrivateKeyOut()}.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_privateKeyOut;
	
	/**
	 * Order ID
	 * 
	 * This property stores the order code that is presented to the 
	 * paying client, and will be used as a reference to the invoice 
	 * document. Read {@link M_MoneyTransaction::setOrderNumber()} for
	 * more info.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_orderId;
	
	/**
	 * Interface language
	 * 
	 * This property stores the interface language. Read the docs on
	 * {@link M_MoneyTransaction::setLanguage()} for more info.
	 * 
	 * NOTE:
	 * The language is defaulted to "nl"
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_language = 'nl';
	
	/**
	 * Country
	 * 
	 * This property stores the country in which the transaction is being used. 
	 * Read the docs on {@link M_MoneyTransaction::setCountry()} for more info.
	 * 
	 * NOTE:
	 * The country is defaulted to "BE"
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_country = 'BE';
	
	/**
	 * Success page
	 * 
	 * This property stores the URL to which successful/failed payments 
	 * are being redirected. For more information, read the docs on
	 * {@link M_MoneyTransaction::setRedirectUrl()}.
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_redirect = array();
	
	/**
	 * Flag: is testing account?
	 * 
	 * This property stores a flag that indicates whether or not the transaction
	 * is being done with a test account.
	 * 
	 * @see M_MoneyTransaction::setIsTestingAccount()
	 * @access private
	 * @var boolean
	 */
	protected $_isTestingAccount = FALSE;
	
	/**
	 * Status
	 * 
	 * This property stores the status of the money transaction, and can be 
	 * retrieved with 
	 * 
	 * - {@link M_MoneyTransaction::getStatus()}
	 * - {@link M_MoneyTransaction::isPaymentAuthorized()}
	 * 
	 * and set (not public) with 
	 * 
	 * - {@link M_MoneyTransaction::_setStatus()}
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_status;
	
	/* -- SETTERS -- */
	
	/**
	 * Factory
	 * 
	 * Similar to {@link M_Form::factory()}, this method is used to 
	 * construct a money transaction, starting from a transaction 
	 * definition.
	 * 
	 * IMPORTANT NOTE:
	 * The config should contain values at least 1 key:
	 * 
	 * <code>* type</code>
	 * 
	 * The type of the transaction. The type refers to the service
	 * provider. The value of this key is used to compose the 
	 * M_MoneyTransaction subclass name. For example, if type is set 
	 * to "ogone", the factory() method will attempt to mount a 
	 * {@link M_MoneyTransactionOgone} object. {@link M_Factory} is 
	 * used to compose the class name.
	 * 
	 * Read more at {@link M_MoneyTransaction::set()}.
	 * 
	 * @access public
	 * @throws M_MoneyTransactionException
	 * @param MI_Config $definition
	 * 		The transaction definition
	 * @return M_MoneyTransaction
	 */
	public function factory(MI_Config $definition) {
		if(! isset($definition->type)) {
			throw new M_MoneyTransactionException('Missing Money Transaction Type.');
		}
		
		$className = (strpos($definition->type, 'MoneyTransaction') !== FALSE)
				? (!strncmp($definition->type, 'M_', 2) 
					? $definition->type 
					: 'M_' . $definition->type)
				: 'M_MoneyTransaction' . ucfirst($definition->type);
		
		if(! class_exists($className)) {
			throw new M_MoneyTransactionException(
				sprintf(
					'Cannot find the "%s" class', 
					$className
				)
			);
		}
		
		$transaction = new $className();
		
		foreach($definition as $name => $spec) {
			if($name != 'type') {
				$transaction->set($name, $spec);
			}
		}
		
		return $transaction;
	}
	
	/**
	 * Add transaction definition
	 * 
	 * This method is used by {@link M_MoneyTransaction::factory()}, 
	 * to mount the money transaction with the given definition. 
	 * Check out Example 1 to see how exactly this method is being 
	 * used by {@link M_MoneyTransaction::factory()}:
	 * 
	 * Example 1
	 * <code>
	 *    $trans = M_MoneyTransaction::factory(new M_Config(array(
	 *       'type' => 'ogone',
	 *       'sha1' => 'ISUDPOUC0x809NSKFD'
	 *    )));
	 * </code>
	 * 
	 * The code illustrated above equals the following code:
	 * 
	 * <code>
	 *    $trans = new M_MoneyTransactionOgone;
	 *    $trans->set('sha1', 'ISUDPOUC0x809NSKFD');
	 * </code>
	 * 
	 * By default, the following definition keys are available (and 
	 * shared all M_MoneyTransaction subclasses):
	 * 
	 * <code>merchant</code>
	 * 
	 * Sets the Merchant code. For more info, read the documentation 
	 * on {@link M_MoneyTransaction::setMerchantCode()}.
	 * 
	 * <code>customerName</code>
	 * 
	 * Sets the Customer name. For more info, read the documentation 
	 * on {@link M_MoneyTransaction::setCustomerName()}.
	 * 
	 * <code>pageTitle</code>
	 * 
	 * Sets the page title. For more info, read the documentation 
	 * on {@link M_MoneyTransaction::setPageTitle()}.
	 * 
	 * <code>currency</code>
	 * 
	 * Sets the currency. For more info, read the documentation 
	 * on {@link M_MoneyTransaction::setCurrency()}.
	 * 
	 * <code>amount</code>
	 * 
	 * Sets the amount. For more info, read the documentation 
	 * on {@link M_MoneyTransaction::setAmount()}.
	 * 
	 * <code>privateKeyIn</code>
	 * 
	 * Sets the Private Key In. For more info, read the documentation 
	 * on {@link M_MoneyTransaction::setPrivateKeyIn()}.
	 * 
	 * <code>privateKeyOut</code>
	 * 
	 * Sets the Private Key Out. For more info, read the documentation 
	 * on {@link M_MoneyTransaction::setPrivateKeyOut()}.
	 * 
	 * <code>orderId</code>
	 * 
	 * Sets the Order ID. For more info, read the documentation 
	 * on {@link M_MoneyTransaction::setOrderNumber()}.
	 * 
	 * <code>language</code>
	 * 
	 * Sets the interface language. For more info, read the docs 
	 * on {@link M_MoneyTransaction::setLanguage()}.
	 * 
	 * <code>redirect</code>
	 * 
	 * Sets the collection of redirect URLs. For more info, read the 
	 * documentation on {@link M_MoneyTransaction::setRedirectUrl()}.
	 * 
	 * With this array of definition keys, you could construct a 
	 * money transaction as illustrated in Example 2.
	 * 
	 * Example 2
	 * <code>
	 *    // Mount transaction
	 *    $transaction = M_MoneyTransaction::factory(new M_Config(array(
	 *       'type' => 'ogone',
	 *       'merchant' => 'MULTIMEDIUM',
	 *       'currency' => 'EUR'
	 *       'amount' => 200,
	 *       'privateKeyIn' => 'MySHA1INSignature',
	 *       'privateKeyOut' => 'MySHA1OUTSignature',
	 *       'orderId' => 'ORDER24PO34',
	 *       'language' => 'en_US',
	 *       'redirect' => array(
	 *          'ok' => 'http://www.multimedium.be/payment/ok',
	 *          'ko' => 'http://www.multimedium.be/payment/error'
	 *       )
	 *    )));
	 * </code>
	 * 
	 * Typically, subclasses of {@link M_MoneyTransaction} will override
	 * this method in order to add more definition keys, similarly to
	 * {@link M_Form} and {@link M_Field}. To learn more about the
	 * definition keys of a specific provider, read the docs on the
	 * set() method of the corresponding class.
	 * 
	 * @access public
	 * @throws M_MoneyTransactionException
	 * @param string $spec
	 * 		The transaction definition key
	 * @param mixed $definition
	 * 		The value of the definition
	 * @return void 
	 */
	public function set($spec, $definition) {
		switch(strtolower($spec)) {
			case 'test':
			case 'testing':
				$this->setIsTestingAccount(M_Helper::isBooleanTrue($definition));
				break;
			
			case 'merchant':
				$this->setMerchantCode($definition);
				break;
			
			case 'customername':
				$this->setCustomerName($definition);
				break;
			
			case 'pagetitle':
				$this->setPageTitle($definition);
				break;
			
			case 'currency':
				$this->setCurrency($definition);
				break;
			
			case 'amount':
				$this->setAmount($definition);
				break;
			
			case 'privatekeyin':
				$this->setPrivateKeyIn($definition);
				break;
			
			case 'privatekeyout':
				$this->setPrivateKeyOut($definition);
				break;
			
			case 'orderid':
				$this->setOrderNumber($definition);
				break;
			
			case 'language':
				$this->setLanguage($definition);
				break;
			
			case 'redirect':
				foreach($definition as $flag => $url) {
					$this->setRedirectUrl($flag, $url);
				}
				break;
		}
	}
	
	/**
	 * Set Merchant code
	 * 
	 * This method will set the code (or account name) of the merchant.
	 * For a payment to be processed, a unique merchant (or beneficiary)
	 * needs to be identified. This code will be used to identify the 
	 * merchant, at the financial entity's server.
	 * 
	 * @access public
	 * @param string $code
	 * 		The Merchant Code
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setMerchantCode($code) {
		$this->_merchantCode = $code;
		return $this;
	}
	
	/**
	 * Set customer name
	 * 
	 * This method can be used to set the name of the paying customer.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the paying customer
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCustomerName($name) {
		$this->_customerName = $name;
		return $this;
	}
	
	/**
	 * Set payment form's page title
	 * 
	 * This method can be used to set the title of the online payment.
	 * Careful: do not confuse the title of the online payment with
	 * the Order Number (or Order ID), which is set with the method
	 * {@link M_MoneyTransaction::setOrderNumber()}! This title is used 
	 * to show as a page title in the payment form, and will not figure 
	 * on any of invoice(s) generated afterwards.
	 * 
	 * @access public
	 * @param string $title
	 * 		The title of the online payment
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPageTitle($title) {
		$this->_pageTitle = $title;
		return $this;
	}
	
	/**
	 * Set currency
	 * 
	 * This method will set the currency in which the amount to be
	 * paid is being expressed. Note that you should provide this method
	 * with a standard ISO code of the currency.
	 * 
	 * Some examples:
	 * - EURO is represented by the ISO Code "EUR".
	 * - American Dollars are represented by the ISO Code "USD"
	 * - British Pounds are represented by the ISO Code "GBP"
	 * 
	 * @access public
	 * @param string $code
	 * 		The currency ISO code
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCurrency($code) {
		$this->_currency = $code;
		return $this;
	}
	
	/**
	 * Set amount
	 * 
	 * This method will set the amount of the payment. Note that many
	 * service providers (eg Ogone) require you to multiply the amount
	 * by 100. For example
	 * 
	 * <code>
	 *    AMOUNT         | VALUE
	 *    -------------- |-------------------
	 *    2.50           | 250
	 *    2599.99        | 259999
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * It is the responsibility of {@link M_MoneyTransaction} to provide 
	 * the entity with a correctly formatted amount. You should provide 
	 * the amount in normal format, not multiplied by any factor 
	 * whatsoever.
	 * 
	 * Example 1
	 * 
	 * <code>
	 *    // Create a transaction for 2.75 EURO
	 *    $transaction = new M_MoneyTransaction;
	 *    $transaction->setCurrency('EUR');
	 *    $transaction->setAmount(2.75);
	 * </code>
	 * 
	 * @access public
	 * @param float $amount
	 * 		The amount to be paid
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAmount($amount) {
		$this->_amount = $amount;
		return $this;
	}
	
	/**
	 * Set Payment Method
	 * 
	 * Allows for setting the payment method used to complete the payment,
	 * like for example "CreditCard".
	 * 
	 * @access public
	 * @param string $paymentMethod
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPaymentMethod($paymentMethod) {
		$this->_paymentMethod = (string) $paymentMethod;
		return $this;
	}
	
	/**
	 * Set Payment Method brand
	 * 
	 * Allows for setting the brand of the payment method used to complete the
	 * payment, like for example "MasterCard" or "VISA".
	 * 
	 * @access public
	 * @param string $brand
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPaymentMethodBrand($brand) {
		$this->_paymentMethodBrand = (string) $brand;
		return $this;
	}
	
	/**
	 * Set Payment Card Number
	 * 
	 * Allows for setting the card number of the card used to complete the
	 * payment, e.g. "XXXXXXXXXXXX9999".
	 * 
	 * NOTE: setting this property is optional
	 * 
	 * @access public
	 * @param string $cardNumber
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPaymentCardNumber($cardNumber) {
		$this->_paymentCardNumber = (string) $cardNumber;
		return $this;
	}
	
	/**
	 * Set Payment Card Holder Name
	 * 
	 * Allows for setting the name of the card holder, whose card was used
	 * to complete the payment. E.g. "John Doe".
	 * 
	 * NOTE: setting this property is optional
	 * 
	 * @access public
	 * @param string $name
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPaymentCardHolderName($name) {
		$this->_paymentCardHolderName = (string) $name;
		return $this;
	}
	
	/**
	 * Set Payment Card Expiration Month And Year
	 * 
	 * Allows for setting the expiration date of the card used to complete
	 * the payment, in month and year format. For example: "0421" describes
	 * that the card will expire in the 4th month of 2021.
	 * 
	 * NOTE: setting this property is optional
	 * 
	 * @access public
	 * @param string $monthAndYear
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPaymentCardExpirationMonthAndYear($monthAndYear) {
		$this->_paymentCardExpirationMonthAndYear = (string) $monthAndYear;
		return $this;
	}
	
	/**
	 * Set Private Key In
	 * 
	 * To increase security of the online payment, you can provide the
	 * payment form with a private key. Some providers will even 
	 * require you to include this private signature in your payment 
	 * form, in order for it to be accepted.
	 * 
	 * Typically, this private signature is being used as an element
	 * of a larger string, which is then used to create an encrypted 
	 * hash. This hash is then compared with a hash on the service
	 * provider's server, in order to check whether or not access is
	 * granted.
	 * 
	 * We refer to this private key as the Private Key In, because this
	 * is the key that's used for encrypted hashes that go IN the 
	 * service provider's server. For more info about the Private
	 * Key Out, read {@link M_MoneyTransaction::setPrivateKeyOut()}.
	 * 
	 * @access public
	 * @param string $key
	 * 		The private key, or private signature
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPrivateKeyIn($key) {
		$this->_privateKeyIn = $key;
		return $this;
	}
	
	/**
	 * Set Private Key Out
	 * 
	 * Some providers will provide with a private key, to compute an
	 * encrypted hash that's provided to the merchant, along with the 
	 * feedback of the completed payment.
	 * 
	 * By using such a private key, the merchant can make sure that the
	 * parameters of the payment feedback have not been tampered with.
	 * Typically, the merchant can then concatenate a range of variables
	 * (from the service provider's feedback), create an enrypted hash
	 * and compare it with the hash that's provided by the external 
	 * entity.
	 * 
	 * This increases security because only the external entity and
	 * the merchant know the Private Key Out, which is included in the
	 * original string that's being hashed for comparison.
	 * 
	 * We refer to this private key as the Private Key Out, because 
	 * this is the key that's used for encrypted hashes that go OUT of
	 * the service provider's server. For more info about the Private
	 * Key In, read {@link M_MoneyTransaction::setPrivateKeyIn()}.
	 * 
	 * @access public
	 * @param string $key
	 * 		The private key, or private signature
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPrivateKeyOut($key) {
		$this->_privateKeyOut = $key;
		return $this;
	}
	
	/**
	 * Set Order ID
	 * 
	 * This method will set the order code that is presented to the 
	 * paying client, and will be used as a reference on the invoice 
	 * document.
	 * 
	 * @access public
	 * @param string $code
	 * 		The Order Number, or Order ID
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setOrderNumber($code) {
		$this->_orderId = $code;
		return $this;
	}
	
	/**
	 * Set language
	 * 
	 * This method will set the language in which the interface is
	 * being presented to the client. You should check the support 
	 * documentation of the service provider, to check which array
	 * of languages they are supporting.
	 * 
	 * Typically, the language is set by providing a locale name that
	 * describes the localization rules. Check the documentation on
	 * {@link M_Locale::getNameParts()}.
	 * 
	 * Example 1, display the interface in Dutch
	 * 
	 * <code>
	 *    $transaction = new M_MoneyTransaction;
	 *    $transaction->setLanguage('nl');
	 * </code>
	 * 
	 * @access public
	 * @param string $locale
	 * 		The locale name
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLanguage($locale) {
		$this->_language = $locale;
		return $this;
	}
	
	/**
	 * Set country
	 * 
	 * This method will set the country in which the transaction is being created. 
	 * This property is used typically in combination with the language - see
	 * {@link M_MoneyTransaction::setLanguage()} - to set the language for the 
	 * payment interface.
	 * 
	 * Example 1, display the interface in Dutch (Belgium)
	 * 
	 * <code>
	 *    $transaction = new M_MoneyTransaction;
	 *    $transaction->setLanguage('nl');
	 *    $transaction->setCountry('BE');
	 * </code>
	 * 
	 * @access public
	 * @param string $country
	 * 		The country ISO code
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCountry($country) {
		$this->_country = $country;
		return $this;
	}
	
	/**
	 * Set Redirect URL
	 * 
	 * This method will set the URL to which a given outcome of the 
	 * payment should be redirected. Assume we want to set the page
	 * that is shown to the client when the payment has been completed
	 * successfully:
	 * 
	 * <code>
	 *    $transaction = new M_MoneyTransaction;
	 *    $transaction->setRedirectUrl(M_MoneyTransaction::SUCCESS, 'http://...');
	 * </code>
	 * 
	 * Or, assume we want to set the page that is shown to the client
	 * when the payment could not be completed:
	 * 
	 * <code>
	 *    $transaction = new M_MoneyTransaction;
	 *    $transaction->setRedirectUrl(M_MoneyTransaction::FAILURE, 'http://...');
	 * </code>
	 * 
	 * You can use the following constants to address different outcomes
	 * of the payment:
	 * 
	 * - {@link M_MoneyTransaction::SUCCESS}
	 * - {@link M_MoneyTransaction::FAILURE}
	 * - {@link M_MoneyTransaction::CANCEL}
	 * 
	 * @access public
	 * @param string $flag
	 * 		The possible outcome of the payment
	 * @param string $url
	 * 		The URL that shows the page for the specified outcome
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setRedirectUrl($flag, $url) {
		$this->_redirect[$flag] = $url;
		return $this;
	}
	
	/**
	 * Set flag: "Is test account"?
	 * 
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if the account is a test account, FALSE if the account
	 * 		is a real/production account
	 * @return M_MoneyTransaction
	 *		Returns itself, for a fluent programming interface
	 */
	public function setIsTestingAccount($flag) {
		$this->_isTestingAccount = (bool) $flag;
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Merchant Code
	 * 
	 * This method will return the merchant code that may have been set 
	 * previously with {@link M_MoneyTransaction::setMerchantCode()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getMerchantCode() {
		return $this->_merchantCode;
	}
	
	/**
	 * Get customer name
	 * 
	 * This method will return the customer name that may have been set 
	 * previously with {@link M_MoneyTransaction::setCustomerName()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getCustomerName() {
		return $this->_customerName;
	}
	
	/**
	 * Get payment form's page title
	 * 
	 * This method will return the page title that may have been set 
	 * previously with {@link M_MoneyTransaction::setPageTitle()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getPageTitle() {
		return $this->_pageTitle;
	}
	
	/**
	 * Get currency
	 * 
	 * This method will return the currency that may have been set 
	 * previously with {@link M_MoneyTransaction::setCurrency()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getCurrency() {
		return $this->_currency;
	}
	
	/**
	 * Get Amount
	 * 
	 * This method will return the amount that may have been set 
	 * previously with {@link M_MoneyTransaction::setAmount()}.
	 * 
	 * @access public
	 * @return float
	 */
	public function getAmount() {
		return $this->_amount;
	}
	
	/**
	 * Get Payment Method
	 * 
	 * Will return the set payment method, e.g. "CreditCard"
	 * 
	 * @see M_MoneyTransaction::setPaymentMethod()
	 * @access public
	 * @return string
	 */
	public function getPaymentMethod() {
		return $this->_paymentMethod;
	}
	
	/**
	 * Get Payment Method Brand
	 * 
	 * Will return the set payment method brand, e.g. "MasterCard" or "VISA"
	 * 
	 * @see M_MoneyTransaction::setPaymentMethodBrand()
	 * @access public
	 * @return string
	 */
	public function getPaymentMethodBrand() {
		return $this->_paymentMethodBrand;
	}
	
	/**
	 * Get Payment Card Number
	 * 
	 * Will return the set payment card number, e.g. "XXXXXXXXXXXX9999"
	 * 
	 * @see M_MoneyTransaction::setPaymentCardNumber()
	 * @access public
	 * @return string
	 */
	public function getPaymentCardNumber() {
		return $this->_paymentCardNumber;
	}
	
	/**
	 * Get Payment Card Holder Name
	 * 
	 * Will return the set payment card holder name, e.g. "John Doe"
	 * 
	 * @see M_MoneyTransaction::setPaymentCardHolderName()
	 * @access public
	 * @return string
	 */
	public function getPaymentCardHolderName() {
		return $this->_paymentCardHolderName;
	}
	
	/**
	 * Get Payment Card Expiration Month And Year
	 * 
	 * Will return the set payment card expiration date in month and year
	 * format, e.g. "0421" for April 2021
	 * 
	 * @see M_MoneyTransaction::setPaymentCardExpirationMonthAndYear()
	 * @access public
	 * @return string
	 */
	public function getPaymentCardExpirationMonthAndYear() {
		return $this->_paymentCardExpirationMonthAndYear;
	}
	
	/**
	 * Get Private Key In
	 * 
	 * This method will return the Private Key In that may have been 
	 * set previously with {@link M_MoneyTransaction::setPrivateKeyIn()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getPrivateKeyIn() {
		return $this->_privateKeyIn;
	}
	
	/**
	 * Get Private Key Out
	 * 
	 * This method will return the Private Key Out that may have been 
	 * set previously with {@link M_MoneyTransaction::setPrivateKeyOut()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getPrivateKeyOut() {
		return $this->_privateKeyOut;
	}
	
	/**
	 * Get Order ID
	 * 
	 * This method will return the Order ID that may have been set 
	 * previously with {@link M_MoneyTransaction::setOrderNumber()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getOrderNumber() {
		return $this->_orderId;
	}
	
	/**
	 * Get language
	 * 
	 * This method will return the language that may have been set 
	 * previously with {@link M_MoneyTransaction::setLanguage()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getLanguage() {
		return $this->_language;
	}
	
	/**
	 * Get country
	 * 
	 * This method will return the country that may have been set 
	 * previously with {@link M_MoneyTransaction::setCountry()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getCountry() {
		return $this->_country;
	}
	
	/**
	 * Get locale name
	 * 
	 * Will provide with the locale name, in the standard notation. Consider 
	 * the following example:
	 * 
	 * Example 1, display the interface in Dutch (Belgium)
	 * 
	 * <code>
	 *    $transaction = new M_MoneyTransaction;
	 *    $transaction->setLanguage('nl');
	 *    $transaction->setCountry('BE');
	 * </code>
	 * 
	 * With the example as illustrated above, this method would produce the 
	 * following output:
	 * 
	 * <code>nl_BE</code>
	 * 
	 * For more information on the format of locale names, you should read the
	 * documentation on {@link M_Locale::getNameParts()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getLocaleName() {
		// if the language has not yet been set:
		if(! $this->_language) {
			// We throw an exception, because we cannot format the locale name
			// without a language code
			throw new M_MoneyTransactionException(sprintf(
				'Cannot format locale name for transaction; missing language code'
			));
		}
		
		// The locale name starts with a language code:
		$out = strtolower((string) $this->_language);
		
		// If a country has been provided:
		if($this->_country) {
			// Then, we add the country to the locale name
			$out .= '_' . strtoupper((string) $this->_country);
		}
		
		// Return the locale name:
		return $out;
	}
	
	/**
	 * Get Redirect URL
	 * 
	 * This method will provide with the URL that may have been set
	 * previously with {@link M_MoneyTransaction::setRedirectUrl()},
	 * for a given outcome.
	 * 
	 * Example 1, get the URL for successful payment
	 * 
	 * <code>
	 *    // Mount transaction
	 *    $transaction = M_MoneyTransaction::factory(new M_Config(array(
	 *       'type' => 'ogone',
	 *       'merchant' => 'MULTIMEDIUM',
	 *       'currency' => 'EUR'
	 *       'amount' => 200,
	 *       'privateKey' => 'MySHA1Signature',
	 *       'orderId' => 'ORDER24PO34',
	 *       'language' => 'en_US',
	 *       'redirect' => array(
	 *          'ok' => 'http://www.multimedium.be/payment/ok',
	 *          'ko' => 'http://www.multimedium.be/payment/error'
	 *       )
	 *    )));
	 *    
	 *    // Get page of successful payment:
	 *    echo $transaction->getRedirectUrl(M_MoneyTransaction::SUCCESS);
	 * </code>
	 * 
	 * NOTE:
	 * If the requested URL could not have been found, this method
	 * will return FALSE!
	 * 
	 * @access public
	 * @param string $flag
	 * 		The possible outcome of the payment
	 * @return string
	 */
	public function getRedirectUrl($flag) {
		return (isset($this->_redirect[$flag]) ? $this->_redirect[$flag] : FALSE);
	}
	
	/**
	 * Is testing account
	 * 
	 * Indicates whether or not the merchant's account (used for the transaction)
	 * is a testing account
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if testing account, FALSE if not
	 */
	public function isTestingAccount() {
		return $this->_isTestingAccount;
	}
	
	/**
	 * Get status
	 * 
	 * Will provide with the status of the money transaction. Typically, this
	 * method is used to check if the payment has been authorized, is awaiting
	 * authorization, or ...
	 * 
	 * @access public
	 * @return string
	 */
	public function getStatus() {
		return $this->_status;
	}
	
	/* -- ABSTRACT -- */
	
	/**
	 * Is Payment Authorized?
	 * 
	 * Will use {@link M_MoneyTransaction::getStatus()} to check whether or not
	 * the payment has been authorized.
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The response (payment feedback) varies from provider 
	 * to provider, and each requires a specific implementation.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the payment has been authorized, FALSE if not
	 */
	abstract public function isPaymentAuthorized();
	
	/**
	 * Is Payment Confirmed?
	 * 
	 * Will use {@link M_MoneyTransaction::getStatus()} to check whether or not
	 * the payment has been confirmed.
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The response (payment feedback) varies from provider 
	 * to provider, and each requires a specific implementation.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the payment has been confirmed, FALSE if not
	 */
	abstract public function isPaymentConfirmed();

	/**
	 * Is Payment Ok?
	 * 
	 * Can use {@link M_MoneyTransaction::isPaymentAuthorized()} and 
	 * {@link M_MoneyTransaction::isPaymentConfirmed()} to check whether or not
	 * the payment has been confirmed and/or authorized.
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The response (payment feedback) varies from provider 
	 * to provider, and each requires a specific implementation.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the payment is OK, FALSE if not
	 */
	abstract public function isPaymentOk();

	/**
	 * Get form
	 * 
	 * This method will return the (constructed) {@link M_Form} object,
	 * that is used to start the transaction. Typically, this form will
	 * not contain any field, except for a collection of hidden fields
	 * (form variables; see {@link M_Form::setVariable()}).
	 * 
	 * NOTE:
	 * This method will return a basic {@link M_Form}, which contains
	 * only hidden fields. To add interaction to the form (typically,
	 * a "Proceed to payment" button), you should add more fields to
	 * the form that is returned by this method. You can use the
	 * method {@link M_Form::addField()} to do so.
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The collection of hidden fields that need to be
	 * included in a payment form vary from provider to provider.
	 * 
	 * @access public
	 * @return M_Form
	 */
	abstract public function getForm();
	
	/**
	 * Import response
	 * 
	 * This method will accept
	 * 
	 * - The Page Request Method
	 * - The Page Request Variables
	 * 
	 * and check if the response of the external entity is valid (and
	 * can be trusted). In many cases, this method will also use the
	 * Private Key Out to check the authenticity (genuineness) of the
	 * response it has been provided with.
	 * 
	 * This method will return TRUE if the response is valid, FALSE if
	 * the response seems to have been tampered with.
	 * 
	 * In the process of validating the response, this method also sets
	 * the properties of the transaction in question (if the validation
	 * result is TRUE, of course). This way, you can use the same object
	 * of {@link M_MoneyTransaction} to get info about the transaction, 
	 * once the response has been checked and validated. Check out 
	 * Example 1.
	 * 
	 * Example 1
	 * 
	 * <code>
	 *    // Construct the transaction object:
	 *    $transaction = M_MoneyTransaction::factory(new M_ConfigXml('config/ogone.xml'));
	 *    
	 *    // Import the external entity's response:
	 *    $ok = $transaction->importResponse(M_Request::getMethod(), M_Request::getVariables());
	 *    
	 *    // If the response has been validated (and can be trusted)
	 *    if($ok) {
	 *       // We get some information about the transaction:
	 *       // - Order number
	 *       echo $transaction->getOrderNumber();
	 *       // - Get currency
	 *       echo $transaction->getCurrency();
	 *       // - Get amount
	 *       echo $transaction->getAmount();
	 *       /// etc...
	 *    }
	 * </code>
	 * 
	 * NOTE:
	 * All subclasses of {@link M_MoneyTransaction} should implement
	 * this method. The response (payment feedback) varies from provider 
	 * to provider, and each requires a specific implementation.
	 * 
	 * @access public
	 * @return boolean
	 */
	abstract public function importResponse($requestMethod, array $requestVariables);
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Set Status
	 * 
	 * Can be used to set the status of the transaction. This status is then 
	 * used by
	 * 
	 * - {@link M_MoneyTransaction::getStatus()}
	 * - {@link M_MoneyTransaction::isPaymentAuthorized()}
	 * 
	 * @access public
	 * @param string $statusCode
	 * @return void
	 */
	protected function _setStatus($statusCode) {
		$this->_status = $statusCode;
	}
}