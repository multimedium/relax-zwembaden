<?php
/**
 * M_CalendarIcsEvent
 * 
 * M_CalendarIcsEvent is a calendar component for {@link M_CalendarIcs}.
 * 
 * The body of the iCalendar object ({@link M_CalendarIcs}) is made up of a list 
 * of calendar properties and one or more "calendar components". The calendar 
 * properties apply to the entire calendar. The calendar components are several 
 * calendar properties which create a calendar schematic (design). 
 * 
 * For example, the calendar component can specify an event, a to-do list, a 
 * journal entry, time zone information, or free/busy time information, or 
 * an alarm. For more information about calendar components, read the information
 * at {@link http://en.wikipedia.org/wiki/ICalendar}
 * 
 * This specific class is used to represent a specific calendar component: an
 * event.
 * 
 * @package Core
 */
class M_CalendarIcsEvent extends M_CalendarIcsComponentAbstract {
	
	/* -- CONSTANTS -- */
	
	/* -- CONSTANTS: Component status -- */
	
	/**
	 * Event status: Draft
	 * 
	 * This constant can be used to describe the status of the event. Typically,
	 * this constance is used with {@link M_CalendarIcsEvent::setStatus()}.
	 * 
	 * @var string 
	 */
	const STATUS_DRAFT = 'DRAFT';
	
	/**
	 * Event status: Tentative
	 * 
	 * This constant can be used to describe the status of the event. Typically,
	 * this constance is used with {@link M_CalendarIcsEvent::setStatus()}.
	 * 
	 * @var string 
	 */
	const STATUS_TENTATIVE = 'TENTATIVE';
	
	/**
	 * Event status: Needs Action
	 * 
	 * This constant can be used to describe the status of the event. Typically,
	 * this constance is used with {@link M_CalendarIcsEvent::setStatus()}.
	 * 
	 * @var string 
	 */
	const STATUS_NEEDS_ACTION = 'NEEDS_ACTION';
	
	/**
	 * Event status: Cancelled
	 * 
	 * This constant can be used to describe the status of the event. Typically,
	 * this constance is used with {@link M_CalendarIcsEvent::setStatus()}.
	 * 
	 * @var string 
	 */
	const STATUS_CANCELLED = 'CANCELLED';
	
	/* -- CONSTANTS: Attendee status -- */
	
	/**
	 * Attendee status: Accepted
	 * 
	 * This constant can be used to describe the status of the event's attendees.
	 * (Accepted; The attendee has confirmed his/her presence in the event or
	 * meeting)
	 * 
	 * @var string
	 */
	const ATTENDEE_STATUS_ACCEPTED = 'ACCEPTED';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Start date
	 *
	 * @see M_CalendarIcsEvent::setStartDate()
	 * @access private
	 * @var M_Date
	 */
	private $_startDate;
	
	/**
	 * End date
	 *
	 * @see M_CalendarIcsEvent::setEndDate()
	 * @access private
	 * @var M_Date
	 */
	private $_endDate;
	
	/**
	 * Is all day event?
	 * 
	 * @see M_CalendarIcsEvent::setIsAllDay()
	 * @access private
	 * @var bool
	 */
	private $_isAllDay;
	
	/**
	 * Last modified date
	 *
	 * @see M_CalendarIcsEvent::setLastModifiedDate()
	 * @access private
	 * @var M_Date
	 */
	private $_lastModifiedDate;
	
	/**
	 * Organizer
	 *
	 * @see M_CalendarIcsEvent::setOrganizer()
	 * @access private
	 * @var M_Contact
	 */
	private $_organizer;
	
	/**
	 * Attendee(s)
	 *
	 * @see M_CalendarIcsEvent::addAttendee()
	 * @access private
	 * @var array
	 */
	private $_attendees = array();
	
	/**
	 * Location
	 *
	 * @see M_CalendarIcsEvent::setLocation()
	 * @access private
	 * @var string
	 */
	private $_location;
	
	/**
	 * Summary
	 * 
	 * This property stores the summary. Typically, the summary is used as the
	 * title of the event, so this is a short/brief summary.
	 *
	 * @see M_CalendarIcsEvent::setSummary()
	 * @access private
	 * @var string
	 */
	private $_summary;
	
	/**
	 * Description
	 * 
	 * This property stores the description. Typically, this description is used
	 * as additional information about the event.
	 *
	 * @see M_CalendarIcsEvent::setDescription()
	 * @access private
	 * @var string
	 */
	private $_description;
	
	/**
	 * Status
	 *
	 * @see M_CalendarIcsEvent::setStatus()
	 * @access private
	 * @var string
	 */
	private $_status;
	
	/**
	 * Unique Identifier
	 *
	 * @see M_CalendarIcsEvent::setUniqueIdentifier()
	 * @access private
	 * @var integer
	 */
	private $_uid;
	
	/**
	 * Update Sequence
	 *
	 * @see M_CalendarIcsEvent::setUpdateSequence()
	 * @see M_CalendarIcsEvent::incrementUpdateSequenceByOne()
	 * @access private
	 * @var integer
	 */
	private $_updateSequence;

	/**
	 * Recurrence Rule
	 *
	 * @access private
	 * @var array
	 */
	private $_recurrence = array();
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @uses M_CalendarIcsComponentAbstract::_setType()
	 * @access public
	 * @param M_Date $startDate
	 * 		See {@link M_CalendarIcsEvent::setStartDate()}
	 * @param M_Date $endDate
	 * 		See {@link M_CalendarIcsEvent::setEndDate()}
	 * @param string $summary
	 * 		See {@link M_CalendarIcsEvent::setSummary()}
	 * @return M_CalendarIcsEvent
	 */
	public function __construct(M_Date $startDate = NULL, M_Date $endDate = NULL, $summary = NULL) {
		// Set the type of the calendar component
		$this->_setType('event');
		
		// If a start date has been provided
		if($startDate) {
			// Set the start date
			$this->setStartDate($startDate);
		}
		
		// If an end date has been provided
		if($endDate) {
			// Set the end date of the event
			$this->setEndDate($endDate);
		}
		
		// If a summary has been defined
		if($summary) {
			// Then, again, set the summary of the event
			$this->setSummary($summary);
		}
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set start date
	 * 
	 * This method can be used to set the start date of the event. Typically, this
	 * is used in combination with {@link M_CalendarIcsEvent::setEndDate()}
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @param M_Date $date
	 * @return void
	 */
	public function setStartDate(M_Date $date) {
		$this->_startDate = $date;
	}
	
	/**
	 * Set end date
	 * 
	 * This method can be used to set the end date of the event. Typically, this
	 * is used in combination with {@link M_CalendarIcsEvent::setStartDate()}
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @param M_Date $date
	 * @return void
	 */
	public function setEndDate(M_Date $date) {
		$this->_endDate = $date;
	}
	
	/**
	 * Set flag: is all day?
	 * 
	 * This method allows to specify whether or not the event is "all day". Note
	 * that this method will set the time of the start and end date to midnight
	 * too!
	 * 
	 * If the start or end date is not known, this method will throw an exception
	 * to inform about the error!
	 * 
	 * @throws M_Exception
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE if all day, FALSE if not
	 * @return void
	 */
	public function setIsAllDay($flag) {
		// If "all day" is set to TRUE:
		if($flag) {
			// If the start or end date is not known:
			if(! $this->_startDate || ! $this->_endDate) {
				// Then, we throw an exception to inform about the error:
				throw new M_Exception(sprintf(
					'Cannot set "all day event"; Missing start and/or end ' . 
					'date in ICS Component!'
				));
			}

			// Set dates to midnight:
			$this->_startDate->setTime(0, 0, 0);
			$this->_endDate->setTime(0, 0, 0);
		}
		
		// Set the flag:
		$this->_isAllDay = (bool) $flag;
	}
	
	/**
	 * Get last modified date
	 * 
	 * This method can be used to set the "last modified" date of the event.
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @param M_Date $date
	 * @return void
	 */
	public function setLastModifiedDate(M_Date $date) {
		$this->_lastModifiedDate = $date;
	}
	
	/**
	 * Set organizer
	 * 
	 * This method can be used to set the organizer of the event. The information
	 * about the organizer is contained by an instance of {@link M_Contact}
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @param M_Contact $organizer
	 * @return void
	 */
	public function setOrganizer(M_Contact $organizer) {
		$this->_organizer = $organizer;
	}
	
	/**
	 * Add attendee
	 * 
	 * This method can be used to add an attendee to the event/meeting. The info
	 * about the attendee is contained by an instance of {@link M_Contact}. 
	 * 
	 * Also, you can specify the status of the attendee, in the event/meeting; 
	 * e.g. "Accepted" (The attendee has confirmed his/her presence in the event
	 * or meeting)
	 * 
	 * @access public
	 * @param M_Contact $attendee
	 * 		The information about the attendee
	 * @param string $status
	 * 		The status of the attendee
	 * @return void
	 */
	public function addAttendee(M_Contact $attendee, $status = NULL) {
		$this->_attendees[] = array($attendee, $status);
	}
	
	/**
	 * Set location
	 * 
	 * This method can be used to define the location of the event/meeting.
	 * (eg. Conference Room - F123)
	 * 
	 * @access public
	 * @param string $location
	 * @return void
	 */
	public function setLocation($location) {
		$this->_location = (string) $location;
	}
	
	/**
	 * Set summary
	 * 
	 * This method can be used to set the summary of the meeting/event. Typically, 
	 * the summary is used as the title of the event, so this should be a very 
	 * brief summary.
	 * 
	 * NOTE:
	 * To set the additional description of the event, you should read the docs 
	 * on {@link M_CalendarIcsEvent::setDescription()}
	 * 
	 * @access public
	 * @param string $summary
	 * @return void
	 */
	public function setSummary($summary) {
		$this->_summary = (string) $summary;
	}
	
	/**
	 * Set description
	 * 
	 * This method can be used to set the description of the meeting/event.
	 * Typically, this description is used as additional information about the 
	 * event.
	 * 
	 * NOTE:
	 * To set the title/summary of the event, you should read the docs 
	 * on {@link M_CalendarIcsEvent::setSummary()}
	 * 
	 * @access public
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
	}
	
	/**
	 * Set the status of the event
	 * 
	 * This method can be used to set the status of the event. Different statusses
	 * may be assigned to an event:
	 * 
	 * - {@link M_CalendarIcsEvent::STATUS_CANCELLED}
	 * - {@link M_CalendarIcsEvent::STATUS_DRAFT}
	 * - {@link M_CalendarIcsEvent::STATUS_NEEDS_ACTION}
	 * - {@link M_CalendarIcsEvent::STATUS_TENTATIVE}
	 * 
	 * @access public
	 * @param string $status
	 * @return void
	 */
	public function setStatus($status) {
		$this->_status = (string) $status;
	}
	
	/**
	 * Set Unique ID
	 * 
	 * This method can be used to set the UID (Unique Identifier) of the event/
	 * meeting. This is particularly necessary to send updates of an event. For
	 * more info, read {@link M_CalendarIcsEvent::setUpdateSequence()}.
	 * 
	 * @access public
	 * @param string $uid
	 * 		The unique identifier of the event/meeting
	 * @return void
	 */
	public function setUniqueId($uid) {
		$this->_uid = (string) $uid;
	}
	
	/**
	 * Set update sequence
	 * 
	 * This method can be used to set the update sequence of the event/meeting.
	 * 
	 * NOTE:
	 * For sending an UPDATE for an event/meeting the UID should match the 
	 * original UID. The UID (Unique Identifier) can be set with the method
	 * {@link M_CalendarIcsEvent::setUniqueId()}. The other component property 
	 * to be set is:
	 * 
	 * <code>
	 *    SEQUENCE:<#Number-Of-Update>
	 * </code>
	 * 
	 * @access public
	 * @param integer $sequenceNumber
	 * @return integer
	 */
	public function setUpdateSequence($sequenceNumber) {
		$this->_updateSequence = (int) $sequenceNumber;
	}
	
	/**
	 * Increment the update sequence by one
	 * 
	 * Read {@link M_CalendarIcsEvent::setUpdateSequence()} for more info about
	 * the update sequence of the event.
	 * 
	 * @access public
	 * @return integer $sequence
	 * 		The new update sequence number
	 */
	public function incrementUpdateSequenceByOne() {
		if(! $this->_updateSequence) {
			$this->_updateSequence  = 1;
		} else {
			$this->_updateSequence += 1;
		}
		return $this->_updateSequence;
	}

	/**
	 * Add recurrence rule
	 *
	 * @access public
	 * @return M_CalendarIcsEvent $event
	 *		Returns itself, for a fluent programming interface
	 */
	public function addRecurrenceRule(M_CalendarRecurrenceRule $rule) {
		$this->_recurrence[] = $rule;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get start date
	 * 
	 * @see M_CalendarIcsEvent::setStartDate()
	 * @access public
	 * @return M_Date
	 */
	public function getStartDate() {
		return $this->_startDate;
	}
	
	/**
	 * Get end date
	 * 
	 * @see M_CalendarIcsEvent::setEndDate()
	 * @access public
	 * @return M_Date
	 */
	public function getEndDate() {
		return $this->_endDate;
	}
	
	/**
	 * Get flag: is all day?
	 * 
	 * @see M_CalendarIcsEvent::setIsAllDay()
	 * @access public
	 * @return bool
	 */
	public function isAllDay() {
		return $this->_isAllDay;
	}
	
	/**
	 * Get Last Modified Date
	 * 
	 * @see M_CalendarIcsEvent::setLastModifiedDate()
	 * @access public
	 * @return M_Date
	 */
	public function getLastModifiedDate() {
		return $this->_lastModifiedDate;
	}
	
	/**
	 * Get organizer
	 * 
	 * @see M_CalendarIcsEvent::setOrganizer()
	 * @access public
	 * @return M_Contact
	 */
	public function getOrganizer() {
		return $this->_organizer;
	}
	
	/**
	 * Get attendees
	 * 
	 * Will return the collection of attendees that have been attached to the event
	 * or meeting.
	 * 
	 * @see M_CalendarIcsEvent::addAttendee()
	 * @access public
	 * @return ArrayIterator $attendees
	 * 		The collection of {@link M_Contact} instances
	 */
	public function getAttendees() {
		// Construct the iterator
		$out  = new M_CalendarIcsAttendeesIterator();
		
		// For each of the attendees in the event/meeting
		foreach($this->_attendees as $attendee) {
			// Add the attendee to the iterator
			$out->addAttendee($attendee[0], $attendee[1]);
		}
		
		// Return the attendees:
		return $out;
	}
	
	/**
	 * Get location
	 * 
	 * @see M_CalendarIcsEvent::setLocation()
	 * @access public
	 * @return string
	 */
	public function getLocation() {
		return $this->_location;
	}
	
	/**
	 * Get summary
	 * 
	 * @see M_CalendarIcsEvent::setSummary()
	 * @access public
	 * @return string
	 */
	public function getSummary() {
		return $this->_summary;
	}
	
	/**
	 * Get description
	 * 
	 * @see M_CalendarIcsEvent::setDescription()
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 * Get status of the event/meeting
	 * 
	 * @see M_CalendarIcsEvent::setStatus()
	 * @access public
	 * @return string
	 */
	public function getStatus() {
		return $this->_status;
	}
	
	/**
	 * Get Unique Identifier
	 * 
	 * @see M_CalendarIcsEvent::setUniqueId()
	 * @access public
	 * @return integer
	 */
	public function getUniqueId() {
		return $this->_uid;
	}

	/**
	 * Get update sequence
	 *
	 * Read the docs on {@link M_CalendarIcsEvent::setUpdateSequence()}, for
	 * more information about the update sequence of the event/meeting.
	 *
	 * @access public
	 * @return integer
	 */
	public function getUpdateSequence() {
		return $this->_updateSequence;
	}

	/**
	 * Get recurrence rules
	 *
	 * Will provide with the collection of recurrence rules that has been added
	 * to the event. Each rule is represented by an instance of the core class
	 * {@link M_CalendarRecurrenceRule}
	 *
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getRecurrenceRules() {
		return new M_ArrayIterator($this->_recurrence);
	}
	
	/**
	 * Export to string
	 * 
	 * This method will export the source code of the component in the .ICS file.
	 * Typically, this is used by {@link M_CalendarIcs::toString()}, in order to
	 * generate the .ICS file.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// Start the output:
		$out  = M_CalendarIcsSourceHelper::getBeginTag('VEVENT');
		
		// Add the component's body source code
		$out .= $this->_getSourceOfComponentBody();
		
		// Finish the output
		$out .= M_CalendarIcsSourceHelper::getEndTag('VEVENT');
		
		// Return the final render:
		return $out;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Export contents to string
	 * 
	 * This method is used by:
	 * 
	 * - {@link M_CalendarIcsEvent::toString()}, and
	 * - {@link M_CalendarIcsTodo::toString()}
	 * 
	 * @access public
	 * @return string
	 */
	protected function _getSourceOfComponentBody() {
		// New lines in the source of the ICS Component:
		$nl = M_CalendarIcsSourceHelper::getNewLine();
		
		// Start with new line
		$out = $nl;
		
		// Se timezone related defaults: we have to use absolute time, and
		// no timezone related attributes are to be passed
		$tzAbsoluteTimeFlag = true;
		$tzAttributes = array();
		
		// Fetch the timezone definition, if any has been specified
		$timezoneDefinition = $this->getTimezoneDefinition();
		if($timezoneDefinition) {
			// It has been found, so specify that we don't have to use absolute
			// time, but use the time relative to the timezone instead
			$tzAbsoluteTimeFlag = false;
			
			// And add the attribute specifying the timezone id
			$tzAttributes['TZID'] = $timezoneDefinition->getTimezoneId();
		}
		
		// Check if a start date has been specified:
		if($this->_startDate) {
			// If so, add the definition of start date to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'DTSTART',
				M_CalendarIcsSourceHelper::getDateString(
					$this->_startDate, 
					$tzAbsoluteTimeFlag,
					////don't include time if this is an all-day event (ical-fix)
					$this->isAllDay() ? false : true 
				),
				$tzAttributes
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check if an end date has been defined:
		if($this->_endDate) {
			// If so, specify the end date in the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'DTEND',
				M_CalendarIcsSourceHelper::getDateString(
					$this->_endDate, 
					$tzAbsoluteTimeFlag,
					////don't include time if this is an all-day event (ical-fix)
					$this->isAllDay() ? false : true 
				),
				$tzAttributes
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// If marked as all day events:
		if($this->_isAllDay) {
			// Then, we add the tags to mark the component as such:
			$out .= 
			M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-FUNAMBOL-ALLDAY',
				M_CalendarIcsSourceHelper::getBooleanValue(TRUE)
			) . 
			$nl . 
			M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-MICROSOFT-CDO-ALLDAYEVENT',
				M_CalendarIcsSourceHelper::getBooleanValue(TRUE)
			) . 
			$nl . 
			M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-MICROSOFT-MSNCALENDAR-ALLDAYEVENT',
				M_CalendarIcsSourceHelper::getBooleanValue(TRUE)
			) . 
			$nl;
		}
		
		// Check if a date of last modification has been defined:
		if($this->_lastModifiedDate) {
			// If so, specify the last-modified date in the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'LAST-MODIFIED', 
				M_CalendarIcsSourceHelper::getDateString($this->_lastModifiedDate)
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check if a summary has been provided:
		if($this->_summary) {
			// If so, we add the summary to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'SUMMARY', 
				M_CalendarIcsSourceHelper::getStringValue($this->_summary)
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check if a description has been provided:
		if($this->_description) {
			// If so, we add the description to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'DESCRIPTION', 
				M_CalendarIcsSourceHelper::getStringValue($this->_description)
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check for a Unique ID
		if($this->_uid) {
			// If so, we add the UID to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'UID', 
				M_CalendarIcsSourceHelper::getStringValue($this->_uid)
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check for an update sequence:
		if($this->_updateSequence) {
			// If so, we add the summary to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag('SEQUENCE', $this->_updateSequence);
			
			// Add the new line
			$out .= $nl;
		}
		
		// We check if the organizer of the event is known. This organizer, if
		// any, will be represented by an instance of M_Contact
		if($this->_organizer) {
			// If an organizer is defined, we also check if the organizer's 
			// email address has been defined
			$email = $this->_organizer->getEmail();
			if($email) {
				// Attributes?
				$attributes = $this->_organizer->getName()
					? array('CN' => $this->_organizer->getName())
					: array();

				// If so, we add the email address to the source:
				$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
					'ORGANIZER', 
					'MAILTO:' . $email->getEmail(),
					$attributes
				);
				
				// Add the new line
				$out .= $nl;
			}
		}
		
		// For each of the attendees
		foreach($this->_attendees as $attendee) {
			// We check if the current attendee's email address has been defined
			$email = $attendee[0]->getEmail();
			if($email) {
				// Prepare the attributes of the current attendee:
				$attributes = $attendee[1]
					? array('PARTSTAT' => $attendee[1])
					: array();
				
				// We add the email address to the source:
				$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
					// Tag name
					'ATTENDEE', 
					// Email address as value
					'MAILTO:' . $email->getEmail(),
					// Attributes of the attendee (status)
					$attributes
				);
				
				// Add the new line
				$out .= $nl;
			}
		}
		
		// Check if a location is available
		if($this->_location) {
			// If so, we add the location to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'LOCATION',
				M_CalendarIcsSourceHelper::getStringValue($this->_location)
			);
			
			// Add the new line
			$out .= $nl;
		}

		// Check if a status is available
		if($this->_status) {
			// If so, we add the status to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'STATUS',
				M_CalendarIcsSourceHelper::getStringValue($this->_status)
			);

			// Add the new line
			$out .= $nl;
		}

		// Recurrence rule(s)
		foreach($this->_recurrence as $rule) {
			/* @var $rule M_CalendarRecurrenceRule */
			// If so, we add the rule to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'RRULE',
				$rule->toStringCalendarIcs()
			);

			// Add the new line
			$out .= $nl;
		}
		
		// Check if an alarm has been attached to the event
		// (this alarm defines how the user is notified by his/her Calendar
		// client application)
		$alarm = $this->getAlarm();
		if($alarm) {
			// If so, we add the alarm's source code to the source:
			$out .= $alarm->toString();
			
			// Add the new line
			$out .= $nl;
		}
		
		// Return the final render:
		return $out;
	}
}