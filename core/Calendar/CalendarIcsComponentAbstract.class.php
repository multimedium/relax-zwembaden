<?php
/**
 * M_CalendarIcsComponentAbstract
 * 
 * The body of the iCalendar object ({@link M_CalendarIcs}) is made up of a list 
 * of calendar properties and one or more "calendar components". The calendar 
 * properties apply to the entire calendar. The calendar components are several 
 * calendar properties which create a calendar schematic (design). 
 * 
 * For example, the calendar component can specify an event, a to-do list, a 
 * journal entry, time zone information, or free/busy time information, or 
 * an alarm. For more information about calendar components, read the information
 * at {@link http://en.wikipedia.org/wiki/ICalendar}
 * 
 * This specific class is used an abstract motherclass to the very specific
 * implementations of calendar components:
 * 
 * - {@link M_CalendarIcsEvent}
 * - {@link M_CalendarIcsTodo}
 * - ...
 * 
 * This class provides with functionality that is shared by most of the calendar
 * components. For example, it allows to nest in another component into the component: 
 * an alarm. Also, it provides with the standard getType() method.
 * 
 * @package Core
 */
abstract class M_CalendarIcsComponentAbstract implements MI_CalendarIcsComponent {
	
	/* -- PROPERTIES -- */
	
	/**
	 * The type of the component
	 * 
	 * @access private
	 * @var string
	 */
	private $_type;
	
	/**
	 * Alarm
	 * 
	 * @access private
	 * @var M_CalendarIcsAlarm
	 */
	private $_alarm;
	
	/**
	 * The timezone definition of the component
	 *
	 * @see M_CalendarIcsComponentAbstract::setTimezoneDefinition()
	 * @see M_CalendarIcsComponentAbstract::getTimezoneDefinition()
	 * @access private
	 * @var M_DateTimezoneDefinition
	 */
	private $_timezoneDefinition;
	
	/* -- SETTERS -- */
	
	/**
	 * Set alarm
	 * 
	 * This method can be used to define the alarm for the ICS Component. This
	 * alarm is represented by another ICS component, and can be nested inside
	 * other components, such as 
	 * 
	 * - {@link M_CalendarIcsEvent}
	 * - {@link M_CalendarIcsTodo}
	 * 
	 * @access public
	 * @param M_CalendarIcsAlarm $alarm
	 * @return void
	 */
	public function setAlarm(M_CalendarIcsAlarm $alarm) {
		$this->_alarm = $alarm;
	}
	
	/**
	 * Set timezone definition
	 *
	 * This method can be used to set the timezone definition of the component.
	 * Read the docs on 
	 * {@link M_CalendarIcsComponentAbstract::getTimezoneDefinition()}
	 * for more information.
	 *
	 * @access public
	 * @param M_DateTimezoneDefinition $timezone
	 * @return M_CalendarIcsComponentAbstract $component
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTimezoneDefinition(M_DateTimezoneDefinition $timezone) {
		$this->_timezoneDefinition = $timezone;
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get the type of the component
	 * 
	 * Will provide with the type of the component
	 * 
	 * @see M_CalendarIcsComponent::_setType()
	 * @access public
	 * @return void
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Get alarm
	 * 
	 * @see M_CalendarIcsComponent::setAlarm()
	 * @access public
	 * @return M_CalendarIcsAlarm
	 */
	public function getAlarm() {
		return $this->_alarm;
	}
	
	/**
	 * Get timezone definition
	 *
	 * This method will provide with the timezone definition of the component,
	 * which may have been set previously with
	 * {@link M_CalendarIcsComponentAbstract::setTimezoneDefinition()}. Note
	 * that, if no timezone definition has been provided to the
	 * calendar specifically, this method will return the default one.
	 *
	 * For more information about the default timezone, read the docs on:
	 * - {@link M_DateTimezoneDefinition}
	 * - {@link M_DateTimezoneDefinitionEuropeBrussels}
	 *
	 * @access public
	 * @return string
	 */
	public function getTimezoneDefinition() {
		// If no timezone definition has been set:
		if(is_null($this->_timezoneDefinition)) {
			// Then, return the default one:
			$this->_timezoneDefinition = new M_DateTimezoneDefinitionEuropeBrussels();
		}
		return $this->_timezoneDefinition;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/* -- SETTERS -- */
	
	/**
	 * Set the type of the coponent
	 * 
	 * This (protected) method can be used to set the type of the component.
	 * This method is used by the subclasses of this (abstract) class, to set 
	 * the type of the component.
	 * 
	 * @access protected
	 * @param string $type
	 * @return void
	 */
	protected function _setType($type) {
		// Check if type has been recognized
		if(! in_array($type, array('event', 'todo', 'journal', 'alarm'))) {
			// Throw exception, if not
			throw new M_CalendarException(sprintf(
				'Unsupported ICS Component Type: %s',
				(string) $type
			));
		}
		
		// Set the type
		$this->_type = (string) $type;
	}
}