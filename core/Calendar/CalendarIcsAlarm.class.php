<?php
/**
 * M_CalendarIcsAlarm
 * 
 * M_CalendarIcsAlarm is a calendar component for {@link M_CalendarIcs}.
 * 
 * The body of the iCalendar object ({@link M_CalendarIcs}) is made up of a list 
 * of calendar properties and one or more "calendar components". The calendar 
 * properties apply to the entire calendar. The calendar components are several 
 * calendar properties which create a calendar schematic (design). 
 * 
 * For example, the calendar component can specify an event, a to-do list, a 
 * journal entry, time zone information, or free/busy time information, or 
 * an alarm. For more information about calendar components, read the information
 * at {@link http://en.wikipedia.org/wiki/ICalendar}
 * 
 * This specific class is used to represent a specific calendar component: an
 * alarm.
 * 
 * @package Core
 */
class M_CalendarIcsAlarm implements MI_CalendarIcsComponent {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Action: Audio
	 * 
	 * This constant can be used to indicate the action that should be undertaken 
	 * by the alarm. Typically, used with {@link M_CalendarIcsAlarm::setAction()}.
	 * 
	 * @var string 
	 */
	const ACTION_AUDIO = 'AUDIO';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Alarm action
	 * 
	 * Defaulted to "AUDIO"
	 * 
	 * @see M_CalendarIcsAlarm::setAction()
	 * @access private
	 * @var string
	 */
	private $_action = 'AUDIO';
	
	/**
	 * Alarm trigger date + time
	 * 
	 * @see M_CalendarIcsAlarm::setTriggerDate()
	 * @access public
	 * @var M_Date
	 */
	private $_triggerDate;
	
	/**
	 * Audio URI
	 * 
	 * The URI at which the audio file (for the alarm) is available.
	 * 
	 * @see M_CalendarIcsAlarm::setAudioUri()
	 * @access private
	 * @var M_Uri
	 */
	private $_audioUri;
	
	/**
	 * Number of repeats
	 * 
	 * This property stores the number of times that the action of the alarm
	 * ({@link M_CalendarIcsAlarm::$_action}) should be repeated.
	 * 
	 * @see M_CalendarIcsAlarm::setRepeat()
	 * @access private
	 * @var integer
	 */
	private $_repeats;
	
	/**
	 * Duration
	 * 
	 * This property stores the duration of the alarm's repeat, expressed in a 
	 * number of seconds.
	 * 
	 * @see M_CalendarIcsAlarm::setDuration()
	 * @access private
	 * @var integer
	 */
	private $_duration;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param M_Date $triggerDate
	 * 		See {@link M_CalendarIcsAlarm::setTriggerDate()}
	 * @param integer $numberOfRepeats
	 * 		See {@link M_CalendarIcsAlarm::setRepeat()}
	 * @return M_CalendarIcsAlarm
	 */
	public function __construct(M_Date $triggerDate = NULL, $numberOfRepeats = 2) {
		// If a trigger date has been provided
		if($triggerDate) {
			// Set the trigger date:
			$this->setTriggerDate($triggerDate);
		}
		
		// If number of repeats is provided
		if($numberOfRepeats) {
			// Set the number of repeats
			$this->setRepeat($numberOfRepeats);
		}
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set action
	 * 
	 * This method can be used to set the action that must be undertaken by the 
	 * alarm. For example, if you want an audio file to be played, you could do
	 * the following:
	 * 
	 * <code>
	 *    $alarm = new M_CalendarIcsAlarm();
	 *    $alarm->setAction(M_CalendarIcsAlarm::ACTION_AUDIO);
	 * </code>
	 * 
	 * @access public
	 * @param string $action
	 * @return void
	 */
	public function setAction($action) {
		$this->_action = (string) $action;
	}
	
	/**
	 * Set trigger date
	 * 
	 * This method can be used to set the date at which the alarm should be triggered.
	 * 
	 * @access public
	 * @param M_Date $date
	 * @return void
	 */
	public function setTriggerDate(M_Date $date) {
		$this->_triggerDate = $date;
	}
	
	/**
	 * Set Audio URI
	 * 
	 * This method can be used to set the URI at which the audio file is being
	 * made available.
	 * 
	 * @access public
	 * @param M_Uri $audioUri
	 * @return void
	 */
	public function setAudioUri(M_Uri $audioUri) {
		$this->_audioUri = $audioUri;
	}
	
	/**
	 * Set number of repeats
	 * 
	 * This method can be used to define how many times the action of the alarm
	 * should be repeated. For more information about the action of the alarm,
	 * read the docs {@link M_CalendarIcsAlarm::setAction()}
	 * 
	 * <code>
	 *    $alarm = new M_CalendarIcsAlarm();
	 *    $alarm->setAction(M_CalendarIcsAlarm::ACTION_AUDIO);
	 *    
	 *    // Repeat the alarm 4 additional times
	 *    $alarm->setRepeat(4);
	 * </code>
	 * 
	 * @see M_CalendarIcsAlarm::setDuration()
	 * @access public
	 * @param integer $numberOfRepeats
	 * @return void
	 */
	public function setRepeat($numberOfRepeats) {
		$this->_repeats = (int) $numberOfRepeats;
	}
	
	/**
	 * Set the duration of the alarm
	 * 
	 * This method can be used to set the duration of the alarm's repeat. This 
	 * duration is expressed in a number of seconds.
	 * 
	 * For example, if you want the alarm to repeat hourly, 2 additional times, 
	 * you would want to do the following:
	 * 
	 * <code>
	 *    $alarm = new M_CalendarIcsAlarm();
	 *    $alarm->setRepeat(2);
	 *    $alarm->setDuration(M_Date::getNumberOfSeconds(1, M_Date::HOUR));
	 * </code>
	 * 
	 * @access public
	 * @param integer $numberOfSeconds
	 * @return void
	 */
	public function setDuration($numberOfSeconds) {
		$this->_duration = (int) $numberOfSeconds;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get action
	 * 
	 * @see M_CalendarIcsAlarm::setAction()
	 * @access public
	 * @return string
	 */
	public function getAction() {
		return $this->_action;
	}
	
	/**
	 * Get trigger date
	 * 
	 * @see M_CalendarIcsAlarm::setTriggerDate()
	 * @access public
	 * @return M_Date
	 */
	public function getTriggerDate() {
		return $this->_triggerDate;
	}
	
	/**
	 * Get Audio URI
	 * 
	 * @see M_CalendarIcsAlarm::setAudioUri()
	 * @access public
	 * @return M_Uri
	 */
	public function getAudioUri() {
		return $this->_audioUri;
	}
	
	/**
	 * Get number of repeats
	 * 
	 * @see M_CalendarIcsAlarm::setRepeat()
	 * @access public
	 * @return integer
	 */
	public function getRepeat() {
		return $this->_repeats;
	}
	
	/**
	 * Get duration
	 * 
	 * @see M_CalendarIcsAlarm::setDuration()
	 * @access public
	 * @return integer
	 */
	public function getDuration() {
		return $this->_duration;
	}
	
	/**
	 * Get the type of the component
	 * 
	 * Will provide with the type of the component
	 * 
	 * @access public
	 * @return void
	 */
	public function getType() {
		return 'alarm';
	}
	
	/**
	 * Export to string
	 * 
	 * This method will export the source code of the component in the .ICS file.
	 * Typically, this is used by 
	 * 
	 * - {@link M_CalendarIcsEvent::toString()}
	 * - {@link M_CalendarIcsTodo::toString()}
	 * - ...
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// New lines in the source of the ICS Component:
		$nl = M_CalendarIcsSourceHelper::getNewLine();
		
		// Start the output:
		$out  = M_CalendarIcsSourceHelper::getBeginTag('VALARM');
		
		// Add a new line
		$out .= $nl;
		
		// Check if an action has been specified:
		if($this->_action) {
			// If so, add the action:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag('ACTION', $this->_action);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check if a trigger date has been set:
		if($this->_triggerDate) {
			// Add the trigger date to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'TRIGGER', 
				M_CalendarIcsSourceHelper::getDateString($this->_triggerDate)
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check if an audio (URI) has been attached
		if($this->_audioUri) {
			// If so, attach to the source
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag('ATTACH', $this->_audioUri->toString(), array(
				'FMTTYPE' => 'audio/basic'
			));
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check if number of repeats has been defined:
		if($this->_repeats) {
			// If so, we add this definition to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag('REPEAT', (int) $this->_repeats);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check if duration has been specified
		if($this->_duration) {
			// If so, we add this definition to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'DURATION', 
				M_CalendarIcsSourceHelper::getDurationString($this->_duration)
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Finish the output
		$out .= M_CalendarIcsSourceHelper::getEndTag('VALARM');
		
		// Return the final render:
		return $out;
	}
}