<?php
/**
 * M_CalendarIcsTodo
 * 
 * M_CalendarIcsTodo is a calendar component for {@link M_CalendarIcs}.
 * 
 * The body of the iCalendar object ({@link M_CalendarIcs}) is made up of a list 
 * of calendar properties and one or more "calendar components". The calendar 
 * properties apply to the entire calendar. The calendar components are several 
 * calendar properties which create a calendar schematic (design). 
 * 
 * For example, the calendar component can specify an event, a to-do list, a 
 * journal entry, time zone information, or free/busy time information, or 
 * an alarm. For more information about calendar components, read the information
 * at {@link http://en.wikipedia.org/wiki/ICalendar}
 * 
 * This specific class is used to represent a specific calendar component:
 * a to-do item/list.
 * 
 * @package Core
 */
class M_CalendarIcsTodo extends M_CalendarIcsEvent {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Date
	 *
	 * @see M_CalendarIcsEvent::setDate()
	 * @access private
	 * @var M_Date
	 */
	private $_date;
	
	/**
	 * Due Date
	 *
	 * @see M_CalendarIcsEvent::setDueDate()
	 * @access private
	 * @var M_Date
	 */
	private $_dueDate;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @uses M_CalendarIcsComponentAbstract::_setType()
	 * @access public
	 * @param M_Date $dueDate
	 * 		See {@link M_CalendarIcsTodo::setDueDate()}
	 * @param string $summary
	 * 		See {@link M_CalendarIcsEvent::setSummary()}
	 * @return M_CalendarIcsTodo
	 */
	public function __construct(M_Date $dueDate = NULL, $summary = NULL) {
		// Set the type of the calendar component
		$this->_setType('todo');
		
		// If a due date has been provided
		if($dueDate) {
			// Set the due date of the to-do
			$this->setDueDate($dueDate);
		}
		
		// If a summary has been defined
		if($summary) {
			// Then, again, set the summary of the event
			$this->setSummary($summary);
		}
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set date
	 * 
	 * This method can be used to set the date of the to-do item. Typically, this
	 * is used in combination with {@link M_CalendarIcsEvent::setDueDate()}
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @param M_Date $date
	 * @return void
	 */
	public function setDate(M_Date $date) {
		$this->_date = $date;
	}
	
	/**
	 * Set due date
	 * 
	 * This method can be used to set the due date of the to-do item. Typically, this
	 * is used in combination with {@link M_CalendarIcsEvent::setDate()}
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @param M_Date $date
	 * @return void
	 */
	public function setDueDate(M_Date $date) {
		$this->_dueDate = $date;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get date
	 * 
	 * @see M_CalendarIcsTodo::setDate()
	 * @access public
	 * @return M_Date
	 */
	public function getDate() {
		return $this->_date;
	}
	
	/**
	 * Get due date
	 * 
	 * @see M_CalendarIcsTodo::setDueDate()
	 * @access public
	 * @return M_Date
	 */
	public function getDueDate() {
		return $this->_dueDate;
	}
	
	/**
	 * Export to string
	 * 
	 * This method will export the source code of the component in the .ICS file.
	 * Typically, this is used by {@link M_CalendarIcs::toString()}, in order to
	 * generate the .ICS file.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// New lines in the source of the ICS Component:
		$nl = M_CalendarIcsSourceHelper::getNewLine();
		
		// Start the output:
		$out  = M_CalendarIcsSourceHelper::getBeginTag('VTODO');
		
		// Add the component's body source code
		$out .= $this->_getSourceOfComponentBody();
		
		// Check if a date has been specified:
		if($this->_date) {
			// If so, add the definition of the date to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'DTSTAMP', 
				M_CalendarIcsSourceHelper::getDateString($this->_date)
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Check if a due date has been specified:
		if($this->_dueDate) {
			// If so, add the definition of the due date to the source:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'DUE', 
				M_CalendarIcsSourceHelper::getDateString($this->_dueDate)
			);
			
			// Add the new line
			$out .= $nl;
		}
		
		// Finish the output
		$out .= M_CalendarIcsSourceHelper::getEndTag('VTODO');
		
		// Return the final render:
		return $out;
	}
}