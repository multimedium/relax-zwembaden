<?php
/**
 * M_CalendarIcs class
 * 
 * iCalendar (ICS) is a computer file format which allows internet users to send 
 * meeting requests and tasks to other internet users, via email, or sharing files 
 * with an .ics extension. 
 * 
 * Recipients of the iCalendar data file (with supporting software, such as an 
 * email client or calendar application) can respond to the sender easily or counter
 * propose another meeting date/time.
 * 
 * @package Core
 */
class M_CalendarIcs {
	
	/* -- PROPERTIES -- */

	/**
	 * The timezone definition of the calendar
	 *
	 * @see M_CalendarIcs::setTimezoneDefinition()
	 * @see M_CalendarIcs::getTimezoneDefinition()
	 * @access private
	 * @var M_DateTimezoneDefinition
	 */
	private $_timezoneDefinition;
	
	/**
	 * The name of the calendar
	 * 
	 * This property stores the name of the calendar
	 * 
	 * @see M_CalendarIcs::setName()
	 * @see M_CalendarIcs::getName()
	 * @access private
	 * @var string
	 */
	private $_name;
	
	/**
	 * The description of the calendar
	 * 
	 * This property stores the description of the calendar
	 * 
	 * @see M_CalendarIcs::setDescription()
	 * @see M_CalendarIcs::getDescription()
	 * @access private
	 * @var string
	 */
	private $_description;
	
	/**
	 * The color of the calendar
	 * 
	 * @see M_CalendarIcs::setColor()
	 * @see M_CalendarIcs::getColor()
	 * @access private
	 * @var M_Color
	 */
	private $_color;
	
	/**
	 * The Product ID of the calendar
	 * 
	 * @see M_CalendarIcs::setProductId()
	 * @see M_CalendarIcs::getProductId()
	 * @access private
	 * @var array
	 */
	private $_productId;
	
	/**
	 * ICS Components
	 * 
	 * This property stores the collection of ICS components in the calendar. The
	 * components are represented by instances that implement the interface
	 * {@link MI_CalendarIcsComponent}
	 * 
	 * @see M_CalendarIcs::addComponent()
	 * @see M_CalendarIcs::addEvent()
	 * @see M_CalendarIcs::addTodo()
	 * @see M_CalendarIcs::addJournal()
	 * @see M_CalendarIcs::addAlarm()
	 * @access public
	 * @var array
	 */
	private $_components = array();
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_CalendarIcs
	 */
	public function __construct() {
		// We set the default Product ID in the iCalendar Object:
		$this->setProductId('Multimedium BVBA', 'Multimanage');
	}

	/* -- STATIC CONSTRUCTORS -- */

	/**
	 * Construct from URI
	 *
	 * This method can be used to construct an object of {@link M_CalendarIcs},
	 * from a provided URL. Typically, this is used to read information from a
	 * webcal feed into the application.
	 *
	 * @static
	 * @uses M_CalendarIcs::constructWithString()
	 * @uses M_FilterTextUtf8Bom::apply()
	 * @access public
	 * @param M_Uri $uri
	 *		The URL of the webcal feed
	 * @return M_CalendarIcs
	 */
	public static function constructWithUri(M_Uri $uri) {
		// We construct a session, to retrieve information about the URL:
		$session = $uri->getSession();

		// If the URL does not exist:
		if(! $session->uriExists()) {
			// Then, we throw an exception to inform about the error:
			throw new M_CalendarException(sprintf(
				'Cannot construct an instance of %s with URI %s. The URL does ' .
				'not exist!',
				__CLASS__,
				$uri->toString()
			));
		}

		// We assume that the calendar is provided in UTF8. We remove the UTF8 BOM:
		$filter = new M_FilterTextUtf8Bom(new M_FilterTextValue($session->getContents()));

		// Construct the calendar with the string that is returned by the URI:
		return self::constructWithString($filter->apply());
	}

	/**
	 * Construct with file (ics)
	 *
	 * @static
	 * @uses M_CalendarIcs::constructWithString()
	 * @access public
	 * @param M_File $file
	 * 		The ICS file
	 * @return M_CalendarIcs
	 */
	public static function constructWithFile(M_File $file) {
		// We assume that the calendar is provided in UTF8. We remove the UTF8 BOM:
		$filter = new M_FilterTextUtf8Bom(new M_FilterTextValue($file->getContents()));

		// In order to construct a calendar from a file, we use the constructWithString()
		// method, and provide it with the file's source code:
		return self::constructWithString($filter->apply());
	}

	/**
	 * Construct from string
	 *
	 * This method can be used to construct an object of {@link M_CalendarIcs},
	 * based on the source code in ICS format...
	 *
	 * @static
	 * @access public
	 * @param string $string
	 *		The source code of an ICS file, or a webcal feed.
	 * @return M_CalendarIcs
	 */
	public static function constructWithString($string) {
		// We construct a new object of M_CalendarIcs
		$calendar = new self;

		// First of all, we get the lines from the provided source code:
		$lines = M_CalendarIcsSourceHelper::getLinesFromString($string);

		// We prepare some working variables:
		// LAYERS
		// The layers variable contains information about nested components. If
		// at a given moment we are parsing an alarm (BEGIN:VALARM) inside an event
		// (BEGIN:VEVENT), then this variable will contain 2 elements. At index
		// 0, the value will be VEVENT, and at index 1 the value is VALARM
		$layers = array();

		// NUMBER OF LAYERS
		// This variable contains the current number of layers. In other words,
		// it contains the count() value of the layers variable. If this count
		// is bigger than 0 (ZERO), we know that we are parsing information about
		// a component at that moment, instead of general calendar information.
		$numberOfLayers = 0;

		// CURRENT LAYER
		// This variable contains the name of the current layer. For example:
		// VEVENT, VTODO, VALARM, VJOURNAL, etc...
		$currentLayer = '';

		// COMPONENT
		// The component variable contains the calendar component that is being
		// created at a given moment. This variable may be an object, but only
		// if the number of layers at that time is bigger than 0 (ZERO). In any
		// other case, this variable is given the value NULL
		$component = null;
		$alarm     = null;

		// For each of the lines:
		foreach($lines as $lineNumber => $line) {
		
			// We place everything in a try{} block. This way, we can easily add
			// Line Numbers to the exceptions that are thrown during the process
			try {
				// We get the tag elements from the current line:
				$tag = M_CalendarIcsSourceHelper::getTagElements($line);

				// If the tag is the opening tag of a calendar component:
				if($tag['name'] == 'BEGIN' && $tag['value'] != 'VCALENDAR') {
					// Then, we add a new layer:
					$layers[] = $tag['value'];

					// And we update the number of layers:
					$numberOfLayers += 1;

					// Set the current layer:
					$currentLayer = $tag['value'];
					// Also, we create a new Calendar Component Object (Event, Todo,
					// Alarm, etc...). The object to be created, depends on the type
					// of event that is being opened with this tag.
					switch($tag['value']) {
						// Event
						case 'VFREEBUSY':
						case 'VEVENT':
							$component = new M_CalendarIcsEvent();
							break;

						// To-Do
						case 'VTODO':
							$component = new M_CalendarIcsTodo();
							break;

						// Journal
						case 'VJOURNAL':
							$component = NULL;
							break;

						// Alarm
						case 'VALARM':
							$alarm = new M_CalendarIcsAlarm();
							break;
					}
				}
				// If the current tag is the closing tag of a calendar component
				elseif($tag['name'] == 'END' && $tag['value'] != 'VCALENDAR') {
					// If no layers have been opened before
					if(array_pop($layers) === FALSE) {
						// Then, it means that the closing tag has been provided
						// before even having opened the tag:
						throw new M_Exception(sprintf(
							'Cannot parse Calendar ICS Source Code. Unexpected tag ' .
							'END:%s. The tag has not been opened.',
							$tag['value']
						));
					}

					// We update the number of layers:
					$numberOfLayers -= 1;
					
					// Note that we compare the removed layer with the closing tag,
					// in order to make sure that a valid syntax is being provided:
					if($currentLayer != $tag['value']) {
						// If the last layer does not match the name of the closing
						// tag, then we assume that a corrupt file is being provided.
						// In this case, we throw an exception to inform about the
						// error
						throw new M_Exception(sprintf(
							'Cannot parse Calendar ICS Source Code. Unexpected tag ' .
							'END:%s, expecting END:%s instead!',
							$tag['value'],
							$currentLayer
						));
					}

					// At this stage, a calendar component should have been constructed.
					// This component is either an event, a to-do, a journal or an
					// alarm. So, we use a switch-case block to further handle the
					// component:
					switch($currentLayer) {
						// Event
						case 'VFREEBUSY':
						case 'VEVENT':
							// Add the event to the calendar
							$calendar->addEvent($component);

							// Re-set the component, for the next item in the ICS
							$component = null;

							// Done with the event
							break;

						// To-Do
						case 'VTODO':
							// Add the event to the calendar
							$calendar->addTodo($component);

							// Re-set the component, for the next item in the ICS
							$component = null;

							// Done with the ti-do
							break;

						// Journal
						case 'VJOURNAL':
							break;

						// Alarm
						case 'VALARM':
							// An alarm can be attached to another component, or to
							// the calendar. We decide this with the current number
							// of layers. If the number of layers is still bigger
							// than 0 (ZERO)
							if($numberOfLayers > 0) {
								// Then, the alarm should be attached to the previously
								// constructed component
								$component->setAlarm($alarm);
							}
							// If zero
							else {
								// Then, the alarm is for the calendar:
								$calendar->addAlarm($alarm);
							}

							// Done with the alarm
							break;
					}

					// Update the current layer:
					$currentLayer = $numberOfLayers == 0 ? '' : $layers[$numberOfLayers - 1];
				}
				// If we are dealing with a piece of info about a calendar component:
				elseif($numberOfLayers > 0) {
					// The way we use the current piece of information, really depends
					// on the type of component we are dealing with at the moment...
					switch($currentLayer) {
						// To-Do
						case 'VTODO':
							// We construct another switch-case block, for all the
							// tags we recognize in the TODO component:
							switch($tag['name']) {
								// Date of the to-do
								case 'DTSTAMP':
									$component->setDate(M_CalendarIcsSourceHelper::getDateFromString($tag['value']));
									break;

								// DUE Date of the to-do
								case 'DUE':
									$component->setDueDate(M_CalendarIcsSourceHelper::getDateFromString($tag['value']));
									break;
							}

							// Note that, here, we do not include the "break" statement.
							// This because we want to continue in the event block,
							// since a to-do is is a subclass of events

							// break;

						// Event
						case 'VFREEBUSY':
						case 'VEVENT':
							// We construct another switch-case block, for all the
							// tags we recognize in the Event Component:
							switch($tag['name']) {
								// Start Date
								case 'DTSTART':
									$component->setStartDate(M_CalendarIcsSourceHelper::getDateFromString($tag['value']));
									break;

								// End Date
								case 'DTEND':
									$component->setEndDate(M_CalendarIcsSourceHelper::getDateFromString($tag['value']));
									break;

								// Last Modified date
								case 'LAST-MODIFIED':
									$component->setLastModifiedDate(M_CalendarIcsSourceHelper::getDateFromString($tag['value']));
									break;

								// Summary
								case 'SUMMARY':
									$component->setSummary($tag['value']);
									break;

								// Description
								case 'DESCRIPTION':
									$component->setDescription($tag['value']);
									break;

								// Set Unique ID
								case 'UID':
									$component->setUniqueId($tag['value']);
									break;

								// Update Sequence number
								case 'SEQUENCE':
									$component->setUpdateSequence($tag['value']);
									break;

								// The organizer of the event:
								case 'ORGANIZER':
									// An instance of M_Contact is expected as
									// an organizer. So, we construct that instance
									$contact = M_CalendarIcsSourceHelper::getContactFromString($tag['value']);

									// Set the organizer contact:
									$component->setOrganizer($contact);

									// Done :)
									break;

								// An attendee to the event:
								case 'ATTENDEE':
									// An instance of M_Contact is expected as
									// an attendee. So, we construct that instance
									$contact = M_CalendarIcsSourceHelper::getContactFromString($tag['value']);

									// Get the status (e.g. "Accepted") of the attendee:
									$contactStatus = isset($tag['@']['PARTSTAT'])
										? $tag['@']['PARTSTAT']
										: NULL;

									// Add the attendee to the event:
									$component->addAttendee($contact, $contactStatus);

									// Done :)
									break;

								// Event Location or Venue
								case 'LOCATION':
									$component->setLocation($tag['value']);
									break;

								// Event status
								case 'STATUS':
									$component->setStatus($tag['value']);
									break;

								// Recurrence Rule
								case 'RRULE':
									$component->addRecurrenceRule(M_CalendarRecurrenceRule::constructWithStringFromCalendarIcs($tag['value']));
									break;
							}

							// Done with event block
							break;

						// Journal
						case 'VJOURNAL':
							break;

						// Alarm
						case 'VALARM':
							// We construct another switch-case block, for all the
							// tags we recognize in the ALARM component:
							switch($tag['name']) {
								// Date of the to-do
								case 'TRIGGER':
									//@todo: trigger datestring is not always a formatted date, but can also be a string like -P0DT0H30M0S
									//which is a reference to the event-date subtracted with minutes/...
									//$alarm->setTriggerDate(M_CalendarIcsSourceHelper::getDateFromString($tag['value']));
									break;

								// Attached Audio (URI)
								case 'ATTACH':
									$alarm->setAudioUri(new M_Uri($tag['value']));
									break;

								// Number of repeats for the alarm:
								case 'REPEAT':
									$alarm->setRepeat($tag['value']);
									break;

								// Duration of the alarm
								case 'DURATION':
									$alarm->setDuration(M_CalendarIcsSourceHelper::getDurationFromString($tag['value']));
									break;
							}

							// Done with Alarm block
							break;
					}
				}
				// If we are not looking at any event-related tag
				else {
					// Then, the tag contains information about the calendar:
					switch($tag['name']) {
						// The version with which we comply in this ICS File
						case 'VERSION':
							break;

						// The Product ID
						case 'PRODID':
							// Get the Product Name Parts:
							$prodid = M_CalendarIcsSourceHelper::getProductIdFromString($tag['value']);

							// Set the Product ID
							$calendar->setProductId($prodid['company'], $prodid['product'], $prodid['language']);

							// Done with Product ID
							break;

						// Calendar Type (Typically Gregorian):
						case 'CALSCALE':
							break;

						// The Calendar's name
						case 'X-WR-CALNAME':
							$calendar->setName($tag['value']);
							break;

						// The Calendar's description
						case 'X-WR-CALDESC':
							$calendar->setDescription($tag['value']);
							break;

						// The color of the calendar (for example, in Apple iCal)
						case 'X-APPLE-CALENDAR-COLOR':
							$calendar->setColor(new M_Color($tag['value']));
							break;
					}
				}
			}
			// If an exception is thrown
			catch(Exception $e) {
				// Then, we throw a new one with the added information about
				// the position in ICS Source Code:
				throw new M_Exception(sprintf(
					'Line #%s in ICS Source Code: %s %s',
					$lineNumber,
					$e->getMessage(),
					'<pre>'. $string .'</pre>'
				));
			}
		}

		// Return the calendar:
		return $calendar;
	}
	
	/* -- SETTERS -- */

	/**
	 * Set timezone definition
	 *
	 * This method can be used to set the timezone definition of the calendar.
	 * Read the docs on {@link M_CalendarIcs::getTimezoneDefinition()}
	 * for more information.
	 *
	 * @access public
	 * @param M_DateTimezoneDefinition $timezone
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTimezoneDefinition(M_DateTimezoneDefinition $timezone) {
		$this->_timezoneDefinition = $timezone;
		return $this;
	}
	
	/**
	 * Set name
	 * 
	 * This method can be used to set the name of the calendar. Typically, this
	 * name will be used (as a default) by the Calendar application that is used
	 * by the visitor (eg. Apple iCal)
	 * 
	 * @access public
	 * @param string $name
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function setName($name) {
		$this->_name = (string) $name;
		return $this;
	}
	
	/**
	 * Set description
	 * 
	 * This method can be used to set the description of the calendar. Typically, 
	 * this name will be used (as a default) by the Calendar application that is 
	 * used by the visitor (eg. Apple iCal)
	 * 
	 * @access public
	 * @param string $description
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
		return $this;
	}
	
	/**
	 * Set color
	 * 
	 * This method can be used to set the color of the calendar. This color may
	 * then be adapted by the visitor's Calendar application (eg. Apple iCal)
	 * 
	 * @access public
	 * @param M_Color $color
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function setColor(M_Color $color) {
		$this->_color = $color;
		return $this;
	}
	
	/**
	 * Set Product ID
	 *
	 * Specifies the identifier for the product that created the iCalendar object
	 *
	 * This method can be used to set the Product ID in the calendar file. Typically,
	 * this is used to set the name of the application that has exported the
	 * ICS File.
	 * 
	 * Also, with this property, you have the possibility to set the language in
	 * which components (events, to-do's, etc.) in the iCalendar object are
	 * written. If not provided, this method will fall back to the default language
	 * (or English, if not set).
	 * 
	 * @access public
	 * @param string $companyName
	 * @param string $productName
	 * @param string $locale
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function setProductId($companyName, $productName, $locale = NULL) {
		// If no locale has been provided to this method
		if(! $locale) {
			// Then, we default to the locale that has been selected for the
			// application right now:
			$locale = M_Locale::getCategory(M_Locale::LANG);
		}

		// If still no locale could have been identified
		if(! $locale) {
			// Then, we default to English as the default language:
			$locale = 'en';
		}

		// Now, we get the locale parts. Note that the provided locale may come
		// with a language identifier, but also with a country name, a variant,
		// etc. We only need the language here:
		$localeParts = M_Locale::getNameParts($locale);

		// Now, we set the Product ID as a property of the calendar:
		$this->_productId = array(
			// The company name
			$companyName,
			// The Product ID
			$productName,
			// The language
			$localeParts['language']
		);

		// Return myself
		return $this;
	}
	
	/**
	 * Add component to the calendar
	 * 
	 * This method can be used to add a component to the calendar. The component
	 * is represented by an instance that implements {@link MI_CalenderIcsComponent}
	 * 
	 * @see M_CalendarIcs::addEvent()
	 * @see M_CalendarIcs::addTodo()
	 * @see M_CalendarIcs::addJournal()
	 * @see M_CalendarIcs::addAlarm()
	 * @access public
	 * @param MI_CalendarIcsComponent $component
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function addComponent(MI_CalendarIcsComponent $component) {
		$this->_components[] = $component;
		return $this;
	}
	
	/**
	 * Add event to the calendar
	 * 
	 * @uses M_CalendarIcs::addComponent()
	 * @access public
	 * @param M_CalendarIcsEvent $event
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function addEvent(M_CalendarIcsEvent $event) {
		return $this->addComponent($event);
	}
	
	/**
	 * Add to-do to the calendar
	 * 
	 * @uses M_CalendarIcs::addComponent()
	 * @access public
	 * @param M_CalendarIcsTodo $todo
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function addTodo(M_CalendarIcsTodo $todo) {
		return $this->addComponent($todo);
	}
	
	/**
	 * Add journal to the calendar
	 * 
	 * @uses M_CalendarIcs::addComponent()
	 * @access public
	 * @param M_CalendarIcsJournal $journal
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function addJournal(M_CalendarIcsJournal $journal) {
		return $this->addComponent($journal);
	}
	
	/**
	 * Add alarm to the calendar
	 * 
	 * @uses M_CalendarIcs::addComponent()
	 * @access public
	 * @param M_CalendarIcsAlarm $alarm
	 * @return M_CalendarIcs $calendar
	 *		Returns itself, for a fluent programming interface
	 */
	public function addAlarm(M_CalendarIcsAlarm $alarm) {
		return $this->addComponent($alarm);
	}
	
	/* -- GETTERS -- */

	/**
	 * Get timezone definition
	 *
	 * This method will provide with the timezone definition of the calendar,
	 * which may have been set previously with
	 * {@link M_CalendarIcs::setTimezoneDefinition()}. Note that, if no
	 * timezone definition has been provided to the calendar specifically, this
	 * method will return the default one.
	 *
	 * For more information about the default timezone, read the docs on:
	 * - {@link M_DateTimezoneDefinition}
	 * - {@link M_DateTimezoneDefinitionEuropeBrussels}
	 *
	 * @access public
	 * @return string
	 */
	public function getTimezoneDefinition() {
		// If no timezone definition has been set:
		if(is_null($this->_timezoneDefinition)) {
			// Then, return the default one:
			$this->_timezoneDefinition = new M_DateTimezoneDefinitionEuropeBrussels();
		}
		return $this->_timezoneDefinition;
	}
	
	/**
	 * Get name
	 * 
	 * This method will provide with the name of the calendar, which may have been
	 * set previously with {@link M_CalendarIcs::setName()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Get description
	 * 
	 * This method will provide with the description of the calendar, which may 
	 * have been set previously with {@link M_CalendarIcs::setDescription()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 * Get color
	 * 
	 * This method will provide with the color of the calendar, which may 
	 * have been set previously with {@link M_CalendarIcs::setColor()}.
	 * 
	 * @access public
	 * @return M_Color
	 */
	public function getColor() {
		return $this->_color;
	}
	
	/**
	 * Get Product ID: Company
	 * 
	 * This method will provide with the Product ID, but will provide only with 
	 * the name of the company.
	 * 
	 * @access public
	 * @return string
	 */
	public function getProductIdCompany() {
		return ($this->_productId
			? $this->_productId[0]
			: NULL
		);
	}

	/**
	 * Get Product ID: Product Name
	 *
	 * This method will provide with the Product ID, but will provide only with
	 * the name of the product.
	 *
	 * @access public
	 * @return string
	 */
	public function getProductIdProductName() {
		return ($this->_productId
			? $this->_productId[1]
			: NULL
		);
	}

	/**
	 * Get Product ID: Language
	 *
	 * This method will provide with the Product ID, but will provide only with
	 * the language part of it.
	 *
	 * @access public
	 * @return string
	 */
	public function getProductIdLanguage() {
		return ($this->_productId
			? $this->_productId[2]
			: NULL
		);
	}
	
	/**
	 * Get components
	 * 
	 * Will provide with the collection of components that may have been added
	 * previously to the calendar, with
	 * 
	 * - {@link M_CalendarIcs::addComponent()}
	 * - {@link M_CalendarIcs::addEvent()}
	 * - {@link M_CalendarIcs::addTodo()}
	 * - {@link M_CalendarIcs::addJournal()}
	 * 
	 * @access public
	 * @return ArrayIterator $components
	 * 		The collection of instances that implement {@link MI_CalendarIcsComponent}
	 */
	public function getComponents() {
		return new ArrayIterator($this->_components);
	}

	/**
	 * Get number of components
	 *
	 * Will provide with the number of components in the calendar.
	 *
	 * @see M_CalendarIcs::getComponents()
	 * @access public
	 * @return integer
	 */
	public function getNumberOfComponents() {
		return count($this->_components);
	}
	
	/**
	 * Export to string
	 * 
	 * This method will export the source code of the .ICS file.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// New lines in the source of the ICS Component:
		$nl = M_CalendarIcsSourceHelper::getNewLine();
		
		// Start the output:
		$out  = M_CalendarIcsSourceHelper::getBeginTag('VCALENDAR');
		$out .= $nl;
		
		// Indicate the version with which we comply in this ICS File
		$out .= M_CalendarIcsSourceHelper::getSelfClosingTag('VERSION', '2.0');
		$out .= $nl;
		
		// We add information about the timezone definition to the calendar:
		if($this->getTimezoneDefinition()) {
			// Add the timezone definition to the source
			$out .= $this->getTimezoneDefinition()->toString();
			
			// Add a new line
			$out .= $nl;
		}
		
		// (For now) We use the Gregorian Calendar
		$out .= M_CalendarIcsSourceHelper::getSelfClosingTag('CALSCALE', 'GREGORIAN');
		$out .= $nl;
		
		// Check if the calendar has been named:
		if($this->_name) {
			// If so, add the name:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-WR-CALNAME',
				M_CalendarIcsSourceHelper::getStringValue($this->_name)
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// Check if the calendar has been given a description:
		if($this->_description) {
			// If so, add the description:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-WR-CALDESC', 
				M_CalendarIcsSourceHelper::getStringValue($this->_description)
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// Check if a color has been assigned to the calendar
		if($this->_color) {
			// If so, add the color value:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-APPLE-CALENDAR-COLOR', 
				$this->_color->getHex(TRUE)
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// Check if the Product ID has been defined:
		if($this->_productId) {
			// If so, add the Product ID:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'PRODID', 
				M_CalendarIcsSourceHelper::getProductIdString(
					$this->_productId[0],
					$this->_productId[1],
					$this->_productId[2]
				)
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// For each of the components:
		foreach($this->_components as $component) {
			// Add the current component's source code to the ICS Source code
			$out .= $component->toString();
			
			// Add a new line
			$out .= $nl;
		}
		
		// Finish the output
		$out .= M_CalendarIcsSourceHelper::getEndTag('VCALENDAR');
		
		// Return the final render:
		return $out;
	}
}