<?php
/**
 * M_CalendarException class
 * 
 * Exceptions thrown by {@link M_Calendar} and related classes
 * 
 * @package Core
 */
class M_CalendarException extends M_Exception {
}