<?php
/**
 * M_CalendarRecurrenceRule
 *
 * Objects of this class describe a rule or a repeating pattern for recurring
 * items on a calendar.
 * 
 * @package Core
 */
class M_CalendarRecurrenceRule extends M_Object {

	/* -- CONSTANTS -- */

	/* -- CONSTANTS: FREQUENCIES -- */

	/**
	 * Frequency
	 *
	 * This constant can be used to set the frequency of the recurrence rule.
	 *
	 * @see M_CalendarRecurrenceRule::setFrequency();
	 * @access public
	 * @var string
	 */
	const FREQ_MINUTELY = 'MINUTELY';

	/**
	 * Frequency
	 *
	 * This constant can be used to set the frequency of the recurrence rule.
	 *
	 * @see M_CalendarRecurrenceRule::setFrequency();
	 * @access public
	 * @var string
	 */
	const FREQ_HOURLY = 'HOURLY';

	/**
	 * Frequency
	 *
	 * This constant can be used to set the frequency of the recurrence rule.
	 *
	 * @see M_CalendarRecurrenceRule::setFrequency();
	 * @access public
	 * @var string
	 */
	const FREQ_DAILY = 'DAILY';

	/**
	 * Frequency
	 *
	 * This constant can be used to set the frequency of the recurrence rule.
	 *
	 * @see M_CalendarRecurrenceRule::setFrequency();
	 * @access public
	 * @var string
	 */
	const FREQ_WEEKLY = 'WEEKLY';

	/**
	 * Frequency
	 *
	 * This constant can be used to set the frequency of the recurrence rule.
	 *
	 * @see M_CalendarRecurrenceRule::setFrequency();
	 * @access public
	 * @var string
	 */
	const FREQ_MONTHLY = 'MONTHLY';

	/**
	 * Frequency
	 *
	 * This constant can be used to set the frequency of the recurrence rule.
	 *
	 * @see M_CalendarRecurrenceRule::setFrequency();
	 * @access public
	 * @var string
	 */
	const FREQ_YEARLY = 'YEARLY';

	/* -- CONSTANTS: WEEKDAYS -- */

	/**
	 * Monday
	 *
	 * This constant can be used to address a given day of the week. This particular
	 * constant addresses Monday
	 *
	 * M_CalendarRecurrenceRule::addWeekday()
	 * @access public
	 * @var string
	 */
	const MONDAY = 'MO';

	/**
	 * Tuesday
	 *
	 * This constant can be used to address a given day of the week. This particular
	 * constant addresses Tuesday
	 *
	 * M_CalendarRecurrenceRule::addWeekday()
	 * @access public
	 * @var string
	 */
	const TUESDAY = 'TU';

	/**
	 * Wednesday
	 *
	 * This constant can be used to address a given day of the week. This particular
	 * constant addresses Wednesday
	 *
	 * M_CalendarRecurrenceRule::addWeekday()
	 * @access public
	 * @var string
	 */
	const WEDNESDAY = 'WE';

	/**
	 * Thursday
	 *
	 * This constant can be used to address a given day of the week. This particular
	 * constant addresses Thursday
	 *
	 * M_CalendarRecurrenceRule::addWeekday()
	 * @access public
	 * @var string
	 */
	const THURSDAY = 'TH';

	/**
	 * Friday
	 *
	 * This constant can be used to address a given day of the week. This particular
	 * constant addresses Friday
	 *
	 * M_CalendarRecurrenceRule::addWeekday()
	 * @access public
	 * @var string
	 */
	const FRIDAY = 'FR';

	/**
	 * Saturday
	 *
	 * This constant can be used to address a given day of the week. This particular
	 * constant addresses Saturday
	 *
	 * M_CalendarRecurrenceRule::addWeekday()
	 * @access public
	 * @var string
	 */
	const SATURDAY = 'SA';

	/**
	 * Sunday
	 *
	 * This constant can be used to address a given day of the week. This particular
	 * constant addresses Sunday
	 *
	 * M_CalendarRecurrenceRule::addWeekday()
	 * @access public
	 * @var string
	 */
	const SUNDAY = 'SU';

	/* -- CONSTANTS: MONTHS -- */

	/**
	 * January
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses January
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const JANUARY = 1;

	/**
	 * Februari
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses Februari
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const FEBRUARI = 2;

	/**
	 * March
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses March
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const MARCH = 3;

	/**
	 * April
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses April
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const APRIL = 4;

	/**
	 * May
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses May
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const MAY = 5;

	/**
	 * June
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses June
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const June = 6;

	/**
	 * July
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses July
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const JULY = 7;

	/**
	 * August
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses August
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const AUGUST = 8;

	/**
	 * September
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses September
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const SEPTEMBER = 9;

	/**
	 * October
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses October
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const OCTOBER = 10;

	/**
	 * November
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses November
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const NOVEMBER = 11;

	/**
	 * December
	 *
	 * This constant can be used to address a given month of the year. This
	 * particular constant addresses December
	 *
	 * M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @var string
	 */
	const DECEMBER = 12;

	/* -- PROPERTIES -- */

	/**
	 * Frequency
	 *
	 * This property stores the frequency that has been defined for the recurrence
	 * rule. Possible values for this property are:
	 *
	 * - {@link M_CalendarRecurrenceRule::FREQ_MINUTELY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_HOURLY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_DAILY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_WEEKLY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_MONTHLY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_YEARLY}
	 *
	 * @see M_CalendarRecurrenceRule::setFrequency()
	 * @access private
	 * @var string
	 */
	private $_frequency;

	/**
	 * End date, or "Until date"
	 *
	 * This property stores the date until which the recurrence rule is to be
	 * applied.
	 *
	 * @access private
	 * @var M_Date
	 */
	private $_endDate;

	/**
	 * Interval
	 *
	 * This property stores the end date of recurrence. For more information,
	 * please read the docs on {@link M_CalendarRecurrenceRule::setInterval()}.
	 *
	 * @access private
	 * @var integer
	 */
	private $_interval;

	/**
	 * Repeat count
	 *
	 * This property stores the number of times that the recurrence is to be
	 * repeated.
	 *
	 * @access private
	 * @var integer
	 */
	private $_count;

	/**
	 * Days of the year
	 *
	 * This property stores the days of the year that match with the recurrence
	 * rule.
	 *
	 * @see M_CalendarRecurrenceRule::addDayOfYear()
	 * @access private
	 * @var array
	 */
	private $_ydays = array();

	/**
	 * Week numbers
	 *
	 * This property stores the week numbers that match with the recurrence rule.
	 *
	 * @see M_CalendarRecurrenceRule::addWeeknumber()
	 * @access private
	 * @var array
	 */
	private $_weeknumbers = array();

	/**
	 * Weekdays
	 *
	 * This property stores the weekday(s) in the recurrence rule. Note that this
	 * property will not only store the weekday names, but may also store index
	 * numbers. For example, for the "first friday of the month"
	 *
	 * @see M_CalendarRecurrenceRule::addWeekday()
	 * @access private
	 * @var array
	 */
	private $_wdays = array();

	/**
	 * Set start day of the week
	 *
	 * This property can be used to set the first day of the week. This is important
	 * to calculate repetition in the recurrence. For more information, please
	 * read the docs on {@link M_CalendarRecurrenceRule::setStartDayOfWeek()}
	 *
	 * @see M_CalendarRecurrenceRule::setStartDayOfWeek()
	 * @access private
	 * @var string
	 */
	private $_weekstart = 'MO';

	/**
	 * Months
	 *
	 * This property stores the months in the recurring rule.
	 *
	 * @see M_CalendarRecurrenceRule::addMonth()
	 * @access private
	 * @var array
	 */
	private $_months = array();

	/**
	 * Days of the month
	 *
	 * This property stores the days of the month that match with the recurrence
	 * rule.
	 *
	 * @see M_CalendarRecurrenceRule::addDayOfMonth()
	 * @access private
	 * @var array
	 */
	private $_mdays = array();

	/**
	 * Hours
	 *
	 * This property stores the hours that match with the recurrence rule
	 *
	 * @see M_CalendarRecurrenceRule::addHour()
	 * @access private
	 * @var array
	 */
	private $_hours = array();

	/**
	 * Minutes
	 *
	 * This property stores the minutes that match with the recurrence rule
	 *
	 * @see M_CalendarRecurrenceRule::addMinute()
	 * @access private
	 * @var array
	 */
	private $_minutes = array();

	/**
	 * Seconds
	 *
	 * This property stores the seconds that match with the recurrence rule
	 *
	 * @see M_CalendarRecurrenceRule::addSecond()
	 * @access private
	 * @var array
	 */
	private $_seconds = array();

	/* -- CONSTRUCTORS -- */

	/**
	 * Construct with ICS value string
	 *
	 * 
	 */
	public static function constructWithStringFromCalendarIcs($string) {
		// If the string still carries the RRULE: prefix
		if(! strncmp($string, 'RRULE:', 6)) {
			// Then, we remove the prefix:
			$string = substr($string, 6);
		}
		
		// We construct a new recurrence object
		$r = new self;

		// In order to separate all of the rules in the provided string, we
		// explode the string by the separator: the semicolon symbol. Then, we
		// loop through each of the rules:
		foreach(explode(';', (string) $string) as $part) {
			// The current rule contains both a name and a value, separated by
			// an "equals" symbol. We separate these from each other
			$elm = explode('=', $part);

			// We must have an array with 2 elements: a name, and a value. If the
			// array does not contain exactly 2 elements:
			if(count($elm) != 2) {
				// Throw an exception to inform about the error
				throw new M_CalendarException(sprintf(
					'Cannot extract recurrence rule from part %s in %s',
					$part,
					$string
				));
			}

			// The value needs to be passed to a setter of this class. Which setter,
			// that depends on the name of the value:
			switch($elm[0]) {
				// Frequency
				case 'FREQ':
					$r->setFrequency($elm[1]);
					break;

				// (Repeat) Count
				case 'COUNT':
					$r->setCount((int) $elm[1]);
					break;

				// Interval
				case 'INTERVAL':
					$r->setInterval($elm[1]);
					break;

				// End date
				case 'UNTIL':
					$r->setEndDate(M_CalendarIcsSourceHelper::getDateFromString($elm[1]));
					break;

				// Year days
				case 'BYYEARDAY':
					// For each of the year days in the value:
					foreach(explode(',', $elm[1]) as $num) {
						// Add the year day:
						$r->addDayOfYear((int) $num);
					}

					// Done with year days
					break;

				// Week numbers
				case 'BYWEEKNO':
					// For each of the week numbers in the value:
					foreach(explode(',', $elm[1]) as $num) {
						// Add the week number
						$r->addWeeknumber((int) $num);
					}

					// Done with week numbers
					break;

				// Days of the week
				case 'BYDAY':
					// For each of the week days in the value:
					foreach(explode(',', $elm[1]) as $num) {
						// Has an index number been provided for the week day?
						$match = array();
						if(preg_match('/^([-0-9]+)([a-z]{2})$/i', $num, $match)) {
							// If so, we add the week day with index number:
							$r->addWeekday((int) $match[1], $match[2]);
						}
						// If no index number is available
						else {
							// Then, add the weekday without index number
							$r->addWeekday($num);
						}
					}

					// Done with week days
					break;

				// Months
				case 'BYMONTH':
					// For each of the months in the value:
					foreach(explode(',', $elm[1]) as $num) {
						// Add the month
						$r->addMonth((int) $num);
					}

					// Done with months
					break;

				// Days of the month
				case 'BYMONTHDAY':
					// For each of the months in the value:
					foreach(explode(',', $elm[1]) as $num) {
						// Add the month
						$r->addDayOfMonth((int) $num);
					}

					// Done with days of month
					break;

				// Hours
				case 'BYHOUR':
					// For each of the hours in the value:
					foreach(explode(',', $elm[1]) as $num) {
						// Add the month
						$r->addHour((int) $num);
					}

					// Done with hours
					break;

				// Minutes
				case 'BYMINUTE':
					// For each of the minutes in the value:
					foreach(explode(',', $elm[1]) as $num) {
						// Add the minute
						$r->addMinute((int) $num);
					}

					// Done with minutes
					break;

				// The day that starts a week
				case 'WKST':
					$r->setStartDayOfWeek($elm[1]);
					break;
			}
		}

		// Return the object:
		return $r;
	}

	/* -- SETTERS -- */

	/**
	 * Set Frequency
	 *
	 * This method can be used to set the frequency of repetition. Possible
	 * frequencies are:
	 *
	 * - {@link M_CalendarRecurrenceRule::FREQ_MINUTELY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_HOURLY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_DAILY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_WEEKLY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_MONTHLY}
	 * - {@link M_CalendarRecurrenceRule::FREQ_YEARLY}
	 *
	 * @access public
	 * @param string $frequency
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFrequency($frequency) {
		// Make sure that the frequency is known/recognized:
		if(! in_array($frequency, array(self::FREQ_MINUTELY, self::FREQ_HOURLY, self::FREQ_DAILY, self::FREQ_WEEKLY, self::FREQ_MONTHLY, self::FREQ_YEARLY))) {
			// If not, throw an exception
			throw new M_CalendarException(sprintf(
				'Unrecognized frequency %s',
				$frequency
			));
		}

		// Set the frequency
		$this->_frequency = $frequency;

		// Return myself
		return $this;
	}

	/**
	 * Set (Repeat) Count
	 *
	 * This method can be used to set the number of times that the item
	 * should be repeated
	 *
	 * @access public
	 * @param integer $count
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCount($count) {
		// Set the count
		$this->_count = (int) $count;

		// Return myself
		return $this;
	}

	/**
	 * Set (Repeat) Count
	 *
	 * ... Alias for {@link M_CalendarRecurrenceRule::setCount()}
	 *
	 * @access public
	 * @param integer $count
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function setRepeatCount($count) {
		// Set the count:
		return $this->setCount($count);
	}

	/**
	 * Set Interval
	 *
	 * This method can be used to set the interval of the repetition: "Every
	 * Nth". You could for example state that you want to repeat the item every
	 * 2 days, by using the following example code:
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_DAILY);
	 *    $rule->setInterval(2);
	 * </code>
	 *
	 * Or, consider the following example where we state that we want to repeat
	 * the item every 3 months:
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_MONTHLY);
	 *    $rule->setInterval(3);
	 * </code>
	 *
	 * @access public
	 * @param integer $interval
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function setInterval($interval) {
		// Set the interval
		$this->_interval = (int) $interval;

		// Return myself
		return $this;
	}

	/**
	 * Set End Date
	 *
	 * This method can be used to define a date at which the recurrence should
	 * end. The item will be repeated until this date is reached.
	 *
	 * @access public
	 * @param M_Date $date
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function setEndDate(M_Date $date) {
		// Set the end date:
		$this->_endDate = $date;

		// Return myself
		return $this;
	}

	/**
	 * Set the day that starts a week (WKST)
	 *
	 * This method allows you to define the day that starts a week. This is important
	 * to calculate repetition in the recurrence rule. An example where the days
	 * generated makes a difference because of WKST
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_WEEKLY);
	 *    $rule->setInterval(2);
	 *    $rule->setCount(4);
	 *    $rule->addWeekday(M_CalendarRecurrenceRule::TUESDAY);
	 *    $rule->addWeekday(M_CalendarRecurrenceRule::SUNDAY);
	 *    $rule->setStartDayOfWeek(M_CalendarRecurrenceRule::MONDAY);
	 * </code>
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_WEEKLY);
	 *    $rule->setInterval(2);
	 *    $rule->setCount(4);
	 *    $rule->addWeekday(M_CalendarRecurrenceRule::TUESDAY);
	 *    $rule->addWeekday(M_CalendarRecurrenceRule::SUNDAY);
	 *    $rule->setStartDayOfWeek(M_CalendarRecurrenceRule::SUNDAY);
	 * </code>
	 *
	 * @access public
	 * @param string $weekday
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function setStartDayOfWeek($weekday) {
		// Set the day that starts a week
		$this->_weekstart = (string) $weekday;

		// Return myself
		return $this;
	}

	/**
	 * Set day of year
	 *
	 * This method can be used to add a day of the year. By using this method,
	 * you could for example state that you want to repeat an item every 100th
	 * day of the year:
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_YEARLY);
	 *    $rule->addDayOfYear(100);
	 * </code>
	 *
	 * @access public
	 * @param integer $number
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function addDayOfYear($number) {
		// Add the day of the year:
		$this->_ydays[] = (int) $number;

		// Return myself
		return $this;
	}

	/**
	 * Add week number
	 *
	 * This method can be used to add a week number to the recurrence. You could,
	 * for example, state that you want to repeat an item every 20th week of the
	 * year:
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_YEARLY);
	 *    $rule->addWeeknumber(20);
	 * </code>
	 *
	 * @access public
	 * @param integer $weeknumber
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function addWeeknumber($weeknumber) {
		// Add the week number
		$this->_weeknumbers[] = (int) $weeknumber;

		// Return myself;
		return $this;
	}

	/**
	 * Add weekday
	 *
	 * This method can be used to add a week number to the recurrence. You could,
	 * for example, state that you want to repeat an item every wednesday
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_WEEKLY);
	 *    $rule->addWeekday(M_CalendarRecurrenceRule::WEDNESDAY);
	 * </code>
	 *
	 * Note that you can also provide with the index of the weekday, relative
	 * to the month. For example, say you want to repeat an item every first
	 * friday of the month. You can accomplish this by constructing a rule as
	 * following:
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_WEEKLY);
	 *    $rule->addWeekday(M_CalendarRecurrenceRule::FRIDAY, 1);
	 * </code>
	 *
	 * You can also use negative index numbers in order to describe more
	 * flexible rules. A negative index starts the other way around: at the end
	 * of the month. Let's say you want to repeat an item every last sunday of
	 * the month:
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_WEEKLY);
	 *    $rule->addWeekday(M_CalendarRecurrenceRule::SUNDAY, -1);
	 * </code>
	 *
	 * @access public
	 * @param string $weekday
	 * @param integer $index
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function addWeekday($weekday, $index = NULL) {
		// Add the week day
		$this->_wdays[] = array((string) $weekday, $index);

		// Return myself;
		return $this;
	}

	/**
	 * Add month
	 *
	 * This mehod can be used to add a month to the recurrence. You could, for
	 * example, state that you want to repeat an item every February of each year.
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_YEARLY);
	 *    $rule->addMonth(M_CalendarRecurrenceRule::FEBRUARY);
	 * </code>
	 *
	 * @access public
	 * @param integer $index
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function addMonth($index) {
		// Add the month
		$this->_months[] = (int) $index;
		
		// Return myself
		return $this;
	}

	/**
	 * Add day of month
	 *
	 * This method can be used to add a day of the month. By using this method,
	 * you could for example state that you want to repeat an item every 15th
	 * and 16th of each month
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_MONTHLY);
	 *    $rule->addDayOfMonth(15);
	 *    $rule->addDayOfMonth(16);
	 * </code>
	 *
	 * @access public
	 * @param integer $number
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function addDayOfMonth($number) {
		// Add the day of the month:
		$this->_mdays[] = (int) $number;

		// Return myself
		return $this;
	}

	/**
	 * Add hour
	 *
	 * This method can be used to add an hour. By using this method, you could
	 * for example state that you want to repeat an item every day at 7AM:
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_DAILY);
	 *    $rule->addHour(7);
	 * </code>
	 *
	 * @access public
	 * @param integer $hour
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function addHour($hour) {
		// Add the hour
		$this->_hours[] = (int) $hour;

		// Return myself
		return $this;
	}

	/**
	 * Add minute
	 *
	 * This method can be used to add a minute. By using this method, you could
	 * for example state that you want to repeat an item every 20 minutes:
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_MINUTELY);
	 *    $rule->addHour(20);
	 * </code>
	 *
	 * Or you may want to repeat an item each day, at a given time. In that case,
	 * you would want to write something like this. This example repeats an item
	 * every day, at 7:20AM
	 *
	 * <code>
	 *    $rule = new M_CalendarRecurrenceRule();
	 *    $rule->setFrequency(M_CalendarRecurrenceRule::FREQ_DAILY);
	 *    $rule->addHour(7);
	 *    $rule->addMinute(20);
	 * </code>
	 *
	 * @access public
	 * @param integer $minute
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function addMinute($minute) {
		// Add the minute
		$this->_minutes[] = (int) $minute;

		// Return myself
		return $this;
	}

	/**
	 * Add second
	 *
	 * This method can be used to add a second to the recurrence rule.
	 *
	 * @see M_CalendarRecurrenceRule::addHour()
	 * @see M_CalendarRecurrenceRule::addMinute()
	 * @access public
	 * @param integer $second
	 * @return M_CalendarRecurrenceRule $rule
	 *		Returns itself, for a fluent programming interface
	 */
	public function addSecond($second) {
		// Add the second
		$this->_seconds[] = (int) $second;

		// Return myself
		return $this;
	}

	/* -- GETTERS -- */

	/**
	 * Get frequency
	 *
	 * @see M_CalendarRecurrenceRule::setFrequency()
	 * @access public
	 * @return string
	 */
	public function getFrequency() {
		return $this->_frequency;
	}

	/**
	 * Get (Repeat) Count
	 *
	 * @see M_CalendarRecurrenceRule::setCount()
	 * @access public
	 * @return integer
	 */
	public function getCount() {
		return $this->_count;
	}

	/**
	 * Get (Repeat) Count
	 *
	 * @see M_CalendarRecurrenceRule::setRepeatCount()
	 * @access public
	 * @return integer
	 */
	public function getRepeatCount() {
		return $this->_count;
	}

	/**
	 * Get Interval
	 *
	 * @see M_CalendarRecurrenceRule::setInterval()
	 * @access public
	 * @return integer
	 */
	public function getInterval() {
		return $this->_interval;
	}
	
	/**
	 * Get End Date
	 *
	 * @see M_CalendarRecurrenceRule::setEndDate()
	 * @access public
	 * @return M_Date
	 */
	public function getEndDate() {
		return $this->_endDate;
	}

	/**
	 * Get the day that starts a week
	 *
	 * @see M_CalendarRecurrenceRule::setStartDayOfWeek()
	 * @access public
	 * @return string
	 */
	public function getStartDayOfWeek() {
		return $this->_weekstart;
	}

	/**
	 * Get days of year
	 *
	 * @see M_CalendarRecurrenceRule::addDayOfYear()
	 * @access public
	 * @return array
	 */
	public function getDaysOfYear() {
		return $this->_weekstart;
	}

	/**
	 * Get week numbers
	 *
	 * @see M_CalendarRecurrenceRule::addWeeknumber()
	 * @access public
	 * @return array
	 */
	public function getWeeknumbers() {
		return $this->_weeknumbers;
	}

	/**
	 * Get week days
	 *
	 * @see M_CalendarRecurrenceRule::addWeekday()
	 * @access public
	 * @return array
	 */
	public function getWeekdays() {
		return $this->_wdays;
	}

	/**
	 * Get months
	 *
	 * @see M_CalendarRecurrenceRule::addMonth()
	 * @access public
	 * @return array
	 */
	public function getMonths() {
		return $this->_months;
	}

	/**
	 * Get days of month
	 *
	 * @see M_CalendarRecurrenceRule::addDayOfMonth()
	 * @access public
	 * @return array
	 */
	public function getDaysOfMonth() {
		return $this->_mdays;
	}

	/**
	 * Get hours
	 *
	 * @see M_CalendarRecurrenceRule::addHour()
	 * @access public
	 * @return array
	 */
	public function getHours() {
		return $this->_hours;
	}

	/**
	 * Get minutes
	 *
	 * @see M_CalendarRecurrenceRule::addMinute()
	 * @access public
	 * @return array
	 */
	public function getMinutes() {
		return $this->_minutes;
	}

	/**
	 * Get (matching) dates
	 *
	 * @access public
	 * @param M_Date $startDate
	 * @return M_ArrayIterator $dates
	 *		The collection of {@link M_Date} instances
	 */
	public function getMatchingDates(M_Date $startDate, M_Date $endDate) {
		/*
		 * FREQ=DAILY;BYMONTH=1;BYHOUR=15;BYMINUTE=20,35
		 */
		// Output array:
		$out = array();

		// We start by expanding the collection, based on the frequency that has
		// been defined in the recurrence rule. For example, if the frequency tells
		// us to repeat daily, we generate a new date for each day since the
		// provided start date. In other words, we calculate new dates by adding
		// date parts (years, months, days, etc...) to the given start date. In
		// the following switch-case block, we define the date part:
		switch($this->getFrequency()) {
			// If repeating on a YEARLY basis:
			case self::FREQ_YEARLY:
				$datePart = M_Date::YEAR;
				break;

			// If repeating MONTHLY
			case self::FREQ_MONTHLY:
				$datePart = M_Date::MONTH;
				break;

			// If repeating WEEKLY
			case self::FREQ_WEEKLY:
				$datePart = M_Date::WEEK;
				break;

			// If repeating DAILY
			case self::FREQ_DAILY:
				$datePart = M_Date::DAY;
				break;

			// If repeating HOURLY
			case self::FREQ_HOURLY:
				$datePart = M_Date::HOUR;
				break;

			// If repeating MINUTELY
			case self::FREQ_MINUTELY:
				$datePart = M_Date::MINUTE;
				break;
		}

		// We'll start generating dates from the start date. In order to change
		// properties of the start date, we clone the object into a temporary
		// working variable, called $date
		$date = clone($startDate);

		/* @var $date M_Date */
		// While the date is before the provided end date:
		do {
			// Add the date part. Note that, by default, we add one unit of the
			// defined part to the date. This can be overwritten by providing an
			// interval to the recurrence rule.
			$date->addTime(($this->_interval ? $this->_interval : 1), $datePart);

			// If the date is still *before* the provided end date:
			if(! $date->isAfter($endDate)) {
				// Add the date to the output collection:
				$out[$date->getTimestamp()] = clone($date);
			}
			// If not
			else {
				// Then, stop adding to the start date:
				break;
			}
		} while(true);

		// Now, we have a collection that complies with the frequency of the
		// recurrence. However, the recurrence rule may modify that basic pattern
		// by specifying BYxxx rules (BYMONTH, BYDAY, ...).

		// BYxxx parts that specify a period of time which is the same or greater
		// than the frequency generally reduce or limit the number of dates
		// generated. For example, "FREQ=DAILY;BYMONTH=1" reduces the number of
		// recurrence instances from all days (if BYMONTH tag is not present) to
		// all days in January.

		// In turn, BYxxx parts that specify a period of time which is less than
		// the frequency generally expand the number of dates generated. For
		// example, "FREQ=YEARLY;BYMONTH=1,2" increases the number of days within
		// the yearly recurrence set from 1 (if BYMONTH tag is not present) to 2.

		// So, we will use two kinds of functions: those that RESTRICT the dates
		// to allowed values and those that EXPAND allowed values.

		// If the frequency is yearly
		if($this->getFrequency() == self::FREQ_YEARLY) {
			// Then, we'll expand the collection of dates for each month that
			// has been specified in the recurrence:
			$byxxx = array('BYMONTH', 'BYWEEKNO');

			$this->_expandMatchingDates($date, 'BYMONTH');
			$this->_expandMatchingDates($date, 'BYWEEKNO');
		}

		// Comments from PHP iCalendar
		// If multiple BYxxx rule parts are specified, then after evaluating the
		// specified FREQ and INTERVAL rule parts, the BYxxx rule parts are
		// applied to the current set of evaluated occurrences in the following
		// order: BYMONTH, BYWEEKNO, BYYEARDAY, BYMONTHDAY, BYDAY, BYHOUR,
		// BYMINUTE, BYSECOND and BYSETPOS; then COUNT and UNTIL are evaluated."
		return $out;
	}

		protected function _expandMatchingDates(M_Date $date, $byxxx, $maximum = NULL) {
		// Output array
		$out = array();

		// We create a map, that connects the BYXXX string to the date parts, and
		// to the internal properties of the recurrence object:
		$byxxxMap = array(
			'BYMONTH'    => array('month',  '_months'),
			'BYMONTHDAY' => array('day',    '_mdays'),
			'BYHOUR'     => array('hour',   '_hours'),
			'BYMINUTE'   => array('minute', '_minutes'),
			'BYSECOND'   => array('second', '_seconds')
		);

		// If the provided BYXXX is available in the map:
		if(array_key_exists($byxxx, $byxxxMap)) {
			// We will need to construct instances of the M_Date class, with new
			// values for the specified date part. First, we initiate an array
			// that contains all of the current values, which we fetch from the
			// provided M_Date object:
			$tmp = array(
				'year'   => $date->getYear(),
				'month'  => $date->getMonth(),
				'day'    => $date->getDay(),
				'hour'   => $date->getHour(),
				'minute' => $date->getMinutes(),
				'second' => $date->getSeconds()
			);

			// Get the internal property name:
			$property = $byxxxMap[$byxxx][1];

			// Then, we expand the collection of dates with the values that
			// have been specified in the map (depending on the type of BYXXX).
			// For each value:
			foreach($this->$property as $value) {
				// Update the array with date parts:
				$tmp[$byxxxMap[$byxxx][0]] = $value;

				// Then, construct a date with the updated array:
				$tmpDate = new M_Date($tmp);

				// Add the date to the output
				$out[$date->getTimestamp()] = $tmpDate;
			}
		}
		// If the provided BYXXX part is not available in the map, then it
		// probably needs a very specific approach. For example, an expansion by
		// week numbers cannot be done by merely updating a part of the date. We
		// need to do some additional stuff.
		// So, if the requested BYXXX is BY WEEK NUMBER:
		elseif($byxxx == 'BYWEEKNO') {
			// For each of the week numbers:
			foreach($this->_weeknumbers as $weekno) {
				// Get the difference between the week number of the current date,
				// and the week number of the current iteration:
				$diff = $weekno - $date->getWeekNumber();

				// If the difference is negative, then it means that the week
				// number of the recurrence rule is BEFORE the current date's one.
				// So, in order to add weeks to the current date, we calculate
				// the number of weeks remaining until next year. We base this
				// calculation on the number of weeks available in a year: 52
				if($diff < 0) {
					$diff += 52;
				}

				// Now, we add the difference to the current date:
				$tmpDate = clone($date);
				$tmpDate->addTime($diff, M_Date::WEEK);
			}
		}
		// An expansion by year days is also a special case
		elseif($byxxx == 'BYYEARDAY') {
			// For each of the year-days:
			foreach($this->_ydays as $yday) {
				// Get the difference between the year day of the current date,
				// and the year day of the current iteration:
				$diff = $yday - $date->getYearday();

				// If the difference is negative, then it means that the day of
				// the year in the recurrence rule is BEFORE the current date's
				// one. So, in order to add days to the current date, we calculate
				// the number of days remaining until next year. We base this
				// calculation on the number of days available in a year: 365,
				// or 366:
				if($diff < 0) {
					$diff += $date->isLeapYear() ? 366 : 365;
				}

				// Now, we add the difference to the current date:
				$tmpDate = clone($date);
				$tmpDate->addTime($diff, M_Date::DAY);
			}
		}
		// An expansion by week days:
		elseif($byxxx == 'BYDAY') {

		}

		// Return the output collection (array)
		return $out;
	}

	protected function _reduceMatchingDates() {

	}

	/**
	 * Render a string, for use as an RRULE value in webcal feeds or ics files
	 *
	 * @access public
	 * @return string
	 */
	public function toStringCalendarIcs() {
		// Output
		$tmp = array();

		// If a frequency has been defined for the recurrence rule:
		if($this->_frequency) {
			// Then, we add that frequency to the output
			$tmp['FREQ'] = $this->_frequency;
		}

		// If a (repeat) count has been set
		if($this->_count) {
			// Then, we add that to the output:
			$tmp['COUNT'] = $this->_count;
		}

		// If an interval has been defined
		if($this->_interval) {
			// Then, we add that to the output:
			$tmp['INTERVAL'] = $this->_interval;
		}

		// If an end date has been defined
		if($this->_endDate) {
			// Then, we add that to the output:
			$tmp['UNTIL'] = M_CalendarIcsSourceHelper::getDateString($this->_endDate);
		}

		// If days of the year have been selected
		if(count($this->_ydays) > 0) {
			// Then, we add that to the output:
			$tmp['BYYEARDAY'] = implode(',', $this->_ydays);
		}

		// If week numbers have been defined
		if(count($this->_weeknumbers) > 0) {
			// Then, we add that to the output:
			$tmp['BYWEEKNO'] = implode(',', $this->_weeknumbers);
		}

		// If week days have been defined
		if(count($this->_wdays) > 0) {
			// Then, we add that to the output:
			$tmp['BYDAY'] = '';

			// For each of the weekdays:
			foreach($this->_wdays as $wday) {
				// If the string of weekdays is no longer empty
				if(! empty($tmp['BYDAY'])) {
					// Then, we add a separator:
					$tmp['BYDAY'] .= ',';
				}

				// If an index has been defined for this specific weekday:
				if($wday[1] !== NULL) {
					// Then, add the index number:
					$tmp['BYDAY'] .= (int) $wday[1];
				}

				// Add to the output:
				$tmp['BYDAY'] .= $wday[0];
			}
		}

		// If months have been defined
		if(count($this->_months) > 0) {
			// Then, we add that to the output:
			$tmp['BYMONTH'] = implode(',', $this->_months);
		}

		// If days of month(s) have been defined
		if(count($this->_mdays) > 0) {
			// Then, we add that to the output:
			$tmp['BYMONTHDAY'] = implode(',', $this->_mdays);
		}

		// If hours have been defined
		if(count($this->_hours) > 0) {
			// Then, we add that to the output:
			$tmp['BYHOUR'] = implode(',', $this->_hours);
		}

		// If hours have been defined
		if(count($this->_minutes) > 0) {
			// Then, we add that to the output:
			$tmp['BYMINUTE'] = implode(',', $this->_minutes);
		}

		// If a week starter has been set
		if($this->_weekstart) {
			// Then, add the day that starts a week
			$tmp['WKST'] = $this->_weekstart;
		}

		// Initiate the final output string
		$out = '';

		// For each of the properties that have been added to the
		// temporary output
		foreach($tmp as $name => $value) {
			// If the output already has properties
			if(! empty($out)) {
				// Then, we add a separator:
				$out .= ';';
			}

			// We add the property, using the right syntax:
			$out .= $name . '=' . $value;
		}

		// Return the string
		return $out;
	}



	

	// http://www.kanzaki.com/docs/ical/rrule.html

	// FREQ=DAILY;UNTIL=19971224T000000Z
	// Every 2 days: FREQ=DAILY;INTERVAL=2
	// 5 occurrences, every 10 days: FREQ=DAILY;INTERVAL=10;COUNT=5
	// Every day in january: FREQ=YEARLY;BYMONTH=1;BYDAY=SU,MO,TU,WE,TH,FR,SA
	// RRULE:FREQ=WEEKLY;COUNT=10
	// Every 1st friday of the month: FREQ=MONTHLY;COUNT=10;BYDAY=1FR
	// Every 2 months, first and last sunday, for ten times: FREQ=MONTHLY;INTERVAL=2;COUNT=10;BYDAY=1SU,-1SU
	// Monthly on the third day of the month: FREQ=MONTHLY;BYMONTHDAY=3
	// Every second and fifteenth: FREQ=MONTHLY;COUNT=10;BYMONTHDAY=2,15
	// Monthly on the third to the last day of the month: FREQ=MONTHLY;BYMONTHDAY=3
	// In june and july: FREQ=YEARLY;COUNT=10;BYMONTH=6,7
	// Every first, 100th and 200th day: FREQYEARLY;INTERVAL=3;COUNT==10;BYYEARDAY=1,100,200
	// Every 20th Monday of the year: FREQ=YEARLY;BYDAY=20MO
	// Every week number 20: FREQ=YEARLY;BYWEEKNO=20;BYDAY=MO
	// The 3rd instance into the month of one of Tuesday, Wednesday or Thursday, for the next 3 months: FREQ=MONTHLY;COUNT=3;BYDAY=TU,WE,TH;BYSETPOS=3
	// The 2nd to last weekday of the month: FREQ=MONTHLY;BYDAY=MO,TU,WE,TH,FR;BYSETPOS=-2
	// Every 3 hours from 9:00 AM to 5:00 PM on a specific day: FREQ=HOURLY;INTERVAL=3;UNTIL=19970902T170000
	// Every 15 minutes for 6 occurrences: FREQ=MINUTELY;INTERVAL=15;COUNT=6
	// FREQ=DAILY;BYHOUR=9,10,11,12,13,14,15,16;BYMINUTE=0,20,40
	// Set starting week day, for recurrence: WKST=MO
}

//$rule = new M_CalendarRecurrenceRule();
//$rule->setFrequency(M_CalendarRecurrenceRule::FREQ_MONTHLY);
//$rule->setInterval(2);
//$rule->addDayOfMonth(1);
//$rule->toStringCalendarIcs();
//$event = new M_CalendarIcsEvent();
//$event->addRecurrenceRule($rule);
//
//$rule->getMatchingDates($startDate, $endDate);