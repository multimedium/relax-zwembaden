<?php
/**
 * M_Calendar class
 * 
 * The M_Calendar class is used to handle collections of Date objects.
 * For example, you can employ a M_Calendar object to visualize the
 * days in a given month + year
 * 
 * @package Core
 */
class M_Calendar {

	/* -- PROPERTIES -- */

	/**
	 * Internal Variable: Year
	 * 
	 * @access private
	 * @see M_Calendar::setYear()
	 * @see M_Calendar::getYear()
	 * @var integer
	 */
	private $_year;
	
	/**
	 * Internal Variable: Month
	 * 
	 * @access private
	 * @see M_Calendar::setMonth()
	 * @see M_Calendar::getMonth()
	 * @var integer
	 */
	private $_month;

	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * Note that the M_Calendar can be constructed without any parameter.
	 * If no month is provided to the calendar, it will be a year-wide
	 * calendar (including all months of that year). If no year is
	 * given to the constructor, it will default the year to the current
	 * year.
	 * 
	 * @access public
	 * @param integer $year
	 * 		The year of the calendar
	 * @param integer $month
	 * 		The month of the calendar
	 * @return M_Calendar
	 */
	public function __construct($year = NULL, $month = NULL) {
		$this->setYear(($year ? $year : date('Y')));
		if($month) {
			$this->setMonth($month);
		}
	}

	/**
	 * Construct current month + year
	 *
	 * Will construct a calendar with the current month and year. This is a constructor,
	 * so an instance of {@link M_Calendar} is returned as the result of the
	 * method.
	 *
	 * @static
	 * @access public
	 * @param integer $difference
	 * 		The difference number, e.g. 1 (next), 2, -3, -1 (previous)
	 * @return M_Calendar
	 */
	public static function constructWithCurrentMonthAndYear() {
		return new self(M_Date::getCurrentYear(), M_Date::getCurrentMonth());
	}

	/**
	 * Construct current month + year
	 *
	 * Will construct a calendar with a month before or after the current month.
	 * If not provided with a difference number, this method will use +0
	 * ("current month") by default
	 *
	 * Example 1, construct a calendar for next month
	 * <code>
	 *    $calendar = M_Calendar::constructWithMonthDifference(1);
	 * </code>
	 *
	 * Example 2, construct a calendar for three months from now
	 * <code>
	 *    $calendar = M_Calendar::constructWithMonthDifference(3);
	 * </code>
	 *
	 * Example 3, construct a calendar for previous month
	 * <code>
	 *    $calendar = M_Calendar::constructWithMonthDifference(-1);
	 * </code>
	 *
	 * @static
	 * @access public
	 * @param integer $difference
	 * 		The difference number, e.g. 1 (next), 2, -3, -1 (previous)
	 * @return M_Calendar
	 */
	public static function constructWithMonthDifference($difference) {
		// Apply the difference:
		list($year, $month) = self::_getMonthDifference(
			M_Date::getCurrentMonth(),
			M_Date::getCurrentYear(),
			$difference
		);

		// Then, construct the calendar:
		return new self($year, $month);
	}

	/* -- GETTERS -- */
	
	/**
	 * Get the year of the calendar
	 * 
	 * @access public
	 * @return integer
	 */
	public function getYear() {
		return $this->_year;
	}
	
	/**
	 * Get the month of the calendar
	 * 
	 * @access public
	 * @return integer
	 */
	public function getMonth() {
		return $this->_month;
	}
	
	/**
	 * Get a month before/after the calendar
	 * 
	 * Will return the requested month before or after the calendar. If not 
	 * provided with a difference number, this method will use +1 ("first month
	 * after the calendar") by default
	 * 
	 * <code>
	 *    $calendar = new M_Calendar(2009, 10);
	 *    list($year, $month) = $calendar->getMonthDiff(1);
	 * </code>
	 *
	 * @uses M_Calendar::_getMonthDifference()
	 * @access public
	 * @param integer $difference
	 * 		The difference number, e.g. 1 (next), 2, -3, -1 (previous)
	 * @return array $array
	 *		Returns an array with the new year at index 0, and the month at index 1
	 */
	public function getMonthDiff($difference) {
		return self::_getMonthDifference($this->_month, $this->_year, $difference);
	}
	
	/**
	 * Get the first day
	 * 
	 * This method will return the first day in the calendar (at 
	 * 00:00:00 hours). Note that this method will return an
	 * {@link M_Date} object.
	 * 
	 * If no specific month has been provided to the calendar,
	 * this method will return the first day of the year (1 Jan). 
	 * If, however, a month has been provided to the calendar, this 
	 * method will return the first day of that month in the 
	 * calendar's year.
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getFirstDay() {
		if($this->_month == NULL) {
			return new M_Date(array(
				'year'   => $this->_year,
				'month'  => 1,
				'day'    => 1,
				'hour'   => 0,
				'minute' => 0,
				'second' => 0,
			));
		}
		
		return new M_Date(array(
			'year'   => $this->_year,
			'month'  => $this->_month,
			'day'    => 1,
			'hour'   => 0,
			'minute' => 0,
			'second' => 0,
		));
	}
	
	/**
	 * Get the last day
	 * 
	 * This method will return the last day in the calendar (at 
	 * 23:59:59 hours). Note that this method will return an
	 * {@link M_Date} object.
	 * 
	 * If no specific month has been provided to the calendar,
	 * this method will return the last day of the year (31 Dec). 
	 * If, however, a month has been provided to the calendar, this 
	 * method will return the last day of that month in the 
	 * calendar's year.
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getLastDay() {
		if($this->_month == NULL) {
			return new M_Date(array(
				'year'   => $this->_year,
				'month'  => 12,
				'day'    => 31,
				'hour'   => 0,
				'minute' => 0,
				'second' => 0,
			));
		}
		
		return new M_Date(array(
			'year'   => $this->_year,
			'month'  => $this->_month,
			'day'    => $this->getNumberOfDays(),
			'hour'   => 0,
			'minute' => 0,
			'second' => 0,
		));
	}
	
	/**
	 * Get number of days
	 * 
	 * If the calendar has been set up for a specific month, this method
	 * will return the number of days in that month. If not, this method
	 * will return the number of days in the entire year of the calendar.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfDays() {
		if($this->_month) {
			return date('t', mktime(0, 0, 0, $this->_month, 1, $this->_year));
		} else {
			$date = new M_Date(array('year' => $this->_year));
			return $date->isLeapYear() ? 366 : 365;
		}
	}
	
	/**
	 * Get iterator on days in calendar
	 * 
	 * This method will return an {@link M_CalendarDaysIterator} 
	 * object, allowing the requesting code to loop through the
	 * days in the calendar. Again, as in {@link M_Calendar::getNumberOfDays()},
	 * this method will return an iterator on the days of ALL 
	 * months in the calendar's year, if no specific month has
	 * been provided to the calendar.
	 * 
	 * @access public
	 * @return M_CalendarDaysIterator
	 */
	public function getDaysIterator() {
		return new M_CalendarDaysIterator($this);
	}
	
	/**
	 * Check if date's in calendar
	 * 
	 * Check if a given date fits in this calendar. This method
	 * will check if the provided M_Date object forms part of the
	 * calendar.
	 * 
	 * @access public
	 * @param M_Date $date
	 * 		The date to be evaluated
	 * @return boolean
	 * 		Will return TRUE if the date falls in the calendar;
	 * 		FALSE if not.
	 */
	public function isDateInCalendar(M_Date $date) {
		// If the years match:
		if($date->year == $this->_year) {
			// If the calendar is of a specific month:
			if($this->_month) {
				// Check for a match in months too:
				if($date->month == $this->_month) {
					// If month and year matches, we return TRUE
					return TRUE;
				}
			}
			// If the calendar is not of a specific month
			else {
				// We return TRUE (because the year already matches)
				return TRUE;
			}
		}
		
		// If we're still here the year does not even match, so we simply 
		// return FALSE :)
		return FALSE;
	}

	/* -- SETTERS -- */

	/**
	 * Set the year of the calendar
	 *
	 * @access public
	 * @param integer $year
	 * 		The year of the calendar
	 * @return void
	 */
	public function setYear($year) {
		$this->_year = $year;
	}
	
	/**
	 * Set the month of the calendar
	 * 
	 * @access public
	 * @param integer $month
	 * 		The month of the calendar
	 * @return void
	 */
	public function setMonth($month) {
		$this->_month = $month;
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get new year and month, for given month difference
	 *
	 * @access protected
	 * @param integer $month
	 * @param integer $year
	 * @param integer $difference
	 * @return array $array
	 *		Returns an array with the new year at index 0, and the month at index 1
	 */
	protected static function _getMonthDifference($month, $year, $difference) {
		// Apply the difference:
		$month += (int) $difference;

		// While the month is greater than 12
		while($month > 12) {
			// Advance a year:
			$month -= 12;
			$year  += 1;
		}

		// While the month is lower than 1
		while($month < 1) {
			// Then, go back a year
			$month += 12;
			$year  -= 1;
		}

		// Return the new year and month
		return array($year, $month);
	}
}