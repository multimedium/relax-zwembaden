<?php
/**
 * M_HashBcrypt
 * 
 * The Bcrypt implementation of {@link MI_Hash}, which may be used
 * to encrypt a value using Bcrypt. Simply put, Bcrypt will use a very slow
 * mathematical algorithm (Blowfish) to encrypt a value, providing much greater
 * security than a fast algorithm like md5 or sha256. How fast or slow the
 * algorithm should be for your implementation can be set with a variable
 * work factor.
 * 
 * Implementation is pretty straightforward:
 * 
 * <code>
 *	// Create a new cryptographer
 *	$bcrypt = new M_HashBcrypt();
 * 
 *	// Set the input value
 *	$bcrypt->setInput('mypassword');
 * 
 *	// Set the salt that is to be used for encryption
 *	$bcrypt->setSalt('saltstring');
 * 
 *	// Set the work factor
 *	$bcrypt->setWorkFactor(13);
 * 
 *	// And generate the hash
 *	$hash = $bcrypt->getHash();
 * </code>
 * 
 * For additional documentation about Bcrypt and the Blowfish algorithm, please
 * refer to the following links:
 * @see http://en.wikipedia.org/wiki/Bcrypt
 * @see http://phpmaster.com/why-you-should-use-bcrypt-to-hash-stored-passwords/
 * @see http://www.nathandavison.com/posts/view/13/php-bcrypt-hash-a-password-with-a-logical-salt
 * @see http://chargen.matasano.com/chargen/2007/9/7/enough-with-the-rainbow-tables-what-you-need-to-know-about-s.html
 * @see http://stackoverflow.com/questions/2235158/sha1-vs-md5-vs-sha256-which-to-use-for-a-php-login
 * @see http://stackoverflow.com/questions/4795385/how-do-you-use-bcrypt-for-hashing-passwords-in-php
 * @author Tim
 * @package Core
 */
class M_HashBcrypt implements MI_Hash {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Input
	 * 
	 * Holds the input string that is to be encrypted by Bcrypt
	 * 
	 * @see M_HashBcrypt::setInput()
	 * @see M_HashBcrypt::getInput()
	 * @access private
	 * @var string
	 */
	private $_input;
	
	/**
	 * Salt
	 * 
	 * Holds the salt string we have to use for Bcrypt encryption
	 * 
	 * @see M_HashBcrypt::setSalt()
	 * @see M_HashBcrypt::getSalt()
	 * @access private
	 * @var string
	 */
	private $_salt;
	
	/**
	 * Work Factor
	 * 
	 * Holds the Bcrypt work factor
	 * 
	 * @see M_HashBcrypt::setWorkFactor()
	 * @see M_HashBcrypt::getWorkFactor()
	 * @access private
	 * @var int
	 */
	private $_workFactor;
	
	/* -- SETTERS -- */
	
	/**
	 * Set Input
	 * 
	 * Allows for setting the input string that is to be encrypted by Bcrypt.
	 * This may for example be a password, for example:
	 * 
	 * <code>
	 *	// Create a new Bcrypt cryptographer
	 *	$crypto = new M_HashBcrypt();
	 * 
	 *	// Set the password that is to be hashed
	 *	$crypto->setSalt('mypassword');
	 * </code>
	 * 
	 * @see M_HashBcrypt::getInput()
	 * @access public
	 * @param string $input
	 * @return M_HashBcrypt $crypto
	 *		Returns itself, for a fluent programming interface
	 */
	public function setInput($input) {
		// Set the property
		$this->_input = (string) $input;
		
		// Return self
		return $this;
	}
	
	/**
	 * Set Salt
	 * 
	 * Allows for setting the salt string we have to use for encryption. To be
	 * a valid salt for use with Bcrypt, it must match the following rules:
	 * - the salt must be at least 22 characters long
	 * - the salt may only contain the following characters: ./0-9A-Za-z
	 * 
	 * For example:
	 * 
	 * <code>
	 *	// Create a new Bcrypt cryptographer
	 *	$crypto = new M_HashBcrypt()
	 * 
	 *	// Set its salt
	 *	$crypto->setSalt('R.gJb2U2N.FmZ4hPp1y2CN');
	 * </code>
	 * 
	 * NOTE: the Blowfish algorithm used to create the hash only uses 22
	 * characters for its salt, so if you provide a salt that is longer than
	 * 22 characters, it will be shortened.
	 * 
	 * @see M_HashBcrypt::getSalt()
	 * @access public
	 * @param string $salt
	 * @return M_HashBcrypt $crypto
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSalt($salt) {
		// Cast to string first
		$salt = (string) $salt;
		
		// Make sure the provided salt is long enough and contains only
		// valid characters
		if(preg_match('/^[\.\/a-z0-9]{22,}$/i', $salt) == 0) {
			throw new M_Exception(sprintf(
				'Invalid salt provided in %s::%s(); The provided salt must ' .
				'be at least 22 characters long and only contain the ' .
				'following characters: ./0-9A-Za-z. Provided salt: %s',
				__CLASS__,
				__FUNCTION__,
				$salt
			));
		}
		
		// Set the property
		$this->_salt = $salt;
		
		// Return self
		return $this;
	}
	
	/**
	 * Set Work Factor
	 * 
	 * Allows for setting the Bcrypt work factor, which is an integer between
	 * 4 and 31. This work factor controls the amount of time Bcrypt needs
	 * to encrypt a value a single time. The higher the value, the longer
	 * the encryption will take.
	 * 
	 * The work factor is used by the Blowfish algorithm, which Bcrypt uses
	 * for encryption. Internally the algorithm will run 2^x iterations, in
	 * which x is the provided work factor. This implies of course that the
	 * amount of time needed for encryption increases exponentially, meaning
	 * you should be careful in choosing the factor, and always make a choice
	 * of security vs performance.
	 * 
	 * As of currently, September 24th 2012, the timings of a single Bcrypt
	 * encryption are as follows:
	 * 
	 * [workfactor] [timing in seconds]
	 * - 04 [0.001187086105]
	 * - 05 [0.002424001693]
	 * - 06 [0.004951000213]
	 * - 07 [0.008852958679]
	 * - 08 [0.019272089004]
	 * - 09 [0.035010099411]
	 * - 10 [0.070755958557]
	 * - 11 [0.137512922286]
	 * - 12 [0.282757997512]
	 * - 13 [0.572612047195]
	 * - 14 [1.129796028137]
	 * - 15 [2.213904857635]
	 * - 16 [4.488440990448]
	 * - ...
	 * 
	 * It is encouraged to pick a factor of at least 11 or 12, resulting in a
	 * decent amount of time needed for encryption. For the user of your
	 * application the added time makes little difference, but for a hacker
	 * it will severely cripple his hacking attempt, especially when using
	 * a brute force attack.
	 * 
	 * One advantage of the Blowfish work factor is that it scales with Moore's
	 * law. Moore's law states that the maximum number of transistors on an
	 * integrated circuit doubles approximately every 2 years, meaning that
	 * a computer's calculation speed doubles as well. To counter a possibly
	 * faster hacking attempt, the work factor can simply be increased by 1
	 * every 2 years, for every new implementation.
	 * 
	 * IMPORTANT NOTE: as the work factor changes, so does the output hash
	 * generated by {@link M_HashBcrypt::getHash()}, as the Blowfish
	 * algorithm will run more/less iterations. As a result, you should pick
	 * a work factor for your implementation and stick with it.
	 * 
	 * For more information on the Blowfish algorithm and work factor, read
	 * the docs on the following links:
	 * - {@link http://en.wikipedia.org/wiki/Bcrypt}
	 * - {@link http://en.wikipedia.org/wiki/Blowfish_%28cipher%29}
	 * 
	 * @see M_HashBcrypt::getWorkFactor()
	 * @access public
	 * @param int $workFactor
	 *		The work factor that is to be used, which is an integer 4-31
	 * @return M_HashBcrypt $crypto
	 *		Returns itself, for a fluent programming interface
	 */
	public function setWorkFactor($workFactor) {
		// Cast the received work factor to an integer
		$workFactor = (int) $workFactor;
		
		// Make sure it is within the accepted range
		if($workFactor < 4 || $workFactor > 31) {
			throw new M_Exception(sprintf(
				'Invalid work factor %s given in %s::%s(); The Bcrypt ' .
				'work factor must be within the range 4-31',
				$workFactor,
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Set the work factor
		$this->_workFactor = (int) $workFactor;
		
		// Return myself
		return $this;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get Input
	 * 
	 * Will return the set input string that is to be encrypted with Bcrypt
	 * 
	 * @see M_HashBcrypt::setInput()
	 * @access public
	 * @return string
	 */
	public function getInput() {
		return $this->_input;
	}
	
	/**
	 * Get Salt
	 * 
	 * Will return the set salt string we have to use for Bcrypt encrption
	 * 
	 * @see M_HashBcrypt::setSalt()
	 * @access public
	 * @return string
	 */
	public function getSalt() {
		return $this->_salt;
	}
	
	/**
	 * Get Work Factor
	 * 
	 * Will return the set Bcrypt work factor
	 * 
	 * @see M_HashBcrypt::setWorkFactor()
	 * @access public
	 * @return int
	 */
	public function getWorkFactor() {
		return $this->_workFactor;
	}
	
	/**
	 * Get Hash
	 * 
	 * Will encrypt the provided input string, returning the output hash
	 * that can for example be used for password comparison or storage. The
	 * returned hash is 32 characters long and contains only characters from
	 * the following range: ./0-9A-Za-z
	 * 
	 * NOTE: before the hash can be generated, the following variables must
	 * be defined:
	 * - the input string: {@link M_HashBcrypt::setInput()}
	 * - the salt string: {@link M_HashBcrypt::setSalt()}
	 * - the work factor: {@link M_HashBcrypt::setWorkFactor()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getHash() {
		// First of all, make sure that the Blowfish algorithm is supported
		// by the version of php that is currently running on the server.
		// If it isn't:
		if(! (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH)) {
			// Throw an exception, to inform the user
			throw new M_Exception(sprintf(
				'Cannot create a Bcrypt hash in %s::%s(); the blowfish ' .
				'algorithm is not supported on this server! Current php ' .
				'version: %s',
				__CLASS__,
				__FUNCTION__,
				PHP_VERSION
			));
		}
		
		// Now make sure that all required variables are set to be able to
		// create the hash:
		if(is_null($this->getInput())) {
			throw new M_Exception(sprintf(
				'Cannot create hash in %s::%s(); please provide the input ' .
				'string to %s::%s()',
				__CLASS__,
				__FUNCTION__,
				__CLASS__,
				'setInput'
			));
		}
		if(is_null($this->getSalt())) {
			throw new M_Exception(sprintf(
				'Cannot create hash in %s::%s(); please provide the salt ' .
				'string to %s::%s()',
				__CLASS__,
				__FUNCTION__,
				__CLASS__,
				'setSalt'
			));
		}
		if(is_null($this->getWorkFactor())) {
			throw new M_Exception(sprintf(
				'Cannot create hash in %s::%s(); please provide the work ' .
				'factor to %s::%s()',
				__CLASS__,
				__FUNCTION__,
				__CLASS__,
				'setWorkFactor'
			));
		}
		
		// Parse the work factor: if it is smaller than 10, add a leading zero
		$workFactor = $this->getWorkFactor();
		if($workFactor < 10) {
			$workFactor = '0' . $workFactor;
		}
		
		// Parse the salt we have to use to create a Bcrypt
		$salt = '$2a$' . $workFactor . '$' . substr($this->getSalt(), 0, 22) . '$';
		
		// Create the Bcrypt hash
		$hash = crypt($this->getInput(), $salt);
		
		// The generated hash looks for example as follows:
		// $2a$13$R.gJb2U2N.FmZ4hPp1y2C.KljEdaFn5zQAAhcR275T5AbV9L0Qyy.
		// 
		// We don't need the hashing information (work factor, salt etc),
		// only the hash itself. Therefore, we skip the first 28 characters,
		// returning only the remaining 32:
		return substr($hash, 28);
	}
}