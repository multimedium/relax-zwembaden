<?php
class M_CodeParameter extends M_CodeVariable {
	protected $_optional;
	
	public function setIsOptional($flag) {
		$this->_optional = (bool) $flag;
	}
	
	public function setDefaultValue($value) {
		$this->setValue($value);
		$this->setIsOptional(TRUE);
	}
	
	public function isOptional() {
		return ($this->_optional === TRUE);
	}
	
	public function getDefaultValue() {
		return $this->getValue();
	}
}