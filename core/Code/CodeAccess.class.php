<?php
class M_CodeAccess {
	const TYPE_PUBLIC = 'public';
	const TYPE_PRIVATE = 'private';
	const TYPE_PROTECTED = 'protected';
}