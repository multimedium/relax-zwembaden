<?php
/**
 * @
 *
 */
class M_CodeComment {
	
	/* -- Comment properties -- */
	
	protected $_title = '';
	protected $_text = '';
	
	/* -- Comment tags (PHP Documentor) -- */
	
	protected $_package;
	protected $_subpackage;
	protected $_abstract;
	protected $_static;
	protected $_access;
	protected $_var;
	protected $_author;
	protected $_copyright;
	protected $_license;
	protected $_see = array();
	protected $_uses = array();
	protected $_params = array();
	
	/**
	 * Return value
	 *
	 * @access protected
	 * @var M_CodeVariable
	 */
	protected $_return;
	protected $_todo;
	
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	public function setText($text) {
		$this->_text = (string) $text;
	}
	
	public function setPackage($package) {
		$this->_package = (string) $package;
	}
	
	public function setSubpackage($package) {
		$this->_subpackage = (string) $package;
	}
	
	public function setIsAbstract($flag) {
		$this->_abstract = (bool) $flag;
	}
	
	public function setIsStatic($flag) {
		$this->_static = (bool) $flag;
	}
	
	public function setAccess($access) {
		$this->_access = (string) $access;
	}
	
	public function setDataType($dataType) {
		$this->_var = (string) $dataType;
	}
	
	public function setAuthor($author) {
		$this->_author = (string) $author;
	}
	
	public function setCopyright($copyright) {
		$this->_copyright = (string) $copyright;
	}
	
	public function setLicense($license) {
		$this->_license = (string) $license;
	}
	
	public function addReference($reference) {
		$this->_see[] = (string) $reference;
	}
	
	public function clearReferences() {
		$this->_see = array();
	}
	
	public function addUse($use) {
		$this->_uses[] = (string) $use;
	}
	
	public function clearUses() {
		$this->_uses = array();
	}
	
	public function addParameter(M_CodeParameter $parameter, $description = NULL) {
		$this->_params[] = array($parameter, $description);
	}
	
	public function clearParameters() {
		$this->_params = array();
	}
	
	public function setReturn(M_CodeVariable $return, $description = NULL) {
		$this->_return = array($return, $description);
	}
	
	public function setReturnVoid() {
		$this->_return = FALSE;
	}
	
	public function clearReturn() {
		$this->_return = NULL;
	}
	
	public function setTodo($todo) {
		$this->_todo = (string) $todo;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function getText() {
		return $this->_text;
	}
	
	public function getPackage() {
		return $this->_package;
	}
	
	public function getSubpackage() {
		return $this->_subpackage;
	}
	
	public function isAbstract() {
		return ($this->_abstract === TRUE);
	}
	
	public function isStatic() {
		return ($this->_static === TRUE);
	}
	
	public function getAccess() {
		return $this->_access;
	}
	
	public function getDataType() {
		return $this->_var;
	}
	
	public function getAuthor() {
		return $this->_author;
	}
	
	public function getCopyright() {
		return $this->_copyright;
	}
	
	public function getLicense() {
		return $this->_license;
	}
	
	public function getReferences() {
		return new ArrayIterator($this->_see);
	}
	
	public function getUses() {
		return new ArrayIterator($this->_uses);
	}
	
	public function getParameters() {
		$out = array();
		foreach($this->_params as $param) {
			$out[] = $param[0];
		}
		return new ArrayIterator($out);
	}
	
	public function getReturn() {
		if(! $this->_return) {
			return NULL;
		} else {
			return $this->_return[0];
		}
	}
	
	public function getTodo() {
		return $this->_todo;
	}
	
	public function toString($numberOfTabsForIndentation = 1) {
		// We get the new line characters (including tabs), to start new lines
		// in the comment block:
		$nl = M_CodeHelper::getNewLineWithTab(1, $numberOfTabsForIndentation);
		
		// We calculate the width of the comment block (depends on the number of
		// tabs that is being used for indentation.
		$wrapWidth = 70 - ($numberOfTabsForIndentation * 5);
		
		// Start the comments block:
		$out  = M_CodeHelper::getTab($numberOfTabsForIndentation) . '/**';
		
		// Add the title to the comments block
		if($this->_title) {
			foreach(self::_getLinesFromText($this->_title, $wrapWidth) as $line) {
				$out .= $nl . ' * ' . $line;
			}
			$out .= $nl . ' * ';
		}
		
		// Add the more elaborative description to the comments block
		if($this->_text) {
			foreach(self::_getLinesFromText($this->_text, $wrapWidth) as $line) {
				$out .= $nl . ' * ' . $line;
			}
			$out .= $nl . ' * ';
		}

		// Now, we start adding the PHP Documentor tags:
		if($this->_todo) {
			$out .= $nl . ' * @todo ' . $this->_todo;
		}
		
		if($this->isAbstract()) {
			$out .= $nl . ' * @abstract';
		}
		
		if($this->isStatic()) {
			$out .= $nl . ' * @static';
		}
		
		if($this->_access) {
			$out .= $nl . ' * @access ' . $this->_access;
		}
		
		if($this->_var) {
			$out .= $nl . ' * @var ' . $this->_var;
		}
		
		if($this->_author) {
			$out .= $nl . ' * @author ' . $this->_author;
		}
		
		if($this->_copyright) {
			$out .= $nl . ' * @copyright ' . $this->_copyright;
		}
		
		if($this->_license) {
			$out .= $nl . ' * @license ' . $this->_license;
		}
		
		if($this->_package) {
			$out .= $nl . ' * @package ' . $this->_package;
		}
		
		if($this->_subpackage) {
			$out .= $nl . ' * @subpackage ' . $this->_subpackage;
		}
		
		foreach($this->getReferences() as $reference) {
			$out .= $nl . ' * @see ' . $reference;
		}
		
		foreach($this->getUses() as $reference) {
			$out .= $nl . ' * @uses ' . $reference;
		}
		
		foreach($this->_params as $parameter) {
			$out .= $nl . ' * @param ' . $parameter[0]->getDataType() . ' $' . M_Helper::ltrimCharlist($parameter[0]->getName(), '$');
			
			$tmp  = $parameter[1];
			if($tmp) {
				foreach(self::_getLinesFromText($tmp, ($wrapWidth - 5)) as $line) {
					$out .= $nl . ' * ' . M_CodeHelper::getTab() . $line;
				}
			}
		}
		
		if($this->_return) {
			$out .= $nl . ' * @return ' . $this->_return[0]->getDataType() . ' $' . M_Helper::ltrimCharlist($this->_return[0]->getName(), '$');
			
			$tmp  = $this->_return[1];
			if($tmp) {
				foreach(self::_getLinesFromText($tmp, ($wrapWidth - 5)) as $line) {
					$out .= $nl . ' * ' . M_CodeHelper::getTab() . $line;
				}
			}
		}
		// If no return has been specified:
		else {
			// Return VOID? (for comments on functions)
			if($this->_return === FALSE) {
				$out .= $nl . ' * @return void';
			}
		}
		
		// Return final source code render
		$out .= $nl . ' */';
		return $out;
	}
	
	protected function _getLinesFromText($text, $width) {
		$text = wordwrap($text, $width, "\n");
		$text = str_replace("\r", '', $text);
		return explode("\n", $text);
	}
}