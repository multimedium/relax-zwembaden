<?php
class M_CodeHelper {
	public static function getNewLine($number = 1) {
		return str_repeat("\n", $number);
	}
	
	public static function getTab($number = 1) {
		return str_repeat("\t", $number);
	}
	
	public static function getNewLineWithTab($numberOfLines = 1, $numberOfTabs = 1) {
		return str_repeat("\n" . str_repeat("\t", $numberOfTabs), $numberOfLines);
	}
}