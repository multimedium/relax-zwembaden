<?php
class M_CodeVariable {
	const TYPE_STRING  = 'string';
	const TYPE_INTEGER = 'integer';
	const TYPE_FLOAT = 'float';
	const TYPE_ARRAY = 'array';
	const TYPE_BOOLEAN = 'bool';
	
	protected $_comment;
	protected $_access;
	protected $_dataType;
	protected $_name;
	protected $_value;
	
	public function __construct($name, $type) {
		$this->setName($name);
		$this->setDataType($type);
	}
	
	public function setComment(M_CodeComment $comment) {
		$this->_comment = $comment;
	}
	
	public function setAccess($access) {
		$this->_access = $access;
	}
	
	public function setDataType($type) {
		$this->_dataType = $type;
	}
	
	public function setName($name) {
		$this->_name = $name;
	}
	
	public function setValue($value) {
		$this->_value = $value;
	}
	
	public function getComment() {
		return $this->_comment;
	}
	
	public function getAccess() {
		return $this->_access;
	}
	
	public function getDataType() {
		return $this->_dataType;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function getValue() {
		return $this->_value;
	}
}