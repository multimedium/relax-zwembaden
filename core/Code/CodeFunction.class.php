<?php
class M_CodeFunction {
	protected $_comment;
	protected $_access;
	protected $_static;
	protected $_abstract;
	protected $_name;
	protected $_body;
	protected $_params = array();
	
	public function __construct($name) {
		$this->setName($name);
	}
	
	public function setComment(M_CodeComment $comment) {
		$this->_comment = $comment;
	}
	
	public function setAccess($access) {
		$this->_access = $access;
	}
	
	public function setIsStatic($flag) {
		$this->_static = (bool) $flag;
	}
	
	public function setIsAbstract($flag) {
		$this->_abstract = (bool) $flag;
		if($this->_abstract) {
			$this->_body = NULL;
		}
	}
	
	public function setName($name) {
		$this->_name = (string) $name;
	}
	
	public function addParameter(M_CodeParameter $parameter) {
		$this->_params[] = $parameter;
	}
	
	public function clearParameters() {
		$this->_params = array();
	}
	
	public function setBody($body) {
		if($this->isAbstract()) {
			throw new M_CodeException(sprintf(
				'%s: Cannot assign body to abstract function %s()',
				__CLASS__,
				$this->getName()
			));
		}
		$this->_body = (string) $body;
	}
	
	/**
	 * Get comment
	 * 
	 * Will return with the {@link M_CodeComment} of the function. Note that,
	 * if no comments have been provided via {@link M_CodeFunction::setComments()}
	 * previously, a defaulted {@link M_CodeComment} will be constructed
	 * 
	 * @access public
	 * @return M_CodeComment
	 */
	public function getComment() {
		// Get the comment
		if(! $this->_comment) {
			$this->_comment = new M_CodeComment;
		}
		
		// Set defaults in the comment:
		if(! $this->_comment->getTitle()) {
			$this->_comment->setTitle($this->getName());
		}
		
		if(! $this->_comment->getAccess()) {
			if(! $this->getAccess()) {
				$this->_comment->setAccess(M_CodeAccess::TYPE_PUBLIC);
			} else {
				$this->_comment->setAccess($this->getAccess());
			}
		}
		
		if(! $this->_comment->getReturn()) {
			$this->_comment->setReturnVoid();
		}
		
		foreach($this->getParameters() as $parameter) {
			$this->_comment->addParameter($parameter);
		}
		
		if($this->isAbstract()) {
			$this->_comment->setIsAbstract(TRUE);
		}
		
		if($this->isStatic()) {
			$this->_comment->setIsStatic(TRUE);
		}
		
		return $this->_comment;
	}
	
	public function getAccess() {
		return $this->_access;
	}
	
	public function isStatic() {
		return ($this->_static === TRUE);
	}
	
	public function isAbstract() {
		return ($this->_abstract === TRUE);
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function getParameters() {
		return new ArrayIterator($this->_params);
	}
	
	public function getBody() {
		return $this->_body;
	}
	
	public function toString($numberOfTabsForIndentation = 1) {
		// We get the new line characters (including tabs), to start new lines
		// in the function block:
		$nl = M_CodeHelper::getNewLineWithTab(1, $numberOfTabsForIndentation);
		
		// Get comment:
		$out = $this->getComment()->toString($numberOfTabsForIndentation);
		
		// Add the function to the class' source code:
		$out .= $nl;
		
		if($this->isAbstract()) {
			$out .= 'abstract ';
		}
		
		// Default the function's access to PUBLIC:
		$tmp = $this->getAccess();
		if(! $tmp) {
			$tmp = M_CodeAccess::TYPE_PUBLIC;
		}
		$out .= $tmp . ' ';
		if($this->isStatic()) {
			$out .= 'static ';
		}
		
		$out .= 'function ' . $this->getName() . '(';
		
		foreach($this->getParameters() as $i => $parameter) {
			if($i > 0) {
				$out .= ', ';
			}
			$out .= '$' . M_Helper::ltrimCharlist($parameter->getName(), '$');
		}
		
		$out .= ')';
		
		// If the method is abstract, we do not add any body.
		if($this->isAbstract()) {
			$out .= ';';
		}
		// If not abstract, we add the body:
		else {
			$out .= ' {';
			$out .= $nl . M_CodeHelper::getTab() . $this->getBody();
			$out .= $nl . '}';
		}
		
		return $out .= $nl;
	}
}