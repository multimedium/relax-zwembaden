<?php
/**
 * M_EventDispatcher class
 * 
 * The M_EventDispatcher class allows to use the Observer Pattern.
 * The essence of this pattern is that one or more objects (called 
 * observers or listeners) are registered (or register themselves) 
 * to observe an event that may be raised (dispatched) by the 
 * observed object (the subject). (The object that may raise an 
 * event generally maintains a collection of the observers.)
 *
 * Event handling depends heavily on the M_EventDispatcher class. You 
 * may be familiar with M_EventDispatcher from other languages such 
 * as JavaScript or ActionScript 2.0/3.0.
 * 
 * M_EventDispatcher is actually a base class, which other classes 
 * extend in order to be able to have access to addEventListener 
 * and other M_EventDispatcher methods. If necessary, more advanced 
 * usages can also include M_EventDispatcher functionality through 
 * composition.
 * 
 * Typically, an M_EventDispatcher subclass will pair with a specific
 * Event subclass, in order to inform in more detail about the raised 
 * event, and to attach default event behavior.
 * 
 * Introduction on Event dispatchers and listeners, at:
 * http://www.adobe.com/devnet/actionscript/articles/event_handling_as3.html
 * 
 * @package Core
 */
class M_EventDispatcher extends M_Object implements MI_EventDispatcher {
	/**
	 * Listeners, or observers
	 * 
	 * The M_EventDispatcher maintains a collection of event listeners,
	 * or observers. To add observers to the event dispatcher, the 
	 * {M_EventDispatcher::addEventListener()} method.
	 *
	 * @access private
	 * @var array
	 */
	private $_eventListeners = array();
	
	/**
	 * Dispatch an event
	 *
	 * This method will announce, or trigger, an event. This dispatches 
	 * an event, and will call the listeners that have been attached to 
	 * that particular event.
	 * 
	 * Note that the signature of this method does not allow subclasses
	 * of M_EventDispatcher to override the implementation.
	 * 
	 * @access public
	 * @param M_Event $event
	 * @return void
	 */
	public final function dispatchEvent(M_Event $event) {
		$type = $event->getType();
		if(isset($this->_eventListeners[$type])) {
			for($i = 0, $n = count($this->_eventListeners[$type]); $i < $n; $i ++) {
				call_user_func($this->_eventListeners[$type][$i], $event);
			}
		}
	}
	
	/**
	 * Register an event listener
	 * 
	 * Event listeners, which are also called event handlers, are 
	 * functions that are executed in response to a specific event 
	 * being triggered, or dispatched. Adding an event listener is 
	 * a two-step process. First, you create a function or class 
	 * method to be executed in response to the event. This  is 
	 * sometimes called the "listener function" or the "event handler 
	 * function". Second, you use the listen() method to register 
	 * your listener function for a given event.
	 * 
	 * The listener function will receive an {@link Event} object.
	 * 
	 * Note that the signature of this method does not allow subclasses
	 * of M_EventDispatcher to override the implementation.
	 * 
	 * @access public
	 * @param string $event
	 * 		The event name
	 * @param mixed $handler
	 * 		The event listener. The event listener is a callback
	 * 		function definition.
	 * @return void
	 */
	public final function addEventListener($type, $handler) {
		if(!isset($this->_eventListeners[$type])) {
			$this->_eventListeners[$type] = array();
		}
		array_push($this->_eventListeners[$type], $handler);
	}
	
	/**
	 * Remove an event listener
	 * 
	 * This method will remove an event listener, previously added
	 * by the {@link M_EventDispatcher::addEventListener()} method.
	 * 
	 * Note that the signature of this method does not allow subclasses
	 * of M_EventDispatcher to override the implementation.
	 * 
	 * @access public
	 * @param string $event
	 * 		The event name
	 * @param mixed $handler
	 * 		The event listener/handler.
	 * @return void
	 */
	public final function removeEventListener() {
		// TODO: Get variable resource id
		// In order to remove an event listener from the M_EventDispatcher,
		// we have to generate a unique identifier for each listener. We
		// should keep in mind that different objects of the same class
		// could be added as listeners (each of them will have a unique
		// resource id)
	}
	
	/**
	 * Has event listeners?
	 * 
	 * This method will check whether or not the object has any event
	 * listeners attached to it. It will return TRUE if at least one
	 * event listener has been attached to the object, FALSE if none
	 * attached at all.
	 * 
	 * Optionally, you can limit the request to a given event type.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function hasEventListeners($type = NULL) {
		if($type) {
			return (isset($this->_eventListeners[$type]) && count($this->_eventListeners[$type]) > 0);
		} else {
			return (count($this->_eventListeners) > 0);
		}
	}

	/**
	 * Copy all Event listeners from this event-dispatcher to another one
	 *
	 * @param MI_EventDispatcher $eventDispatcher
	 * @author Ben Brughmans
	 * @return void
	 */
	public function copyEventListeners(MI_EventDispatcher $eventDispatcher) {
		foreach($this->_eventListeners AS $type => $handlers) {
			foreach($handlers AS $handler) {
				$eventDispatcher->addEventListener($type, $handler);
			}
		}
	}
}