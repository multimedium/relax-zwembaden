<?php
/**
 * M_Stopwatch class
 * 
 * The M_Stopwatch class is used, as its name suggests, to measure time.
 * It offers the typical tools that are available on a traditional 
 * stopwatch:
 * 
 * - start
 * - lap
 * - stop
 * 
 * @package Core
 */
class M_Stopwatch extends M_Object {
	/**
	 * The timers
	 *
	 * This property holds the timers that have been started with the
	 * {@link M_Stopwatch::start()} method.
	 * 
	 * @access private
	 * @var array
	 */
	private static $_timers = array();
	
	/**
	 * The stopped (freezed) timers
	 * 
	 * This property holds the time at which a timer has been stopped,
	 * or freezed. Check the {@link M_Stopwatch::stop()} method for more
	 * information.
	 *
	 * @access private
	 * @var array
	 */
	private static $_frozen = array();
	
	/**
	 * Start a timer
	 * 
	 * This method will start a timer. Once a timer has been started,
	 * you can get the elapsed time later on by using the following
	 * methods:
	 * 
	 * - {@link M_Stopwatch::getLap()}
	 * - {@link M_Stopwatch::getTime()}
	 * 
	 * You can use this method in combination with {@link M_Stopwatch::stop()},
	 * to measure the execution time of selected parts in your code.
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the timer
	 * @return float
	 */
	public static function start($name) {
		// PHP 4 method:
		// list($msec, $sec) = explode(' ', microtime());
		// self::$_timers[$name] = ((float) $msec + (float) $sec);
		// PHP 5:
		return self::$_timers[$name] = microtime(TRUE);
	}
	
	/**
	 * Will return the elapsed time in a lap
	 * 
	 * Note that, after the lap time has been measured, M_Stopwatch
	 * will reset the time, so it starts counting time from 0 (ZERO)
	 * again.
	 * 
	 * Example 1, measure the lap time:
	 * <code>
	 *    $i = 0;
	 *    M_Stopwatch::start('myTimer');
	 *    while(++$i < 3) {
	 *       sleep(2);
	 *       echo "loop $i: " . M_Stopwatch::getLap('myTimer') . ' seconds<br>';
	 *    }
	 * </code>
	 * 
	 * Example 1 would output something similar to the following:
	 * 
	 * <code>
	 *    loop 1: 2.0002639293671 seconds
	 *    loop 2: 2.0003340244293 seconds
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * You should have started the timer with {@link M_Stopwatch::start()},
	 * for this function to work properly. If the requested timer could 
	 * not have been found, this method will return FALSE.
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the timer
	 * @return float
	 */
	public static function getLap($name) {
		if(isset(self::$_timers[$name])) {
			if(self::$_timers[$name] === FALSE) {
				return self::$_frozen[$name];
			} else {
				$startTime = self::$_timers[$name];
				self::$_timers[$name] = microtime(TRUE);
				return (self::$_timers[$name] - $startTime);
			}
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Return elapsed time
	 * 
	 * This will return the elapsed time in a given timer.  Note 
	 * that this method does not reset the timer, as the method
	 * {@link M_Stopwatch::getLap()} does. Compare Example 1 to
	 * Example 1 of {@link M_Stopwatch::getLap()} to appreciate the
	 * difference in behavior:
	 * 
	 * Example 1, measure the elapsed time:
	 * <code>
	 *    $i = 0;
	 *    M_Stopwatch::start('myTimer');
	 *    while(++$i < 3) {
	 *       sleep(2);
	 *       echo "loop $i: " . M_Stopwatch::getTime('myTimer') . ' seconds<br>';
	 *    }
	 * </code>
	 * 
	 * Example 1 would output something similar to the following:
	 * 
	 * <code>
	 *    loop 1: 2.0002019405365 seconds
	 *    loop 2: 4.0004858970642 seconds
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * You should have started the timer with {@link M_Stopwatch::start()},
	 * for this function to work properly. If the requested timer could 
	 * not have been found, this method will return FALSE.
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the timer
	 * @return float
	 */
	public static function getTime($name) {
		if(self::$_timers[$name] === FALSE) {
			return self::$_frozen[$name];
		} else {
			$time = (microtime(TRUE) - self::$_timers[$name]);
			if(isset(self::$_frozen[$name])) {
				$time += self::$_frozen[$name];
			}
			return $time;
		}
	}
	
	/**
	 * Stop the stopwatch
	 * 
	 * This method will pause the stopwatch, remembering the time
	 * when the watch has been stopped, or frozen. Once you call 
	 * start on the timer again, M_Stopwatch will continue counting.
	 *
	 * Typically, this method is used to measure the execution 
	 * time of selected parts in the code.
	 * 
	 * Example 1
	 * <code>
	 *    $i = 0;
	 *    while(++$i < 10) {
	 *       // Start measuring the execution time of code:
	 *       M_Stopwatch::start('myTimer');
	 *       sleep(2);
	 *       M_Stopwatch::stop('myTimer');
	 *       // End of measured code.
	 *       
	 *       echo "This echo() call is not measured!<br>";
	 *    }
	 *    
	 *    // output the total execution time of the measured block
	 *    // of code
	 *    echo M_Stopwatch::getTime('myTimer');
	 * </code>
	 * 
	 * Example 1 would output something similar to the following:
	 * 
	 * <code>
	 *    This echo() call is not measured!
	 *    This echo() call is not measured!
	 *    This echo() call is not measured!
	 *    This echo() call is not measured!
	 *    This echo() call is not measured!
	 *    This echo() call is not measured!
	 *    This echo() call is not measured!
	 *    This echo() call is not measured!
	 *    This echo() call is not measured!
	 *    18.002415418625
	 * </code>
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the timer
	 * @return void
	 */
	public static function stop($name) {
		if(!isset(self::$_frozen[$name])) {
			self::$_frozen[$name]  = (float) self::getLap($name);
		} else {
			self::$_frozen[$name] += (float) self::getLap($name);
		}
		self::$_timers[$name] = FALSE;
	}
	
	/**
	 * Alias for {@link M_Stopwatch::stop()}
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the timer
	 * @return void
	 */
	public static function pause($name) {
		self::stop($name);
	}
	
	/**
	 * Remove timer
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the timer
	 * @return void
	 */
	public static function remove($name) {
		unset(self::$_timers[$name]);
		unset(self::$_frozen[$name]);
	}
}