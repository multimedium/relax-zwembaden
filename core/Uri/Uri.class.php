<?php
/**
 * M_Uri class
 * 
 * M_Uri is used to work with URL schemes. Typically, M_Uri is used
 * to fetch the parts that are present in a given URL, or to compose
 * a new URL. Also, this class may be used to manipulate selected
 * parts of a given URL.
 * 
 * @package Core
 */
class M_Uri extends M_Object {
	/**
	 * URL scheme
	 * 
	 * The scheme of the URL is the protocol identifier. For example,
	 * "http", "ftp", etc.
	 * 
	 * @access private
	 * @var string
	 */
	private $_scheme;
	
	/**
	 * User
	 * 
	 * @access private
	 * @var string
	 */
	private $_user;
	
	/**
	 * Password
	 * 
	 * @access private
	 * @var string
	 */
    private $_pass;
    
    /**
     * Host
     * 
     * @access private
     * @var string
     */
    private $_host;
    
    /**
     * Port
     * 
     * @access private
     * @var integer
     */
    private $_port;
    
    /**
     * Path
     * 
     * @access private
     * @var string
     */
    private $_path;
    
    /**
     * URL Query variables
     * 
     * @access private
     * @var array
     */
    private $_queryVariables;
    
    /**
     * Query arguments separator
     * 
     * @access private
     * @var string
     */
    private $_queryArgumentSeparator = '&';
    
    /**
     * Fragment
     * 
     * @access private
     * @var string
     */
    private $_fragment;
	
	/**
	 * Get Uri As String
	 * 
	 * Provides {@link M_Uri::toString()} functionality as a static method,
	 * so it can be used without having to construct an {@link M_Uri} first.
	 * For example:
	 * 
	 * <code>
	 *		echo M_Uri::getUriAsString('link-to-some-page');
	 * </code>
	 * 
	 * @see M_Uri::toString()
	 * @static
	 * @access public
	 * @param string $uri
	 * @return string
	 */
	public static function getUriAsString($uri) {
		// Construct a new M_Uri instance
		$uri = new self($uri);
		
		// Return the uri as a string
		return $uri->toString();
	}
    
    /**
     * Parse a path element out of string
     * 
     * This method can be used to parse a path element out of a given
     * string. Accents, spaces, and other special characters are removed
     * from the string. For more information about URI path elements,
     * see the docs on
     * 
     * - {@link M_Uri::getPath()}
     * - {@link M_Uri::getPathElement()}
     * 
     * @static
     * @access public
     * @param string $string
     * 		The string out of which to parse a URI Path Element
     * @param boolean $shortenPathToNumberOfWords
     * 		For SEO, it is recommended to have short paths. By setting this
     * 		argument, the path will be shortened (by removing the shortest
     * 		words). You can specify the max # of words in the path.
	 * @param array $preserveCharacters
	 *		Some special characters are replaced by URL-Compatible characters
	 *		(typically replaced by dashes). If you want some characters to NOT
	 *		be replaced, you should provide them here.
     * @return string
     */
	public static function getPathElementFromString($string, $shortenPathToNumberOfWords = 7, array $preserveCharacters = array()) {
    	// First of all, we create a chain of filters, to
    	// - remove all accents (converted to corresponding character)
    	// - remove funny MS Word characters :)
    	// - remove html markup
    	$filter = new M_FilterTextAccents(
    		new M_FilterTextMsWordCharacters(
	    		new M_FilterTextHtml(
	    			new M_FilterTextValue($string)
	    		)
	    	)
    	);
    	
    	// Remove punctuation at the beginning/end of the string
    	// (we don't want a point at the end to be converted into a dash)
    	$string = M_Helper::trimCharlist($filter->apply(), ',.?!)("\'/\\_+;:*');
    	
		// Remove redundant white-spaces:
		$string = preg_replace('/[\s\-]+/', ' ', $string);
		
		// Remove punctuation (replaced by dashes):
		if(! in_array(',',  $preserveCharacters)) $string = str_replace(',', '-', $string);
		if(! in_array('.',  $preserveCharacters)) $string = str_replace('.', '-', $string);
		if(! in_array('?',  $preserveCharacters)) $string = str_replace('?', '-', $string);
		if(! in_array('!',  $preserveCharacters)) $string = str_replace('!', '-', $string);
		if(! in_array(')',  $preserveCharacters)) $string = str_replace(')', '-', $string);
		if(! in_array('(',  $preserveCharacters)) $string = str_replace('(', '-', $string);
		if(! in_array('"',  $preserveCharacters)) $string = str_replace('"',  '', $string);
		if(! in_array('\'', $preserveCharacters)) $string = str_replace('\'', '', $string);
		if(! in_array('/',  $preserveCharacters)) $string = str_replace('/', '-', $string);
		if(! in_array('_',  $preserveCharacters)) $string = str_replace('_', '-', $string);
		if(! in_array('+',  $preserveCharacters)) $string = str_replace('+', '-', $string);
		if(! in_array(';',  $preserveCharacters)) $string = str_replace(';', '-', $string);
		if(! in_array(':',  $preserveCharacters)) $string = str_replace(':', '-', $string);
		if(! in_array('*',  $preserveCharacters)) $string = str_replace('*',  '', $string);
		if(! in_array(' ',  $preserveCharacters)) $string = str_replace(' ', '-', $string);
		if(! in_array('%',  $preserveCharacters)) $string = str_replace('%', '-', $string);
		if(! in_array('@',  $preserveCharacters)) $string = str_replace('@', '-', $string);
		if(! in_array('&',  $preserveCharacters)) $string = str_replace('&', '-', $string);
		if(! in_array('#',  $preserveCharacters)) $string = str_replace('#', '-', $string);
		
		// Remove redundant dashes, and make lowercase:
		$string = strtolower(preg_replace('/[-]+/', '-', $string));
		
		// Now, we remove anything that is not in the alphanumerical range. We
		// compose a string with allowed characters:
		$alphabet = ' abcdefghijklmnopqrstuvwxyz0123456789-_' . implode('', $preserveCharacters);
		
		// For each of the characters in the string:
		$out = '';
		for($i = 0, $strLength = strlen($string); $i < $strLength; $i ++) {
			// If the current character is in the range of allowed characters:
			if(strpos($alphabet, $string{$i})) {
				// We add the character to the output string:
				$out .= $string{$i};
			}
		}
		
		// Prepare some variables, in order to start shortening the path:
		$elements = explode('-', $out);
		$elementCount = count($elements);
		$minWordLength = 3;

		// By default, we do not remove numbers, because many times they add
		// meaning to the path. We prepare a counter of numeric path elements,
		// and a boolean that indicates whether or not numeric elements should
		// be removed from the path (FALSE by default)
		$numericElementCount = 0;
		$removeNumbers = FALSE;

		// While the path contains more than the max # of words:
		while($elementCount > $shortenPathToNumberOfWords) {
			// For each of the path elements:
			for($i = 0; $i < $elementCount; $i ++) {
				// Note that we do not remove numbers, because many times they add
				// meaning to the path. Of course, it is possible that the numbers
				// are removed, if they cause an infinite loop in this method.
				// The boolean $removeNumbers tells us whether or not to remove
				// numbers...
				if($removeNumbers || (! $removeNumbers && ! is_numeric($elements[$i]))) {
					// If the current element in the path is shorter than the minimum
					// length of words that has been defined for the current loop
					if(strlen($elements[$i]) < $minWordLength) {
						// We remove it:
						array_splice($elements, $i, 1);

						// Update some loop variables
						$i -= 1; $elementCount -= 1;
					}
				}
				// If the current path element is numeric (a number)
				else {
					// Then, we increment the number of numeric path elements,
					// and we set a new value for the $removeNumbers boolean (the
					// boolean that tells us whether or not we should remove
					// numeric path elements)
					if(! $removeNumbers) {
						$removeNumbers = (++ $numericElementCount >= $shortenPathToNumberOfWords);
					}
				}
			}
			
			// In the next loop, we look for longer words to remove:
			$minWordLength += 1;
		}
		
		// return final path element:
		return implode('-', $elements);
	}
	
	/**
	 * Shifts the first value of the path off, and returns it
	 * 
	 * Shifts the first value of the path off and returns it, shortening 
	 * the path by one element.
	 * 
	 * NOTE:
	 * If no more elements are available in the path, this method will
	 * return (boolean) FALSE.
	 * 
	 * @example M_Uri::shiftElementFromPath('news/test/test2') = array('news',  'test/test2');
	 * @example M_Uri::shiftElementFromPath('test/test2') = array('test',  'test2');
	 * @example M_Uri::shiftElementFromPath('test2') = array('test2', '');
	 * @example M_Uri::shiftElementFromPath('') = array(FALSE, '');
	 * @access public
	 * @return array
	 */
	public static function shiftElementFromPath($path) {
		$path  = explode('/', M_Helper::trimCharlist($path, '/'));
		$first = array_shift($path);
		return array($first, implode('/', $path));
	}
	
	/**
	 * Pop the last value of the path off, and returns it
	 * 
	 * Pops the last value of the path off and returns it, shortening 
	 * the path by one element.
	 * 
	 * NOTE:
	 * If no more elements are available in the path, this method will
	 * return (boolean) FALSE.
	 * 
	 * @see M_Uri::shiftElementFromPath()
	 * @access public
	 * @return array
	 */
	public static function popElementFromPath($path) {
		$path  = explode('/', $path);
		$last = array_pop($path);
		return array($last, implode('/', $path));
	}
	
    /**
     * Constructor
     * 
     * Note that the URL string is an optional argument. Typically,
     * you would construct an M_Uri object to create a new URL.
     * 
     * Example 1
     * <code>
     *    $uri = new M_Uri;
     *    $uri->setScheme('http');
     *    $uri->setHost('www.multimedium.be');
     *    echo $uri;
     * </code>
     * 
     * NOTE:
     * The echo() call will cast the M_Uri object to a string, so the
     * result of {@link M_Uri::__toString()} is outputted. In the case
     * of Example 1, the output would be:
     * 
     * <code>http://www.multimedium.be</code>
     * 
     * @access public
     * @see M_Uri::__toString()
     * @param string $uri
     * @return M_Uri
     */
	public function __construct($uri = NULL) {
		if($uri) {
			$this->setUri($uri);
		}
	}
	
	/**
	 * Get the scheme of the URL
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    echo $uri->getScheme();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>http</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getScheme() {
		return $this->_scheme;
	}
	
	/**
	 * Get the username in the URL
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('ftp://john:mypassword@ftp.multimedium.be');
	 *    echo $uri->getUsername();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>john</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getUsername() {
		return $this->_user;
	}
	
	/**
	 * Get the password in the URL
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('ftp://john:mypassword@ftp.multimedium.be');
	 *    echo $uri->getPassword();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>mypassword</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getPassword() {
		return $this->_pass;
	}
	
	/**
	 * Get the domain name (without subdomain)
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    echo $uri->getDomainName();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>multimedium.be</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getDomainName() {
		// Get the host, and explode into array:
		$parts = explode('.', $this->getHost());
		
		// If more than one part is found in the array
		if(count($parts) > 1) {
			// Then, a subdomain is available. We remove the subdomain:
			unset($parts[0]);
		}
		
		// Return the domain name:
		return implode('.', $parts);
	}
	
	/**
	 * Get the subdomain name
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    echo $uri->getSubdomain();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>www</code>
	 * 
	 * @access public
	 * @return string|NULL
	 */
	public function getSubdomainName() {
		// Get the host, and explode into array:
		$parts = explode('.', $this->getHost());
		
		// If more than one part is found in the array
		if(count($parts) > 1) {
			// Then, a subdomain is available. We return it:
			return $parts[0];
		}
		
		// If still here, no subdomain is set. 
		// We return NULL in this case:
		return NULL;
	}
	
	/**
	 * Get the host
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    echo $uri->getHost();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>www.multimedium.be</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getHost() {
		return $this->_host;
	}
	
	/**
	 * Get the port
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be:80');
	 *    echo $uri->getPort();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>80</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getPort() {
		return $this->_port;
	}
	
	/**
	 * Get host, including port
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be:80');
	 *    echo $uri->getHostWithPort();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>www.multimedium.be:80</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getHostWithPort() {
		$output = $this->_host;
		if(!empty($this->_port)) {
			$output .= ':' . $this->_port;
		}
		return $output;
	}
	
	/**
	 * Get TLD
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be:80');
	 *    echo $uri->getTld();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>be</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getTld() {
		return array_pop(explode('.', $this->getHost()));
	}
	
	/**
	 * Get path
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be/news/archive');
	 *    echo $uri->getPath();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>news/archive</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getPath() {
		return $this->_path;
	}
	
	/**
	 * Is path?
	 * 
	 * Will tell whether or not the URI path is the same one as the provided one
	 * 
	 * @access public
	 * @param string $path
	 * 		The path to compare with
	 * @return bool
	 */
	public function isPath($path) {
		return (M_Helper::trimCharlist($this->_path, '/') == M_Helper::trimCharlist((string) $path, '/'));
	}
	
	/**
	 * Is internal?
	 * 
	 * Will tell whether or not the URI is "internal" (same domain)
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if relative, FALSE if not
	 */
	public function isInternal() {
		return ($this->getHost() == M_Request::getUri()->getHost());
	}
	
	/**
	 * Get relative path
	 * 
	 * If the URI is internal (see {@link M_Uri::isInternal()}), this method 
	 * will remove the base href from the path, in order to provide with a relative
	 * path. The base path is:
	 * 
	 * <code>M_Request::getLink()</code>
	 * 
	 * NOTE:
	 * If the URI is not internal, this method will return NULL
	 * 
	 * @access public
	 * @return string
	 */
	public function getRelativePath() {
		// Is not internal? Then return NULL
		if(! $this->isInternal()) {
			return NULL;
		}
		
		// Get base href:
		$basePath = M_Request::getLinkWithoutPrefix();
		
		// Make a copy of the URI, and remove the variables in the request string.
		// This way, we only extract the path :)
		$uriTemp = clone $this;
		$uriTemp->removeQueryVariables();
		$path = $uriTemp->getUri();
		
		// Remove base href from the path
		$path = str_replace($basePath, '', $path);
		
		if($path == false) return '';
		
		// Return path:
		return M_Helper::trimCharlist($path, '/');
	}
	
	/**
	 * Get path element
	 * 
	 * This will return the value of a given element in the path. To 
	 * select an element in the path, an index number (starting at
	 * 0) is provided.
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be/news/archive');
	 *    echo $uri->getPathElement(1);
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>archive</code>
	 * 
	 * NOTE:
	 * If the requested path element does not exist, this method will
	 * return NULL.
	 * 
	 * @access public
	 * @param integer $index
	 * 		The element's index number (starting at 0)
	 * @return string
	 */
	public function getPathElement($index) {
		return $this->_getPathElementFrom($this->_path, $index);
	}

	/**
	 * Get path elements
	 *
	 * This will return the collection of elements that is present in the path of
	 * the URI.
	 *
	 * @access public
	 * @return string
	 */
	public function getPathElements() {
		return new M_ArrayIterator($this->_getPathElementsFrom($this->_path));
	}

	/**
	 * Get path element count
	 *
	 * This will return the number of elements that is present in the URI.
	 *
	 * @access public
	 * @return integer
	 */
	public function getPathElementCount() {
		return $this->_getPathElementCountFrom($this->_path);
	}
	
	/**
	 * Get relative path element
	 * 
	 * This will return the value of a given element in the relative path. To 
	 * select an element in the path, an index number (starting at
	 * 0) is provided.
	 * 
	 * Example 1 with http://localhost/applications as base-path
	 * <code>
	 *    $uri = new M_Uri('http://localhost/applications/test/nl/news/archive');
	 *    echo $uri->getRelativePathElement(1);
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>nl</code>
	 * 
	 * NOTE:
	 * If the requested path element does not exist, this method will
	 * return NULL.
	 *
	 * @uses M_Uri::getRelativePath()
	 * @access public
	 * @param integer $index
	 * 		The element's index number (starting at 0)
	 * @return string
	 */
	public function getRelativePathElement($index) {
		return $this->_getPathElementFrom($this->getRelativePath(), $index);
	}

	/**
	 * Get relative path elements
	 *
	 * This will return the collection of elements that is present in the relative
	 * path of the URI.
	 *
	 * @uses M_Uri::getRelativePath()
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getRelativePathElements() {
		return new M_ArrayIterator($this->_getPathElementsFrom($this->getRelativePath()));
	}

	/**
	 * Get relative path element count
	 *
	 * This will return the number of elements that is present in the relative path;
	 * see {@link M_Uri::getRelativePath()}.
	 *
	 * @uses M_Uri::getRelativePath()
	 * @access public
	 * @return integer
	 */
	public function getRelativePathElementCount() {
		return $this->_getPathElementCountFrom($this->getRelativePath());
	}

	/**
	 * Has query variables?
	 *
	 * @access public
	 * @return bool $flag
	 *		Will return TRUE if the URI has query variables, FALSE if not
	 */
	public function hasQueryVariables() {
		return (count($this->_queryVariables) > 0);
	}

	/**
	 * Get query variables
	 * 
	 * This method will return an iterator (ArrayIterator) on the
	 * query variables that have been parsed from the original URL.
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be?lng=nl&page=news');
	 *    $var = $uri->getQueryVariables();
	 *    foreach($var as $name => $value) {
	 *       echo "$name: $value\n";
	 *    }
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>
	 *    lng: nl
	 *    page: news
	 * </code>
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getQueryVariables() {
		return new ArrayIterator($this->_queryVariables);
	}
	
	/**
	 * Get a query variable
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be?lng=nl&page=news');
	 *    echo $uri->getQueryVariable('page');
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>news</code>
	 * 
	 * If the requested variable could not have been found, this 
	 * method will return the default value (second argument to this
	 * method).
	 * 
	 * @access public
	 * @see M_Uri::getQueryVariables()
	 * @param string $name
	 * 		The name of the query variable
	 * @param mixed $defaultValue
	 * 		The default value to be returned, if no value could have
	 * 		been found for the requested query variable.
	 * @return mixed
	 */
	public function getQueryVariable($name, $defaultValue = FALSE) {
		return (isset($this->_queryVariables[$name]) ? $this->_queryVariables[$name] : $defaultValue);
	}
	
	/**
	 * Get query string
	 * 
	 * This method will return the query string, parsed into the
	 * URL-encoded format.
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be?lng=nl&page=news');
	 *    echo $uri->getQueryString();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>lng=nl&page=news</code>
	 * 
	 * NOTE:
	 * To separate the query variables, this method will use the
	 * symbol that has been set previously with {@link M_Uri::setQuerySeparator()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getQueryString() {
		return http_build_query($this->_queryVariables, '', $this->_queryArgumentSeparator);
	}
	
	/**
	 * Get path
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be/news#highlighted');
	 *    echo $uri->getFragment();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>highlighted</code>
	 * 
	 * @access public
	 * @return string
	 */
	public function getFragment() {
		return $this->_fragment;
	}
	
	/**
	 * Get URI
	 * 
	 * This method will return the complete URL string that is being
	 * represented by the M_Uri object.
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri;
	 *    $uri->setScheme('ftp');
	 *    $uri->setHost('ftp.multimedium.be');
	 *    $uri->setUsername('john');
	 *    $uri->setPassword('mypass');
	 *    $uri->setPort(21);
	 *    echo $uri->getUri();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>ftp://john:mypass@ftp.multimedium.be:21</code>
	 * 
	 * Note that you can decide whether or not to include parts of the URI in the
	 * output string. For example, you might want to retrieve the URL string for
	 * the object, without the login credentials in our example:
	 * 
	 * Example 2
	 * <code>
	 *    // Compose the URI
	 *    $uri = new M_Uri;
	 *    $uri->setScheme('ftp');
	 *    $uri->setHost('ftp.multimedium.be');
	 *    $uri->setUsername('john');
	 *    $uri->setPassword('mypass');
	 *    $uri->setPort(21);
	 *    
	 *    // Show the URL String, without login credentials:
	 *    echo $uri->getUri(TRUE, FALSE, FALSE);
	 * </code>
	 * 
	 * In our second example, we would get the following output:
	 * <code>ftp://ftp.multimedium.be:21</code>
	 * 
	 * For more information about the parts that can be included/excluded, please
	 * check the documentation on this function's accepted parameters.
	 * 
	 * @access public
	 * @param boolean $includeScheme
	 *		Set to TRUE to include the protocol (scheme), FALSE to exclude
	 * @param boolean $includeHost
	 *		Set to TRUE to include the host name, FALSE to exclude
	 * @param boolean $includeUser
	 *		Set to TRUE to include the usernmae, FALSE to exclude
	 * @param boolean $includePass
	 *		Set to TRUE to include the password, FALSE to exclude
	 * @param boolean $includePort
	 *		Set to TRUE to include the port number, FALSE to exclude
	 * @param boolean $includePath
	 *		Set to TRUE to include the path, FALSE to exclude
	 * @param boolean $includeQueryVariables
	 *		Set to TRUE to include the query variables, FALSE to exclude
	 * @param boolean $includeFragment
	 *		Set to TRUE to include the fragment, FALSE to exclude
	 * @return string
	 */
	public function getUri($includeScheme = TRUE, $includeHost = TRUE, $includeUser = TRUE, $includePass = TRUE, $includePort = TRUE, $includePath = TRUE, $includeQueryVariables = TRUE, $includeFragment = TRUE) {
		// The output string:
		$uri = '';
		
		// If the scheme is to be included:
		if($includeScheme) {
			// Then, we add the scheme to the output string:
			$uri = $this->_scheme . '://';
		}
		
		// If the username is to be shown, and a username is available
		if($includeUser && $this->_user) {
			// Then, we add the username:
			$uri .= urlencode($this->_user);
			
			// If the password is to be shown, and a password has been defined
			// for the URI object:
			if($includePass && $this->_pass) {
				// Then, we add the password to the output string
				$uri .= ':' . urlencode($this->_pass);
			}
			
			// We add a separator for the host:
			$uri .= '@';
		}
		
		// If the host name is to be included:
		if($includeHost) {
			// Then, add the host name to the output string:
			$uri .= $this->_host;
		}
		
		// If the port number is to be included, and a port number has been
		// specified for the URI:
		if($includePort && $this->_port) {
			// Then, add to output:
			$uri .= ':' . $this->_port;
		}
		
		// If the path is to be included in the output, and a path is available:
		if($includePath && $this->_path) {
			// Then, add to output:
			$uri .= strncmp($this->_path, '/', 1) 
				? '/' . $this->_path 
				: $this->_path;
		}
		
		// If query variables have been included with the arguments to this function,
		// and variables are effectively present in the URI:
		if($includeQueryVariables && count($this->_queryVariables) > 0) {
			// Then, we add the query variables the URI:
			$uri .= '?' . $this->getQueryString($this->_queryArgumentSeparator);
		}
		
		// Show fragment?
		if($includeFragment && $this->_fragment) {
			// Yes, add to the output string
			$uri .= '#' . $this->_fragment;
		}
		
		// Return the output string:
		return $uri;
	}

	/**
	 * Get shortened version, for display
	 *
	 * Will shorten the URI String, for display. Note that you can provide this
	 * function with a maximum number of characters that you wish to display, and
	 * that the function will replace parts of the URI at the beginning, so the
	 * end of the URI String stays intact.
	 *
	 * @access public
	 * @param integer $maxLength
	 * @param integer $etc
	 * 		The "etcetera" symbol (used to replace elements in the URI)
	 * @return string
	 */
	public function getUriSnippetEnd($maxLength = 40, $etc = '...') {
		// Get a clone of the URI:
		$clone = clone($this);

		// Prepare working variables:
		// - The number of characters in the "etcetera" symbol
		$lengthEtc = strlen($etc);

		/* @var $clone M_Uri */
		// While the URI is longer than the provided maximum length:
		while(strlen($clone->toString()) > $maxLength) {
			// Then, we'll need to shorten the URI. First, we'll try to remove path
			// elements. We prepare a boolean that indicates whether or not we
			// have shortened a path element:
			$pathShortened = FALSE;

			// Get the path elements in the URI:
			$elements = $clone->getPathElements()->getArrayCopy();
			$elementLastIndex = $clone->getPathElementCount() - 1;
			
			// For each of the path elements in the URI:
			foreach($elements as $i => $element) {
				// If the current path element does not equal the "etcetera" symbol,
				// and if the element has more characters than the symbol:
				if($element != $etc && strlen($element) > $lengthEtc && $i < $elementLastIndex) {
					// Then, we replace the current path element by the etcetera
					// symbol:
					$elements[$i] = $etc;

					// We set the boolean that indicates that we have shortened
					// an element in the path:
					$pathShortened = TRUE;

					// We break out of the foreach loop, because we have shortened
					// the URI. This way, we force a new evaluation of the URI's
					// length
					break;
				}
			}

			// If we have not shortened any path element:
			if(! $pathShortened) {
				// Then, we replace the host (domain name) by the "etcetera" symbol.
				// That is, of course, if the host name contains more characters
				// than the symbol that would be replacing it:
				if(strlen($clone->getHost()) > $lengthEtc) {
					// Replace the host name:
					$clone->setHost($etc);
				}

				// We break out of the while loop, to prevent infinite loops:
				break;
			}
			// If we have shortened a path element:
			else {
				// Set the new path:
				$clone->setPathElements($elements);
			}
		}

		// Return the snippet:
		return $clone->toString();
	}
	
	/**
	 * Get session
	 * 
	 * Will construct an instance of {@link M_UriSession} with this URI.
	 * 
	 * @access public
	 * @return M_UriSession
	 */
	public function getSession() {
		return new M_UriSession($this);
	}
	
	/**
	 * Set URL
	 * 
	 * This method will set the URL that is being represented by the
	 * M_Uri object. This method is also used by the constructor, if
	 * it is being provided with a URL.
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri;
	 *    $uri->setUri('http://www.multimedium.be:80');
	 *    echo $uri->getPort();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>80</code>
	 * 
	 * @access public
	 * @param string $uri
	 * @return void
	 */
	public function setUri($uri) {
		$parts = parse_url($uri);
		$this->_scheme = isset($parts['scheme']) ? $parts['scheme'] : NULL;
		$this->_user = isset($parts['user']) ? $parts['user'] : NULL;
		$this->_pass = isset($parts['pass']) ? $parts['pass'] : NULL;
		$this->_host = isset($parts['host']) ? $parts['host'] : NULL;
		$this->_port = isset($parts['port']) ? $parts['port'] : NULL;
		$this->_path = isset($parts['path']) ? $parts['path'] : NULL;
		if(isset($parts['query'])) {
			$this->setQuery($parts['query']);
		} else {
			$this->_queryVariables = array();
		}
		$this->_fragment = isset($parts['fragment']) ? $parts['fragment'] : NULL;
	}
	
	/**
	 * Set the scheme of the URL
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    $uri->setScheme('https');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>https://www.multimedium.be</code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param string $scheme
	 * @return void
	 */
	public function setScheme($scheme) {
		$this->_scheme = rtrim($scheme, " \t\n\r\0\x0B:/");
	}
	
	/**
	 * Set username
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('ftp://user:pass@ftp.multimedium.be');
	 *    $uri->setUsername('john');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>ftp://john:pass@ftp.multimedium.be</code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param string $name
	 * @return void
	 */
	public function setUsername($name) {
		$this->_user = $name;
	}
	
	/**
	 * Set username
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('ftp://user:pass@ftp.multimedium.be');
	 *    $uri->setPassword('myPassword');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>ftp://user:myPassword@ftp.multimedium.be</code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param string $password
	 * @return void
	 */
	public function setPassword($password) {
		$this->_pass = $password;
	}
	
	/**
	 * Set username
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('ftp://user:pass@ftp.multimedium.be');
	 *    $uri->setHost('www.my-domain.com');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>ftp://user:pass@www.my-domain.com</code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param string $host
	 * @return void
	 */
	public function setHost($host) {
		$this->_host = $host;
	}
	
	/**
	 * Set port
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('ftp://user:pass@ftp.multimedium.be');
	 *    $uri->setPort(21);
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>ftp://user:pass@ftp.multimedium.be:21</code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param integer $port
	 * @return void
	 */
	public function setPort($port) {
		$this->_port = $port;
	}
	
	/**
	 * Set path
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('ftp://user:pass@ftp.multimedium.be');
	 *    $uri->setPath('public_html');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>ftp://user:pass@ftp.multimedium.be/public_html</code>
	 * 
	 * Example 2
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    $uri->setPath('news/archive');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 2 would output the following:
	 * <code>http://www.multimedium.be/news/archive</code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param string $path
	 * @return void
	 */
	public function setPath($path) {
		$this->_path = M_Helper::rtrimCharlist($path, '/');
	}

	/**
	 * Set path with array of path elements
	 *
	 * @see M_Uri::setPath()
	 * @access public
	 * @param array $elements
	 *		The array of path elements, to be included in the path of the URI
	 * @return M_Uri
	 */
	public function setPathElements(array $elements) {
		$this->_path = M_Helper::rtrimCharlist(implode('/', $elements));
	}

	/**
	 * Set path element
	 *
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be/blog/one-time-passwords');
	 *    $uri->setPathElement(0, 'news');
	 *    echo $uri;
	 * </code>
	 *
	 * Example 1 would output the following:
	 * <code>http://www.multimedium.be/news/one-time-passwords</code>
	 *
	 * @access public
	 * @param integer $index
	 * 		The element's index number (starting at 0)
	 * @param string $pathElement
	 *		The new element
	 * @return void
	 */
	public function setPathElement($index, $pathElement) {
		// Get path elements:
		$elm = $this->_getPathElementsFrom($this->_path);

		// If no element exists at the requested index:
		if(! isset($elm[$index])) {
			// Then, we append to the path:
			$index = count($elm);
		}

		// Set the new element:
		$elm[$index] = (string) $pathElement;

		// Set the path:
		$this->setPathElements($elm);
	}
	
	/**
	 * Append to path
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('ftp://user:pass@ftp.multimedium.be');
	 *    $uri->setPath('public_html');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>ftp://user:pass@ftp.multimedium.be/public_html</code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param string $path
	 * @return void
	 */
	public function appendPath($path) {
		$this->_path = M_Helper::rtrimCharlist((string) $this->_path, '/') . '/' . M_Helper::trimCharlist((string) $path, '/');
	}
	
	/**
	 * Set fragment
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    $uri->setFragment('contact');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>http://www.multimedium.be#contact</code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param string $fragment
	 * @return void
	 */
	public function setFragment($fragment) {
		$this->_fragment = ltrim($fragment, " \t\n\r\0\x0B#");
	}
	
	/**
	 * Set query
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    $uri->setQuery('page=news&lng=nl');
	 *    $var = $uri->getQueryVariables();
	 *    foreach($var as $name => $value) {
	 *       echo "$name: $value\n";
	 *    }
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>
	 *    page: news
	 *    lng: nl
	 * </code>
	 * 
	 * @access public
	 * @param string $query
	 * @return void
	 */
	public function setQuery($query) {
		parse_str(urldecode(str_replace('&amp;', '&', $query)), $this->_queryVariables);
	}
	
	/**
	 * Set query variable
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be?page=news&lng=nl');
	 *    $uri->setQueryVariable('lng', array('nl', 'fr'));
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>
	 *    http://www.multimedium.be?page=news&lng%5B0%5D=nl&lng%5B1%5D=fr
	 * </code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param string $name
	 * 		The name of the query variable
	 * @param string $value
	 * 		The new value of the query variable
	 * @return void
	 */
	public function setQueryVariable($name, $value) {
		$this->_queryVariables[$name] = $value;
	}
	
	/**
	 * Set query variables (in bulk)
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be?page=news&lng=nl');
	 *    $uri->setQueryVariables(array(
	 *       'type' => 'events',
	 *       'lng'  => 'en'
	 *    ));
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>
	 *    http://www.multimedium.be?page=news&lng=en&type=events
	 * </code>
	 * 
	 * Note that, if you set the second argument to TRUE, this method 
	 * will first clear all query variables, so the query contains only 
	 * thpse variables that have been provided to this method.
	 * 
	 * Example 2
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be?page=news&lng=nl');
	 *    $uri->setQueryVariables(array('page' => 'events'), TRUE);
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 2 would output the following:
	 * <code>
	 *    http://www.multimedium.be?page=events
	 * </code>
	 * 
	 * @access public
     * @see M_Uri::setQueryVariable()
     * @see M_Uri::__toString()
	 * @param array $vars
	 * 		The associative array of query variables
	 * @param boolean $clear
	 * 		If set to TRUE, M_Uri will first clear previously set
	 * 		query variables. Check out Example 2.
	 * @return void
	 */
	public function setQueryVariables(array $vars, $clear = FALSE) {
		if($clear) {
			$this->_queryVariables = $vars;
		} else {
			foreach($vars as $name => $value) {
				$this->setQueryVariable($name, $value);
			}
		}
	}
	
	/**
	 * Set query variables separator
	 * 
	 * This method will set the character(s) with which query variables
	 * are separated in a parsed query string.
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be?page=news&lng=nl');
	 *    $uri->setQuerySeparator('&amp;');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>
	 *    http://www.multimedium.be?page=news&amp;lng=nl
	 * </code>
	 * 
	 * @access public
	 * @see M_Uri::getQueryString()
	 * @see M_Uri::getUri()
     * @see M_Uri::__toString()
	 * @param string $separator
	 * @return void
	 */
	public function setQuerySeparator($separator) {
		$this->_queryArgumentSeparator = $separator;
	}
	
	/**
	 * Remove query variable
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be?page=news&lng=nl');
	 *    $uri->removeQueryVariable('page');
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>
	 *    http://www.multimedium.be?lng=nl
	 * </code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @param string $name
	 * @return void
	 */
	public function removeQueryVariable($name) {
		unset($this->_queryVariables[$name]);
	}
	
	/**
	 * Remove all query variables in the URI
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be?page=news&lng=nl');
	 *    $uri->removeQueryVariables();
	 *    echo $uri;
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>
	 *    http://www.multimedium.be
	 * </code>
	 * 
	 * @access public
     * @see M_Uri::__toString()
	 * @return void
	 */
	public function removeQueryVariables() {
		$this->_queryVariables = array();
	}

	/**
	 * Export to string
	 *
	 * @access public
	 * @uses M_Uri::getUri()
	 * @return string
	 */
	public function toString() {
		return $this->getUri();
	}
	
	/**
	 * Export to string
	 * 
	 * This is a "magic method", called automatically by PHP when the
	 * M_Uri object is being casted to a string. For example, an echo 
	 * call on an M_Uri object will print the result of this function.
	 * 
	 * Example 1
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    echo $uri; // prints the result of __toString()
	 * </code>
	 * 
	 * Example 2
	 * <code>
	 *    $uri = new M_Uri('http://www.multimedium.be');
	 *    $str  = (string) $uri; // saves the result of __toString()
	 * </code>
	 * 
	 * @access public
	 * @uses M_Uri::getUri()
	 * @return string
	 */
	public function __toString() {
		return $this->getUri();
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * This will return the elements in a given path
	 *
	 * @access public
	 * @param string $path
	 * 		The path in which we will search for elements
	 * @return string
	 */
	private function _getPathElementsFrom($path) {
		return explode('/', M_Helper::trimCharlist((string) $path, '/'));
	}

	/**
	 * This will return the value of a given element in a path
	 *
	 * @access public
	 * @param string $path
	 * 		The path in which we search for the given element
	 * @param integer $index
	 * 		The element's index number (starting at 0)
	 * @return string
	 */
	private function _getPathElementFrom($path, $index) {
		$elm = $this->_getPathElementsFrom($path);
		if(isset($elm[$index])) {
			return $elm[$index];
		} else {
			return NULL;
		}
	}

	/**
	 * This will return the number of elements in a given path
	 *
	 * @access public
	 * @param string $path
	 * 		The path in which we count elements
	 * @return string
	 */
	private function _getPathElementCountFrom($path) {
		return count($this->_getPathElementsFrom($path));
	}
}