<?php
/**
 * M_ValidatorBankAccount
 *
 * M_ValidatorBankAccount, a subclass of {@link M_Validator} is used
 * to validate a Belgian bank account number.
 *
 * @package Core
 */
class M_ValidatorBankAccount extends M_Validator {
	/**
	 * Evaluate the subject
	 *
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 *
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		$match = array();
		// Remove dashes from the subject
		$subject = str_replace('-', '', $subject);
		
		if(preg_match('/^([0-9]{10})([0-9]{2})$/', $subject, $match)) {
			// Mod97 check on last nine digits
			$checksum = fmod($match[1],97);
			// check if 0
			if ($checksum == 0) $checksum = 97;
			// Do Mod97 check
			if ($checksum == $match[2]) {
				return TRUE;
			}
		}
		return FALSE;
	}
}