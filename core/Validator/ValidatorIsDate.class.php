<?php
/**
 * M_ValidatorIsDate
 * 
 * M_ValidatorIsDate, a subclass of {@link M_Validator} is used
 * to validate a date string.
 * 
 * @package Core
 */
class M_ValidatorIsDate extends M_Validator {
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the date format as validator
	 * criteria. Read {@link M_Validator::setCriteria()} to learn more
	 * about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsDate;
	 *    $validator->setCriteria(M_ValidatorIsDate::FORMAT, 'Y-m-d');
	 * </code>
	 * 
	 * This criteria sets the pattern that the tested date string
	 * should match. Note that the pattern is described in exactly the
	 * same way as you would provide it to PHP's date() function.
	 * 
	 * @access public
	 * @var string
	 */
	const FORMAT = 'format';
	
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to set the offset for short year notations. This
	 * offset is added to the 2-digit years, in order to calculate the final year
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsDate;
	 *    $validator->setCriteria(M_ValidatorIsDate::FORMAT, 'd/m/y');
	 *    $validator->setCriteria(M_ValidatorIsDate::OFFSET_YEAR_2DIGITS, 1900);
	 * </code>
	 * 
	 * This example shows a validator that would interpret the string 
	 * "14/04/81" as "April 14th 1981".
	 * 
	 * Example 2
	 * <code>
	 *    $validator = new M_ValidatorIsDate;
	 *    $validator->setCriteria(M_ValidatorIsDate::FORMAT, 'd/m/y');
	 *    $validator->setCriteria(M_ValidatorIsDate::OFFSET_YEAR_2DIGITS, 2000);
	 * </code>
	 * 
	 * This example shows a validator that would interpret the string 
	 * "25/12/11" as "December 25th 2011".
	 * 
	 * Note that, if you do not provide this offset, the validator will try to
	 * guess the offset. If the 2-digit year is higher than the current year,
	 * then 1900 will be used. In any other case, 2000 will be used.
	 * 
	 * This offset will be completely ignored if no 2-digit year is foreseen in
	 * the date's pattern.
	 * 
	 * @access public
	 * @var string
	 */
	const OFFSET_YEAR_2DIGITS = 'offset-year-2digits';
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// Get the format against which the date is to be validated:
		$format = $this->_getCriteriaValue(self::FORMAT, 'Y-m-d');
		
		// Define the special characters in the format. For each character, we
		// define a piece of regular expression for matching against the provided
		// subject.
		$chars = array(
			'y' => '([0-9]{2})\\syear',       // Year (2 digits)
			'Y' => '([0-9]{4})\\year',      // Year (4 digits)
			'm' => '([0-9]{1,2})\\month',   // Month (2 digits; leading zero)
			'd' => '([0-9]{1,2})\\day',     // Day (2 digits; leading zero)
			'H' => '([0-9]{1,2})\\hour',    // Hour (2 digits; leading zero)
			'i' => '([0-9]{1,2})\\minutes', // Minuts (2 digits; leading zero)
			's' => '([0-9]{1,2})\\seconds', // Seconds (2 digits; leading zero)
			'U' => '([0-9]{10})\\timestamp' // UNIX Timestamp (10 digits)
		);
		
		// First, we escape the provided format, so this format does not create
		// syntax errors when ran as a regular expression:
		$format = preg_quote($format, '/');
		
		// Then, we replace the special characters by the corresponding pieces
		// of regular expression. Note that we also add delimiters for the pattern
		$expression = '/^' . strtr($format, $chars) . '$/';
		
		// We'll look for pointers in the expression, in order to identify the
		// position of date elements in the pattern.
		$match = array();
		preg_match_all('/\\year|month|day|hour|minutes|seconds|timestamp|syear/', $expression, $match);
		$varIndexes = array();
		
		// For each of the pointers encountered:
		foreach($match[0] as $i => $currentMatch) {
			// Store the position of the date element in the pattern:
			$varIndexes[(string) $currentMatch] = $i + 1;
			
			// Remove the pointer from the regular expression
			$expression = str_replace('\\' . $currentMatch, '', $expression);
		}
		
		// Try to match against the subject
		if(preg_match($expression, $subject, $match)) {
			// We have a match! This means that the subject has passed the first
			// test, and that the expected format has been matched. However, this
			// does not mean that the subject represents a valid date. We'll check
			// for a valid date now. For each of the date elements we have matched
			// in the subject:
			$elements = array();
			foreach($varIndexes as $element => $index) {
				// Get the matched date element from the subject:
				$elements[$element] = $match[$index];
			}
			
			// As an absolute minimum, we expect day and month to be provided in
			// the subject. So, if day or month is missing:
			if(! array_key_exists('day', $elements) || ! array_key_exists('month', $elements)) {
				// Then, the date is no longer valid. We return FALSE;
				return FALSE;
			}
			
			// If the year is missing in the subject:
			if(! array_key_exists('year', $elements)) {
				// Then, we'll default to
				$elements['year'] = array_key_exists('syear', $elements)
					// The short year (2 digits)
					? ($elements['syear'] > M_Date::getCurrentYear() - 2000)
						? $elements['syear'] + (int) $this->_getCriteriaValue(self::OFFSET_YEAR_2DIGITS, 1900)
						: $elements['syear'] + (int) $this->_getCriteriaValue(self::OFFSET_YEAR_2DIGITS, 2000)
					// or the current year, if none provided
					: M_Date::getCurrentYear();
			}
			
			// The month must be a number in the range of 1-12
			if($elements['month'] < 1 || $elements['month'] > 12) {
				// If that is not the case, then the date is no longer valid
				return FALSE;
			}
			
			// In order to check whether or not the date is an existing date,
			// we use the checkdate() function with the date elements extracted.
			// If the date elements pass this test:
			if(checkdate($elements['month'], $elements['day'], $elements['year'])) {
				// Then, finally, we assume that the subject is a valid date.
				return TRUE;
			}
		}
		
		// If we are still here, then the subject did not match the expected
		// date format. In that case, we return FALSE:
		return FALSE;
	}
}