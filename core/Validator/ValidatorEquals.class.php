<?php
class M_ValidatorEquals extends M_Validator {
	const VALUE = 'value';
	public function check($subject) {
		return ($subject == $this->_getCriteriaValue(self::VALUE, NULL, TRUE));
	}
}