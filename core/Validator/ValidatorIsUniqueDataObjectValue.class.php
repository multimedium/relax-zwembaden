<?php
/**
 * M_ValidatorIsUniqueDataObjectValue
 * 
 * M_ValidatorIsUniqueDataObjectValue, a subclass of {@link M_Validator}, is used 
 * to check if a value can be considered a unique value in a given data object's
 * storage space.
 * 
 * Typically, this validator is used to check whether or not an email address is
 * already in use, or a username, etc...
 * 
 * @package Core
 */
class M_ValidatorIsUniqueDataObjectValue extends M_Validator {
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to set the Module ID as validator
	 * criteria. The Module ID is optional, and is used to load the data object
	 * classes in the application
	 * 
	 * NOTE:
	 * This criteria is only required, if no Data Object Mapper is provided with 
	 * the criteria {@link M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_MAPPER}
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsUniqueDataObjectValue;
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::MODULE_ID, 'client');
	 * </code>
	 * 
	 * @see M_Validator::setCriteria()
	 * @see M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT
	 * @see M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_FIELD
	 * @var string
	 */
	const MODULE_ID = 'MODULE_ID';
	
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to set the Data Object Name as validator
	 * criteria. The Data Object Name is required, and is used to load the data 
	 * object classes in the application.
	 * 
	 * NOTE:
	 * This criteria is only required, if no Data Object Mapper is provided with 
	 * the criteria {@link M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_MAPPER}
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsUniqueDataObjectValue;
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::MODULE_ID, 'client');
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT, 'Client');
	 * </code>
	 * 
	 * @see M_Validator::setCriteria()
	 * @see M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_FIELD
	 * @var string
	 */
	const DATA_OBJECT = 'DATA_OBJECT';
	
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to set the Data Object Mapper as validator
	 * criteria. The Data Object Mapper is required, and is used to fetch the data 
	 * object.
	 * 
	 * NOTE:
	 * This criteria is only required, if no Module ID and Data Object Name is 
	 * provided with the criteria {@link M_ValidatorIsUniqueDataObjectValue::MODULE_ID}
	 * and {@link M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT}
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsUniqueDataObjectValue;
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::MODULE_ID, 'client');
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT, 'Client');
	 * </code>
	 * 
	 * Rewrite Example 1 as following, with a mapper:
	 * 
	 * Example 2
	 * <code>
	 *    $validator = new M_ValidatorIsUniqueDataObjectValue;
	 *    $validator->setCriteria(
	 *       M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_MAPPER,
	 *       M_Loader::getDataObjectMapper('Client', 'client')
	 *    );
	 * </code>
	 * 
	 * @see M_Validator::setCriteria()
	 * @see M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_FIELD
	 * @var string
	 */
	const DATA_OBJECT_MAPPER = 'DATA_OBJECT_MAPPER';
	
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to set the Data Object Field as validator
	 * criteria. The Data Object Field is required, and is used to compare the
	 * validated subject against.
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsUniqueDataObjectValue;
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::MODULE_ID, 'client');
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT, 'Client');
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_FIELD, 'email');
	 * </code>
	 * 
	 * @see M_Validator::setCriteria()
	 * @var string
	 */
	const DATA_OBJECT_FIELD = 'DATA_OBJECT_FIELD';
	
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to set the Data Object ID as validator
	 * criteria. The Data Object ID is optional, and is used to leave a specific
	 * data object out of the comparison, so it is ignored.
	 * 
	 * Example 1
	 * <code>
	 *    $client = M_Loader
	 *       ::getDataObjectMapper('Client', 'client')
	 *       ->getRandom();
	 *    
	 *    $validator = new M_ValidatorIsUniqueDataObjectValue;
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::MODULE_ID, 'client');
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT, 'Client');
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_FIELD, 'email');
	 *    $validator->setCriteria(M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_IGNORE_ID, $client->getId());
	 *    
	 *    M_Debug::printr($validator->check($client->getEmail()));
	 * </code>
	 * 
	 * NOTE:
	 * This criteria is unused, if a data object mapper has been provided with
	 * criteria {@link M_ValidatorIsUniqueDataObjectValue::DATA_OBJECT_MAPPER}!
	 * 
	 * @see M_Validator::setCriteria()
	 * @var string
	 */
	const DATA_OBJECT_IGNORE_ID = 'DATA_OBJECT_IGNORE_ID';
	
	/**
	 * Check subject
	 *
	 * @access public
	 * @param mixed $subject
	 * 		The value to be checked
	 * @return boolean $flag
	 * 		Returns TRUE if the value is unique (that is, if no matching data object
	 * 		can be found with the value), FALSE if not
	 */
	public function check($subject) {
		// Try to get the data object mapper
		$dataObjectMapper = $this->_getCriteriaValue(self::DATA_OBJECT_MAPPER, NULL);
		
		// If a data object mapper has not been provided (or it is not of the 
		// expected format)
		if(! ($dataObjectMapper && M_Helper::isInstanceOf($dataObjectMapper, 'M_DataObjectMapper'))) {
			// Load the mapper that looks up data objects:
			$dataObjectMapper = M_Loader
				::getDataObjectMapper(
					// With the provided data object name
					$this->_getCriteriaValue(self::DATA_OBJECT, NULL, TRUE),
					// With the Module ID (if any)
					$this->_getCriteriaValue(self::MODULE_ID, NULL)
				);
		}
		
		// Add a filter to the mapper:
		$dataObjectMapper->addFilter(
			// Where...
			new M_DbQueryFilterWhere(
				// The provided field name...
				$this->_getCriteriaValue(self::DATA_OBJECT_FIELD, NULL, TRUE),
				// ... equals the provided value
				$subject
			)
		);
		
		// Ignore given data object?
		$i = $this->_getCriteriaValue(self::DATA_OBJECT_IGNORE_ID, NULL);
		
		// If so, 
		if($i) {
			// We add another filter to the mapper
			$dataObjectMapper->addFilter(
				// Where...
				new M_DbQueryFilterWhere(
					// The ID...
					'id',
					// ...is not the provided ignored one
					$i,
					M_DbQueryFilterWhere::NEQ
				)
			);
		}
		
		// With the mapper we have prepared, we ask for a data object that matches the
		// provided search value. If a data object has been found, we return FALSE. 
		// This means that the value is not unique
		if($dataObjectMapper->getOne()) {
			// Not unique: return invalid result
			return FALSE;
		}
		
		// If we are still here, it means that we have not found a data object.
		// So, we can say that the value is unique => return TRUE
		return TRUE;
	}
}