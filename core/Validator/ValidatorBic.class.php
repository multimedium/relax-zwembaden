<?php
/**
 * M_ValidatorBic
 *
 * M_ValidatorBic, a subclass of {@link M_Validator} is used to validate
 * a given BIC code. For more info, read the following docs on wikipedia:
 * - {@see http://en.wikipedia.org/wiki/ISO_9362}
 * - {@see http://nl.wikipedia.org/wiki/Bank_Identifier_Code}
 * 
 * @author Tim
 * @package Core
 */
class M_ValidatorBic extends M_Validator {
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Evaluate the subject
	 * 
	 * Will check the provided BIC code, returning TRUE if it is a valid one,
	 * or FALSE otherwise.
	 * 
	 * NOTE: the provided BIC code is cleaned up before validation, removing
	 * all characters that are more or less logical, and making it uppercase.
	 * 
	 * @see http://en.wikipedia.org/wiki/ISO_9362
	 * @see http://nl.wikipedia.org/wiki/Bank_Identifier_Code
	 * @see MI_Validator::check()
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// First of all, remove all junk from the subject and cast it to
		// uppercase
		$subject = strtoupper(str_replace(array(' ', '-', '_', ',', '.'), '', trim($subject)));

		// Next, we have to check whether the provided BIC code is of the
		// correct length and format. The code always begins with 6 letters,
		// followed by either 2 or 5 alphanumeric characters
		if(preg_match('/^[A-Z]{6}([A-Z0-9]{2}|[A-Z0-9]{5})$/', $subject) != 1) {
			// Set the error message
			$this->_setErrorMessage(t('Invalid BIC code'));
			
			// Return FALSE, indicating the BIC code is invalid
			return FALSE;
		}
		
		// Return TRUE if we are still here, as the BIC code is valid
		return TRUE;
	}
	
	/**
	 * Get Example Format
	 * 
	 * Will return an example string of how a BIC number should look for
	 * the provided country. For example, for Belgium:
	 * 
	 * <code>
	 *	ABCDBEBA
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param string $isoCode
	 * @return string
	 */
	public static function getExampleFormat($isoCode = NULL) {
		// Check whether the country is known
		if(! $isoCode) {
			// No, so default to Belgium
			$isoCode = 'BE';
		}
		
		// Return the example
		return 'ABCD' . $isoCode . 'BA';
	}
}