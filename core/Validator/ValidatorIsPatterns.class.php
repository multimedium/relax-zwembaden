<?php
/**
 * M_ValidatorIsPatterns
 * 
 * @package Core
 */
class M_ValidatorIsPatterns extends M_Validator {

	/* -- CONSTANTS -- */

	/**
	 * Validator Criteria Constant
	 * 
	 * With this constant, you can set a validator criteria. More specifically, 
	 * you can use this constant to set a collection of patterns - in pcre 
	 * syntax - with which the subject is to be validated by this validator.
	 * 
	 * Note that, if you provide with a collection of patterns, the subject will
	 * be evaluated as valid, if it matches any of the provided expressions. If
	 * the subject does not match any, the result of validation will be FALSE.
	 *
	 * @access public
	 * @var string
	 */
	const PATTERNS = 'patterns';

	/**
	 * Validator Criteria Constant
	 * 
	 * With this constant, you can set a validator criteria. More specifically, 
	 * you can use this constant to set the logical operator with which patterns
	 * are to be matched.
	 *
	 * @access public
	 * @var string
	 */
	const LOGICAL_OPERATOR = 'OR';

	/* -- MI_VALIDATOR -- */
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface, {@link M_Validator} 
	 * subclasses must implement the check() method. This method evaluates a 
	 * given subject. This method returns TRUE if the test subject is considered 
	 * "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// Get the patterns to match against:
		$patterns = $this->_getCriteriaValue(self::PATTERNS, array(), TRUE);
		
		// The collection of patterns must be an iterator:
		if(! M_Helper::isIterator($patterns)) {
			// If not the case, we throw an exception
			throw new M_Exception(sprintf(
				'Cannot perform validation. Expecting an iterator of patterns; ' . 
				'encountered the value %s instead',
				var_export($patterns, true)
			));
		}
		
		// Get the logical operator (OR, by default)
		$operator = strtoupper($this->_getCriteriaValue(self::LOGICAL_OPERATOR, 'OR'));
		
		// Only AND and OR are accepted as logical operators
		if($operator != 'AND' && $operator != 'OR') {
			// If not the case, we throw an exception
			throw new M_Exception(sprintf(
				'Cannot perform validation. Expecting an logical operator AND or ' . 
				'OR; encountered the value %s instead',
				var_export($operator, true)
			));
		}
		
		// For each of the patterns:
		foreach($patterns as $pattern) {
			// Get the result of the current pattern, by matching it against the
			// subject:
			$c = (preg_match($pattern, $subject) == 1);
			
			// If the logical operator is AND
			if($operator == 'AND') {
				// Then, all patterns should match! So, if the outcome of the 
				// current match is FALSE, then the validator should also return
				// FALSE:
				if(! $c) {
					return FALSE;
				}
			}
			// If the logical operator is OR
			else {
				// Then, any match is good. So, if the outcome of the current
				// match is TRUE, then the validator should also return TRUE:
				if($c) {
					return TRUE;
				}
			}
		}
		
		// Return FALSE, if still here
		return FALSE;
	}
}