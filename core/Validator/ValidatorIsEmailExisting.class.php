<?php
/**
 * M_ValidatorIsEmailExisting
 * 
 * M_ValidatorIsEmailExisting, a subclass of {@link M_Validator} is used
 * to validate an email address, and to make sure that the email address actually
 * exists.
 * 
 * NOTE:
 * In order to check whether or not the email address exists, this validator will
 * send out some SMTP commands. No email is sent to the provided address.
 * 
 * @package Core
 */
class M_ValidatorIsEmailExisting extends M_Validator {
	/**
	 * Evaluate the subject
	 * 
	 * NOTE:
	 * You can validate multiple addresses, by providing with an email recipient
	 * string.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// Try...
		try {
			// In order to send SMTP commands, we will use the host name of
			// the application (NOTE: "localhost" will not work on some SMTP
			// servers, so we default to multimedium if that is the case)
			$senderDomain = M_Request::getHost() == 'localhost' ? 'multimedium.be' : M_Request::getHost();
			
			// We separate users from domain names. This way, we group all connections
			// to the same domain in one call, which is obviously better :)
			$domains = array();
			
			// For each of the email recipients in the provided string
			foreach(array_keys(M_MailHelper::getRecipientsFromString($subject)->getArrayCopy()) as $email) {
				// Separate the username from the domain:
				list($user, $domain) = explode('@', $email);
				
				// Check if a user was already provided for this domain:
				if(! isset($domains[$domain])) {
					// If not, we create a group for this domain:
					$domains[$domain] = array();
				}
				
				// Store the user in the domain group:
				$domains[$domain][] = $user;
			}
			
			// For each of the domains we need to check:
			foreach($domains as $domain => $users) {
				// We perform the check on the current domain:
				$rs = $this->_checkUsersAtDomain($domain, $users, $senderDomain);
				
				// We loop through the email addresses that have been checked:
				foreach($rs as $email => $result) {
					// If the email address has been validated as NOT existing:
					if(! $result) {
						// Set the error message
						/*$this->_setErrorMessage(
							t(
								'The email address @email does not exist.',
								array(
									'@email' => $email
								)
							)
						);*/
						
						// Then, the result of the validator is FALSE!
						return FALSE;
					}
				}
			}
			
			// Return SUCCESS:
			return TRUE;
		}
		// If we fail
		catch(Exception $e) {
		}
		
		// Return FALSE if still here
		return FALSE;
	}
	
	/**
	 * Check users at domain
	 * 
	 * @param string $domain
	 * 		The domain name that should be checked
	 * @param array $users
	 * 		The collection of usernames that should be checked, for the domain
	 * @param string $senderDomain
	 * 		Host name of sender
	 * @param string $senderUser
	 * 		user of sender
	 * @param integer $port
	 * 		The SMTP Port to connect with
	 * @param integer $maxConnectionTime
	 * 		Maximum connection time to an MTA, expressed in a number of seconds
	 * @param integer $maxReadTime
	 * 		Maximum read time (timeout period on the stream)
	 * @return array $flags
	 * 		Will return an associative array with email addresses as array keys. 
	 * 		Each of the values is a boolean, indicating the result of validation
	 * 		for the corresponding email address.
	 */
	public function _checkUsersAtDomain($domain, array $users, $senderDomain = 'multimedium.be', $senderUser = 'info', $port = 25, $maxConnectionTime = 30, $maxReadTime = 5) {
		// The output variable
		$out = array();

		// 1: retrieve SMTP Server via MX query on domain
		$hosts = dns_get_record($domain, DNS_MX);
		
		// 2: if the MX check returned no results, we try with an A record
		if (! $hosts ) {
			$hosts = dns_get_record($domain, DNS_A);
		}

		//check if at least one dns-record has been found
		if (! $hosts) {
			return $this->_getFailureOutputForAllUsers($domain, $users);
		}
		
		// parse out the actual server names == the 'target' in the hosts array
		foreach ($hosts as $host) {
			$servers[] = isset($host['target']) ? $host['target'] : $domain;
		}
				
		// We calculate the time we can spend on connecting with each of the hosts.
		// This is, of course, the total time divided by the number of hosts that
		// we have retrieved earlier
		$timeout = $maxConnectionTime / count($servers);

		// For each of the hosts
		foreach ($servers as $host) {
			// We connect to the SMTP server.
			// $errNo holds the system level error number that occurred 
			// in the system-level connect() call:
			$errNo = NULL;
			
			// $errStr is very much alike, but stores the error message as a string.
			$errStr = NULL;
			
			// In order to connect to the SMTP server, we open a domain socket 
			// connection with fsockopen()
			$socket = fsockopen($host, $port, $errNo, $errStr, ( float ) $timeout);
			
			// If we have succeeded in connecting to the SMTP:
			if($socket) {
				// Set timeout period on a stream
				stream_set_timeout($socket, $maxReadTime);
				// We break out of the loop, now that we have established a 
				// connection
				break;
			}
		}

		
		// If we managed to establish a connection with the SMTP server
		if($socket) {
			// Check if we get response from the SMTP server:
			$reply = fread($socket, 2082);
						
			// We run a regular expression, in order to extract the response
			// code we got from the server
			$matches = array();
			preg_match('/^([0-9]{3}) /ims', $reply, $matches);
			
			// The code should be 220!
			$code = isset($matches[1]) ? $matches[1] : '';
			
			if ($code != '220') {
				// If that is not the case, we return FALSE as validation result:
				return $this->_getFailureOutputForAllUsers($domain, $users);
			}
			
			// We send the HELO command to the SMTP server:
			$this->_sendSmtpCommand($socket, 'HELO smtp.' . $senderDomain);
			
			// We send the command with sender info: 
			$this->_sendSmtpCommand($socket, 'MAIL FROM: <' . $senderUser . '@' . $senderDomain . '>');
			
			// Now, we go through all of the users that should be validated for
			// this domain: 
			foreach($users as $user) {
				// We check the response we get, when we ask for the recipient
				// address for the current user:
				$reply = $this->_sendSmtpCommand($socket, 'RCPT TO: <' . urlencode($user) . '@' . $domain . '>');
				
				// We get the code and the corresponding message from response
				preg_match('/^([0-9]{3}) /ims', $reply, $matches);
				$code = isset($matches[1]) ? $matches[1] : '';
								
				// We received 250, which means that the email address was 
				// accepted by the SMTP Server
				if($code == '250') {
					$out[$user . '@' . $domain] = TRUE;
				}
				// We received 451, which means that the email address was 
				// greylisted (or some temporary error occured on the MTA). In
				// that case, we assume everything is OK  
				elseif ($code == '451' || $code == '452') {
					$out[$user . '@' . $domain] = TRUE;
				}
				// In any other case, the result of validation is FAILURE!
				else {
					$out[$user . '@' . $domain] = FALSE;
				}
			}
			
			// Quit the connection with the server
			$this->_sendSmtpCommand($socket, 'quit');
			
			// Close socket  
			fclose($socket);
		}
		
		// Return the output
		return $out;
	}

	/**
	 * Get the output which will be sent if all validation fails
	 *
	 * In some cases (when no mx-records are found, we cannot connect to smtp, ...)
	 * we need to send an output for all users. This output contains FALSE for
	 * every user which needs to be validated
	 *
	 * @author Ben Brughmans
	 * @param string $domain The domain which is validated
	 * @param array $users Every user which is validated
	 */
	protected function _getFailureOutputForAllUsers($domain, $users) {
		foreach($users as $user) { 
			$out[$user . '@' . $domain] = FALSE;
		}
		return $out;
	}
	
	/**
	 * Send SMTP command
	 * 
	 * Will send an SMTP COMMAND over the provided socket connection. Will return
	 * the reply it gets from the server
	 * 
	 * @access protected
	 * @param mixed $socket
	 * 		The socket resource, as returned by PHP's fsockopen()
	 * @param string $cmd
	 * 		The command to be sent to the SMTP server
	 * @return string $reply
	 * 		The server's reply message
	 */
	public function _sendSmtpCommand($socket, $cmd) {
		fwrite($socket, $cmd . "\r\n");
		return fread($socket, 2082);
	}
}