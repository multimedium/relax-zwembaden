<?php
/**
 * M_ValidatorQueue
 *
 * The {@link M_ValidatorQueue} is a special validator: it is a queue of validators.
 * In other words, this validator can contain a collection of {@link MI_Validator}
 * objects in order to perform a validation.
 *
 * @package Core
 */
class M_ValidatorQueue extends M_Validator {

	/* -- PROPERTIES -- */

	/**
	 * Validators
	 *
	 * This property stores the collection of validators that is to be used
	 * by this object.
	 *
	 * @access private
	 * @var array
	 */
	private $_validators = array();

	/* -- SETTERS -- */

	/**
	 * Add validator
	 *
	 * @access public
	 * @param MI_Validator $validator
	 * @return M_ValidatorQueue $validator
	 *		Returns itself, for a fluent programming interface
	 */
	public function addValidator(MI_Validator $validator) {
		$this->_validators[] = $validator;
	}

	/**
	 * Remove all contained validators
	 *
	 * @access public
	 * @return M_ValidatorQueue $validator
	 *		Returns itself, for a fluent programming interface
	 */
	public function clearValidators() {
		$this->_validators = array();
	}

	/* -- GETTERS -- */

	/**
	 * Evaluate the subject
	 *
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method. This
	 * method evaluates a given subject and returns TRUE if the test subject is
	 * considered "valid", FALSE if not.
	 *
	 * @access public
	 * @param mixed $subject
	 * 		The subject to be validated
	 * @return boolean $flag
	 *		Returns TRUE on success, FALSE on failure
	 */
	public function check($subject) {
		// For each of the validators:
		foreach($this->_validators as $validator) {
			/* @var $validator MI_Validator */
			// Evaluate the subject with the current validator:
			if(! $validator->check($subject)) {
				// If the subject is not considered "valid", then we set the
				// error code + message:
				$this->_setErrorCode($validator->getErrorCode());
				$this->_setErrorCriteria($validator->getErrorCriteria());
				$this->_setErrorMessage($validator->getErrorMessage());

				// And, we return FALSE in order to indicate an "invalid" test
				// subject:
				return FALSE;
			}
		}

		// We return TRUE (success) if still here:
		return TRUE;
	}
}