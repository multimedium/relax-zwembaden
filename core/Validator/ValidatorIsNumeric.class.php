<?php
/**
 * M_ValidatorIsNumeric
 * 
 * M_ValidatorIsNumeric, a subclass of {@link M_Validator}, is used
 * to check if a subject value is numeric.
 * 
 * M_ValidatorIsNumeric will use {@link M_Number::constructWithString()}
 * to construct a number with the given subject. By using 
 * {@link M_Number}, the validator is made sensitive to locale-dependent 
 * number formatting.
 * 
 * @package Core
 */
class M_ValidatorIsNumeric extends M_Validator {

	/* -- CONSTANTS -- */

	/**
	 * Validator Criteria Constant
	 *
	 * This constant is used to address the minimum value as validator criteria.
	 * Read {@link M_Validator::setCriteria()} to learn more about validator
	 * criteria.
	 *
	 * Example 1
	 * <code>
	 *    // Set the minimum value to 10. This means that the introduced number
	 *    // must be 10 or higher
	 *    $validator = new M_ValidatorIsNumeric;
	 *    $validator->setCriteria(M_ValidatorIsNumeric::MIN, 10);
	 * </code>
	 *
	 * @access public
	 * @var string
	 */
	const MIN = 'min';

	/**
	 * Validator Criteria Constant
	 *
	 * This constant is used to address the maximum number of words as
	 * validator criteria. Read {@link M_Validator::setCriteria()} to
	 * learn more about validator criteria.
	 *
	 * Example 1
	 * <code>
	 *    // Set the maximum value to 50. This means that the introduced number
	 *    // must be 50 or lower
	 *    $validator = new M_ValidatorIsNumeric;
	 *    $validator->setCriteria(M_ValidatorIsNumeric::MAX, 50);
	 * </code>
	 *
	 * @access public
	 * @var string
	 */
	const MAX = 'max';

	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the locale name as validator
	 * criteria. Read {@link M_Validator::setCriteria()} to learn more
	 * about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorIsNumeric;
	 *    $validator->setCriteria(M_ValidatorIsNumeric::LOCALE, 'en');
	 * </code>
	 * 
	 * This criteria sets the locale that should be used to evaluate the 
	 * string as a numeric value. The name of the locale will be passed
	 * into {@link M_Number::constructWithString()} in order to try
	 * and construct a number.
	 * 
	 * @see M_Locale
	 * @see M_Number::constructWithString()
	 * @access public
	 * @var string
	 */
	const LOCALE = 'locale';
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		try {
			if ((string) (double) $subject != $subject) {
				$number =  M_Number::constructWithString($subject, $this->_getCriteriaValue(self::LOCALE, NULL));
			} else {
				$number = new M_Number((float) $subject);
			}

			// get validator criteria:
			$min = $this->_getCriteriaValue(self::MIN, null);
			$max = $this->_getCriteriaValue(self::MAX, null);

			// If the number is lower than the minimum value:
			if(!is_null($min) && $number->getNumber() < $min) {
				// Then, we set the error message:
				$this->_setErrorMessage(
					// ... to the one that has been provided for this criteria
					$this->_getCriteriaErrorMessage(
						self::MIN,
						t('The number should be equal to, or greater than @number', array(
							'@number' => $min
						))
					)
				);

				// Return FALSE as result
				return FALSE;
			}

			// If the number is higher than the maximum
			if(!is_null($max) && $max > 0 && $number->getNumber() > $max) {
				// Set the error message...
				$this->_setErrorMessage(
					// ... to the one that has been provided for this criteria
					$this->_getCriteriaErrorMessage(
						self::MAX,
						t('The number cannot be greater than @number', array(
							'@number' => $max
						))
					)
				);

				// Return FALSE as result
				return FALSE;
			}
			
			// Return success, if the number got constructed and adheres to the
			// defined minimum and maximum
			return TRUE;
		}
		// If failed to construct a number:
		catch(Exception $e) {
			// Is this really a validator error? If we set this message here, it
			// would be shown in forms...
			// $this->setErrorCode($e->getCode());
			// $this->setErrorMessage($e->getMessage());
		}
		
		// Return FALSE if still here
		return FALSE;
	}
}
?>