<?php
/**
 * M_ValidatorIsCount class
 * 
 * M_ValidatorIsCount, a subclass of {@link M_Validator} is used to validate an 
 * array (or iterator) against a minimum and maximum number of array elements.
 * 
 * @package Core
 */
class M_ValidatorIsCount extends M_Validator {
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the minimum number of array (or iterator) 
	 * elements as validator criteria. Read {@link M_Validator::setCriteria()} to 
	 * learn more about validator criteria.
	 */
	const MIN = 'min';
	
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the maximum number of array (or iterator) 
	 * elements as validator criteria. Read {@link M_Validator::setCriteria()} to 
	 * learn more about validator criteria.
	 */
	const MAX = 'max';
	
	/**
	 * Evaluate the subject
	 * 
	 * @see MI_Validator::check()
	 * @access public
	 * @param mixed $subject
	 * 		The subject to be validated
	 * @return boolean $flag
	 * 		Returns TRUE if the subject is valid, FALSE if not
	 */
	public function check($subject) {
		// If the subject is not an iterator:
		if(! M_Helper::isIterator($subject)) {
			// Then, we return FALSE
			return FALSE;
		}
		
		// Get the count of the provided iterator
		$num = count($subject);
		
		// Get the provided validator criteria:
		$min = $this->_getCriteriaValue(self::MIN, 0);
		$max = $this->_getCriteriaValue(self::MAX, 0);
		
		// If less than the minimum:
		if($min > 0 && $num < $min) {
			// Set the error message...
			$this->_setErrorMessage(
				// ... to the one that has been provided for this criteria
				$this->_getCriteriaErrorMessage(
					self::MIN,
					t('At least @number elements should be provided', array(
						'@number' => $min
					))
				)
			);
			
			// Return FALSE as result
			return FALSE;
		}
		
		// If more than the maximum
		if($max > 0 && $num > $max) {
			// Set the error message...
			$this->_setErrorMessage(
				// ... to the one that has been provided for this criteria
				$this->_getCriteriaErrorMessage(
					self::MAX,
					t('A maximum of @number elements is allowed', array(
						'@number' => $max
					))
				)
			);
			
			// Return FALSE as result
			return FALSE;
		}
		
		// If still here, we return success (value is considered valid)
		return TRUE;
	}
}