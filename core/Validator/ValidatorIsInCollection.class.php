<?php
/**
 * M_ValidatorIsInCollection class
 * 
 * @package Core
 */
class M_ValidatorIsInCollection extends M_Validator {

	/* -- CONSTANTS -- */

	/**
	 * Validator Criteria Constant
	 * 
	 * @access public
	 * @var string
	 */
	const COLLECTION = 'collection';

	/* -- MI_VALIDATOR -- */
	
	/**
	 * Evaluate the subject
	 * 
	 * @see MI_Validator::check()
	 * @access public
	 * @param mixed $subject
	 * 		The subject to be validated
	 * @return boolean $flag
	 * 		Returns TRUE if the subject is valid, FALSE if not
	 */
	public function check($subject) {
		// Get the provided validator criteria:
		$collection = $this->_getCriteriaValue(self::COLLECTION, array());

		// If the collection is not an iteratable collection:
		if(! M_Helper::isIterator($collection)) {
			// Then, we return FALSE
			return FALSE;
		}

		// If the provided subject is an iterator
		if(M_Helper::isIterator($subject)) {
			// For each of the elements in the test subject:
			foreach($subject as $element) {
				// check to see if the current element is present in the collection
				$isPresent = $this->_checkSubject($element, $collection);

				// If not
				if(! $isPresent) {
					// Then, we return FALSE
					return FALSE;
				}
			}

			// Return TRUE, if still here
			return TRUE;
		}
		// if the subject is not an iterator
		else {
			// Then, check to see if the subject is present in the collection
			return $this->_checkSubject($subject, $collection);
		}
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Check subject
	 * 
	 * @param string $subject
	 * @param array|ArrayIterator $collection
	 * @return bool
	 */
	protected function _checkSubject($subject, $collection) {
		// For each of the elements in the collection
		foreach($collection as $element) {
			// If the current element matches the provided test subject
			if($element == $subject) {
				// Then, we return TRUE:
				return TRUE;
			}
		}

		// If still here, we return failure
		return FALSE;
	}
}