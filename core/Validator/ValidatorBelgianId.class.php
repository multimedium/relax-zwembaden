<?php
/**
 * M_ValidatorBelgianId
 * 
 * M_ValidatorBelgianId, a subclass of {@link M_Validator} is used
 * to validate a Belgian Identity Card Number.
 * 
 * Extracted from official source:
 * Om de juistheid van een rijksregisternummer te controleren, moet de 
 * check digit van dit nummer berekend worden. De check digit bestaat 
 * uit 2 cijfers. Dit getal is het complement van 97 van modulo 97 van 
 * het getal gevormd door:
 * 
 * - hetzij de eerste 9 cijfers van het rijksregisternummer indien de 
 *   persoon vóór 1/1/2000 geboren is (zie voorbeeld 1),
 * - hetzij het cijfer 2 gevolgd door de eerste 9 cijfers van het 
 *   rijksregisternummer indien de persoon na 31/12/1999 geboren is 
 *   (zie voorbeeld 2).
 * 
 * NOTA:
 * Modulo 97 van een getal is de rest van de deling van het getal 
 * door 97.
 * 
 * Example 1:
 * <code>
 *   RN = 72020290081: 97 - (modulo 97 van 720202900) = 97 - 16 = 81
 * </code>
 * 
 * Example 2:
 * <code>
 *   RN = 00012556777: 97 - (modulo 97 van 2000125567) = 97 - 20 = 77
 * </code>
 * 
 * @package Core
 */
class M_ValidatorBelgianId extends M_Validator {
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the year of birth as validator
	 * criteria. Read {@link M_Validator::setCriteria()} to learn more
	 * about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    $validator = new M_ValidatorBelgianId;
	 *    $validator->setCriteria(M_ValidatorBelgianId::YEAR_OF_BIRTH, 2008);
	 * </code>
	 * 
	 * This criteria sets the year of birth that should be used to
	 * evaluate the tested identity card number against.
	 */
	const YEAR_OF_BIRTH = 'yearOfBirth';
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		$match = array();
		if(preg_match('/^([0-9]{9})([0-9]{2})$/', $subject, $match)) {
			$base = (int) ($this->_getCriteriaValue(self::YEAR_OF_BIRTH, 1900) < 2000 ? $match[1] : '2' . $match[1]);
			if((97 - ($base % 97)) == $match[2]) {
				return TRUE;
			}
		}
		return FALSE;
	}
}
?>