<?php
/**
 * M_ValidatorIsPhoneNumber
 *
 * M_ValidatorIsPhoneNumber, a subclass of {@link M_Validator} is used
 * to validate a given phone number input.
 *
 * @author Tim Segers
 * @package Core
 */
class M_ValidatorIsPhoneNumber extends M_Validator {

	/* -- PROPERTIES -- */

	/**
	 * Pattern - National
	 *
	 * This pattern will be checked against to determine wheter
	 * the number is a valid national number or not (e.g. without land number).
	 *
	 * @access private
	 * @var string
	 */
	private $_national = '/^0([0-9]{8,10})$/';

	/**
	 * Pattern - International
	 *
	 * This pattern will be checked against to determine wheter the number
	 * is a valid international number or not (e.g. with land number)
	 *
	 * @access private
	 * @var string
	 */
	private $_international = '/^(\+|00)([0-9]{2})([0-9]{8,10})$/';

	/* -- MI_VALIDATOR -- */

	/**
	 * Evaluate the subject
	 *
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 *
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {

		// First of all, strip all whitespace from the phone number
		$subject = preg_replace('/[\s\.\/\-]+/', '', $subject);

		// Next, also strip off weird characters
		$subject = str_replace(array(',', '/', '.', ' ', '(', ')', '-'), '', $subject);

		// First of all, check the length. In Belgium and The Netherlands the
		// length can vary between minimum 8 (02 567 890) to maximum 14
		// characters (0031 232 23 45 67). For more info, check:
		// - http://taaladvies.net/taal/advies/tekst/52/
		// - http://taaladvies.net/taal/advies/tekst/53/
		// - http://nl.wikipedia.org/wiki/Lijst_van_Belgische_zonenummers
		// - http://nl.wikipedia.org/wiki/Lijst_van_Nederlandse_netnummers
		if(strlen($subject) < 8 || strlen($subject) > 14) return FALSE;

		// Create a new M_ValidatorIsPattern, which we will use to check the given phone number
		$validator = new M_ValidatorIsPattern();

		// First we will check if the number is a valid international number.
		// If it is, it's automatically a valid national number as well.

		// Set the pattern the validator should check against
		$validator->setCriteria(M_ValidatorIsPattern::PATTERN, $this->_international);

		// Check the given phone number against the validator
		if ($validator->check($subject)) {
			// If validation succeeded, return TRUE
			return TRUE;

		// If the phone number failed international format validation
		} else {
			// The number is not in a valid international format, so we check
			// it against a national format
			$validator->setCriteria(M_ValidatorIsPattern::PATTERN, $this->_national);

			// Check the given phone number against the validator
			if ($validator->check($subject)) {
				// If validation succeeded, return TRUE
				return TRUE;
			}
			// If we are still here it means the number is not in a correct
			// international format, nor in a correct national format, so we
			// return FALSE
			return FALSE;
		}
	}
}