<?php
/**
 * M_ValidatorIsLinkExisting
 *
 * M_ValidatorIsLinkExisting, a subclass of {@link M_Validator} is used
 * to validate a link, and to make sure that the link actually
 * exists.
 *
 * NOTE:
 * In order to check whether or not the linkexists, this validator will
 * be using {@link M_UriSession}
 *
 * @package Core
 */
class M_ValidatorIsLinkExisting extends M_Validator {
	/**
	 * Evaluate the subject
	 *
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// Try...
		try {

			// Generate new Uri
			$uri = new M_Uri($subject);

			// Get session
			$session = $uri->getSession();
			$session->setFollowRedirects(TRUE);
			$session->setAcceptanceOfAnyPeerCertificate();

			// First check if Uri existing
			$exists = $session->uriExists();
			
			if($exists) {
				 // See if the http-code indicates success
				 $code = $session->getHttpCode();
				 return (($code >= 200) && ($code < 400));
			}

			// If still here: return false
			return FALSE;
		}
		
		// If we fail
		catch(Exception $e) {
		}

		// Return FALSE if still here
		return FALSE;
	}
}