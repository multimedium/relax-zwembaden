<?php
/**
 * M_ValidatorIban
 *
 * M_ValidatorIban, a subclass of {@link M_Validator} is used to validate
 * a given IBAN number. The class supports all possible IBAN numbers from
 * all countries that employ them. For more info, read the docs on wikipedia:
 * {@see http://en.wikipedia.org/wiki/International_Bank_Account_Number}
 * 
 * @author Tim
 * @package Core
 */
class M_ValidatorIban extends M_Validator {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Countries
	 * 
	 * Stores the list of countries that employ IBAN numbers, with the
	 * corresponding length of the IBAN number. This length is used for
	 * validation purposes.
	 * 
	 * NOTE: the countries are defined by their standardized 2-character
	 * ISO codes.
	 * 
	 * NOTE: This list is copied from Wikipedia, at the following link:
	 * {@see http://en.wikipedia.org/wiki/International_Bank_Account_Number}
	 * 
	 * @static
	 * @access private
	 * @var array
	 */
	private static $_countries = array(
		'AL' => 28, // Albania
		'AD' => 24, // Andorra
		'AO' => 25, // Angola
		'AT' => 20, // Austria
		'AZ' => 28, // Azerbaijan
		'BH' => 22, // Bahrein
		'BE' => 16, // Belgium
		'BJ' => 28, // Benin
		'BA' => 20, // Bosnia and Herzegovina
		'BF' => 27, // Burkina Faso
		'BI' => 16, // Burundi
		'BR' => 29, // Brazil
		'BG' => 22, // Bulgaria
		'CM' => 27, // Cameroon
		'CV' => 25, // Cape Verde
		'CR' => 21, // Costa Rica
		'HR' => 21, // Croatia
		'CY' => 28, // Cyprus
		'CZ' => 24, // Czech Republic
		'DK' => 18, // Denmark
		'DO' => 28, // Dominican Republic
		'ES' => 20, // Estonia
		'FO' => 18, // Faroe Islands
		'FI' => 18, // Finland
		'FR' => 27, // France
		'GE' => 22, // Georgia
		'DE' => 22, // Germany
		'GI' => 23, // Gibraltar
		'GR' => 27, // Greece
		'GL' => 18, // Greenland
		'GT' => 28, // Guatemala
		'HU' => 28, // Hungary
		'IS' => 26, // Iceland
		'IR' => 26, // Iran
		'IE' => 22, // Ireland
		'IL' => 23, // Israel
		'IT' => 27, // Italy
		'CI' => 28, // Ivory Coast
		'KZ' => 20, // Kazakhstan
		'KW' => 30, // Kuwait
		'LV' => 21, // Latvia
		'LB' => 28, // Lebanon
		'LI' => 21, // Lichtenstein
		'LT' => 20, // Lithuania
		'LU' => 20, // Het land van de koekskes
		'MK' => 19, // Macedonia
		'MG' => 27, // Madagascar
		'ML' => 28, // Mali
		'MT' => 31, // Malta
		'MR' => 27, // Mauritania
		'MU' => 30, // Mauritius
		'MC' => 27, // Monaco
		'MD' => 24, // Moldova
		'ME' => 22, // Montenegro
		'MZ' => 25, // Mozambique
		'NL' => 18, // Netherlands
		'NO' => 15, // Norway
		'PK' => 24, // Pakistan
		'PS' => 29, // Palestina
		'PL' => 28, // Poland
		'PT' => 25, // Portugal
		'RO' => 24, // Romania
		'SM' => 27, // San Marino
		'SA' => 24, // Saudi Arabia
		'SN' => 28, // Senegal
		'RS' => 22, // Serbia
		'SK' => 24, // Slovakia
		'SI' => 19, // Slovenia
		'ES' => 24, // Spain
		'SE' => 24, // Sweden
		'CH' => 21, // Switzerland
		'TN' => 24, // Tunisia
		'TR' => 26, // Turkey
		'AE' => 23, // United Arab Emirates
		'GB' => 22, // United Kingdom
		'VG' => 24  // Virgin Islands
	);
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Evaluate the subject
	 * 
	 * Will check the provided IBAN code, returning TRUE if it is a valid one,
	 * or FALSE otherwise.
	 * 
	 * Validation itself is performed as follows:
	 * 
	 * "An IBAN is validated by converting it into an integer and performing
	 *  a basic mod-97 operation (as described in ISO 7064) on it. If the IBAN
	 *  is valid, the remainder equals 1. The algorithm of IBAN validation is
	 *  as follows:
	 * 
	 *  1. Check that the total IBAN length is correct as per the country. If
	 *     not, the IBAN is invalid.
	 *  2. Move the four initial characters to the end of the string.
	 *  3. Replace each letter in the string with two digits, thereby expanding
	 *     the string, where A = 10, B = 11, ..., Z = 35.
	 *	4. Interpret the string as a decimal integer and compute the remainder
	 *	   of that number on division by 97.
	 *  5. If the remainder is 1, the check digit test is passed."
	 * 
	 * For more info, read the docs on the links below and within the method
	 * itself.
	 * 
	 * NOTE: the provided IBAN number is cleaned up before validation, removing
	 * all characters that are more or less logical, and making it uppercase.
	 * 
	 * @see http://en.wikipedia.org/wiki/International_Bank_Account_Number
	 * @see MI_Validator::check()
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// First of all, remove all junk from the subject and cast it to
		// uppercase
		$subject = strtoupper(str_replace(array(' ', '-', '_', ',', '.'), '', trim($subject)));
		
		// Check the 4 first characters, which consist of 2 letters (the country
		// ISO code) followed by 2 numbers (the IBAN checksum). We will check
		// the exact length later on, once we have determined the country.
		if(preg_match('/^[A-Z]{2}[0-9]{2}[A-Z0-9]{0,}$/', $subject) != 1) {
			// Set the error message
			$this->_setErrorMessage(t('Invalid IBAN number'));
			
			// Return FALSE, indicating the IBAN code is invalid
			return FALSE;
		}

		// Next, we have to check whether the first 2 characters of the
		// provided IBAN number represent a valid ISO country code, which is
		// supported by the system. Fetch the code:
		$isoCode = substr($subject, 0, 2);
		
		// Check whether the code is supported by the system
		if(! array_key_exists($isoCode, self::$_countries)) {
			// Set the error message
			$this->_setErrorMessage(t(
				'Invalid IBAN number; the given number is from a ' .
				'non-existant or unsupported country'
			));
			
			// Return FALSE, indicating the IBAN code is invalid
			return FALSE;
		}
		
		// Next, make sure the provided number is of the correct length, and
		// only contains characters A-Z and 0-9
		$length = self::$_countries[$isoCode];
		if(preg_match('/^[A-Z0-9]{'. $length .'}$/', $subject) != 1) {
			// Set the error message
			$this->_setErrorMessage(t('Invalid IBAN number'));
			
			// Return FALSE, indicating the IBAN code is invalid
			return FALSE;
		}

		// The provided IBAN number is supported and has the correct format.
		// Next, we have to prepare the number so it can be properly validated.
		// First, move the 4 first characters to the end of the string
		$subject = substr($subject, 4) . substr($subject, 0, 4);

		// Then, replace all letters with an integer. A = 10, B = 11, ... Z = 35.
		// Create some working variables first:
		$temp = $subject;
		$subject = '';
		
		// Now, for every character:
		for($i = 0; $i < $length; $i++) {
			// If it is already numeric
			if(is_numeric($temp{$i})) {
				// Then simply append it to the string
				$subject .= (string) $temp{$i};
			}
			// Otherwise, if not numeric
			else {
				// Replace it with the proper number, as stated above
				$subject .= (string) ord($temp[$i]) - 55;
			}
		}
		
		// Finally, perform a MOD97 check on the string. If the result equals
		// 1, then the IBAN number is valid. If not the case:
		if(bcmod($subject, 97) != '1') {
			// Set the error message
			$this->_setErrorMessage(t('Invalid IBAN number'));
			
			// Return FALSE, indicating the IBAN code is invalid
			return FALSE;
		}
		
		// Return TRUE if we are still here, as the IBAN number is valid
		return TRUE;
	}
	
	/**
	 * Get Example Format
	 * 
	 * Will return an example string of how an IBAN number should look for
	 * the provided country. For example, for Belgium:
	 * 
	 * <code>
	 *	BExx xxxx xxxx xxxx
	 * </code>
	 * 
	 * NOTE: if no country is given, Belgium will be used by default
	 * 
	 * NOTE: we use the character x as placeholder, because depending on the
	 * country, the IBAN code can contain both letters and numbers, in which
	 * case using an example like "BE01 2345 6789 1234" would cause confusion.
	 * 
	 * @static
	 * @access public
	 * @param string $isoCode
	 * @return string
	 */
	public static function getExampleFormat($isoCode = NULL) {
		// Check whether the country is known
		if(! $isoCode || ! array_key_exists($isoCode, self::$_countries)) {
			// No, so default to Belgium
			$isoCode = 'BE';
		}
		
		// 3. Implode the groups of 4 characters, using whitespaces
		return implode(
			' ', 
			// 2. Split the placeholder per 4 characters
			str_split(
				// 1. Generate the placeholder format
				$isoCode . str_repeat('x', self::$_countries[$isoCode] - 2),
				4
			)
		);
	}
}