<?php
/**
 * M_ValidatorException class
 * 
 * Exceptions thrown by {@link M_Validator} and subclasses.
 * 
 * @package Core
 */
class M_ValidatorException extends M_Exception {
}