<?php
/**
 * M_ValidatorVat
 *
 * M_ValidatorVat, a subclass of {@link M_Validator} is used to validate a given
 * VAT Number.
 *
 * @package Core
 */
class M_ValidatorVat extends M_Validator {

	/* -- CONSTANTS -- */

	/**
	 * Validator Criteria Constant
	 *
	 * This constant is used to set the COUNTRY as validator criteria. The country
	 * is optional, and is used to explicitely validate the subject as a VAT Number
	 * from that country. If the country is not provided to this validator, the
	 * validator will try recognize the country from the provided number.
	 *
	 * NOTE:
	 * In order to set a country for the validator, you should provide with the
	 * country's ISO Code!
	 *
	 * @see M_Validator::setCriteria()
	 * @var string
	 */
	const COUNTRY = 'COUNTRY';

	/* -- GETTERS -- */

	/**
	 * Evaluate the subject
	 *
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 *
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// Clean up the validated subject
		$subject = trim(
			strtoupper(
				preg_replace('/[\s]+/', '', str_replace('.', '', $subject))
			)
		);

		// If a country has been defined in the validator:
		$country = $this->_getCriteriaValue(self::COUNTRY, FALSE);

		// If so
		if($country) {
			// Convert the country code to uppercase
			$country = trim(strtoupper($country));

			// Then, we make sure that the subject carries the country's prefix:
			if(substr($subject, 0, 2) != $country) {
				// If not, we add the prefix now:
				$subject = $country . $subject;
			}
		}

		// Look for a common syntax in the subject
		$aVatMatch = array();
		if(! preg_match('/^([a-z]{2})[ ]*(.+)$/is', $subject, $aVatMatch)) {
			$this->_setErrorMessage(t('VAT number must contain a country code'));
			return false;
		}

		// Regular expressions for all the countries
		$aVatRegexes	= array(
			'AT'	=> 'U[0-9]{8}',
			'BE'	=> '0[0-9]{9}',
			'BG'	=> '[0-9]{9,10}',
			'CY'	=> '[0-9]{8}[A-Za-z]',
			'CZ'	=> '[0-9]{8,10}',
			'DE'	=> '[0-9]{9}',
			'DK'	=> '[0-9]{2} ?[0-9]{2} ?[0-9]{2} ?[0-9]{2}',
			'EE'	=> '[0-9]{9}',
			'EL'	=> '[0-9]{9}',
			'ES'	=> '([A-Za-z0-9][0-9]{7}[A-Za-z0-9])',
			'FI'	=> '[0-9]{8}',
			'FR'	=> '[A-Za-z0-9]{2} ?[0-9]{9}',
			'GB'	=> '([0-9]{3} ?[0-9]{4} ?[0-9]{2}|[0-9]{3} ?[0-9]{4} ?[0-9]{2} ?[0-9]{3}|GD[0-9]{3}|HA[0-9]{3})',
			'HU'	=> '[0-8]{8}',
			'IE'	=> '[0-9][A-Za-z0-9+*][0-9]{5}[A-Za-z]',
			'IT'	=> '[0-9]{11}',
			'LT'	=> '([0-9]{9}|[0-9]{12})',
			'LU'	=> '[0-9]{8}',
			'LV'	=> '[0-9]{11}',
			'MT'	=> '[0-9]{8}',
			'NL'	=> '[0-9]{9}B[0-9]{2}',
			'PL'	=> '[0-9]{10}',
			'PT'	=> '[0-9]{9}',
			'RO'	=> '[0-9]{2,10}',
			'SE'	=> '[0-9]{12}',
			'SI'	=> '[0-9]{8}',
			'SK'	=> '[0-9]{10}',
		);

		// If this doens't match the right format:
		if (! isset($aVatRegexes[$aVatMatch[1]]) || ! preg_match('/^([a-z]{2})[ ]*'.$aVatRegexes[$aVatMatch[1]].'$/is', $subject)) {
			$this->_setErrorMessage(t('VAT number has an incorrect format'));
			return false;
		}

		// Country specific checks:
		switch($aVatMatch[1]) {
			// If Belgian VAT:
			case 'BE':
				if(! $this->_checkBE($subject)) {
					return false;
				}
				break;
			// If Ducht VAT:
			case 'NL':
				if(! $this->_checkNL($subject)) {
					return false;
				}
				break;
			default:
				break;
		}

		// Return TRUE if still here
		return true;
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * If the VAT Number is a Dutch one, do the 11-check
	 *
	 * @param $subject
	 * @return bool
	 */
	private function _checkNL($subject) {
		// Remove the "NL " part from the beginning
		if(! strncmp($subject, 'NL', 2)) {
			$subject = substr($subject, 2);
		}

	 	// Get the 9 digits between the country code and the last 3 characters
		$subject = trim(substr($subject, 0, 9));

	  	// Array with numbers that are correct for the 11-proef, but are not
		// valid in the VAT number
		$aInvalid = array( '111111110', '999999990', '000000000' );

		if( strlen( $subject ) != 9 || !ctype_digit( $subject ) || in_array( $subject, $aInvalid ) ) {
			// If number not valid:
			$this->_setErrorMessage(t('VAT number has an incorrect format'));
			return false;
		}

		/** Start 11-proef **/
		for( $i = 9, $som = -$subject % 10; $i > 1; $i-- ) {
			$som += $i * $subject{( 9 - $i )};
		}

		$proef = ( $som % 11 == 0 );

		// If 11-proef correct:
		if(!$proef) {
			$this->_setErrorMessage(t('Error while validating Dutch VAT number checksum'));
			return false;
		}

		return true;
	}


	/**
	 * If the VAT Number is a Belgian one, do the Mod97 check
	 *
	 * @param $subject
	 * @return bool
	 */
	private function _checkBE($subject) {
		// Remove the "BE " part from the beginning
		if(! strncmp($subject, 'BE', 2)) {
			$subject = trim(substr($subject, 2));
		}

		$strLen = strlen($subject);

		//Vat-number of 10 digits should start with a 0
		if ($strLen == 10) {
			if ($subject{0} != '0') {
				$this->_setErrorMessage(t('Belgian VAT number should start with "0"'));
				return false;
			}
		}

		//If other length: impossible
		else {
			$this->_setErrorMessage(t('Belgian VAT number should have 10 digits'));
			return false;
		}

		// Mod97 check on last nine digits
		$checksum = 97 - fmod(substr($subject,0,8),97);

		// check if 0
		if ($checksum == 0) $checksum = 97;

		// Do Mod97 check
		if ($checksum != substr($subject,8,10)) {
			$this->_setErrorMessage(t('Error while validating Belgian VAT number checksum'));
			return false;
		}

		return true;
	}

}