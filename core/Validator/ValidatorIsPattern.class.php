<?php
/**
 * M_ValidatorIsPattern
 * 
 * M_ValidatorIsPattern, a subclass of {@link M_Validator}, is used
 * to check if a subject value matches a given pattern.
 * 
 * @package Core
 */
class M_ValidatorIsPattern extends M_Validator {

	/* -- CONSTANTS -- */

	/**
	 * Validator Criteria Constant
	 * 
	 * With this constant, you can set a validator criteria. More specifically, 
	 * you can use this constant to set the pattern - in pcre syntax - with which 
	 * the subject is to be validated by this validator.
	 *
	 * @access public
	 * @var string
	 */
	const PATTERN = 'pattern';

	/**
	 * Validator Criteria Constant
	 * 
	 * With this constant, you can set a validator criteria. More specifically, 
	 * you can use this constant to set a collection of patterns - in pcre 
	 * syntax - with which the subject is to be validated by this validator.
	 * 
	 * Note that, if you provide with a collection of patterns, the subject will
	 * be evaluated as valid, if it matches any of the provided expressions. If
	 * the subject does not match any, the result of validation will be FALSE.
	 *
	 * @access public
	 * @var string
	 */
	const PATTERNS = 'patterns';

	/* -- MI_VALIDATOR -- */
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface, {@link M_Validator} 
	 * subclasses must implement the check() method. This method evaluates a 
	 * given subject. This method returns TRUE if the test subject is considered 
	 * "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		return (
			preg_match(
				$this->_getCriteriaValue(self::PATTERN, FALSE, TRUE),
				$subject
			)
			== 1
		);
	}
}