<?php
/**
 * MI_Validator interface 
 * 
 * The MI_Validator interface describes the public API that must be
 * implemented by Validator classes. A validator class is in charge 
 * of evaluating a given subject as "valid" or "not valid". Typically, 
 * Validator classes will inherit from the abstract {@link M_Validator} 
 * class, which already implements most of the required interface.
 * 
 * Non-abstract implementations of {@link M_Validator} validate a very specific 
 * aspect of a given subject. For example {@link M_ValidatorIsEmail} makes sure 
 * that a given string is a correctly formed email address.
 * 
 * @package Core
 */
interface MI_Validator {
	/**
	 * Set validator criteria
	 * 
	 * A Validator Criteria is a principle or standard by which the validated 
	 * subject will be judged. In other words, a validator criteria sets the value 
	 * of a specific variable that may influence the validation process.
	 * 
	 * For example, consider a class that checks if a given text (subject) has 
	 * a predetermined minimum and a defined maximum number of words. In our 
	 * example, we call this class M_ValidatorIsNumWords.
	 * 
	 * <code>
	 *    class M_ValidatorIsNumWords extends M_Validator {
	 *    }
	 * </code>
	 * 
	 * In order for this class to be capable of judging the validated subject, 
	 * this validator would need two validator criteria:
	 * 
	 * 1) What should be the minimum number of words?
	 * 2) What should be the maximum number of words?
	 * 
	 * Let's assume that the validator in our example defines two constants, made
	 * available so you can address each of the above-mentioned criteria:
	 * 
	 * <code>
	 *    class M_ValidatorIsNumWords extends M_Validator {
	 *       const MIN = 'min';
	 *       const MAX = 'max';
	 *    }
	 * </code>
	 * 
	 * Now, with this class in mind, assume we wanted to evaluate a given text 
	 * with this validator, to make sure that the text has at least 2 but no more 
	 * than 10 words. We would write the following code to achieve this:
	 * 
	 * Example 1
	 * <code>
	 *    // We construct an instance of the validator
	 *    $validator = new M_ValidatorIsNumWords;
	 *    
	 *    // Set the minimum number of words (2)
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MIN, 2);
	 *    
	 *    // Set the maximum number of words (10)
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MAX, 10);
	 *    
	 *    // Check the text subject
	 *    if($validator->check('Hello world!')) {
	 *       // The validator's check() method returns TRUE, so we can assume
	 *       // that the text respects the given minimum and maximum number of
	 *       // words:
	 *       echo 'Yes, the subject is valid!';
	 *    } else {
	 *       // The validator provides with FALSE as result, so we can assume
	 *       // that the text has less than 2 words, or more than 10 words.
	 *       echo 'Oops, ths subject is not valid!';
	 *    }
	 * </code>
	 * 
	 * The above code would output the following:
	 * 
	 * <code>
	 *    Yes, the subject is valid!
	 * </code>
	 * 
	 * However, you might want to show a different error message, depending on the
	 * validator criteria that caused the subject to be judged as invalid. Let's
	 * assume that you want to show the text "The text must contain at least 2 
	 * words" if the text contains less than 2 words. But, if the text contains 
	 * more than 10 words, you would like to show the following text instead:
	 * "The text must not contain more than 10 words".
	 * 
	 * In order to achieve this, you can set specific error messages for each
	 * of the criteria, with this method. Let's rewrite Example 1 as following:
	 * 
	 * Example 2
	 * <code>
	 *    // We construct an instance of the validator
	 *    $validator = new M_ValidatorIsNumWords;
	 *    
	 *    // Set the minimum number of words (2), and provide with the error
	 *    // message that should be set if the text does not comply with this 
	 *    // requirement
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MIN, 2, t('The text must contain at least 2 words'));
	 *    
	 *    // Set the maximum number of words (10), and provide with the error
	 *    // message that should be set if the text does not comply with this 
	 *    // requirement
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MAX, 10, t('The text must not contain more than 10 words'));
	 *    
	 *    // Check the text subject
	 *    if($validator->check('Hello world!')) {
	 *       // The validator's check() method returns TRUE, so we can assume
	 *       // that the text respects the given minimum and maximum number of
	 *       // words:
	 *       echo 'The text subject is valid!';
	 *    } else {
	 *       // The validator provides with FALSE as result, so we can assume
	 *       // that the text has less than 2 words, or more than 10 words. However,
	 *       // now we can show a specific error message to precisely describe 
	 *       // which of the validator criteria has caused the subject to be judged
	 *       // as invalid:
	 *       echo $validator->getErrorMessage();
	 *    }
	 * </code>
	 * 
	 * Also, you should be able to use {@link M_Validator::getErrorCriteria()} to 
	 * identify the validator criteria that has caused the subject to be judged 
	 * as invalid. Maybe you would want to undertake some additional actions if 
	 * the text from our example contains less than 2 words:
	 * 
	 * <code>
	 *    if($validator->getErrorCriteria() == M_ValidatorIsNumWords::MIN) {
	 *       // ... actions here
	 *    }
	 * </code>
	 * 
	 * You could rewrite as following:
	 * 
	 * <code>
	 *    if($validator->isErrorCriteria(M_ValidatorIsNumWords::MIN)) {
	 *       // ... actions here
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the criteria
	 * @param mixed $value
	 * 		The value of the criteria
	 * @param string $errorMessage
	 * 		The specific error message, for this criteria
	 * @return MI_Validator $validator
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setCriteria($name, $value, $errorMessage = NULL);
	
	/**
	 * Get error code
	 * 
	 * A validator may choose to define a set of error codes. By defining error 
	 * codes, the validator can make more information available on why the subject 
	 * failed to validate.
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getErrorCode();
	
	/**
	 * Get error criteria
	 * 
	 * You should be able to use {@link M_Validator::getErrorCriteria()} to 
	 * identify the validator criteria that has caused the subject to be judged 
	 * as invalid.
	 * 
	 * @see M_Validator::setCriteria()
	 * @access public
	 * @return mixed
	 */
	public function getErrorCriteria();
	
	/**
	 * Is error criteria?
	 * 
	 * Can be used to check whether or not the validator error criteria matches
	 * the provided one; see {@link M_Validator::getErrorCriteria()}
	 * 
	 * @see MI_Validator::setCriteria()
	 * @see MI_Validator::getErrorCriteria()
	 * @access public
	 * @param string $criteria
	 * 		The validator criteria to compare with
	 * @return mixed
	 */
	public function isErrorCriteria($criteria);
	
	/**
	 * Get error message
	 * 
	 * Similarly to the error code ({@link M_Validator::getErrorCode()}),
	 * an M_Validator class may also provide with a text that describes
	 * the error.
	 * 
	 * @see MI_Validator::setCriteria()
	 * @access public
	 * @return mixed
	 */
	public function getErrorMessage();
	
	/**
	 * Has error message?
	 * 
	 * Will tell whether or not the validator carries an error message.
	 * 
	 * @see M_Validator::getErrorMessage()
	 * @access public
	 * @return boolean $flag
	 * 		Returns TRUE if the validator has an error message, FALSE if not
	 */
	public function hasErrorMessage();
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface, all validator 
	 * classes must implement the check() method.
	 * 
	 * This method evaluates a given subject, and returns TRUE if the test subject 
	 * is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject);
}