<?php
/**
 * M_ValidatorRGBAMatrix
 * 
 * M_ValidatorRGBAMatrix, a subclass of {@link M_Validator} is used
 * to validate an RGBA Matrix. The matrix should be represented by
 * an array, with 20 numeric values:
 * 
 * <code>
 *    1  0  0  0  0
 *    0  1  0  0  0
 *    0  0  1  0  0
 *    0  0  0  1  0
 * </code>
 * 
 * To learn more about RGBA matrixes, read the documentation on 
 * {@link M_Color}.
 * 
 * @package Core
 */
class M_ValidatorRGBAMatrix extends M_Validator {
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		$c = (is_array($subject) && count($subject) == 20);
		for($i = 0, $c = TRUE; $i < 20 && $c; $i ++) {
			$c = is_numeric($subject[$i]);
		}
		
		return $c;
	}
}