<?php
/**
 * M_ValidatorNumWords class
 * 
 * M_ValidatorNumWords, a subclass of {@link M_Validator} is used
 * to validate a text against a minimum and maximum number of words.
 * 
 * @package Core
 */
class M_ValidatorIsNumWords extends M_Validator {

	/* -- CONSTANTS -- */

	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the minimum number of words as 
	 * validator criteria. Read {@link M_Validator::setCriteria()} to 
	 * learn more about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    // Set the minimum number of words to 10
	 *    $validator = new M_ValidatorIsNumWords;
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MIN, 10);
	 * </code>
	 * 
	 * This criteria sets the minimum number of words that the tested 
	 * string should contain. In Example 1, the text should have at
	 * least 10 words, in order to validate successfully.
	 */
	const MIN = 'min';
	
	/**
	 * Validator Criteria Constant
	 * 
	 * This constant is used to address the maximum number of words as 
	 * validator criteria. Read {@link M_Validator::setCriteria()} to 
	 * learn more about validator criteria.
	 * 
	 * Example 1
	 * <code>
	 *    // Set the maximum number of words to 50
	 *    $validator = new M_ValidatorIsNumWords;
	 *    $validator->setCriteria(M_ValidatorIsNumWords::MAX, 50);
	 * </code>
	 * 
	 * This criteria sets the maximum number of words that the tested 
	 * string can contain. In Example 1, the text cannot have more
	 * than 50 words, in order to validate successfully.
	 */
	const MAX = 'max';

	/* -- GETTERS -- */
	
	/**
	 * Evaluate the subject
	 * 
	 * To completely support the {@link MI_Validator} interface,
	 * {@link M_Validator} subclasses must implement the check() method.
	 * This method evaluates a given subject. This method returns
	 * TRUE if the test subject is considered "valid", FALSE if not.
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject to be validated
	 * @return boolean
	 */
	public function check($subject) {
		// First of all, we remove punctuation from the tested subject
		$filter  = new M_FilterTextPunctuation(
			new M_FilterTextValue(
				(string) $subject
			)
		);
		
		$subject = $filter->apply();
		
		// Now, we can safely split the text subject by white-space, in 
		// order to count the number of words:
		$num = count(explode(' ', $subject));
		
		// get validator criteria:
		$min = $this->_getCriteriaValue(self::MIN, 0);
		$max = $this->_getCriteriaValue(self::MAX, 0);
		
		// If less than the minimum:
		if($min > 0 && $num < $min) {
			// Set the error message...
			$this->_setErrorMessage(
				// ... to the one that has been provided for this criteria
				$this->_getCriteriaErrorMessage(
					self::MIN,
					t('The text should at least contain @number words', array(
						'@number' => $min
					))
				)
			);
			
			// Return FALSE as result
			return FALSE;
		}
		
		// If more than the maximum
		if($max > 0 && $num > $max) {
			// Set the error message...
			$this->_setErrorMessage(
				// ... to the one that has been provided for this criteria
				$this->_getCriteriaErrorMessage(
					self::MAX,
					t('The text should not contain more than @number words', array(
						'@number' => $max
					))
				)
			);
			
			// Return FALSE as result
			return FALSE;
		}
		
		// If still here, we return success (value is considered valid)
		return TRUE;
	}
}
?>