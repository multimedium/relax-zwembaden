<?php
class M_ChartGoogleBar extends M_ChartGoogle {
	
	//position
	const OPTION_BAR_POSITION_HORIZONTAL = 'h';
	const OPTION_BAR_POSITION_VERTICAL = 'v';
	
	//type
	const OPTION_BAR_TYPE_GROUPED = 'g';
	const OPTION_BAR_TYPE_STACKED = 's';
	
	const OPTION_BAR_WIDTH_AUTOMATIC ='a';
	const OPTION_BAR_WIDTH_RELATIVE ='r';
	
	
	/**
	 * Holds all the different axes, instances of {@link M_ChartAxe}
	 * 
	 * @var array
	 */
	private $_axes = array();
	
	/**
	 * Holds all the different ranges, instances of {@link M_ChartRange}
	 * @var array
	 */
	private $_ranges = array();
	
	/**
	 * Bar position
	 * 
	 * @var string
	 */
	private $_barPosition = self::OPTION_BAR_POSITION_HORIZONTAL;
	
	/**
	 * Bar type (grouped / stacked)
	 * 
	 * @var string
	 */
	private $_barType = self::OPTION_BAR_TYPE_STACKED;
	
	/**
	 * Bar width
	 * @var string|int
	 */
	private $_barWidth = self::OPTION_BAR_WIDTH_RELATIVE;
	
	/**
	 * Chart type
	 * 
	 * @var string
	 */
	private $_type = 'b';
	
	/**
	 * Generates the GoogleChart Api URI
	 * 
	 * @return M_Uri $uri
	 */
	protected function _getGoogleChartApiUri() {
		
		// Get the basic URI
		$uri = parent::_getGoogleChartApiUri();

		// Set the Type
		$uri->setQueryVariable('cht', $this->_type.$this->_barPosition.$this->_barType);
		
		// Get total count of datasets
		$totalcount = $this->getDatasets()->count();
		
		// Generate colorstring
		$colorstring = $this->_generateColorString();
		if($colorstring != '') {
			$uri->setQueryVariable('chco', $colorstring);
		}
		
		// Generate axes
		$axeTypes = array();
		$axeRanges = array();
		$i = 0;
		foreach($this->_axes AS $axe) {
			/* @var $axe M_ChartAxe */
			
			//always add a type
			$axeTypes[] = $axe->getType();
			
			//add ranges?
			if ($axe->getStartRange() || $axe->getEndRange()) {
				$axeRanges[] = $i.','.$axe->getStartRange().','.$axe->getEndRange();
			}
			$i++;
		}
		if(count($axeTypes) > 0) {
			$uri->setQueryVariable('chxr', implode('|',$axeRanges));
			$uri->setQueryVariable('chxt', implode(',',$axeTypes));
		}
		
		//Axe labels?
		$axeLabels = '';
		$i = 0;
		foreach($this->_axes AS $axe) {
			$axeLabelData = $axe->getLabels();
			//check if labels are defined
			if (count($axeLabelData) > 0) {
				$axeLabels .= $i.':|'.implode('|',$axeLabelData);
			}
			$i++;
		}
		if($axeLabels != '') {
			$uri->setQueryVariable('chxl', $axeLabels);
		}
		 
		//Add ranges?
		$range = array();
		foreach($this->_ranges AS $rangeData) {
			/* @var $range M_ChartRange */
			$rangeString = $rangeData->getType();
			$rangeString .= ','.$rangeData->getColor()->getHex();
			$rangeString .= ','.$rangeData->getStartPoint();
			$rangeString .= ','.$rangeData->getEndPoint();
			$rangeString .= ','.$rangeData->getPriority();
			$range[] = $rangeString;
		}
		if (count($range) > 0) {
			$uri->setQueryVariable('chm', implode('|', $range));
		}
		
		//Legend
		$legend = $this->_generateLegendString();
		if($legend) $uri->setQueryVariable('chdl', $legend);
		
		//Set bar-width to relative
		$uri->setQueryVariable('chbh', $this->_barWidth);
		return $uri;
	}

	/**
	 * Generate color string
	 * 
	 * This function looks into all datasets for colors.
	 * When every element has a legend with a valid color, the colors will be displayed
	 * 
	 * @return string $string
	 */
	protected function _generateColorString() {	
		$colors = array();
		foreach($this->getDatasets() AS $set) {
			/* @var $set M_ChartDataset */
			$color = $set->getColor();
			if($color) $colors[] = $color->getHex();
		}
		
		return implode(',', $colors);
	}
	
	/**
	 * Set the bar-position to horizontal
	 * 
	 * @return M_ChartGoogleBar
	 */
	public function setBarPositionHorizontal() {
		$this->_barPosition = self::OPTION_BAR_POSITION_HORIZONTAL;
	}
	
	/**
	 * Set the bar-position to vertical
	 * 
	 * @return M_ChartGoogleBar
	 */
	public function setBarPositionVertical() {
		$this->_barPosition = self::OPTION_BAR_POSITION_VERTICAL;
		return $this;
	}
	
	/**
	 * Set the bar width.
	 * 
	 * Possible options:
	 * - automatic
	 * - relative
	 * - width in pixels
	 * 
	 * @param type $arg
	 * @return M_ChartGoogleBar
	 */
	public function setBarWidth($arg) {
		$this->_barWidth = $arg;
		return $this;
	}
	
	/**
	 * Set the bar type to grouped: each set will have its own bar
	 * 
	 * @return M_ChartGoogleBar
	 */
	public function setBarTypeGrouped() {
		$this->_barType = self::OPTION_BAR_TYPE_GROUPED;
		return $this;
	}
	
	/**
	 * Set the bar-type to stacked: data-sets will be grouped into one bar
	 * 
	 * @return M_ChartGoogleBar
	 */
	public function setBarTypeStacked() {
		$this->_barType = self::OPTION_BAR_TYPE_STACKED;
		return $this;
	}
	
	/**
	 * Add an {@link M_ChartAxe}-instance to this chart
	 * 
	 * @param M_ChartAxe $axe
	 * @return M_ChartGoogleBar
	 */
	public function addAxe(M_ChartAxe $axe) {
		$this->_axes[] = $axe;
		return $this;
	}
	
	/**
	 * Add a range to the chart
	 * 
	 * @param M_ChartRange $range
	 * @return M_ChartGoogleBar
	 */
	public function addRange(M_ChartRange $range) {
		$this->_ranges[] = $range;
		return $this;
	}
	
	/**
	 * This function looks into all dataset if a legend(s) were added. If at
	 * least one legend has been found: it will create a legendstring (for all datasets)
	 * 
	 * @return string $string
	 */
	protected function _generateLegendString() {
		$legends = array();
		
		//don't use a legend by default
		$legendFound = false;
		
		foreach($this->getDatasets() AS $dataset) {
			$legend = $dataset->getLegend();
			$title = '';
			
			//if at least one legend has been found: enable legend
			if($legend) {
				$legendFound = true;
				$title = $legend->getTitle();
			}
			
			$legends[] = $title;
		}
		
		if(!$legendFound) return false;
		
		return implode('|',$legends);
	}
}