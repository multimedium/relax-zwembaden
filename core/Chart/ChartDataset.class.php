<?php 
/**
 * M_ChartDataset
 * 
 * The Dataset contains all the specific data in order to build
 * a chart
 * 
 * @author Wim Van De Mierop
 *
 */
class M_ChartDataset implements MI_ChartDataset {
	
	/**
	 * Used to store the different {@link M_ChartData} in an array
	 */
	private $_data = array();
	
	/**
	 * Minimum scale of the dataset
	 * 
	 * @var int $int
	 */
	private $_minScale;
	
	/**
	 * Minimum scale of the dataset
	 * 
	 * @var int $int
	 */
	private $_maxScale;

	/**
	 * Holds the color for this set
	 * 
	 * @var M_Color
	 */
	private $_color;
	
	/**
	 * The legend for this set
	 * 
	 * @var M_ChartLegend
	 */
	private $_legend;
	
	/**
	 * Add Data
	 * 
	 * Used to add data to this Dataset
	 * 
	 * @param M_ChartData $data
	 */
	public function addData(MI_ChartData $data) {
		$this->_data[] = $data;
	}
	
	/**
	 * Get Data
	 * 
	 * Used to get all the data of this Dataset
	 * 
	 * @return M_ArrayIterator $data
	 */
	public function getData() {
		return new M_ArrayIterator($this->_data);
	}
	
	/**
	 * Get Legend
	 * 
	 * Gets the legend of this data
	 * 
	 * @return M_ChartLegend $legend
	 */
	public function getLegend() {
		return $this->_legend;
	}

	/**
	 * Set Legend
	 * 
	 * Sets the legend of this data
	 * @param M_ChartLegend $legend
	 */
	public function setLegend(MI_ChartLegend $legend) {
		$this->_legend = $legend;
	}
	
	/**
	 * Get the minimum of the chart
	 * 
	 * @return int $minScale
	 */
	public function getMinScale() {
		return $this->_minScale;
	}
	
	/**
	 * Set the minScale
	 * 
	 * @param int $minScale
	 * @return void
	 */
	public function setMinScale($minScale) {
		$this->_minScale = $minScale;
	}
	
	/**
	 * Get the maximum scale of the chart
	 * 
	 * @return int $maxScale
	 */
	public function getMaxScale() {
		return $this->_maxScale;
	}
		
	/**
	 * Set the maximum scale of the chart
	 * 
	 * @param int $maxScale
	 * @return void
	 */
	public function setMaxscale($maxScale) {
		$this->_maxScale = $maxScale;
	}
	
	/**
	 * Get the color for this set
	 * 
	 * @return M_Color
	 */
	public function getColor() {
		return $this->_color;
	}
	
	/**
	 * Set the color for this set
	 * 
	 * @param M_Color $color
	 * @return void
	 */
	public function setColor(M_Color $color) {
		$this->_color = $color;
	}
		
	/**
	 * Get the maximum value for this dataset
	 * 
	 * @return M_ChartData
	 */
	public function getMaxChartData() {
		$max = false;
		foreach($this->getData() AS $data) {
			/* @var $data M_ChartData */
			if(!$max) $max = $data;
			elseif ($data->getValue() > $max->getValue()) $max = $data;
		}
		return $max;
	}
}