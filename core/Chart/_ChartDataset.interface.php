<?php
/**
 * MI_ChartDataset interface
 * 
 * @package Core
 */
interface MI_ChartDataset {
	
	/**
	 * Adds data to the Dataset
	 * 
	 * @param MI_ChartData $data
	 * @return void
	 */
	public function addData(MI_ChartData $data);
	
	/**
	 * Get collection of data
	 *  
	 * @return ArrayIterator $data
	 * 		Returns a collection of {@link MI_ChartData}
	 */
	public function getData();
	
}