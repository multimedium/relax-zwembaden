<?php 
/**
 * M_ChartLegend
 * 
 * Stores the legend of a certain {@link M_ChartData}
 * A legend contains
 * 	- the color to display the graph
 * 	- the title of this legend
 * 	- the description of this legend
 * 
 * @author Wim Van De Mierop
 *
 */
class M_ChartLegend implements MI_ChartLegend {
	
	/**
	 * Stores the color
	 * 
	 * @var M_Color
	 */
	private $_color;
	
	/**
	 * Stores the title
	 * 
	 * @var string
	 */
	private $_title;
	
	/**
	 * Stores the description
	 * 
	 * @var string
	 */
	private $_description;
	
	public function __construct($title = NULL, $color = NULL) {
		if(!is_null($title)) {
			$this->setTitle($title);
		}
		
		if(!is_null($color)) {
			$this->setColor($color);
		}
	}
	/**
	 * Get Color
	 * 
	 * Gets the color
	 * 
	 * @return M_Color $color
	 */
	public function getColor() {
		return $this->_color;
	}

	/**
	 * Set Color
	 * 
	 * Sets the color
	 * 
	 * @param M_Color $color
	 */
	public function setColor(M_Color $color) {
		$this->_color = $color;
	}

	/**
	 * Get Title
	 * 
	 * @return the $_title
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * Set Title
	 * 
	 * Sets the title
	 * 
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->_title = $title;
	}

	/**
	 * Get Description
	 * 
	 * Gets the description
	 * 
	 * @return string $description
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * Set Description
	 * 
	 * Sets the description
	 * 
	 * @param string $_description
	 */
	public function setDescription($description) {
		$this->_description = $description;
	}	
	
}