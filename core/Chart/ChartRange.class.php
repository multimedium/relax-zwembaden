<?php
/**
 * M_ChartRange
 * 
 * Add a range to a chart, so you can project a {@link M_ChartDataset} against a range
 * 
 * @author Ben Brughmans
 *
 */
class M_ChartRange {
	
	const TYPE_RANGE_HORIZONTAL = 'r';
	const TYPE_RANGE_VERTICAL = 'R';
	
	/**
	 * @var float
	 */
	private $_startPoint = 0;
	
	/**
	 * @var float
	 */
	private $_endPoint = 0;
	
	/**
	 * @var int
	 */
	private $_priority = 0;
	
	/**
	 * @var M_Color
	 */
	private $_color;
	
	/**
	 * @var string
	 */
	private $_type;
	
	/**
	 * Set the color for this range
	 * 
	 * @param M_Color $color
	 * @return M_ChartRange 
	 */
	public function setColor(M_Color $color) {
		$this->_color = $color;
		return $this;
	}
	
	/**
	 * Set start point for this range
	 * 
	 * @param float $startPoint
	 * @return M_ChartRange 
	 */
	public function setStartPoint($startPoint) {
		$this->_startPoint = $startPoint;
		return $this;
	}
	
	/**
	 * Set the end point for this range
	 * @param float $endPoint
	 * @return M_ChartRange 
	 */
	public function setEndPoint($endPoint) {
		$this->_endPoint = $endPoint;
		return $this;
	}
	
	/**
	 * Set the priority for this range
	 * 
	 * A priority will determine whether (or not) a range will be placed on top
	 * of another range. Increase priority so this range will be visualized on 
	 * top.
	 * 
	 * @param int $priority
	 * @return M_ChartRange
	 */
	public function setPriority($priority) {
		$this->_priority = (int)$priority;
		return $this;
	}
	
	/**
	 * Set the type for this range
	 * 
	 * @param string
	 * @return M_ChartRange
	 */
	public function setType($type) {
		$this->_type = (string)$type;
		return $this;
	}
	
	/**
	 * Get the start-point for this range
	 * 
	 * @return float
	 */
	public function getStartPoint() {
		return $this->_startPoint;
	}
	
	/**
	 * Get the end-point for this range
	 * 
	 * @return float
	 */
	public function getEndPoint() {
		return $this->_endPoint;
	}
	
	/**
	 * Get the priority
	 * 
	 * @see setPriority()
	 * @return int
	 */
	public function getPriority() {
		return $this->_priority;
	}
	
	/**
	 * Get the color for this range
	 * 
	 * @return M_Color
	 */
	public function getColor() {
		return $this->_color;
	}
	
	/**
	 * Get the type for this range
	 * 
	 * @return string
	 */
	public function getType() {
		return $this->_type;
	}
}