<?php
/**
 * MI_Chart interface
 * 
 * @package Core
 */
interface MI_Chart {
	
	/**
	 * Add Dataset
	 * 
	 * Is used to add a {@link MI_ChartDataset}, containing
	 * all the necessary data in order to draw an element on the chart
	 * 
	 * @param MI_ChartDataset $dataset
	 * @return void
	 */
	public function addDataset(MI_ChartDataset $dataset);
	
	/**
	 * Get Datasets
	 * 
	 * Gets a collection of {@link MI_ChartDataset}, containing data to
	 * build the chart
	 * 
	 * @return ArrayIterator
	 */
	public function getDatasets();
	
	/**
	 * set Width
	 * 
	 * Used to set the width of the chart's graph
	 * 
	 * @param int $width
	 * @return void
	 */
	public function setWidth($width);
	
	/**
	 * Get Width
	 * 
	 * Used to get the width of the chart's graph
	 * 
	 * @return int $width
	 * 		Return the width
	 */
	public function getWidth();
	
	/**
	 * Set Height
	 * 
	 * Used to set the height of the chart's graph
	 * 
	 * @param int $height
	 * @return void
	 */
	public function setHeight($height);
	
	/**
	 * Get Height
	 * 
	 * Used to get the height of the chart's graph
	 * 
	 * @return void
	 */
	public function getHeight();
	
	/**
	 * set the Title
	 * 
	 * Is used to set the title of the legend
	 * 
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title);
	
	/**
	 * get the Title
	 * 
	 * Returns the title of the legend
	 * 
	 * @return string $title
	 */
	public function getTitle();

	/**
	 * get the HTML of the chart
	 * 
	 * Used to generate the chart's graph
	 * 
	 * @return string
	 */
	public function getHtml();
}