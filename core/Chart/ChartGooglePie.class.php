<?php
class M_ChartGooglePie extends M_ChartGoogle {
	

	/**
	 * Contains the type of the pie
	 * 
	 * @var string
	 */
	private $_type = 'p';
	
	/**
	 * Generates the GoogleChart Api URI
	 * 
	 * @return M_Uri $uri
	 */
	protected function _getGoogleChartApiUri() {
		
		// Get the basic URI
		$uri = parent::_getGoogleChartApiUri();

		// Set the Type (Pie-Chart)
		$uri->setQueryVariable('cht', $this->_type);
		
		// Get total count of datasets
		$totalcount = $this->getDatasets()->count();
		
		// The PIE only supports one type of dataset
		if($totalcount > 1) {
			throw new M_Exception('M_ChartGooglePie allows only 1 dataset, ' . $totalcount . ' datasets initiated');
		}

		// Set the dataset
		$dataset = $this->getDatasets()->current();
		
		// Generate colorstring
		$colorstring = $this->_generateColorString();
		if($colorstring) {
			$uri->setQueryVariable('chco', $colorstring);
		}
		
		// Generate legend string
		$legendstring = $this->_generateLegendString();

		if($legendstring) {
			$uri->setQueryVariable('chl', $legendstring);
		}

		return $uri;
	}
	
	/**
	 * Generate Google API scale string (min and max value)
	 * 
	 * @return string
	 */
	protected function _generateGoogleScaleString() {
		$dataset = $this->getDatasets()->first();
		$maxValue = $dataset->getMaxChartData();
		if ($maxValue) $value = $maxValue->getValue();
		else $value = 0;
		return '0,'.$value;
	}

	/**
	 * Generate color string
	 * 
	 * This function looks into the first dataset (only one is supported in a pie)
	 * When every element has a legend with a valid color, the colors will be displayed
	 * 
	 * @return string $string
	 */
	protected function _generateColorString() {	
		// Counter for the seperator
		$i = 1;

		// Look at the data
		$dataArray = $this->getDatasets()->current()->getData();
		
		$result = '';
		
		// Get the data
		foreach($dataArray as $data) {
			
			$color = $data->getLegend()->getColor();
			
			if(is_null($color)) {
				return false;
			} else {			
				$color = $color->getHex();
			}
			
			// Add the value
			$result .= $color;
			
			// If this is not the last item, add the seperator
			if($i != $dataArray->count()) {
				$result .= ',';
			}
			
			// Count++
			$i++;
			
		}
		
		return $result;
	}
	
	
	/**
	 * This function looks into the first dataset (only one is supported in a pie)
	 * When every element has a legend with a valid title, the title will be displayed
	 * 
	 * @return string $string
	 */
	protected function _generateLegendString() {
		
		// Get the first dataset
		$dataset = $this->getDatasets()->current();
		
		// Counters
		$i = 1;
		$count = $dataset->getData()->count();
		
		$result = '';
		foreach($dataset->getData() as $data) {
			
			// Get title
			$title = $data->getLegend()->getTitle();
			
			$result .= $title;
			
			if($i != $count) {
				$result .= '|';
			}
			
			$i++;
			
		}
		
		// Return the generated string
		return $result;
	}
	
	
	/**
	 * Set the pie to 3D
	 * 
	 * @param $bool
	 * @return void
	 */
	public function set3D($bool = true) {
		if($bool) {
			$this->_type = 'p3';
		} else {
			$this->_type = 'p';
		}
	}
}