<?php
/**
 * M_MailerSmtp class
 * 
 * M_MailerSmtp is an implementation of the {@link MI_Mailer} interface, that uses
 * an SMTP Host to send email messages.
 * 
 * @package Core
 */
class M_MailerSmtp extends M_Mailer implements MI_Mailer {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Transport
	 * 
	 * @var Swift_SmtpTransport 
	 */
	protected $_transport;
	
	/**
	 * SMTP host
	 * 
	 * This property stores the SMTP host, which can be set with the method
	 * {@link M_MailerSmtp::setHost()}.
	 * 
	 * @access protected
	 * @var M_Uri
	 */
	protected $_host;
	
	/**
	 * AntiFlood
	 * 
	 * @var array
	 */
	protected $_antiFlood;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_MailerSmtp
	 */
	public function __construct(M_Uri $host = NULL) {
		if($host) {
			$this->setHost($host);
		}
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set SMTP host
	 * 
	 * NOTE:
	 * If the SMTP requires authentication, you should provide the username and
	 * password in the {@link M_Uri}. For more information, read the docs on
	 * 
	 * - {@link M_Uri::setUsername()}
	 * - {@link M_Uri::setPassword()}
	 * 
	 * @access public
	 * @return void
	 */
	public function setHost(M_Uri $host) {
		$this->_host = $host;
		
		//when changing the host and when the transporter has already been
		//initiated: we need to update the uri back again in this transporter
		if($this->_transport) {
			$this->_setUriInTransport($host);
		}
	}
	
	/**
	 * Will limit the connection to a number of e-mails sent and/or 
	 * pause for a number of seconds.
	 * 
	 * E.g. when this mailer sends messages it will count the number of 
	 * messages that have been sent since the last re-connect. Once the number 
	 * hits your specified threshold it will disconnect and re-connect, 
	 * optionally pausing for a specified amount of time.
	 * 
	 * @param int $numberOfMails
	 * @param int $numberOfSeconds 
	 * @return void
	 */
	public function setAntiFlood($numberOfMails, $pauseInseconds) {
		$this->_antiFlood = array($numberOfMails, $pauseInseconds);
		
		//when changing the antiflood and when the transporter has already been
		//initiated: we need to update it back again in this transporter
		if($this->_transport) {
			$this->_setAntifloodInTransport($numberOfMails, $pauseInseconds);
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get SMTP Host
	 * 
	 * Will provide with the SMTP host that has been set previously with 
	 * {@link M_MailerSmtp::setHost()}.
	 * 
	 * @access public
	 * @return M_Uri
	 */
	public function getHost() {
		return $this->_host;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get instance of {@link Swift_SmtpTransport}
	 * 
	 * Overrides {@link M_Mailer::_getTransport()}, and provides with an instance
	 * of {@link Swift_SmtpTransport} that has been set up to use SMTP instead 
	 * of mail.
	 * 
	 * @access protected
	 * @return Swift_SmtpTransport
	 */
	protected function _getTransport() {
		
		// Make sure the SMTP host has been provided:
		if(! $this->_host) {
			throw new M_MailException('Cannot send email; missing SMTP Host!');
		}
		
		// If no mailer exists: create one
		if(is_null($this->_transport)) {
			
			// Return the PHPMailer instance
			$this->_transport = Swift_SmtpTransport::newInstance();
			
			// Set host data in transporter
			$this->_setUriInTransport($this->getHost());
			
			// Set antiflood plugin?
			if($this->_antiFlood) {
				$this->_setAntifloodInTransport(
					$this->_antiFlood[0], 
					$this->_antiFlood[1]
				);
			}
		}
		
		return $this->_transport;
	}
	
	/**
	 * Set host data in the transporter
	 * 
	 * @param M_Uri $uri 
	 */
	protected function _setUriInTransport(M_Uri $uri) {
		
		if(!$this->_transport) {
			throw new M_Exception('Transport not yet set: cannot set uri in it');
		}
		
		/* @var $transport Swift_SmtpTransport */
		$this->_transport->setHost($this->_host->getHost());
		$this->_transport->setPort($this->_host->getPort());
		
		// SMTP Authentication
		$temp = $this->_host->getUsername();
		if( $temp ) {
			// SMTP Authentication is needed:
			$this->_transport->setUsername($temp);
		}

		$temp = $this->_host->getPassword();
		if( $temp ) {
			$this->_transport->setPassword($temp);
		}
	}
	
	/**
	 * Register antiflood data in the transporter
	 * 
	 * @param int $numberOfMails
	 * @param int $numberOfSeconds 
	 */
	protected function _setAntifloodInTransport($numberOfMails, $numberOfSeconds) {
		$this->_transport->registerPlugin(
			new Swift_Plugins_AntiFloodPlugin((int)$numberOfMails, (int)$numberOfSeconds)
		);
	}
}