<?php
/**
 * M_Mailer class
 * 
 * @package Core
 */

/**
 * Import {@link PHPMailer} package
 */
//require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . FOLDER_THIRDPARTY . DIRECTORY_SEPARATOR . 'phpmailer/class.phpmailer.php';
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . FOLDER_THIRDPARTY . DIRECTORY_SEPARATOR . 'swift/swift_required.php';

/**
 * M_Mailer class
 * 
 * M_Mailer is an implementation of the {@link MI_Mailer} interface, that uses
 * the sendmail program to send email messages.
 * 
 * @package Core
 */
class M_Mailer extends M_Object implements MI_Mailer {

	/**
	 * Constants for the encoding
	 */
	const ENCODING_8BIT = "8bit";
	const ENCODING_7BIT = "7bit";
	const ENCODING_BINARY = "binary";
	const ENCODING_BASE64 = "base64";
	const ENCODING_QUOTEDPRINTABLE = "quoted-printable";
	
	/**
	 * Logger
	 * 
	 * This property stores the logger. For more info, read the documentation
	 * on {@link M_Mailer::setLogger()}
	 * 
	 * @access protected
	 * @var MI_MailLogger
	 */
	protected $_logger;
	
	/**
	 * Email message
	 * 
	 * This property stores the email message that should be sent. For more info,
	 * read the docs on {@link M_Mailer::setMail()}.
	 * 
	 * @access protected
	 * @var M_Mail
	 */
	protected $_mail;
	
	/**
	 * Error message
	 * 
	 * This property stores the error message (if any)
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_errorMessage;

	/**
	 * Word wrap
	 *
	 * This property stores the amount of characters per line before a line-break
	 * is added to the source
	 *
	 * Note: if wordwrap is false, the function will be ignored
	 * 
	 * @var mixed
	 */
	protected $_wordWrap = 75;

	/**
	 * Encoding
	 *
	 * This property stores the encoding of the email
	 * 
	 * @var string
	 */
	protected $_encoding = self::ENCODING_QUOTEDPRINTABLE;
	
	/**
	 * Set logger
	 * 
	 * Will set the object that is in charge of logging the email message. This
	 * object must implement the {@link MI_MailLogger} interface.
	 * 
	 * @see MI_Mailer::setLogger()
	 * @access public
	 * @param MI_MailLogger $logger
	 * 		The email logger object
	 * @return void
	 */
	public function setLogger(MI_MailLogger $logger) {
		$this->_logger = $logger;
	}
	
	/**
	 * Set email
	 * 
	 * Will set the email that is to be sent.
	 * 
	 * @see MI_Mailer::setMail()
	 * @access public
	 * @param M_Mail $mail
	 * 		The email message, represented by an instance of {@link M_Mail}
	 * @return void
	 */
	public function setMail(M_Mail $mail) {
		$this->_mail = $mail;
	}

	/**
	 * Set wordWrap
	 *
	 * Will set the property wordWrap: the amount of characters per line before
	 * a line-break is added
	 *
	 * @param int $wordWrap
	 */
	public function setWordWrap($wordWrap) {
		$this->_wordWrap = $wordWrap;
	}

	/**
	 * Set encoding
	 *
	 * Will set the property encoding: the type of encoding that will be used when
	 * sending the email
	 * 
	 * @param string $encoding
	 */
	public function setEncoding($encoding) {
		if(in_array($encoding, array(
			self::ENCODING_7BIT,
			self::ENCODING_8BIT,
			self::ENCODING_BASE64,
			self::ENCODING_BINARY,
			self::ENCODING_QUOTEDPRINTABLE
		))) {
			$this->_encoding = $encoding;
		} else {
			throw new M_MailException(sprintf(
				'Could not find encoding "%s"',
				$encoding
			));
		}
	}

	/**
	 * Get encoding
	 * 
	 * @return string $encoding
	 */
	public function getEncoding() {
		return $this->_encoding;
	}
	
	/**
	 * Get logger
	 * 
	 * Will provide with the object that is in charge of logging the email message,
	 * previously set with {@link M_Mailer::setLogger()}
	 * 
	 * NOTE:
	 * If no logger has been assigned previously, this method will return NULL.
	 * 
	 * @see MI_Mailer::getLogger()
	 * @access public
	 * @return MI_MailLogger
	 */
	public function getLogger() {
		return $this->_logger;
	}
	
	/**
	 * Get mail
	 * 
	 * Will provide with the email message that should be sent by the mailer.
	 * 
	 * @see MI_Mailer::getMail()
	 * @access public
	 * @return M_Mail
	 */
	public function getMail() {
		return $this->_mail;
	}
	
	/**
	 * Send the email
	 * 
	 * @see MI_Mailer::send()
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function send() {
		
		// Make sure a mail has been provided:
		if(! $this->_mail) {
			throw new M_MailException('Cannot send email; no email message has been provided to mailer');
		}
		
		// Make sure receipients are set
		if($this->_mail->getRecipients()->count() == 0) {
			throw new M_MailException('Cannot send email; no recipients have been set in the mail');
		}
		
		// First, we check if we need to embed images
		if($this->_mail->getEmbedImages()) {
			
			// Check all images in the body
			$dom = new M_HtmlDom($this->_mail->getBody());
			
			// We keep all files and tokens in an array, to avoid duplicate embedded
			// images
			$files = array();
			
			foreach($dom->find('img') as $img) {
				/*@var $img M_HtmlDomNode */
				
				// Try to get the path/file of the image
				$uri = new M_Uri($img->getAttribute('src'));
				$path = str_replace(M_Request::getLinkWithoutPrefix() . '/', '', $uri->toString());
				
				// Create the file
				$file = new M_File($path);
				
				// If the file exists:
				if($file->exists()) {
					// If we already added this file as token:
					if(isset($files[$path])) {
						$token = $files[$path];
					} else {
						// If the file is not yet added: add as embedded attachment
						// with the token as CID
						$token = M_Helper::getUniqueToken().'@'.M_Request::getUri()->getDomainName();
						
						// Add it as embedded attachment
						$this->_mail->addEmbeddedAttachment($file, $token, null, M_Mailer::ENCODING_BASE64);
						// Add the token to the array
						$files[$path] = $token;
					}
					
					// Add the token as CID
					$img->setAttribute('src', 'cid:'.$token);
				}
			}
			
			// Set the body
			$this->_mail->setBody((string) $dom);
		}
		
		// Try to send e-mail
		try {
			
			// Create a new swift message, with the SwitftMailer package:
			$message = Swift_Message::newInstance();
			/* @var $message Swift_Message */
			
			//set the character set of the mail
			$message->setCharset('utf-8');
			
			//set the encoding of the mail (base64, quoted printable, ...)
			$message->setEncoder(
				$this->_getEncoder(
					$this->_encoding
				)
			);

			// Set from and sender:
			$message->setFrom(
				$this->_mail->getFromEmail(), 
				$this->_mail->getFromName()
			);
			$message->setSender(
				$this->_mail->getFromEmail(), 
				$this->_mail->getFromName()
			);
			
			// Set subject:
			if($this->_mail->getSubject()) {
				$message->setSubject($this->_mail->getSubject());
			}else {
				$message->setSubject('[Untitled] An email message');
			}

			// In debug mode?
			if(M_Debug::getDebugMode() && M_Debug::getDebugEmailRecipient()) {
				// For each email recipient in debug mode;
				foreach(M_MailHelper::getRecipientsFromString(M_Debug::getDebugEmailRecipient()) as $email => $name) {
					// Add the recipient to the mailer:
					$message->addTo($email, $name);
				}
				
				// Now, add "DEBUG" before the subject
				$message->setSubject('[DEBUG-MODE] ' . $message->getSubject());
				
				// Add extra headers for tracking original recipients
				$this->_mail->addCustomHeader('X-DEBUG-RECIPIENTS', $this->_mail->getRecipientsString());
				$this->_mail->addCustomHeader('X-DEBUG-CC', $this->_mail->getCcString());
				$this->_mail->addCustomHeader('X-DEBUG-BCC', $this->_mail->getBccString());
				
				if($this->_mail->getReplyToEmail()) {
					$this->_mail->addCustomHeader('X-DEBUG-REPLY-TO', $this->_mail->getReplyToEmail());
				}
			}
			
			// In production mode: add all recipients and (b)cc
			else {
				// Set recipients:
				foreach($this->_mail->getRecipients() as $email => $name) {
					$message->addTo($email, $name);
				}

				// Add cc
				foreach($this->_mail->getCc() as $email => $name) {
					$message->addCc($email, $name);
				}

				// Add bcc
				foreach($this->_mail->getBcc() as $email => $name) {
					$message->addBcc($email, $name);
				}

				// Set Reply-To
				$replyAddress = $this->_mail->getReplyToEmail();
				$replyName = $this->_mail->getReplyToName();
				if($replyAddress) {
					$message->addReplyTo($replyAddress, ($replyName ? $replyName : ''));
				}
			}

			// Add attachments:
			foreach($this->_mail->getAttachments() as $name => $file) {
				// The two statements above could be written in one line instead
				$swiftAttachment = Swift_Attachment::fromPath($file->getPath());
				$swiftAttachment->setFilename($name);
				$message->attach($swiftAttachment);
			}

			// Add embeddeded attachments
			foreach($this->_mail->getEmbeddedAttachments() AS $cid => $attachment) {

				/* $attachment is an array containing 5 elements:
				* 
				* 0: MI_FsItemFile, The file which needs to be embedded to the mail
				* 1: Content ID of the attachment. Use this to identify the Id for 
				*	  accessing the image in HTML.
				* 2: Overrides the attachment name
				* 3: File encoding (default base64)
				* 4: MIME type (e.g. image/jpeg or image/gif). Leave empty to autodetect
				*/

				$image = Swift_Image::fromPath($attachment[0]->getPath());

				//set content id
				$image->setId($cid);

				//set correct encoding
				$image->setEncoder($this->_getEncoder($attachment[3]));
				
				//set the filename
				if($attachment[2]) {
					$image->setFilename($attachment[2]);
				}

				//embed it
				$message->embed($image);

			}

			// and Body
			$message->setBody($this->_mail->getBody(), 'text/html');

			// Set alternative text (plain text)
			if(!is_null($this->_mail->getAltBody())) {
				$message->addPart($this->_mail->getAltBody(), 'text/plain');
			}
			// Set wordwrap
			if($this->_wordWrap !== FALSE) {
				$message->setMaxLineLength($this->_wordWrap);
			}

			// Set return path
			if(!is_null($this->_mail->getReturnPath())) {
				$message->setReturnPath($this->_mail->getReturnPath());			
			}
			// If not given, default to the FROM address
			else {
				$message->setReturnPath($this->_mail->getFromEmail());		
			}

			// Set priority
			$message->setPriority($this->_mail->getPriority());
			// Get the container which holds all headers
			$headerSet = $message->getHeaders();
			/* @var $headerSet Swift_Mime_HeaderSet */
			
			// Add custom headers to it
			foreach($this->_mail->getCustomHeaders() as $name => $value) {
				$headerSet->addTextHeader($name, $value);
			}
			
			// prepare to mail using correct transporter
			$mailer = Swift_Mailer::newInstance($this->_getTransport());
			
			// fire away! :)
			if($mailer->send($message)) {
				// If the mail has been sent successfully, we log the email message.
				$logger = $this->getLogger();
				if($logger) {
					// Create an entry for the logger, and save:
					$entry = $logger->getNewEntry();
					$entry->setMail($this->_mail);
					$entry->save();
				}
				
				return TRUE;

			} else {
				return FALSE;
			}
		} 
		
		//Something went wrong
		catch (Exception $e) {
			echo $this->_errorMessage = $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Get correct encoder based on encoding-string
	 * 
	 * @param string $encoding
	 * @return \Swift_Mime_ContentEncoder_Base64ContentEncoder|\Swift_Mime_ContentEncoder_PlainContentEncoder|\Swift_Mime_ContentEncoder_NativeQpContentEncoder
	 * @throws M_Exception 
	 */
	protected function _getEncoder($encoding) {
		switch ($encoding) {
			case M_Mailer::ENCODING_BASE64:
				return new Swift_Mime_ContentEncoder_Base64ContentEncoder();
			case M_Mailer::ENCODING_7BIT:
				return new Swift_Mime_ContentEncoder_PlainContentEncoder('7bit');
			case M_Mailer::ENCODING_8BIT:
				return new Swift_Mime_ContentEncoder_PlainContentEncoder('8bit');
			case M_Mailer::ENCODING_QUOTEDPRINTABLE:
				return new Swift_Mime_ContentEncoder_NativeQpContentEncoder();
			case M_Mailer::ENCODING_BINARY:
				return new Swift_Mime_ContentEncoder_PlainContentEncoder('binary');
			default:
				throw new M_Exception(sprintf('Unsupported encoding %s', $encoding));
		}
	}
	
	/**
	 * Get error message
	 * 
	 * Will provide with an error message (if any).
	 * 
	 * Example 1
	 * <code>
	 *    $mail = new M_Mail;
	 *    $mail->setSubject('Hello');
	 *    $mail->setBody('Hello world!');
	 *    $mail->addRecipient('Tom Bauwens <tom@multimedium.be>');
	 *    
	 *    $mailer = new M_Mailer;
	 *    $mailer->setMail($mail);
	 *    if($mailer->send()) {
	 *       echo $mailer->getError();
	 *    }
	 * </code>
	 * 
	 * @see MI_Mailer::getError()
	 * @access public
	 * @return string
	 */
	public function getError() {
		return $this->_errorMessage;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get instance of {@link Swift_MailTransport}
	 * 
	 * {@link M_Mailer} is a wrapper of {@link SwiftMailer}. This method provides
	 * with an instance of {@link Swift_MailTransport}, that can be used to 
	 * send the email message uing php's mail() function.
	 * 
	 * To send using SMTP, please use {@link MailerSmtp}
	 * 
	 * @access protected
	 * @return Swift_MailTransport
	 */
	protected function _getTransport() {
		return Swift_MailTransport::newInstance();
	}
	
}