<?php
/**
 * M_MailLogger class
 * 
 * M_MailLogger is an implementation of the {@link MI_MailLogger} interface, that 
 * uses the database to log email messages.
 * 
 * @package Core
 */
class M_MailLogger extends M_Object implements MI_MailLogger {
	/**
	 * Get logged email messages
	 * 
	 * @see MI_MailLogger::getEntries()
	 * @access public
	 * @param string $context
	 * 		The context of the log entries
	 * @return ArrayIterator
	 */
	public function getEntries($context = NULL) {
		// Array with which the iterator is built:
		$out = array();
		
		// Get the table where the entries are being stored:
		$table = $this->_getDbTable($context);
		
		// get a select statement on that table:
		$select = $table->select();
		
		// Get the entries, ordered descendingly in time
		$rs = $select->order('log_time', 'DESC')->execute();
		if($rs !== FALSE && count($rs) > 0) {
			// For each of the entries:
			foreach($rs as $row) {
				// Create a new mail:
				$mail = new M_Mail();
				$mail->addRecipient($row['log_to']);
				$mail->addCc($row['log_cc']);
				$mail->addBcc($row['log_bcc']);
				$mail->setSubject($row['log_subject']);
				$mail->setBody($row['log_body']);
				
				// Create a log entry:
				$entry = new M_MailLoggerEntry();
				$entry->setLogger($this);
				$entry->setMail($mail);
				$entry->setTime(new M_Date((int) $row['log_time']));
				
				// Add the entry to the iterator:
				$out[] = $entry;
			}
		}
		
		// Return the iterator:
		return new ArrayIterator($out);
	}
	
	/**
	 * Add an entry
	 * 
	 * @see MI_MailLogger::addEntry()
	 * @access public
	 * @param MI_MailLoggerEntry $entry
	 * 		The entry to be saved to the log
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function addEntry(MI_MailLoggerEntry $entry) {
		// Get the email message from the entry:
		$mail = $entry->getMail();
		if(! $mail) {
			throw new M_MailException('Cannot log the email message; no email message found');
		}
		
		// Get the table to log the email message in:
		// (we use the context of the email message)
		$table = $this->_getDbTable($mail->getContext());
		
		// Insert the entry into the table, and return the boolean flag as result
		$date = $entry->getTime();
		return $table->insert(array(
			'log_to'          => $mail->getRecipientsString(),
			'log_cc'          => $mail->getCcString(),
			'log_bcc'         => $mail->getBccString(),
			'log_attachments' => '',
			'log_subject'     => $mail->getSubject(),
			'log_body'        => $mail->getBody(),
			'log_time'        => $date->timestamp
		));
	}
	
	/**
	 * Get new entry
	 * 
	 * @see MI_MailLogger::getNewEntry()
	 * @access public
	 * @return MI_MailLoggerEntry
	 */
	public function getNewEntry() {
		$entry = new M_MailLoggerEntry();
		$entry->setLogger($this);
		$entry->setTime(new M_Date());
		return $entry;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Database table
	 * 
	 * Will provide with the database table where the log entry will be stored.
	 * Note that, if the table does not yet exist, the table will be created first.
	 * 
	 * @access protected
	 * @param string $context
	 * 		The context of the email message. Is used to compose the database
	 * 		table name.
	 * @return MI_DbTable
	 */
	protected function _getDbTable($context = NULL) {
		// Parse a string suffix:
		if(! $context) {
			$context = '_default';
		} else {
			$context = '_' . M_Helper::trimCharlist($context, '_');
		}
		
		// With the suffix, we compose the name of the table:
		$dbTableName = 'log_mail' . $context;
		
		// Get the database connection:
		$db = M_Db::getInstance();
		
		// Check if the table exists:
		if($db->hasTable($dbTableName)) {
			$table = $db->getTable($dbTableName);
		}
		// If the table does not exist:
		else {
			// Create the table first:
			$table = $this->_getNewDbTable($db, $dbTableName);
			$table->create();
		}
		
		// Return the table:
		return $table;
	}
	
	/**
	 * Get new database table
	 * 
	 * Called to create the log table, if not yet created
	 * 
	 * @access protected
	 * @param MI_Db $db
	 * 		The database connection
	 * @param string $tableName
	 * 		The table name
	 * @return MI_DbTable
	 */
	protected function _getNewDbTable(MI_Db $db, $tableName) {
		// Ask the database connection for a new table, with the specified name:
		$table = $db->getNewTable();
		$table->setName($tableName);
		
		// Create the columns of the table:
		$id = new M_DbColumn('log_id');
		$id->setIsAutoIncrement(TRUE);
		$id->setType(M_DbColumn::TYPE_INTEGER);
		$table->addColumn($id);
		
		$to = new M_DbColumn('log_to');
		$to->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($to);
		
		$cc = new M_DbColumn('log_cc');
		$cc->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($cc);
		
		$bcc = new M_DbColumn('log_bcc');
		$bcc->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($bcc);
		
		$attachments = new M_DbColumn('log_attachments');
		$attachments->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($attachments);
		
		$subject = new M_DbColumn('log_subject');
		$subject->setType(M_DbColumn::TYPE_VARCHAR);
		$subject->setLength(255);
		$table->addColumn($subject);
		
		$body = new M_DbColumn('log_body');
		$body->setType(M_DbColumn::TYPE_TEXT);
		$table->addColumn($body);
		
		$time = new M_DbColumn('log_time');
		$time->setType(M_DbColumn::TYPE_INTEGER);
		$time->setDefaultValue(0);
		$table->addColumn($time);
		
		// Create the indexes of the table:
		$primary = new M_DbIndex('PRIMARY');
		$primary->setType(M_DbIndex::PRIMARY);
		$primary->addColumn($id);
		$table->setPrimaryKey($primary);
		
		// Return the table:
		return $table;
	}
}