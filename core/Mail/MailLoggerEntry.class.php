<?php
/**
 * M_MailLoggerEntry class
 * 
 * M_MailLoggerEntry is an implementation of the {@link MI_MailLoggerEntry} 
 * interface. This (general) implementation could be used in combination with
 * any implementation of {@link MI_MailLogger}.
 * 
 * @package Core
 */
class M_MailLoggerEntry extends M_Object implements MI_MailLoggerEntry {
	/**
	 * Logger
	 * 
	 * This property stores the logger object. For more information, read the 
	 * docs on {@link M_MailLoggerEntry::setLogger()}
	 * 
	 * @access protected
	 * @var MI_MailLogger
	 */
	protected $_logger;
	
	/**
	 * Mail
	 * 
	 * This property stores the email message that should be contained by the
	 * log entry. For more info, read the docs on {@link M_MailLogEntry::setMail()}
	 * 
	 * @access protected
	 * @var M_Mail
	 */
	protected $_mail;
	
	/**
	 * Date + time
	 * 
	 * This property stores the date + time of the log entry.
	 * 
	 * @see M_MailLoggerEntry::setTime()
	 * @see M_MailLoggerEntry::getTime()
	 * @access protected
	 * @var M_Date
	 */
	protected $_time;
	
	/**
	 * Set logger
	 * 
	 * Will set the logger that is in charge of saving the entry. Note that the
	 * logger is represented by an object that implements the {@link MI_MailLogger}
	 * interface.
	 * 
	 * @see MI_MailLoggerEntry::setLogger()
	 * @access public
	 * @param MI_MailLogger $logger
	 * @return void
	 */
	public function setLogger(MI_MailLogger $logger) {
		$this->_logger = $logger;
	}
	
	/**
	 * Set email message
	 * 
	 * Will set the email message that should be contained by the log entry.
	 * 
	 * @see MI_MailLoggerEntry::setMail()
	 * @access public
	 * @param M_Mail $mail
	 * 		The email message
	 * @return void
	 */
	public function setMail(M_Mail $mail) {
		$this->_mail = $mail;
	}
	
	/**
	 * Set time
	 * 
	 * Will set the date + time of the log entry
	 * 
	 * @see MI_MailLoggerEntry::setTime()
	 * @access public
	 * @param M_Date $time
	 * @return void
	 */
	public function setTime(M_Date $time) {
		$this->_time = $time;
	}
	
	/**
	 * Get logger
	 * 
	 * Will provide with the logger that has been set previously with
	 * {@link M_MailLoggerEntry::setLogger()}.
	 * 
	 * @see MI_MailLoggerEntry::getLogger()
	 * @access public
	 * @return MI_MailLogger
	 */
	public function getLogger() {
		return $this->_logger;
	}
	
	/**
	 * Get email message
	 * 
	 * Will provide with the email message that is contained by the log entry.
	 * 
	 * @see MI_MailLoggerEntry::getMail()
	 * @access public
	 * @return M_Mail
	 */
	public function getMail() {
		return $this->_mail;
	}
	
	/**
	 * Get date + time
	 * 
	 * Will provide with the date + time of the log entry. The date + time is
	 * represented by an instance of {@link M_Date}.
	 * 
	 * @see MI_MailLoggerEntry::getTime()
	 * @access public
	 * @return M_Date
	 */
	public function getTime() {
		return $this->_time;
	}
	
	/**
	 * Save the entry
	 * 
	 * Will save the log entry to the log. This method will ask the logger object
	 * (previously set with {@link M_MailLoggerEntry::setLogger()}) to save the
	 * entry to the log.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function save() {
		$logger = $this->getLogger();
		if(! $logger) {
			throw new M_MailException('Cannot save the log entry; missing logger object!');
		}
		$logger->addEntry($this);
	}
}