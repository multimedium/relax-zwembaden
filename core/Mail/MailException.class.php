<?php
/**
 * M_MailException class
 * 
 * Is employed to represent the exceptions thrown by {@link M_Mail}.
 * 
 * @package Core
 */
class M_MailException extends M_Exception {
}