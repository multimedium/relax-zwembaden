<?php
/**
 * M_Mail class
 * 
 * @package Core
 */
class M_Mail extends M_Object {

	/**
	 * Priority constants
	 *
	 * This constants holds the priority integers that are used
	 * in the email message
	 */
	const PRIORITY_LOW = 5;
	const PRIORITY_NORMAL = 3;
	const PRORITY_HIGH = 1;
	
	/**
	 * Encoding constatns
	 * 
	 * Holds the different encodings used for embedded attachments in the e-mail
	 * message
	 */
	const ENCODING_BASE64 = 'base64';
	
	/**
	 * Recipients
	 * 
	 * This property holds the recipients that will receive the
	 * email message.
	 * 
	 * @see M_Mail::addRecipient()
	 * @access public
	 * @var array
	 */
	private $_to = array();
	
	/**
	 * CC
	 * 
	 * This property holds the recipients that will receive the
	 * email message, in Carbon Copy.
	 * 
	 * @see M_Mail::addCc()
	 * @access public
	 * @var array
	 */
	private $_cc = array();
	
	/**
	 * BCC
	 * 
	 * This property holds the recipients that will receive the
	 * email message, in Blind Carbon Copy.
	 * 
	 * @see M_Mail::addBcc()
	 * @access public
	 * @var array
	 */
	private $_bcc = array();
	
	/**
	 * Reply to
	 * 
	 * This property holds the recipients that will receive the
	 * reply message.
	 * 
	 * @see M_Mail::setReplyTo()
	 * @access public
	 * @var array
	 */
	private $_replyTo = array(NULL, NULL);
	
	/**
	 * Attachments
	 * 
	 * This property stores the references to the attachments that have
	 * been added to the email message.
	 * 
	 * @see M_Mail::addAttachment()
	 * @access public
	 * @var array
	 */
	private $_attachments = array();
	
	/**
	 * Embedded attachments
	 * 
	 * This property stores the references to the embedded attachments that have
	 * been added to the email message.
	 * 
	 * @see M_Mail::addEmbeddedAttachment()
	 * @access public
	 * @var array
	 */
	private $_embeddedAttachments = array();
	
	/**
	 * Subject
	 * 
	 * This property holds the subject of the email message.
	 * 
	 * @see M_Mail::setSubject()
	 * @access public
	 * @var string
	 */
	private $_subject;
	
	/**
	 * Message Body
	 * 
	 * This property holds the body (message) of the email message.
	 * 
	 * @see M_Mail::setBody()
	 * @access private
	 * @var string
	 */
	private $_body;
	
	/**
	 * Context
	 * 
	 * This property stores the context of the email message. For more info,
	 * read the docs on {@link M_Mail::setContext()}.
	 * 
	 * @access private
	 * @var string
	 */
	private $_context;
	
	/**
	 * Mailer
	 * 
	 * This property stores the mailer that has been assigned to the email
	 * message. This mailer is in charge of sending out the email message.
	 * 
	 * @access private
	 * @var MI_Mailer
	 */
	private $_mailer;
	
	/**
	 * Alternative text
	 * 
	 * This property stores the alternative (plain) text that has been assigned to the email
	 * message. This text will be viewed when the recipient doens't allow or support HTML-mails
	 * 
	 * @access private
	 * @var string
	 */
	private $_altBody;
	
	/**
	 * Return path
	 * 
	 * This property stores the return path that has been assigned to the email
	 * message. Bounced e-mails will be returned to this e-mailaddress.
	 * 
	 * @access private
	 * @var string
	 */
	private $_returnPath;

	/**
	 * Priority
	 *
	 * This property stores the priority that has been assign to the email message.
	 *
	 * @var int
	 */
	private $_priority = self::PRIORITY_NORMAL;

	/**
	 * Custom headers
	 *
	 * This property stores the custom headers assigned to the email message.
	 *
	 * @var array
	 */
	private $_customHeaders = array();
	
	/**
	 * Embed images
	 * 
	 * This property stores the flag to embed images or not
	 * 
	 * @var bool
	 */
	private $_embedImages = FALSE;
	
	/**
	 * From
	 * 
	 * This property holds the name and email address of the entity
	 * or person that is sending the email message. Typically, this
	 * information equals the application's title and address.
	 * 
	 * Check {@link M_Mail::setFrom()} to learn more.
	 * 
	 * @access private
	 * @static
	 * @var array
	 */
	private static $_from = NULL;

	
	/**
	 * Constructor
	 * 
	 * NOTE:
	 * Read {@link M_Mail::setFrom()} to learn more about the default
	 * value of {@link M_Mail::$_from}.
	 * 
	 * @access public
	 * @param string $recipients
	 * 		The recipients that will receive the email message
	 * @param string $subject
	 * 		The subject of the email message
	 * @param string $body
	 * 		The message body
	 * @return M_Mail
	 */
	public function __construct($recipient = NULL, $subject = NULL, $body = NULL) {
		// Recipients:
		if($recipient) {
			$this->addRecipient($recipient);
		}
		
		// Subject:
		if($subject) {
			$this->setSubject($subject);
		}
		
		// Message body:
		if($body) {
			$this->setBody($body);
		}
	}
	
	/**
	 * Set context
	 * 
	 * An email message is always sent in a given context. An application may want
	 * to send out messages to notify about significant system events, send a 
	 * users's comments via email, etc... 
	 * 
	 * You can assign a context to any email message, by using this method. For
	 * example: consider a contact form, that sends out an email message with the
	 * visitor's comments:
	 * 
	 * <code>
	 *    $mail = new M_Mail;
	 *    $mail->setContext('contact');
	 * </code>
	 * 
	 * The example above assigns the context 'contact' to the email message. This
	 * context identifier (any custom string) can then be used by loggers, to 
	 * group email messages by context.
	 * 
	 * @access public
	 * @param string $context
	 * 		The context of the email message
	 * @return void
	 */
	public function setContext($context) {
		$this->_context = (string) $context;
	}
	
	/**
	 * Set mailer
	 * 
	 * Can be used to set the mailer. The mailer is an object that implements
	 * the {@link MI_Mailer} interface, and is in charge of sending the mail.
	 * 
	 * @access public
	 * @param MI_Mailer $mailer
	 * @return void
	 */
	public function setMailer(MI_Mailer $mailer) {
		$this->_mailer = $mailer;
	}
	
	/**
	 * Set From
	 * 
	 * This method will set the name and email address of the entity
	 * or person that is sending the email message. Typically, this
	 * information equals the application's title and address.
	 * 
	 * If this property has not been set by {@link M_Mail::setFrom()},
	 * {@link M_Mail} will default this property to the values it may
	 * find in the Application Identity ({@link M_ApplicationIdentity}):
	 * 
	 * The default name of the entity or person:
	 * 
	 * <code>
	 *    $identity = M_ApplicationIdentity::getInstance();
	 *    $identity->getContact()->getEmail()->getName();
	 * </code>
	 * 
	 * The default email address of the entity or person:
	 * 
	 * <code>
	 *    $identity = M_ApplicationIdentity::getInstance();
	 *    $identity->getContact()->getEmail()->getEmail();
	 * </code>
	 * 
	 * Consequently, an exception will be thrown if {@link M_ApplicationIdentity}
	 * fails to load the application's identity details.
	 * 
	 * NOTE:
	 * Read {@link M_MailHelper::getRecipientsFromString()} to learn more on how 
	 * the info is expected by this method.
	 * 
	 * @access public
	 * @param string $from
	 * 		The information about the sender. A recipient string is expected. See
	 * 		{@link M_MailHelper::getRecipientStringFormat()}
	 * @return void
	 */
	public function setFrom($from) {
		$r = M_MailHelper::getRecipientsFromString($from);
		if(count($r) > 0) {
			self::$_from = array($r->key(), $r->current());
		} else {
			throw new M_MailException(sprintf(
				'Cannot set email sender with empty string!'
			));
		}
	}
	
	/**
	 * Set Reply-To
	 * 
	 * This method will set the name and email address of the entity or person
	 * to which the reply message should be sent.
	 * 
	 * @access public
	 * @param string $recipient
	 * 		The information about the receiver. A recipient string is expected. See
	 * 		{@link M_MailHelper::getRecipientStringFormat()}
	 * @return void
	 */
	public function setReplyTo($recipient) {
		$r = M_MailHelper::getRecipientsFromString($recipient);
		if(count($r) > 0) {
			$this->_replyTo = array($r->key(), $r->current());
		} else {
			throw new M_MailException(sprintf(
				'Cannot set Reply-To with string "%s"',
				(string) $recipient
			));
		}
	}
	
	/**
	 * Add CC
	 * 
	 * This method will add recipient(s) in Carbon Copy.
	 * 
	 * NOTE:
	 * Read {@link M_MailHelper::getRecipientsFromString()} to learn more on how 
	 * the info is expected by this method.
	 * 
	 * @access public
	 * @param string $recipient
	 * 		The recipient(s) in CC
	 * @return void
	 */
	public function addCc($recipient) {
		foreach(M_MailHelper::getRecipientsFromString($recipient) as $address => $name) {
			$this->_cc[$address] = $name;
		}
	}
	
	/**
	 * Clear CC
	 * 
	 * This method will remove all recipients that may have been added by 
	 * {@link M_Mail::addCc()}
	 * 
	 * @access public
	 * @return void
	 */
	public function clearCc() {
		$this->_cc = array();
	}
	
	/**
	 * Add BCC
	 * 
	 * This method will add recipient(s) in Blind Carbon Copy.
	 * 
	 * NOTE:
	 * Read {@link M_MailHelper::getRecipientsFromString()} to learn more on how 
	 * the info is expected by this method.
	 * 
	 * @access public
	 * @param string $recipient
	 * 		The recipient(s) in BCC
	 * @return void
	 */
	public function addBcc($recipient) {
		foreach(M_MailHelper::getRecipientsFromString($recipient) as $address => $name) {
			$this->_bcc[$address] = $name;
		}
	}
	
	/**
	 * Clear BCC
	 * 
	 * This method will remove all recipients that may have been added by 
	 * {@link M_Mail::addBcc()}
	 * 
	 * @access public
	 * @return void
	 */
	public function clearBcc() {
		$this->_bcc = array();
	}
	
	/**
	 * Add recipient
	 * 
	 * This method will add recipient(s). These recipients are 
	 * added to the "To: " header. 
	 * 
	 * NOTE:
	 * Read {@link M_MailHelper::getRecipientsFromString()} to learn more on how 
	 * the info is expected by this method.
	 * 
	 * @access public
	 * @param string $recipient
	 * 		The recipient(s)
	 * @return void
	 */
	public function addRecipient($recipient) {
		foreach(M_MailHelper::getRecipientsFromString($recipient) as $address => $name) {
			$this->_to[$address] = $name;
		}
	}
	
	/**
	 * Clear recipients
	 * 
	 * This method will remove all recipients that may have been added by 
	 * {@link M_Mail::addRecipient()}
	 * 
	 * @access public
	 * @return void
	 */
	public function clearRecipients() {
		$this->_to = array();
	}
	
	/**
	 * Add attachment
	 * 
	 * This method will add an attachment to the email message.
	 * 
	 * @access public
	 * @throws M_MailException
	 * @param M_File $file
	 * 		The file to be attached, represented by an object that implements
	 * 		the {@link MI_FsItemFile} interface.
	 * @param string $name
	 * 		Filename (optional). If provided, this filename will be used to
	 * 		override the file's filename.
	 * @return void
	 */
	public function addAttachment(MI_FsItemFile $file, $filename = NULL) {
		if(! $file->exists()) {
			throw new M_MailException(sprintf(
				'Cannot attach %s to a email; Cannot find the file',
				$file->getPath()
			));
		}
		
		// Set the filename:
		$finalFilename = $filename ? $filename : $file->getPath();
		
		// Add reference to the attachment:
		$this->_attachments[$finalFilename] = $file;
	}
	
	 /**
	 * Adds an embedded attachment to the e-mail message.  This can include 
	 * images, sounds, and just about any other document.
	 * 
	 * Use the $cid parameter to use the file in the html-message.
	 *
	 * @param MI_FsItemFile $file 
	 *		The file which needs to be embedded to the mail
	 * @param string $cid 
	 *		Content ID of the attachment. Use this to identify the Id for 
	 *		accessing the image in HTML.
	 * @param string $name
	 *		Overrides the attachment name
	 * @param string $encoding
	 *		File encoding (default base64)
	 * @param string $type 
	 *		MIME type (e.g. image/jpeg or image/gif). Leave empty to autodetect 
	 *		the mime-type
	 */
	public function addEmbeddedAttachment(MI_FsItemFile $file, $cid, $name = NULL, $encoding = self::ENCODING_BASE64, $type = NULL) {
		if(! $file->exists()) {
			throw new M_MailException(sprintf(
				'Cannot attach %s to a email; Cannot find the file',
				$file->getPath()
			));
		}
		
		//if no specific type has been set: extract if from the file itself
		if(is_null($type)) {
			$type = $file->getMimeType();
		}
		
		//check if cid follows url specification
		//@see http://www.ietf.org/rfc/rfc2392.txt
		if(!strpos($cid, '@')) {
			$cid .= '@'.M_Request::getUri()->getDomainName();
		}
		
		// Add reference to the embedded attachment:
		$this->_embeddedAttachments[$cid] = array(
			$file,
			$cid,
			$name,
			$encoding,
			$type
		);
	}
	
	/**
	 * Set subject
	 * 
	 * This method will set the subject of the email message.
	 * 
	 * NOTE:
	 * A mail forms part of your interface, so you should probably
	 * pass in translated strings. To learn more about translated
	 * strings, read the docs on
	 * 
	 * - {@link M_Locale}
	 * - {@link M_LocaleMessageCatalog}
	 * - {@link t()}
	 * 
	 * @access public
	 * @param string $subject
	 * 		The subject of the email message
	 * @return void
	 */
	public function setSubject($subject) {
		$this->_subject = (string) $subject;
	}
	
	/**
	 * Set body
	 * 
	 * This method will set the body of the email message.
	 * 
	 * NOTE:
	 * A mail forms part of your interface, so you should probably
	 * pass in translated texts. To learn more about translated
	 * texts, read the docs on
	 * 
	 * - {@link M_Locale}
	 * - {@link M_LocaleMessageCatalog}
	 * - {@link t()}
	 * 
	 * NOTE:
	 * This method also supports any object that provides with an
	 * implementation of the magic __toString(). So, you can also pass
	 * in an {@link M_View} object.
	 * 
	 * @access public
	 * @param string $body
	 * 		The body of the email message
	 * @return void
	 */
	public function setBody($body) {
		$this->_body = stripslashes((string) $body);
		
		//autoset alt body when still empty
		if(!$this->_altBody) {
			$this->_altBody = M_MailHelper::getAltText($this->_body);
		}
	}
	
	/**
	 * Set alt body
	 * 
	 * This method will set the alternative text of the email message.
	 * 
	 * NOTE:
	 * This text will be viewed if the recipient doesn't support or allow
	 * HTML-formed e-mail messages
	 * 
	 * NOTE:
	 * A mail forms part of your interface, so you should probably
	 * pass in translated texts. To learn more about translated
	 * texts, read the docs on
	 * 
	 * - {@link M_Locale}
	 * - {@link M_LocaleMessageCatalog}
	 * - {@link t()}
	 * 
	 * NOTE:
	 * This method also supports any object that provides with an
	 * implementation of the magic __toString(). So, you can also pass
	 * in an {@link M_View} object.
	 * 
	 * @access public
	 * @param string $altBody
	 * 		The alternative body of the email message
	 * @return void
	 */
	public function setAltBody($altBody) {
		$this->_altBody = (string) $altBody;
	}
	
	/**
	 * Set return path
	 * 
	 * This method will set the return path of the email message.
	 * 
	 * NOTE:
	 * You might need to add a return-path mail header to trouble shoot bounced messages.
	 * 
	 * @access public
	 * @param string $returnPath
	 * 		The body of the email message
	 * @return void
	 */
	public function setReturnPath($returnPath) {
		$this->_returnPath = (string) $returnPath;
	}

	/**
	 * Set priority
	 * (1 = High, 3 = Normal, 5 = low).
	 *
	 * Please use PRIORITY_* constants
	 *
	 * @access public
	 * @param int $priority
	 * @return void
	 */
	public function setPriority($priority) {
		$this->_priority = (int) $priority;
	}
	
	/**
	 * Set embed images
	 * 
	 * @access public
	 * @param bool $flag
	 * @return void 
	 */
	public function setEmbedImages($flag) {
		$this->_embedImages = (bool) $flag;
	}

	/**
	 * Add custom header to the email message
	 * 
	 * @param string $name
	 * @param string $value
	 */
	public function addCustomHeader($name, $value = null) {
		$this->_customHeaders[$name] = $value;
	}
	
	/**
	 * Get context
	 * 
	 * Will provide with the context of the email message. For more information,
	 * read the docs on {@link M_Mail::setContext()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getContext() {
		return $this->_context;
	}
	
	/**
	 * Get from name
	 * 
	 * Will provide with the current sender's name. For more information,
	 * read the docs on {@link M_Mail::setFrom()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getFromName() {
		self::_initHeaderFrom();
		return self::$_from[1];
	}
	
	/**
	 * Get from email
	 * 
	 * Will provide with the current sender's email address. For more information,
	 * read the docs on {@link M_Mail::setFrom()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getFromEmail() {
		self::_initHeaderFrom();
		return self::$_from[0];
	}
	
	/**
	 * Get Reply-To name
	 * 
	 * Will provide with the name that has been set previously with
	 * {@link M_Mail::setReplyTo()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getReplyToName() {
		return $this->_replyTo[1];
	}
	
	/**
	 * Get Reply-To email address
	 * 
	 * Will provide with the email address that has been set previously with
	 * {@link M_Mail::setReplyTo()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getReplyToEmail() {
		return $this->_replyTo[0];
	}
	
	/**
	 * Get recipients
	 * 
	 * Will return an iterator with the recipients that may have been added to
	 * the email previously, with {@link M_Mail::addRecipient()}.
	 * 
	 * Example 1
	 * <code>
	 *    $mail = new M_Mail;
	 *    $mail->addRecipient('Tom Bauwens <tom@multimedium.be>; Info <info@multimedium.be>');
	 *    foreach($mail->getRecipients() as $email => $name) {
	 *       echo "The email address of $name is $email<br>";
	 *    }
	 * </code>
	 * 
	 * Example 1 will generate the following output:
	 * <code>
	 *    The email address of Tom Bauwens is tom@multimedium.be<br>
	 *    The email address of Info is info@multimedium.be<br>
	 * </code>
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getRecipients() {
		return new ArrayIterator($this->_to);
	}
	
	/**
	 * Get recipients, in a string
	 * 
	 * Will return the recipients of the email message, in a string.
	 * 
	 * @see M_Mail::getRecipients()
	 * @see M_MailHelper::getRecipientStringFormat()
	 * @access public
	 * @return string
	 */
	public function getRecipientsString() {
		$out = '';
		foreach($this->getRecipients() as $email => $name) {
			if(!empty($out)) {
				$out .= M_MailHelper::RECIPIENT_SEPARATOR;
			}
			$out .= M_MailHelper::getRecipientStringFormat($name, $email);
		}
		return $out;
	}
	
	/**
	 * Get CC
	 * 
	 * Will return an iterator with the recipients in CC that may have been added 
	 * to the email previously, with {@link M_Mail::addCc()}.
	 * 
	 * Example 1
	 * <code>
	 *    $mail = new M_Mail;
	 *    $mail->addCc('Tom Bauwens <tom@multimedium.be>; Info <info@multimedium.be>');
	 *    foreach($mail->getCc() as $email => $name) {
	 *       echo "The email address of $name is $email<br>";
	 *    }
	 * </code>
	 * 
	 * Example 1 will generate the following output:
	 * <code>
	 *    The email address of Tom Bauwens is tom@multimedium.be<br>
	 *    The email address of Info is info@multimedium.be<br>
	 * </code>
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getCc() {
		return new ArrayIterator($this->_cc);
	}
	
	/**
	 * Get CC, in a string
	 * 
	 * Will return the recipients in CC of the email message, in a string.
	 * 
	 * @see M_Mail::getCc()
	 * @see M_MailHelper::getRecipientStringFormat()
	 * @access public
	 * @return string
	 */
	public function getCcString() {
		$out = '';
		foreach($this->getCc() as $email => $name) {
			if(!empty($out)) {
				$out .= M_MailHelper::RECIPIENT_SEPARATOR;
			}
			$out .= M_MailHelper::getRecipientStringFormat($name, $email);
		}
		return $out;
	}
	
	/**
	 * Get BCC
	 * 
	 * Will return an iterator with the recipients in BCC that may have been added 
	 * to the email previously, with {@link M_Mail::addBcc()}.
	 * 
	 * Example 1
	 * <code>
	 *    $mail = new M_Mail;
	 *    $mail->addBcc('Tom Bauwens <tom@multimedium.be>; Info <info@multimedium.be>');
	 *    foreach($mail->getBcc() as $email => $name) {
	 *       echo "The email address of $name is $email<br>";
	 *    }
	 * </code>
	 * 
	 * Example 1 will generate the following output:
	 * <code>
	 *    The email address of Tom Bauwens is tom@multimedium.be<br>
	 *    The email address of Info is info@multimedium.be<br>
	 * </code>
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getBcc() {
		return new ArrayIterator($this->_bcc);
	}
	
	/**
	 * Get BCC, in a string
	 * 
	 * Will return the recipients in BCC of the email message, in a string.
	 * 
	 * @see M_Mail::getBcc()
	 * @see M_MailHelper::getRecipientStringFormat()
	 * @access public
	 * @return string
	 */
	public function getBccString() {
		$out = '';
		foreach($this->getBcc() as $email => $name) {
			if(!empty($out)) {
				$out .= M_MailHelper::RECIPIENT_SEPARATOR;
			}
			$out .= M_MailHelper::getRecipientStringFormat($name, $email);
		}
		return $out;
	}
	
	/**
	 * Get attachments
	 * 
	 * Will return an M_ArrayIterator with the email's attachments. The keys of 
	 * the iterator are the filenames, the elements are objects that implement
	 * the {@link MI_FsItemFile} interface.
	 * 
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getAttachments() {
		return new M_ArrayIterator($this->_attachments);
	}
	
	
	/**
	 * Get embedded attachments
	 * 
	 * Will return an M_ArrayIterator with the email's embedded attachments. 
	 * The keys of the iterator are the cid's, the elements themselves are arrays
	 * with the following data:
	 * - file (M_File)
	 * - cid
	 * - name
	 * - encoding
	 * - MIME type
	 * 
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getEmbeddedAttachments() {
		return new M_ArrayIterator($this->_embeddedAttachments);
	}
	
	/**
	 * Get subject
	 * 
	 * Will return the subject of the email, previously set with the method
	 * {@link M_Mail::setSubject()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getSubject() {
		return $this->_subject;
	}
	
	/**
	 * Get body
	 * 
	 * Will return the body of the email, previously set with the method
	 * {@link M_Mail::setBody()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getBody() {
		return $this->_body;
	}
	
	/**
	 * Get alt body
	 * 
	 * Will return the alternative text of the email, previously set with the method
	 * {@link M_Mail::setAltBody()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getAltBody() {
		return $this->_altBody;
	}
	
	/**
	 * Get return path
	 * 
	 * Will return the return path of the email, previously set with the method
	 * {@link M_Mail::setReturnPath()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getReturnPath() {
		return $this->_returnPath;
	}

	/**
	 * Get priority
	 *
	 * Will return the priority of the email, previously set with the method
	 * {@link M_Mail::setPriority()}.
	 * 
	 * @return int
	 */
	public function getPriority() {
		return $this->_priority;
	}

	/**
	 * Get embed images
	 * 
	 * Will return the flag to embed images or not, previously set with the method
	 * {@link M_Mail::setEmbedImages()}
	 * 
	 * @return type 
	 */
	public function getEmbedImages() {
		return $this->_embedImages;
	}
	
	/**
	 * Get mailer
	 * 
	 * Will return the mailer that is in charge of sending the mail, maybe set
	 * previously with {@link M_Mail::setMailer()}.
	 * 
	 * NOTE:
	 * If no mailer has been set previously with {@link M_Mail::setMailer()},
	 * {@link M_Mail} will provide with an instance of {@link M_Mailer} by default.
	 * 
	 * @access public
	 * @return MI_Mailer
	 */
	public function getMailer() {
		// Return an instance of M_Mailer by default
		if(! $this->_mailer) {
			$this->_mailer = new M_Mailer;
		}
		
		// Return the mailer:
		return $this->_mailer;
	}
	
	/**
	 * Send the mail
	 * 
	 * This method will decorate the message body and send the email
	 * message to the recipients that have been set by
	 * 
	 * - {@link M_Mail::addRecipient()}
	 * - {@link M_Mail::addCc()}
	 * - {@link M_Mail::addBcc()}
	 * 
	 * This method will return TRUE on success, FALSE on failure.
	 * 
	 * @access public
	 * @return boolean
	 * 		TRUE on success, FALSE on failure
	 */
	public function send() {
		$mailer = $this->getMailer();
		$mailer->setMail($this);
		return $mailer->send();
	}
	
	/**
	 * Get error message
	 * 
	 * Will provide with the error message, if any.
	 * 
	 * @access public
	 * @return string
	 */
	public function getError() {
		return $this->getMailer()->getError();
	}

	/**
	 * Get custom headers
	 *
	 * Will provide with the custom headers, if any.
	 * 
	 * @return array
	 */
	public function getCustomHeaders() {
		return $this->_customHeaders;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Initiate the FROM Header
	 * 
	 * @access private
	 * @return void
	 */
	private function _initHeaderFrom() {
		// Default the sender, if not set yet:
		if(self::$_from == NULL) {
			// Ask the Application Identity for the email address:
			$identity = M_ApplicationIdentity::getInstance();
			$email    = $identity->getContact()->getEmail();
			
			// If an email address is provided by the ApplicationIdentity class
			if($email) {
				// Get name + address
				self::$_from = array(
					$email->getEmail(),
					$identity->getContact()->getName()
				);
				
				// Double-check:
				if(! self::$_from[0] || ! self::$_from[1]) {
					throw new M_Exception(sprintf(
						'%s could not retrieve "From" from M_ApplicationIdentity; Empty name/email.',
						__CLASS__
					));
				}
			}
			// If no email address is provided:
			else {
				throw new M_Exception(sprintf(
					'%s could not retrieve "From" from M_ApplicationIdentity',
					__CLASS__
				));
			}
		}
	}
}