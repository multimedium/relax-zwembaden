<?php
/**
 * MI_Mailer interface
 * 
 * The MI_Mailer interface dictates the interface that must be implemented by
 * mailers.
 * 
 * @package Core
 */
interface MI_Mailer {
	/**
	 * Set logger
	 * 
	 * Will set the object that is in charge of logging the email message. This
	 * object must implement the {@link MI_MailLogger} interface.
	 * 
	 * @access public
	 * @param MI_MailLogger $logger
	 * 		The email logger object
	 * @return void
	 */
	public function setLogger(MI_MailLogger $logger);
	
	/**
	 * Set email
	 * 
	 * Will set the email that is to be sent.
	 * 
	 * @access public
	 * @param M_Mail $mail
	 * 		The email message, represented by an instance of {@link M_Mail}
	 * @return void
	 */
	public function setMail(M_Mail $mail);
	
	/**
	 * Get logger
	 * 
	 * Will provide with the object that is in charge of logging the email message,
	 * previously set with {@link MI_Mailer::setLogger()}
	 * 
	 * NOTE:
	 * If no logger has been assigned previously, this method will return NULL.
	 * 
	 * @access public
	 * @return MI_MailLogger
	 */
	public function getLogger();
	
	/**
	 * Get mail
	 * 
	 * Will provide with the email message that should be sent by the mailer.
	 * 
	 * @access public
	 * @return M_Mail
	 */
	public function getMail();
	
	/**
	 * Send the email
	 * 
	 * Will send the email message that has been provided to the mailer previously,
	 * with {@link MI_Mailer::setMail()}. Also, if a logger has been provided to
	 * the mailer with {@link M_Mailer::setLogger()}, the mailer will tell the
	 * logger to log the email message.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function send();
	
	/**
	 * Get error message
	 * 
	 * Will provide with an error message (if any).
	 * 
	 * @see M_Mailer::getError()
	 * @access public
	 * @return string
	 */
	public function getError();
}