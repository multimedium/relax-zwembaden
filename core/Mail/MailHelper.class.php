<?php
/**
 * M_MailHelper class
 * 
 * @package Core
 */
class M_MailHelper extends M_Object {
	/**
	 * Recipient separator
	 * 
	 * This constant contains the separator characters that are used to render the 
	 * recipients' string format, by
	 * 
	 * - {@link M_Mail::getRecipientsString()},
	 * - {@link M_Mail::getCcString()}, and
	 * - {@link M_Mail::getBccString()}
	 */
	const RECIPIENT_SEPARATOR = '; ';
	
	/**
	 * Get recipients from string
	 * 
	 * This method will extract the recipient(s) from a given string. 
	 * The recipients in the string are separated by either one of the 
	 * following characters:
	 * 
	 * <code>, ; [space]</code>
	 * 
	 * So, a valid example of a string that provides with multiple
	 * recipients would be:
	 * 
	 * Example 1
	 * <code>tom@multimedium.be; info@multimedium.be</code>
	 * 
	 * Optionally, the string can define both the recipients' names 
	 * and email addresses. To define both the name and email address, 
	 * the string must have the following format:
	 * 
	 * <code>Name <email@address.com></code>
	 * 
	 * So, in order to describe both the names and email addresses of
	 * multiple recipients in a single string, we could rewrite the
	 * string in Example 1 as following:
	 * 
	 * Example 2
	 * <code>
	 *    Tom Bauwens <tom@multimedium.be>; Info <info@multimedium.be>
	 * </code>
	 * 
	 * This method will return an ArrayIterator with the names and addresses
	 * of each of the recipients. Check out Example 3 to learn more
	 * about the iterator's format.
	 * 
	 * Example 3
	 * <code>
	 *    $iterator = M_MailHelper::getRecipientsFromString('Tom Bauwens <tom@multimedium.be>; Info <info@multimedium.be>');
	 *    foreach($iterator as $email => $name) {
	 *       echo "The email address of $name is $email<br>";
	 *    }
	 * </code>
	 * 
	 * Example 3 will generate the following output:
	 * 
	 * <code>
	 *    The email address of Tom Bauwens is tom@multimedium.be<br>
	 *    The email address of Info is info@multimedium.be<br>
	 * </code>
	 * 
	 * NOTE:
	 * This method uses {@link M_ValidatorIsEmail} to make sure that the email 
	 * addresses are formatted correctly.
	 * 
	 * @static
	 * @throws M_MailException
	 * @access public
	 * @uses M_ValidatorIsEmail
	 * @see M_Mail::addRecipient();
	 * @see M_Mail::addBcc();
	 * @see M_Mail::addCc();
	 * @param string $recipient
	 * 		The string of recipient(s)
	 * @return ArrayIterator
	 */
	public static function getRecipientsFromString($recipient) {
		
		// The collection of email addresses that will be outputted by this method
		$output = array();

		// We prepare a validator object, which will be used to check for valid
		// email addresses:
		$validator = new M_ValidatorIsEmail;

		// We are expecting the recipient string in a given format. We run a
		// regular expression, that should extract all email addresses for us,
		// but first, we prepare an array where all of the matches will be
		// stored:
		$matches = array();

		// Run the expression:
		preg_match_all(
			// The pattern:
			'/("[^"]*"|[^@]*)?[\s<]{0,}[.0-9\pL_-]+@([0-9\pL-]+\.)+[0-9a-z]{2,15}[>\s;,]{0,}/im',
			// The string to search in
			$recipient,
			// The array where matches are stored:
			$matches
		);
		
		// For each of the recipients that we have found in the provided string:
		foreach($matches[0] as $recipient) {
			// The $recipient variable now contains the whole sub-string as a
			// match. From this string, we will have to extract both the name
			// and the email address. We clean up the string:
			$recipient = str_replace('<', ' ', M_Helper::trimCharlist($recipient, '<>";,'));
			
			// We know that the last piece in the string is the email address,
			// so we look for that piece now, by finding the last ocurrence of
			// a space character:
			$pos = strrpos($recipient, ' ');

			// If no white-space has been found
			if($pos === FALSE) {
				// Then we interpret the entire string as the email address:
				$email = $recipient;

				// And, we also assume that no name has been provided
				$name = '';
			}
			// If a white-space has been found:
			else {
				// Then, we extract the email address:
				$email = substr($recipient, $pos + 1);

				// And, the name of the recipient:
				$name = M_Helper::trimCharlist(substr($recipient, 0, $pos), '"');
			}
			
			// If the email is not empty, and if the email is a valid address:
			if(! empty($email) && $validator->check($email)) {
				// Then, we add the email address to the output collection:
				$output[$email] = $name;
			}
			// If the string is not a valid email address:
			else {
				// Then, we throw an exception:
				throw new M_MailException(sprintf(
					'Cannot get email recipient; invalid email address "<xmp>%s</xmp>"',
					$email
				));
			}
		}

		// Return the collection
		return new ArrayIterator($output);
	}
	
	/**
	 * Get one recipient from string
	 *
	 * This method will use {@link M_MailHelper::getRecipientsFromString()} in
	 * order to extract one recipient from the provided string. Note that, if
	 * more (or less) than one recipient has been found in the string, this
	 * method will throw an exception! In other words: exactly ONE recipient
	 * is expected.
	 *
	 * The result of this function is an array:
	 *
	 * <code>
	 *    Array (
	 *       0 => [Email],
	 *       1 => [Name]
	 *    )
	 * </code>
	 *
	 * @static
	 * @throws M_MailException
	 * @access public
	 * @param string $recipient
	 * @return array
	 */
	public static function getRecipientFromString($recipient) {
		// Get the recipients:
		$recipients = self::getRecipientsFromString($recipient)->getArrayCopy();

		// If not exactly one recipient has been found:
		if(count($recipients) != 1) {
			// Throw an exception
			throw new M_MailException(sprintf(
				'Expecting exactly one recipient in %s. A total of %s recipients ' .
				'has been found instead',
				$recipient,
				count($recipients)
			));
		}

		// Return the recipient
		return array(array_shift(array_keys($recipients)), array_shift($recipients));
	}
	
	/**
	 * Get recipient string
	 * 
	 * Will provide with the string format of a recipient.
	 * 
	 * Example 1
	 * <code>
	 *    echo M_MailHelper::getRecipientStringFormat('Tom Bauwens', 'tom@multimedium.be');
	 * </code>
	 * 
	 * Example 1 would produce the following output:
	 * <code>
	 *    Tom Bauwens <tom@multimedium.be>
	 * </code>
	 * 
	 * NOTE:
	 * This method uses {@link M_ValidatorIsEmail} to make sure that the email 
	 * addresses are formatted correctly.
	 * 
	 * @static
	 * @access public
	 * @param string $name
	 * 		The name of the recipient
	 * @param string $email
	 * 		The email address of the recipient
	 * @return string
	 */
	public static function getRecipientStringFormat($name, $email) {
		// First of all, we make sure that the provided email address is valid:
		$validator = new M_ValidatorIsEmail;
		
		// If not:
		if(! $validator->check($email)) {
			// We throw an exception:
			throw new M_MailException(sprintf(
				'Cannot make email recipient string format; invalid email address: %s',
				$email
			));
		}
		
		// Clean up the name:
		$name = M_Helper::trimCharlist($name);
		
		// If the name is not empty:
		if($name) {
			// Include the name in the recipient string
			return '"' . $name . '" <' . $email . '>';
		}
		// If the name is empty:
		else {
			// Include only email address
			return $email;
		}
	}
	
	/**
	 * Will convert html text to plain text
	 * 
	 * @param string $html
	 * @return string
	 */
	public static function getAltText($html) {
		//remove newlines
		$html = preg_replace("/\n+/", "", trim($html));
		
		//create br from <br/>
		$html = preg_replace('/<br(\s+)?\/?>/i', "\n", $html);
		
		//set href as anchortext
		$html = new M_HtmlDom($html);
		foreach($html->find('a') AS $link) {
			/* @var $link M_HtmlDomNode */
			$link->setInnerText($link->getAttribute('href'));
		}
		
		//remove html
		$filter = new M_FilterTextHtml(new M_FilterTextValue((string)$html));
		
		//trim
		return trim($filter->apply());
	}
}