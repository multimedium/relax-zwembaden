<?php
/**
 * M_ApplicationVersion
 * 
 * Read intro at {@link M_ApplicationVersion::__construct()}
 * 
 * @package Core
 */
class M_ApplicationVersion extends M_Object {
	/**
	 * Major
	 * 
	 * @see M_ApplicationVersion::__construct()
	 * @access protected
	 * @var integer
	 */
	protected $_major = 0;
	
	/**
	 * Minor
	 * 
	 * @see M_ApplicationVersion::__construct()
	 * @access protected
	 * @var integer
	 */
	protected $_minor = 0;
	
	/**
	 * Build
	 * 
	 * @see M_ApplicationVersion::__construct()
	 * @access protected
	 * @var integer
	 */
	protected $_build = 0;
	
	/**
	 * Constructor
	 * 
	 * In some schemes, sequence-based identifiers are used to convey the 
	 * significance of changes between releases: changes are classified by 
	 * significance level, and the decision of which sequence to change between 
	 * releases is based on the significance of the changes from the previous 
	 * release, whereby the first sequence is changed for the most significant 
	 * changes, and changes to sequences after the first represent changes of 
	 * decreasing significance.
	 * 
	 * For instance, in a scheme that uses a four-sequence identifier, the first 
	 * sequence may be incremented only when the code is completely rewritten, 
	 * while a change to the user interface or the documentation may only warrant 
	 * a change to the fourth sequence.
	 * 
	 * We use a three-sequence identifier, which is translated to the following
	 * meaning:
	 * 
	 * <code>
	 *    major.minor[.build]
	 * </code>
	 * 
	 * @access public
	 * @param integer $major
	 * @param integer $minor
	 * @param integer $build
	 * @return M_ApplicationVersion
	 */
	public function __construct($major = 0, $minor = 0, $build = 0) {
		$this->_major = (int) $major;
		$this->_minor = (int) $minor;
		$this->_build = (int) $build;
	}
	
	/**
	 * Construct with string
	 * 
	 * Will construct the version number from a given string. Read the intro about
	 * version numbers at {@link M_ApplicationVersion::__construct()}.
	 * 
	 * Example 1
	 * <code>
	 *    $version = M_ApplicationVersion::constructWithString('2,5,1');
	 *    echo $version->getMajor(); // 2
	 *    echo $version->getMinor(); // 5
	 *    echo $version->getBuild(); // 1
	 * </code>
	 * 
	 * @access public
	 * @param string $string
	 * 		The version number, in a string
	 * @return M_ApplicationVersion
	 */
	public function constructWithString($string) {
		$parts   = preg_split('/[\.\-,]+/', $string);
		$version = new M_ApplicationVersion();
		if(isset($parts[0])) {
			$version->setMajor($parts[0]);
		}
		if(isset($parts[1])) {
			$version->setMinor($parts[1]);
		}
		if(isset($parts[2])) {
			$version->setBuild($parts[2]);
		}
		return $version;
	}
	
	/**
	 * Set Major
	 * 
	 * @see M_ApplicationVersion::__construct()
	 * @access public
	 * @param integer $number
	 * @return integer
	 */
	public function setMajor($number) {
		$this->_major = (int) $number;
	}
	
	/**
	 * Set Minor
	 * 
	 * @see M_ApplicationVersion::__construct()
	 * @access public
	 * @param integer $number
	 * @return integer
	 */
	public function setMinor($number) {
		$this->_minor = (int) $number;
	}
	
	/**
	 * Set Build
	 * 
	 * @see M_ApplicationVersion::__construct()
	 * @access public
	 * @param integer $number
	 * @return integer
	 */
	public function setBuild($number) {
		$this->_build = (int) $number;
	}
	
	/**
	 * Get Major
	 * 
	 * @access public
	 * @return integer
	 */
	public function getMajor() {
		return $this->_major;
	}
	
	/**
	 * Get Minor
	 * 
	 * @access public
	 * @return integer
	 */
	public function getMinor() {
		return $this->_minor;
	}
	
	/**
	 * Get Build
	 * 
	 * @access public
	 * @return integer
	 */
	public function getBuild() {
		return $this->_build;
	}
	
	/**
	 * Is greater than?
	 * 
	 * @access public
	 * @param M_ApplicationVersion $version
	 * 		The version number to compare with
	 * @return bool $flag
	 * 		Returns TRUE if the version is greater than the provided one
	 */
	public function isAfter(M_ApplicationVersion $version) {
		if((int) $this->_major >= (int) $version->getMajor()) {
			if((int) $this->_minor >= (int) $version->getMinor()) {
				if((int) $this->_build > (int) $version->getBuild()) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}
	
	/**
	 * Is lesser than?
	 * 
	 * @uses M_ApplicationVersion::isAfter()
	 * @access public
	 * @param M_ApplicationVersion $version
	 * 		The version number to compare with
	 * @return bool $flag
	 * 		Returns TRUE if the version is lesser than the provided one
	 */
	public function isBefore(M_ApplicationVersion $version) {
		return $version->isAfer($this);
	}
	
	/**
	 * Equals?
	 * 
	 * @access public
	 * @param M_ApplicationVersion $version
	 * 		The version number to compare with
	 * @return bool $flag
	 * 		Returns TRUE if the version equals the provided one
	 */
	public function equals(M_ApplicationVersion $version) {
		if((int) $this->_major == (int) $version->getMajor()) {
			if((int) $this->_minor == (int) $version->getMinor()) {
				if((int) $this->_build == (int) $version->getBuild()) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}
	
	/**
	 * Get complete version number
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		$op  = $this->_major ? $this->_major : '0';
		$op .= ',' . ($this->_minor ? $this->_minor : '0');
		$op .= ',' . ($this->_build ? $this->_build : '0');
		return $op;
	}
	
	/**
	 * String caster
	 * 
	 * @access public
	 * @return string
	 */
	public function __toString() {
		return $this->toString();
	}
}