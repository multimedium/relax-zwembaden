<?php
/**
 * M_ApplicationReleaseNote class
 * 
 * M_ApplicationReleaseNote can be used to describe the properties of an application's
 * release note. A release note contains:
 * 
 * - The version number
 * - The date of the release
 * - The description of the release
 * 
 * @package Core
 */
class M_ApplicationReleaseNote {
	/**
	 * The version number
	 * 
	 * @access protected
	 * @var M_ApplicationVersion
	 */
	protected $_version;
	
	/**
	 * The date of the release
	 * 
	 * @access protected
	 * @var M_Date
	 */
	protected $_date;
	
	/**
	 * The description of the release
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_description;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_ApplicationReleaseNote
	 */
	public function __construct() {
	}
	
	/**
	 * Set version number
	 * 
	 * @access public
	 * @param M_ApplicationVersion $version
	 * @return void
	 */
	public function setVersion(M_ApplicationVersion $version) {
		$this->_version = $version;
	}
	
	/**
	 * Set Date
	 * 
	 * @access public
	 * @param M_Date $date
	 * @return void
	 */
	public function setDate(M_Date $date) {
		$this->_date = $date;
	}
	
	/**
	 * Set description
	 * 
	 * @access public
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
	}
	
	/**
	 * Get version number
	 * 
	 * @access public
	 * @return M_ApplicationVersion
	 */
	public function getVersion() {
		return $this->_version;
	}
	
	/**
	 * Get date
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getDate() {
		return $this->_date;
	}
	
	/**
	 * Get description
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
}