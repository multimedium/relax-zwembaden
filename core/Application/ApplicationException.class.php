<?php
/**
 * M_ApplicationException
 * 
 * Exceptions thrown by {@link M_Application} and related classes.
 *
 * @package Core
 */
class M_ApplicationException extends M_Exception {
}