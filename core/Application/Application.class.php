<?php
/**
 * M_Application class
 * 
 * M_Application can be used to get information about the installed
 * application.
 * 
 * @package Core
 */
class M_Application {
	/**
	 * Module XML File
	 *
	 * This property stores the SimpleXMLElement object that helps the
	 * {@link M_Application} class in reading the application's info.xml
	 * file.
	 * 
	 * The application's info file is located at:
	 * <code>/application/info.xml</code>
	 * 
	 * @static
	 * @access private
	 * @var SimpleXMLElement
	 */
	private static $_xml;
	
	/**
	 * Get the name of the application
	 * 
	 * This method will provide with the name of the application. The name of the
	 * application is fetched from the application's info.xml file. For more info,
	 * read the docs on {@link M_Application::$_xml}
	 *
	 * @static
	 * @access public
	 * @return string
	 */
	public static function getName() {
		return (string) self::_getInfoXmlFile()->name;
	}
	
	/**
	 * Get the version of the application
	 *
	 * @static
	 * @access public
	 * @return M_ApplicationVersion
	 */
	public static function getVersion() {
		return M_ApplicationVersion::constructWithString((string) self::_getInfoXmlFile()->version);
	}
	
	/**
	 * Get release notes
	 * 
	 * Will provide with a collection of release notes. Each of the release notes 
	 * is represented by an instance of {@link M_ApplicationReleaseNote}.
	 * 
	 * @static
	 * @access public
	 * @return ArrayIterator
	 */
	public static function getReleaseNotes() {
		// Return value
		$out = array();
		
		// If release notes are available on the module:
		$node = self::_getInfoXmlFile()->releases;
		if($node) {
			// For each of the release notes:
			$children = $node->children();
			if(count($children) > 0) {
				foreach($children as $child) {
					// Make sure a version number has been given:
					if(! isset($child['version'])) {
						throw new M_ApplicationException(sprintf(
							'Missing Version Number in Release Note "%s"',
							(string) $child
						));
					}
					
					// Construct a Release note:
					$note = new M_ApplicationReleaseNote;
					$note->setVersion(M_ApplicationVersion::constructWithString($child['version']));
					$note->setDescription((string) $child);
					
					// Add the release note to the output:
					$out[] = $note;
				}
			}
		}
		
		// Return the collection of release notes:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get the description of the module
	 *
	 * @static
	 * @access public
	 * @return string
	 */
	public static function getDescription() {
		return (string) self::_getInfoXmlFile()->description;
	}
	
	/**
	 * Get config
	 * 
	 * This method will provide with the configuration of the application.
	 * Note that the application's configuration is being represented by the 
	 * singleton instance of {@link M_ApplicationConfig} (with default namespace).
	 * 
	 * Using this method is the same as:
	 * 
	 * <code>
	 *   M_ApplicationConfig::getInstance()->getConfig();
	 * </code>
	 * 
	 * @access public
	 * @return M_Config
	 */
	public static function getConfig() {
		return M_ApplicationConfig::getInstance()->getConfig();
	}
	
	/**
	 * Get config of module
	 * 
	 * This method will provide with the configuration of an application module.
	 * This method is a short alias to:
	 * 
	 * <code>
	 *   $module = new M_ApplicationModule('my-module');
	 *   $module->getConfig()
	 * </code>
	 * 
	 * @access public
	 * @return M_Config
	 */
	public static function getConfigOfModule($moduleId) {
		$module = new M_ApplicationModule($moduleId);
		return $module->getConfig();
	}
	
	/**
	 * Get modules
	 * 
	 * This method will provide with the collection of modules that
	 * has been installed in the application. Note that this method
	 * will return a collection of {@link M_ApplicationModule} objects.
	 *
	 * @static
	 * @access public
	 * @return array
	 */
	public static function getModules() {
		// Return value (cached, for performance):
		static $out = NULL;
		
		// If not requested before:
		if(! $out) {
			// Prepare collection:
			$out = array();
			
			// Construct the directory where modules are located:
			$directory = new M_Directory(M_Loader::getAbsolute(M_Loader::getApplicationFolder() . '/' . M_Loader::getModulesFolder()));
			
			// For each of the modules in the directory:
			foreach($directory->getItems() as $item) {
				// Add the current item to the return value:
				if($item instanceof M_Directory) {
					if(strncmp($item->getName(), '.', 1)) {
						$out[] = new M_ApplicationModule($item->getName());
					}
				}
			}
		}
		
		// Return the iterator:
		return $out;
	}
	
	/**
	 * Is module installed?
	 * 
	 * This method will tell whether or not a given module has been 
	 * installed in the application. The return value is TRUE if the
	 * module has been installed, FALSE if not.
	 *
	 * @access public
	 * @param string $module
	 * 		The module to check installation of
	 * @return boolean
	 */
	public static function isModuleInstalled($module) {
		$module = new M_ApplicationModule((string) $module);
		return $module->isInstalled();
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get info xml
	 * 
	 * Will provide with a SimpleXMLElement, that contains the structure and
	 * data of the application's info.xml file. For more info, read the docs on
	 * {@link M_Application::$_xml}
	 * 
	 * @static
	 * @access private
	 * @return SimpleXMLElement
	 */
	private function _getInfoXmlFile() {
		// If the XML file has not been requested before:
		if(!isset(self::$_xml)) {
			// We compose the complete (absolute) path to the application's
			// info.xml file. With this path, we construct an M_File object.
			$file = new M_File(M_Loader::getAbsolute(M_Loader::getApplicationFolder() . '/info.xml'));
			
			// If the file cannot be found, we throw an exception to
			// inform about the failure to fetch module info.
			if(!$file->exists()) {
				throw new M_ApplicationException(sprintf(
					'%s: Cannot load %s',
					__CLASS__,
					$file->getPath()
				));
			}
			
			// If the file does exist, we try to parse the XML file into
			// an instance of SimpleXmlElement:
			try {
				// Load & Parse the file
				self::$_xml = @simplexml_load_file($file->getPath());
				
				// Validate required fields:
				$hasName        = isset(self::$_xml[$this->_id]->name);
				$hasVersion     = isset(self::$_xml[$this->_id]->version);
				$hasDescription = isset(self::$_xml[$this->_id]->description);
				if(! $hasName || ! $hasVersion || ! $hasDescription) {
					throw new M_ApplicationException(sprintf(
						'%s::%s() - Missing required "name", "version" or "description"',
						__CLASS__,
						__FUNCTION__
					));
				}
			}
			// If anything goes wrong, we throw an exception to inform
			// about the failure to fetch module info.
			catch(Exception $e) {
				throw new M_ApplicationException(sprintf(
					'%s: Could not parse info.xml of Application (%s)',
					__CLASS__,
					$e->getMessage()
				));
			}
		}
		
		// We return the XML object:
		return self::$_xml;
	}
}