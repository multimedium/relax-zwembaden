<?php
/**
 * M_ApplicationIdentity class
 * 
 * M_ApplicationIdentity can be used to get information about the identity of the
 * installed application. Examples of this class' usage:
 * 
 * - Get name + email of the contact person/entity that figures in outgoing emails.
 * - Get vCard of contact person/entity
 * - etc...
 * 
 * @package Core
 */
class M_ApplicationIdentity {
	/**
	 * Singleton instance
	 * 
	 * @static
	 * @access private
	 * @var M_ApplicationIdentity
	 */
	private static $_instance;
	
	/**
	 * Contact person/entity
	 * 
	 * @access private
	 * @var M_Contact
	 */
	private $_contact;
	
	/**
	 * Private constructor
	 * 
	 * {@link M_ApplicationIdentity} is a singleton object, and can therefore
	 * be constructed ONLY by the class itself. To obtain the singleton instance,
	 * you should use {@link M_ApplicationIdentity::getInstance()}
	 * 
	 * @access private
	 * @return M_ApplicationIdentity
	 */
	private function __construct() {
	}
	
	/**
	 * Singleton constructor
	 * 
	 * Will provide with the singleton instance of {@link M_ApplicationIdentity}
	 * 
	 * @access public
	 * @return M_ApplicationIdentity
	 */
	public static function getInstance() {
		if(! self::$_instance) {
			self::$_instance = new self;
		}
		return self::$_instance;
	}
	
	/**
	 * Set contact
	 * 
	 * This method can be used to set the contact person/entity that is represented
	 * by the application.
	 * 
	 * @see M_ApplicationIdentity::getContact()
	 * @access public
	 * @return M_Contact
	 */
	public function setContact(M_Contact $contact) {
		$this->_contact = $contact;
	}
	
	/**
	 * Get contact
	 * 
	 * Will provide with the contact person/entity that is being represented by
	 * the application.
	 * 
	 * Note that you can use {@link M_ApplicationIdentity::setContact()} to set
	 * the contact person/entity. However, if not provided a contact person/entity,
	 * {@link M_ApplicationIdentity} will load the contact data from the app's
	 * definition file:
	 * 
	 * <code>application/resources/definitions/identity.xml</code>
	 * 
	 * If that definition file is missing, or incorrectly formatted, an exception
	 * will be thrown to inform about the error! A correctly formatted identity
	 * file would like the example below:
	 * 
	 * <code>
	 *    <?xml version="1.0" encoding="UTF-8"?>
	 *    <identity>
	 *       <fn>Multimedium</fn>
	 *       <address>
	 *          <street>Buizelstraat 14 Bus 2</street>
	 *          <postalCode>2320</postalCode>
	 *          <city>Hoogstraten</city>
	 *          <country>Belgium</country>
	 *       </address>
	 *       <emails>
	 *          <email type="info"><![CDATA[Multimedium Info <info@multimedium.be>]]></email>
	 *          <email type="tom"><![CDATA[Tom Bauwens <tom@multimedium.be>]]></email>
	 *          <email type="ben"><![CDATA[Ben Brughmans <ben@multimedium.be>]]></email>
	 *          <email type="frank"><![CDATA[Frank Van den Bogerd <frank@multimedium.be>]]></email>
	 *       </emails>
	 *       <phoneNumbers>
	 *          <number type="T">+32 (0)3 297 71 54</number>
	 *          <number type="F">+32 (0)3 297 71 84</number>
	 *       </phoneNumbers>
	 *    </identity>
	 * </code>
	 * 
	 * @access public
	 * @return M_Contact
	 */
	public function getContact() {
		if(! $this->_contact) {
			$this->_contact = $this->_getContactFromXml();
		}
		return $this->_contact;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get contact, from XML
	 * 
	 * Is used internally, to load the contact data from the app's definition
	 * file. See {@link M_ApplicationIdentity::getContact()} for more info.
	 * 
	 * @access private
	 * @return M_Contact
	 */
	private function _getContactFromXml() {
		// Compose the absolute path to the XML file:
		$fn = new M_File(
			M_Loader::getAbsolute(
				M_Loader::getApplicationFolder() . '/' . 
				M_Loader::getResourcesFolder() . '/' . 
				M_Loader::getDefinitionsFolder() . '/' . 
				'identity.xml'
			)
		);
		
		// Make sure the file exists:
		if(! $fn->exists()) {
			throw new M_ApplicationException(sprintf(
				'Cannot parse Application Identity File (XML); Missing file %s',
				$fn->getPath()
			));
		}
		
		// (Try to) parse the identity file
		$xml = M_XmlElement::constructWithFile($fn);
		
		// We parse an M_Contact object out of the file:
		$contact = new M_Contact((string) $xml->fn);
		
		// If a logo was provided
		if(isset($xml->logo) && ! empty($xml->logo)) {
			$contact->setPicture(new M_Image(M_Loader::getAbsolute($xml->logo)));
		}
		
		// If an address has been provided
		if(isset($xml->address) && count($xml->address) > 0) {
			foreach($xml->xpath('/identity/address') AS $xmlAddress) {
				// Construct an address object:
				$address = new M_ContactAddress();

				// If a name for the address is available:
				if(isset($xmlAddress->name)) {
					$address->setName((string) $xmlAddress->name);
				}
				
				// If street name is available:
				if(isset($xmlAddress->street)) {
					$address->setStreetAddress((string) $xmlAddress->street);
				}
				
				// If postal code is available:
				if(isset($xmlAddress->postalCode)) {
					$address->setPostalCode((string) $xmlAddress->postalCode);
				}
				
				// If city has been made available
				if(isset($xmlAddress->city)) {
					$address->setCity((string) $xmlAddress->city);
				}
				
				// If region has been made available
				if(isset($xmlAddress->region)) {
					$address->setRegion((string) $xmlAddress->region);
				}
				
				// If country is available
				if(isset($xmlAddress->country)) {
					$address->setCountry((string) $xmlAddress->country);
				}
				
				// If longitude is available
				if(isset($xmlAddress->longitude)) {
					$address->setLongitude((string) $xmlAddress->longitude);
				}
				
				// If latitude is available
				if(isset($xmlAddress->latitude)) {
					$address->setLatitude((string) $xmlAddress->latitude);
				}
				
				// Set the type of address
				if (isset($xmlAddress['type']))	{
					$address->setType((string) $xmlAddress['type']);
				}
				
				// Attach the address to the contact:
				$contact->addAddress($address);
			}
		}

		// If emails are available:
		if(isset($xml->emails) && count($xml->emails) > 0) {
			// For each of the email addresses:
			foreach($xml->emails->children() as $email) {
				// Type given?
				if(! isset($email['type'])) {
					throw new M_ApplicationException('Cannot parse Application Identity File (XML); Missing email type');
				}
				
				// Try to:
				try {
					// Parse the name and email address:
					$recipient = M_MailHelper::getRecipientsFromString((string) $email);
					
					// Make sure only one has been provided
					if($recipient->count() != 1) {
						throw new M_ApplicationException(sprintf(
							'Multiple email recipients, or none at all: "%s"',
							(string) $email
						));
					}
				}
				// If failed:
				catch(Exception $e) {
					throw new M_ApplicationException('Cannot parse Application Identity File (XML); ' . $e->getMessage());
				}
				
				// Construct an email object:
				$email = new M_ContactEmail($recipient->key(), $email['type']);
				
				// Set the name of the email:
				$email->setName($recipient->current());
				
				// Attach the email to the contact:
				$contact->addEmail($email);
			}
		}
		
		// If phone numbers are available:
		if(isset($xml->phoneNumbers) && count($xml->phoneNumbers) > 0) {
			// For each of the phone numbers:
			foreach($xml->phoneNumbers->children() as $number) {
				// Type given?
				if(! isset($number['type'])) {
					throw new M_ApplicationException('Cannot parse Application Identity File (XML); Missing phone number type');
				}
				
				// Construct a new phone number, and attach to the contact:
				$contact->addPhoneNumber(new M_ContactPhoneNumber((string) $number, $number['type']));
			}
		}

		// If VAT number is available:
		if(isset($xml->vat) && ! empty($xml->vat)) {
			// Set the VAT number in the contact object:
			$contact->setVatNumber((string) $xml->vat);
		}

		// If nickname is available:
		if(isset($xml->nickname) && ! empty($xml->nickname)) {
			// Set the VAT number in the contact object:
			$contact->setNickname((string) $xml->nickname);
		}
		
		// return the contact person/entity:
		return $contact;
	}
}