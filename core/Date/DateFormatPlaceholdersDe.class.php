<?php
/**
 * M_DateFormatPlaceholdersDe
 * 
 * @package Core
 */
class M_DateFormatPlaceholdersDe implements MI_DateFormatPlaceholders {
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get placeholders
	 * 
	 * @access public
	 * @return array $placeholders
	 *		Returns an associative array, of which the keys are the date part
	 *		symbols used in date formats, and the values are the corresponding
	 *		placeholders
	 */
	public static function getPlaceholders() {
		return array(
			'yyyyy' => 'JJJJJ',
			'yyyy'  => 'JJJJ',
			'yyy'   => 'JJJJ',
			'yy'    => 'JJ',
			'y'     => 'JJJJ',
			'YYYY'  => 'JJJJ',
			'YYY'   => 'JJJ',
			'YY'    => 'JJ',
			'MMMMM' => 'M',
			'MMMM'  => 'Monat',
			'MMM'   => 'Monat',
			'MM'    => 'MM',
			'M'     => 'M',
			'dd'    => 'Tag',
			'd'     => 'Tag',
			'EEEEE' => 'Tag (ZB D)',
			'EEEE'  => 'Tag (ZB Dienstag)',
			'EEE'   => 'Tag (ZB Di.)',
			'EE'    => 'Tag (ZB Di.)',
			'E'     => 'Tag (ZB Di.)',
			'ee'    => 'Tag der Woche (ex. 01)',
			'e'     => 'Tag der Woche (ex. 1)',
			'a'     => 'AM',
			'hh'    => 'hh',
			'h'     => 'h',
			'HH'    => 'hh',
			'H'     => 'h',
			'KK'    => 'hh',
			'K'     => 'h',
			'kk'    => 'hh',
			'k'     => 'h',
			'mm'    => 'mm',
			'm'     => 'm',
			'ss'    => 'ss',
			's'     => 's'
		);
	}
}