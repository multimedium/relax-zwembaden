<?php
/**
 * MI_DateFormatPlaceholders
 * 
 * This interface describes the methods that are available in the definition 
 * classes for working with placeholders in date formats.
 * 
 * @package Core
 */
interface MI_DateFormatPlaceholders {
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get placeholders
	 * 
	 * @access public
	 * @return array $placeholders
	 *		Returns an associative array, of which the keys are the date part
	 *		symbols used in date formats, and the values are the corresponding
	 *		placeholders
	 */
	public static function getPlaceholders();
}