<?php
/**
 * M_DateFormatPlaceholdersEs
 * 
 * @package Core
 */
class M_DateFormatPlaceholdersEs implements MI_DateFormatPlaceholders {
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get placeholders
	 * 
	 * @access public
	 * @return array $placeholders
	 *		Returns an associative array, of which the keys are the date part
	 *		symbols used in date formats, and the values are the corresponding
	 *		placeholders
	 */
	public static function getPlaceholders() {
		return array(
			'yyyyy' => 'aaaaa',
			'yyyy'  => 'aaaa',
			'yyy'   => 'aaaa',
			'yy'    => 'aa',
			'y'     => 'aaaa',
			'YYYY'  => 'aaaa',
			'YYY'   => 'aaa',
			'YY'    => 'aa',
			'MMMMM' => 'm',
			'MMMM'  => 'mes',
			'MMM'   => 'mes',
			'MM'    => 'mm',
			'M'     => 'm',
			'dd'    => 'día',
			'd'     => 'día',
			'EEEEE' => 'día (p. ej. M)',
			'EEEE'  => 'día (p. ej. martes)',
			'EEE'   => 'día (p. ej. mar)',
			'EE'    => 'día (p. ej. mar)',
			'E'     => 'día (p. ej. mar)',
			'ee'    => 'día de la semana (p. ej. 01)',
			'e'     => 'día de la semana (p. ej. 1)',
			'a'     => 'AM',
			'hh'    => 'hh',
			'h'     => 'h',
			'HH'    => 'hh',
			'H'     => 'h',
			'KK'    => 'hh',
			'K'     => 'h',
			'kk'    => 'hh',
			'k'     => 'h',
			'mm'    => 'mm',
			'm'     => 'm',
			'ss'    => 'ss',
			's'     => 's'
		);
	}
}