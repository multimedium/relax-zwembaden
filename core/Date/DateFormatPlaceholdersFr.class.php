<?php
/**
 * M_DateFormatPlaceholdersFr
 * 
 * @package Core
 */
class M_DateFormatPlaceholdersFr implements MI_DateFormatPlaceholders {
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get placeholders
	 * 
	 * @access public
	 * @return array $placeholders
	 *		Returns an associative array, of which the keys are the date part
	 *		symbols used in date formats, and the values are the corresponding
	 *		placeholders
	 */
	public static function getPlaceholders() {
		return array(
			'yyyyy' => 'aaaaa',
			'yyyy'  => 'aaaa',
			'yyy'   => 'aaaa',
			'yy'    => 'aa',
			'y'     => 'aaaa',
			'YYYY'  => 'aaaa',
			'YYY'   => 'aaa',
			'YY'    => 'aa',
			'MMMMM' => 'm',
			'MMMM'  => 'mois',
			'MMM'   => 'mois',
			'MM'    => 'mm',
			'M'     => 'm',
			'dd'    => 'jour',
			'd'     => 'jour',
			'EEEEE' => 'jour (ex. M)',
			'EEEE'  => 'jour (ex. mardi)',
			'EEE'   => 'jour (ex. mar.)',
			'EE'    => 'jour (ex. mar.)',
			'E'     => 'jour (ex. mar.)',
			'ee'    => 'jour de la semaine (ex. 01)',
			'e'     => 'jour de la semaine (ex. 1)',
			'a'     => 'AM',
			'hh'    => 'hh',
			'h'     => 'h',
			'HH'    => 'hh',
			'H'     => 'h',
			'KK'    => 'hh',
			'K'     => 'h',
			'kk'    => 'hh',
			'k'     => 'h',
			'mm'    => 'mm',
			'm'     => 'm',
			'ss'    => 'ss',
			's'     => 's'
		);
	}
}