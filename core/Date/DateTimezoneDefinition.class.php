<?php
/**
 * M_DateTimezoneDefinition class
 * 
 * Allows for creating a date timezone, which is typically added to an
 * {@link M_CalendarIcs} feed.
 * 
 * For information on how the timezones - defined by a VTIMEZONE iCalendar
 * component - are constructed, read the docs on the following links:
 * - http://www.ietf.org/rfc/rfc2445.txt
 * - http://wiki.zimbra.com/wiki/Changing_ZCS_Time_Zones
 * - http://www.kanzaki.com/docs/ical/vtimezone.html
 * - http://blog.jonudell.net/2011/10/17/x-wr-timezone-considered-harmful/
 * 
 * @author Tim Segers
 * @package Core
 */
class M_DateTimezoneDefinition {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Timezone ID
	 * 
	 * Holds the unique timezone id for this {@link M_DateTimezoneDefinition()}
	 * 
	 * @see M_DateTimezoneDefinition::setTimezoneId()
	 * @see M_DateTimezoneDefinition::getTimezoneId()
	 * @access private
	 * @var string
	 */
	private $_timezoneId;
	
	/**
	 * Last Modified Date
	 * 
	 * Holds the date the timezone definition was last modified 
	 * 
	 * @see M_DateTimezoneDefinition::setLastModifiedDate()
	 * @see M_DateTimezoneDefinition::getLastModifiedDate()
	 * @access private
	 * @var M_Date
	 */
	private $_lastModifiedDate;
	
	/**
	 * Components
	 * 
	 * Holds the collection of {@link M_DateTimezoneComponent} objects
	 * for this timezone, stored by their respective type
	 * 
	 * @see M_DateTimezoneDefinition::addComponent()
	 * @see M_DateTimezoneDefinition::getComponents()
	 * @access private
	 * @var array
	 */
	private $_components;
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Set Timezone Id
	 * 
	 * Allows for setting the timezone id. This timezone id is a string
	 * that uniquely identifies the timezone definition within the scope
	 * of an iCalendar.
	 * 
	 * NOTE: this property is mandatory for the correct functioning of the
	 * timezone definition
	 * 
	 * @access public
	 * @param string $timezoneId
	 * @return M_DateTimezoneDefinition
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTimezoneId($timezoneId) {
		$this->_timezoneId = (string) $timezoneId;
		return $this;
	}
	
	/**
	 * Get Timezone Id
	 * 
	 * @see M_DateTimezoneDefinition::setTimezoneId() 
	 * @access public
	 * @return string
	 */
	public function getTimezoneId() {
		return $this->_timezoneId;
	}
	
	/**
	 * Set Last Modified Date
	 * 
	 * Allows for setting the last date and time this timezone definition
	 * was updated.
	 * 
	 * NOTE: setting this property is optional for the correct fuctioning of
	 * the timezone definition
	 * 
	 * @access public
	 * @param M_Date $lastModifiedDate
	 * @return M_DateTimezoneDefinition
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLastModifiedDate(M_Date $lastModifiedDate) {
		$this->_lastModifiedDate = $lastModifiedDate;
		return $this;
	}
	
	/**
	 * Get Last Modified Date
	 * 
	 * @see M_DateTimezoneDefinition::setLastModifiedDate()
	 * @access public
	 * @return M_Date
	 */
	public function getLastModifiedDate() {
		return $this->_lastModifiedDate;
	}
	
	/**
	 * Add Component
	 * 
	 * Allows for adding an {@link M_DateTimezoneComponent} to the timezone,
	 * which typically describes either the Standard Time or Daylight Saving
	 * Time observance.
	 * 
	 * Note that at least one component is required for proper timezone
	 * definition functionality. Additionally, if only one component is
	 * specified, it should be for Standard Time, as Daylight Saving Time
	 * does not apply in that case. For more information about this, read
	 * the docs on {@link M_DateTimezoneComponent::setType()}.
	 * 
	 * You may specify multiple components of the same type. If this is the
	 * case, we assume that components of the same type complement eachother.
	 * For example, from {@link http://www.ietf.org/rfc/rfc2445.txt}:
	 * 
	 * "This is an example showing a fictitious set of rules for the Eastern
	 * United States, where the first Daylight Time rule has an effective
	 * end date. There is a second Daylight Time rule that picks up where
	 * the other left off.
	 *
     * BEGIN:VTIMEZONE
     * TZID:US--Fictitious-Eastern
     * LAST-MODIFIED:19870101T000000Z
	 * 
     * BEGIN:STANDARD
     * DTSTART:19671029T020000
     * RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10
     * TZOFFSETFROM:-0400
     * TZOFFSETTO:-0500
     * TZNAME:EST
     * END:STANDARD
	 * 
     * BEGIN:DAYLIGHT
     * DTSTART:19870405T020000
     * RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=4;UNTIL=19980404T070000Z
     * TZOFFSETFROM:-0500
     * TZOFFSETTO:-0400
     * TZNAME:EDT
     * END:DAYLIGHT
	 * 
     * BEGIN:DAYLIGHT
     * DTSTART:19990424T020000
     * RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=4
     * TZOFFSETFROM:-0500
     * TZOFFSETTO:-0400
     * TZNAME:EDT
     * END:DAYLIGHT
	 * 
     * END:VTIMEZONE"
	 * 
	 * @access public
	 * @param M_DateTimezoneDefinition $component
	 * @return M_DateTimezoneDefinition
	 *		Returns itself, for a fluent programming interface
	 */
	public function addComponent(M_DateTimezoneComponent $component) {
		// Initialize the property, if not done before
		if(is_null($this->_components)) {
			$this->_components = array(
				M_DateTimezoneComponent::TYPE_STANDARD => array(),
				M_DateTimezoneComponent::TYPE_DAYLIGHT => array()
			);
		}
		
		// Make sure the component's type is valid
		if(! array_key_exists($component->getType(), $this->_components)) {
			throw new M_Exception(sprintf(
				'Could not add M_DateTimezoneComponent in %s::%s(); invalid type %s',
				__CLASS__,
				__FUNCTION__,
				$component->getType()
			));
		}
		
		// Add the component to the property
		$this->_components[$component->getType()][] = $component;
		
		// Return self
		return $this;
	}
	
	/**
	 * Get Components
	 * 
	 * @see M_DateTimezoneDefinition::addComponent()
	 * @access public
	 * @return array
	 */
	public function getComponents() {
		// Output array
		$out = array();
		
		// If components were provided
		if(! is_null($this->_components)) {
			// First add the standard ones
			foreach($this->_components[M_DateTimezoneComponent::TYPE_STANDARD] as $component) {
				$out[] = $component;
			}
			
			// Then add the DST ones
			foreach($this->_components[M_DateTimezoneComponent::TYPE_DAYLIGHT] as $component) {
				$out[] = $component;
			}
		}

		// Return the output array
		return $out;
	}
	
	/**
	 * Export to string
	 * 
	 * This method will export the source code of the timezone defintion, which
	 * is typically used in the .ICS file.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// Before proceeding, we must first check wheter all required properties
		// are given:
		if(! $this->getTimezoneId()) $this->_errorMissingProperty('timezone id');
		
		// Custom check: make sure that at least one calendar timezone component
		// has been provided
		if(count($this->getComponents()) == 0) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(); at least one timezone ' .
				'component should be provided for proper timezone functionality. ' .
				'Please provide one or more M_DateTimezoneComponent objects to ' . 
				'%s::%s()',
				__CLASS__,
				__FUNCTION__,
				__CLASS__,
				'addComponent'
			));
		}
		
		// Next, make sure that at least one Standard Time Definition
		// component is given
		$standardFound = false;
		foreach($this->getComponents() as $component) {
			/* @var $component M_DateTimezoneComponent */
			if($component->getType() == M_DateTimezoneComponent::TYPE_STANDARD) {
				$standardFound = true;
				break;
			}
		}
		
		// If no standard component was found
		if(! $standardFound) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(); at least one Standard Time ' .
				'timezone component should be provided for proper timezone ' .
				'functionality. Please provide this timezone component to %s::%s()',
				__CLASS__,
				__FUNCTION__,
				__CLASS__,
				'addComponent'
			));
		}
		
		// Define a new line
		$nl = M_CalendarIcsSourceHelper::getNewLine();
		
		// Open the output with a begin tag
		$out = M_CalendarIcsSourceHelper::getBeginTag('VTIMEZONE');
		
		// Add a new line
		$out .= $nl;
		
		// Add the timezone id
		$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
			'TZID', $this->getTimezoneId()
		);
		
		// Add a new line
		$out .= $nl;
		
		// If a recurrence rule was set
		if($this->getLastModifiedDate()) {
			// Add it to the output
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'LAST-MODIFIED', 
				M_CalendarIcsSourceHelper::getDateString($this->getLastModifiedDate())
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// For each component
		foreach($this->getComponents() as $component) {
			/* @var $component M_DateTimezoneComponent */
			
			// Print it
			$out .= $component->toString();
			
			// And add a new line
			$out .= $nl;
		}
		
		// End the output with an end tag
		$out .= M_CalendarIcsSourceHelper::getEndTag('VTIMEZONE');
		
		// Finally, return the output
		return $out;
	}
	
	/* -- PRIVATE / PROTECTED -- */
	
	/**
	 * Error: Missing Property
	 * 
	 * Whenever a mandatory property for the correct functioning of the
	 * timezone definition is missing in
	 * {@link M_DateTimezone::toString()}, this function will
	 * be used to inform the user about the error.
	 * 
	 * @access protected
	 * @param string $property
	 * @throws M_Exception
	 * @return void
	 */
	protected function _errorMissingProperty($property) {
		throw new M_Exception(sprintf(
			'Cannot proceed in %s::%s(); missing mandatory property %s',
			__CLASS__,
			'toString',
			$property
		));
	}
}