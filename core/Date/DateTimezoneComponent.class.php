<?php
/**
 * M_DateTimezoneComponent
 * 
 * This class defines a timezone component, which is typically used in 
 * an {@link M_DateTimezoneDefinition} to describe the recurrence of either
 * the Standard Time or Daylight Saving Time timezone change. Read the docs on
 * {@see M_DateTimezoneDefinition} for more information about this.
 * 
 * From the official RFC2445 standard (see link below):
 * 
 * "Each "VTIMEZONE" calendar component consists of a collection of one
 * or more sub-components that describe the rule for a particular observance 
 * (either a Standard Time or a Daylight Saving Time observance).
 * 
 * The "STANDARD" sub-component consists of a collection of properties 
 * that describe Standard Time. The "DAYLIGHT" sub-component consists 
 * of a collection of properties that describe Daylight Saving Time. 
 * In general this collection of properties consists of:
 * 
 * - the first onset date-time for the observance
 * - the last onset date-time for the observance, if a last onset is known
 * - the offset to be applied for the observance
 * - a rule that describes the day and time when the observance takes effect
 * - an optional name for the observance"
 * 
 * @link http://www.ietf.org/rfc/rfc2445.txt
 * @link http://en.wikipedia.org/wiki/Time_zone
 * @author Tim Segers
 * @package Core 
 */
class M_DateTimezoneComponent {
	
	/* -- CONSTANTS -- */
	
	/**
	 * Type: Standard
	 * 
	 * This constant defines the component type used for the standard
	 * timezone definition
	 * 
	 * @see M_DateTimezoneComponent::setType()
	 * @see M_DateTimezoneComponent::getType()
	 * @var string 
	 */
	const TYPE_STANDARD = 'STANDARD';
	
	/**
	 * Type: Daylight
	 * 
	 * This constant defines the component type used for the daylight
	 * timezone definition
	 * 
	 * @see M_DateTimezoneComponent::setType()
	 * @see M_DateTimezoneComponent::getType()
	 * @var string 
	 */
	const TYPE_DAYLIGHT = 'DAYLIGHT';
	
	/* -- PROPERTIES -- */
	
	/**
	 * Type
	 * 
	 * Holds the type for this component, which is typically either standard
	 * or daylight
	 * 
	 * @see M_DateTimezoneComponent::setType()
	 * @see M_DateTimezoneComponent::getType()
	 * @access private
	 * @var string
	 */
	private $_type;
	
	/**
	 * Recurrence Rule
	 * 
	 * Holds the recurrence rule for this component
	 * 
	 * @see M_DateTimezoneComponent::setRecurrenceRule() 
	 * @see M_DateTimezoneComponent::getRecurrenceRule() 
	 * @access private
	 * @var M_CalendarRecurrenceRule
	 */
	private $_recurrenceRule;
	
	/**
	 * Start Date
	 * 
	 * Holds the start date in history from which point on this component
	 * is to be used
	 * 
	 * @see M_DateTimezoneComponent::setStartDate() 
	 * @see M_DateTimezoneComponent::getStartDate() 
	 * @access private
	 * @var M_Date
	 */
	private $_startDate;
	
	/**
	 * End Date
	 * 
	 * Holds the end date in history until which point this component is to
	 * be used
	 * 
	 * @see M_DateTimezoneComponent::setEndDate() 
	 * @see M_DateTimezoneComponent::getEndDate() 
	 * @access private
	 * @var M_Date
	 */
	private $_endDate;
	
	/**
	 * Offset From
	 * 
	 * Holds the GMT offset before the transition
	 * 
	 * @see M_DateTimezoneComponent::setOffsetFrom()
	 * @see M_DateTimezoneComponent::getOffsetFrom()
	 * @access private
	 * @var string
	 */
	private $_offsetFrom;
	
	/**
	 * Offset To
	 * 
	 * Holds the GMT offset after the transition
	 * 
	 * @see M_DateTimezoneComponent::setOffsetTo() 
	 * @see M_DateTimezoneComponent::getOffsetTo() 
	 * @access private
	 * @var string
	 */
	private $_offsetTo;
	
	/**
	 * Name
	 * 
	 * Holds the timezone component name
	 * 
	 * @see M_DateTimezoneComponent::setName()
	 * @see M_DateTimezoneComponent::getName()
	 * @access private
	 * @var string
	 */
	private $_name;
	
	/* -- PUBLIC FUNCTIONS -- */
	
	/**
	 * Set Type
	 * 
	 * Allows for setting the component type, which is typically either
	 * standard or daylight. This type decided wheter this component
	 * describes Standard Time or Daylight Saving Time.
	 * 
	 * NOTE: this property is mandatory for the correct functioning of
	 * the timezone component
	 * 
	 * @access public
	 * @param string $type
	 * @return M_DateTimezoneComponent
	 *		Returns itself, for a fluent programming interface
	 */
	public function setType($type = self::TYPE_STANDARD) {
		// Cast to string
		$type = (string) $type;
	
		// Before setting the type, make sure the received type is valid:
		if(! in_array($type, array(self::TYPE_STANDARD, self::TYPE_DAYLIGHT))) {
			throw new M_Exception(sprintf(
				'Cannot set %s type: Unknown type %s specified in %s::%s',
				__CLASS__,
				$type,
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Set the property
		$this->_type = $type;
		
		// And return self
		return $this;
	}

	/**
	 * Get Type
	 * 
	 * @see M_DateTimezoneComponent::setType()
	 * @access public
	 * @return string
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Set Recurrence Rule
	 * 
	 * Allows for setting the {@link M_CalendarRecurrenceRule}, which
	 * specifies the rule for the transition date, e.g. when the date
	 * changes from standard to daylight or the other way around. For example,
	 * if we have the timezone "Europe/Brussels", and we create a component
	 * for the daylight transition, we can specify its recurrence rule here.
	 * 
	 * A recurrence rule like this, which does not directly specify an end date
	 * for the timezone transition, is typically used whenever the end date
	 * of the timezone rule is unknown. Contrary to setting a real end date
	 * through {@link M_DateTimezoneComponent::setEndDate()}, a timezone with
	 * a recurrence rule specified will run indefinitely, whereas a timezone
	 * with an end date will effectively end at the given date. As a result,
	 * it is best to use a recurrence rule for maximum flexibility. For more
	 * info, read the docs on {@link M_DateTimezonecomponent::setEndDate()}.
	 * 
	 * NOTE: you can only define either a recurrence rule, or an end date.
	 * Specifying both at the same time is impossible.
	 * 
	 * NOTE: if the time zone you want to declare does not use daylight
	 * saving time (see {@link http://www.worldtimezone.com/daylight.html} for
	 * more information), the recurrence rule, nor the end date,
	 * should not be specified.
	 * 
	 * @access public
	 * @param M_CalendarRecurrenceRule $rrule
	 * @return M_DateTimezoneComponent
	 *		Returns itself, for a fluent programming interface
	 */
	public function setRecurrenceRule(M_CalendarRecurrenceRule $rrule) {
		$this->_recurrenceRule = $rrule;
		return $this;
	}
	
	/**
	 * Get Recurrence Rule
	 * 
	 * @see M_DateTimezoneComponent::setRecurrenceRule()
	 * @return M_CalendarRecurrenceRule 
	 */
	public function getRecurrenceRule() {
		return $this->_recurrenceRule;
	}
	
	/**
	 * Set Start Date
	 * 
	 * Allows for setting the date and time in history from which point on
	 * this timezone component is in use. For example, for the timezone
	 * "Europe/Brussels", the current start date for daylight saving time
	 * is 29/03/1981 at 2AM in the morning.
	 * 
	 * NOTE: this property is mandatory for the correct functioning of
	 * the timezone component
	 * 
	 * @access public
	 * @param M_Date $startDate
	 * @return M_DateTimezoneComponent
	 *		Returns itself, for a fluent programming interface
	 */
	public function setStartDate(M_Date $startDate) {
		$this->_startDate = $startDate;
		return $this;
	}
	
	/**
	 * Get Start Date
	 * 
	 * @see M_DateTimezoneComponent::setStartDate()
	 * @access public
	 * @return M_Date
	 */
	public function getStartDate() {
		return $this->_startDate;
	}
	
	/**
	 * Set End Date
	 * 
	 * Allows for setting the date and time in history until which point this
	 * timezone component should be used, e.g. the last date on which the
	 * standard to daylight transition (or the other way around) takes place.
	 * 
	 * An end date like this is typically used to describe a timezone component
	 * that has a fixed end date. This is the alternative to specifying
	 * a recurrence rule through the use of 
	 * {@link M_DateTimezoneComponent::setRecurrenceRule()}, which defines
	 * a timezone component that can be used indefinitely.
	 * 
	 * To support maximum flexibility, you should consider the use of the
	 * application you are writing currently, and judge if the currently
	 * composed timezone component has an end date far enough in the future
	 * to support it. If this is not the case, you should specify more than
	 * one timezone component of this type (standard or daylight saving time),
	 * to cover enough ground in the future.
	 * 
	 * For more info about this, check the examples about VTIMEZONE at the 
	 * official RFC2445 definition standard, which can be found at 
	 * {@link http://www.ietf.org/rfc/rfc2445.txt}.
	 * 
	 * NOTE: you can only define either a recurrence rule, or an end date.
	 * Specifying both at the same time is impossible.
	 * 
	 * NOTE: if the time zone you want to declare does not use daylight
	 * saving time (see {@link http://www.worldtimezone.com/daylight.html} for
	 * more information), the recurrence rule, nor the end date,
	 * should not be specified.
	 * 
	 * @access public
	 * @param M_Date $endDate
	 * @return M_DateTimezoneComponent
	 *		Returns itself, for a fluent programming interface
	 */
	public function setEndDate(M_Date $endDate) {
		$this->_endDate = $endDate;
		return $this;
	}
	
	/**
	 * Get End Date
	 * 
	 * @see M_DateTimezoneComponent::setEndDate()
	 * @access public
	 * @return M_Date 
	 */
	public function getEndDate() {
		return $this->_endDate;
	}
	
	/**
	 * Set Offset From
	 * 
	 * Allows for setting the GMT offset before the daylight saving time
	 * transition. Typically, this value is a string formatted as +/- followed
	 * by hhmm. For example:
	 * 
	 * <code>
	 *	$component = new M_DateTimezoneComponent();
	 *	$component->setOffsetFrom('-0800');
	 * </code>
	 * 
	 * From the official RFC2445 definition:
	 * 
	 * "The mandatory "TZOFFSETFROM" property gives the UTC offset which 
	 * is in use when the onset of this time zone observance begins. 
	 * "TZOFFSETFROM" is combined with "DTSTART" (the start date)
	 * to define the effective onset for the time zone sub-component 
	 * definition. For example, the following represents the time at 
	 * which the observance of Standard Time took effect in Fall 1967 
	 * for New York City:
	 * 
	 * DTSTART:19671029T020000
	 * TZOFFSETFROM:-0400"
	 * 
	 * NOTE: if the timezone does not apply daylight saving time, both the
	 * offset to and offset from values will be the allround year GMT offset,
	 * e.g. +0500.
	 * 
	 * NOTE: this property is mandatory for the correct functioning of
	 * the timezone component
	 * 
	 * @access public
	 * @param string $offsetFrom
	 * @return M_DateTimezoneComponent
	 *		Returns itself, for a fluent programming interface
	 */
	public function setOffsetFrom($offsetFrom) {
		// Before setting the value, we have to check if it is correctly
		// formatted. If this is not the case:
		if(! $this->_checkOffsetValue($offsetFrom)) {
			throw new M_Exception(sprintf(
				'Invalid offset value %s provided in %s::%s()', 
				$offsetFrom, 
				__CLASS__, 
				__FUNCTION__
			));
		}
		
		// Set the property, and return self
		$this->_offsetFrom = (string) $offsetFrom;
		return $this;
	}
	
	/**
	 * Get Offset From
	 * 
	 * @see M_DateTimezoneComponent::setOffsetFrom()
	 * @access public
	 * @return string
	 */
	public function getOffsetFrom() {
		return $this->_offsetFrom;
	}
	
	/**
	 * Set Offset To
	 * 
	 * Allows for setting the GMT offset after the daylight saving time
	 * transition. Typically, this value is a string formatted as +/- followed
	 * by hhmm. For example:
	 * 
	 * <code>
	 *	$component = new M_DateTimezoneComponent();
	 *	$component->setOffsetTo('-0800');
	 * </code>
	 * 
	 * NOTE: if the timezone does not apply daylight saving time, both the
	 * offset to and offset from values will be the allround year GMT offset,
	 * e.g. +0500.
	 * 
	 * NOTE: this property is mandatory for the correct functioning of
	 * the timezone component
	 * 
	 * @access public
	 * @param string $offsetTo
	 * @return M_DateTimezoneComponent
	 *		Returns itself, for a fluent programming interface
	 */
	public function setOffsetTo($offsetTo) {
		// Before setting the value, we have to check if it is correctly
		// formatted. If this is not the case:
		if(! $this->_checkOffsetValue($offsetTo)) {
			throw new M_Exception(sprintf(
				'Invalid offset value %s provided in %s::%s()', 
				$offsetTo, 
				__CLASS__, 
				__FUNCTION__
			));
		}
		
		// Set the property, and return self
		$this->_offsetTo = (string) $offsetTo;
		return $this;
	}
	
	/**
	 * Get Offset To
	 * 
	 * @see M_DateTimezoneComponent::setOffsetTo()
	 * @access public
	 * @return string
	 */
	public function getOffsetTo() {
		return $this->_offsetTo;
	}
	
	/**
	 * Set Name
	 * 
	 * Allows for setting the customary name for the time zone, which may for
	 * example be used for display. For example, it could be useful for
	 * display purposes to name this timezone "Brussels Daylight Saving Time",
	 * rather than the official "(GMT+01.00) Brussels/Copenhagen/Madrid/Paris".
	 * 
	 * NOTE: setting this property is optional for the correct fuctioning of
	 * the timezone component
	 * 
	 * @access public
	 * @param string $name
	 * @return M_DateTimezoneComponent
	 *		Returns itself, for a fluent programming interface
	 */
	public function setName($name) {
		$this->_name = (string) $name;
		return $this;
	}
	
	/**
	 * Get Name
	 * 
	 * @see M_DateTimezoneComponent::setName()
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Export to string
	 * 
	 * This method will export the source code of the component in the .ICS file.
	 * Typically, this is used by {@link M_DateTimezoneDefinition::toString()}, 
	 * in order to generate the .ICS file VTIMEZONE entry.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// Before proceeding, we must first check wheter all required properties
		// are given:
		if(! $this->getType()) $this->_errorMissingProperty('type');
		if(! $this->getStartDate()) $this->_errorMissingProperty('start date');
		if(! $this->getOffsetFrom()) $this->_errorMissingProperty('offset from');
		if(! $this->getOffsetTo()) $this->_errorMissingProperty('offset to');
		
		// As a secondary check, make sure that not both the recurrence rule
		// and end date are given, as both cannot be set at the same time
		if($this->getRecurrenceRule() && $this->getEndDate()) {
			throw new M_Exception(sprintf(
				'Cannot proceed in %s::%s(); the recurrence rule and end time cannot be set at the same time',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Define a new line
		$nl = M_CalendarIcsSourceHelper::getNewLine();
		
		// Open the output with a begin tag
		$out = M_CalendarIcsSourceHelper::getBeginTag($this->getType());
		
		// Add a new line
		$out .= $nl;
		
		// If a timezone name was set
		if($this->getName()) {
			// Add it to the output
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'TZNAME', 
				$this->getName()
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// Add the start time
		$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
			'DTSTART', 
			M_CalendarIcsSourceHelper::getDateString($this->getStartDate())
		);
		
		// Add a new line
		$out .= $nl;
		
		// Add the offset to
		$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
			'TZOFFSETTO', $this->getOffsetTo()
		);
		
		// Add a new line
		$out .= $nl;
		
		// Add the offset from
		$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
			'TZOFFSETFROM', $this->getOffsetFrom()
		);
		
		// Add a new line
		$out .= $nl;
		
		// If a recurrence rule was set
		if($this->getRecurrenceRule()) {
			// Add it to the output
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'RRULE', 
				$this->getRecurrenceRule()->toStringCalendarIcs()
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// If an end date was set
		if($this->getEndDate()) {
			// Add it to the output
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'RDATE', 
				M_CalendarIcsSourceHelper::getDateString($this->getEndDate())
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// End the output with an end tag
		$out .= M_CalendarIcsSourceHelper::getEndTag($this->getType());
		
		// Finally, return the output
		return $out;
	}
	
	/* -- PRIVATE / PROTECTED -- */
	
	/**
	 * Check Offset Value
	 * 
	 * Will check a given offset value (a string) to see if it is correctly
	 * formatted.
	 * 
	 * @access protected
	 * @param string $offset
	 * @return bool
	 *		Returns TRUE if the offset value is valid, FALSE if not
	 */
	protected function _checkOffsetValue($offset) {
		// Cast to string first
		$offset = (string) $offset;

		// First of all, check wheter the given offset is 5 characters long:
		if(strlen($offset) != 5) return false;
		
		// Next, split the string into 3 parts: the offset factor, hours
		// and minutes
		$factor = substr($offset, 0, 1);
		$hours = substr($offset, 1, 2);
		$minutes = substr($offset, 3, 2);
		
		// First check the factor, making sure it is either a plus or minus
		if($factor != '+' && $factor != '-') return false;
		
		// Next check the hour, if it is not 00. We make this check first,
		// as checking wheter the hours are an integer by casting them to an
		// integer won't do any good in this case
		if($hours != '00') {
			// First cast to an integer:
			$hours = (int) $hours;
			
			// Make sure the hour value is between 1 and 12
			if($hours < 1 || $hours > 12) return false;
		}
		
		// Next make the same check for the minutes. If the provided minutes
		// aren't 00
		if($minutes != '00') {
			// First cast to an integer
			$minutes = (int) $minutes;
			
			// Then check wheter the value is between 1 and 59
			if($minutes < 1 || $minutes > 59) return false;
		}
		
		// If we are still here it means that the check succeeded, so
		// return TRUE
		return true;
	}
	
	/**
	 * Error: Missing Property
	 * 
	 * Whenever a mandatory property for the correct functioning of the
	 * timezone component is missing in
	 * {@link M_DateTimezoneComponent::toString()}, this function will
	 * be used to inform the user about the error.
	 * 
	 * @access protected
	 * @param string $property
	 * @throws M_Exception
	 * @return void
	 */
	protected function _errorMissingProperty($property) {
		throw new M_Exception(sprintf(
			'Cannot proceed in %s::%s(); missing mandatory property %s',
			__CLASS__,
			'toString',
			$property
		));
	}
}