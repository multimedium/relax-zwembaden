<?php
/**
 * M_DateFormatPlaceholdersNl
 * 
 * @package Core
 */
class M_DateFormatPlaceholdersNl implements MI_DateFormatPlaceholders {
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get placeholders
	 * 
	 * @access public
	 * @return array $placeholders
	 *		Returns an associative array, of which the keys are the date part
	 *		symbols used in date formats, and the values are the corresponding
	 *		placeholders
	 */
	public static function getPlaceholders() {
		return array(
			'yyyyy' => 'jjjjj',
			'yyyy'  => 'jjjj',
			'yyy'   => 'jjjj',
			'yy'    => 'jj',
			'y'     => 'jjjj',
			'YYYY'  => 'jjjj',
			'YYY'   => 'jjj',
			'YY'    => 'jj',
			'MMMMM' => 'm',
			'MMMM'  => 'maand',
			'MMM'   => 'mnd',
			'MM'    => 'mm',
			'M'     => 'm',
			'dd'    => 'dd',
			'd'     => 'dd',
			'EEEEE' => 'dag (bv. D)',
			'EEEE'  => 'dag (bv. dinsdag)',
			'EEE'   => 'dag (bv. di)',
			'EE'    => 'dag (bv. di)',
			'E'     => 'dag (bv. di)',
			'ee'    => 'dag vd week (bv. 01)',
			'e'     => 'dag vd week (bv. 1)',
			'a'     => 'AM',
			'hh'    => 'uu',
			'h'     => 'u',
			'HH'    => 'uu',
			'H'     => 'u',
			'KK'    => 'uu',
			'K'     => 'u',
			'kk'    => 'uu',
			'k'     => 'u',
			'mm'    => 'mm',
			'm'     => 'm',
			'ss'    => 'ss',
			's'     => 's'
		);
	}
}