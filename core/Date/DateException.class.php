<?php
/**
 * M_DateException class
 * 
 * This class handles the exceptions that are thrown by the
 * M_Date API:
 * 
 * - {@link M_Date}
 * - {@link M_Calendar}
 * - {@link M_CalendarDaysIterator}
 * 
 * @package Core
 */
class M_DateException extends M_Exception {
}
?>