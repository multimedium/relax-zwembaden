<?php
/**
 * DateFormatPlaceholdersEn
 * 
 * @package Core
 */
class M_DateFormatPlaceholdersEn implements MI_DateFormatPlaceholders {
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get placeholders
	 * 
	 * @access public
	 * @return array $placeholders
	 *		Returns an associative array, of which the keys are the date part
	 *		symbols used in date formats, and the values are the corresponding
	 *		placeholders
	 */
	public static function getPlaceholders() {
		return array(
			'yyyyy' => 'yyyyy',
			'yyyy'  => 'yyyy',
			'yyy'   => 'yyyy',
			'yy'    => 'yy',
			'y'     => 'yyyy',
			'YYYY'  => 'yyyy',
			'YYY'   => 'yyy',
			'YY'    => 'yy',
			'MMMMM' => 'm',
			'MMMM'  => 'month',
			'MMM'   => 'mth',
			'MM'    => 'mm',
			'M'     => 'm',
			'dd'    => 'dd',
			'd'     => 'd',
			'EEEEE' => 'day (eg. T)',
			'EEEE'  => 'day (eg. Tuesday)',
			'EEE'   => 'day (eg. Tues)',
			'EE'    => 'day (eg. Tues)',
			'E'     => 'day (eg. Tues)',
			'ee'    => 'day of week (eg. 01)',
			'e'     => 'day of week (eg. 1)',
			'a'     => 'AM/PM',
			'hh'    => 'hh',
			'h'     => 'h',
			'HH'    => 'HH',
			'H'     => 'H',
			'KK'    => 'hh',
			'K'     => 'h',
			'kk'    => 'HH',
			'k'     => 'H',
			'mm'    => 'mm',
			'm'     => 'm',
			'ss'    => 'ss',
			's'     => 's'
		);
	}
}