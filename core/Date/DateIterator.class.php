<?php
/**
 * M_DateIterator class
 * 
 * Used to loop through a collection of {@link M_Date} objects which are
 * stored chronologically, typically providing a range of dates.
 * 
 * @package Core
 */
class M_DateIterator implements Countable, Iterator {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Count of dates in the iterator
	 * 
	 * @see M_DateIterator::getCount()
	 * @access private
	 * @var integer
	 */
	private $_count;
	
	/**
	 * The cursor position in the iterator
	 * 
	 * @access private
	 * @var integer
	 */
	private $_index;
	
	/**
	 * The first date of the collection
	 * 
	 * @see M_DateIterator::getFirstDay()
	 * @access private
	 * @var M_Date
	 */
	private $_firstDay;
	
	/**
	 * The last date of the collection
	 * 
	 * @see M_DateIterator::getLastDay()
	 * @access private
	 * @var M_Date
	 */
	private $_lastDay;
	
	/**
	 * The date object
	 * 
	 * ...as returned by the {@link M_DateIterator::current()} method.
	 * 
	 * @see M_DateIterator::current()
	 * @access private
	 * @var M_Date
	 */
	private $_date;
	
	/**
	 * Iterator Diff
	 * 
	 * Holds the iterator difference we have to use. For more info, read the
	 * docs on {@link M_DateIterator::setIteratorDiff()}
	 * 
	 * @see M_DateIterator::setIteratorDiff()
	 * @access private
	 * @var string
	 */
	private $_diff = M_Date::DAY;
	
	/**
	 * Iterator Step
	 * 
	 * Holds the step of iterator difference we have to use. For more info,
	 * read the docs on {@link M_DateIterator::setIteratorDiff()}
	 * 
	 * @see {@link M_DateIterator::setIteratorDiff()}
	 * @access private
	 * @var int
	 */
	private $_step = 1;
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param M_Date $startDate
	 *		The start date of the date iterator
	 * @return M_DateIterator
	 */
	public function __construct(M_Date $startDate = null) {
		// If no start date was given
		if(! $startDate) $startDate = new M_Date();
		
		// Set properties
		$this->_count     = 1;
		$this->_index     = 0;
		$this->_firstDay  = clone $startDate;
		$this->_date      = clone $startDate;
	}
	
	/**
	 * Clone
	 * 
	 * If the iterator is cloned, make sure all properties referencing an
	 * object properly get cloned as well
	 * 
	 * @see http://www.php.net/manual/en/language.oop5.cloning.php
	 * @access public
	 * @return M_DateIterator
	 */
	public function __clone() {
		$this->_firstDay = clone $this->_firstDay;
		$this->_lastDay = clone $this->_lastDay;
		$this->_date = clone $this->_date;
	}
	
	/* -- ITERATOR, COUNTABLE INTERFACE -- */
	
	/**
	 * Support for the Countable interface
	 *
	 * @access public
	 * @return integer
	 * 		The item count
	 */
	public function count() {
		return $this->_count;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return M_Date $date
	 * 		The current date object
	 */
	public function current() {
		return clone $this->_date;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return string
	 * 		The key of the current date
	 */
	public function key() {
		return $this->_date->timestamp;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Moves the internal cursor to the next item in stored data
	 * 
	 * @access public
	 * @return void
	 */
	public function next() {
		$this->_date->addTime($this->_step, $this->_diff);
		$this->_index += 1;
	}
	
	/**
	 * Support for the Iterator interface
	 * 
	 * Resets the internal cursor for iteration. foreach() calls on
	 * an instance of the class will call the rewind()
	 * method, to start looping at the beginning.
	 *
	 * @access public
	 * @return void
	 */
	public function rewind() {
		$this->_date = clone $this->_firstDay;
		$this->_index = 0;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Will indicate if the end of the iterator has been reached.
	 * 
	 * @access public
	 * @return boolean
	 * 		Will return TRUE if not reached the end of the iterator,
	 * 		or FALSE if all items in the iterator have been accessed
	 * 		in the loop.
	 */
	public function valid() {
		return $this->_index < $this->_count;
	}
	
	/* -- ADDITIONAL ITERATOR FUNCTIONS -- */
	
	/**
	 * Set Count
	 * 
	 * @access public
	 * @param int $count
	 * @return M_DateIterator
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCount($count) {
		$this->_count = (int) $count;
		return $this;
	}
	
	/**
	 * Get Count
	 * 
	 * @access public
	 * @return int
	 */
	public function getCount() {
		return $this->_count;
	}
	
	/**
	 * Set Index
	 * 
	 * @access public
	 * @param int $index
	 * @return M_DateIterator
	 *		Returns itself, for a fluent programming interface
	 */
	public function setIndex($index) {
		$this->_index = (int) $index;
		return $this;
	}
	
	/**
	 * Get Index
	 * 
	 * @access public
	 * @return int
	 */
	public function getIndex() {
		return $this->_index;
	}
	
	/**
	 * Set first day in the iterator
	 *
	 * @access public
	 * @param M_Date $firstDay
	 * @return M_DateIterator
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFirstDay(M_Date $firstDay) {
		$this->_firstDay = clone $firstDay;
		return $this;
	}
	
	/**
	 * Get first day in the iterator
	 *
	 * @access public
	 * @return M_Date
	 */
	public function getFirstDay() {
		return clone $this->_firstDay;
	}
	
	/**
	 * Is current date first in collection?
	 * 
	 * Will tell whether or not the current date in the collection, as provided
	 * by {@link M_DateIterator::current()}, is the first day in the 
	 * collection of dates. Returns TRUE if so, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isCurrentFirstDate() {
		return ($this->_date->equals($this->_firstDay, M_Date::DAY));
	}

	/**
	 * Get last day
	 *
	 * @access public
	 * @return M_Date
	 */
	public function getLastDay() {
		return clone $this->_lastDay;
	}
	
	/**
	 * Set last day in the iterator
	 * 
	 * Forms an alias to {@link DateIterator::setLastDate()}, for API
	 * consistency
	 *
	 * @access public
	 * @param M_Date $lastDay
	 * @return M_DateIterator
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLastDay(M_Date $lastDay) {
		$this->setEndDate($lastDay);
	}
	
	/**
	 * Is current date last in collection?
	 * 
	 * Will tell whether or not the current date in the collection, as provided
	 * by {@link M_DateIterator::current()}, is the last day in the 
	 * collection of dates. Returns TRUE if so, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isCurrentLastDate() {
		return ($this->_date->equals($this->_lastDay, M_Date::DAY));
	}
	
	/**
	 * Set Date
	 * 
	 * @access public
	 * @param M_Date $date
	 * @return M_DateIterator
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDate(M_Date $date) {
		$this->_date = clone $date;
		return $this;
	}
	
	/**
	 * Get Date
	 * 
	 * @access public
	 * @return M_Date
	 */
	public function getDate() {
		return clone $this->_date;
	}
	
	/**
	 * Set End Date
	 * 
	 * Allows for setting the end date for the iterator. This option is
	 * typically used to create a date range between 2 dates.
	 * 
	 * @access public
	 * @param M_Date $endDate
	 * @return DateIterator
	 *		Returns itself, for a fluent programming interface
	 */
	public function setEndDate(M_Date $endDate) {
		// Set the property
		$this->_lastDay = clone $endDate;
		
		// Now we have to set the count. To calculate it, we need to fetch the
		// number of diffs between the start- and end date. Before we can do
		// so however, check whether the currently set diff is a day or more.
		// If that is the case, we have to reset the times of the start- and
		// end dates, otherwise the elapsed time might be calculated wrong
		$firstDay = clone $this->_firstDay;
		if(! in_array($this->_diff, array(M_Date::SECOND, M_Date::MINUTE, M_Date::HOUR))) {
			// Set the start time and end time properly
			$firstDay->setTime(0, 0, 0);
			$endDate = clone $endDate;
			$endDate->setTime(23, 59, 59);
		}
		
		// Calculate the count. Note that we add 1, as the count of the iterator
		// is always at least 1. Also: devide the elapsed time by the step, so
		// if we for example loop using 30 minute intervals, the correct count
		// is returned
		$this->_count = (int) ($endDate->getElapsedTimeSince($firstDay, $this->_diff) / $this->_step) + 1;
		
		// Return self
		return $this;
	}
	
	/**
	 * Set Iterator Diff
	 * 
	 * May be used to set the iterator difference property. This property
	 * is used while looping through the iterator to determine the steps
	 * we have to use. By default the difference is one {@link M_Date::DAY}.
	 * While looping through the iterator, this will result in single days
	 * as steps. For example:
	 * 
	 * <code>
	 *	$date = new M_Date('2011-01-01');
	 *	$iterator = new M_DateIterator($date);
	 *	$iterator->setIteratorDiff(M_Date::DAY, 1);
	 *	foreach($iterator as $day) {
	 *		echo $day->toString() . '<br/>';
	 *	);
	 * </code>
	 * 
	 * Will result in the following output:
	 * 
	 * <code>
	 *	1 January 2011
	 *	2 January 2011
	 *	3 January 2011
	 *	4 January 2011
	 *	5 January 2011
	 *	...
	 * </code>
	 * 
	 * If we set the difference property to a single {@link M_Date::WEEK}
	 * through for example, this will result in the following:
	 * 
	 * <code>
	 *	$date = new M_Date('2011-01-01');
	 *	$iterator = new M_DateIterator($date);
	 *	$iterator->setIteratorDiff(M_Date::WEEK, 1);
	 *	foreach($iterator as $day) {
	 *		echo $day->toString() . '<br/>';
	 *	);
	 * </code>
	 * 
	 * Will result in the following output:
	 * 
	 * <code>
	 *	1 January 2011
	 *	8 January 2011
	 *	15 January 2011
	 *	22 January 2011
	 *	29 January 2011
	 *	...
	 * </code>
	 * 
	 * Effectively looping through the weeks of the year. As second parameter
	 * you may provide the steps: the number of differences we have to
	 * use at a time. This may for example be used to iterate through
	 * every 2 weeks of the year, or every 5 days, etc. For example:
	 * 
	 * <code>
	 *	$date = new M_Date('2011-01-01');
	 *	$iterator = new M_DateIterator($date);
	 *	$iterator->setIteratorDiff(M_Date::WEEK, 2);
	 *	foreach($iterator as $day) {
	 *		echo $day->toString() . '<br/>';
	 *	);
	 * </code>
	 * 
	 * Will result in the following output, skipping a week every time:
	 * 
	 * <code>
	 *	1 January 2011
	 *	15 January 2011
	 *	29 January 2011
	 *	...
	 * </code>
	 * 
	 * Do note that in these examples we did not specify an end date, so
	 * the loop will keep running forever. To specify an end date,
	 * use either of the following 2 functions:
	 * 
	 * - {@link M_DateIterator::setEndDate()}
	 * - {@link M_DateIterator::setLimit()}
	 * 
	 * @see M_Date::addTime()
	 * @access public
	 * @param string $diff
	 * @param int $step
	 * @return DateIterator
	 *		Returns itself, for a fluent programming interface
	 */
	public function setIteratorDiff($diff, $step) {
		// Make sure the provided diff is valid
		if(! in_array($diff, array(
			M_Date::HOUR, M_Date::MINUTE, M_Date::WEEK, M_Date::SECOND,
			M_Date::DAY, M_Date::MONTH, M_Date::YEAR
		))) {
			throw new M_Exception(sprintf(
				'Invalid difference type provided in %s::%s',
				__CLASS__,
				__FUNCTION__
			));
		}

		// Set the difference
		$this->_diff = $diff;
		
		// Set the step
		$this->_step = $step;
		
		// Return self
		return $this;
	}
	
	/**
	 * Get Iterator Diff
	 * 
	 * Will return the diff that is currently applied to the iterator, which
	 * is represented by the combination of:
	 * 
	 * - the date part, like for example {@link M_Date::DAY}
	 * - the step, e.g. 5
	 * 
	 * For example, if the user specified the diff as 2 hours, this method
	 * will return an array looking as follows:
	 * 
	 * <code>
	 *		array('hour', 2)
	 * </code>
	 * 
	 * @see M_DateIterator::setIteratorDiff()
	 * @access public
	 * @return array
	 *		An array containing the date part as first element, and the step
	 *		as second element
	 */
	public function getIteratorDiff() {
		return array($this->_diff, $this->_step);
	}
	
	/**
	 * Set Limit
	 * 
	 * May be used to set the limit for this date iterator, e.g. the max
	 * number of iterations this iterator is allowed to make.
	 * 
	 * @access public
	 * @param int $iterations
	 * @return M_DateIterator
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLimit($iterations) {
		$iterations = (int) $iterations;
		
		// Set the count
		$this->_count = $iterations;
		
		// Calculate the new end date. Note that we take the step into account:
		$endDate = clone $this->_firstDay;
		$endDate->addTime(($iterations * $this->_step) - 1, $this->_diff);
		
		// And set it
		$this->setEndDate($endDate);
		
		// Return self
		return $this;
	}
}