<?php
class M_FeedRssItem extends M_FeedItem implements MI_FeedItem {
	public function toString() {
		$out  = '<item>';
		
		$temp = $this->getTitle();
		if($temp) {
			$out .=    '<title><![CDATA['. $temp .']]></title>';
		}
		
		$temp = $this->getLink();
		if($temp) {
			$out .=    '<link><![CDATA['. $temp .']]></link>';
		}
		
		$temp = $this->getDescription();
		if($temp) {
			$out .=    '<description><![CDATA['. $temp .']]></description>';
		}
		
		$date = $this->getPublicationDate();
		if($date) {
			$out .=    '<pubDate>'. date('r', $date->timestamp) .'</pubDate>';
		}
		
		$out .= '</item>';
		return $out;
	}
	
	public function __toString() {
		return $this->toString();
	}
}