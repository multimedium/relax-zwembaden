<?php
/**
 * M_FeedException
 * 
 * Handles exceptions thrown by {@link MI_Feed} and {@link MI_FeedItem} classes.
 * 
 * @package Core
 */
class M_FeedException extends M_Exception {
}