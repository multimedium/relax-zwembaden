<?php
/**
 * M_FeedImage
 *
 * M_FeedImage is an implementation of {@link MI_FeedImage}, representing
 * image(s) that can be added to an RSS feed.
 *
 * {@see http://www.w3schools.com/rss/rss_tag_image.asp}
 * @package Core
 */
class M_FeedImage implements MI_FeedImage {

	/* -- PROPERTIES -- */

	/**
	 * Holds the title of the image. For more info, read the docs on
	 * {@link M_FeedImage::setTitle()}
	 *
	 * @access private
	 * @var string
	 */
	private $_title;

	/**
	 * Holds the description of the image. For more info, read the docs on
	 * {@link M_FeedImage::setDescription()}
	 *
	 * @access private
	 * @var string
	 */
	private $_description;

	/**
	 * Holds the {@link M_Uri} representing the url to the image. For more
	 * info, read the docs on {@link M_FeedImage::setUrl()}
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_url;

	/**
	 * Holds the {@link M_Uri} representing the link placed on the image. For
	 * more info, read the docs on {@link M_FeedImage::setLink()}
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_link;

	/**
	 * Holds the height of the image. For more info, read the docs on
	 * {@link M_FeedImage::setHeight()}
	 *
	 * @access private
	 * @var int
	 */
	private $_height;

	/**
	 * Holds the width of the image. For more info, read the docs on
	 * {@link M_FeedImage::setWidth()}
	 *
	 * @access private
	 * @var int
	 */
	private $_width;

	/* -- SETTERS / GETTERS -- */

	/**
	 * Set Title
	 *
	 * Allows for setting the title of the image, shown if the image would not
	 * be displayed.
	 *
	 * NOTE: this property is required
	 *
	 * @access public
	 * @param string $title
	 * @return M_FeedImage
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
		return $this;
	}

	/**
	 * Get Title
	 *
	 * Returns the title, set in {@mink M_FeedImage::setTitle()}
	 *
	 * @access public
	 * @return string $title
	 */
	 public function getTitle() {
		 return $this->_title;
	 }

	/**
	 * Set Description
	 *
	 * Allows for setting the description of the image. This discription is
	 * shown as title of the link, displayed on mouse hover over the image.
	 *
	 * @access public
	 * @param string $title
	 * @return M_FeedImage
	 *		Returns itself, for a fluent programming interface
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
		return $this;
	}

	/**
	 * Get Description
	 * 
	 * Returns the title, set in {@mink M_FeedImage::setDescription()}
	 *
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * Set Url
	 *
	 * Allows for setting the {@link M_Uri} object representing the direct link
	 * to the image itself.
	 *
	 * NOTE: this property is required
	 *
	 * @access public
	 * @param M_Uri $url
	 * @return M_FeedImage
	 *		Returns itself, for a fluent programming interface
	 */
	public function setUrl(M_Uri $url) {
		$this->_url = $url;
		return $this;
	}

	/**
	 * Get Url
	 *
	 * Returns the url, set in {@link M_FeedImage::setUrl()}
	 *
	 * @access public
	 * @return M_Uri
	 */
	public function getUrl() {
		return $this->_url;
	}

	/**
	 * Set Link
	 *
	 * Allows for setting the {@link M_Uri} object representing the link that
	 * is placed on the image. Typically, this is the link to the homepage
	 * of the RSS owner's website.
	 *
	 * NOTE: this property is required
	 *
	 * @access public
	 * @param M_Uri $link
	 * @return M_FeedImage
	 *		Returns itself, for a fluent programming interface
	 */
	public function setLink(M_Uri $link) {
		$this->_link = $link;
		return $this;
	}

	/**
	 * Get Link
	 *
	 * Returns the link, set in {@link M_FeedImage::setLink()}
	 *
	 * @access public
	 * @return M_Uri
	 */
	public function getLink() {
		return $this->_link;
	}

	/**
	 * Set Height
	 *
	 * Allows for setting the height of the image. By default this value is
	 * 31. The maximum allowed is 400.
	 *
	 * @access public
	 * @param in $height
	 * @return M_FeedImage
	 *		Returns itself, for a fluent programming interface
	 */
	public function setHeight($height) {
		$this->_height = (int) $height;
		return $this;
	}

	/**
	 * Get Height
	 *
	 * Returns the height, set in {@link M_FeedImage::getHeight()}
	 *
	 * @access public
	 * @return int
	 */
	public function getHeight() {
		return $this->_height;
	}

	/**
	 * Set Width
	 *
	 * Allows for setting the width of the image. By default this value is
	 * 88. The maximum allowed is 144.
	 *
	 * @access public
	 * @param in $width
	 * @return M_FeedImage
	 *		Returns itself, for a fluent programming interface
	 */
	public function setWidth($width) {
		$this->_width = (int) $width;
		return $this;
	}

	/**
	 * Get Width
	 *
	 * Returns the width, set in {@link M_FeedImage::getWidth()}
	 *
	 * @access public
	 * @return int
	 */
	public function getWidth() {
		return $this->_width;
	}

	/* -- PUBLIC FUNCTIONS -- */

	/**
	 * To String
	 * 
	 * Will create and return a proper xml node with the specified image
	 * properties. For more information, read the docs on
	 * {@link http://www.w3schools.com/rss/rss_tag_image.asp}
	 * 
	 * @access public
	 * @return string
	 *		The image, specified as an xml tag
	 */
	public function toString() {
		// Start output
		$out = '';

		// Open image tag
		$out .= '<image>';

		// Add required property: url
		$url = $this->getUrl();
		if($url) {
			$out .= '<url><![CDATA['. $url->toString() .']]></url>';
		} else {
			throw new M_FeedException(sprintf(
				'Please provide the url to the image; this is mandatory as without it the image cannot be shown'
			));
		}

		// Add required property: title
		$title = $this->getTitle();
		if($title) {
			$out .= '<title><![CDATA['. $title .']]></title>';
		} else {
			throw new M_FeedException(sprintf(
				'Please provide a title for the feed image; this is mandatory as it is shown if the image could not be found'
			));
		}

		// Add required property: link
		$link = $this->getLink();
		if($link) {
			$out .= '<link><![CDATA['. $link->toString() .']]></link>';
		} else {
			throw new M_FeedException(sprintf(
				'Please provide a link for the feed image; this is mandatory as specifies the link the user will go to by clicking on the image'
			));
		}

		// Add optional property: description
		$description = $this->getDescription();
		if($description) {
			$out .= '<description><![CDATA['. $description .']]></description>';
		}

		// Add optional property: height
		$height = $this->getHeight();
		if($height) {
			$out .= '<height><![CDATA['. $height .']]></height>';
		}

		// Add optional property: width
		$width = $this->getWidth();
		if($width) {
			$out .= '<width><![CDATA['. $width .']]></width>';
		}

		// Close image tag
		$out .= '</image>';

		// Finally, return the output string
		return $out;
	}
}