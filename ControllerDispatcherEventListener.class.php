<?php
/**
 * M_ControllerDispather Event Listener
 * 
 * Will respond to the events that are dispatched by the Controller Dispatcher.
 * For more info, read the docs on:
 * 
 * - {@link M_ControllerDispatcher}
 * - {@link M_ControllerDispatcherEvent}
 * 
 * @package App
 */
class ControllerDispatcherEventListener {
	/**
	 * When path is received
	 * 
	 * @access public
	 * @param M_ControllerDispatcherEvent $event
	 * @return void
	 */
	public function onReceivePath(M_ControllerDispatcherEvent $event) {
		// Get the controller dispatcher:
		$dispatcher = $event->getControllerDispatcher();
		
		// Get the locale from the path:
		$locale = $dispatcher->shiftPathElement();
		
		// Make sure that the locale is supported:
		$isLocaleSupported = ($locale && M_LocaleMessageCatalog::isInstalled($locale));
		
		// If the path is empty, or the locale is not supported:
		if(! $dispatcher->getPath() || ! $isLocaleSupported) {
			// Then, we redirect to the homepage:
			M_Header::redirect(
				$dispatcher
					->getHomepageUri()
					->getRelativePath()
			);
		}
		
		// Set the locale:
		M_Locale::setCategory(M_Locale::LANG, $locale);
		
		// Also, set prefix for all links in the application
		M_Request::setLinkPrefix($locale);
	}
}