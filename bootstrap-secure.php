<?php
// Blocked File Extensions:
define('M_BLOCK_FILE_UPLOAD_EXTENSIONS', '.php,.exe,.asp,.js');

// Only allow these request methods
define('M_REQUEST_METHODS_ALLOWED',      'GET,POST,PUT,DELETE');

// Utility Functions
function _isSuspiciousFile($value) {
    if(! is_array($value)) {
        $value = (string) $value;
        foreach(explode(',', M_BLOCK_FILE_UPLOAD_EXTENSIONS) as $extension) {
            if(stripos($value, $extension) !== FALSE) { return TRUE; }
        }
    } else {
        foreach($value as $current) {
            if(_isSuspiciousFile($current)) { return TRUE; }
        }
    }
    return FALSE;
}
function _exitSuspiciousRequest() {
    // Tease that motha-fuckin hacker bitch, and
    // make him wait for a response
    sleep(rand(10, 20)); die();
}

// Stop file uploads with blocked extensions
foreach($_FILES as $file) {
    if(! isset($file['name'])) {
        _exitSuspiciousRequest();
    }
    if(_isSuspiciousFile($file['name'])) {
        _exitSuspiciousRequest();
    }
}

// Only allow these request methods:
$methods = explode(',', M_REQUEST_METHODS_ALLOWED);
if(! in_array($_SERVER['REQUEST_METHOD'], $methods)) {
    _exitSuspiciousRequest();
}