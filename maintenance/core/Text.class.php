<?php
/**
 * M_Text class
 * 
 * @package Core
 */
class M_Text extends M_Object {
	/**
	 * The text
	 * 
	 * This property holds a string; the text on which the M_Text
	 * object does its operations.
	 * 
	 * @access private
	 * @var string
	 */
	private $_text;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $text
	 * 		The text on which to construct an M_Text object
	 * @return M_Text
	 */
	public function __construct($text) {
		$this->_text = (string) $text;
	}
	
	/**
	 * Map keywords to URL's
	 * 
	 * This method will map a given keyword (or multiple) to a URL. By
	 * doing so, it will make a link out of the provided keywords. 
	 * Note that this method will perform a case-insensitive (!) scan 
	 * on the text, in order to find occurrences of the provided 
	 * keyword(s).
	 * 
	 * Example 1, make the keyword "google" link to www.google.com
	 * <code>
	 *    $text = new M_Text('Google is the number one search engine!');
	 *    $text->mapKeywords(array(
	 *       'google' => 'http://www.google.com'
	 *    ));
	 * </code>
	 * 
	 * You can also provide the link format to this method, by passing 
	 * in a second argument. This argument (string) is the link 
	 * pattern, which will be used to render each of the links in the 
	 * text. You can make use of the following syntax variables:
	 * 
	 * <code>
	 *    @keyword
	 *    @url
	 * </code>
	 * 
	 * Example 2, open "google" keyword in a new window:
	 * <code>
	 *    $text = new M_Text('Google is the number one search engine!');
	 *    $text->mapKeywords(
	 *       array(
	 *          'google' => 'http://www.google.com'
	 *       ),
	 *       '<a href="@url" target="_blank">@keyword</a>'
	 *    );
	 * </code>
	 * 
	 * NOTE:
	 * The link format is defaulted to 
	 * 
	 * <code>
	 *    <a href="@url" title="link: @keyword">@keyword</a>
	 * </code>
	 * 
	 * @access public
	 * @uses M_Text::_doKeywordReplacement()
	 * @param array $keywords
	 * 		Keyword-URL mapping array
	 * @param string $format
	 * 		The link format
	 * @return void
	 */
	public function mapKeywords(array $keywords, $format = NULL) {
		// Default the format, if not provided
		if(!$format) {
			$format = '<a href="@url" title="link: @keyword">@keyword</a>';
		}
		
		// Force the keywords to lowercase
		$keywords = array_change_key_case($keywords, CASE_LOWER);
		
		// prepare the pattern
		$pattern = '(' . implode('|', array_map('preg_quote', array_keys($keywords))) . ')';
		$pattern = '/(.?)'. $pattern .'(.?)/ie';
		
		// do the keyword replacements:
		$this->_text = preg_replace(
			$pattern, 
			'$this->_doKeywordReplacement("\\2", "\\1", "\\3", $keywords, $format)', 
			$this->_text
		);
	}
	
	/**
	 * Do keyword replacement
	 * 
	 * Internal function, only available to the M_Text class itself. 
	 * This method is used by {@link M_Text::mapKeywords()} to do the 
	 * replacements in the text.
	 * 
	 * @access private
	 * @param string $keyword
	 * 		The matched keyword, from the original text. Does not 
	 * 		necessarily match in case with the keyword in the 
	 * 		Keyword-URL mapping array.
	 * @param string $charBefore
	 * 		The character that goes before the matched keyword
	 * @param string $charAfter
	 * 		The character that goes after the matched keyword
	 * @param string $keywords
	 * 		The original Keyword-URL mapping array (with lowercased 
	 * 		array keys)
	 * @param string $format
	 * 		The link format
	 * @return string
	 */
	private function _doKeywordReplacement($keyword, $charBefore, $charAfter, $keywords, $format) {
		// we only do replacements of separate words:
		if($this->isPunctuation($charBefore) && $this->isPunctuation($charAfter)) {
			return $charBefore . strtr($format, array(
				'@keyword' => $keyword,
				'@url'     => M_Helper::getArrayElement(strtolower($keyword), $keywords)
			)) . $charAfter;
		}
		// if not a separate word, we leave it untouched
		else {
			return $charBefore . $keyword . $charAfter;
		}
	}
	
	/**
	 * Make URL clickable
	 * 
	 * This method will scan the text for all occurrences of a URL. In
	 * the process, it will try to parse an HTML link out of each 
	 * URL it finds. Note that, similarly to {@link M_Text::mapKeywords()},
	 * you can also provide this method with a link format. This 
	 * format is the pattern that will be used to render each of the 
	 * links in the text. You can make use of the following syntax 
	 * variables in this format string:
	 * 
	 * <code>
	 *    @href
	 *    @url
	 * </code>
	 * 
	 * NOTE:
	 * The link format is defaulted to:
	 * <code><a href="@href" title="link: @url">@url</a></code>
	 * 
	 * Example 1, make URL's clickable in the text
	 * <code>
	 *    $text = new M_Text('Google is at www.google.com.');
	 *    $text->makeUrlClickable();
	 *    echo $text;
	 * </code>
	 * 
	 * Example 1 would output:
	 * <code>
	 *    Google is at <a href="http://www.google.com" title="link: www.google.com">www.google.com</a>.
	 * </code>
	 * 
	 * Example 2, does the same thing as Example 1, but makes the 
	 * links open in a new window:
	 * <code>
	 *    $text = new M_Text('Google is at www.google.com.');
	 *    $text->makeUrlClickable('<a href="@href" target="_blank">@url</a>');
	 *    echo $text;
	 * </code>
	 * 
	 * Example 2 would output:
	 * <code>
	 *    Google is at <a href="http://www.google.com" target="_blank">www.google.com</a>.
	 * </code>
	 * 
	 * @access public
	 * @uses M_Text::_doUrlClickable()
	 * @see M_Text::__toString()
	 * @param string $format
	 * 		The link format
	 * @return void
	 */
	public function makeUrlClickable($format = NULL) {
		// Default the format, if not provided
		if(!$format) {
			$format = '<a href="@href" title="link: @url">@url</a>';
		}
		
		// scan for URL's and make them clickable
		// (first, we scan for strings that start with a protocol)
		$this->_text = preg_replace(
			'/(.?)(http|https)(:\/\/[^\s]+)(.?)/ie', 
			'$this->_doUrlClickable("\\2\\3", "\\1", "\\4", $format)', 
			$this->_text
		);
		
		// scan for URL's and make them clickable
		// (now, we scan for strings that "look like an URL")
		$this->_text = preg_replace(
			'/(.?)([a-z0-9]+\.[a-z0-9]+\.[a-z]{2,3}\/?[a-z0-9\/\?&%="\._-]*)(.?)/ie', 
			'$this->_doUrlClickable("\\2", "\\1", "\\3", $format)', 
			$this->_text
		);
	}
	
	/**
	 * Do URL replacement
	 * 
	 * Internal function, only available to the M_Text class itself. 
	 * This method is used by {@link M_Text::makeUrlClickable()} to do 
	 * the replacements in the text.
	 * 
	 * @access private
	 * @see M_Text::makeUrlClickable()
	 * @uses M_Text::isPunctuation()
	 * @uses M_Text::getLeftRightPunctuation()
	 * @param string $url
	 * 		The matched URL, from the original text. This URL is not
	 * 		necessarily the final link href.
	 * @param string $charBefore
	 * 		The character that goes before the matched URL
	 * @param string $charAfter
	 * 		The character that goes after the matched URL
	 * @param string $format
	 * 		The link format
	 * @return string
	 */
	private function _doUrlClickable($url, $charBefore, $charAfter, $format) {
		// we only do replacements of a separated URL:
		if($this->isPunctuation($charBefore) && $this->isPunctuation($charAfter)) {
			// separate punctuation from the URL
			$url = $this->getLeftRightPunctuation($url);
			
			// return parsed url
			return $charBefore . $url[0] . strtr($format, array(
				'@url'  => $url[1],
				'@href' => strncmp($url[1], 'http', 4) ? 'http://' . $url[1] : $url[1]
			)) . $url[2] . $charAfter;
		}
		// if not a separated URL, we leave it untouched
		// (probably, it's already linked)
		else {
			return $charBefore . $url . $charAfter;
		}
	}
	
	/**
	 * Make email addresses clickable
	 * 
	 * This method will scan the text for occurrences of email 
	 * addresses. In the process, it will try to parse an HTML link 
	 * out of each email address it finds. Note that, similarly to 
	 * {@link M_Text::mapKeywords()} and {@link M_Text::makeUrlClickable()}, 
	 * you can also provide this method with a link format. This 
	 * format is the pattern that will be used to render each of the 
	 * links in the text. You can make use of the following syntax 
	 * variables in this format string:
	 * 
	 * <code>
	 *    @href
	 *    @email
	 * </code>
	 * 
	 * NOTE:
	 * The link format is defaulted to:
	 * <code><a href="@href" title="link: @email">@email</a></code>
	 * 
	 * Example 1, make email addresses clickable in the text
	 * <code>
	 *    $text = new M_Text('My email address is tom@multimedium.be.');
	 *    $text->makeEmailClickable();
	 *    echo $text;
	 * </code>
	 * 
	 * Example 1 would output:
	 * <code>
	 *    My email address is <a href="mailto:tom@multimedium.be" title="link: tom@multimedium.be">tom@multimedium.be</a>.
	 * </code>
	 * 
	 * @access public
	 * @uses M_Text::_doEmailClickable()
	 * @see M_Text::__toString()
	 * @param string $format
	 * 		The link format
	 * @return void
	 */
	public function makeEmailClickable($format = NULL) {
		// Default the format, if not provided
		if(!$format) {
			$format = '<a href="@href" title="link: @email">@email</a>';
		}
		
		// scan for email addresses and make them clickable
		$this->_text = preg_replace(
			'/(.?)([a-z0-9-_.]+@[a-z0-9]+\.[a-z]{2,3})(.?)/ie', 
			'$this->_doEmailClickable("\\2", "\\1", "\\3", $format)', 
			$this->_text
		);
	}
	
	/**
	 * Do email address replacement
	 * 
	 * Internal function, only available to the M_Text class itself. 
	 * This method is used by {@link M_Text::makeEmailClickable()} to 
	 * do the replacements in the text.
	 * 
	 * @access private
	 * @uses M_Text::isPunctuation()
	 * @see M_Text::makeEmailClickable()
	 * @param string $email
	 * 		The matched email address, from the original text. This 
	 * 		email address is not necessarily the final link href.
	 * @param string $charBefore
	 * 		The character that goes before the matched email address
	 * @param string $charAfter
	 * 		The character that goes after the matched email address
	 * @param string $format
	 * 		The link format
	 * @return string
	 */
	private function _doEmailClickable($email, $charBefore, $charAfter, $format) {
		// we only do replacements of a separated email address:
		if($this->isPunctuation($charBefore) && $this->isPunctuation($charAfter)) {
			return $charBefore . strtr($format, array(
				'@email' => $email,
				'@href'  => strncmp($email, 'mailto:', 7) ? 'mailto:' . $email : $email
			)) . $charAfter;
		}
		// if not a separated URL, we leave it untouched
		// (probably, it's already linked)
		else {
			return $charBefore . $email . $charAfter;
		}
	}
	
	/**
	 * Is punctuation?
	 * 
	 * This method will tell whether or not a given character is a 
	 * punctuation mark. Punctuation marks are characters such as 
	 * . ? ! , : ; " '
	 * 
	 * This method will return TRUE if the given character is 
	 * considered to be a punctuation mark, FALSE if not. Note that
	 * this method will also interpret a white-space (including an 
	 * empty argument) as a punctuation mark.
	 * 
	 * This method is currently being used by M_Text, to separate
	 * keywords, URL's and email addresses from their surrounding
	 * punctuation marks:
	 * 
	 * - {@link M_Text::mapKeywords()}
	 * - {@link M_Text::makeUrlClickable()}
	 * - {@link M_Text::makeEmailClickable()}
	 * 
	 * @access public
	 * @param string $character
	 * 		The character to evaluated as punctuation mark
	 * @return boolean
	 */
	public function isPunctuation($character) {
		return (empty($character) || in_array($character, array(' ', '.', '?', '!', ',', ':', ';', '"', "'")));
	}
	
	/**
	 * Separate surrounding punctuation
	 * 
	 * This method will separate the surrounding punctuation marks 
	 * from the contained string (both left and right). This method is
	 * kind of similar to a trim() call, except that this method:
	 * 
	 * - strips punctuation marks
	 * - returns both the contained text and the stripped characters
	 * 
	 * Example 1, get surrounding punctuation:
	 * <code>
	 *    $text = new M_Text;
	 *    $text->getLeftRightPunctuation('.;Hello World!?!');
	 * </code>
	 * 
	 * Example 1 would output:
	 * <code>
	 *    Array (
	 *       [0] => .;
	 *       [1] => Hello World
	 *       [2] => !?!
	 *    )
	 * </code>
	 * 
	 * As you can see in Example 1, this method will always return an
	 * array with three elements:
	 * 
	 * 0) The characters stripped from the left hand side
	 * 1) The text contained inside
	 * 2) The characters stripped from the right hand side
	 * 
	 * @access public
	 * @param string $string
	 * 		The string to operate on
	 * @return array
	 */
	public function getLeftRightPunctuation($string) {
		$output = array('', '', '');
		
		$i = 0;
		while($this->isPunctuation($string{$i})) {
			$output[0] .= $string{$i ++};
		}
		
		$j = strlen($string);
		while($this->isPunctuation($string{$j})) {
			$output[2]  = $string{$j --} . $output[2];
		}
		
		$output[1] = substr($string, $i, $j - $i + 1);
		return $output;
	}
	
	/**
	 * Get snippet
	 * 
	 * This method will return an extract from the original text. To 
	 * do so, it will accept a maximum length for the snippet. Note
	 * that this method will try not to break words in the original 
	 * text, by looking for the closes whitespace character.
	 * 
	 * IMPORTANT NOTE:
	 * To reliably provide with an extract of the text, this method
	 * will first remove all HTML from the text subject. To do so, it 
	 * uses {@link M_FilterTextHtml}.
	 * 
	 * @uses M_FilterTextHtml
	 * @access public
	 * @param integer $maxlength
	 * 		The maximum length of the snippet (number of characters)
	 * @param integer $etc
	 * 		The "etcetera" symbol (appended to the snippet only if the
	 * 		original text has been shortened in order to match the 
	 * 		provided maximum length)
	 * @return string
	 */
	public function getSnippet($maxlength, $etc = '...') {
		// First, remove HTML
		$filter = new M_FilterTextHtml(new M_FilterTextValue($this->_text));
		$text   = $filter->apply();
		
		// Also, remove redundant white-spaces
		$text   = preg_replace('/[\s]+/m', ' ', $text);
		
		// Prepare the extract:
		if(strlen($text) > $maxlength) {
			$out = substr($text, 0, $maxlength);
			$space = strrpos($out, ' ');
			if($space !== FALSE) {
				$out = substr($text, 0, $space);
			}
			return $out . $etc;
		} else {
			return $text;
		}
	}
	
	/**
	 * Get snippet, stop at sentence
	 * 
	 * This method will return an extract from the original text. To 
	 * do so, it will accept a maximum length for the snippet. Note
	 * that this method will try not to break sentences in the original 
	 * text, by looking for the closes punctuation character.
	 * 
	 * IMPORTANT NOTE:
	 * To reliably provide with an extract of the text, this method
	 * will first remove all HTML from the text subject. To do so, it 
	 * uses {@link M_FilterTextHtml}.
	 * 
	 * @uses M_FilterTextHtml
	 * @access public
	 * @param integer $maxlength
	 * 		The maximum length of the snippet (number of characters)
	 * @param integer $etc
	 * 		The "etcetera" symbol (appended to the snippet only if the
	 * 		original text has been shortened in order to match the 
	 * 		provided maximum length)
	 * @return string
	 */
	public function getSnippetSentences($maxlength, $etc = '...') {
		// First, remove HTML
		$filter = new M_FilterTextHtml(new M_FilterTextValue($this->_text));
		$text   = $filter->apply();
		
		// Also, remove redundant white-spaces
		$text   = preg_replace('/[\s]+/m', ' ', $text);
		
		// Prepare the extract:
		if(strlen($text) > $maxlength) {
			$out = substr($text, 0, $maxlength);
			$space = $this->_strrPosArray($out, array('. ', '?', '!'));
			if($space !== FALSE) {
				$out = substr($text, 0, $space+1);
			}
			return $out . $etc;
		} else {
			return $text;
		}
	}
	
	// TODO: getSnippetByKeywords()
	function getSnippetByKeywords(array $keywords, $format = NULL, $maxlength = 250, $etc = '... ') {
		// Remember the positions where keywords where found:
		$pos = array();
		$key = array();
		
		// Default the format, if not provided
		if(!$format) {
			$format = '<strong>\\1</strong>';
		} else {
			$format = str_replace('@keyword', '\\1', $format);
		}
		
		// If no keywords have been provided, return normal snippet
		if(count($keywords) == 0) {
			return $this->getSnippet($maxlength);
		}
		
		// For each of the keywords
		foreach($keywords as $keyword) {
			// For each of the positions at which the current keyword
			// occurs in the text:
			$len = strlen($keyword);
			$tmp = stripos($this->_text, $keyword);
			while($tmp !== FALSE) {
				// we round the current position to a factor of 30
				// (consider 30 like some kind of "density factor")
				$density = 30;
				$current = round($tmp / $density) * $density;
				
				// increment the number of times a keyword has been
				// found at that (rounded) position. This way, we 
				// create a map with keyword density per rounded 
				// position.
				$pos[$current] = isset($pos[$current]) ? $pos[$current] + 1 : 1;
				if(!isset($key[$current])) {
					$key[$current] = array();
				}
				if(!in_array($keyword, $key[$current])) {
					$key[$current][] = $keyword;
				}
				
				// prepare for the next loop
				$tmp  += $len;
				$tmp   = stripos($this->_text, $keyword, $tmp);
			}
		}
		
		// Order by density, in descending order
		// (comment line below to maintain natural order of text)
		arsort($pos, SORT_NUMERIC);
		
		// Now, we show extracts from the text (based on the highest
		// density positions)
		$out = '';
		foreach($pos as $i => $tmp) {
			if(strlen($out) < $maxlength) {
				$current = $i - $density;
				if($current > 0) {
					$len = strpos($this->_text, ' ', $current);
					if($len !== FALSE) {
						$current = $len;
					}
					$out .= $etc;
				}
				$tmp = substr($this->_text, $current, $density * 2);
				$len = strrpos($tmp, ' ');
				if($len !== FALSE) {
					$tmp = substr($tmp, 0, $len);
				}
				foreach($key[$i] as $keyword) {
					$tmp = preg_replace('/(' . preg_quote($keyword, '/') . ')/im', $format, $tmp);
				}
				$out .= $tmp;
				if($current + ($density * 2) < strlen($this->_text)) {
					$out .= $etc;
				}
			}
		}
		
		// return the final snippet:
		return $out;
	}
	
	function highlightKeywords(array $keywords, $format = NULL) {
		// Default the format, if not provided
		if(!$format) {
			$format = '<strong>@keyword</strong>';
		}
		
		// prepare the pattern
		$pattern = '(' . implode('|', array_map('strtolower', $keywords)) . ')';
		$pattern = '/(.?)'. $pattern .'(.?)/ie';
		
		// do the keyword replacements:
		$this->_text = preg_replace(
			$pattern, 
			'$this->_doKeywordReplacement("\\2", "\\1", "\\3", $keywords, $format)', 
			$this->_text
		);
	}
	
	/**
	 * Render diff
	 * 
	 * This method will compare with another instance of {@link M_Text}, and render
	 * a view of the diff result.
	 * 
	 * @access public
	 * @param M_Text $text
	 * 		The text to compare with
	 * @return string
	 */
	public function getDiff(M_Text $text) {
		// Load required PEAR Classes:
		M_Loader::loadRelative('core/' . FOLDER_THIRDPARTY . '/pear/Text/Diff.php');
		M_Loader::loadRelative('core/' . FOLDER_THIRDPARTY . '/pear/Text/Diff/Renderer.php');
		M_Loader::loadRelative('core/' . FOLDER_THIRDPARTY . '/pear/Text/Diff/Renderer/inline.php');
		
		// Get diff:
		$diff = new Text_Diff(
			explode("\n", str_replace("\r", '', $this->getText())), 
			explode("\n", str_replace("\r", '', $text->getText()))
		);
		
		// Render diff as a text:
		$renderer = new Text_Diff_Renderer_inline();
		
		// Return the text:
		return $renderer->render($diff);
	}

    /**
     * Get HTML-attribute of this text
     *
     * To use this text safely as a HTML-attribute, we need to strip HTML-tags
     * and escape special characters
     *
     * @see M_FilterTextHtmlAttribute
     * @see M_FilterTextHtml
     * @see M_FilterTextValue
     * @return string
     */
    public function toHtmlAttribute() {
         // Create chain of filters, to remove HTML and escape special characters,
         // this way you can use it safely as a HTML-attribute
        $filter = new M_FilterTextHtmlAttribute(
            new M_FilterTextHtml(
                new M_FilterTextValue($this->getText())
            )
        );
        return $filter->apply();
    }
	
	/**
	 * This function find position of first occurrence of any $needles in a string $haystack.
	 * Return the position as an integer. If needles is not found, strposa() will return boolean FALSE. 
	 * 
	 * @param string $haystack
	 * @param array $needles
	 * @param int $offset
	 */
	private function _strrPosArray($haystack ,$needles=array(),$offset=0){
	    $chr = array();
	    foreach($needles as $needle){
	        if (strrpos($haystack,$needle,$offset) !== false) {
	          $chr[] = strrpos($haystack,$needle,$offset);
	       }
	    }
	    if(empty($chr)) return false;
	    return min($chr);
	}
	
	/**
	 * Get the text
	 * 
	 * This method will return the text that is contained in the
	 * M_Text object.
	 * 
	 * @access public
	 * @return string
	 */
	public function getText() {
		return $this->_text;
	}
	
	/**
	 * Export to string
	 * 
	 * This is a "magic method", called automatically by PHP when the
	 * M_Text object is being casted to a string. For example, an echo 
	 * call on an M_Text object will print the result of this function.
	 * 
	 * Example 1
	 * <code>
	 *    $text = new M_Text('Hello world!');
	 *    echo $text; // prints the result of __toString()
	 * </code>
	 * 
	 * Example 2
	 * <code>
	 *    $text = new M_Text('Hello world!');
	 *    $str  = (string) $text; // saves the result of __toString()
	 * </code>
	 * 
	 * @access public
	 * @uses M_Text::getText()
	 * @return string
	 */
	public function __toString() {
		return $this->getText();
	}
}
?>