<?php
/**
 * M_Request class
 * 
 * M_Request provides with tools to fetch the values of page request 
 * variables, to fetch the page request method, etc.
 *
 * @package Core
 */
class M_Request extends M_Object {
	/**
	 * Constant to address data type
	 * 
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_STRING = 'string';

	/**
	 * Constant to address data type
	 *
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_STRING_SANITIZED = 'string-sanitized';
	
	/**
	 * Constant to address data type
	 * 
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_INT = 'int';

	/**
	 * Constant to address data type
	 *
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_NUMERIC = 'numeric';

	/**
	 * Constant to address data type
	 *
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_NUMERIC_LOCALE_DEPENDENT = 'numeric-locale-dependent';
	
	/**
	 * Constant to address data type
	 * 
	 * This constant is used to address an expected data type. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const TYPE_ARRAY = 'array';
	
	/**
	 * Constant to address request method
	 * 
	 * This constant is used to address the GET method. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const GET = 'GET';
	
	/**
	 * Constant to address request method
	 * 
	 * This constant is used to address the POST method. See
	 * {@link M_Request::getVariable()} for more info on how such
	 * a constant should be used.
	 */
	const POST = 'POST';
	
	/**
	 * Link prefix
	 * 
	 * This property stores the prefix, which is prepended to the relative path
	 * of each link that is rendered by {@link M_Request::getLink()}.
	 * 
	 * @static
	 * @access private
	 * @var string
	 */
	private static $_linkPrefix = '';
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_Request
	 */
	public function __construct() {
	}
	
	/**
	 * Get request method
	 * 
	 * This method will return the request method with which the page
	 * has been requested.
	 * 
	 * @access public
	 * @static
	 * @return string
	 * 		The page request method (GET|POST|...)
	 */
	public static function getMethod() {
		return strtoupper($_SERVER['REQUEST_METHOD']);
	}
	
	/**
	 * Get request variables
	 * 
	 * This method returns the page request variables that have been
	 * sent to the current page request. If the page request method is
	 * POST, this method will return a copy of
	 * 
	 * <code>$_POST</code>
	 * 
	 * If the page request method is GET, this method will return a 
	 * copy of
	 * 
	 * <code>$_GET</code>
	 * 
	 * This method returns an exact copy; an associative array of
	 * which the keys are the names of the page request variables.
	 * 
	 * You can force this method to fetch the variables from a 
	 * specific requested method.
	 * 
	 * Example 1, fetch GET variables
	 * <code>
	 *    print_r(M_Request::getVariables(M_Request::GET));
	 * </code>
	 * 
	 * @access public
	 * @static
	 * @param string $method
	 * 		The method from which to fetch the page request
	 * 		variables.
	 * @return array
	 */
	public static function getVariables($method = NULL) {
		if($method) {
			$vars = $GLOBALS['_' . strtoupper($method)];
		} else {
			$vars = $GLOBALS['_' . self::getMethod()];
		}
		return $vars;
	}
	
	/**
	 * Get query string
	 * 
	 * This method will return the query string, parsed into the
	 * URL-encoded format.
	 * 
	 * Example 1
	 * <code>
	 *    $request = M_Request::getInstance();
	 *    $request->setVariable('author', 'ben');
	 *    echo $request->getQueryString();
	 * </code>
	 * 
	 * Example 1 would output the following:
	 * <code>author=ben</code>
	 * 
	 * NOTE:
	 * To separate the query variables, this method will use "&"
	 * 
	 * @access public
	 * @return string
	 */
	public function getQueryString() {
		return http_build_query(self::getVariables(), '', '&');
	}
	
	/**
	 * Get request variable
	 * 
	 * Get a selected variable from the page request variables.
	 * For example, consider a page that has been requested via the
	 * following URL:
	 * 
	 * <code>
	 *    http://www.multimedium.be/?page=news
	 * </code>
	 * 
	 * To fetch the value of the request variable "page", you could
	 * use the following code:
	 * 
	 * Example 1
	 * <code>
	 *    echo M_Request::getVariable('page');
	 * </code>
	 * 
	 * You can specify the default value that should be returned by
	 * this method, if no value has been found for the requested var.
	 * 
	 * Also, you can specify the expected data type. If a value has
	 * been found for the requested variable, but its type does not
	 * match the expected one, this method will also return the default
	 * value. If the expected data type is not given, it is ignored.
	 * 
	 * Last, but not least, you can also specify the request method
	 * from which you expect the variable's value. If the variable has
	 * not been find in the specified request method, this method will
	 * again return the default value. If the method is not given, it
	 * is ignored.
	 * 
	 * In Example 1, we know that the request variable "page" should 
	 * be a string, provided via the GET method. So we could rewrite
	 * Example 1 as following, in order to obtain more secure code:
	 * 
	 * Example 2, secure rewrite of Example 1
	 * (this example sets the default value to "home")
	 * <code>
	 *    echo M_Request::getVariable('page', 'home', M_Request::TYPE_STRING, M_Request::GET);
	 * </code>
	 * 
	 * By fetching variables as illustrated in Example 2, we write
	 * more secure code. We force the variable to a given type (a string
	 * in this case), and we only accept a value if it has been
	 * provided in the expected request method.
	 * 
	 * @access public
	 * @static
	 * @param string $name
	 * 		The name of the request variable
	 * @param mixed $defaultValue
	 * 		The default value of the variable
	 * @param string $cast
	 * 		The expected data type
	 * @param string $method
	 * 		The requested method
	 * @return mixed
	 */
	public static function getVariable($name, $defaultValue = FALSE, $cast = NULL, $method = NULL) {
		$vars = self::getVariables($method);
		if(isset($vars[$name])) {
			switch($cast) {
				case self::TYPE_STRING:
					return is_string($vars[$name]) ? (string) $vars[$name] : $defaultValue;

				case self::TYPE_STRING_SANITIZED:
					if(!is_string($vars[$name])) {
						return $defaultValue;
					}
					$filter = new M_FilterTextXss(new M_FilterTextValue($vars[$name]));
					return $filter->apply();
				
				case self::TYPE_INT:
					return is_numeric($vars[$name]) ? (int) $vars[$name] : $defaultValue;

				case self::TYPE_NUMERIC:
					return is_numeric($vars[$name]) ? $vars[$name] : $defaultValue;

				case self::TYPE_NUMERIC_LOCALE_DEPENDENT:
					try {
						return M_Number::constructWithString($vars[$name])->getNumber();
					} catch(M_Exception $e) {
						return $defaultValue;
					}
				
				case self::TYPE_ARRAY:
					return is_array($vars[$name]) ? (array) $vars[$name] : $defaultValue;
				
				default:
					return $vars[$name];
			}
		} else {
			return $defaultValue;
		}
	}
	
	/**
	 * Set variable
	 * 
	 * @static
	 * @access public
	 * @param string $name
	 * 		The name of the variable
	 * @param mixed $value
	 * @return void
	 */
	public static function setVariable($name, $value) {
		$GLOBALS['_' . self::getMethod()][$name] = $value;
	}
	
	/**
	 * Get protocol
	 * 
	 * This method will return the protocol via which the page has
	 * been requested. Possible return values:
	 * 
	 * <code>
	 *    http
	 *    https
	 * </code>
	 * 
	 * @access public
	 * @static
	 * @return string
	 */
	public static function getProtocol() {
		// If the protocol is https
		if(self::isHttps()) {
			// Then, return https as protocol
			return 'https';
		}
		
		// Check if the server variables indicate a protocol:
		if(isset($_SERVER['SERVER_PROTOCOL'])) {
			// The server protocol string is a string like HTTP/1.0. So, we explode
			// the protocol string by the slash separator:
			$temp = explode('/', $_SERVER['SERVER_PROTOCOL']);
			
			// Get the first element, and convert to lowercased string:
			return strtolower($temp[0]);
		}
		// If the server does not define the protocol:
		else {
			// Then, we return 'http' by default
			return 'http';
		}
	}
	
	/**
	 * Get the host of the request
	 * 
	 * This method will return the name of the host via which the
	 * application has been requested.
	 * 
	 * Example 1. Assuming the application is requested at the following
	 * address: http://localhost/myApp
	 * 
	 * <code>
	 *    echo M_Request::getHost();
	 * </code>
	 * 
	 * Example 1 will produce the following output:
	 * 
	 * <code>
	 *    localhost
	 * </code>
	 * 
	 * NOTE:
	 * If the host could not have been identified, this method will
	 * return FALSE.
	 * 
	 * @access public
	 * @static
	 * @return string|FALSE
	 */
	public static function getHost() {
		if(isset($_SERVER['HTTP_HOST'])) {
			return $_SERVER['HTTP_HOST'];
		}
		return FALSE;
	}
	
	/**
	 * Get Host Address (string)
	 * 
	 * This method will return the full address to the domain via which 
	 * the application has been requested.
	 * 
	 * Example 1. Assuming the application is requested at the following
	 * address: http://localhost/myApp
	 * 
	 * <code>
	 *    echo M_Request::getHostAddress();
	 * </code>
	 * 
	 * Example 1 will produce the following output:
	 * 
	 * <code>
	 *    http://localhost
	 * </code>
	 * 
	 * @see M_Request::getHost()
	 * @access public
	 * @static
	 * @return string
	 */
	public static function getHostAddress() {
		return self::getProtocol() . '://' . self::getHost();
	}
	
	/**
	 * Get Request URI (string)
	 * 
	 * This method will return the Request URI. This method will check
	 * the server environment variables, in order to find a value for
	 * the Request URI (different for apache server, iis, ...).
	 * 
	 * @access public
	 * @static
	 * @return string
	 */
	public static function getUriString() {
		if(!isset($_SERVER['REQUEST_URI'])) {
			$uri = $_SERVER['SCRIPT_NAME'];
			if(isset($_SERVER['QUERY_STRING'])) {
				$uri .= '?' . $_SERVER['QUERY_STRING'];
			} elseif(isset($_SERVER['argv'])) {
				$uri .= '?' . $_SERVER['argv'];
			}
			return $uri;
		} else {
			return $_SERVER['REQUEST_URI'];
		}
	}
	
	/**
	 * Get Request URI (object)
	 * 
	 * This method will return the Request URI, in the form of an
	 * {@link M_Uri} object. This object will include all properties
	 * of the Request URI:
	 * 
	 * - host
	 * - path
	 * - variables
	 * - etc.
	 * 
	 * @access public
	 * @static
	 * @return M_Uri
	 */
	public static function getUri() {
		return new M_Uri(self::getProtocol() . '://' . self::getHost() . self::getUriString());
	}
	
	/**
	 * Get (absolute) link
	 * 
	 * This method will create an absolute link out of a relative path.
	 * To do so, it uses the {@link M_Uri} object that is returned by
	 * {@link M_Request::getUri()}, and changes the path to the 
	 * relative one.
	 * 
	 * Note that this method will prepend the application's base href,
	 * as returned by {@link M_Server::getBaseHref()}, to the given
	 * path.
	 * 
	 * Also, this method will prepend the link prefix (if any), that may have
	 * been set previously with {@link M_Request::setLinkPrefix()}.
	 * 
	 * Also, you can provide the query variables for the new link 
	 * (second argument to this method). For more info on that, check
	 * {@link M_Uri::setQueryVariables()}.
	 * 
	 * @access public
	 * @static
	 * @see M_Request::getLinkWithoutPrefix()
	 * @param string $path
	 * 		The relative path for the link
	 * @param string $variables
	 * 		The query variables for the new link (associative array)
	 * @return string
	 */
	public static function getLink($path = NULL, array $variables = array()) {
		return self::getLinkWithoutPrefix(
			empty(self::$_linkPrefix) ? $path : self::$_linkPrefix . '/' . M_Helper::ltrimCharlist((string) $path, '/'),
			$variables
		);
	}
	
	/**
	 * Get (absolute) link, without prefix
	 * 
	 * @access public
	 * @static
	 * @see M_Request::getLink()
	 * @param string $path
	 * 		The relative path for the link
	 * @param string $variables
	 * 		The query variables for the new link (associative array)
	 * @return string
	 */
	public static function getLinkWithoutPrefix($path = NULL, array $variables = array()) {
		$uri = self::getUri();
		$uri->setPath(M_Server::getBaseHref() . $path);
		$uri->setQueryVariables($variables, TRUE);
		return $uri->getUri();
	}
	
	/**
	 * Set link prefix
	 * 
	 * Can be used to set the link prefix, which should be added to all absolute 
	 * links that are rendered by {@link M_Request::getLink()}.
	 * 
	 * Example 1. Assuming the application is requested at the following
	 * address: http://localhost/myApp
	 * 
	 * <code>
	 *    M_Request::setLinkPrefix('nl');
	 *    echo M_Request::getLink('myPage');
	 * </code>
	 * 
	 * Example 1 will produce the following output:
	 * 
	 * <code>
	 *    http://localhost/myApp/nl/myPage
	 * </code>
	 * 
	 * @access public
	 * @param string $prefix
	 * 		The link prefix, to be added to all links
	 * @return void
	 */
	public static function setLinkPrefix($prefix) {
		self::$_linkPrefix = M_Helper::trimCharlist((string) $prefix, '/');
	}
	
	/**
	 * Get link prefix
	 * 
	 * Link prefix is added to all absolute links
	 * 
	 * @see setLinkPrefix()
	 * @return string
	 */
	public static function getLinkPrefix() {
		return self::$_linkPrefix;
	}
	
	/**
	 * Is https?
	 * 
	 * This method will check if the request has been made over a
	 * SSL (Secure Socket Layer) connection. This method will return
	 * TRUE if the request has been made over SSL, FALSE if not.
	 * 
	 * @access public
	 * @static
	 * @return boolean
	 */
	public static function isHttps() {
		// "HTTPS", PHP Predefined variable in SERVER
		// Set to a non-empty value if the script was queried through 
		// the HTTPS protocol. Note that when using ISAPI with IIS, the 
		// value will be "off" if the request was not made through the 
		// HTTPS protocol. 
		return (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off');
	}
	
	/**
	 * Is POST?
	 * 
	 * This method can be used to check whether or not the page has
	 * been requested via the POST method.
	 * 
	 * @static
	 * @access public
	 * @return boolean
	 */
	public static function isPost() {
		return (self::getMethod() == 'POST');
	}
	
	/**
	 * Is GET?
	 * 
	 * This method can be used to check whether or not the page has
	 * been requested via the GET method.
	 * 
	 * @static
	 * @access public
	 * @return boolean
	 */
	public static function isGet() {
		return (self::getMethod() == 'GET');
	}
	
	/**
	 * Parse the variables out of a path
	 * 
	 * This method will return an array with values for the variables
	 * in the given path (starting at index number 0). If the URL does
	 * not match the provided template path, this method will return
	 * FALSE.
	 * 
	 * Example 1
	 * <code>
	 *    $vars = M_Request::getPathVariables('news/my-news-article', 'news/%');
	 *    print_r($vars);
	 * </code>
	 * 
	 * Example 1 would output the following array:
	 * 
	 * <code>
	 *    Array (
	 *       0 => 'my-news-article'
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @static
	 * @param string $path
	 * 		The path to extract the variables from
	 * @param string $template
	 * 		The pattern that describes the path
	 * @return array|FALSE
	 */
	public static function getPathVariables($path, $template) {
		$template = str_replace('%', '(.+)', preg_quote($template, '/'));
		$match = array();
		if(preg_match('/'. $template .'/i', $path, $match)) {
			// We do not include the URL in the result:
			array_shift($match);
			
			// Return the variables
			return $match;
		}
		return FALSE;
	}
}
?>