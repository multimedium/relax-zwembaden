<?php
/**
 * M_OneTimePasswordAuthAttempt
 *
 * M_OneTimePasswordAuthAttempt implements the {@link MI_AuthAttempt} interface,
 * and can therefore be used as authentication attempts in authentication schemes
 * that use the one-time password system.
 *
 * @package Core
 */
class M_OneTimePasswordAuthAttempt extends M_Object implements MI_AuthAttempt {
	
}