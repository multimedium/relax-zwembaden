<?php
/**
 * M_OneTimePasswordException
 *
 * Used to throw exceptions, by the core's One-Time Password API
 *
 * @package Core
 */
class M_OneTimePasswordException extends M_Exception {
}