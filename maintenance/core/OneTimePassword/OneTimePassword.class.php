<?php
/**
 * M_OneTimePassword
 *
 * @package Core
 */
class M_OneTimePassword extends M_OneTimePasswordSystem {

	/* -- PROPERTIES -- */

	/**
	 * One-Time Password, in bits
	 *
	 * This property stores the 64-bit string representation of the one-time
	 * password.
	 *
	 * @access private
	 * @var string
	 */
	private $_bits;

	/**
	 * One-Time Password, in hexadecimal format
	 *
	 * This property stores the hexadecimal representation of the one-time
	 * password.
	 *
	 * @access private
	 * @var string
	 */
	private $_hex;

	/**
	 * One-Time Password, in words
	 *
	 * This property stores the .
	 *
	 * @access private
	 * @var string
	 */
	private $_hex;
}