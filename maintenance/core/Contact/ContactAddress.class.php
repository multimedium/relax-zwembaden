<?php
class M_ContactAddress {
	const TYPE_WORK = 'work';
	const TYPE_PRIVATE = 'private';

	/**
	 * Holds the complete street address (name + number)
	 *
	 * @var string Street Address
	 */
	protected $_streetAddress;
	
	/**
	 * Holds the street name
	 * 
	 * @var string Streetname
	 */
	protected $_streetName;

	/**
	 * Holds the street number
	 *
	 * @var string
	 */
	protected $_streetNumber;

	/**
	 * Holds the city
	 *
	 * @var string
	 */
	protected $_city;

	/**
	 * Holds the postal code
	 * @var string
	 */
	protected $_postalCode;

	/**
	 * Holds the region
	 *
	 * @var string
	 */
	protected $_region;

	/**
	 * Holds the country
	 * @var string
	 */
	protected $_country;

	/**
	 * Set the type of address
	 * 
	 * @var Type of address (work/home)
	 * @see M_ContactAddress::TYPE_WORK
	 * @see M_ContactAddress::TYPE_PRIVATE
	 */
	protected $_type;

	/**
	 * Latitude for this address
	 * 
	 * @var string
	 */
	protected $_latitude;

	/**
	 * Longitude for this address
	 *
	 * @var string
	 */
	protected $_longitude;

	/**
	 * Create a new address
	 * 
	 * @param string $streetAddress
	 * @param string $city
	 * @param string $postalCode
	 * @param string $region
	 * @param string $country
	 */
	public function __construct($streetAddress = NULL, $city = NULL, $postalCode = NULL, $region = NULL, $country = NULL) {
		if($streetAddress) {
			$this->setStreetAddress($streetAddress);
		}
		if($city) {
			$this->setCity($city);
		}
		if($postalCode) {
			$this->setPostalCode($postalCode);
		}
		if($region) {
			$this->setRegion($region);
		}
		if($country) {
			$this->setCountry($country);
		}
	}

	/* -- Setters -- */

	/**
	 * Set the street address
	 *
	 * @param string $address
	 * @return M_ContactAddress
	 */
	public function setStreetAddress($address) {
		$this->_streetAddress = (string) $address;
		return $this;
	}

	/**
	 * Set the street name
	 *
	 * @param string $streetName
	 * @return M_ContactAddress
	 */
	public function setStreetName($streetName) {
		$this->_streetName = (string) $streetName;
		return $this;
	}
	/**
	 * Set the street number
	 *
	 * @param string $streetNumber
	 * @return M_ContactAddress
	 */
	public function setStreetNumber($streetNumber) {
		$this->_streetNumber = (string) $streetNumber;
		return $this;
	}
	
	/**
	 * Set the city
	 *
	 * @param string $city
	 * @return M_ContactAddress
	 */
	public function setCity($city) {
		$this->_city = (string) $city;
		return $this;
	}
	
	/**
	 * Set the postalcode
	 *
	 * @param string $postalCode
	 * @return M_ContactAddress
	 */
	public function setPostalCode($postalCode) {
		$this->_postalCode = (string) $postalCode;
		return $this;
	}
	
	/**
	 * Set the region
	 *
	 * @param string $region
	 * @return M_ContactAddress
	 */
	public function setRegion($region) {
		$this->_region = (string) $region;
		return $this;
	}
	
	/**
	 * Set the country
	 *
	 * @param string $country
	 * @return M_ContactAddress
	 */
	public function setCountry($country) {
		$this->_country = (string) $country;
		return $this;
	}
	
	/**
	 * Set the addresss-type
	 *
	 * @param string $type
	 * @see M_ContactAddress::TYPE_PRIVATE
	 * @see M_ContactAddress::TYPE_WORK
	 * @return M_ContactAddress
	 */
	public function setType($type) {
		$this->_type = (string) $type;
		return $this;
	}

	/**
	 * Set Latitude
	 *
	 * @param string $arg
	 * @return M_ContactAddress
	 */
	public function setLatitude($arg) {
		$this->_latitude = $arg;
		return $this;
	}
	
	/**
	 * Set Longitude
	 *
	 * @param string $arg
	 * @return M_ContactAddress
	 */
	public function setLongitude($arg) {
		$this->_longitude = $arg;
		return $this;
	}

	/* -- Getters -- */

	/**
	 * Get Latitude
	 *
	 * @return string
	 */
	public function getLatitude() {
		return $this->_latitude;
	}

	/**
	 * Get Longitude
	 *
	 * @return string
	 */
	public function getLongitude() {
		return $this->_longitude;
	}

	/**
	 * Get the streetAddress
	 * 
	 * In some cases the straatname and number are stored in one field:
	 * the street-address. In other cases these data is stored separatedly in
	 * 2 fields. When this is the case we return the combination of 
	 * {@link M_ContactAddress::getStreetName()} and 
	 * {@link M_ContactAddress::getStreetNumber()}
	 */
	public function getStreetAddress() {
		$streetAddress = array();
		//a specific street address has been set
		if ($this->_streetAddress) $streetAddress[] = $this->_streetAddress;
		//return combination of streetname and streetnumber
		else {
			if ($this->getStreetName()) $streetAddress[] = $this->getStreetName();
			if ($this->getStreetNumber()) $streetAddress[] = $this->getStreetNumber();
		}
		
		return implode(' ', $streetAddress);
	}
	
	/**
	 * Get streetname
	 * 
	 * @return string
	 */
	public function getStreetName() {
		return $this->_streetName;
	}
	
	/**
	 * Get streetnumber
	 * 
	 * @return string
	 */
	public function getStreetNumber() {
		return $this->_streetNumber;
	}
	
	/**
	 * Get city
	 * 
	 * @return string
	 */
	public function getCity() {
		return $this->_city;
	}
	
	/**
	 * Get postalcode
	 * 
	 * @return string
	 */
	public function getPostalCode() {
		return $this->_postalCode;
	}
	
	/**
	 * Get region
	 * 
	 * @return string
	 */
	public function getRegion() {
		return $this->_region;
	}
	
	/**
	 * Get country
	 * 
	 * @return string
	 */
	public function getCountry() {
		return $this->_country;
	}
	
	/**
	 * Get type
	 * 
	 * @return string
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Get address string
	 * 
	 * @todo: Should the string be localized?
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// Output
		$t = array();
		
		// If a street address (including house number) is known
		if($this->getStreetAddress()) {
			// Then, we add the street address to the output:
			$t[] = $this->getStreetAddress();
		}
		
		// If a city name is known
		if($this->getCity()) {
			// And also a postal code has been defined:
			if($this->getPostalCode()) {
				// Then, we add the postal code with the city name
				$t[] = $this->getPostalCode() . ' ' . $this->getCity();
			}
			// If the postal code has not been defined
			else {
				// Then, we only add the city name
				$t[] = $this->getCity();
			}
		}
		
		// If a region is known:
		if($this->getRegion()) {
			// Then, we add the region string to the output
			$t[] = $this->getRegion();
		}
		
		// If a country is known:
		if($this->getCountry()) {
			// Then, we add the country string to the output
			$t[] = $this->getCountry();
		}
		
		// Return the address string
		return implode(', ', $t);
	}

	/**
	 * Get this address as string
	 *
	 * @see M_ContactAddress::toString();
	 * @author Ben Brughmans
	 */
	public function __toString() {
		return $this->toString();
	}
}