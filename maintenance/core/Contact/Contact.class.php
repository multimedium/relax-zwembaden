<?php
class M_Contact {
	const TITLE_MR = 'Mr';
	const TITLE_MS = 'Ms';
	const TITLE_DOCTOR = 'Doctor';
	const TITLE_ENGINEER = 'Engineer';
	
	protected $_title;
	protected $_name;
	protected $_firstName;
	protected $_middleName;
	protected $_surNames;
	protected $_organisation;
	protected $_nickname;
	protected $_vatNumber;
	
	// instances of M_ContactAddress
	protected $_addresses = array();
	
	// instances of M_ContactEmail
	protected $_emails = array();
	
	// M_Uri object
	protected $_websites = array();
	
	// instances of M_ContactPhoneNumber
	protected $_phoneNumbers = array();
	
	// instance of M_Image
	protected $_picture;
	
	public function __construct($name = NULL) {
		if($name) {
			$this->_name = (string) $name;
		}
	}
	
	/**
	 * Set Contact Title
	 * 
	 * @see M_Contact::TITLE_MR
	 * @see M_Contact::TITLE_MS
	 * @see M_Contact::TITLE_DOCTOR
	 * @see M_Contact::TITLE_ENGINEER
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	public function setName($name) {
		$this->_name = (string) $name;
	}
	
	public function setFirstname($name) {
		$this->_firstName = (string) $name;
	}
	
	public function setMiddleName($name) {
		$this->_middleName = (string) $name;
	}
	
	public function setSurnames($name) {
		$this->_surNames = (string) $name;
	}
	
	public function setNickname($name) {
		$this->_nickname = (string) $name;
	}
	
	public function setOrganisation($organisation) {
		$this->_organisation = (string) $organisation;
	}
	
	public function setVatNumber($vat) {
		$this->_vatNumber = (string) $vat;
	}
	
	public function addAddress(M_ContactAddress $address) {
		$this->_addresses[] = $address;
	}
	
	public function addEmail(M_ContactEmail $email) {
		$this->_emails[] = $email;
	}
	
	public function addWebsite(M_Uri $uri) {
		$this->_websites[] = $uri;
	}
	
	public function addPhoneNumber(M_ContactPhoneNumber $number) {
		$this->_phoneNumbers[] = $number;
	}
	
	public function setPicture(M_Image $image) {
		$this->_picture = $image;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function getName() {
		if(! $this->_name) {
			if($this->_firstName || $this->_surNames) {
				return $this->_firstName . ' ' . $this->_surNames;
			}
		}
		return $this->_name;
	}
	
	public function getFirstname() {
		return $this->_firstName;
	}
	
	public function getMiddlename() {
		return $this->_middleName;
	}
	
	public function getSurnames() {
		return $this->_surNames;
	}
	
	public function getNickname() {
		return $this->_nickname;
	}
	
	public function getOrganisation() {
		return $this->_organisation;
	}
	
	public function getVatNumber() {
		return $this->_vatNumber;
	}
	
	/**
	 * Get addresses
	 * 
	 * Will provide with an ArrayIterator, which has been populated with the
	 * {@link M_ContactAddress} instances that have been provided previously to
	 * {@link M_Contact::addAddress()}.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getAddresses() {
		return new ArrayIterator($this->_addresses);
	}
	
	/**
	 * Get main address
	 * 
	 * Will provide with the first address that has been added to the contact
	 * person/entity, via {@link M_Contact::addAddress()}
	 *
	 * @access public
	 * @return M_ContactAddress
	 */
	public function getAddress() {
		if(isset($this->_addresses[0])) {
			return $this->_addresses[0];
		} else {
			return null;
		}
	}
	
	/**
	 * Get address, by type
	 * 
	 * Will look up an address of the contact person/entity, given a type. 
	 * For an example, check out {@link M_Contact::getEmailByType()}.
	 * 
	 * @see M_Contact::getEmailByType()
	 * @see M_Contact::getPhoneNumberByType()
	 * @access public
	 * @param string $type
	 * 		The type, with which to look up the address
	 * @return M_ContactAddres
	 */
	public function getAddressByType($type) {
		foreach($this->_addresses as $n) {
			if($n->getType() == $type) {
				return $n;
			}
		}
		return NULL;
	}
	
	/**
	 * Get email addresses
	 * 
	 * Will provide with an ArrayIterator, which has been populated with the
	 * {@link M_ContactEmail} instances that have been provided previously to
	 * {@link M_Contact::addEmail()}.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getEmails() {
		return new ArrayIterator($this->_emails);
	}
	
	/**
	 * Get main email address
	 * 
	 * Will provide with the first email address that has been added to the contact
	 * person/entity, via {@link M_Contact::addEmail()}
	 *
	 * @access public
	 * @return M_ContactEmail
	 */
	public function getEmail() {
		if(count($this->_emails) > 0) {
			return $this->_emails[0];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get email, by type
	 * 
	 * Will look up an email address of the contact person/entity, given a type. 
	 * For example, in order to fetch the contact person/entity's work email:
	 * 
	 * <code>
	 *    $contact = new M_Contact('Tom Bauwens');
	 *    $contact->addEmail(new M_ContactEmail('tom@multimedium.be'), M_ContactEmail::TYPE_WORK);
	 *    $contact->addEmail(new M_ContactEmail('info@tombauwens.be'), M_ContactEmail::TYPE_PRIVATE);
	 *    
	 *    // Will output "tom@multimedium.be"
	 *    echo $contact->getEmailByType(M_ContactEmail::TYPE_WORK)->getEmail();
	 * </code>
	 * 
	 * NOTE:
	 * Will return NULL, if the email could not have been found!
	 * 
	 * @see M_Contact::getPhoneNumberByType()
	 * @see M_Contact::getAddressByType()
	 * @access public
	 * @param string $type
	 * 		The type, with which to look up the email address
	 * @return M_ContactEmail
	 */
	public function getEmailByType($type) {
		foreach($this->_emails as $email) {
			if($email->getType() == $type) {
				return $email;
			}
		}
		return NULL;
	}
	
	/**
	 * Get websites
	 * 
	 * Will provide with an ArrayIterator, which has been populated with the
	 * {@link M_Uri} instances that have been provided previously to
	 * {@link M_Contact::addWebsite()}.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getWebsites() {
		return new ArrayIterator($this->_websites);
	}
	
	/**
	 * Get main website
	 * 
	 * Will provide with the first website address that has been added to the 
	 * contact person/entity, via {@link M_Contact::addWebsite()}
	 *
	 * @access public
	 * @return M_Uri
	 */
	public function getWebsite() {
		if(count($this->_websites) > 0) {
			return $this->_websites[0];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get phone numbers
	 * 
	 * Will provide with an ArrayIterator, which has been populated with the
	 * {@link M_ContactPhoneNumber} instances that have been provided previously 
	 * to {@link M_Contact::addPhoneNumber()}.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getPhoneNumbers() {
		return new ArrayIterator($this->_phoneNumbers);
	}
	
	/**
	 * Get main phone number
	 * 
	 * Will provide with the first phone number that has been added to the 
	 * contact person/entity, via {@link M_Contact::addPhoneNumber()}
	 *
	 * @access public
	 * @return M_ContactPhoneNumber
	 */
	public function getPhoneNumber() {
		if(count($this->_phoneNumbers) > 0) {
			return $this->_phoneNumbers[0];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get phone number, by type
	 * 
	 * Will look up a phone number of the contact person/entity, given a type. 
	 * For an example, check out {@link M_Contact::getEmailByType()}.
	 * 
	 * @see M_Contact::getEmailByType()
	 * @see M_Contact::getAddressByType()
	 * @access public
	 * @param string $type
	 * 		The type, with which to look up the phone number
	 * @return M_ContactPhoneNumber
	 */
	public function getPhoneNumberByType($type) {
		foreach($this->_phoneNumbers as $n) {
			if($n->getType() == $type) {
				return $n;
			}
		}
		return NULL;
	}
	
	/**
	 * Get picture
	 *
	 * @access public
	 * @return M_Image
	 */
	public function getPicture() {
		return $this->_picture;
	}
	
	/**
	 * Get vcard
	 * 
	 * @access public
	 * @return M_ContactVcard
	 */
	public function getVcard() {
		return new M_ContactVcard($this);
	}
	
	/**
	 * Get Microformat hCard
	 * 
	 * @access public
	 * @return M_ViewMicroformatHcard
	 */
	public function getMicroformatHcard() {
		$v = new M_ViewMicroformatHcard();
		$v->setContact($this);
		return $v;
	}
}