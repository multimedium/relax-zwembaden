<?php
/**
 * M class
 * 
 * The M class is the "System class". This class is used to store
 * and retrieve system variables (for example: configuration
 * values, system preferences, etc.).
 * 
 * @package Core
 */
class M extends M_Object {
	/**
	 * Variable repository
	 * 
	 * This property holds the object that stores the variables.
	 * The repository is an object that implements the {@link MI_Config}
	 * interface.
	 * 
	 * @static
	 * @access private
	 * @var MI_Config
	 */
	private static $_repository = NULL;
	
	/**
	 * Private Constructor
	 * 
	 * The M class cannot be instantiated.
	 *
	 * @access private
	 * @return M
	 */
	private function __construct() {
	}
	
	/**
	 * Set the variable repository
	 * 
	 * This method will set the repository where variables are
	 * stored and fetched from.
	 * 
	 * @access public
	 * @param IConfig $repository
	 * 		The variable repository
	 * @return void
	 */
	public static function setVariableRepository(MI_Config $repository) {
		self::$_repository = $repository;
	}
	
	/**
	 * Get a variable
	 * 
	 * This method is used to get the value of a variable in 
	 * the repository.
	 * 
	 * The repository is a Config object, and may therefore contain
	 * a hierarchy of variables. You can request the value of a
	 * variable directly with this method, even if the variable has
	 * been nested inside another one. To do so, check out Example 1.
	 * 
	 * Example 1, get nested variable:
	 * <code>
	 *    echo M::getVariable('files/folder');
	 * </code>
	 * 
	 * In example 1, the M class will internally route this request 
	 * to the variables as follows:
	 * 
	 * <code>
	 *    // self::$_repository is the internal IConfig object
	 *    self::$_repository->variables->files->folder
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * Note that you can also call this method statically. 
	 * Internally, this method will instantiate the singleton 
	 * object.
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the variable
	 * @param mixed $defaultValue
	 * 		This method will return this parameter, if no value 
	 * 		could have been found for the requested variable.
	 * @return mixed
	 */
	public static function getVariable($name, $defaultValue = FALSE) {
		if(isset(self::$_repository->variables)) {
			$path = explode('/', $name);
			$root = self::$_repository->variables;
			foreach($path as $element) {
				if(isset($root->$element)) {
					$root = $root->$element;
				} else {
					return $defaultValue;
				}
			}
			return $root;
		}
		return $defaultValue;
	}
	
	/**
	 * Set a variable
	 * 
	 * This will set the value of a variable in the repository
	 * 
	 * The repository is a Config object, and may therefore contain
	 * a hierarchy of variables. You can set the value of a
	 * variable directly with this method, even if the variable has
	 * been nested inside another one. To do so, check out Example 1
	 * at {@link M::getVariable()}.
	 * 
	 * IMPORTANT NOTE:
	 * Setting a new value of a variable in the repository will 
	 * not cause the IConfig object to fire the save() method
	 * right away. The M class will delay the actual saving of 
	 * the repository to the end of the page request. However, the
	 * changes to the variables are reflected in the M singleton
	 * object meanwhile. Read {@link M::__destruct()} to learn
	 * more
	 * 
	 * @access public
	 * @return void
	 */
	public static function setVariable($name, $value) {
		if(self::$_repository != NULL) {
			$path = explode('/', $name);
			for($first = count($path) - 1, $i = $first; $i >= 0; $i --) {
				switch($i) {
					case $first:
						$temp = array($path[$i] => $value);
						break;
					case 0:
						self::$_repository->variables->merge(
							new Config(array($path[$i] => $temp)),
							Config::OVERWRITE
						);
						break;
					default:
						$temp = array($path[$i] => $temp);
						break;
				}
			}
		}
	}
	
	/**
	 * Set variables
	 * 
	 * This method is used to set the value of various variables
	 * at the same time. This method expects an associative array,
	 * and will use the keys of that array as variable names.
	 * For each variable, this method will set the value by using
	 * {@link M::setVariable()}.
	 * 
	 * NOTE:
	 * This method expects an associative array, but any object
	 * that implements the Iterator interface will do.
	 * 
	 * @access public
	 * @param array|Iterator $vars
	 * @return void
	 */
	public static function setVariables($vars) {
		if(self::$_repository != NULL) {
			foreach($vars as $name => $value) {
				self::setVariable($name, $value);
			}
		}
	}
	
	/**
	 * Get installed version
	 * 
	 * This method will return the version number of the 
	 * installed system.
	 * 
	 * IMPORTANT NOTE:
	 * If the installed version could not have been found in the
	 * variables repository, this method will return "0.1.0" by
	 * default.
	 * 
	 * @access public
	 * @return string
	 */
	public static function getInstalledVersion() {
		return self::getVariable('installedVersion', '0.1.2');
	}
}
?>