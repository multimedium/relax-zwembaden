<?php 
class M_Debug extends M_Object {

	/**
	 * Stores if debug-mode is on/off
	 * 
	 * @var bool
	 */
	private static $_debugMode = false;

	/**
	 * Holds the recipient to which mails will be sent if
	 * {@link M_Debug::getDebugMode()} is enabled
	 * 
	 * @var string
	 */
	private static $_debugEmailRecipient;

	/**
	 * Enable/disable debug-mode
	 *
	 * @param bool $arg
	 */
	public static function setDebugMode($arg) {
		self::$_debugMode = (bool)$arg;
	}

	/**
	 * Check if debug mode is enabled/disabled
	 * 
	 * @return bool
	 */
	public static function getDebugMode() {
		return self::$_debugMode;
	}

	/**
	 * Enable maintenance mode
	 *
	 * In maintenance mode all traffic will be redirected to maintenance.php,
	 * and a header 302 will be sent (temporary redirect).
	 *
	 * You will call this method before dispatch, all traffic will then be
	 * redirected.
	 *
	 * @see http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
	 * @param string $excludeIp
	 *		Check the ip and do not redirect if the users' ip matches the $excludeIp
	 * @return void
	 */
	public static function maintenanceMode($excludeIp = null) {
		//do not redirect if users' ip matches the exclude-ip
		if(!is_null($excludeIp) && $excludeIp == M_Client::getIp()) {
			//display a message to remind the developer maintenance mode is enabled
			$msg = '<div style="padding:10px;background-color:#dd0000;margin-bottom:20px;color:#fff;text-align:center;">';
			$msg .=		'Maintenance mode enabled';
			$msg .= '</div>';

			echo $msg;
			return false;
		}

		M_Header::sendHttpStatus(302);
		M_Header::redirect(M_Request::getLink('maintenance'));
	}

	/**
	 * Set the recipient to which all mail will be redirected if
	 * {@link M_Debug::getDebugMode()} is enabled
	 *
	 * @param string
	 */
	public static function setDebugEmailRecipient($arg) {
		self::$_debugEmailRecipient = (string)$arg;
	}

	/**
	 * Set the recipient to which all mail will be redirected if
	 * {@link M_Debug::getDebugMode()} is enabled
	 *
	 * Note: if empty (or invalid) the mail won't be redirected automaticcally
	 * 
	 * @return string
	 */
	public static function getDebugEmailRecipient() {
		return self::$_debugEmailRecipient;
	}
	
	/**
	 * Output array to screen
	 *
	 * @param array $array
	 * @return void
	 */
	public static function printArray($array) {
		echo "<pre>";
		print_r((array)$array);
		echo"</pre>";
	}
	
	/**
	 * Alias for {@link M_Debug::printArray()}
	 * 
	 * @access public
	 * @param array $array
	 * @return void
	 */
	public static function printr($array) {
		self::printArray($array);
	}
	
	/**
	 * Output var_dump(); to screen
	 *
	 * @param mixed $mixed
	 * @param str $message
	 */
	public static function printVarDump($mixed, $message = null) {
		echo "<pre>";
		if ($message) echo $message.'<hr>';
		var_dump($mixed);
		echo"</pre>";
	}
	
	/**
	 * Alias for {@link M_Debug::printVarDump()}
	 * 
	 * @access public
	 * @param mixed $mixed
	 * @param string $message
	 * @return void
	 */
	public static function dump($mixed, $message = null) {
		self::printVarDump($mixed, $message);
	}

	/* -- Query logging -- */

	/**
	 * Get a log of all queries which have been run during runtime
	 *
	 * Each query is stored in an array containing the sql statement,
	 * execution time and backtrace
	 *
	 * @author Ben Brughmans
	 * @return M_ArrayIterator
	 */
	public static function getQueryLog() {
		return M_Registry::getInstance()->get(
			'queries',
			new M_ArrayIterator()
		);
	}

	/**
	 * Update the query log with a new set of queries
	 * 
	 * @param M_ArrayIterator $log
	 */
	protected static function _setQueryLog(M_ArrayIterator $log) {
		M_Registry::getInstance()->__set('queries', $log);
	}

	/**
	 * Add a query to the registry-log
	 *
	 * By storing queries (and optionally their execution time and backtrace)
	 * in the registry we can create a log of all queries which have been
	 * executed during runtime.
	 *
	 * This is done automatically during debug-mode.
	 *
	 * If debug-mode is disabled you could also do this yourself.
	 *
	 * @see M_Debug::getQueryLog()
	 * @param string $sql
	 * @param float $executionTime
	 * @param array $backtrace
	 * @author Ben Brughmans
	 */
	public static function addToQueryLog($sql, $executionTime = null, $backtrace = null) {
		$log = self::getQueryLog();

		//append new values to query log
		$log->append(array(
			'sql' => (string)$sql ,
			'executiontime' => floatval($executionTime),
			'backtrace' => $backtrace
		));

		//update query log with new query
		self::_setQueryLog($log);
	}

	/**
	 * Print a list of queries which have been executed during runtime
	 *
	 * @return void
	 * @see M_Debug::getQueryLog()
	 * @author Ben Brughmans
	 * @todo: toggle backtrace (backtrace row is hidden now)
	 */
	public static function printQueryLog() {
		$totalExecutionTime = 0;
		$str = '';
		foreach(self::getQueryLog() AS $i => $log) {
			$executiontime = M_Helper::getArrayElement('executiontime', $log);

			//store the total amount of executiontime
			$totalExecutionTime += $executiontime;
			$str .= '<tr onclick="document.getElementById(\'debug-row-'. $i .'\').style.display = (document.getElementById(\'debug-row-'. $i .'\').style.display == \'none\' ? \'block\' : \'none\')">';
			$str .=		'<td>'.$log['sql'].'</td>';
			$str .=		'<td>'.$executiontime.'</td>';
			$str .= '</tr>';

			//get backtrace, and print if available
			$backtrace = M_Helper::getArrayElement('backtrace', $log);

			//if a backtrace is set, print it
			if (is_array($backtrace)):
				$str .= '<tr style="display:none;" id="debug-row-'. $i .'">';
				$str .=		'<td colspan="2">';
				$str .=			'<table>';
				foreach($backtrace AS $backtraceItem) {
					$str .=			'<tr>';
					$str .=				'<td>'.M_Helper::getArrayElement('file', $backtraceItem).'</td>';
					$str .=				'<td>'.M_Helper::getArrayElement('class', $backtraceItem).'::'.M_Helper::getArrayElement('function', $backtraceItem).'</td>';
					$str .=				'<td>'.M_Helper::getArrayElement('line', $backtraceItem).'</td>';
					$str .=			'</tr>';
				}
				$str .=			'</table>';
				$str .= '</tr>';
			endif;
		}

		//output the log in a table, with the totals on top
		$table = '<table border="1" cellpadding="2" style="background:#fff;color:#000;border:1px black solid;clear:both;">';
		$table .= '<tr>';
		$table .=	'<td style="background-color:#ccc;font-weight:bold;">' . count(self::getQueryLog()) . ' queries executed</td>';
		$table .=	'<td style="background-color:#ccc;font-weight:bold;">'.$totalExecutionTime.'</td>';
		$table .= '</tr>';
		$table .= $str;
		$table .= '</table>';

		echo $table;
	}
}