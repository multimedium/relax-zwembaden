<?php
/**
 * MI_ChartData interface
 * 
 * @package Core
 */
interface MI_ChartData {
	
	/**
	 * Set the legend for the containing data
	 * 
	 * @param MI_ChartLegend $legend
	 * @return void
	 */
	public function setLegend(MI_ChartLegend $legend);
	
	/**
	 * Get the legend for this containing data
	 * 
	 * @return MI_ChartLegend $legend
	 * 		Returns the legend of this chartdata
	 */
	public function getLegend();
	
	/**
	 * Set the data-value
	 * Chart data will always be a number/float
	 * 
	 * @param float $value
	 * @return void
	 */
	public function setValue($value);
	
	/**
	 * Get the data-value of the object
	 * 
	 * @return float
	 * 		Return a float
	 */
	public function getValue();
}