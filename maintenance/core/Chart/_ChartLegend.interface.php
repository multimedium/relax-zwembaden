<?php
/**
 * MI_ChartLegend interface
 * 
 * @package Core
 */
interface MI_ChartLegend {
	
	/**
	 * set the color
	 * 
	 * Is used to set the color of the legend-item
	 * Uses the {@link M_Color} object
	 * 
	 * @param M_Color $color
	 * @return void
	 */
	public function setColor(M_Color $color);
	
	/**
	 * Get the color
	 * 
	 * Is used to get the color of the legend-item
	 * 
	 * @return M_Color
	 * 		Returns a {@link M_Color} object
	 */
	public function getColor();

	/**
	 * set the Title
	 * 
	 * Is used to set the title of the legend
	 * 
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title);
	
	/**
	 * get the Title
	 * 
	 * Returns the title of the legend
	 * 
	 * @return string $title
	 */
	public function getTitle();
	
	/**
	 * set Description
	 * 
	 * Is used to set the description of the legend
	 * 
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description);
	
	/**
	 * get Description
	 * 
	 * Is used to get the description of the legend
	 * 
	 * @return string $description
	 * 		Returns the description of the legend
	 */
	public function getDescription();

}