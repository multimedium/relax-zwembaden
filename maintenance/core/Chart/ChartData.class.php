<?php 
/**
 * M_ChartData
 * 
 * Stores specific data, the data will be part of a {@link M_ChartDataset}
 * This object contains:
 * - a legend, from the {@link M_ChartLegend}
 * - a value
 * 
 * @author Wim Van De Mierop
 *
 */
class M_ChartData implements MI_ChartData {
	
	/**
	 * Stores the legend
	 * 
	 * @var M_ChartLegend
	 */
	private $_legend;
	
	/**
	 * Stores the value
	 * 
	 * @var float
	 */
	private $_value;
	
	public function __construct($value, M_ChartLegend $legend) {
		$this->setValue($value);
		$this->setLegend($legend);
	}
	
	/**
	 * Get Legend
	 * 
	 * Gets the legend of this data
	 * 
	 * @return M_ChartLegend $legend
	 */
	public function getLegend() {
		return $this->_legend;
	}

	/**
	 * Set Legend
	 * 
	 * Sets the legend of this data
	 * @param M_ChartLegend $legend
	 */
	public function setLegend(MI_ChartLegend $legend) {
		$this->_legend = $legend;
	}
	
	/**
	 * Get Value
	 * 
	 * Gets the value of this data
	 * 
	 * @return float $value
	 */
	public function getValue() {
		return $this->_value;
	}

	/**
	 * Set Value
	 * 
	 * Sets the value of this data
	 * 
	 * @param float $value
	 */
	public function setValue($value) {
		$this->_value = $value;
	}

}