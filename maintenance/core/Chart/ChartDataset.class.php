<?php 
/**
 * M_ChartDataset
 * 
 * The Dataset contains all the specific data in order to build
 * a chart
 * 
 * @author Wim Van De Mierop
 *
 */
class M_ChartDataset implements MI_ChartDataset {
	
	/**
	 * Used to store the different {@link M_ChartData} in an array
	 */
	private $_data = array();
	
	/**
	 * Minimum scale of the dataset
	 * 
	 * @var int $int
	 */
	private $_minScale;
	
	/**
	 * Minimum scale of the dataset
	 * 
	 * @var int $int
	 */
	private $_maxScale;
	
	/**
	 * Add Data
	 * 
	 * Used to add data to this Dataset
	 * 
	 * @param M_ChartData $data
	 */
	public function addData(MI_ChartData $data) {
		$this->_data[] = $data;
	}
	
	/**
	 * Get Data
	 * 
	 * Used to get all the data of this Dataset
	 * 
	 * @return ArrayIterator $data
	 */
	public function getData() {
		return new ArrayIterator($this->_data);
	}
	
	/**
	 * Get the minimum of the chart
	 * 
	 * @return int $minScale
	 */
	public function getMinScale() {
		return $this->_minScale;
	}
	
	/**
	 * Set the minScale
	 * 
	 * @param int $minScale
	 * @return void
	 */
	public function setMinScale($minScale) {
		$this->_minScale = $minScale;
	}
	
	/**
	 * Get the maximum scale of the chart
	 * 
	 * @return int $maxScale
	 */
	public function getMaxScale() {
		return $this->_maxScale;
	}
	
	/**
	 * Set the maximum scale of the chart
	 * 
	 * @param int $maxScale
	 * @return void
	 */
	public function setMaxScale($maxScale) {
		$this->_maxScale = $maxScale;
	}
	
}