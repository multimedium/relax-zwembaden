<?php 
/**
 * M_Chart
 * 
 * This class contains the necessary data in order to build a graph
 * 
 * @author Wim Van De Mierop
 *
 */
abstract class M_Chart implements MI_Chart {
	
	/**
	 * Stores all the datasets of the chart
	 * @var array
	 */
	private $_datasets = array();
	
	/**
	 * Stores the title of the chart
	 * @var string
	 */
	private $_title;
	
	/**
	 * Stores the width of the chart
	 * @var int
	 */
	private $_width;
	
	/**
	 * Stores the height of the chart
	 * @var int
	 */
	private $_height;
	
	/**
	 * Get Datasets
	 * 
	 * Get the datasets in order to build the chart
	 * 
	 * @return ArrayIterator $datasets
	 */
	public function getDatasets() {
		return new ArrayIterator($this->_datasets);
	}

	/**
	 * Add Dataset
	 * 
	 * Adds a dataset to the chart
	 * 
	 * @param M_ChartDataset $dataset
	 */
	public function addDataset(MI_ChartDataset $dataset) {
		$this->_datasets[] = $dataset;
	}

	/**
	 * Get Title
	 * 
	 * Get the tilte of the chart
	 * 
	 * @return string $title
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * Set Title
	 * 
	 * Set the title of the chart
	 * 
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->_title = $title;
	}

	/**
	 * Get width
	 * 
	 * Get the width of the chart
	 * 
	 * @return int $width
	 */
	public function getWidth() {
		return $this->_width;
	}

	/**
	 * Set width
	 * 
	 * Set the width of the chart
	 * 
	 * @param int $width
	 */
	public function setWidth($width) {
		$this->_width = $width;
	}

	/**
	 * Get height
	 * 
	 * Get the height of the chart
	 * 
	 * @return int $height
	 */
	public function getHeight() {
		return $this->_height;
	}

	/**
	 * Set height
	 * 
	 * Set the height of the chart
	 * 
	 * @param int $height
	 */
	public function setHeight($height) {
		$this->_height = $height;
	}
	
}