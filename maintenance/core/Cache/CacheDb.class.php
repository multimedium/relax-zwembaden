<?php
/**
 * M_CacheDb class
 * 
 * This concrete subclass of M_CacheMemory handles the cache memory
 * that is saved into a database table.
 *
 * @package Core
 */
class M_CacheDb extends M_CacheMemory {
	/**
	 * Set the location of cache memory
	 * 
	 * This method will set the location where cache'd data lives. In
	 * the case of the M_CacheDb class, this method is used to set
	 * the database table where cache'd data is stored and retrieved.
	 * The table is represented by an object that implements the 
	 * {@link MM_IDbTable} interface.
	 * 
	 * @access public
	 * @param M_IDbTable $location
	 * 		The location of the cache memory
	 * @return void
	 */
	public function setLocation(M_IDbTable $location) {
		$this->_location = $location;
	}
	
	/**
	 * Read from cache
	 * 
	 * Overwrites the {@link M_CacheMemory::read()} method.
	 *
	 * @access public
	 * @abstract
	 * @param string $dataSetId
	 * 		The ID of the data in cache memory. Each set of data is 
	 * 		marked with an ID when written to cache. The same ID is
	 * 		used to retrieve the data from cache.
	 * @return mixed
	 */
	public function read($dataSetId) {
		// Fetch the requested data from the location
		$select = $this->_location->select();
		$select->columns(array('data', 'time'));
		$select->where('id = ?', $dataSetId);
		$rs = $select->execute();
		
		// check if cache'd data has been found:
		if($rs !== FALSE && count($rs) == 1) {
			$row = $rs->current();
			if($this->_lifetime == CacheMemory::LIFETIME_UNLIMITED || ($row['time'] + $this->_lifetime) < time()) {
				return unserialize($row['data']);
			}
		}
		
		// if the data has not been found, return NULL
		return NULL;
	}
	
	/**
	 * Write data to cache memory
	 * 
	 * Overrides the {@link M_CacheMemory::write()} method.
	 *
	 * @access public
	 * @abstract
	 * @param string $dataSetId
	 * 		The IF of the data in cache memory.
	 * @param mixed $data
	 * 		The data to be saved to cache memory
	 * @return boolean
	 * 		Will return TRUE on success, FALSE on error
	 */
	public function write($dataSetId, $data) {
		return (boolean) $this->_location->insert(array(
			'id'   => $dataSetId,
			'data' => serialize($data),
			'time' => time()
		));
	}
}
?>