<?php
/**
 * M_Server class
 * 
 * M_Server is used to fetch information about the server on which 
 * the installed system is running. Some variables in PHP's predefined
 * $_SERVER superglobal will depend on the server environment. This
 * class takes responsibility of reading the released info correctly.
 * 
 * @package Core
 */
class M_Server extends M_Object {
	/**
	 * The base href
	 * 
	 * This property stores the base href (return value of 
	 * {@link M_Server::getBaseHref()}. M_Server stores the base href,
	 * to improve performance of {@link M_Server::getBaseHref()} (may
	 * be called frequently in the application).
	 * 
	 * @access private
	 * @static
	 * @var string
	 */
	private static $_baseHref = NULL;
	
	/**
	 * The document root
	 * 
	 * This property stores the document root (return value of 
	 * {@link M_Server::getDocumentRoot()}. M_Server stores the document root,
	 * to improve performance of {@link M_Server::getDocumentRoot()} (may
	 * be called frequently in the application).
	 * 
	 * @access private
	 * @static
	 * @var string
	 */
	private static $_documentRoot = NULL;

	/**
	 * If modified since
	 *
	 * This request header is used with GET method to make it conditional:
	 * if the requested document has not changed since the time specified in
	 * this field the document will not be sent, but instead a  Not Modified
	 * 304 reply.
	 *
	 * @var string
	 */
	private static $_ifModifiedSince = NULL;

	/**
	 * If none match
	 *
	 * A etag header is a unique value which represents the content of the
	 * requested url. It is used to check if the content has been changed.
	 *
	 * In typical usage, when a URL is retrieved the web server will return the
	 * resource along with its corresponding ETag value, which is placed in an
	 * HTTP "ETag" header: 686897696a7c876b7e.
	 *
	 * The client may then decide to cache the resource, along with its ETag.
	 * Later, if the client wants to retrieve the same URL again, it will send
	 * its previously saved copy of the ETag along with the request in
	 * a "If-None-Match" header.
	 *
	 * @see http://en.wikipedia.org/wiki/HTTP_ETag
	 * @var string
	 */
	private static $_ifNoneMatch = NULL;
	
	/**
	 * Get Base Href
	 * 
	 * This method will take its best guess at the "base href" of the
	 * running application. Typically, such a "base href" is added to
	 * the .htaccess file, if the application is not running in the
	 * root folder.
	 * 
	 * Example 1, .htaccess base href
	 * <code>
	 *    <IfModule mod_rewrite.c>
	 *       RewriteEngine on
	 *       
	 *       # Modify the RewriteBase if you are using the system in a 
	 *       # subdirectory and the rewrite rules are not working 
	 *       # properly.
	 *       RewriteBase /myApplication/
	 *    </IfModule>
	 * </code>
	 * 
	 * @access public
	 * @uses M_Request::getUriString()
	 * @return string
	 */
	public static function getBaseHref() {
		if(self::$_baseHref == NULL) {
			// echo 'render base href<br>';
			$uri = M_Request::getUriString();
			$uriLen = strlen($uri);
			$script = pathinfo($_SERVER['SCRIPT_NAME'], PATHINFO_DIRNAME) . '/';
			$c = TRUE;
			$href = '';
			for($i = 0, $n = strlen($script); $i < $n && $c; $i ++) {
				if($i < $uriLen) {
					$c = ($uri{$i} == $script{$i});
					if($c) {
						$href .= $uri{$i};
					}
				}
			}
			self::$_baseHref = $href;
		}
		return self::$_baseHref;
	}
	
	/**
	 * Get Documentroot
	 * 
	 * This method will returns the "document root" of the
	 * webserver, which is defined mostly in httpd.conf (apache)
	 * 
	 * @access public
	 * @uses M_Request::getUriString()
	 * @return string
	 */
	public static function getDocumentRoot() {
		if(self::$_documentRoot == NULL) {
			self::$_documentRoot = M_Helper::getArrayElement(
				'DOCUMENT_ROOT', 
				$_SERVER
			);
		}
		return self::$_documentRoot;
	}

	/**
	 * Get If-modified-since
	 *
	 * This method will returns the "if modified since" header value, if
	 * available
	 *
	 * @access public
	 * @return string
	 */
	public static function getIfModifiedSince() {
		if(self::$_ifModifiedSince == NULL) {
			self::$_ifModifiedSince = M_Helper::getArrayElement(
				'HTTP_IF_MODIFIED_SINCE',
				$_SERVER
			);
		}
		return self::$_ifModifiedSince;
	}

	/**
	 * Get If-none-match
	 *
	 * This method will returns the "if none match" header value, if
	 * available.
	 *
	 * @see M_Server::_ifNoneMatch
	 * @access public
	 * @return string
	 */
	public static function getIfNoneMatch() {
		if(self::$_ifNoneMatch == NULL) {
			self::$_ifNoneMatch = M_Helper::getArrayElement(
				'HTTP_IF_NONE_MATCH',
				$_SERVER
			);
		}
		return self::$_ifNoneMatch;
	}
}