<?php
/**
 * M_Helper class
 * 
 * This class contains different helper (or utitlity) functions which can 
 * be used throughout the application.
 * 
 * @package Core
 */
class M_Helper extends M_Object {
	/**
	 * Make a string's first character lowercase
	 * 
	 * Returns a string with the first character lowercased, if that character 
	 * is alphabetic.
	 *
	 * @access public
	 * @param string $str
	 * @return string
	 */
	public static function lcfirst($str) {
		if(! is_string($str)) {
			throw new M_Exception(sprintf(
				'Expected string value in %s::%s()',
				__CLASS__,
				__FUNCTION__
			));
		}
		return strtolower( $str{0} ) . substr( $str, 1);
	}
	
	/**
	 * Strip whitespace (or other characters) from a string
	 * 
	 * Will do exactly the same as PHP's built-in trim() function. Note however
	 * that white-space characters are ALWAYS removed from the original string,
	 * but you can choose to strip additional characters, by providing the second
	 * argument.
	 * 
	 * Example 1, strip /
	 * <code>
	 *    $original = 'Hello    /';
	 *    echo '"' . M_Helper::trimCharlist($original, '/') . '"';
	 * </code>
	 * 
	 * Example 1 will produce the following output:
	 * <code>
	 *    "Hello"
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string that will be trimmed
	 * @param string $characterList
	 * 		The list of additional characters to be trimmed from the string
	 * @return string
	 */
	public static function trimCharlist($string, $characterList = '') {
		if(! is_string($string)) {
			throw new M_Exception(sprintf(
				'Expected string value in %s::%s()',
				__CLASS__,
				__FUNCTION__
			));
		}
		return trim($string, " \t\n\r\0\x0B" . (is_array($characterList) ? implode('', $characterList) : (string) $characterList));
	}
	
	/**
	 * Strip whitespace (or other characters) from a string
	 * 
	 * Will do exactly the same as {@link M_Helper::trimCharlist()}, but will
	 * strip the characters only from the BEGINNING of the string!
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string that will be trimmed
	 * @param string $characterList
	 * 		The list of additional characters to be trimmed from the string
	 * @return string
	 */
	public static function ltrimCharlist($string, $characterList = '') {
		if(! is_string($string)) {
			throw new M_Exception(sprintf(
				'Expected string value in %s::%s()',
				__CLASS__,
				__FUNCTION__
			));
		}
		return ltrim($string, " \t\n\r\0\x0B" . (is_array($characterList) ? implode('', $characterList) : (string) $characterList));
	}
	
	/**
	 * Strip whitespace (or other characters) from a string
	 * 
	 * Will do exactly the same as {@link M_Helper::trimCharlist()}, but will
	 * strip the characters only from the END of the string!
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string that will be trimmed
	 * @param string $characterList
	 * 		The list of additional characters to be trimmed from the string
	 * @return string
	 */
	public static function rtrimCharlist($string, $characterList = '') {
		if(! is_string($string)) {
			throw new M_Exception(sprintf(
				'Expected string value in %s::%s()',
				__CLASS__,
				__FUNCTION__
			));
		}
		return rtrim($string, " \t\n\r\0\x0B" . (is_array($characterList) ? implode('', $characterList) : (string) $characterList));
	}
	
	/**
	 * Remove newline-characters from a string
	 *
	 * @param string $string
	 * @return string
	 */
	public static function filterNewlines($string) {
		return str_replace(array("\n", "\r"), '', $string);
	}
	
	/**
	 * Get unique token
	 * 
	 * Will generate a unique token based on time in microseconds. The returned
	 * value is a 32-character long hash (MD5).
	 * 
	 * @access public
	 * @return str
	 */
	public static function getUniqueToken() {
		return md5(uniqid(rand(), true));
	}
	
	/**
	 * Explode a string into array, by camel casing
	 *
	 * Will explode a string into an array. An array element is created out of
	 * each word in the camel-cased string.
	 * 
	 * Example 1
	 * <code>
	 *    print_r(M_Helper::explodeByCamelCasing('ThisIsMyExample'));
	 * </code>
	 * 
	 * Example 1 would output the following array:
	 * <code>
	 *    Array (
	 *       [0] => 'This',
	 *       [1] => 'Is',
	 *       [2] => 'My',
	 *       [3] => 'Example'
	 *    )
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The camel-cased string, to be exploded into an array
	 * @return array
	 */
	public static function explodeByCamelCasing($string) {
		$out = array('');
		$pos = 0;
		for($i = 0, $n = strlen($string); $i < $n; $i ++) {
			$o = ord($string{$i});
			if($o > 64 && $o < 91) {
				$cur = ($pos == 0 && empty($out[0])) ? 0 : ++ $pos;
				$out[$cur] = $string{$i};
			} else {
				$out[$pos] .= $string{$i};
			}
		}
		return $out;
	}

	/**
	 * Explode a string into array, by newlines
	 *
	 * Will explode a string into an array. An array element is created out of
	 * each newline.
	 *
	  * Example 1
	 * <code>
	 *    print_r(M_Helper::explodeByNewLines('Here goes a
	 * newline'));
	 * </code>
	 *
	 * Example 1 would output the following array:
	 * <code>
	 *    Array (
	 *       [0] => 'Here goes a',
	 *       [1] => 'newline'
	 *    )
	 * </code>
	 *
	 * @param string $string
	 * @return array
	 * @author Ben Brughmans
	 * @static
	 */
	public static function explodeByNewLines($string) {
		$string = str_replace("\r", '', $string);
		return explode("\n", trim($string));
	}

	/**
	 * Convert a string to a camel-cased string
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string to be converted into camel-cased string
	 * @param bool $lcFirst
	 * 		Set to TRUE if you want the first word to start with an uppercase
	 * 		character, FALSE if you prefer lowercase
	 * @return string
	 */
	public static function getCamelCasedString($string, $lcfirst = FALSE) {
		// First of all, we create a chain of filters, to
    	// - remove all accents (converted to corresponding character)
    	// - remove html markup
    	$filter = new M_FilterTextAccents(new M_FilterTextHtml(new M_FilterTextValue($string)));
    	
    	// Remove punctuation at the beginning/end of the string
    	$string = M_Helper::trimCharlist($filter->apply(), ',.?!)("\'/\\_+;:*');
    	
		// Remove redundant white-spaces:
		$string = preg_replace('/[\s\-]+/', ' ', $string);
		
		// Remove punctuation (replaced by dashes):
		$string = str_replace(',', '-', $string);
		$string = str_replace('.', '-', $string);
		$string = str_replace('?', '-', $string);
		$string = str_replace('!', '-', $string);
		$string = str_replace(')', '-', $string);
		$string = str_replace('(', '-', $string);
		$string = str_replace('"',  '', $string);
		$string = str_replace('\'', '', $string);
		$string = str_replace('/', '-', $string);
		$string = str_replace('_', '-', $string);
		$string = str_replace('+', '-', $string);
		$string = str_replace(',', '-', $string);
		$string = str_replace(';', '-', $string);
		$string = str_replace(':', '-', $string);
		$string = str_replace('*',  '', $string);
		$string = str_replace(' ', '-', $string);
		
		// Some characters are replaced by full words:
		$string = str_replace('%', 'percent-', $string);
		$string = str_replace('@', 'at-',      $string);
		$string = str_replace('&', 'and-',     $string);
		
		// Remove redundant dashes, and make lowercase:
		$string = strtolower(preg_replace('/[-]+/', '-', $string));
		
		// Now, we remove anything that is not in the alphanumerical range. For
		// each of the characters in the string:
		$out = '';
		for($i = 0; $i < strlen($string); $i ++) {
			// If the current character is in the range of allowed characters:
			if(strpos(' abcdefghijklmnopqrstuvwxyz0123456789-', $string{$i})) {
				// We add the character to the output string:
				$out .= $string{$i};
			}
		}
		
		// return final camel-cased string:
		$out = implode(array_map('ucfirst', explode('-', $out)));
		
		return (
			$lcfirst
				? self::lcfirst($out)
				: $out
		);
	}
	
	/**
	 * Remove repeated characters from a string
	 * 
	 * @static
	 * @access public
	 * @example M_Helper::getUniqueCharsInString(1,'aaaaaaabcbc')   = 'abcbc'
	 * @example M_Helper::getUniqueCharsInString(2,'aaaaaaabcbc')   = 'aabc'
	 * @example M_Helper::getUniqueCharsInString(2,'aaaaaaabcdbcd') = 'aabcd'
	 * @param integer $charLength
	 * 		How big is the repeated string?
	 * @param string $string
	 * 		The string to be cleaned
	 * @return string
	 */
	public static function getUniqueCharsInString($charLength, $string) {
		$cleaned = '';
		for ($i = 0; $i < strlen($string) ; $i++) {
			$repeated = true;
			for ($j = 0; $j < $charLength && ($j + $i + $charLength) < strlen($string) ; $j++) {
				$repeated = $repeated && ($string{($j + $i)} == $string{($j + $i + $charLength)});
			}
			if ($j < $charLength) {
				$repeated = false;
			}
			if ($repeated) {
				$i += $charLength - 1;
				$repeated = false;
			}
			else {
				$cleaned .= $string{$i};
			}
		}
		return $cleaned;
	}
	
	/**
	 * Has character(s)?
	 * 
	 * Will check if the provided string contains one of the characters.
	 * 
	 * @static
	 * @access public
	 * @example M_Helper::containsCharacters('tom', 't') = TRUE
	 * @example M_Helper::containsCharacters('tom', 'k') = FALSE
	 * @example M_Helper::containsCharacters('tom', 'om') = TRUE
	 * @example M_Helper::containsCharacters('tom', 'mj') = TRUE
	 * @param string $string
	 * 		The string value to be evaluated
	 * @param string $characterList
	 * 		The characters to look for
	 * @return bool $flag
	 * 		Returns TRUE if the string contains any of the character(s),
	 * 		FALSE if not.
	 */
	public static function containsCharacters($string, $characterList) {
		// For each of the characters in the string:
		for($i = 0, $n = strlen($string); $i < $n; $i ++) {
			// Check if the current character is one of the characters we are
			// looking for:
			if(strpos($characterList, $string{$i}) !== FALSE) {
				// If so, we return TRUE:
				return TRUE;
			}
		}
		
		// If we're still here, it means that we did not find any of the characters.
		// In that case, we return FALSE
		return FALSE;
	}
	
	/**
	 * Has CDATA characters?
	 * 
	 * Will check if the provided string contains special characters, which would
	 * require the string to be wrapped by 
	 * 
	 * <code><![CDATA[ ]]></code>
	 * 
	 * in an XML file's source code. 
	 * 
	 * FYI, W3C Says:
	 * The term CDATA is used about text data that should not be parsed by the 
	 * XML parser. Characters like "<" and "&" are illegal in XML elements. "<" 
	 * will generate an error because the parser interprets it as the start of 
	 * a new element. "&" will generate an error because the parser interprets 
	 * it as the start of an character entity.
	 * 
	 * @static
	 * @access public
	 * @example M_Helper::containsCharactersForXmlCdata('tom') = FALSE
	 * @example M_Helper::containsCharactersForXmlCdata('Tom Bauwens <tom@multimedium.be>') = TRUE
	 * @uses M_Helper::containsCharacters()
	 * @param string $string
	 * 		The string value to be evaluated
	 * @return bool $flag
	 * 		Returns TRUE if the string contains any of the special character(s),
	 * 		FALSE if not.
	 */
	public static function containsCharactersForXmlCdata($string) {
		return self::containsCharacters($string, '&<');
	}
	
	/**
	 * Get closest interval number
	 * 
	 * This method will round the provided number to the "closest
	 * neighbour". This closest neighbour is defined by another number,
	 * of which a multiplying table is used to create neighbours.
	 * 
	 * @static
	 * @access public
	 * @example M_Helper::getIntegerClosestNeighbour(5, 88) = 90
	 * @example M_Helper::getIntegerClosestNeighbour(5, 51) = 50
	 * @example M_Helper::getIntegerClosestNeighbour(5, 53) = 55
	 * @example M_Helper::getIntegerClosestNeighbour(5, 2)  = 0
	 * @param integer $neighbours
	 * 		The number with which to create neighbours
	 * @param integer $number
	 * 		The number to be rounded to the closest neighbour
	 * @return integer
	 */
	public static function getIntegerClosestNeighbour($neighbours, $number) {
		return ((int) $neighbours * round((int) $number / (int) $neighbours));
	}
	
	/**
	 * Is boolean true?
	 * 
	 * This method will check if a given variable is a boolean, in the widest 
	 * meaning of the word. The following values will be considered a boolean
	 * TRUE:
	 * 
	 * - (bool) TRUE
	 * - (string) yes|YES
	 * - (int) 1
	 * 
	 * The following values will be considered a boolean FALSE:
	 * 
	 * - (bool) FALSE
	 * - (string) no|NO
	 * - (int) 0
	 * 
	 * This method is typically used to recognize boolean values in XML files,
	 * or other formats in which configuration variables are defined.
	 * 
	 * This method will return TRUE if the variable is considered a boolean TRUE,
	 * FALSE if not
	 * 
	 * @access public
	 * @param mixed $value
	 * @return bool
	 */
	public static function isBooleanTrue($value) {
		if(is_bool($value)) {
			return $value;
		} else {
			if(strtolower((string) $value) == 'true' || strtolower((string) $value) == 'yes' || $value == 1) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	/**
	 * Check if a class/object implements an interface
	 * 
	 * Will check if a given class/object implements a given interface name.
	 * Returns TRUE If the class/object implements the interface, FALSE if not.
	 * 
	 * Example 1:
	 * <code>
	 *    $rs = M_Helper::isInterface(new M_Config, 'MI_Config');
	 *    var_dump($rs);
	 * </code>
	 * 
	 * Example 1 would produce the following output:
	 * <code>
	 *    bool(true)
	 * </code>
	 * 
	 * NOTE:
	 * If the provided class/object cannot be found, this method will return FALSE.
	 * No exception is thrown. The class/object and the interface will be auto-loaded. 
	 * For more info on auto-loading, read {@link M_Loader::setAsAutoloader()}.
	 * 
	 * @access public
	 * @return bool
	 */
	public static function isInterface($object, $interface) {
		$className = is_object($object) ? get_class($object) : (string) $object;
		return (
			class_exists($className, TRUE) &&
			in_array((string) $interface, class_implements($className, TRUE))
		);
	}
	
	/**
	 * Check if an object is an instance of a given class
	 * 
	 * Will check if the given object is an instance of the given class. For
	 * example:
	 *
	 * <code>
	 *   if(! M_Helper::isInstanceOf($config, 'MI_Config')) {
	 *      echo 'not a MI_Config instance!';
	 *   }
	 * </code>
	 *
	 * Note that you can also provide with various class names. If you do, this
	 * method will check if the object is an object of either one of the provided
	 * class names. For example:
	 *
	 * <code>
	 *   if(! M_Helper::isInstanceOf($field, array('M_FieldText', 'M_FieldSelect'))) {
	 *      echo 'not an instance of M_FieldText or M_FieldSelect!';
	 *   }
	 * </code>
	 * 
	 * @access public
	 * @param mixed $object
	 * 		The object to be checked
	 * @param string $class
	 * 		The name of the class
	 * @return bool $flag
	 * 		Returns TRUE if the object is an instance of the class, FALSE if not
	 */
	public static function isInstanceOf($object, $class) {
		// If the provided class name is an array/iterator:
		if(self::isIterator($class)) {
			// For each of the class names:
			foreach($class as $currentClass) {
				// Do the check for the current class name:
				if(is_object($object) && $object instanceof $currentClass) {
					// Return TRUE if the object is an instance of the current
					// class name:
					return TRUE;
				}
			}

			// Return FALSE, if we could not find a match
			return FALSE;
		}

		// If the provided class name is not an array/iterator:
		return (is_object($object) && $object instanceof $class);
	}
	
	/**
	 * Is Iterator?
	 * 
	 * Will check if the provided argument is an iterator. Note that the following
	 * are considered iterators:
	 * 
	 * - Array
	 * - Object that implements the Iterator interface
	 * 
	 * @see M_Helper::isInterface()
	 * @access public
	 * @param mixed $var
	 * 		The variable that is to be evaluated as iterator
	 * @return bool $flag
	 * 		Returns TRUE if the argument is an iterator, FALSE if not
	 */
	public static function isIterator($var) {
		if(is_array($var)) {
			return TRUE;
		} elseif(is_object($var)) {
			return M_Helper::isInterface($var, 'Iterator');
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Is Iterator with elements of given interface?
	 * 
	 * Will check if the provided argument is an iterator. To do so, it uses
	 * {@link M_Helper::isIterator()}. Additionally, this method will make sure
	 * that all of the contained elements implement the given interface.
	 * 
	 * @see M_Helper::isIterator()
	 * @access public
	 * @param mixed $var
	 * 		The variable that is to be evaluated as iterator
	 * @param string $interface
	 * 		The name of the interface
	 * @return bool $flag
	 * 		Returns TRUE if the argument is an iterator with elements of the given
	 * 		interface, FALSE if not
	 */
	public static function isIteratorOfInterface($var, $interface) {
		if(! M_Helper::isIterator($var)) {
			return FALSE;
		}
		foreach($var as $element) {
			if(! M_Helper::isInterface($element, $interface)) {
				return FALSE;
			}
		}
		return TRUE;
	}
	
	/**
	 * Is Iterator with elements of given class?
	 * 
	 * Will check if the provided argument is an iterator. To do so, it uses
	 * {@link M_Helper::isIterator()}. Additionally, this method will make sure
	 * that all of the contained elements are instances of a given class name.
	 *
	 * <code>
	 *   if(! M_Helper::isIteratorOfClass($configs, 'MI_Config')) {
	 *      echo 'Oops; expecting MI_Config instances';
	 *   }
	 * </code>
	 *
	 * Note that you can also provide with various class names. If you do, this
	 * method will check if the objects are of either one of the provided class
	 * names. For example:
	 *
	 * <code>
	 *   if(! M_Helper::isIteratorOfClass($field, array('M_FieldText', 'M_FieldSelect'))) {
	 *      echo 'Oops; expecting instances of M_FieldText or M_FieldSelect!';
	 *   }
	 * </code>
	 * 
	 * @see M_Helper::isIterator()
	 * @see M_Helper::isIteratorOfInterface()
	 * @access public
	 * @param mixed $var
	 * 		The variable that is to be evaluated as iterator
	 * @param string $class
	 * 		The name of the class
	 * @return bool $flag
	 * 		Returns TRUE if the argument is an iterator with elements of the given
	 * 		class, FALSE if not
	 */
	public static function isIteratorOfClass($var, $class) {
		if(! M_Helper::isIterator($var)) {
			return FALSE;
		}
		foreach($var as $element) {
			if(! M_Helper::isInstanceOf($element, $class)) {
				return FALSE;
			}
		}
		return TRUE;
	}
	
	/**
	 * Get a value from an array or return a default value when it doesn't exist
	 * 
	 * Will safely check if $key exists in $array
	 * 
	 * @param str|int $key
	 * @param array $array
	 * @param mixed $default
	 * 			The value which will be returned when element doesn't exist
	 */
	public static function getArrayElement($key, array $array,$default = null) {
		if (array_key_exists($key,$array)) return $array[$key];
		else return $default;
	}
	
	/**
	 * Sort a 2-dimensional array
	 *
	 * @param array $array
	 * @param string $key
	 * @param string $order ASC|DESC
	 * @return unknown
	 */
	public static function arraySortMulti($array, $key, $order = "ASC")
    {
        $tmp = array();
        foreach($array as $akey => $array2) {
            $tmp[$akey] = $array2[$key];
        }
       
        if($order == "DESC") {
        	arsort($tmp , SORT_NUMERIC );
        }
        else {
        	asort($tmp , SORT_NUMERIC );
       	}

        $tmp2 = array();       
        foreach($tmp as $key => $value) {
            $tmp2[$key] = $array[$key];
        }       
       
        return $tmp2;
    } 
	
	/**
	 * Decode a json-string
	 *
	 * Note: this will check if the php function json_decode exists, if not
	 * an internal service will be used
	 * 
	 * @param string $json
	 * @return array
	 */
	public static function jsonDecode($json) {
		
		if (function_exists('json_decode')) {
			$result = json_decode($json);
		}
		else
		{
			M_Loader::loadRelative('core/_thirdparty/json/json.class.php', true);
			$jsonService = new Services_JSON;
			$result = $jsonService->decode($json);
		}
		
		return $result;
   	}  
   	
   	/**
   	 * Encode to a json string
   	 *
	 * Note: this will check if the php function json_encode exists, if not
	 * an internal service will be used
	 * 
   	 * @param mixed $arg
   	 * @return string
   	 */
   	public static function jsonEncode($arg) {
		if (function_exists('json_encode')) {
			$result = json_encode($arg);
		}
		else
		{
			M_Loader::loadRelative('core/_thirdparty/json.class.php', true);
			$jsonService = new Services_JSON;
			$result = $jsonService->encode($arg);
		}
		
		return $result;
   	}
}