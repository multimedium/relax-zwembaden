<?php
/**
 * M_Header class
 * 
 * M_Header is used to send headers to the client browser. In many
 * cases, it is an interface (or wrapper) to PHP's header() function 
 * call(s).
 * 
 * @package Core
 */
class M_Header extends M_Object {
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular constant is used
	 * to describe the Content Type.
	 * 
	 * NOTE:
	 * The use of this constant will require a header value. Check out
	 * this example:
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE, 'text/xml');
	 * </code>
	 * 
	 * M_Header also provides with Content-Type constants that do not
	 * require an additional value. Check out the following constants
	 * for more info:
	 * 
	 * - {@link M_Header::CONTENT_TYPE_XML}
	 * - {@link M_Header::CONTENT_TYPE_PDF}
	 * - {@link M_Header::CONTENT_TYPE_EXE}
	 * - {@link M_Header::CONTENT_TYPE_ZIP}
	 * - {@link M_Header::CONTENT_TYPE_DOC}
	 * - {@link M_Header::CONTENT_TYPE_XLS}
	 * - {@link M_Header::CONTENT_TYPE_PPT}
	 * - {@link M_Header::CONTENT_TYPE_JPG}
	 * - {@link M_Header::CONTENT_TYPE_PNG}
	 * - {@link M_Header::CONTENT_TYPE_GIF}
	 * - {@link M_Header::CONTENT_TYPE_MP3}
	 * - {@link M_Header::CONTENT_TYPE_WAV}
	 * - {@link M_Header::CONTENT_TYPE_MPG}
	 * - {@link M_Header::CONTENT_TYPE_MOV}
	 * - {@link M_Header::CONTENT_TYPE_AVI}
	 * - {@link M_Header::CONTENT_TYPE_FORCE_DOWNLOAD}
	 */
	const CONTENT_TYPE = 'Content-Type';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe XML.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_XML);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_XML = 'Content-Type: text/xml';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a PDF Document.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_PDF);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_PDF = 'Content-Type: application/pdf';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a Windows Executable (.exe).
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_EXE);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_EXE = 'Content-Type: application/octet-stream';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe an Archive File (.zip).
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_ZIP);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_ZIP = 'Content-Type: application/zip';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a Microsoft Word Document.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_DOC);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_DOC = 'Content-Type: application/msword';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a Microsoft Excel Spreadsheet.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_XLS);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_XLS = 'Content-Type: application/vnd.ms-excel';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a Microsoft Powerpoint Presentation.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_PPT);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_PPT = 'Content-Type: application/vnd.ms-powerpoint';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a Graphic Interchange Format (GIF) image
	 * file.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_GIF);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_GIF = 'Content-Type: image/gif';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a Joint Photographic Experts Group (JPEG) 
	 * image file.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_JPG);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_JPG = 'Content-Type: image/jpg';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a Portable Network Graphics (PNG) image 
	 * file.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_PNG);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_PNG = 'Content-Type: image/png';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe an MP3 Audio File.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_MP3);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_MP3 = 'Content-Type: audio/mpeg';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe an WAV Audio File.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_WAV);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_WAV = 'Content-Type: audio/x-wav';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a movie file, encoded in the Moving Pictures 
	 * Experts Group (MPEG) format.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_MPG);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_MPG = 'Content-Type: video/mpeg';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a QuickTime Movie File (MOV).
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_MOV);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_MOV = 'Content-Type: video/quicktime';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a movie file in AVI (Audio-Video Interleave)
	 * format.
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_AVI);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_AVI = 'Content-Type: video/x-msvideo';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a file (of any type) of which the download
	 * should be forced. This is typically used to force the download
	 * dialog to pop up at the client's browser (even if the file is
	 * an image, for example)
	 * 
	 * NOTE:
	 * The use of this constant does not require a header value.
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_FORCE_DOWNLOAD);
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_FORCE_DOWNLOAD = 'Content-Type: application/force-download';
	
	/**
	 * Header value
	 * 
	 * This constant is used to describe a header value that can be
	 * sent to the client's browser. This particular header constant 
	 * is used to describe a fileof type text/html. This is typically used
	 * to set the client's browser encoding
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_TEXT/HTML, 'charset=utf-8');
	 * </code>
	 * 
	 * @see M_Header::CONTENT_TYPE
	 */
	const CONTENT_TYPE_TEXT_HTML = 'Content-Type: text/html';
	
	/**
	 * Base href
	 * 
	 * @access private
	 * @static 
	 * @var string
	 */
	private static $_rewriteBase = NULL;
	
	/**
	 * Set base href
	 * 
	 * This method will set the base href, used by the method 
	 * {@link M_Header::redirect()}.
	 * 
	 * NOTE:
	 * This method can be called statically
	 * 
	 * @access public
	 * @return void
	 */
	public static function setBaseHref($path) {
		self::$_rewriteBase = rtrim($path, " \t\n\r\0\x0B/");
	}
	
	/**
	 * Redirect
	 * 
	 * This method will (try to) redirect to the given path. If the 
	 * path is relative, it is interpreted as relative to the 
	 * application's base href that has been set previously with 
	 * {@link M_Header::setBaseHref()}.
	 * 
	 * NOTE:
	 * If no base href has been set for the running application, this
	 * method will use {@link M_Server::getBaseHref()} to take its
	 * best guess at what the base href is.
	 * 
	 * NOTE:
	 * This method can be called statically
	 * 
	 * @access public
	 * @param string $path
	 * 		The path to which the page request should be redirected
	 * @return void
	 */
	public static function redirect($path) {
		// If the path is relative:
		if(strncmp($path, 'http', 4)) {
			// If the base href has not been set previously, we take 
			// our own best guess:
			if(self::$_rewriteBase == NULL) {
				// self::setBaseHref(M_Server::getBaseHref());
				self::setBaseHref(M_Helper::rtrimCharlist(M_Request::getLink(), '/'));
			}
			
			// Compose the full path:
			$path = self::$_rewriteBase . '/' . ltrim($path, " \t\n\r\0\x0B/");
		}
		
		// clean the output buffer, if buffering the output
		@ob_clean();
		
		// If headers have already been sent:
		if(headers_sent()) {
			// in that case, we have to redirect with a javascript thingy :(
			$js  = '<script type="text/javascript" language="Javascript">';
			$js .=    'document.location = "'. $path .'";';
			$js .= '</script>';
			echo $js;
		}
		// If no headers have been sent yet:
		else {
			header('Location: ' . $path);
		}
		
		// after redirecting, we stop the script
		die();
	}
	
	/**
	 * Send a header value
	 * 
	 * This method can be used to send a header value. For example,
	 * this method could be used to send out the Content-Type header,
	 * by writing the following code:
	 * 
	 * <code>
	 *    M_Header::send('Content-Type', 'text/xml');
	 * </code>
	 * 
	 * However, to avoid typos and other mistakes, M_Header also provides
	 * with a set of predefined constants that describe header values.
	 * The example above could be written as following, using the class
	 * constants:
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE, 'text/xml');
	 * </code>
	 * 
	 * Note that the value of the header to be sent is optional. In this
	 * case, it is probably easier to leave out the header value, and
	 * use another predefined header:
	 * 
	 * <code>
	 *    M_Header::send(M_Header::CONTENT_TYPE_XML);
	 * </code>
	 * 
	 * Read more about M_Header constants, to learn about all of the
	 * predefined headers that exist.
	 * 
	 * @access public
	 * @param string $header
	 * 		The header to be sent, eg "Content-Type"
	 * @param string $value
	 * 		The header value (optional). Some constants of M_Header
	 * 		will not require you to add a header value (see examples).
	 * @return void
	 */
	public static function send($header, $value = NULL) {
		// If the header is of type text_html we use another separator
		if ($header == self::CONTENT_TYPE_TEXT_HTML) {
			header($header . '; ' . $value);
		}
		// If the header contains the : character, it is already separating
		// the header name and value. In that case, we completely
		// ignore the header value that has been passed in:
		elseif(strpos($header, ':')) {
			header($header);
		}
		// If the header does not separate the name and value, we
		// treat it as a header name. In that case, we do use the 
		// header value to compose the complete header.
		else {
			header($header . ': ' . $value);
		}
	}
	
	/**
	 * Send "404" or "Not Found"
	 * 
	 * The "404" or "Not Found" error message is an HTTP standard 
	 * response code indicating that the client was able to communicate 
	 * with the server but either the server could not find what was 
	 * requested, or it was configured not to fulfill the request and 
	 * did not reveal the reason why.
	 * 
	 * NOTE:
	 * This method can be called statically
	 * 
	 * @access public
	 * @return void
	 */
	public static function send404() {
		self::sendHttpStatus(404);
	}
	
	/**
	 * Send Status Code
	 * 
	 * This method will send HTTP standard responses to describe the
	 * status of the request. For example, this method could be used
	 * as an alternative to {@link M_Header::send404()}:
	 * 
	 * Example 1, send error message 404
	 * <code>
	 *    M_Header::sendHttpStatus(404);
	 * </code>
	 * 
	 * NOTE:
	 * This method can be called statically
	 * 
	 * @access public
	 * @param integer $code
	 * 		The status code, e.g. 404
	 * @return void
	 */
	public static function sendHttpStatus($code) {
		header(' ', TRUE, $code);
	}
	
	/**
	 * Send the headers for a download
	 * 
	 * This method will send the headers to force the download of a 
	 * given file. Typically, this will force a download dialog box to 
	 * pop up in the client's browser.
	 * 
	 * NOTE:
	 * This method can be called statically
	 * 
	 * @static
	 * @access public
	 * @author http://be.php.net/manual/en/function.header.php#48538
	 * @author Tom Bauwens
	 * @param M_File $file
	 * 		The file to send the download for
	 * @param string $filename
	 *		The filename to output
	 * @param bool $useCache
	 *		Use caching (and send 301 headers) to avoid unnecessary bandwith
	 * @return void
	 */
	public static function sendDownload(M_File $file, $filename = NULL, $useCache = true) {
		@ob_clean();

		// Begin writing headers
	    header('Pragma: public');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

	    header('Cache-Control: public');
	    header('Content-Description: File Transfer');

	    // Send Content-Type, corresponding to the file:
	    header('Content-Type: ' . self::_getContentTypeFromFile($file));

		// Send a last modified header, so we can enable caching
		header('Last-Modified: '.gmdate('D, d M Y H:i:s', $file->getLastModifiedDate()->getTimestamp()).' GMT', true, 200);

		//check if page has changed. If not, send 304 and exit
		if ($useCache && self::isCached($file))
		{
			header("HTTP/1.1 304 Not Modified");
			exit;
		}

	    // Force the download
	    header('Content-Disposition: attachment; filename='. ($filename == NULL ? $file->getBasename() : $filename) .';');
	    header('Content-Transfer-Encoding: binary');
	    header('Content-Length: ' . $file->getSize());
	    @readfile($file->getPath());
	    exit;
	}

	/**
	 * Check if a file is cached
	 *
	 * Use {@link M_Server::getIfModifiedSince()} and
	 * {@link M_Server::getIfNoneMatch()} to check if the file has been changed.
	 *
	 * If not we will return TRUE, so we can prevent output and reduce bandwith
	 *
	 * @author Ben Brughmans
	 * @param M_File $file
	 * @return bool
	 */
	public static function isCached(M_File $file) {
		//get the last-modified-date of this very file
		$lastModified = $file->getLastModifiedDate()->getTimestamp();

		//get a unique hash of this file (etag)
		$etagFile = md5_file($file->getPath());

		//check if page has changed: compare server-variables
		if (@strtotime(M_Server::getIfModifiedSince()) == $lastModified ||
			M_Server::getIfNoneMatch() == $etagFile)
		{
			return true;
		}

		return false;
	}
	
	/**
	 * Send a string as a download
	 * 
	 * This method will send the headers to force the download of a given variable's
	 * content (string). Typically, this will force a download dialog box to pop 
	 * up in the client's browser.
	 * 
	 * Note that the string is sent to the browser, as if it were a file. For
	 * that reason, you also need to provide an instance of {@link M_File}, in
	 * order to provide with the file's properties.
	 * 
	 * Example 1, send a download of a text:
	 * <code>
	 *    $text = 'Hello world';
	 *    M_Header::sendDownloadFromString($text, new M_File('my-filename.txt'));
	 * </code>
	 * 
	 * NOTE:
	 * This method can be called statically
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string to be made available as download
	 * @param M_File $file
	 * 		The download is sent as a file. The properties of the file to be
	 * 		sent, is provided by an instance of {@link M_File}
	 * @return void
	 */
	public static function sendDownloadFromString($string, M_File $file) {
		// clean the output buffer, if buffering the output. We do
		// this in order to prevent a corrupted file.
		@ob_clean();
		
		// Begin writing headers
	    header('Pragma: public');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Cache-Control: public');
	    header('Content-Description: File Transfer');
	   
	    // Send Content-Type, corresponding to the file:
	    header('Content-Type: ' . self::_getContentTypeFromFile($file));
		
	    // Force the download
	    header('Content-Disposition: attachment; filename='. $file->getBasename() .';');
	    header('Content-Transfer-Encoding: binary');
	    header('Content-Length: ' . strlen($string));
	    echo $string;
	    exit;
	}
	
	/**
	 * Send Content-Type header by file
	 *
	 * @param MI_FsItemFile $file
	 * @return void
	 * @author b.brughmans
	 */
	public static function sendContentTypeByFile(MI_FsItemFile $file) {
		 header('Content-Type: ' . self::_getContentTypeFromFile($file));
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get Content-Type value, from file
	 * 
	 * Used by
	 * 
	 * - {@link M_Header::sendDownload()}
	 * - {@link M_Header::sendDownloadFromString()}
	 * 
	 * To generate the correct Content-Type header value.
	 * 
	 * @access private
	 * @param MI_FsItemFile $file
	 * 		The file, for which to determine the Content-Type value
	 * @return string
	 */
	private static function _getContentTypeFromFile(MI_FsItemFile $file) {
		switch(strtolower($file->getFileExtension())) {
			case 'pdf':
				return 'application/pdf';
			
			case 'exe':
				return 'application/octet-stream';
			
			case 'zip':
				return 'application/zip';
			
			case 'doc':
				return 'application/msword';
			
			case 'xls':
				return 'application/vnd.ms-excel';
			
			case 'ppt':
				return 'application/vnd.ms-powerpoint';
			
			case 'gif':
				return 'image/gif';
			
			case 'png':
				return 'image/png';
			
			case 'jpeg':
			case 'jpg':
				return 'image/jpg';
			
			case 'mp3':
				return 'audio/mpeg';
			
			case 'wav':
				return 'audio/x-wav';
			
			case 'mpeg':
			case 'mpg':
			case 'mpe':
				return 'video/mpeg';
			
			case 'mov':
				return 'video/quicktime';
			
			case 'avi':
				return 'video/x-msvideo';
			
			case 'htm':
			case 'html':
				return 'text/html';
			
			case 'xml':
				return 'text/html';
			
			case 'txt':
				return 'application/txt';
			
			// By default, we try to force a download dialog
			default:
				return 'application/force-download';
		}
	}
}