<?php
/**
 * M_Search class
 * 
 * M_Search is used both to index content and to search for content.
 * It provides with tools to clean up text, separate search terms, 
 * etc.
 * 
 * @package Core
 */
class M_Search extends M_Object {
	/**
	 * Clean text
	 * 
	 * This method will clean a given text, so it can be indexed.
	 * To clean the provided text, this method does the following:
	 * 
	 * - Remove accents (see {@link M_FilterTextUTF8Accents})
	 * - Remove punctuation (see {@link M_FilterTextPunctuation})
	 * - Remove stopwords (see {@link M_FilterTextStopwords})
	 * - Replace synonyms (see {@link M_FilterTextSynonyms})
	 * 
	 * To clean up a text, locale dependent values are used (such as
	 * stopwords and synonyms). To set the locale in which the text
	 * should be cleaned up, provide the locale name as the second 
	 * argument.
	 * 
	 * If the locale has not been provided, this method will use the
	 * locale name that has been set in the locale category LC_MESSAGES:
	 * See {@link M_Locale::getCategory()}. Note that, if no locale
	 * has been set at all, this method will use "en" as default value.
	 * 
	 * TODO: stopwords & synonyms
	 * 
	 * @access public
	 * @param string $text
	 * 		The text to be cleaned up
	 * @param string $locale
	 * 		The name of the locale from which to fetch indexing 
	 * 		rules (stopwords and synonyms).
	 * @return string
	 */
	public function getIndex($text, $locale = NULL) {
		// Set locale, if not provided
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
			if(!$locale) {
				$locale = 'en';
			}
		}
		
		// Set the chain of filters that is to be applied on the 
		// provided text.
		$decorator = new M_Decorator(new M_FilterTextValue($text));
		$decorator->addDecorator('M_FilterTextUTF8Accents');
		$decorator->addDecorator('M_FilterTextPunctuation');
		//$decorator->addDecorator('M_FilterTextStopwords');
		//$decorator->addDecorator('M_FilterTextSynonyms');
		return $decorator->getChain()->apply();
	}
	
	/**
	 * Get search terms
	 * 
	 * This method will extract the search parts out of the provided
	 * search expression. To specify how words should be searched for,
	 * the user can use the following tools:
	 * 
	 * <code>"</code>
	 * 
	 * Double quotes enclose a search phrase. Search phrases are not
	 * exploded into separate words, and are interpreted as a search
	 * term.
	 * 
	 * Also, the user can use the following boolean operators, to 
	 * include (AND) or exclude (NOT) search terms from the results:
	 * 
	 * <code>+</code>
	 * 
	 * Use the boolean AND operator
	 * 
	 * <code>-</code>
	 * 
	 * Use the boolean NOT operator
	 * 
	 * The return value of this method is an array. This array will
	 * have the following format:
	 * 
	 * <code>
	 *    Array (
	 *       0 => Array (
	 *          [term] => (string)
	 *          [phrase] => (boolean)
	 *          [boolean] => AND|OR
	 *          [operator] => (stirng)   // Possible values: = !=
	 *       ),
	 *       ...
	 *    )
	 * </code>
	 * 
	 * Example 1
	 * <code>
	 *    print_r(M_Search::getSearchTerms('"Tom Bauwens" is here');
	 * </code>
	 * 
	 * Example 1 will output the following array:
	 * <code>
	 *    Array (
	 *       0 => Array (
	 *          [term] => Tom Bauwens
	 *          [phrase] => TRUE
	 *          [boolean] => AND
	 *          [operator] => =
	 *       ),
	 *       1 => Array (
	 *          [term] => is
	 *          [phrase] => FALSE
	 *          [boolean] => AND
	 *          [operator] => =
	 *       ),
	 *       2 => Array (
	 *          [term] => here
	 *          [phrase] => FALSE
	 *          [boolean] => AND
	 *          [operator] => =
	 *       )
	 *    )
	 * </code>
	 * 
	 * IMPORTANT NOTE!
	 * To clean up the search expression, you should use the method
	 * {@link M_Search::getIndex()} on each of the search terms that 
	 * are returned by this method. If you have applied the method 
	 * {@link M_Search::getIndex()} on the original search expression
	 * passed in to this method, this method may procude unexpected 
	 * results.
	 * 
	 * @access public
	 * @param string $expression
	 * 		The search expression from which to extract the search 
	 * 		terms.
	 * @return array
	 */
	public function getSearchTerms($expression) {
		$out = array();
		
		// Remove redundant white-spaces and trim the search expression
		$expression = trim(preg_replace('/[\s]+/m', ' ', $expression));
		
		// Browse the text by phrases
		$term = explode('"', $expression);
		$phrase = (substr($expression,0,1) == '"');
		for($i = 0, $n = count($term); $i < $n; $i ++) {
			$term[$i] = trim($term[$i]);
			if(!empty($term[$i])) {
				if($phrase) {
					$out[] = array(
						'term'     => $term[$i],
						'phrase'   => TRUE,
						'boolean'  => 'AND',
						'operator' => '='
					);
				} else {
					foreach(explode(' ', $term[$i]) as $word) {
						$out[] = array(
							'term'     => $word,
							'phrase'   => FALSE,
							'boolean'  => 'AND',
							'operator' => '='
						);
					}
				}
				
				$phrase = !$phrase;
			}
		}
		
		// return the array of search terms
		return $out;
	}
	
	/**
	 * Get select
	 * 
	 * This method will use {@link M_Search::getSearchTerms()} to 
	 * extract the search terms from the provided search expression, 
	 * and {@link M_Search::getIndex()} to clean up each of the 
	 * extracted search terms.
	 * 
	 * Then, it will add the conditions to the {@link MI_DbSelect}
	 * that has been passed in. The select can then be used to search
	 * for entries that match the search terms in the expression.
	 * 
	 * Note that you can provide the locale name (optionally) to this 
	 * method. For more info about the purpose of the locale name,
	 * read the docs on {@link M_Search::getIndex()}.
	 * 
	 * Example 1
	 * Assume we have a database table "news" that stores the news
	 * articles. "title" and "text" are the table columns that store
	 * the title and the body of the news article respectively.
	 * 
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    // search for "Multimedium Rocks" as a phrase, in the news
	 *    // table
	 *    $select = M_Search::getSelect(
	 *       $db->select('news'), 
	 *       array('title', 'text'), 
	 *       '"Multimedium Rocks"'
	 *    );
	 *    // Output the select:
	 *    echo $select->toString();
	 * </code>
	 * 
	 * Example 1 will output the following output:
	 * 
	 * <code>
	 *    SELECT *
	 *    FROM `news` 
	 *       WHERE ( 
	 *          `title` LIKE "%Multimedium Rocks%" 
	 *          OR `text` LIKE "%Multimedium Rocks%" 
	 *       )
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * This method should be used to search a database table that
	 * stores INDEXED content, because the search expression will be
	 * cleaned in order to match cleaned content.
	 * 
	 * @access public
	 * @uses M_Search::getIndex()
	 * @uses M_Search::getSearchTerms()
	 * @param MI_DbSelect $select
	 * 		The select; an object that implements {@link MI_DbSelect}
	 * @param array $columns
	 * 		The collection of columns that should contain the terms
	 * 		in the search expression
	 * @param string $expression
	 * 		The search expression, as you would provide to 
	 * 		{@link M_Search::getSearchTerms()}.
	 * @param string $locale
	 * 		The name of the locale. See {@link M_Search::getIndex()}.
	 * @return MI_DbSelect
	 * 		The select, containing the search condition(s)
	 */
	public function getSelect(MI_DbSelect $select, array $columns, $expression, $locale = NULL) {
		// Set locale, if not provided
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
			if(!$locale) {
				$locale = 'en';
			}
		}
		
		// Extract the search terms from the provided search expression
		$terms = self::getSearchTerms($expression);
		
		// get the database object from the select:
		$db = $select->getDb();
		
		// Clean up the extracted search terms, and add the condition
		// to the select:
		for($i = 0, $n = count($terms); $i < $n; $i ++) {
			$term = self::getIndex($terms[$i]['term'], $locale);
			
			// Compose the condition
			$sql = '';
			foreach($columns as $column) {
				if(!empty($sql)) {
					$sql .= ' OR ';
				}
				$sql .= $db->quoteIdentifier($column);
				$sql .= ' LIKE ';
				$sql .= $db->quote('%' . $term . '%');
			}
			
			// Add the condition to the select:
			$select->where($sql);
		}
		
		// return the final select:
		return $select;
	}
}