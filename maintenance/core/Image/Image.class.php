<?php
/**
 * M_Image
 * 
 * The M_Image class is an extension of the {@link M_File} class. It adds 
 * additional information methods, to retrieve information about an image
 * file (width, height, exif, ...)
 * 
 * To perform operations on image files (e.g. resizing), you should have
 * a look at the MI_ImageResource interface and its concrete implementations
 * (such as M_ImageResourceGd). Probably, you won't even have to directly
 * interact with an image resource. Most of the time, the M_FilterImageDecorators
 * will do the job.
 * 
 * @package Core
 */
class M_Image extends M_File {
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type. Typically,
	 * this constant is used to check the image's type as following:
	 * 
	 * Example 1
	 * <code>
	 *    // Construct an image:
	 *    $image = new M_Image('files/myImage.jpg);
	 *    
	 *    // Check if the image is a JPEG:
	 *    if($image->getType() == M_Image::TYPE_JPEG) {
	 *       echo 'Yes, the image is a JPEG Image';
	 *    }
	 * </code>
	 * 
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_JPEG = 'jpeg';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_JPEG2000 = 'jpeg2000';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_GIF = 'gif';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_BMP = 'bmp';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_IFF = 'iff';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_JB2 = 'jb2';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_JP2 = 'jp2';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_JPC = 'jpc';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_JPX = 'jpx';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_PNG = 'png';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_PSD = 'psd';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_SWC = 'swc';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_SWF = 'swf';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_TIFF_II = 'tiff_II';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_TIFF_MM = 'tiff_MM';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_WBMP = 'wbmp';
	
	/**
	 * Image Type Constant
	 * 
	 * This constant is used to describe an image type.
	 * 
	 * @see M_Image::TYPE_JPEG
	 * @see M_Image::getType()
	 * @var string
	 */
	const TYPE_XBM = 'xbm';
	
	/**
	 * Image info
	 * 
	 * This property stores extended information about the image, and
	 * is populated by {@link M_Image::_getImageInfo()}
	 * 
	 * @access private
	 * @var array
	 */
	private $_imageInfo;
	
	/**
	 * Constructor
	 * 
	 * Note that the Image constructor accepts the same (optional)
	 * path parameter as the {@link M_File} constructor does.
	 * 
	 * @access public
	 * @return Image
	 */
	public function __construct($path = NULL) {
		parent::__construct($path);
	}
	
	/**
	 * Get image dimensions
	 *
	 * @access public
	 * @return array $dimensions
	 * 		Returns an array, in which the width is stored at index 0,
	 * 		and the height at index 1. Eg: [250, 340]
	 */
	public function getDimensions() {
		// Get the image's information:
		$info = $this->_getImageInfo();
		
		// Return the image's dimensions:
		return array($info[0], $info[1]);
	}
	
	/**
	 * Get width
	 * 
	 * @access public
	 * @return integer
	 */
	public function getWidth() {
		// Get the image's dimensions:
		$tmp = $this->getDimensions();
		
		// Return the width:
		return $tmp[0];
	}
	
	/**
	 * Get height
	 * 
	 * @access public
	 * @return integer
	 */
	public function getHeight() {
		// Get the image's dimensions:
		$tmp = $this->getDimensions();
		
		// Return the height:
		return $tmp[1];
	}
	
	/**
	 * Get Image type
	 * 
	 * Will provide with the image's type. Note that this method is
	 * different from {@link M_File::getFileExtension()}, in the sense
	 * that this method checks the image's type, by reading the image's
	 * headers (from the file).
	 * 
	 * @access public
	 * @return string $type
	 * 		The image type
	 */
	public function getImageType() {
		// Get the image's info
		$info = $this->_getImageInfo();
		
		// Map PHP's built-in constants to M_Image constants:
		$map = array(
			IMAGETYPE_BMP      => self::TYPE_BMP,
			IMAGETYPE_GIF      => self::TYPE_GIF,
			IMAGETYPE_IFF      => self::TYPE_IFF,
			IMAGETYPE_JB2      => self::TYPE_JB2,
			IMAGETYPE_JP2      => self::TYPE_JP2,
			IMAGETYPE_JPC      => self::TYPE_JPC,
			IMAGETYPE_JPEG     => self::TYPE_JPEG,
			IMAGETYPE_JPEG2000 => self::TYPE_JPEG2000,
			IMAGETYPE_JPX      => self::TYPE_JPX,
			IMAGETYPE_PNG      => self::TYPE_PNG,
			IMAGETYPE_PSD      => self::TYPE_PSD,
			IMAGETYPE_SWC      => self::TYPE_SWC,
			IMAGETYPE_SWF      => self::TYPE_SWF,
			IMAGETYPE_TIFF_II  => self::TYPE_TIFF_II,
			IMAGETYPE_TIFF_MM  => self::TYPE_TIFF_MM,
			IMAGETYPE_WBMP     => self::TYPE_WBMP,
			IMAGETYPE_XBM      => self::TYPE_XBM
		);
		
		// Check if we can recognize the image's type:
		if(! isset($map[$info[2]])) {
			// If not, we throw an exception:
			throw new M_ImageException(sprintf(
				'Cannot retrieve Image Type of %s; Did not recognize image type number %s',
				$this->getPath(),
				$info[2]
			));
		}
		
		// Return the image's type:
		return $map[$info[2]];
	}
	
	/**
	 * Get MIME type
	 * 
	 * @access public
	 * @return string $type
	 * 		The mime type
	 */
	public function getMimeType() {
		// Get the image's info
		$info = $this->_getImageInfo();
		
		// Check if we can find the image's mime type:
		if(! isset($info['mime'])) {
			// If not, we throw an exception:
			throw new M_ImageException(sprintf(
				'Cannot find Image MIME Type of %s',
				$this->getPath()
			));
		}
		
		// Return the image's mime type:
		return $info['mime'];
	}
	
	/**
	 * Get EXIF Data
	 * 
	 * Will provide with the EXIF data that is available in the image.
	 * Note that, in order for this method to function properly, you
	 * need to have the exif extension installed. This method requires
	 * {@link exif_read_data()}.
	 * 
	 * Example 1
	 * <code>
	 *    // Construct the image
	 *    $image = new M_Image('files/myImage.jpg');
	 *    
	 *    // Read the EXIF data:
	 *    foreach ($image->getExifData() as $key => $section) {
	 *       foreach ($section as $name => $val) {
	 *          echo "$key.$name: $val<br />\n";
	 *       }
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @return array
	 */
	public function getExifData() {
		// Make sure the image exists! Throw an exception if not
		if(!is_file($this->_path)) {
			throw new M_FsException(sprintf(
				'EXIF not available; Reason: Could not locate the image file "%s"', 
				$this->_path
			));
		}
		
		// Make sure the exif functions are available! Throw an exception
		// if not
		if(!function_exists('exif_read_data')) {
			throw new M_FsException(sprintf(
				'EXIF extensions are not available to read EXIF data from image "%s".', 
				$this->_path
			));
		}
		
		// Check the image's extension against the file-extensions that 
		// support EXIF data. Throw an exception if this image does
		// not support EXIF data.
		if(!in_array(strtolower($this->getFileExtension()), array('jpg', 'jpeg', 'tif', 'tiff'))) {
			throw new M_FsException('EXIF data is supported only in JPG and TIFF images.');
		}
		
		// Read the EXIF data on the JPG/TIFF image, and return it:
		return exif_read_data($this->_path);
	}
	
	/**
	 * Get IPTC
	 * 
	 * Will provide with whatever information is available under the
	 * IPTC specification. Go to {@link http://www.iptc.org} for more
	 * info
	 * 
	 * @access public
	 * @return 
	 */
	public function getIPTC() {
		// Prepare empty APP Marker:
		$appMarker = NULL;
		
		// Get the image info:
		$info = $this->_getImageInfo();
		
		// First, check if the binary APP13 marker is available
		if(! isset($info['APP13'])) {
			// If not, we throw an exception
			throw new M_ImageException(sprintf(
				'Cannot retrieve IPTC information from image %s',
				$this->getPath()
			));
		}
		
		// Parse the binary data into something comprehensible:
		return iptcparse($appMarker);
	}
	
	/**
	 * Get number of color channels
	 * 
	 * Will provide with the number of color channels in the image.
	 * This helps in determining whether or not the image is 
	 * 
	 * - RGB (3 channels), or
	 * - CMYK (4 channels)
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfColorChannels() {
		// Get the image info:
		$info = $this->_getImageInfo();
		
		// Check if the number of channels is available:
		if(! isset($info['channels'])) {
			// Throw an exception, if not:
			throw new M_ImageException(sprintf(
				'Number of Color Channels cannot be retrieved from image %s',
				$this->getPath()
			));
		}
		
		// return the number of channels:
		return (int) $info['channels'];
	}
	
	/**
	 * Is RGB?
	 * 
	 * Will determine whether or not the image is RGB. This method
	 * will use {@link M_Image::getNumberOfColorChannels()} to do so.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the image is RGB, FALSE if not
	 */
	public function isRGB() {
		return ($this->getNumberOfColorChannels() == 3);
	}
	
	/**
	 * Is CMYK?
	 * 
	 * Will determine whether or not the image is CMYK. This method
	 * will use {@link M_Image::getNumberOfColorChannels()} to do so.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the image is CMYK, FALSE if not
	 */
	public function isCMYK() {
		return ($this->getNumberOfColorChannels() == 4);
	}
	
	/**
	 * Get bits per color
	 * 
	 * Will provide with the number of bits per color
	 * 
	 * NOTE (php.net):
	 * For some image types, the presence of channels and bits values 
	 * can be a bit confusing. As an example, GIF always uses 3 channels 
	 * per pixel, but the number of bits per pixel cannot be calculated 
	 * for an animated GIF with a global color table. 
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfBitsPerColor() {
		// Get the image info:
		$info = $this->_getImageInfo();
		
		// Check if the number of bits is available:
		if(! isset($info['bits'])) {
			// Throw an exception, if not:
			throw new M_ImageException(sprintf(
				'Number of Bits per Color cannot be retrieved from image %s',
				$this->getPath()
			));
		}
		
		// return the number of channels:
		return (int) $info['bits'];
	}
	
	/**
	 * Get image resource
	 * 
	 * This method will provide with an image resource, which is 
	 * represented by an instance of {@link M_ImageResourceGd}, on
	 * which graphical operations can be performed.
	 * 
	 * @access public
	 * @return M_ImageResourceGd
	 */
	public function getResourceGd() {
		return new M_ImageResourceGdFromFile($this);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Image info
	 * 
	 * This protected method will provide with extended information 
	 * about the image. This method is used by
	 * 
	 * - {@link M_Image::getDimensions()}
	 * - {@link M_Image::getWidth()}
	 * - {@link M_Image::getHeight()}
	 * - {@link M_Image::getNumberOfColorChannels()}
	 * - {@link M_Image::isRGB()}
	 * - {@link M_Image::isCMYK()}
	 * - etc...
	 * 
	 * In order to get information about the image
	 * 
	 * @access protected
	 * @return array
	 */
	protected function _getImageInfo() {
		// If the image info has not yet been requested:
		if(! $this->_imageInfo) {
			// If the image does not exists, we throw an exception:
			if(! $this->exists()) {
				throw new M_FsException(sprintf(
					'Cannot retrieve Image Info; cannot locate file %s',
					$this->getPath()
				));
			}
			// We get the image's size. Note that getimagesize() will
			// do much more for us, if we want to. We also provide with
			// an array that is to be populated with extended info
			// about the image:
			$extendedInfo = array();
			$tmp = getimagesize($this->getPath(), $extendedInfo);
			
			// If we failed to retrieve the image's size, we throw an
			// exception to inform about the error
			if(! $tmp) {
				throw new M_ImageException(sprintf(
					'Cannot retrieve information about the image (%s) with getimagesize()',
					$this->getPath()
				));
			}
			
			// Now, we store the information we have achieved in the
			// internal property:
			$this->_imageInfo = array_merge($tmp, $extendedInfo);
		}
		
		// Return the image info:
		return $this->_imageInfo;
	}
}