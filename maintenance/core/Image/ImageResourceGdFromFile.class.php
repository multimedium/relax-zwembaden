<?php
/**
 * M_ImageResourceGdFromFile class
 * 
 * This class can be used to create a new image resource out of an
 * existing image file. Note that this class, being a subclass of 
 * {@link M_ImageResourceGd}, also is an implementation based on the
 * GD Library.
 * 
 * @package Core
 */
class M_ImageResourceGdFromFile extends M_ImageResourceGd {
	/**
	 * Constructor
	 *
	 * @access public
	 * @param M_Image $image
	 * 		The image out of which to create a new image resource
	 * @return M_ImageResourceFromFile
	 */
	public function __construct(M_Image $image) {
		$this->_resource = $this->_getResource($image);
		$dim = $image->getDimensions();
		$this->_width = $dim[0];
		$this->_height = $dim[1];
	}
}