<?php
/**
 * M_ImageException class
 * 
 * Exceptions thrown by {@link M_Image}, {@link M_ImageResourceGd} 
 * and related classes.
 *
 * @package Core
 */
class M_ImageException extends M_Exception {
}
?>