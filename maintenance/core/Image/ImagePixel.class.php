<?php
/**
 * M_ImagePixel class
 * 
 * The M_ImagePixel class is used to describe a pixel. A pixel contains
 * the following information:
 * 
 * - X Coordinate
 * - Y Coordinate
 * - Color (represented by an instance of {@link M_Color})
 * 
 * @package Core
 */
class M_ImagePixel implements MI_ImagePixel {
	/**
	 * X Coordinate
	 * 
	 * This property stores the X coordinate of the pixel.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_x;
	
	/**
	 * Y Coordinate
	 * 
	 * This property stores the Y coordinate of the pixel.
	 * 
	 * @access private
	 * @var integer
	 */
	private $_y;
	
	/**
	 * Color
	 * 
	 * This property stores the color of the pixel. Note that the color
	 * is represented by an instance of {@link M_Color}.
	 * 
	 * @access private
	 * @var M_Color
	 */
	private $_color;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param integer $x
	 * 		The X Coordinate of the pixel
	 * @param integer $y
	 * 		The Y Coordinate of the pixel
	 * @param M_Color $color
	 * 		The color of the pixel
	 * @return M_ImagePixel
	 */
	public function __construct($x, $y, M_Color $color) {
		$this->_x = $x;
		$this->_y = $y;
		$this->_color = $color;
	}
	
	/**
	 * Get X and Y Coordinates
	 * 
	 * Will return both the X and Y Coordinates of the pixel. Typical
	 * usage example:
	 * 
	 * <code>
	 *    $pixel = new M_ImagePixel(1, 1, new M_Color(array(255, 255, 255)));
	 *    list($x, $y) = $pixel->getXY();
	 * </code>
	 *
	 * @access public
	 * @return array
	 */
	public function getXY() {
		return array($this->_x, $this->_y);
	}
	
	/**
	 * Get X Coordinate
	 *
	 * @access public
	 * @return integer
	 */
	public function getX() {
		return $this->_x;
	}
	
	/**
	 * Get Y Coordinate
	 *
	 * @access public
	 * @return integer
	 */
	public function getY() {
		return $this->_y;
	}
	
	/**
	 * Get color
	 *
	 * @access public
	 * @return M_Color
	 */
	public function getColor() {
		return $this->_color;
	}
	
	/**
	 * Set color
	 * 
	 * @access public
	 * @param M_Color $color
	 * 		The new color of the pixel
	 * @return void
	 */
	public function setColor(M_Color $color) {
		$this->_color = $color;
	}
	
	/**
	 * Set X Coordinate
	 * 
	 * @access public
	 * @param integer $x
	 * 		The new X Coordinate of the pixel
	 * @return void
	 */
	public function setX($x) {
		$this->_x = $x;
	}
	
	/**
	 * Set Y Coordinate
	 * 
	 * @access public
	 * @param integer $y
	 * 		The new Y Coordinate of the pixel
	 * @return void
	 */
	public function setY($y) {
		$this->_y = $y;
	}
}
?>