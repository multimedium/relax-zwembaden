<?php
/**
 * M_FeedRss
 * 
 * M_FeedRss is a specific implementation of {@link MI_Feed} that supports
 * RSS Feeds
 * 
 * @package Core
 */
class M_FeedRss extends M_Feed implements MI_Feed {
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Construct with file (xml)
	 * 
	 * @uses M_FeedRss::constructWithString()
	 * @access public
	 * @param M_File $file
	 * 		The XML file
	 * @return M_FeedRss
	 */
	public static function constructWithFile(M_File $file) {
		// In order to construct a feed from a file, we use the constructWithString()
		// method, and provide it with the file's source code:
		return self::constructWithString($file->getContents());
	}
	
	/**
	 * Construct with string (source code)
	 * 
	 * @access public
	 * @param string $xmlFeedSourceCode
	 * 		The XML Source Code of the feed
	 * @return M_FeedRss
	 */
	public static function constructWithString($xmlFeedSourceCode) {
		// Parse the XML Feed into a SimpleXmlElement object:
		$xml = @simplexml_load_string($xmlFeedSourceCode);
		
		// If we failed to parse:
		if(! $xml) {
			// Throw new exception
			throw new M_FeedException(sprintf(
				'Cannot parse XML from the string %s',
				$xmlFeedSourceCode
			));
		}
		
		// Check if the channel has been defined:
		$c = (
			// - It must exist
			isset($xml->channel) && 
			// - It must be a SimpleXmlElement object
			M_Helper::isInstanceOf($xml->channel, 'SimpleXmlElement') &&
			// - It must contain at least 1 node
			count($xml->channel->children()) > 0
		);
		
		// If not so:
		if(! $c) {
			// Throw new exception
			throw new M_FeedException(sprintf(
				'Cannot construct an instance of %s from string; Cannot find CHANNEL in source code',
				__CLASS__
			));
		}
		
		// Construct the feed:
		$feed = new self;
		
		// Check if a title has been provided:
		if(isset($xml->channel->title)) {
			// If so, set the feed's title
			$feed->setTitle((string) $xml->channel->title);
		}
		
		// Check if a description has been provided:
		if(isset($xml->channel->description)) {
			// If so, set the feed's description
			$feed->setDescription((string) $xml->channel->description);
		}
		
		// Check if a link has been provided:
		if(isset($xml->channel->link)) {
			// If so, set the link in the feed
			$feed->setLink(new M_Uri((string) $xml->channel->link));
		}
		
		// Check if items have been provided. To do so, we run an xpath expression
		// on the source code, which should find us all items. For each of the 
		// items:
		foreach($xml->channel->xpath('item') as $item) {
			// Check if the item has been parsed successfully:
			$c = (
				// - It must be a SimpleXmlElement object
				M_Helper::isInstanceOf($item, 'SimpleXmlElement') &&
				// - It must contain at least 1 node
				count($item->children()) > 0
			);
			
			// If not so:
			if(! $c) {
				// Throw new exception
				throw new M_FeedException(sprintf(
					'Cannot construct an instance of M_FeedRssItem from string'
				));
			}
			
			// Construct a feed item:
			$feedItem = new M_FeedRssItem;
		
			// Check if a title has been provided:
			if(isset($item->title)) {
				// If so, set the feed item's title
				$feedItem->setTitle((string) $item->title);
			}
			
			// Check if a description has been provided:
			if(isset($item->description)) {
				// If so, set the feed item's description
				$feedItem->setDescription((string) $item->description);
			}
			
			// Check if a link has been provided:
			if(isset($item->link)) {
				// If so, set the link in the feed item
				$feedItem->setLink(new M_Uri((string) $item->link));
			}
			
			// Check if a date has been provided:
			if(isset($item->pubDate)) {
				// If so, set the link in the feed item
				$feedItem->setPublicationDate(new M_Date((string) $item->pubDate));
			}
			
			// Add the feed item to the feed:
			$feed->addItem($feedItem);
		}
		
		// Return the feed:
		return $feed;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Render the source code
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		$out  = '<?xml version="1.0" encoding="UTF-8"?>';
		$out .= '<rss version="0.92">';
		$out .=    '<channel>';
		
		$temp = $this->getTitle();
		if($temp) {
			$out .=       '<title><![CDATA['. $this->getTitle() .']]></title>';
		} else {
			throw new M_FeedException(sprintf(
				'Please provide a title for the feed, title is mandatory in an RSS-feed in order to be autodetected by all browsers'
			));
		}
		
		$temp = $this->getLink();
		if($temp) {
			$out .=       '<link><![CDATA['. $temp .']]></link>';
		} else {
			throw new M_FeedException(sprintf(
				'Please provide a link for the feed, link is mandatory in an RSS-feed in order to be autodetected by all browsers'
			));
		}
		
		$temp = $this->getDescription();
		if($temp) {
			$out .=       '<description><![CDATA['. $temp .']]></description>';
		} else {
			throw new M_FeedException(sprintf(
				'Please provide a description for the feed, description is mandatory in an RSS-feed in order to be autodetected by all browsers'
			));
		}
		
		foreach($this->getItems() as $item) {
			$out .= $item->toString();
		}
		
		$out .=    '</channel>';
		$out .= '</rss>';
		
		return $out;
	}
	
	/**
	 * Magic toString method
	 * 
	 * @uses M_FeedRss::toString()
	 * @access public
	 * @return string
	 */
	public function __toString() {
		return $this->toString();
	}
}