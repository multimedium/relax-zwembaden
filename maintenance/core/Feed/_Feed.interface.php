<?php
interface MI_Feed {
	public function setTitle($title);
	public function setDescription($description);
	public function setLink(M_Uri $link);
	public function addItem(MI_FeedItem $item);
	public function getTitle();
	public function getDescription();
	public function getLink();
	public function getItems();
	public function toString();
	public function __toString();
}