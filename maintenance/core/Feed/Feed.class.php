<?php
abstract class M_Feed implements MI_Feed {
	protected $_title;
	protected $_description;
	protected $_link;
	protected $_items = array();
	
	public function __construct() {
	}
	
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	public function setDescription($description) {
		$this->_description = (string) $description;
	}
	
	public function setLink(M_Uri $link) {
		$this->_link = $link;
	}
	
	public function addItem(MI_FeedItem $item) {
		$this->_items[] = $item;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function getDescription() {
		return $this->_description;
	}
	
	public function getLink() {
		return $this->_link;
	}
	
	public function getItems() {
		return new ArrayIterator($this->_items);
	}
}