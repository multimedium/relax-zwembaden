<?php
/**
 * M_ViewFilePreview class
 * 
 * M_ViewFilePreview, a subclass of {@link M_View}, is used to render 
 * the "preview" of a given file.
 * 
 * @package Core
 */
class M_ViewFilePreview extends M_ViewCoreHtml {
	/**
	 * The file
	 * 
	 * This property stores the file of which to render a preview. The
	 * file is represented by an object that implements the {@link MI_FsItemFile} 
	 * interface.
	 * 
	 * @see M_ViewFilePreview::setFile()
	 * @access protected
	 * @var MI_FsItemFile
	 */
	protected $_file;
	
	/**
	 * Title
	 * 
	 * This property stores the title of the file preview. Note that
	 * {@link M_ViewFilePreview} will default the value of this property
	 * to the filename; see {@link MI_FsItemFile::getName()}. 
	 * 
	 * @see M_ViewFilePreview::setTitle()
	 * @access protected
	 * @var string
	 */
	protected $_title;
	
	/**
	 * Description
	 * 
	 * This property stores the additional description of the file 
	 * preview.
	 * 
	 * @see M_ViewFilePreview::setDescription()
	 * @access protected
	 * @var string
	 */
	protected $_description;
	
	/**
	 * Link
	 * 
	 * This property stores the link to the file. The file preview does not 
	 * necessarily have to link to the actual file (although it does by default).
	 * 
	 * @see M_ViewFilePreview::setLink()
	 * @access protected
	 * @var string
	 */
	protected $_link;
	
	/**
	 * Create a link
	 *
	 * This property stores wether or not to create a link around the preview.
	 * 
	 * @var bool
	 */
	protected $_createLink = true;
	
	/**
	 * Add a extra class to the preview element
	 *
	 * @var string
	 */
	protected $_class;
	
	/**
	 * Set file
	 * 
	 * This method is used to set the file of which a preview is to be
	 * rendered. The file is represented by an object that implements the 
	 * {@link MI_FsItemFile} interface.
	 * 
	 * @access public
	 * @param M_File $file
	 * 		The file of which to render a preview
	 * @return void
	 */
	public function setFile(MI_FsItemFile $file) {
		$this->_file = $file;
	}
	
	/**
	 * Set name of the file
	 * 
	 * This method is used to set the title of the file preview. Note
	 * that {@link M_ViewFilePreview} will use the filename as title
	 * by default, if no other title has been provided.
	 * 
	 * @access public
	 * @param string $title
	 * 		The title of the file preview
	 * @return void
	 */
	public function setTitle($text) {
		$this->_title = (string) $text;
	}
	
	/**
	 * Set description of the file
	 * 
	 * This method is used to add an additional description to the 
	 * file preview.
	 * 
	 * @access public
	 * @param string $title
	 * 		The title of the file preview
	 * @return void
	 */
	public function setDescription($text) {
		$this->_description = (string) $text;
	}
	
	/**
	 * Set link to file
	 * 
	 * Since the file preview does not necessarily links to the actual
	 * file (although it does by default), you can use this method to 
	 * set the link that is to be used in the file preview.
	 * 
	 * @access public
	 * @param string $link
	 * 		The link
	 * @return void
	 */
	public function setLink($link) {
		$this->_link = (string) $link;
	}
	
	/**
	 * Get wether or not a link has to be created
	 * 
	 * We don't always want to create a link on the preview-file, this is why
	 * you can choose if you want to create a link or not. The link itself
	 * can be adjusted by using {@link M_ViewFilePreview::setLink()}
	 * 
	 * @author Ben Brughmans
	 * @return bool
	 */
	public function getCreateLink() {
		return $this->_createLink;
	}
	
	/**
	 * Set wheter or not a link should be created
	 * 
	 * We don't always want to create a link on the preview-file, this is why
	 * you can choose if you want to create a link or not. The link itself
	 * can be adjusted by using {@link M_ViewFilePreview::setLink()}
	 * 
	 * @param bool $_createLink
	 */
	public function setCreateLink($_createLink) {
		$this->_createLink = $_createLink;
	}
	
	/**
	 * Get the additional class name(s)
	 * 
	 * @return string
	 */
	public function getClass() {
		return $this->_class;
	}
	
	/**
	 * Set the additional class name(s)
	 * 
	 * @param string $_class
	 */
	public function setClass($_class) {
		$this->_class = $_class;
	}

	/**
	 * Pre-Processing
	 * 
	 * @see M_ViewHtml
	 * @access protected
	 * @throws M_ViewException
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure a file has been provided
		if(!$this->_file) {
			throw new M_ViewException('Cannot render a file preview; missing file');
		}
		
		// Make sure the file exists:
		if(! $this->_file->exists()) {
			throw new M_ViewException(sprintf('Cannot render a file preview; cannot locate file "%s"', $this->_file->getPath()));
		}
		// Assign the full path to the file
		// (may be overwritten by a link)
		$this->assign('link', !empty($this->_link) 
			? $this->_link 
			: M_Request::getLinkWithoutPrefix(M_Loader::getRelative($this->_file->getPath())));
		
		// Assign the file to the template
		$this->assign('file', $this->_file);
		$this->assign('filesize', M_FsHelper::getFileSizeString($this->_file->getSize()));
		$this->assign('extension', strtolower($this->_file->getFileExtension()));
		$this->assign('filePathRelative', M_Loader::getRelative($this->_file->getPath()));
		
		// Assign title and description to the view:
		$this->assign('title', !empty($this->_title) ? $this->_title : $this->_file->getName());
		$this->assign('description', $this->_description);
	}
	
	/**
	 * Default HTML source code rendering
	 * 
	 * @see M_ViewCoreHtml::getHtml()
	 * @access protected
	 * @return string
	 */
	protected function getHtml() {
		// Start container
		$html  = '<div class="file-preview" id="' . $this->getId() . '">';
		
		// How the HTML is rendered, depends on the file type (extension):
		switch($this->getVariable('extension')) {
			case 'jpg':
			case 'jpeg':
			case 'gif':
			case 'png':
				//create a link only if necesarry
				if ($this->getCreateLink()) {
					$html .= '<a href="'. $this->getVariable('link') .'">';
				}
				
				$html .= '<img class="'.$this->getClass().'" src="'. M_Request::getLink($this->_file->getPath()) .'" alt="'. htmlentities($this->getVariable('title')) .'" border="0" />';
		
				//create a link only if necesarry
				if ($this->getCreateLink()) {
					$html .= '</a>';
				}
				
				if($this->_description) {
					$html .= '<p>';
					$html .=    $this->_description;
					$html .= '</p>';
				}
				break;
			
			case 'doc':
			case 'docx':
			case 'xls':
			case 'csv':
			case 'pdf':
			case 'zip':
			case 'rar':
			default:
				$html .= '<div class="file-'. $this->getVariable('extension') .'">';
				$html .=    '<a href="'. $this->getVariable('link') .'">';
				$html .=       $this->getVariable('title');
				$html .=    '</a>';
				$html .= '</div>';
				
				if($this->_description) {
					$html .= '<p>';
					$html .=    $this->_description;
					$html .= '</p>';
				}
				break;
		}
		
		// Finish container:
		$html .= '</div>';
		
		// return final render:
		return $html;
	}
	
	/**
	 * Get template
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return $this->_getResourceFromModuleOwner('core/FilePreview.tpl');
	}
}
?>