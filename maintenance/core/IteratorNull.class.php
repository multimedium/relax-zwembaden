<?php
/**
 * M_IteratorNull class
 * 
 * The Null Iterator is used to return an empty collection of items. Typically,
 * this is used in the Composite Pattern (e.g. Filesystem; {@link MI_FsItem}).
 * 
 * @package Core
 */
class M_IteratorNull extends M_Object implements Countable, Iterator {
	/**
	 * Constructor
	 * 
	 * @access public
	 * @returns M_IteratorNull
	 */
	public function __construct() {
	}
	
	/**
	 * Support for the Countable interface
	 * 
	 * In the case of a Null Iterator, this method always returns 0 (ZERO), 
	 * because we're dealing with an empty collection of items.
	 *
	 * @access public
	 * @return integer
	 * 		The item count
	 */
	public final function count() {
		return 0;
	}
	
	/**
	 * Support for the Iterator interface: Get current item
	 * 
	 * In the case of a Null Iterator, this method always returns NULL, because
	 * we're dealing with an empty collection of items.
	 *
	 * @access public
	 * @return mixed
	 * 		The current item in stored data
	 */
	public final function current() {
		return NULL;
	}
	
	/**
	 * Support for the Iterator interface: Get current key
	 * 
	 * In the case of a Null Iterator, this method always returns NULL, because
	 * we're dealing with an empty collection of items.
	 *
	 * @access public
	 * @return string
	 * 		The key of the current item in stored data
	 */
	public final function key() {
		return NULL;
	}
	
	/**
	 * Support for the Iterator interface: Move to next
	 *
	 * Moves the internal cursor to the next item in stored data. In the case of
	 * a Null Iterator, this method does not do anything, because we're dealing 
	 * with an empty collection of items.
	 * 
	 * @access public
	 * @return void
	 */
	public final function next() {
	}
	
	/**
	 * Support for the Iterator interface: Rewind
	 * 
	 * Resets the internal cursor for iteration. foreach() calls on an iterator
	 * will call the rewind() method, to start looping at the beginning. In the 
	 * case of a Null Iterator, this method does not do anything, because we're
	 * dealing with an empty collection of items.
	 *
	 * @access public
	 * @return void
	 */
	public final function rewind() {
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Will indicate if the end of the iterator has been reached. In the case of 
	 * a Null Iterator, this method always returns FALSE, because we're dealing
	 * with an empty collection.
	 * 
	 * @return boolean
	 * 		Will return TRUE if not reached the end of the iterator, or FALSE if 
	 * 		all items in the iterator have been accessed through the loop.
	 */
	public final function valid() {
		return FALSE;
	}
}