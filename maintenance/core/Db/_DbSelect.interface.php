<?php
/**
 * Select interface
 * 
 * This interface is used for every type select query
 *
 */
interface MI_DbSelect extends MI_DbQuery {
	
	/* -- SQL syntax -- */
	
	public function distinct($column, $table = NULL);
	public function count($field);
	
	/* -- Getters -- */
	public function getFrom();
	public function getColumns();
	public function getJoin();
	public function getGroup();
	public function getHaving();

	/* -- Setters -- */
	public function from($table);
	public function columns(array $columns,$literal = false);
	public function joinInner($table, $tableAlias = null, $on, $condition);
	public function joinLeft($table, $tableAlias = null, $on, $condition);
	public function joinRight($table, $tableAlias = null, $on, $condition);
	public function hasJoin($table = null);
	public function hasJoinInner($table = null);
	public function hasJoinLeft($table = null);
	public function hasJoinRight($table = null);
	public function hasGroup();
	public function group($columns);
	public function having($conditions);

	/* -- Resetters -- */
	public function resetFrom();
	public function resetColumns();
	public function resetGroup();
	public function resetHaving();
	
	/* -- Fetchers -- */
	
	public function getOne();
	public function getAll();

	/* -- Other -- */
	public function merge(MI_DbSelect $select);
}