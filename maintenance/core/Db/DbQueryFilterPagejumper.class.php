<?php
/**
 * M_DbQueryFilterPagejumper class
 *
 * @package Core
 */
class M_DbQueryFilterPagejumper extends M_DbQueryFilter {
	/**
	 * @var M_Pagejumper
	 */
	protected $_pagejumper;

	/**
	 * Construct
	 *
	 * @access public
	 * @param M_Pagejumper $pagejumper
	 * @return M_DbQueryFilterPagejumper
	 */
	public function __construct(M_Pagejumper $pagejumper) {
		$this->_pagejumper = $pagejumper;
	}
	
	/**
	 * Apply the filter
	 *
	 * @param M_DbSelect $query
	 * @return M_DbSelect $query
	 */
	public function filter(M_DbQuery $query) {
		$query->limit(
			($this->_pagejumper->getPage() - 1) * $this->_pagejumper->getNumberOfItemsPerPage(), 
			$this->_pagejumper->getNumberOfItemsPerPage()
		);
		return $query;
	}
	
	/**
	 * Get fields
	 * 
	 * Will always return an empty iterator as a LIMIT filter doesn't filter
	 * on fields
	 * 
	 * @return ArrayIterator
	 */
	public function getFields() {
		return new ArrayIterator();
	}
}