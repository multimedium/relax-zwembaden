<?php
/**
 * M_DbIndex class
 * 
 * This class is used to represent indexes in database tables. Note
 * that this class can be used to represent both primary keys as other
 * indexes.
 * 
 * @package Core
 */
class M_DbIndex implements MI_DbIndex {
	/**
	 * Type constant
	 *
	 * This specific constant is used to address a primary key.
	 * Typically, this constant is used to set the index' type. For 
	 * more info, read {@link M_DbIndex::setType()}.
	 */
	const PRIMARY = 'primary';
	
	/**
	 * Type constant
	 *
	 * This specific constant is used to address an index. Typically, 
	 * this constant is used to set the index' type. For more info, 
	 * read {@link M_DbIndex::setType()}.
	 */
	const INDEX = 'index';
	
	/**
	 * Type constant
	 *
	 * This specific constant is used to address an index. Typically, 
	 * this constant is used to set the index' type. For more info, 
	 * read {@link M_DbIndex::setType()}.
	 */
	const UNIQUE = 'unique';
	
	/**
	 * Index name
	 * 
	 * This property stores the name of the index.
	 *
	 * @access private
	 * @var string
	 */
	private $_name;
	
	/**
	 * Index type
	 * 
	 * This property stores the type of the index. The following class
	 * constants are available to address index types:
	 * 
	 * - {@link M_DbIndex::PRIMARY}
	 * - {@link M_DbIndex::INDEX}
	 *
	 * @access private
	 * @var string
	 */
	private $_type;
	
	/**
	 * Columns
	 * 
	 * This property stores the collection of columns that are present
	 * in the index. Each of the columns is represented by an object
	 * that implements the {@link MI_DbColumn} interface.
	 *
	 * @var array
	 */
	private $_columns;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $name
	 * 		The name of the index
	 * @return M_DbIndex
	 */
	public function __construct($name = NULL) {
		if($name) {
			$this->setName($name);
		}
	}
	
	/**
	 * Get name
	 * 
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Get type
	 * 
	 * @access public
	 * @return string
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Is primary key?
	 * 
	 * @access public
	 * @return bool $flag
	 */
	public function isPrimaryKey() {
		return ($this->getType() == self::PRIMARY);
	}
	
	/**
	 * Is index?
	 * 
	 * @access public
	 * @return bool $flag
	 */
	public function isIndex() {
		return ($this->getType() == self::INDEX);
	}
	
	/**
	 * Is unique index?
	 * 
	 * @access public
	 * @return bool $flag
	 */
	public function isUnique() {
		return ($this->getType() == self::UNIQUE);
	}
	
	/**
	 * Get columns
	 * 
	 * This method will provide with the collection of columns that
	 * together constitute the index.
	 *
	 * @see M_DbIndex::addColumn()
	 * @access public
	 * @return ArrayIterator
	 */
	public function getColumns() {
		return new ArrayIterator($this->_columns);
	}
	
	/**
	 * Set name of the index
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the index
	 * @return void
	 */
	public function setName($name) {
		$this->_name = $name;
	}
	
	/**
	 * Set type of the index
	 * 
	 * This method can be used to set the type of the index. This way,
	 * you can distinguish between primary Keys and other indexes.
	 * 
	 * Example 1, create a primary key:
	 * <code>
	 *    $index = new M_DbIndex();
	 *    $index->setName('PRIMARY');
	 *    $index->setType(M_DbIndex::PRIMARY);
	 * </code>
	 *
	 * NOTE:
	 * An exception is thrown, if the given index type could not have
	 * been recognized.
	 * 
	 * @throws M_DbException
	 * @access public
	 * @param string $type
	 * 		The name of the index
	 * @return void
	 */
	public function setType($type) {
		switch($type) {
			case self::PRIMARY:
			case self::INDEX:
			case self::UNIQUE:
				$this->_type = $type;
				break;
			
			default:
				throw new M_DbException(sprintf('%s: %s: Unrecognized index type "%s"', __CLASS__, __METHOD__, $type));
				break;
		}
	}
	
	/**
	 * Add column to the index
	 * 
	 * This method can be used to add a column to the index. Note that
	 * a column is represented by an object that implements the {@link
	 * MI_DbColumn} interface.
	 *
	 * @access public
	 * @param MI_DbColumn $column
	 * 		The column to be added to the index
	 * @return void
	 */
	public function addColumn(MI_DbColumn $column) {
		$this->_columns[$column->getName()] = $column;
	}
	
	/**
	 * Check if column is in index
	 * 
	 * This method can be used to check whether or not a given column
	 * forms part of the index. Note that a column is represented by an
	 * object that implements the {@link MI_DbColumn} interface.
	 * 
	 * NOTE:
	 * This method returns TRUE if the column is in the index, FALSE
	 * if not!
	 *
	 * @access public
	 * @param MI_DbColumn $column
	 * 		The column to be checked
	 * @return boolean
	 */
	public function containsColumn(MI_DbColumn $column) {
		return isset($this->_columns[$column->getName()]);
	}
}
?>