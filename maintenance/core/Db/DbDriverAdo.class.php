<?php
// Load ADODB
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . FOLDER_THIRDPARTY . DIRECTORY_SEPARATOR . 'adodb' . DIRECTORY_SEPARATOR . 'adodb.inc.php';

// Since we're using ADODB under the hood, we write a generic driver,
// instead of writing a driver per database brand.
class M_DbDriverAdo extends M_DbDriver {
	// This property holds the (fetch) mode in which result sets return
	// record. By default, the fetch mode is set to M_Db::FETCH_BOTH
	private $_fetchMode = M_Db::FETCH_BOTH;
	
	// ADOConnection object
	private $_connection;
	
	public function __construct($driver) {
		$this->_connection =& ADONewConnection($driver);
	}
	
	public function connect($host, $user, $pass, $dbname) {
		// Note that we use NConnect() in order to force a new connection in each
		// separate driver. We do this to make sure that different instances
		// of M_DbDriverAdo do not confuse connections, by re-using Resource IDs
		if(!$this->_connection->NConnect($host, $user, $pass, $dbname)) {
			throw new M_DbException('Could not connect to database.');
		}
		
		//set charset to utf-8
		$this->_connection->Execute("set names 'utf8'");

	}
	
	// Used to retrieve a property from the connection. Note that, if
	// the requested property could not have been found, this method
	// will return FALSE!
	public function getConnectionProperty($property) {
		if(isset($this->_connection->$property)) {
			return $this->_connection->$property;
		}
		return FALSE;
	}
	
	public function getId($sequence = 'dbseq', $startAt = 1) {
		return $this->_connection->GenID($sequence, $startAt);
	}
	
	public function beginTransaction() {
		$this->_connection->BeginTrans();
	}
	
	public function commit() {
		$this->_connection->CommitTrans();
	}
	
	public function rollback() {
		$this->_connection->RollbackTrans();
	}
	
	/**
	 * Get tables
	 * 
	 * This method will return an ArrayIterator that has been populated
	 * with objects of {@link M_DbTableAdo}, each of them representing
	 * a table in the database.
	 * 
	 * NOTE:
	 * The keys of the iterator are the names of the tables.
	 *
	 * @access public
	 * @return M_ArrayIterator
	 */
	public function getTables() {
		$out = array();
		$rs = $this->_connection->_Execute($this->_connection->metaTablesSQL);
		if($rs !== FALSE && $rs->RecordCount() > 0) {
			while(!$rs->EOF) {
				array_push($out, $this->getTable($rs->fields[0]));
				$rs->moveNext();
			}
		}
		return new M_ArrayIterator($out);
	}
	
	/**
	 * Returns a table object
	 *
	 * @param str $name
	 * @return M_DbTableAdo
	 */
	public function getTable($name) {
		return new M_DbTableAdo($this, $name);
	}
	
	/**
	 * Returns a new table object
	 *
	 * @param str $name
	 * @return M_DbTableAdo
	 */
	public function getNewTable() {
		return new M_DbTableAdo($this, 'untitled-table');
	}
	
	// Construct a MI_DbSelect for a given table
	public function select($from = NULL) {
		if($from) {
			return $this->getTable($from)->select();
		} else {
			return new M_DbSelect($this);
		}
	}
	
	/**
	 * Runs a query, and returns the result set
	 * 
	 * @param mixed $select
	 * @return bool
	 */ 
	public function query($select) {
		if(is_object($select)) {
			if(!in_array('MI_DbSelect', class_implements($select))) {
				throw new M_DbException(sprintf('Cannot execute a database query from a "%s" object', get_class($select)));
			}
			$rs = $select->execute();
		} else {
			$args = func_get_args();
			$sql = $this->quoteIn(array_shift($args), $args);

			//in debug mode: log query
			if (M_Debug::getDebugMode()) {
				M_Stopwatch::start('query');
			}

			//do we use cache?
			if ($this->getCacheMode()) {
				$rs = $this->_connection->CacheExecute(
					$this->getCacheLifetime(),
					$sql
				);
			}
			//nope: execute query
			else {
				$rs = $this->_connection->Execute($sql);
			}

			//in debug mode: log query
			if (M_Debug::getDebugMode()) {
				M_Stopwatch::stop('query');
				M_Debug::addToQueryLog(
					$sql,
					M_Stopwatch::getTime('query'),
					debug_backtrace()
				);
			}

			//sql could not be executed: error
			if ($rs === FALSE) {
				throw new M_DbException(sprintf(
					'Query error "%s" in query: %s',
					$this->getError(),
					$select)
				);
			}
			
			return ($rs === FALSE ? FALSE : new M_DbResultAdo($rs));
		}
	}
	
	public function queryRange($select, $offset, $numberOfRows) {
		if(is_object($select)) {
			if(!in_array('MI_DbSelect', class_implements($select))) {
				throw new M_DbException(sprintf('Cannot execute a database query from a "%s" object', get_class($select)));
			}
			$select->limit($offset, $numberOfRows);
			return $select->execute();
		} else {
			//in debug mode: log query
			if (M_Debug::getDebugMode()) {
				M_Stopwatch::start('query');
			}

			//use cache?
			if ($this->getCacheMode()) {
				$rs = $this->_connection->CacheSelectLimit(
					$this->getCacheLifetime(),
					$select,
					$numberOfRows,
					$offset
				);
			}

			//no: execute query
			else {
				$rs = $this->_connection->SelectLimit($select, $numberOfRows, $offset);
			}

			//in debug mode: log query
			if (M_Debug::getDebugMode()) {
				M_Stopwatch::stop('query');
				M_Debug::addToQueryLog(
					$select,
					M_Stopwatch::getTime('query'),
					debug_backtrace()
				);
			}

			//query error?
			if ($rs == false) {
				throw new M_DbException(sprintf(
					'Query error "%s" in query: %s',
					$this->getError(),
					$select)
				);
			}
		
			return ($rs === FALSE ? FALSE : new M_DbResultAdo($rs));
		}
	}
	
	// run INSERT / UPDATE / DELETE
	// (will return TRUE on success, FALSE on failure)
	public function queryOp($sql) {
		$args = array();
		for($i = 1, $n = func_num_args(); $i < $n; $i ++) {
			$args[] = func_get_arg($i);
		}
		
		$sql = $this->quoteIn($sql, $args);

		//in debug mode: log query
		if (M_Debug::getDebugMode()) {
			M_Stopwatch::start('query');
		}

		//execute this query
		$rs = $this->_connection->_Execute($sql);

		//in debug mode: log query
		if (M_Debug::getDebugMode()) {
			M_Stopwatch::stop('query');
			M_Debug::addToQueryLog(
				$sql,
				M_Stopwatch::getTime('query'),
				debug_backtrace()
			);
		}
		
		//query error?
		if ($rs == false) {
			throw new M_DbException(sprintf(
				'Query error "%s" in query: %s',
				$this->getError(),
				$sql)
			);
		}
		return ($rs !== FALSE);
	}
	
	public function getLastInsertId() {
		return $this->_connection->Insert_ID();
	}
	
	public function getAffectedRows() {
		return $this->_connection->Affected_Rows();
	}
	
	public function getError() {
		return $this->_connection->ErrorMsg();
	}
	
	public function quoteIdentifier($name) {
		//check if user passed table.column value
		if (strpos($name, '.')) {
			$nameArray = explode('.',$name);
			if (count($nameArray) > 2) throw new M_DbException(
				sprintf(
					'$name parameter %s should contain table.column, or column',
					$name
				)
			);
			if ($nameArray[0]!= '*') $nameArray[0] = '`'.$nameArray[0].'`';
			if ($nameArray[1]!= '*') $nameArray[1] = '`'.$nameArray[1].'`';
			$name = implode('.',$nameArray);
		}
		//if not escape whole value
		else $name = '`'. $name .'`';
		
		return $name;
	}
	
	// Set fetch mode
	public function setFetchMode($mode) {
		$this->_fetchMode = $mode;
	}
	
	// Get fetch mode
	public function getFetchMode() {
		return $this->_fetchMode;
	}
	
	/**
	 * Create table
	 * 
	 * Since {@link M_DbDriverAdo} is an implementation that is based
	 * on ADODB, this method uses ADODB's Data Dictionary. For more
	 * information, read the documentation on ADODB.
	 *
	 * @access public
	 * @param MI_DbTable $table
	 * 		The database table to be created in the database
	 * @return success
	 */
	public function createTable(MI_DbTable $table) {
		// We prepare an array with (string) field definitions
		$fields = array();
		
		// Get the primary key of the table:
		$primary = $table->getPrimaryKey();
		
		// Get the indexes in the table:
		$indexes = $table->getIndexes();
		
		// For each of the columns in the table:
		
		/* @var $column MI_DbColumn */
		foreach($table->getColumns() as $column) {
			// Start the definition of the field with its name:
			$spec = $this->quoteIdentifier($column->getName()) . ' ';
			
			// Add the type to the definition. Note that we use 
			// ADODB's data type definitions:
			switch($column->getType()) {
				case M_DbColumn::TYPE_FLOAT:
					$spec .= 'F';
					break;
				
				case M_DbColumn::TYPE_BLOB:
					$spec .= 'B';
					break;
				
				case M_DbColumn::TYPE_TEXT:
					$spec .= 'XL';
					break;
				
				case M_DbColumn::TYPE_VARCHAR:
					$spec .= 'C';
					break;
				
				case M_DbColumn::TYPE_INTEGER:
					$spec .= 'I';
					break;
					
				case M_DbColumn::TYPE_TINY_INTEGER:
					$spec .= 'I1';
					break;
					
				case M_DbColumn::TYPE_DATETIME:
					$spec .= 'T';
					break;
					
				case M_DbColumn::TYPE_DATE:
					$spec .= 'D';
					break;
			}
			
			// Add the length to the definition:
			// (if available)
			$tmp = $column->getLength();
			if($tmp) {
				$spec .= '('. $tmp .')';
			}
			
			// Add the default value to the definition:
			// (if available)
			$tmp = $column->getDefaultValue();
			if($tmp) {
				if(is_numeric($tmp)) {
					$spec .= ' DEFAULT '. $tmp;
				} else {
					$spec .= ' DEFAULT \''. str_replace('\'', '\\\'', $tmp) . '\'';
				}
			}
			
			// If the option UNSIGNED has been enabled for the column,
			// we add the option to the definition string:
			if($column->isUnsigned()) {
				$spec .= ' UNSIGNED';
			}
			
			// If the option NOT-NULL has been enabled for the column,
			// we add the option to the definition string:
			if($column->isNotNull()) {
				$spec .= ' NOTNULL ';
			}
			
			// If the option AUTO-INCREMENT has been enabled for the 
			// column, we add the option to the definition string:
			if($column->isAutoIncrement()) {
				$spec .= ' AUTOINCREMENT';
			}
			
			// For each of the indexes in the table:
			foreach($indexes as $index) {
				if($index->containsColumn($column)) {
					$spec .= ' '. ($index->isUnique() ? 'UNIQUE INDEX' : 'INDEX') . ' ' . $index->getName();
				}
			}
			
			// Add the primary-key signature to the definition, if
			// this field is part of the primary key:
			if($primary && $primary->containsColumn($column)) {
				$spec .= ' PRIMARY';
			}
			
			// Add the charset for this column
			//@todo: this gets removed by adodb, is this valid SQL?
			if ($column->getCharset()) {
			    $spec .= ' CHARACTER SET = ' . $column->getCharset();
			}
			
			// Add the collation for this column
			//@todo: this gets removed by adodb, is this valid SQL?
			if ($column->getCollate()) {
			    $spec .= ' COLLATE ' . $column->getCollate();
			}
			
			// Add the comments for this column
			//@todo: this gets removed by adodb, is this valid SQL?
		    if ($column->getComments()) {
			    $spec .= ' COMMENT ' . $this->quote($column->getComments());
			}

			// Add field definition to the collection:
			$fields[] = $spec;
			
		}
		// Construct ADODB Data Dictionary object:
		$dictionary = NewDataDictionary($this->_connection);
		
		// Set the table options
		$mysqlOptions = ' ENGINE = '.(string)$table->getEngine();
		if ($table->getCharset()) $mysqlOptions .= ' CHARACTER SET '.(string)$table->getCharset();
		if ($table->getCollate()) $mysqlOptions .= ' COLLATE '.(string)$table->getCollate();
		if ($table->getComments()) $mysqlOptions .= ' COMMENT = '.(string)$this->quote($table->getComments());
		$tableoptions = array(
			'mysql' => $mysqlOptions
		);
		
		// Parse the definition strings into an SQL. The resulting
		// SQL can be used to create the table.
		$sqlArray = $dictionary->CreateTableSQL($table->getName(), implode(', ', $fields) , $tableoptions);

		// Run the SQL:
		$dictionary->ExecuteSQLArray($sqlArray);
	}
	
	// TODO: alter table
	public function alterTable(MI_DbTable $table) {
	}
	
	public function dropTable(MI_DbTable $table) {
		$this->_connection->_Execute(sprintf($this->_connection->_dropSeqSQL, $table->getName()));
	}
	
	/**
	 * Get indexes
	 *
	 * This method will return the collection of column names that are
	 * used as an index in a given table. The return value of this method
	 * is an array:
	 * 
	 * <code>
	 *    Array (
	 *       [name-of-index-1] => [array-of-column-names],
	 *       [name-of-index-2] => [array-of-column-names],
	 *       ...
	 *    )
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * This method does NOT form part of the public {@link MI_Db} interface.
	 * {@link M_DbDriverAdo} implements this method, so it can be called
	 * by the class {@link M_DbTableAdo}. 
	 * 
	 * In other words, this method is a driver-specific method, and you
	 * should never call it directly. Instead, you should use the
	 * methods
	 * 
	 * - {@link M_DbTableAdo::getIndexes()}
	 * - {@link M_DbTableAdo::getPrimaryKey()}
	 * 
	 * For example:
	 * <code>
	 *    $db = M_Db::getInstance();
	 *    $table = $db->getTable('news');
	 *    $index = $table->getPrimaryKey();
	 * </code>
	 * 
	 * @access public
	 * @param string $table
	 * 		The table of which you wish to fetch the 
	 * @param boolean $primary
	 * 		Set to TRUE if you want to fetch primary keys only
	 * @return array
	 */
	public function getIndexes($table, $primary = FALSE) {
		$out = array();
		$ind = $this->_connection->MetaIndexes((string) $table, $primary);
		if(is_array($ind) && is_array($ind)) {
			foreach($ind as $name => $index) {
				$out[$name] = $index['columns'];
			}
		}
		return $out;
	}
	
	public function close() {
		$this->_connection->Close();
		// should this method also unregister() the connection from the
		// M_Db class?
	}

	/**
	 * Set the directory to which the cache files should be saved
	 * 
	 * @param M_Directory $directory
	 * @return M_DbDriverAdo
	 */
	public function setCacheDirectory(M_Directory $directory) {
		//unfortunately ADODB uses global variables to store the cache-directory
		$GLOBALS['ADODB_CACHE_DIR'] = M_Loader::getAbsolute($directory->getPath());
		return $this;
	}

	/**
	 * Delete all cache from the cache-directory
	 *
	 * @return void
	 */
	public function flushCache() {
		$this->_connection->CacheFlush();
	}
}