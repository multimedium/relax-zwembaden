<?php
/**
 * M_DbQueryFilterOrder class
 * 
 * adjust the query-order
 *
 * @package Core
 * @access public
 */
class M_DbQueryFilterOrder extends M_DbQueryFilter 
{
	/**
	 * @var array
	 */
	protected $_order = array();

	/**
	 * Construct
	 *
	 * @param str $column 
	 * @param str $order (ASC/DESC) 
	 * @return void
	 */
	public function __construct( $column, $order = 'ASC') {
		$this->_order[] = array($column, strtoupper($order));
	}
	
	/**
	 * Order
	 *
	 * @param str $column
	 * @param str $order (ASC/DESC)
	 * @return M_DbQueryFilterOrder
	 */
	public function addOrder( $column, $order) {
		$this->_order[] = array($column, strtoupper($order));
		
		return $this;
	}
	
	/**
	 * Reset the order
	 *
	 * @return M_DbQueryFilterOrder
	 */
	public function resetOrder() {
		$this->_order = array();
		
		return $this;
	}
	
	/**
	 * Apply the filter
	 * 
	 * apply the added column-order.
	 * 
	 * @example 
	 * 
	 * $filter = new M_DbQueryFilterOrder('column1', 'ASC');
	 * $filter->filter($select);
	 * 
	 * Final result: 
	 * ORDER BY `column1` ASC
	 *
	 * @param M_DbSelect $query
	 * @param M_DataObjectMapper $mapper
	 * @return M_DbSelect $query
	 */
	public function filter( M_DbQuery $query, M_DataObjectMapper $mapper = null) {
		$orderIterator = new ArrayIterator($this->_order);
		while( $orderIterator->valid())
		{
			$order = $orderIterator->current();
			$dbName = $order[0];
			//if a mapper is provided: ask him for the localized field name
			if (!is_null($mapper)) {
				$dbName = $mapper->getFieldDbNameLocalized($dbName);
			}
			$query->order($dbName,$order[1]);
			$orderIterator->next();
		}
		return $query;
	}
	
	/**
	 * Get fields for this filter, based on every field on which we want
	 * to order
	 * 
	 * @return ArrayIterator
	 */
	public function getFields() {
		$fields = array();
		foreach($this->_order AS $order) {
			$fields[] = $order[0];
		}
		
		return new ArrayIterator($fields);
	}
}