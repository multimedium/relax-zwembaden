<?php
/**
 * M_DbQueryFilterColumn class
 *
 * @package Core
 * @access public
 */
class M_DbQueryFilterColumn extends M_DbQueryFilter 
{
	/**
	 * @var bool
	 */
	protected $_resetColumns;
	
	/**
	 * Construct
	 *
	 * @param bool $clearColumns Clear all previously defined columns
	 * @return void
	 */
	public function __construct( $resetColumns = false ) {
		$this->_resetColumns = (bool)$resetColumns;
	}
	
	/**
	 * Add a column to the select query
	 *
	 * @param str $column
	 * @return void
	 */
	public function addColumn( $column ) 
	{
		$this->_columns[] = $column;
	}
	
	/**
	 * Add multiple columns at once
	 *
	 * @param array $columns
	 * @return void
	 */
	public function addColumns( $columns )
	{
		foreach ($columns AS $column)
		{
			$this->_columns[] = $column;
		}
	}
	
	/**
	 * Apply the filter
	 *
	 * @param M_DbSelect $query
	 * @param M_DataObjectMapper $mapper
	 * @return M_DbSelect $query
	 */
	public function filter( M_DbQuery $query, M_DataObjectMapper $mapper = null) {
		//if we want to reset, we use the columns() method which automaticcally
		//resets previously selected columns
		if ($this->_resetColumns === true) $query->resetColumns ();

		//otherwise we add every column at a time
		foreach( $this->_columns AS $column)
		{
			//use mapper to translate object-name to db-name?
			if(!is_null($mapper)) $column = $mapper->getFieldDbNameLocalized($column);
			$query->addColumn($column);
		}
		
		return $query;
	}
	
	/**
	 * Get the fields on which is filtered on
	 * 
	 * @return ArrayIterator
	 */
	public function getFields() {
		$fields = array();
		
		foreach($this->_columns AS $column) {
			$fields[] = $column;
		}
		
		return new ArrayIterator($fields);
	}
}