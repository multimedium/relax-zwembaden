<?php
/**
 * M_DbQueryFilter class
 *
 * @package Core
 * @access abstract
 */
abstract class M_DbQueryFilter implements  MI_DbQueryFilter 
{
	/**
	 * Apply the filter
	 *
	 * @param M_DbQuery $query
	 * @return M_DbQuery $query
	 */
	public function filter(M_DbQuery $query) {
	}
	
	/**
	 * Change the field name of a certain field in a condition
	 * 
	 * Can be used when filtering on e.g. joined tables
	 * The column name of the joined table is unknown for this filter
	 * That's why we need to change the name of the column that is used in the 
	 * filters, the old name will be replaced in all conditions by the new column
	 * name
	 * 
	 * @param string $old
	 * @param string $new
	 * 
	 * @return M_DbQueryFilterWhere $this
	 */
	public function changeFieldName($old, $new) {
		foreach($this->_conditions AS $i => $condition) {
			$field = $condition['field'];
			if($field == $old) {
				$this->_conditions[$i]['field'] = $new;
			}	
		}
		
		return $this;
	}
}