<?php
// Abstract class to support database-level drivers. This class provides
// with the basic implementation of MI_Db, shared by all driver
// implementations
abstract class M_DbDriver implements MI_Db {

	/**
	 * Enable/disable caching of recordsets
	 *
	 * @var bool
	 */
	protected $_cacheMode = false;

	/**
	 * The lifetime a cache of a recordset will live
	 *
	 * @var int
	 */
	protected $_cacheLifetime = 3600;
	
	// When all references to the database are removed, we close the 
	// connection. Note that the database will be a singleton instance,
	// so this function will be called at the end of the page request.
	public function __destruct() {
		// This has been commented, because this will cause database
		// queries to fail in other objects' __destruct() method!!!
		// (eg. M_LocaleMessageCatalog)
		//$this->close();
	}
	
	// Check if a table exists in the database:
	public function hasTable($name) {
		foreach($this->getTables() as $table) {
			if($name == $table->getName()) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	// Quote a value for secure use in a SQL query, regardless of 
	// its type
	public static function quote($value) {
		//numeric values which have leading zero need to be quoted as a string
		//to keep the zero in place. Otherwise it will get lost
		if(is_numeric($value) && !strncmp('056','0',1) == 0) {
			// is_float() fails on string values!
			if((float) $value > (int) $value) {
				return (float) $value;
			}
			return (int) $value;
		} elseif(is_null($value)) {
			return 'NULL';
		} else {
			return '"' . self::quoteString($value) . '"';
		}
	}
	
	// Quote a string value for secure use in a SQL query
	public function quoteString($string) {
		return addslashes((string)$string);
	}
	
	// This method will insert the placeholder values (note that this
	// method will quote the values, taking their types into account)
	// Note that you can provide a variable number of arguments, by
	// adding parameters to the quoteIn() call. However, you can also
	// choose the all-in-one approach, by providing an array with values
	public function quoteIn($sql, $args) {
		if(!is_array($args)) {
			$args = array();
			for($i = 1, $n = func_num_args(); $i < $n; $i ++) {
				$args[] = func_get_arg($i);
			}
		}
		
		$n = count($args);
		if($n > 0) {
			return preg_replace('/\?/e', 'self::quote(array_shift($args))', $sql, $n);
		} else {
			return $sql;
		}
	}

	/**
	 * Is cache enable or not?
	 *
	 * @return bool
	 */
	public function getCacheMode() {
		return $this->_cacheMode;
	}

	/**
	 * Enable caching of recordsets
	 *
	 * Note: lifetime is set default to 3600 seconds
	 *
	 * @param bool $mode TRUE is on, FALSE is off
	 * @param int $lifetime number of seconds the cache will live
	 * @return M_DbDriverAdo
	 */
	public function setCacheMode($mode, $lifetime = null) {
		$this->_cacheMode = (bool)$mode;

		//set lifetime only is available
		if(!is_null($lifetime)) $this->_cacheLifetime = $lifetime;

		return $this;
	}

	/**
	 * The number of seconds cache of a recordset will live
	 * 
	 * @return int
	 */
	public function getCacheLifetime() {
		return $this->_cacheLifetime;
	}

	/**
	 * Tell the driver how low the cache needs to live
	 * 
	 * @param int $arg
	 * @return M_DbDriver
	 */
	public function setCacheLifetime($arg) {
		$this->_cacheLifetime = (int)$arg;
		return $this;
	}
}