<?php
/**
 * M_DbQueryFilterSearch class
 *
 * @package Core
 */
class M_DbQueryFilterSearch extends M_DbQueryFilterWhere {
	/**
	 * Conditions
	 * 
	 * The search conditions which will be applied to the DbQuery
	 * 
	 * @access protected
	 * @var array
	 * @example $_conditions = array(
	 * 		array('field' => 'id', 'value' => 'keyword1', 'logical' => null),
	 * 		array('field' => 'price', 'value' => 'keyword2', 'logical' => 'AND')
	 * );
	 */
	protected $_searchConditions = array();
	
	/**
	 * Construct
	 *
	 * @access public
	 * @uses M_DbQueryFilterSearch::addSearchCondition()
	 * @param array $fields
	 * @param mixed $value
	 * @return M_DbQueryFilterSearch
	 */
	public function __construct(array $fields, $value) {
		$this->addSearchCondition($fields, $value);
	}
	
	/**
	 * Add a Search Condition
	 * 
	 * Very much like {@link M_DbQueryFilterWhere::addCondition()}, but will
	 * add a prepared search condition.
	 * 
	 * @access public
	 * @param array $fields
	 * 		The fields on which to apply the filter, or in other words: the 
	 * 		fields in which to search for the words in the provided search
	 * 		expression
	 * @param string $expression
	 * 		The search expression
	 * @param string $logical
	 * 		The logical operator, with which to add the search condition (AND/OR...)
	 * @return M_DbQueryFilterSearch
	 */
	public function addSearchCondition(array $fields, $value, $logical = M_DbQuery::OPERATOR_AND) {
		// The filter must be applied on at least 1 field. We check the provided
		// collection of fields:
		if(count($fields) == 0) {
			// If no fields have been provided, we throw an exception:
			throw new M_DbException(sprintf(
				'%s cannot add search condition; at least 1 field should be provided',
				__CLASS__
			));
		}
		
		// Add the search condition to the internal property:
		$this->_searchConditions[] = array(
			'fields' => $fields,
			'value' => $value,
			'logical' => $logical
		);
		
		// We return the filter, so we can chain the calls of addCondition() and
		// addSearchCondition()
		return $this;
	}
	
	/**
	 * Get all fields for this filter, including the ones of the subfilters
	 * 
	 * Note: DbQueryFilterSearch has its own implementation of getFields() since
	 * it stores the fields it filters on in 
	 * {@link DbQueryFilterSearch::__searchConditions}
	 * 
	 * @return ArrayIterator
	 */
	public function getFields() {
		$fields = parent::getFields()->getArrayCopy();
		foreach($this->_searchConditions AS $condition) {
			$fields = array_merge($condition['fields'], $fields);
		}
		
		return new ArrayIterator($fields);
	}
	
	/**
	 * Apply the filter
	 *
	 * @param M_DbQuery $query
	 * 		The database query on which the filter is to be applied
	 * @param M_DataObjectMapper $mapper
	 * 		Optional: mapper which is used to translate column names
	 * @return M_DbQuery $query
	 * 		The database query, with the filter applied
	 */
	public function filter(M_DbQuery $query, M_DataObjectMapper $mapper = null) {
		// For each of the search conditions that have been added to the filter:
		foreach($this->_searchConditions as $condition) {
			// Extract the search terms from the search expression:
			$terms = M_Search::getSearchTerms($condition['value']);
			
			// If at least 1 search term has been extracted from the expression:
			$countTerms = count($terms);
			if($countTerms > 0) {
				// For each of the search terms:
				for($i = 0; $i < $countTerms; $i ++) {
					// We open a where-block in the query, so we can group the 
					// SQL conditions for the current search term:
					$query->openWhereBlock(M_DbQuery::OPERATOR_AND);
					
					// Now, for each of the fields on which the current search
					// condition is being applied:
					foreach($condition['fields'] as $field) {
						//translate the db-name if a mapper is provided
						if (!is_null($mapper)) {
							$field = $mapper->getFieldDbNameLocalized($field);
						}
							
						// We send a whereContains() message to the query, to
						// add the search for the current search term, in the
						// current field:
						$query->whereContains(
							$field,
							// Current search term:
							$terms[$i]['term'],
							// Search position is middle (otherwise, it would mean
							// "begins with", or "ends with", instead of "contains"
							'middle',
							// The current field is only one of the fields that
							// needs to be searched in. We use the logical OR 
							// operator
							M_DbQuery::OPERATOR_OR
						);
					}
					
					// We close the where block that we have opened earlier
					$query->closeWhereBlock();
					
				} // End foreach search term
				
			} // If at least 1 search term
		} // End foreach search condition
		
		// At this point, we have applied the search conditions to the query.
		// Note however that M_DbQueryFilterSearch is a subclass of M_DbQueryFilterWhere,
		// and more conditions may have been attached to the filter. So, we
		// apply the other conditions as well:
		$query = parent::filter($query);
		
		// We're done. We return the query!
		return $query;
	}
}