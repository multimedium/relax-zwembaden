<?php
/**
 * M_Callback
 * 
 * M_Callback can be used to describe a user-defined function. Note that
 * callback functions are not only simple functions, but can also be
 * object methods, including static class methods.
 * 
 * A PHP function is passed by its name as a string. Any built-in or 
 * user-defined function can be used, except language constructs such 
 * as: array(), echo(), empty(), eval(), exit(), isset(), list(), 
 * print(), unset().
 * 
 * So, for example, to describe a simple function, you would do the 
 * following:
 * 
 * Example 1
 * <code>
 *    $callback = new M_Callback;
 *    $callback->setFunctionName('myFunction');
 * </code>
 * 
 * You can always to choose to define the callback in the standard
 * PHP way, by using the setCallback() method. So, you can rewrite
 * Example 1 as following:
 * 
 * Example 2
 * <code>
 *    $callback = new M_Callback;
 *    $callback->setCallback('myFunction');
 * </code>
 * 
 * A callback function can also be a method of an instantiated object.
 * In PHP, such a callback function is defined by an array containing 
 * an object at index 0 and the method name at index 1. Consider the 
 * following method:
 * 
 * Example 3
 * <code>
 *    // Construct my object:
 *    $myObject = new MyObject();
 *    
 *    // Set the callback to a method of my object:
 *    $callback = new M_Callback;
 *    $callback->setCallback(array($myObject, 'myMethod'));
 * </code>
 * 
 * Of course, you can use alternative ways to define the same callback
 * function. Consider the following example to rewrite Example 3:
 * 
 * Example 4
 * <code>
 *    // Construct my object:
 *    $myObject = new MyObject();
 *    
 *    // Set the callback to a method of my object:
 *    $callback = new M_Callback;
 *    $callback->setObject($myObject);
 *    $callback->setFunctionName('myMethod');
 * </code>
 * 
 * Static class methods can also be passed without instantiating an 
 * object of that class by passing the class name instead of an object 
 * at index 0.
 * 
 * Example 5
 * <code>
 *    $callback = new M_Callback;
 *    $callback->setCallback(array('MyObject', 'myMethod'));
 * </code>
 * 
 * Again, you can use alternative ways to define the same static 
 * callback. Consider the following example to rewrite Example 5:
 * 
 * Example 6
 * <code>
 *    $callback = new M_Callback;
 *    $callback->setClassName('MyObject');
 *    $callback->setFunctionName('myMethod');
 * </code>
 * 
 * Note:
 * If you want to define a static class method as your callback 
 * function, you may also choose to define it with a string, as 
 * following:
 * 
 * Example 7
 * <code>
 *    $callback = new M_Callback;
 *    $callback->setCallbackByString('MyObject::myMethod');
 * </code>
 * 
 * Once you have defined your callback function, you can set the
 * arguments to the function by sending either one of the following
 * messages to M_Callback:
 * 
 * - {@link M_Callback::setArguments()}
 * - {@link M_Callback::setArgument()}
 * - {@link M_Callback::addArgument()}
 * 
 * The whole point of defining a callback function is to dynamically
 * call it, and eventually get the return value of the defined function.
 * Once you have defined your callback function, you can call/run it 
 * with the run() function:
 * 
 * Example 8
 * <code>
 *    $callback = new M_Callback;
 *    $callback->setCallback('myFunction');
 *    $resultOfMyFunction = $callback->run();
 * </code>
 * 
 * @package Core
 */
class M_Callback {
	/**
	 * Type constant
	 * 
	 * This constant can be used to address a given callback type. For
	 * more information on callback types, read the docs on:
	 * 
	 * - {@link M_Callback}
	 * - {@link M_Callback::getCallbackType()}
	 * - {@link M_Callback::isCallbackType()}
	 *
	 */
	const SIMPLE_FUNCTION = 'simple function';
	
	/**
	 * Type constant
	 * 
	 * This constant can be used to address a given callback type. For
	 * more information on callback types, read the docs on:
	 * 
	 * - {@link M_Callback}
	 * - {@link M_Callback::getCallbackType()}
	 * - {@link M_Callback::isCallbackType()}
	 *
	 */
	const OBJECT_METHOD = 'object method';
	
	/**
	 * Type constant
	 * 
	 * This constant can be used to address a given callback type. For
	 * more information on callback types, read the docs on:
	 * 
	 * - {@link M_Callback}
	 * - {@link M_Callback::getCallbackType()}
	 * - {@link M_Callback::isCallbackType()}
	 *
	 */
	const CLASS_METHOD = 'class method';
	
	/**
	 * Object
	 * 
	 * This property stores the object in the callback definition. For
	 * more information, read the docs on:
	 * 
	 * - {@link M_Callback::getObject()}
	 * - {@link M_Callback::setObject()}
	 *
	 * @access private
	 * @var object
	 */
	private $_object = NULL;
	
	/**
	 * Class name
	 * 
	 * This property stores the name of the class in the callback 
	 * definition. For more information, read the docs on:
	 * 
	 * - {@link M_Callback::getClassName()}
	 * - {@link M_Callback::setClassName()}
	 *
	 * @access private
	 * @var string
	 */
	private $_className = NULL;
	
	/**
	 * Function name/Method name
	 *
	 * This property stores the name of the function/method in the 
	 * callback definition
	 * 
	 * @access private
	 * @var string
	 */
	private $_function;
	
	/**
	 * Arguments
	 * 
	 * This property stores the arguments that have been provided for
	 * the callback function. For more information, read the docs on:
	 * 
	 * - {@link M_Callback::setArguments()}
	 * - {@link M_Callback::setArgument()}
	 * - {@link M_Callback::addArgument()}
	 * - {@link M_Callback::unshiftArgument()}
	 * - {@link M_Callback::dropArguments()}
	 *
	 * @access private
	 * @var array
	 */
	private $_args = array();
	
	/* -- GETTERS -- */
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param mixed $callbackDefinition
	 * @uses M_Callback::setCallback()
	 * @return M_Callback
	 */
	public function __construct($callbackDefinition = NULL) {
		// If the callback definition has been provided
		if($callbackDefinition) {
			// Then, set it:
			$this->setCallback($callbackDefinition);
		}
	}
	
	/**
	 * Get object
	 * 
	 * If the callback function has been defined as a method of an 
	 * instantiated object, you can use this method to obtain the object
	 * from the callback's definition.
	 * 
	 * NOTE:
	 * If no object is contained by the callback function, the return
	 * value will be NULL.
	 *
	 * @access public
	 * @return object|null
	 */
	public function getObject() {
		return $this->_object;
	}
	
	/**
	 * Get class name
	 * 
	 * If the callback function has been defined as a static method
	 * (without instantiating an object of a class), you can use this
	 * method to obtain the name of the class.
	 * 
	 * Note that this method will also return the class name of the 
	 * callback object, if defined as a method of an instantiated 
	 * object.
	 * 
	 * NOTE:
	 * If no class name could be extracted from the callback function, 
	 * the return value will be NULL.
	 *
	 * @access public
	 * @return object|null
	 */
	public function getClassName() {
		if(is_object($this->_object)) {
			return get_class($this->_object);
		} else {
			return $this->_className;
		}
	}
	
	/**
	 * Get function name
	 * 
	 * This method provides with the name of the function (or method) 
	 * in the callback definition.
	 * 
	 * NOTE:
	 * If no function name could be extracted from the callback 
	 * definition, the return value will be NULL.
	 *
	 * @access public
	 * @return string|null
	 */
	public function getFunctionName() {
		return $this->_function;
	}
	
	/**
	 * Get callback type
	 * 
	 * This method will indicate the type of the callback function.
	 * As explained in the introduction of {@link M_Callback}, a callback
	 * can be of 3 different types:
	 * 
	 * - A simple function
	 * - A method of an instantiated object
	 * - A static method
	 * 
	 * In order to easily identify the callback types, M_Callback
	 * provides with constants, which are respectively:
	 * 
	 * - {@link M_Callback::SIMPLE_FUNCTION}
	 * - {@link M_Callback::OBJECT_METHOD}
	 * - {@link M_Callback::CLASS_METHOD}
	 *
	 * To check a callback against a given type, check out the docs
	 * at {@link M_Callback::isCallbackType()}.
	 * 
	 * NOTE:
	 * If no callback type could be recognized (because of missing
	 * elements in the callback definition), the return value will be 
	 * NULL.
	 * 
	 * @access public
	 * @return string|null
	 */
	public function getCallbackType() {
		if($this->_className && $this->_function) {
			return self::CLASS_METHOD;
		} elseif($this->_object && $this->_function) {
			return self::OBJECT_METHOD;
		} elseif($this->_function) {
			return self::SIMPLE_FUNCTION;
		} else {
			return NULL;
		}
	}
	
	/**
	 * Check callback against a given type
	 * 
	 * This method can be used to check whether or not the callback is
	 * of a given type. To learn more about callback types, read the
	 * docs on:
	 * 
	 * - {@link M_Callback}
	 * - {@link M_Callback::getCallbackType()}
	 * 
	 * Example 1, check if callback is a simple function:
	 * <code>
	 *    $callback = new M_Callback();
	 *    $callback->setFunctionName('myFunction');
	 *    
	 *    if($callback->isCallbackType(M_Callback::SIMPLE_FUNCTION)) {
	 *       echo 'I have created a callback with a simple function';
	 *    }
	 * </code>
	 * 
	 * Note:
	 * This method will return TRUE if the callback is of the given
	 * type, FALSE if not.
	 * 
	 * @access public
	 * @param string $type
	 * 		The callback type string to check against
	 * @return boolean
	 */
	public function isCallbackType($type) {
		return ($this->getCallbackType() == $type);
	}
	
	/**
	 * Get arguments
	 * 
	 * This method will provide with the collection of arguments that
	 * has been provided for the callback function.
	 *
	 * @access public
	 * @return ArrayIterator
	 */
	public function getArguments() {
		return new ArrayIterator($this->_args);
	}
	
	/**
	 * Get argument
	 * 
	 * This method will provide with the value of the argument at a 
	 * given index. For example, to get the value of the first argument,
	 * you would do the following:
	 * 
	 * Example 1
	 * <code>
	 *    $callback = new M_Callback();
	 *    $callback->setArguments(
	 *       array(
	 *          'Argument One',
	 *          'Argument Two',
	 *          'Argument Three',
	 *          'Argument Four'
	 *       )
	 *    );
	 *    
	 *    echo $callback->getArgument(0); // Will output "Argument One"
	 * </code>
	 * 
	 * Note:
	 * If no value could have been found for the requested argument,
	 * this method will return NULL.
	 * 
	 * @access public
	 * @param integer $index
	 * 		The index of the requested argument
	 * @return mixed
	 */
	public function getArgument($index) {
		return (isset($this->_args[$index]) ? $this->_args[$index] : NULL);
	}
	
	/**
	 * Get number of arguments
	 * 
	 * This method will return the number of arguments that has been 
	 * provided to the {@link M_Callback} object.
	 *
	 * @access public
	 * @return integer
	 */
	public function getNumberOfArguments() {
		return count($this->_args);
	}
	
	/**
	 * Get number of required arguments
	 * 
	 * This method will return the number of arguments that is required
	 * by the callback function.
	 *
	 * @access public
	 * @return integer
	 */
	public function getNumberOfRequiredArguments() {
		$method = new ReflectionMethod($this->getClassName(), $this->_function);
		return $method->getNumberOfRequiredParameters();
	}
	
	/**
	 * Get callback
	 * 
	 * This method will provide with a definition of the callback in 
	 * the standard PHP way:
	 * 
	 * SIMPLE FUNCTION
	 * A simple function is defined by a string, containing the name
	 * of the function.
	 * 
	 * METHOD OF INSTANTIATED OBJECT
	 * A method of an instantiated object is defined by an array 
	 * containing an object at index 0 and the method name at index 1
	 * 
	 * STATIC CLASS METHOD
	 * A static method of a class is defined by an array containing 
	 * a class name at index 0 and the method name at index 1
	 *
	 * NOTE:
	 * Note that if no callback has been provided yet, this method 
	 * will return NULL.
	 * 
	 * @access public
	 * @return array|string|NULL
	 */
	public function getCallback() {
		if($this->_className && $this->_function) {
			return array($this->_className, $this->_function);
		} elseif($this->_object && $this->_function) {
			return array($this->_object, $this->_function);
		} elseif($this->_function) {
			return $this->_function;
		} else {
			return NULL;
		}
	}
	
	/**
	 * Is callable?
	 * 
	 * This method will check if the callback is callable. Returns
	 * TRUE if callable, FALSE if not.
	 *
	 * @access public
	 * @return boolean
	 */
	public function isCallable() {
		return is_callable($this->getCallback());
	}
	
	/**
	 * Call/Run the callback
	 *
	 * This method will run the callback, and provide with the return 
	 * value of the callback function as the result.
	 * 
	 * @throws M_CallbackException
	 * @access public
	 * @return mixed
	 */
	public function run() {
		if($this->isCallable()) {
			$this->_cookCallback();
			return call_user_func_array($this->getCallback(), $this->_args);
		} else {
			throw new M_CallbackException(sprintf(
				'Cannot run callback; %s::%s() is not callable',
				$this->getClassName(),
				$this->getFunctionName()
			));
		}
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set object
	 * 
	 * If the callback function should be defined as a method of an 
	 * instantiated object, this method can be used to set the object
	 * in which the method is implemented.
	 * 
	 * @throws M_CallbackException
	 * @access public
	 * @param object $object
	 * 		The object
	 * @return void
	 */
	public function setObject($object) {
		if(!is_object($object)) {
			throw new M_CallbackException(sprintf(
				'%s: %s() expects an object',
				__CLASS__,
				__FUNCTION__
			));
		}
		$this->_object = $object;
		$this->dropClassName();
	}
	
	/**
	 * Drop object
	 * 
	 * This method will drop/remove the object from the callback. For
	 * example, if you drop the object of a callback that has been
	 * defined as a method of an instantiated object, the callback will
	 * remain as a simple function.
	 * 
	 * @access public
	 * @return void
	 */
	public function dropObject() {
		$this->_object = NULL;
	}
	
	/**
	 * Set class name
	 * 
	 * If the callback function should be defined as a static method 
	 * of a class, this method can be used to set the name of the class
	 * in which the method is implemented.
	 * 
	 * @access public
	 * @param string $class
	 * 		The name of the class
	 * @return void
	 */
	public function setClassName($class) {
		$this->_className = (string) $class;
		$this->dropObject();
	}
	
	/**
	 * Drop class name
	 * 
	 * This method will drop/remove the class name from the callback. 
	 * For example, if you drop the class name of a callback that has 
	 * been defined as a static method of a class, the callback will
	 * remain as a simple function.
	 * 
	 * @access public
	 * @return void
	 */
	public function dropClassName() {
		$this->_className = NULL;
	}
	
	/**
	 * Set function name
	 * 
	 * This method can be used to set the name of the function (or
	 * method).
	 *
	 * @access public
	 * @param string $function
	 * 		The name of the function (or method)
	 * @return void
	 */
	public function setFunctionName($function) {
		$this->_function = $function;
	}
	
	/**
	 * Set arguments
	 * 
	 * This method can be used to set the arguments that should be
	 * passed to the callback function. Note that this specific method
	 * is used to set the entire collection of arguments at once.
	 *
	 * @access public
	 * @param array $args
	 * 		The collection of arguments
	 * @return void
	 */
	public function setArguments(array $args) {
		$this->_args = array_values($args); // We use numerical indexes!
	}
	
	/**
	 * Set argument
	 * 
	 * This method can be used to set the value of an argument at a 
	 * given index. For example, to set the value of the first argument,
	 * you would do the following:
	 * 
	 * Example 1
	 * <code>
	 *    $callback = new M_Callback();
	 *    $callback->setArguments(
	 *       array(
	 *          'Argument One',
	 *          'Argument Two',
	 *          'Argument Three',
	 *          'Argument Four'
	 *       )
	 *    );
	 *    
	 *    // Change the value of the first argument:
	 *    $callback->setArgument(0, 'Parameter One');
	 * </code>
	 *
	 * @throws M_CallbackException
	 * @access public
	 * @param integer $index
	 * 		The index position of the argument
	 * @param mixed $arg
	 * 		The new value of the argument
	 * @return void
	 */
	public function setArgument($index, $arg) {
		if(array_key_exists((int) $index, $this->_args)) {
			$this->_args[$index] = $arg;
		} else {
			throw new M_CallbackException(sprintf(
				'%s: Cannot set value of argument %d, because the argument does not exist',
				__CLASS__,
				$index
			));
		}
	}
	
	/**
	 * Add argument
	 * 
	 * This method can be used to add an argument to the callback. The
	 * argument will be pushed onto the end of the current collection 
	 * of arguments.
	 *
	 * @access public
	 * @param mixed $arg
	 * 		The value of the new argument
	 * @return void
	 */
	public function addArgument($arg) {
		$this->_args[] = $arg;
	}
	
	/**
	 * Unshift argument
	 * 
	 * This method can be used to add an argument to the callback. Note
	 * that, on the contrary to {@link M_Callback::addArgument()}, the
	 * argument will be added to the beginning/front of the current 
	 * collection of arguments.
	 * 
	 * @access public
	 * @param mixed $arg
	 * 		The value of the new argument
	 * @return void
	 */
	public function unshiftArgument($arg) {
		array_unshift($this->_args, $arg);
	}
	
	/**
	 * Drop arguments
	 * 
	 * This method can be used to drop/remove all arguments from the
	 * callback definition.
	 * 
	 * @access public
	 * @return void
	 */
	public function dropArguments() {
		$this->_args = array();
	}
	
	/**
	 * Set callback
	 * 
	 * This method will set the callback, provided a callback that is
	 * defined in the standard PHP way. In PHP, callbacks are defined
	 * as following:
	 * 
	 * SIMPLE FUNCTION
	 * A simple function is defined by a string, containing the name
	 * of the function.
	 * 
	 * METHOD OF INSTANTIATED OBJECT
	 * A method of an instantiated object is defined by an array 
	 * containing an object at index 0 and the method name at index 1
	 * 
	 * STATIC CLASS METHOD
	 * A static method of a class is defined by an array containing 
	 * a class name at index 0 and the method name at index 1
	 * 
	 * NOTE:
	 * If the callback is not well-formed, this method will throw an 
	 * exception!
	 * 
	 * @throws M_CallbackException
	 * @access public
	 * @param array|string $callback
	 * 		The callback definition
	 * @return void
	 */
	public function setCallback($callback) {
		if(is_array($callback)) {
			if(count($callback) == 2 && is_callable($callback)) {
				$this->setFunctionName($callback[1]);
				if(is_object($callback[0])) {
					$this->setObject($callback[0]);
				} else {
					$this->setClassName($callback[0]);
				}
			} else {
				throw new M_CallbackException(sprintf(
					'%s: %s: Invalid callback definition',
					__CLASS__,
					__FUNCTION__
				));
			}
		} else {
			$this->setCallbackByString($callback);
		}
	}
	
	/**
	 * Set callback by string
	 * 
	 * This method will parse the callback definition out of a given
	 * string. For example, to define the callback as a static method
	 * of a class, you can do the following:
	 * 
	 * Example 1
	 * <code>
	 *    $callback = new M_Callback;
	 *    $callback->setCallbackByString('MyClassName::myMethodName');
	 * </code>
	 *
	 * @throws M_CallbackException
	 * @access public
	 * @param string $string
	 * 		The callback definition, given as a string
	 * @return void
	 */
	public function setCallbackByString($string) {
		$e = explode('::', trim(str_replace(' ', '', (string) $string), " \t\n\r\0\x0B/()"));
		$n = count($e);
		if($n == 2) {
			$this->setClassName($e[0]);
			$this->setFunctionName($e[1]);
		} elseif($n == 1) {
			$this->dropClassName();
			$this->dropObject();
			$this->setFunctionName($e[0]);
		} else {
			throw new M_CallbackException(sprintf(
				'Could not extract callback function out of %s',
				$string
			));
		}
	}
	
	/**
	 * Cook Callback
	 *
	 * This method is called internally, to prepare the callback before
	 * calling it. This is done in order to verify that the callback
	 * has been properly defined, and all required arguments have been
	 * provided.
	 * 
	 * This method will check the following things:
	 * 
	 * - Is the function/method public?
	 * - Have all required parameters been provided?
	 * 
	 * Note that this method will throw an exception, if the current
	 * callback definition does not match any of the above-mentioned
	 * criteria.
	 * 
	 * @throws M_CallbackException
	 * @access private
	 * @return void
	 */
	private function _cookCallback() {
		if($this->isCallbackType(self::SIMPLE_FUNCTION)) {
			$method = new ReflectionFunction($this->_function);
		} else {
			$className = $this->getClassName();
			if(!method_exists($className, $this->_function)) {
				throw new M_CallbackException(sprintf(
					'Method %s does not exist in class %s',
					$this->_function,
					$className
				));
			}
			$method = new ReflectionMethod($className, $this->_function);
			if(!$method->isPublic()) {
				throw new M_CallbackException(sprintf(
					'Callback method %s() is not public',
					$method->getName()
				));
			}
		}
		
		if($method->getNumberOfRequiredParameters() <= $this->getNumberOfArguments()) {
			return TRUE;
		} else {
			throw new M_CallbackException(sprintf(
				'Missing argument(s) for Callback function %s',
				$method->getName()
			));
		}
	}
}