<?php
/**
 * M_Object class
 * 
 * The M_Object class is the superclass for all Core classes. It 
 * provides with the basic extensions that we expect from all Core
 * classes.
 * 
 * @package Core
 */
abstract class M_Object {
	/**
	 * Object ID
	 * 
	 * Each core object has a Unique ID. Typically, this Unique ID is 
	 * used to identify objects (eg. Event Listeners).
	 * 
	 * @access private
	 * @var integer
	 */
	private $_uniqueObjectId;
	
	/* -- GETTERS -- */
	
	/**
	 * Get class name
	 * 
	 * Provides with the class name of the object.
	 * 
	 * @final
	 * @access public
	 * @return string
	 */
	public final function getClassName() {
		return get_class($this);
	}
	
	/**
	 * Get parent class name
	 * 
	 * Provides with the class name of the object's superclass.
	 * 
	 * @final
	 * @access public
	 * @return string
	 */
	public final function getParentClassName() {
		return get_parent_class($this);
	}
	
	/**
	 * Get Object ID
	 * 
	 * Will provide with the Object ID. Each core object has a Unique 
	 * ID. Typically, this Unique ID is used to identify objects (eg. 
	 * Event Listeners).
	 * 
	 * @final
	 * @access public
	 * @return integer
	 */
	public final function getCoreObjectId() {
		// If no Object ID has been assigned to the object:
		if(! $this->_uniqueObjectId) {
			// We assign an Object ID to the object now:
			$this->_uniqueObjectId = self::getNewCoreObjectId();
		}
		
		// Return the Object ID:
		return $this->_uniqueObjectId;
	}
	
	/**
	 * Get new Object ID
	 * 
	 * This method is used, to assign new Object ID's. For more info,
	 * read {@link M_Object::getCoreObjectId()}.
	 * 
	 * @static
	 * @final
	 * @access public
	 * @return integer
	 */
	public static final function getNewCoreObjectId() {
		// Maintain Object ID Counter:
		static $objectCounter = 0;
		
		// Get new Object ID:
		return (++ $objectCounter);
	}
	
	/* -- MAGIC METHODS -- */
	
	/**
	 * Console
	 * 
	 * This method is used by {@link M_Console}, to write an object
	 * to the console log file. The result of this method (string) is
	 * used to write to the log file.
	 * 
	 * @access public
	 * @return string
	 */
	public function __console() {
		// We prepare the output string:
		$out = '';
		
		// First of all, we add the name of the object's class:
		$out .= '#('. $this->getClassName() .') ';
		
		// We also add the Object ID:
		$out .= '#' . $this->getCoreObjectId();
		
		$out .= M_Console::NEWLINE;
		
		// We construct an instance of ReflectionObject, in order to
		// analyze the object:
		$reflection = new ReflectionObject($this);
		
		// For each of the properties in the object
		foreach($reflection->getProperties() as $property) {
			// Start adding property to the output:
			$out .= M_Console::NEWLINE;
			
			// We add the property to the output string:
			$out .= $property->getName() . ' (';
			
			// Add the scope of the property:
			if($property->isPublic()) {
				$out .= 'public';
			}
			elseif($property->isPrivate()) {
				$out .= 'private';
			}
			elseif($property->isProtected()) {
				$out .= 'protected';
			}
			
			// Now, we add the value of the property:
			$out .= ') = ('. gettype($property->getValue()) .') ' . $property->getValue();
			
			// Add separator:
			$out .= M_Console::NEWLINE;
			$out .= '--';
		}
	}
	
	/**
	 * To string
	 * 
	 * Called automatically by PHP when the object is being casted to a string. 
	 * For example, an echo call on the object will print the result of this 
	 * function.
	 * 
	 * @access public
	 * @return string
	 */
	public function __toString() {
		return 'Object(' . $this->getClassName() . '){ ObjectID : ' . $this->getCoreObjectId() . ' }';
	}
}