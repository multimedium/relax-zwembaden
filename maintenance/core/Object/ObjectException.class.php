<?php
/**
 * M_ObjectException class
 * 
 * Exceptions thrown by {@link M_Object} and {@link M_ObjectSingleton}
 * 
 * @package Core
 */
class M_ObjectException extends M_Exception {
}