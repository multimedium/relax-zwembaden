<?php
/**
 * Javascript holder
 * 
 * This class holds all javascript which can be added to a View. By gathering
 * all javascript instead of outputting it directly we have more control and
 * e.g. we can output it to screen on the end of the file
 */
class M_ViewJavascript extends M_ViewPageElement {
	
	/**
	 * Singleton object
	 * 
	 * M_ViewJavascript is a singleton object. This property holds the 
	 * singleton. Note that it is defaulted to NULL, and no M_ViewJavascript
	 * object will be constructed until specifically requested by
	 * other classes.
	 *
	 * @access private
	 * @var M_ViewJavascript
	 */
	private static $_instance;
	
	/**
	 * Private Constructor
	 * 
	 * The signature on the M_ViewJavascript class constructor is PRIVATE.
	 * This way, we force the M_ViewJavascript object to be a singleton, as
	 * objects need to be constructed with the {@link M_ViewJavascript::getInstance()}
	 * method.
	 *
	 * @access private
	 * @return M_ViewJavascript
	 */
	private function __construct() {
	}
	
	/**
     * Get a instance M_ViewJavascript instance
     * 
     * @return M_ViewJavascript
     */
    public function getInstance() {
    	if(self::$_instance == NULL) {
			self::$_instance = new self;
		}
		return self::$_instance;
    }
   
    /**
	 * Defines the starting tag for inline content
	 * 
	 * @return string
	 */
    protected function _getInlineStartingTag() {
    	return '<script type="text/javascript"><!--';
    }
    
    /**
	 * Defines the closing tag for inline content
	 * 
	 * @return string
	 */
    protected function _getInlineClosingTag() {
    	return '--></script>';
    }
    
    /**
     * Use a file to get valid (x)html sourcecode to link with a css
     * 
     * @param M_File $file
     * @return string
     */
    protected function _getFileCode(M_File $file) {
		$output = M_CodeHelper::getNewLineWithTab(1,1);
		$output .= '<script type="text/javascript" src="';
		$output .= M_Request::getLinkWithoutPrefix(M_Loader::getRelative($file->getPath()));
		$output .= '"></script>';
		
		return $output;
    }
    
    /**
     * Use a file to get valid (x)html sourcecode to link with a css
     * 
     * @param M_Uri $uri
     * @return string
     */
    protected function _getExternalCode(M_Uri $uri) {
		$output = M_CodeHelper::getNewLineWithTab(1,1);
		$output .= '<script type="text/javascript" src="';
		$output .= htmlentities($uri->toString());
		$output .= '"></script>';
		
		return $output;
    }
    
    /**
     * Use inline css-data to use in a html file
     * 
     * @param M_File $file
     * @return string
     */
    protected function _getInlineCode($data) {
    	$output = M_CodeHelper::getNewLine(1) . '<script type="text/javascript"><!--';
    	$output .= M_CodeHelper::getNewLine(1) . $data;
    	$output .= M_CodeHelper::getNewLine(1) . '--></script>';
    	
    	return $output;
    }
}