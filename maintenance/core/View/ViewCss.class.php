<?php
/**
 * Css holder
 * 
 * This class holds all css which can be added to a View. By gathering
 * all css instead of outputting it directly we have more control about which
 * css has been added and we can prevent double css-links
 */
class M_ViewCss extends M_ViewPageElement {
	
	/**
	 * Singleton object
	 * 
	 * M_ViewCss is a singleton object. This property holds the 
	 * singleton. Note that it is defaulted to NULL, and no M_ViewCss
	 * object will be constructed until specifically requested by
	 * other classes.
	 *
	 * @access private
	 * @var M_ViewCss
	 */
	private static $_instance;
	
	/**
	 * Private Constructor
	 * 
	 * The signature on the M_ViewCss class constructor is PRIVATE.
	 * This way, we force the M_ViewCss object to be a singleton, as
	 * objects need to be constructed with the {@link M_ViewCss::getInstance()}
	 * method.
	 *
	 * @access private
	 * @return M_ViewCss
	 */
	private function __construct() {
	}
	
	/**
     * Get a instance M_ViewCss instance
     * 
     * @return M_ViewCss
     */
    public static function getInstance() {
    	if(self::$_instance == NULL) {
			self::$_instance = new self;
		}
		return self::$_instance;
    }
   
    /**
	 * Defines the starting tag for inline content
	 * 
	 * @return string
	 */
    protected function _getInlineStartingTag() {
    	return '<style type="text/css">';
    }
    
    /**
	 * Defines the closing tag for inline content
	 * 
	 * @return string
	 */
    protected function _getInlineClosingTag() {
    	return '</style>';
    }
    
    /**
     * Use a file to get valid (x)html sourcecode to link with a css
     * 
     * @param M_File $file
     * @return string
     */
    protected function _getFileCode(M_File $file) {
		$output = M_CodeHelper::getNewLineWithTab(1,1);
		$output .= '<link rel="stylesheet" type="text/css" href="';
		$output .= M_Request::getLinkWithoutPrefix(M_Loader::getRelative($file->getPath()));
		$output .= '" media="screen" />';
		
		return $output;
    }
    
    /**
     * Use a file to get valid (x)html sourcecode to link with a css
     * 
     * @param M_Uri $uri
     * @return string
     */
    protected function _getExternalCode(M_Uri $uri) {
		$output = M_CodeHelper::getNewLineWithTab(1,1);
		$output .= '<link rel="stylesheet" type="text/css" href="';
		$output .= htmlentities($uri->toString());
		$output .= '" media="screen" />';
		
		return $output;
    }
    
    /**
     * Use inline css-data to use in a html file
     * 
     * @param M_File $file
     * @return string
     */
    protected function _getInlineCode($data) {
    	$output = M_CodeHelper::getNewLine(1) . '<style type="text/css">';
    	$output .= M_CodeHelper::getNewLine(1) . $data;
    	$output .= M_CodeHelper::getNewLine(1) . '</style>';
    	
    	return $output;
    }
}