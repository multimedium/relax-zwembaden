<?php
/**
 * M_ViewException class
 * 
 * Exceptions thrown by {@link M_View} and subclasses.
 *
 * @package Core
 */
class M_ViewException extends M_Exception {
}
?>