<?php
/**
 * M_Password class
 * 
 * M_Password is used to calculate the strength of a password, to
 * generate a new password, etc.
 * 
 * @package Core
 */
class M_Password extends M_Object {
	/**
	 * Calculate strength
	 * 
	 * This method will calculate the strength of a given password.
	 * It will return a rating from 0 to 100, 0 being the weakest
	 * evaluation, and 100 being the strongest evaluation.
	 * 
	 * @access public
	 * @param string $password
	 * 		The password to be evaluated
	 * @return float
	 */
	public function getStrength($password) {
		$score = 0;
		
		//password length
		$password_length = strlen($password);
		$score += $password_length * 4;
		
		$score += strlen(M_Helper::getUniqueCharsInString(1, $password)) - $password_length;
		$score += strlen(M_Helper::getUniqueCharsInString(2, $password)) - $password_length;
		$score += strlen(M_Helper::getUniqueCharsInString(3, $password)) - $password_length;
		$score += strlen(M_Helper::getUniqueCharsInString(4, $password)) - $password_length;
		
		// password has 3 numbers
		if (preg_match('/(.*[0-9].*[0-9].*[0-9])/', $password)) {
			$score += 5;
		}
		
		// password has 2 symbols
		if (preg_match('/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/', $password)) {
			$score += 5;
		}
		
		// password has Upper and Lower chars
		if (preg_match('/([a-z].*[A-Z])|([A-Z].*[a-z])/', $password)) {
			$score += 10;
		}
		
		// password has number and chars
		if (preg_match('/([a-zA-Z])/', $password) && preg_match('/([0-9])/', $password)) {
			$score += 15;
		}
		
		// password has number and symbol
		if (preg_match('/([!,@,#,$,%,^,&,*,?,_,~])/', $password) && preg_match('/([0-9])/', $password)) {
			$score += 15;
		}
		
		// password has char and symbol
		if (preg_match('/([!,@,#,$,%,^,&,*,?,_,~])/', $password) && preg_match('/([a-zA-Z])/', $password)) {
			$score += 15;
		}
		
		//password is just a nubers or chars
		if (preg_match('/^\w+$/', $password) || preg_match('/^\d+$/', $password)) {
			$score -= 10;
		}
		
		//verifing 0 < score < 100
		if ($score < 0)  {
			$score = 0;
		}
		
		if ($score > 100) {
			$score = 100;
		}
		
		return $score;
	}
	
	/**
	 * Generate a new password
	 * 
	 * This method will generate a new password. It will generate a
	 * string (of a given length) that contains randomly picked 
	 * characters (which includes lowercase, uppercase, numbers and 
	 * symbols).
	 * 
	 * @access public
	 * @param integer $length
	 * 		The length of the password
	 * @return string
	 */
	public static function getNew($length = 8) {
		$chars = array(
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
			'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 
			'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
			'Y', 'Z',
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '$', '*', 
			'#', '@', '&', '%'
		);
		
		// Shuffle the array of characters a couple of times
		for($i = 0; $i < 5; $i ++) {
			shuffle($chars);
		}
		
		// For the new password, we glue the first n characters of
		// the scrambled array together. The number of characters is
		// defined by the requested password length
		return implode('', array_slice($chars, 0, $length));
	}
}