<?php
/**
 * M_ArrayIterator class
 * 
 * @package Core
 * @author Ben Brughmans
 */
class M_ArrayIterator extends ArrayIterator {
	
	/**
	 * Create a recursive array
	 * 
	 * When an ArrayIterator holds a list of {@link M_DataObject} or arrays,
	 * we can use this method to create easily a recursive array containing
	 * both for key as value data of this objects or arrays.
	 * 
	 * @example 
	 * //get a list of media-items
	 * $list = $mediaMapper()->getAll();
	 * //create a recursive array with id as key and title as value
	 * $array = $list->toRecursiveArray('getId', 'getTitle');
	 * //create a form select-field and use the array as options
	 * $selectField = new M_FieldSelect();
	 * $selectField->setOptions($array);
	 * 
	 * @param string $key Method name, or array property, which is used as key. 
	 * 	Or leave empty to use default index
	 * @param string $value Method name, or array property, which is used as value
	 * @return array
	 */
	public function toRecursiveArray($key = null, $value) {
		$array = array();
		$i = 0;
		$this->rewind();
		while($this->valid()) {
			$element = $this->current();
			
			//if $element is an object, $key and $value stand for the method
			//names of $element
			if (is_object($element)) {
				//check if both methods are callable
				if ((!is_null($key) && !method_exists($element,$key)) || !method_exists($element, $value)) {
					throw new M_Exception(	
						sprintf(
							'Either %s or %s is not callable on an object in this ArrayIterator', 
							$key, 
							$value
						)
					);
				}
				$keyValue = !is_null($key) ? call_user_func(array($element, $key)) : $i;
				$valueValue = call_user_func(array($element, $value));
			}
			//if $el is an array, $key and $value stand for array-properties
			//of $el
			else {
				$keyValue = !is_null($key) ? M_Helper::getArrayElement($key, $element) : $i;
				$valueValue = M_Helper::getArrayElement($value, $element);
			}
			
			//store the data in a recursive array
			$array[$keyValue] = $valueValue;
			
			//continue
			$i++;			
			$this->next();
		}
		$this->rewind();
		return $array;
	}

	/**
	 * Shuffle
	 *
	 * Randomize the results of this M_ArrayIterator. In fact a new M_ArrayIterator
	 * is created based on the shuffled contents of the array-copy.
	 * 
	 * @return M_ArrayIterator
	 */
	public function shuffle() {
		$arrayCopy = $this->getArrayCopy();
		shuffle($arrayCopy);
		return new M_ArrayIterator($arrayCopy);
	}
	
	/**
	 * Get last element of ArrayIterator
	 *
	 * @return mixed
	 */
	public function last() {
		return array_pop($this->getArrayCopy());
	}
	
	/**
	 * Get first element of ArrayIterator
	 *
	 * @return mixed
	 */
	public function first() {
		return array_shift($this->getArrayCopy());
	}
}