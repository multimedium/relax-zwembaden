<?php
/**
 * M_LocaleMessage class
 * 
 * Objects of M_LocaleMessage are used to represent translated strings.
 * Note however that they should not be used to translate texts in your
 * code. For that, you should use {@link t()}. For more information on
 * translating texts, read {@link M_LocaleMessageCatalog}.
 * 
 * Objects of M_LocaleMessage are used as an intermediate object between
 * {@link M_LocaleMessageCatalog} and {@link M_LocalePortableObject}.
 * Typically, it is used to represent translated strings in an exported
 * or imported .po file.
 * 
 * @package Core
 */
class M_LocaleMessage {
	/**
	 * Constant to address a comment type
	 * 
	 * This constant is used to address a given type of comment on
	 * a translated string. This particular constant is used to address
	 * a Translator Comment
	 */
	const TRANSLATOR_COMMENT = '# ';
	
	/**
	 * Constant to address a comment type
	 * 
	 * This constant is used to address a given type of comment on
	 * a translated string. This particular constant is used to address
	 * an Extracted Comment
	 */
	const EXTRACTED_COMMENT = '#. ';
	
	/**
	 * Constant to address a comment type
	 * 
	 * This constant is used to address a given type of comment on
	 * a translated string. This particular constant is used to address
	 * a Reference
	 */
	const REFERENCE = '#: ';
	
	/**
	 * Constant to address a comment type
	 * 
	 * This constant is used to address a given type of comment on
	 * a translated string. This particular constant is used to address
	 * a Flag
	 */
	const FLAG = '#, ';
	
	/**
	 * Constant to address a comment type
	 * 
	 * This constant is used to address a given type of comment on
	 * a translated string. This particular constant is used to address
	 * the Previous Untranslated String
	 */
	const PREVIOUS_UNTRANSLATED_STRING = '#| ';
	
	/**
	 * Untranslated string; singular
	 * 
	 * @access private
	 * @var string
	 */
	private $_msgid;
	
	/**
	 * Untranslated string; plural
	 * 
	 * @access private
	 * @var string
	 */
	private $_msgid_plural;
	
	/**
	 * Translated string(s)
	 * 
	 * @access private
	 * @var array
	 */
	private $_msgstr = array();
	
	/**
	 * Comments on the message
	 * 
	 * This property stores the comments that have been made on the
	 * message. Note that this property will store all different types
	 * of comments. For more info on the types, check the docs on:
	 * 
	 * - {@link M_LocaleMessage::TRANSLATOR_COMMENT}
	 * - {@link M_LocaleMessage::EXTRACTED_COMMENT}
	 * - {@link M_LocaleMessage::REFERENCE}
	 * - {@link M_LocaleMessage::FLAG}
	 * - {@link M_LocaleMessage::PREVIOUS_UNTRANSLATED_STRING}
	 * 
	 * @access private
	 * @var array
	 */
	private $_comments = array();
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $untranslated
	 * 		The untranslated string
	 * @return M_LocaleMessage
	 */
	public function __construct($untranslated = NULL) {
		$this->_msgid = $untranslated;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get untranslated string; singular
	 * 
	 * This method can be used to retrieve the untranslated string from
	 * the {@link M_LocaleMessage}. The return value is the untranslated
	 * string in singular form.
	 * 
	 * @access public
	 * @return string
	 */
	public function getUntranslatedString() {
		return $this->_msgid;
	}
	
	/**
	 * Get untranslated string; plural
	 * 
	 * This method can be used to retrieve the untranslated string from
	 * the {@link M_LocaleMessage}. The return value is the untranslated
	 * string in plural form.
	 * 
	 * Since the untranslated strings are treated in the context of a
	 * Germanic language, it is assumed that there is only one plural
	 * form. So there is no need to provide with a number to indicate
	 * which plural form you want to retrieve; there is only one.
	 * 
	 * NOTE:
	 * If the message does not come with a plural form, this method
	 * will return (boolean) FALSE instead. You can check whether or 
	 * not the message has a plural form, by calling the method
	 * {@link M_LocaleMessage::hasPluralForm()}
	 * 
	 * @access public
	 * @return string|boolean
	 */
	public function getUntranslatedPluralString() {
		if($this->_msgid_plural) {
			return $this->_msgid_plural;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get translated string; singular
	 * 
	 * This method can be used to retrieve the translated string, in
	 * singular form.
	 * 
	 * NOTE:
	 * If no translated string could have been found, this method will
	 * return (boolean) FALSE instead.
	 * 
	 * @access public
	 * @return string
	 */
	public function getTranslatedString() {
		if(isset($this->_msgstr[0])) {
			return $this->_msgstr[0];
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Has Plural Forms?
	 * 
	 * This method will tell if the message has a plural form. If the
	 * message has a plural form, you can use
	 * 
	 * - {@link M_LocaleMessage::getUntranslatedPluralString()}
	 * - {@link M_LocaleMessage::getTranslatedPluralString()}
	 * 
	 * to fetch the untranslated and translated plural forms respectively.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function hasPluralForm() {
		if($this->_msgid_plural) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get number of plural forms
	 * 
	 * This method will return the number of translated (!) plural 
	 * forms are available for the message. You can use this method, in 
	 * combination with {@link M_LocaleMessage::getTranslatedPluralString()},
	 * to fetch all translated plural forms for the message.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfPluralForms() {
		return (count($this->_msgstr) - 1);
	}
	
	/**
	 * Get translated string; plural
	 * 
	 * This method can be used to retrieve the translated string from
	 * the {@link M_LocaleMessage}. The return value is the translated
	 * string in plural form.
	 * 
	 * Note that more than one plural form may be available for the 
	 * message. You can supply the index of the plural form to this
	 * method, in order to lookup a specific plural form. Check out
	 * the following example, which fetches all plural forms from the
	 * message:
	 * 
	 * <code>
	 *    $message = new M_LocaleMessage;
	 *    for($i = 0, $n = $message->getNumberOfPluralForms(); $i < $n; $i ++) {
	 *       echo $message->getTranslatedPluralString($i + 1);
	 *    }
	 * </code>
	 * 
	 * Read {@link M_LocaleMessageCatalog::getPluralText()} for more
	 * information about translated plural strings.
	 * 
	 * NOTE:
	 * If the requested plural form could not have been found, this 
	 * method will return (boolean) FALSE instead.
	 * 
	 * @access public
	 * @param integer $n
	 * 		The index of the plural form. Should be greater then 0 (ZERO)
	 * @return string|boolean
	 */
	public function getTranslatedPluralString($n = 1) {
		if($n < 1) {
			$n = 1;
		}
		if(isset($this->_msgstr[$n])) {
			return $this->_msgstr[$n];
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get comments
	 * 
	 * This method can be used to get the comments that have been made
	 * on the message. Note that different types of comments exist, and
	 * you can retrieve each type separately. For more info on comment
	 * types:
	 * 
	 * - {@link M_LocaleMessage::TRANSLATOR_COMMENT}
	 * - {@link M_LocaleMessage::EXTRACTED_COMMENT}
	 * - {@link M_LocaleMessage::REFERENCE}
	 * - {@link M_LocaleMessage::FLAG}
	 * - {@link M_LocaleMessage::PREVIOUS_UNTRANSLATED_STRING}
	 * 
	 * So, you could consider the following example code that fetches
	 * the translator's comments on the translated string:
	 * 
	 * <code>
	 *    $message = new M_LocaleMessage();
	 *    $message->getComment(M_LocaleMessage::TRANSLATOR_COMMENT);
	 * </code>
	 * 
	 * NOTE:
	 * If the requested comment could not have been found, this method
	 * will return (boolean) FALSE instead.
	 * 
	 * @access public
	 * @param string $type
	 * 		The type of the comment you want to retrieve
	 * @return string|boolean
	 */
	public function getComment($type) {
		if(isset($this->_comments[$type])) {
			return $this->_comments[$type];
		} else {
			return FALSE;
		}
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set untranslated string; singular
	 * 
	 * This method can be used to set the untranslated string in singular
	 * form. Use {@link M_LocaleMessage::getUntranslatedString()} to
	 * retrieve the untranslated string later.
	 * 
	 * @access public
	 * @param string $msgid
	 * 		The untranslated string
	 * @return void
	 */
	public function setUntranslatedString($msgid) {
		$this->_msgid = (string) $msgid;
	}
	
	/**
	 * Set untranslated string; plural
	 * 
	 * This method can be used to set the untranslated string in plural
	 * form. Use {@link M_LocaleMessage::getUntranslatedPluralString()}
	 * to retrieve the untranslated string later.
	 * 
	 * Since the untranslated strings are treated in the context of a
	 * Germanic language, it is assumed that there is only one plural
	 * form. So there is no need to provide with a number to indicate
	 * which plural form you want to set; there is only one.
	 * 
	 * @access public
	 * @param string $msgid
	 * 		The untranslated string
	 * @return void
	 */
	public function setUntranslatedPluralString($msgid) {
		$this->_msgid_plural = (string) $msgid;
	}
	
	/**
	 * Set translated string; singular
	 * 
	 * This method can be used to set the translated string in singular
	 * form. Use {@link M_LocaleMessage::getTranslatedString()} to 
	 * retrieve the translated string later.
	 * 
	 * @access public
	 * @param string $msgstr
	 * 		The translated string
	 * @return void
	 */
	public function setTranslatedString($msgstr) {
		$this->_msgstr[0] = (string) $msgstr;
	}
	
	/**
	 * Set translated string; plural
	 * 
	 * This method can be used to set the translated string in plural
	 * form. Use {@link M_LocaleMessage::getTranslatedPluralString()} 
	 * to retrieve the translated string later.
	 * 
	 * Similar to {@link M_LocaleMessage::getTranslatedPluralString()},
	 * you need to supply the index of the plural form to this method, 
	 * in order to set the translated version of a specific plural form.
	 * 
	 * Read {@link M_LocaleMessageCatalog::getPluralText()} for more
	 * information about translated plural strings.
	 * 
	 * @access public
	 * @param integer $n
	 * 		The index of the plural form. Should be greater then 0 (ZERO)
	 * @param string $msgstr
	 * 		The translated string
	 * @return void
	 */
	public function setTranslatedPluralString($n, $msgstr) {
		if($n < 1) {
			$n = 1;
		}
		$this->_msgstr[$n] = (string) $msgstr;
	}
	
	/**
	 * Set comments
	 * 
	 * This method can be used to add comments to the message. Note 
	 * that different types of comments exist, and you can set each 
	 * type separately. For more info on comment types:
	 * 
	 * - {@link M_LocaleMessage::TRANSLATOR_COMMENT}
	 * - {@link M_LocaleMessage::EXTRACTED_COMMENT}
	 * - {@link M_LocaleMessage::REFERENCE}
	 * - {@link M_LocaleMessage::FLAG}
	 * - {@link M_LocaleMessage::PREVIOUS_UNTRANSLATED_STRING}
	 * 
	 * So, you could consider the following example code that sets
	 * the translator's comments on the translated string:
	 * 
	 * <code>
	 *    $message = new M_LocaleMessage();
	 *    $message->setComment(M_LocaleMessage::TRANSLATOR_COMMENT, 'My Comments');
	 * </code>
	 * 
	 * NOTE:
	 * If the comment type could not have been recognized, this method
	 * will throw an exception.
	 * 
	 * @throws M_LocaleException
	 * @access public
	 * @param string $type
	 * 		The type of the comment you want to set
	 * @param string $comment
	 * 		The comment
	 * @return void
	 */
	public function setComment($type, $comment) {
		switch($type) {
			case self::EXTRACTED_COMMENT:
			case self::FLAG:
			case self::PREVIOUS_UNTRANSLATED_STRING:
			case self::REFERENCE:
			case self::TRANSLATOR_COMMENT:
				$this->_comments[$type] = (string) $comment;
				break;
			
			default:
				throw new M_LocaleException(sprintf('%s: Unrecognized comment type: %s', __CLASS__, $type));
		}
	}
}
?>