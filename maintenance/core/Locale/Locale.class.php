<?php
/**
 * M_Locale class
 * @package Core
 */

/**
 * Load the M_LocaleMessageCatalog class
 * 
 * We load this class, in order to "autoload" the {@link t()} function. 
 * Once the Locale API is involved, {@link t()} should be available.
 */
M_Loader::loadCoreClass('LocaleMessageCatalog');

/**
 * Define the default path to locale data files (ldml)
 * 
 * The value of this constant is used to set the default location of locale
 * data files. For more info about locale data files, read the docs on
 * {@link M_LocaleDataLDML}
 */
define('FOLDER_DEFAULT_LOCALE_DATA', FOLDER_ROOT . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . FOLDER_THIRDPARTY . DIRECTORY_SEPARATOR . 'localedata');

/**
 * M_Locale class
 * 
 * Translation is only one aspect of locales. There are many 
 * attributes that are needed to define a country's cultural 
 * conventions. These attributes include beside the country's 
 * native language, the formatting of the date and time, the 
 * representation of numbers, the symbols for currency, etc. 
 * These local rules are termed the country's locale. The locale 
 * represents the knowledge needed to support the country's native 
 * attributes.
 * 
 * PHP's setlocale() functions are not safe in multithreaded 
 * environments, so we cannot rely on these functions to work 
 * with locale dependent values. The locale may be changed at 
 * runtime, even though we do not change it explicitly in our 
 * own script.
 * 
 * @package Core
 */
class M_Locale extends M_Object {
	/**
	 * Constant to describe locale category
	 *
	 * This constant is used to improve code readability, and to improve 
	 * the programming experience (code hinting). It's much easier to 
	 * choose a constant from a list then to remember the actual string 
	 * values :)
	 */
	const LANGUAGE    = 'LANGUAGE';
	
	/**
	 * Constant to describe locale category
	 *
	 * This constant is used to improve code readability, and to improve 
	 * the programming experience (code hinting). It's much easier to 
	 * choose a constant from a list then to remember the actual string 
	 * values :)
	 */
	const LC_ALL      = 'LC_ALL';
	
	/**
	 * Constant to describe locale category
	 *
	 * This constant is used to improve code readability, and to improve 
	 * the programming experience (code hinting). It's much easier to 
	 * choose a constant from a list then to remember the actual string 
	 * values :)
	 */
	const LC_MESSAGES = 'LC_MESSAGES';
	
	/**
	 * Constant to describe locale category
	 *
	 * This constant is used to improve code readability, and to improve 
	 * the programming experience (code hinting). It's much easier to 
	 * choose a constant from a list then to remember the actual string 
	 * values :)
	 */
	const LC_TIME     = 'LC_TIME';
	
	/**
	 * Constant to describe locale category
	 *
	 * This constant is used to improve code readability, and to improve 
	 * the programming experience (code hinting). It's much easier to 
	 * choose a constant from a list then to remember the actual string 
	 * values :)
	 */
	const LC_NUMERIC  = 'LC_NUMERIC';
	
	/**
	 * Constant to describe locale category
	 *
	 * This constant is used to improve code readability, and to improve 
	 * the programming experience (code hinting). It's much easier to 
	 * choose a constant from a list then to remember the actual string 
	 * values :)
	 */
	const LC_MONETARY = 'LC_MONETARY';
	
	/**
	 * Constant to describe locale category
	 *
	 * This constant is used to improve code readability, and to improve 
	 * the programming experience (code hinting). It's much easier to 
	 * choose a constant from a list then to remember the actual string 
	 * values :)
	 */
	const LANG        = 'LANG';
	
	/**
	 * Reserved Session Namespace.
	 * 
	 * To make the choice of a locale persistent through different page
	 * requests, M_Locale reserves an {@link M_SessionNamespace} for
	 * internal use. This constant holds the name of that namespace.
	 */
	const _SESSION_NAMESPACE = '_M_NS';
	
	/**
	 * Locale category
	 * 
	 * This property holds the values of the locale categories. The
	 * value of this property is set with the {@link M_Locale::setCategory()}
	 * method.
	 * 
	 * @access public
	 * @var array
	 */
	private static $_category = array();
	
	/**
	 * Locale Data Directory
	 * 
	 * This property holds the directory where locale data files 
	 * (LDML files) are located. The value of this property is set
	 * with the {@link M_Locale::setDataDirectory()} method.
	 *
	 * @access public
	 * @var string
	 */
	private static $_dataDirectory = FOLDER_DEFAULT_LOCALE_DATA;
	
	/**
	 * Constructor
	 * 
	 * Please note that the (private) signature on the M_Locale constructor
	 * prevents a M_Locale object from being constructed. All methods of 
	 * the M_Locale class are called statically, to highlight the fact that
	 * the changes made to M_Locale affect the entire Locale API. There is 
	 * no need to instantiate an object of the M_Locale class.
	 *
	 * @access private
	 * @return M_Locale
	 */
	private function __construct() {
	}
	
	/**
	 * Get locale name parts
	 * 
	 * A locale name usually has the form ‘ll_CC’. Here ‘ll’ is an 
	 * ISO 639 two-letter language code, and ‘CC’ is an ISO 3166 two-letter 
	 * country code. For example, for German in Germany, ll is de, and CC 
	 * is DE.
	 * 
	 * You might think that the country code specification is redundant. 
	 * But in fact, some languages have dialects in different countries. 
	 * For example, ‘de_AT’ is used for Austria, and ‘pt_BR’ for Brazil. 
	 * The country code serves to distinguish the dialects.
	 * 
	 * Many locale names have an extended syntax ‘ll_CC.encoding’ that also 
	 * specifies the character encoding. These are in use because between 
	 * 2000 and 2005, most users have switched to locales in UTF-8 encoding. 
	 * For example, the German locale on glibc systems is nowadays 
	 * ‘de_DE.UTF-8’. The older name ‘de_DE’ still refers to the German 
	 * locale as of 2000 that stores characters in ISO-8859-1 encoding – a 
	 * text encoding that cannot even accomodate the Euro currency sign.
	 * 
	 * Some locale names use ‘ll_CC@variant’ instead of ‘ll_CC’. The 
	 * ‘@variant’ can denote any kind of characteristics that is not 
	 * already implied by the language ll and the country CC. It can denote 
	 * a particular monetary unit. For example, on glibc systems, 
	 * ‘de_DE@euro’ denotes the locale that uses the Euro currency, in 
	 * contrast to the older locale ‘de_DE’ which implies the use of the 
	 * currency before 2002. It can also denote a dialect of the language, 
	 * or the script used to write text (for example, ‘sr_RS@latin’ uses 
	 * the Latin script, whereas ‘sr_RS’ uses the Cyrillic script to write 
	 * Serbian), or the orthography rules, or similar. 
	 * 
	 * This method will accept a locale name of above-described format,
	 * and returns an associative array with information about the 
	 * provided locale name. The array will have the following keys:
	 * 
	 * - language
	 * - country
	 * - encoding
	 * - variant
	 * 
	 * Example 1
	 * <code>
	 *    print_r(M_Locale::getNameParts('de_DE@euro'));
	 * </code>
	 * 
	 * The above example will output:
	 * 
	 * <code>
	 *    Array (
	 *       'language' => 'de',
	 *       'country' => 'DE',
	 *       'encoding' => NULL,
	 *       'variant' => 'euro'
	 *    )
	 * </code>
	 * 
	 * @access public
	 * @param string $locale
	 * 		The locale name from which to extract the name parts
	 * @return array
	 */
	public static function getNameParts($locale) {
		$part1 = explode('@', $locale);
		$part0 = explode('_', $part1[0]);
		$part1 = isset($part1[1]) ? explode('.', $part1[1]) : array(NULL,NULL);
		return array(
			'language' => $part0[0],
			'country'  => isset($part0[1]) ? $part0[1] : NULL,
			'encoding' => isset($part1[1]) ? $part1[1] : NULL,
			'variant'  => $part1[0]
		);
	}
	
	/**
	 * Get language display name
	 * 
	 * This method will return the name of the requested language, in
	 * the given locale name. 
	 * 
	 * The locale name is defaulted to the one that has been set in the
	 * locale category LC_MESSAGES, see {@link M_Locale::getCategory()} 
	 * for more info. The locale's data will be used to fetch locale 
	 * data (display names)
	 * 
	 * IMPORTANT NOTE:
	 * If no locale name has been provided to this method, and no locale
	 * has been set in any of the locale categories, this method will
	 * return the original language code
	 * 
	 * @access public
	 * @param string $language
	 * 		The language of which to retrieve the display name
	 * @param string $locale
	 * 		The locale in which to look for a display name
	 * @return string
	 */
	public static function getLanguageDisplayName($language, $locale = NULL) {
		// if locale is not provided, set to locale that has been set 
		// in the LC_MESSAGES category
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
			if(!$locale) {
				return $language;
			}
		}
		
		// Get locale's data:
		$data = M_LocaleData::getInstance($locale);
		
		// Ask locale data for display names:
		return $data->getDisplayName('language', $language);
	}
	
	/**
	 * Get territory display name
	 * 
	 * This method will return the name of the requested territory, in
	 * the given locale name. 
	 * 
	 * The locale name is defaulted to the one that has been set in the
	 * locale category LC_MESSAGES, see {@link M_Locale::getCategory()} 
	 * for more info. The locale's data will be used to fetch locale 
	 * data (display names)
	 * 
	 * IMPORTANT NOTE:
	 * If no locale name has been provided to this method, and no locale
	 * has been set in any of the locale categories, this method will
	 * return the original territory code
	 * 
	 * @access public
	 * @param string $territory
	 * 		The territory of which to retrieve the display name
	 * @param string $locale
	 * 		The locale in which to look for a display name
	 * @return string
	 */
	public static function getTerritoryDisplayName($territory, $locale = NULL) {
		// if locale is not provided, set to locale that has been set 
		// in the LC_MESSAGES category
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
			if(!$locale) {
				return $territory;
			}
		}
		
		// Get locale's data:
		$data = M_LocaleData::getInstance($locale);
		
		// Ask locale data for display names:
		return $data->getDisplayName('territory', strtoupper($territory));
	}
	
	/**
	 * Get country code
	 * 
	 * This method will return the ISO Code of the requested country.
	 * 
	 * <code>
	 *    M_Locale::getCountryCode('Alemania', 'es'); // will return "de"
	 * </code>
	 * 
	 * IMPORTANT NOTE:
	 * If no locale name has been provided to this method, and no locale
	 * has been set in any of the locale categories, this method will
	 * return FALSE instead.
	 * 
	 * @access public
	 * @param string $country
	 * 		The country of which to retrieve the country code
	 * @param string $locale
	 * 		The locale in which to look for a country code
	 * @return string
	 */
	public static function getCountryCode($country, $locale = NULL) {
		// if locale is not provided, set to locale that has been set 
		// in the LC_MESSAGES category
		if(!$locale) {
			$locale = M_Locale::getCategory(M_Locale::LC_MESSAGES);
			if(!$locale) {
				return FALSE;
			}
		}
		
		// Get locale's data:
		$data = M_LocaleData::getInstance($locale);
		
		// Ask locale data for display names:
		return $data->getIsoCode('territory', $country);
	}
	
	/**
	 * Get locale category
	 * 
	 * Translation is only one aspect of locales. There are many 
	 * attributes that are needed to define a country's cultural 
	 * conventions. These attributes include beside the country's 
	 * native language, the formatting of the date and time, the 
	 * representation of numbers, the symbols for currency, etc. 
	 * These local rules are termed the country's locale. The locale 
	 * represents the knowledge needed to support the country's native 
	 * attributes. 
	 * 
	 * These areas of cultural conventions are called locale categories. 
	 * It is an unfortunate term; locale aspects or locale feature 
	 * categories would be a better term, because each “locale category” 
	 * describes an area or task that requires localization. The 
	 * concrete data that describes the cultural conventions for such 
	 * an area and for a particular culture is also called a locale 
	 * category. In this sense, a locale is composed of several locale 
	 * categories: the locale category describing the codeset, the 
	 * locale category describing the formatting of numbers, the 
	 * locale category containing the translated messages, and so on. 
	 * 
	 * When a program looks up locale dependent values, it does this 
	 * according to the following environment variables, in priority 
	 * order: 
	 * 
	 * <code>
	 *    M_Locale::LANGUAGE
	 *    M_Locale::LC_ALL
	 *    M_Locale::LC_MESSAGES
	 *    M_Locale::LC_TIME
	 *    M_Locale::LC_NUMERIC
	 *    M_Locale::LC_MONETARY
	 *    M_Locale::LANG
	 * </code>
	 * 
	 * LANG is the normal environment variable for specifying a locale. 
	 * As a user, you normally set this variable.
	 * 
	 * LC_CTYPE, LC_NUMERIC, LC_TIME, LC_COLLATE, LC_MONETARY, LC_MESSAGES
	 * are the environment variables meant to override LANG and affecting 
	 * a single locale category only. For example, assume you are a Swedish 
	 * user in Spain, and you want your programs to handle numbers and dates 
	 * according to Spanish conventions, and only the messages should be 
	 * in Swedish. Then you could set the LANG variable to es_ES and the 
	 * LC_MESSAGES variable to sv_SE.
	 * 
	 * LC_ALL is an environment variable that overrides all of these.
	 * 
	 * However, not all programs have translations for all languages. By 
	 * default, an English message is shown in place of a nonexistent 
	 * translation. If you understand other languages, you can set up a 
	 * priority list of languages. This is done through a different 
	 * environment variable, called LANGUAGE. M_Locale gives preference 
	 * to LANGUAGE over LC_ALL and LANG for the purpose of message handling,
	 * For example, some Swedish users who would rather read translations 
	 * in German than English for when Swedish is not available, set 
	 * LANGUAGE to "sv:de" while leaving LANG to "sv_SE".
	 * 
	 * Example 1, get the language of translated messages
	 * <code>
	 *    M_Locale::getCategory(M_Locale::LC_MESSAGES);
	 * </code>
	 * 
	 * Note that in example 1, M_Locale will give preference to the variables
	 * LANGUAGE/LC_ALL, in order to determine the language (or better, 
	 * locale).
	 * 
	 * To get the locale from the priority list that is defined by the 
	 * environment variable LANGUAGE, use {@link M_Locale::getLanguagePriority()}
	 * 
	 * IMPORTANT NOTE:
	 * If the requested locale category has not been set (with the method
	 * {@link M_Locale::setCategory()}), and it is not overriden by the value
	 * of another category, this method will return FALSE.
	 * 
	 * @access public
	 * @param string $category
	 * 		The locale category
	 * @return string|boolean
	 */
	public static function getCategory($category) {
		if($category != self::LC_ALL && isset(self::$_category[self::LC_ALL])) {
			return self::$_category[self::LC_ALL];
		}
		if(isset(self::$_category[$category])) {
			return self::$_category[$category];
		}
		if(isset(self::$_category[self::LANG])) {
			return self::$_category[self::LANG];
		}
		return FALSE;
	}
	
	/**
	 * Set locale category
	 * 
	 * This method is used to set a locale identifier for the given 
	 * category. Read the documentation on {@link M_Locale::getCategory()}
	 * to learn more about locale categories, and to consult the list
	 * of available locale categories.
	 * 
	 * Note that this method will throw a {@link M_LocaleException} if
	 * the specified category cannot be recognised.
	 * 
	 * Example 1, set the locale to Dutch (Belgium)
	 * <code>
	 *    M_Locale::setCategory(M_Locale::LANG, 'nl_BE');
	 * </code>
	 *
	 * @access public
	 * @throws M_LocaleException
	 * @param string $category
	 * 		The affected locale category
	 * @param string $locale
	 * 		The locale to be set in the locale category
	 * @return void
	 */
	public static function setCategory($category, $locale) {
		switch($category) {
			case self::LC_ALL:
			case self::LC_MESSAGES:
			case self::LC_TIME:
			case self::LC_NUMERIC:
			case self::LC_MONETARY:
			case self::LANG:
				self::$_category[$category] = $locale;
				break;

			case self::LANGUAGE:
				self::$_category[self::LANGUAGE] = is_array($locale) ? $locale : explode(':', $locale);
				break;

			default:
				throw new M_LocaleException('Undefined Locale Category: ' . $category);
				break;
		}
	}
	
	/**
	 * Get a language from the priority list
	 * 
	 * M_Locale gives preference to LANGUAGE over LC_ALL and LANG 
	 * for the purpose of message handling. Read the documentation on
	 * {@link M_Locale::getCategory()} to learn more.
	 * 
	 * Example 1, get a language by priority
	 * <code>
	 *    M_Locale::setCategory(M_Locale::LANGUAGE, 'nl:en:es');
	 *    echo M_Locale::getLanguagePriority(0); // outputs "nl"
	 *    echo M_Locale::getLanguagePriority(1); // outputs "en"
	 *    echo M_Locale::getLanguagePriority(2); // outputs "es"
	 * </code>
	 *
	 * @access public
	 * @param integer $priority
	 * 		The priority, starting from 0 (ZERO)
	 * @return mixed
	 * 		Returns the language code if found by the given priority,
	 * 		or FALSE if not found.
	 */
	public static function getLanguagePriority($priority) {
		if(isset(self::$_category[self::LANGUAGE])) {
			if(isset(self::$_category[self::LANGUAGE][$priority])) {
				return self::$_category[self::LANGUAGE][$priority];
			}
		}
		return FALSE;
	}

	/**
	 * Get the full locale code containing language-code and country-code.
	 *
	 * Typically this code is presented as nl_BE, in which nl stands for dutch
	 * language and BE for Belgium.
	 *
	 * If no languageCode is provided, we use {@link Locale::getCategory()} to
	 * get the active language
	 *
	 * @author Ben Brughmans
	 * @param string $countryCode Countrycode, in uppercase
	 * @param string $language Language-code, in lowercase
	 * @return string
	 */
	public static function getLocaleCode($countryCode, $languageCode = null) {
		//if no language code is given, we use the current active locale
		if (is_null($languageCode)) {
			$languageCode = self::getCategory(M_Locale::LANG);
		}
		return $languageCode . '_' . strtoupper($countryCode);
	}
	
	/**
	 * Get Locale Data
	 *
	 * This method will return a constructed {@link M_LocaleData}
	 * object, which is used to handle locale dependent values.
	 * 
	 * Example 1, set the locale to Dutch (Belgium) and get the
	 * locale data that describes the cultural conventions in 
	 * that locale.
	 * <code>
	 *    M_Locale::setCategory(M_Locale::LANG, 'nl_BE');
	 *    $data = M_Locale::getData();
	 * </code>
	 * 
	 * NOTE:
	 * If no locale data could have been located by using the
	 * locale category LANG (see {@link M_Locale::getCategory()}, 
	 * this method will return FALSE.
	 *
	 * @access public
	 * @return M_LocaleData|boolean
	 */
	public static function getData() {
		$lang = self::getCategory(self::LANG);
		if($lang) {
			return M_LocaleData::getInstance($lang);
		}
		return FALSE;
	}
	
	/**
	 * TODO: Is locale Data available?
	 * 
	 * This method will check if locale data is available for the 
	 * specified locale. Obviously, this method will return TRUE
	 * if data is available, FALSE if not.
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale identifier
	 * @return boolean
	 */
	public static function isDataAvailable($locale) {
		return !empty($locale);
	}
	
	/**
	 * TODO: Get supported locales
	 * 
	 * This method will return a list of supported languages. The
	 * return value is an array that contains the locale names (or
	 * locale identifiers)
	 *
	 * @access public
	 * @return array
	 */
	public static function getSupportedList() {
		return array(
		);
	}
	
	/**
	 * Set locale data directory
	 * 
	 * This method will set the directory where locale data files 
	 * (LDML files) are located. Read the documentation on the
	 * following classes, to learn more about locale data:
	 * 
	 * - {@link M_LocaleData}
	 * - {@link M_LocaleDataSourceLDML}
	 *
	 * @access public
	 * @param string $directory
	 * @return void
	 */
	public static function setDataDirectory($directory) {
		self::$_dataDirectory = rtrim($directory, " \t\n\r\0\x0B/\\");
	}
	
	/**
	 * Get locale data directory
	 * 
	 * This method will return the full path to the directory 
	 * where locale data files are located. Read the documentation 
	 * on the following classes, to learn more about locale data:
	 * 
	 * - {@link M_LocaleData}
	 * - {@link M_LocaleDataSourceLDML}
	 *
	 * @access public
	 * @return string
	 */
	public static function getDataDirectory() {
		return self::$_dataDirectory . DIRECTORY_SEPARATOR;
	}
}
?>