<?php
/**
 * M_LocalePortableObject class
 * 
 * Objects of this class represent Portable Object (PO) files. Check 
 * out the GNU gettext specification for more information about .po
 * files:
 * 
 * http://www.gnu.org/software/gettext/manual/gettext.html
 * 
 * This class is used to export a locale's message catalog to a .po
 * file, but can also be used to import a .po file into the message
 * catalog.
 * 
 * @package Core
 */
class M_LocalePortableObject {
	/**
	 * Portable Object Header
	 * 
	 * This constant can be used to address a specific header in the
	 * Portable Object file: Project-Id-Version
	 * 
	 * For more information about the headers in a Portable Object file,
	 * read the docs on {@link M_LocalePortableObject::getHeaders()}.
	 */
	const PROJECT_ID_VERSION = 'Project-Id-Version';
	
	/**
	 * Portable Object Header
	 * 
	 * This constant can be used to address a specific header in the
	 * Portable Object file: Report-Msgid-Bugs-To
	 * 
	 * For more information about the headers in a Portable Object file,
	 * read the docs on {@link M_LocalePortableObject::getHeaders()}.
	 */
	const REPORT_BUGS_TO = 'Report-Msgid-Bugs-To';
	
	/**
	 * Portable Object Header
	 * 
	 * This constant can be used to address a specific header in the
	 * Portable Object file: POT-Creation-Date
	 * 
	 * For more information about the headers in a Portable Object file,
	 * read the docs on {@link M_LocalePortableObject::getHeaders()}.
	 * Also, see {@link M_LocalePortableObject::setPotCreationDate()}.
	 */
	const POT_CREATION_DATE = 'POT-Creation-Date';
	
	/**
	 * Portable Object Header
	 * 
	 * This constant can be used to address a specific header in the
	 * Portable Object file: PO-Revision-Date
	 * 
	 * For more information about the headers in a Portable Object file,
	 * read the docs on {@link M_LocalePortableObject::getHeaders()}.
	 * Also, see {@link M_LocalePortableObject::setPoRevisionDate()}.
	 */
	const PO_REVISION_DATE = 'PO-Revision-Date';
	
	/**
	 * Portable Object Header
	 * 
	 * This constant can be used to address a specific header in the
	 * Portable Object file: Last-Translator
	 * 
	 * For more information about the headers in a Portable Object file,
	 * read the docs on {@link M_LocalePortableObject::getHeaders()}.
	 */
	const LAST_TRANSLATOR = 'Last-Translator';
	
	/**
	 * Portable Object Header
	 * 
	 * This constant can be used to address a specific header in the
	 * Portable Object file: Language-Team
	 * 
	 * For more information about the headers in a Portable Object file,
	 * read the docs on {@link M_LocalePortableObject::getHeaders()}.
	 */
	const LANGUAGE_TEAM = 'Language-Team';
	
	/**
	 * Portable Object Header
	 * 
	 * This constant can be used to address a specific header in the
	 * Portable Object file: Plural-Forms
	 * 
	 * For more information about the headers in a Portable Object file,
	 * read the docs on {@link M_LocalePortableObject::getHeaders()}.
	 */
	const PLURAL_FORMS = 'Plural-Forms';
	
	/**
	 * Portable Object Header
	 * 
	 * This constant can be used to address a specific header in the
	 * Portable Object file: Content-Type
	 * 
	 * The value of this header will be defaulted to UTF-8, since all
	 * texts in the core are being handled as UTF-8 encoded. Also, see
	 * {@link M_LocalePortableObject::setCharacterEncoding()}
	 * 
	 * For more information about the headers in a Portable Object file,
	 * read the docs on {@link M_LocalePortableObject::getHeaders()}.
	 */
	const CONTENT_TYPE_CHARSET = 'Content-Type-Charset';
	
	/**
	 * Number of plural forms
	 * 
	 * This property stores the number of plural forms that is defined
	 * by the Portable Object file. For more information about plural
	 * forms in Portable Object files:
	 * 
	 * http://www.gnu.org/software/gettext/manual/gettext.html#Plural-forms
	 * 
	 * @access private
	 * @var integer
	 */
	private $_numberOfPluralForms = NULL;
	
	/**
	 * Plural formula
	 * 
	 * This property stores the plural formula that is defined by the 
	 * Portable Object file. For more information about plural forms 
	 * in Portable Object files:
	 * 
	 * http://www.gnu.org/software/gettext/manual/gettext.html#Plural-forms
	 * 
	 * @access private
	 * @var string
	 */
	private $_pluralFormula = NULL;
	
	/**
	 * Messages
	 * 
	 * This property stores the translated strings that are contained
	 * by the Portable Object file. For more information about translated
	 * strings, read the docs on:
	 * 
	 * - {@link M_LocaleMessageCatalog}
	 * - {@link M_LocaleMessage}
	 * 
	 * @access private
	 * @var array
	 */
	private $_msgid = array();
	
	/**
	 * Headers
	 * 
	 * This property stores the headers that are contained by the 
	 * Portable Object file. For more information about the headers
	 * in a .po file, read {@link M_LocalePortableObject::getHeaders()}.
	 * 
	 * @access private
	 * @var array
	 */
	private $_headers = array();
	
	/**
	 * Character encoding
	 * 
	 * This property stores the character encoding in which the messages
	 * should be fetched from the Portable Object. The setter of this 
	 * property is {@link M_PortableObject::setCharacterEncoding()}.
	 * 
	 * @access private
	 * @var string
	 */
	private $_charset;
	
	/* -- CONSTRUCTOR -- */
	
	/**
	 * Constructor
	 * 
	 * Typically, M_LocalePortableObject is used to parse .po files, 
	 * in order to extract information out of the original file. For
	 * example, the following code loads in a .po file and reads the
	 * headers that are contained by the file:
	 * 
	 * <code>
	 *    $po = new M_LocalePortableObject(new M_File('en.po'));
	 *    print_r($po->getHeaders());
	 * </code>
	 * 
	 * Note that you can create a Portable Object file from scratch,
	 * if you do not construct the object with a .po file. Consider the 
	 * following example, to create a new .po file:
	 * 
	 * <code>
	 *    $po = new M_LocalePortableObject;
	 *    $po->setHeader(M_LocalePortableObject::PROJECT_ID_VERSION, 'MyProject');
	 *    $po->save('en.po');
	 * </code>
	 * 
	 * @access public
	 * @return M_LocalePortableObject
	 */
	public function __construct(M_File $po = NULL, $charset = NULL) {
		// If a .po file has been passed in to the constructor
		if($po) {
			// If a charset has been given, we override whatever charset
			// has been defined in the .po file. If not, we leave the
			// definition of charset up to the .po file
			if($charset) {
				$this->setCharacterEncoding($charset);
			}
			
			// We check if the file physically exists:
			if($po->exists()) {
				// We parse the contents of the file, in order to extract
				// the headers and translated strings
				list($this->_headers, $this->_msgid) = self::_parsePoFileContents($po);
				
				// If plural forms are available, we parse additional
				// info on the Plural-Forms header:
				if(isset($this->_headers['Plural-Forms'])) {
					// Try to extract more info:
					$plural = self::_parseHeaderPluralForms($this->_headers['Plural-Forms']);
					if($plural) {
						$this->_numberOfPluralForms = $plural[0];
						$this->_pluralFormula = $plural[1];
					}
					// If failed to extract more info about plural forms,
					// we remove the Plural-Forms header
					else {
						unset($this->_headers['Plural-Forms']);
					}
				}
			}
			// If file does not exist:
			else {
				throw new M_LocaleException(sprintf('%s: Cannot find file %s', __CLASS__, $po->getPath()));
			}
		}
		// If we're constructing a new portable object (not out of
		// existing file), we set some default values:
		else {
			$this->setPotCreationDate(time());
			$this->setPoRevisionDate(time());
			if($charset) {
				$this->setCharacterEncoding($charset);
			} else {
				$this->setCharacterEncoding('UTF-8');
			}
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Has Plural Formula?
	 * 
	 * Similar to {@link M_LocaleMessageCatalog::hasPluralFormula()}.
	 * 
	 * NOTE:
	 * The return value is (boolean) TRUE if the Portable Object file
	 * comes with a plural formula, or (boolean) FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function hasPluralFormula() {
		if(isset($this->_headers['Plural-Forms'])) {
			if($this->_numberOfPluralForms != NULL && $this->_pluralFormula != NULL) {
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/**
	 * Get number of plural forms
	 * 
	 * Similar to {@link M_LocaleMessageCatalog::getNumberOfPluralForms()}.
	 * 
	 * NOTE:
	 * If no plural forms have been defined in the Portable Object, 
	 * this method will return NULL instead.
	 * 
	 * @access public
	 * @return integer|NULL
	 */
	public function getNumberOfPluralForms() {
		return $this->_numberOfPluralForms;
	}
	
	/**
	 * Get Plural Formula
	 * 
	 * Similar to {@link M_LocaleMessageCatalog::getPluralFormula()}.
	 * Note however that this method will return the plural formula in
	 * the original C syntax, instead of PHP syntax.
	 * 
	 * NOTE:
	 * If no plural forms have been defined in the Portable Object, 
	 * this method will return NULL instead.
	 * 
	 * @access public
	 * @return string|NULL
	 */
	public function getPluralFormula() {
		return $this->_pluralFormula;
	}
	
	/**
	 * Get headers
	 * 
	 * This method will return an associative array, of which the keys
	 * are the names of the headers. A Portable Object headers include 
	 * the following:
	 * 
	 * <code>Project-Id-Version</code>
	 * 
	 * This is the name and version of the package.
	 * 
	 * <code>Report-Msgid-Bugs-To</code>
	 * 
	 * This contains an email address or URL where you can report bugs 
	 * in the untranslated strings:
	 * 
	 * - Strings which are not entire sentences.
	 * - Strings which use unclear terms or require additional context.
	 * - Strings which make invalid assumptions about notation of date, time or money.
	 * - Pluralisation problems.
	 * - Incorrect English spelling.
	 * - Incorrect formatting. 
	 * - ...
	 * 
	 * <code>POT-Creation-Date</code>
	 * 
	 * This contains the creation date of the po file.
	 * 
	 * <code>PO-Revision-Date</code>
	 * 
	 * Filled by the PO file editor when translator saves the file.
	 * 
	 * <code>Last-Translator</code>
	 * 
	 * Name and email address of the translator (without double quotes).
	 * 
	 * <code>Language-Team</code>
	 * 
	 * The English name of the language, and the email address or 
	 * homepage URL of the language team the translator(s) is/are part 
	 * of.
	 * 
	 * <code>Content-Type</code>
	 * 
	 * Also contains the character encoding that is being used in the
	 * language.
	 * 
	 * <code>Plural-Forms</code>
	 * 
	 * This field is optional. It is only needed if the PO file has 
	 * plural forms. You can find them by searching for the "msgid_plural" 
	 * keyword.
	 * 
	 * @access public
	 * @return array
	 */
	public function getHeaders() {
		return $this->_headers;
	}
	
	/**
	 * Get header
	 * 
	 * This method will return the value of a given header, if available.
	 * Consider the following example code, that fetches the value of
	 * the POT-Creation-Date header:
	 * 
	 * <code>
	 *    $po = new M_LocalePortableObject('en.po');
	 *    echo $po->getHeader(M_LocalePortableObject::POT_CREATION_DATE);
	 * </code>
	 * 
	 * NOTE:
	 * Note that this method will return (boolean) FALSE if no value
	 * could have been found for the requested header!
	 * 
	 * @see M_LocalePortableObject::getHeaders()
	 * @access public
	 * @param string $header
	 * 		The name of the header
	 * @return string|boolean
	 */
	public function getHeader($header) {
		if(isset($this->_headers[$header])) {
			return $this->_headers[$header];
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get POT Creation Date
	 * 
	 * This method will return an {@link M_Date} object that represents
	 * the date at which the Portable Object file was created. Note 
	 * that, if the POT Creation Date is not available in the headers
	 * (see {@link M_LocalePortableObject::getHeaders()}), this method
	 * will return (boolean) FALSE instead!
	 * 
	 * @access public
	 * @return M_Date|NULL
	 */
	public function getPotCreationDate() {
		$tmp = $this->getHeader(self::POT_CREATION_DATE);
		if($tmp !== FALSE) {
			return new M_Date($tmp);
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get PO Revision Date
	 * 
	 * This method will return an {@link M_Date} object that represents
	 * the revision date of the Portable Object file. Note that, if the 
	 * PO Revision Date is not available in the PO headers (see 
	 * {@link M_LocalePortableObject::getHeaders()}), this method will 
	 * return (boolean) FALSE instead!
	 * 
	 * @access public
	 * @return M_Date|NULL
	 */
	public function getPoRevisionDate() {
		$tmp = $this->getHeader(self::PO_REVISION_DATE);
		if($tmp !== FALSE) {
			return new M_Date($tmp);
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get character encoding
	 * 
	 * This method will return the character encoding that is being used
	 * at the moment.
	 * 
	 * NOTE:
	 * If no character encoding has been set, this method will return 
	 * UTF-8 (which is used as default encoding of Portable Objects)
	 * 
	 * @see M_LocalePortableObject::setCharacterEncoding()
	 * @access public
	 * @return string
	 */
	public function getCharacterEncoding() {
		if(!isset($this->_headers['Content-Type'])) {
			// $this->setCharacterEncoding(M_LocaleCharset::UTF8);
			return $this->_charset;
		} else {
			if(!$this->_charset) {
				$matches = array();
				if(preg_match('/charset=(.*)$/i', $this->_headers['Content-Type'], $matches)) {
					$this->setCharacterEncoding($matches[1]);
				} else {
					// $this->setCharacterEncoding(M_LocaleCharset::UTF8);
				}
			}
			return $this->_charset;
		}
	}
	
	/**
	 * Get iterator on translated strings
	 * 
	 * This method will build the collection of translated strings that
	 * is contained by the Portable Object file. The return value of 
	 * this method is an iterator on that collection.
	 * 
	 * Each element in the iterator is an {@link M_LocaleMessage} object.
	 * This allows other objects to obtain the collection of strings
	 * that is contained by the Portable Object, and to retrieve the
	 * properties on each of them.
	 * 
	 * NOTE:
	 * The messages that are returned by this method are encoded in
	 * UTF-8. Read {@link M_LocalePortableObject::setCharacterEncoding()}
	 * for more info.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getIterator() {
		$out = array();
		
		// For each the translated strings:
		foreach($this->_msgid as $untranslated => $entry) {
			// Construct the M_LocaleMessage
			$string = new M_LocaleMessage($untranslated);
			$string->setTranslatedString($entry['msgstr'][0]);
			
			// Plural forms?
			if($entry['msgid-plural']) {
				$string->setUntranslatedPluralString($entry['msgid-plural']);
				for($i = 1, $n = count($entry['msgstr']); $i < $n; $i ++) {
					$string->setTranslatedPluralString($i, $entry['msgstr'][$i]);
				}
			}
			
			// Set comments
			if(!empty($entry['translator'])) {
				$string->setComment(M_LocaleMessage::TRANSLATOR_COMMENT, $entry['translator']);
			}
			
			if(!empty($entry['extracted'])) {
				$string->setComment(M_LocaleMessage::EXTRACTED_COMMENT, $entry['extracted']);
			}
			
			if(!empty($entry['reference'])) {
				$string->setComment(M_LocaleMessage::REFERENCE, $entry['reference']);
			}
			
			if(!empty($entry['flag'])) {
				$string->setComment(M_LocaleMessage::FLAG, $entry['flag']);
			}
			
			if(!empty($entry['previous'])) {
				$string->setComment(M_LocaleMessage::PREVIOUS_UNTRANSLATED_STRING, $entry['previous']);
			}
			
			// Add the string to the iterator
			$out[] = $string;
		}
		
		// Return the iterator
		return new ArrayIterator($out);
	}
	
	/**
	 * 
	 */
	public function toString() {
		$out = '';
		
		// New lines in the file
		$nl = "\n";
		
		// Prepare the headers string
		$msgstr = '';
		foreach($this->_headers as $name => $value) {
			// ; is apparently not necessary to end the lines in the headers. Even
			// more importantly, POEdit will fail to show strings in UTF-8, because
			// it will think that the encoding scheme is UTF-8; instead of UTF-8
			// $msgstr .= $name . ': ' . $value . ';' . $nl;
			$msgstr .= $name . ': ' . $value . $nl;
		}
		
		// Add the headers as a msgid
		$out .= '# Exported at ' . date('Y-m-d H:i') . $nl;
		$out .= '# Powered by Multimedium <info@multimedium.be>' . $nl;
		$out .= 'msgid ""' . $nl;
		$out .= 'msgstr ' . self::_serializeString($msgstr) . $nl . $nl;
		
		// For each of the translated strings that are currently being
		// contained by the object.
		foreach($this->_msgid as $untranslated => $entry) {
			// Add comments
			if(!empty($entry['translator'])) {
				$out .= '# ' . $entry['translator'] . $nl;
			}
			
			if(!empty($entry['extracted'])) {
				$out .= '#. ' . $entry['extracted'] . $nl;
			}
			
			if(!empty($entry['reference'])) {
				$out .= '#: ' . $entry['reference'] . $nl;
			}
			
			if(!empty($entry['flag'])) {
				$out .= '#, ' . $entry['flag'] . $nl;
			}
			
			if(!empty($entry['previous'])) {
				$out .= '#| ' . $entry['previous'] . $nl;
			}
			
			// Add untranslated string
			$out .= 'msgid ' . self::_serializeString($untranslated) . $nl;
			
			// Add untranslated plural form
			if($entry['msgid-plural']) {
				$out .= 'msgid_plural ' . self::_serializeString($entry['msgid-plural']) . $nl;
			}
			
			// Add translated string(s)
			if($entry['msgid-plural']) {
				foreach($entry['msgstr'] as $i => $msgstr) {
					$out .= 'msgstr['. $i .'] ' . self::_serializeString($msgstr) . $nl;
				}
			} else {
				$out .= 'msgstr ' . self::_serializeString($entry['msgstr'][0]) . $nl;
			}
			
			// Separate from the next string:
			$out .= $nl;
		}
		
		// Return the final render of the Portable Object file
		return $out;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set Message Catalog
	 * 
	 * @uses M_LocaleMessageCatalog::getIterator()
	 */
	public function setMessageCatalog(M_LocaleMessageCatalog $catalog) {
		// For each of the translated strings in the message catalog:
		foreach($catalog->getIterator() as $string) {
			// We set/add the translated string in the Portable Object:
			$this->setTranslatedString($string);
		}
		
		// Is a plural formula is available in the message catalog:
		if($catalog->hasPluralFormula()) {
			// We set the number of plural forms, and the formula:
			$this->setNumberOfPluralForms($catalog->getNumberOfPluralForms());
			$this->setPluralFormulaInPhp($catalog->getPluralFormula());
		}
	}
	
	/**
	 * Set translated string
	 * 
	 * This method is used to add a translated string to the Portable
	 * Object file. Note that the translated string is represented by
	 * an object of {@link M_LocaleMessage}.
	 * 
	 * @access public
	 * @param M_LocaleMessage $message
	 * 		The translated string
	 * @return void
	 */
	public function setTranslatedString(M_LocaleMessage $message) {
		$entry = array();
		
		// Check for translator's comments:
		$temp = $message->getComment(M_LocaleMessage::TRANSLATOR_COMMENT);
		$entry['translator'] = $temp !== FALSE ? $temp : '';
		
		// Check for extracted comments:
		$temp = $message->getComment(M_LocaleMessage::EXTRACTED_COMMENT);
		$entry['extracted'] = $temp !== FALSE ? $temp : '';
		
		// Check for reference comment:
		$temp = $message->getComment(M_LocaleMessage::REFERENCE);
		$entry['reference'] = $temp !== FALSE ? $temp : '';
		
		// Check for flag:
		$temp = $message->getComment(M_LocaleMessage::FLAG);
		$entry['flag'] = $temp !== FALSE ? $temp : '';
		
		// Check for previous untranslated string:
		$temp = $message->getComment(M_LocaleMessage::PREVIOUS_UNTRANSLATED_STRING);
		$entry['previous'] = $temp !== FALSE ? $temp : '';
		
		// Translated string, WITH plural forms
		$entry['msgstr'] = array();
		$entry['msgstr'][0] = $message->getTranslatedString();
		if($message->hasPluralForm()) {
			$entry['msgid-plural'] = $message->getUntranslatedPluralString();
			for($i = 0, $j = 1, $n = $message->getNumberOfPluralForms(); $i < $n; $i ++, $j ++) {
				$entry['msgstr'][$j] = $message->getTranslatedPluralString($j);
			}
		} else {
			$entry['msgid-plural'] = FALSE;
		}
		
		// Add the translated string to the local repository of
		// messages
		$this->_msgid[$message->getUntranslatedString()] = $entry;
	}
	
	/**
	 * Set number of plural forms
	 * 
	 * This method can be used to define the number of plural forms
	 * in the Portable Object file. For more information about plural
	 * forms in Portable Object files:
	 * 
	 * http://www.gnu.org/software/gettext/manual/gettext.html#Plural-forms
	 * 
	 * @access public
	 * @param integer $number
	 * 		The number of plural forms
	 * @return void
	 */
	public function setNumberOfPluralForms($number) {
		$this->_numberOfPluralForms = (int) $number;
		if($this->_pluralFormula != NULL) {
			$this->_headers[self::PLURAL_FORMS]  = 'nplurals='. $this->_numberOfPluralForms .'; ';
			$this->_headers[self::PLURAL_FORMS] .= 'plural=' . $this->_pluralFormula;
		}
	}
	
	/**
	 * Set plural formula
	 * 
	 * This method can be used to set the plural formula that is used
	 * to calculate the correct plural index for a given number. For 
	 * more information about plural forms in Portable Object files:
	 * 
	 * http://www.gnu.org/software/gettext/manual/gettext.html#Plural-forms
	 * 
	 * @access public
	 * @param integer $formula
	 * 		The plural formula
	 * @return void
	 */
	public function setPluralFormula($formula) {
		$this->_pluralFormula = (string) $formula;
		if($this->_numberOfPluralForms != NULL) {
			$this->_headers[self::PLURAL_FORMS]  = 'nplurals='. $this->_numberOfPluralForms .'; ';
			$this->_headers[self::PLURAL_FORMS] .= 'plural=' . $this->_pluralFormula;
		}
	}
	
	/**
	 * 
	 */
	public function setPluralFormulaInPhp($formula) {
		$this->setPluralFormula(str_replace('$', '', (string) $formula));
	}
	
	/**
	 * Set the value of a header
	 * 
	 * This method can be used to set the value of a specific header
	 * in the Portable Object file. Consider the following example,
	 * that sets the value of the "Project-Id-Version":
	 * 
	 * <code>
	 *    $po = new M_LocalePortableObject;
	 *    $po->setHeader(M_LocalePortableObject::PROJECT_ID_VERSION, 'MyProject');
	 * </code>
	 * 
	 * For an overview of all headers, you should read the docs on:
	 * 
	 * - {@link M_LocalePortableObject::getHeaders()}
	 * - M_LocalePortableObject Class Constants
	 * 
	 * @throws M_LocaleException
	 * @see M_LocalePortableObject::setPotCreationDate()
	 * @see M_LocalePortableObject::setPoRevisionDate()
	 * @access public
	 * @param string $header
	 * 		The header for which you want to set the new value
	 * @param string $value
	 * 		The new header value
	 * @return void
	 */
	public function setHeader($header, $value) {
		switch($header) {
			case self::PLURAL_FORMS:
				$temp = self::_parseHeaderPluralForms($value);
				if($temp === FALSE) {
					throw new M_LocaleException(sprintf(
						'%s: Invalid Plural-Forms Header: %s',
						__CLASS__,
						$value
					));
				} else {
					$this->_headers[self::PLURAL_FORMS] = $value;
					$this->_numberOfPluralForms = (int) $temp[0];
					$this->_pluralFormula = (string) $temp[1];
				}
				break;
			
			case self::POT_CREATION_DATE:
				$this->setPotCreationDate($header, $value);
				break;
			
			case self::PO_REVISION_DATE:
				$this->setPoRevisionDate($header, $value);
				break;
			
			case self::CONTENT_TYPE_CHARSET:
				$this->setCharacterEncoding($value);
				break;
			
			default:
				$this->_headers[$header] = $value;
				break;
		}
	}
	
	/**
	 * Set POT Creation Date
	 * 
	 * This method will set the date at which the Portable Object file 
	 * was created.
	 * 
	 * NOTE:
	 * This method will both accept a UNIX timestamp (integer) or an
	 * object of {@link M_Date}.
	 * 
	 * @access public
	 * @param M_Date|integer $date
	 * @return void
	 */
	public function setPotCreationDate($date) {
		if($date instanceof M_Date) {
			$this->_headers[self::POT_CREATION_DATE] = date('Y-m-d H:i', $date->timestamp);
		} else {
			$this->_headers[self::POT_CREATION_DATE] = date('Y-m-d H:i', (int) $date);
		}
	}
	
	/**
	 * Set PO Revision Date
	 * 
	 * This method will set the revision date of the PO file.
	 * 
	 * NOTE:
	 * This method will both accept a UNIX timestamp (integer) or an
	 * object of {@link M_Date}.
	 * 
	 * @access public
	 * @param M_Date|integer $date
	 * @return void
	 */
	public function setPoRevisionDate($date) {
		if($date instanceof M_Date) {
			$this->_headers[self::PO_REVISION_DATE] = date('Y-m-d H:i', $date->timestamp);
		} else {
			$this->_headers[self::PO_REVISION_DATE] = date('Y-m-d H:i', (int) $date);
		}
	}
	
	/**
	 * Set character encoding
	 * 
	 * This method will set the character encoding in which the 
	 * contained messages should be fetched from the Portable Object.
	 * Note that {@link M_LocalePortableObject} will always provide 
	 * with messages in UTF-8:
	 * 
	 * - {@link M_LocalePortableObject::getIterator()}
	 * 
	 * To do so, it will use {@link M_LocaleCharset} to convert the 
	 * encoding of contained messages, if necessary.
	 * 
	 * Example 1, read a .po file that has been encoded in ISO-8859-1
	 * <code>
	 *    $po = new M_LocalePortableObject(new M_File('es.po'));
	 *    $po->setCharacterEncoding(M_LocaleCharset::ISO_8859_1);
	 *    foreach($po->getIterator() as $message) {
	 *       // Get string, which is UTF-8, no matter what the original
	 *       // encoding of the .po file is:
	 *       echo $message->getUntranslatedString();
	 *    }
	 * </code>
	 * 
	 * If you are reading an existing .po file, {@link M_LocalePortableObject}
	 * will automatically set the character encoding to the one that 
	 * has been specified in the .po file (if specified). For more info,
	 * see {@link M_LocalePortableObject::getHeaders()}.
	 * 
	 * IMPORTANT NOTE:
	 * Note that the original character encoding is maintained, if 
	 * rendering .po file contents. For example, if you want to create
	 * a .po file that is encoded in ISO-8859-1:
	 * 
	 * Example 2
	 * <code>
	 *    $po = new M_LocalePortableObject(new M_File('es.po'));
	 *    $po->setCharacterEncoding(M_LocaleCharset::ISO_8859_1);
	 *    $po->save('es.po');
	 * </code>
	 * 
	 * You can render the same .po file, encoded in UTF-8, by changing
	 * the code in Example 2 to the following:
	 * 
	 * Example 3
	 * <code>
	 *    $po = new M_LocalePortableObject(new M_File('es.po'));
	 *    $po->setCharacterEncoding(M_LocaleCharset::UTF8);
	 *    $po->save('es.po');
	 * </code>
	 * 
	 * @access public
	 * @param string $encoding
	 * 		The character encoding to be used
	 * @return void
	 */
	public function setCharacterEncoding($encoding) {
		$this->_headers['Content-Type'] = 'text/plain; charset=' . strtoupper($encoding);
		$this->_charset = $encoding;
	}
	
	/**
	 * Save to file
	 * 
	 * IMPORTANT NOTE:
	 * Returns object of {@link M_File} on success, throws an exception
	 * {@link M_LocaleException} if failed.
	 */
	public function save($to) {
		$fp = @fopen($to, 'w');
		if($fp) {
			if(@fwrite($fp, $this->toString())) {
				@fclose($fp);
				return new M_File($to);
			}
		}
		
		// If we're still here, something went wrong. In this case,
		// we throw an exception.
		throw new M_LocaleException(sprintf(
			'%s: Cannot not save Portable Object file to %s',
			__CLASS__,
			$to
		));
	}
	
	/* -- Magic Methods -- */
	
	public function __toString() {
		return $this->toString();
	}
	
	/* -- PRIVATE: Parsing to read .po file -- */
	
	/**
	 * Parse .po file contents
	 * 
	 * <code>
	 *    list($headers, $entries) = self::_parsePoFileContents(new M_File('files/en.po'));
	 * </code>
	 * 
	 * @uses M_PortableObject::_parsePoFileEntry()
	 * @uses M_PortableObject::_parseString()
	 * @uses M_PortableObject::_parseHeaders()
	 * @throws M_LocaleException
	 */
	private function _parsePoFileContents(M_File $po) {
		// Output array
		$out   = array();
		$count = 0;
		
		// Information about the current translated string is stored
		// in $current
		$current = array();
		$index   = 0;
		
		// We run through the file line by line. The context of the
		// current line is stored in $context
		$context = 'comment';
		
		// For each of the lines in the file:
		foreach($po->getContentsInLines() as $lineno => $line) {
			// Remove redundant white-spaces from the current line (both
			// from start and beginning of the line)
			$line = trim($line);
			
			// If the line is not empty
			if(!empty($line)) {
				// We determine the context of the current line. To do so,
				// we check the first N characters:
				// (Use strncmp(), because that's better for performance)
				// COMMENT Context
				if(!strncmp('#', $line, 1)) {
					// If the current context is msgstr, this comment
					// is the start of a new entry:
					if($context == 'msgstr') {
						self::_parsePoFileEntry($out, $current, $count ++);
						$current = array();
					}
					
					// Initiate comments, if not done yet:
					if(!isset($current[0])) {
						$current[0] = array();
					}
					
					// TRANSLATOR COMMENT
					if(!strncmp('# ', $line, 2)) {
						$current[0][0] = substr($line, 2);
					}
					// EXTRACTED COMMENT
					elseif(!strncmp('#. ', $line, 3)) {
						$current[0][1] = substr($line, 3);
					}
					// REFERENCE
					elseif(!strncmp('#: ', $line, 3)) {
						$current[0][2] = substr($line, 3);
					}
					// FLAG
					elseif(!strncmp('#, ', $line, 3)) {
						$current[0][3] = substr($line, 3);
					}
					// PREVIOUS UNTRANSLATED STRING
					elseif(!strncmp('#| ', $line, 3)) {
						$current[0][4] = substr($line, 3);
					}
					
					// Set the new context:
					$context = 'comment';
				}
				// STRING Context
				elseif(!strncmp('"', $line, 1)) {
					// A string can only come after msgid, msgid_plural or msgstr
					if($context == 'msgid' || $context == 'msgid-plural' || $context == 'msgstr') {
						// Add to string
						if($context == 'msgid') {
							$current[1] .= self::_parseString($line, $lineno);
						} elseif($context == 'msgid-plural') {
							$current[2] .= self::_parseString($line, $lineno);
						} elseif($context == 'msgstr') {
							if(!isset($current[3][$index])) {
								$current[3][$index] = '';
							}
							$current[3][$index] .= self::_parseString($line, $lineno);
						}
					} else {
						throw new M_LocaleException(sprintf(
							'%s: Unexpected string at Line #%s', 
							__CLASS__, 
							$lineno
						));
					}
				}
				// MSGID-PLURAL Context
				elseif(!strncmp('msgid_plural', $line, 12)) {
					// msgid_plural can only come after msgid!
					if($context == 'msgid') {
						// Prepare the variable that holds the complete
						// string
						$current[2] = self::_parseString(substr($line, 12), $lineno);
						
						// Set the new context
						$context = 'msgid-plural';
					}
					// Unexpected msgid_plural
					else {
						throw new M_LocaleException(sprintf(
							'%s: Unexpected msgid_plural at Line #%s', 
							__CLASS__, 
							$lineno
						));
					}
				}
				// MSGID Context
				elseif(!strncmp('msgid', $line, 5)) {
					// If the current context is msgstr, this msgid
					// is the start of a new entry:
					if($context == 'msgstr') {
						self::_parsePoFileEntry($out, $current, $count ++);
						$current = array();
					}
					
					// msgid can only come after comments!
					if($context == 'comment' || $context == 'msgstr') {
						// Prepare the variable that holds the string
						$current[1] = self::_parseString(substr($line, 5), $lineno);
						
						// Set the new context
						$context = 'msgid';
					}
					// Unexpected msgid
					else {
						throw new M_LocaleException(sprintf(
							'%s: Unexpected msgid at Line #%s', 
							__CLASS__, 
							$lineno
						));
					}
				}
				// MSGSTR Context
				elseif(!strncmp('msgstr', $line, 6)) {
					// msgstr can only come after msgid, msgid_plural or msgstr!
					if($context == 'msgid' || $context == 'msgid-plural' || $context == 'msgstr') {
						// Set the plural-form index (if given)
						$index = 0;
						if(!strncmp('msgstr[', $line, 7)) {
							$end = strpos($line, ']', 7);
							if($end === FALSE) {
								throw new M_LocaleException(sprintf(
									'%s: msgstr[] Parse Error at Line #%s', 
									__CLASS__, 
									$lineno
								));
							}
							$index = (int) substr($line, 7, $end);
						} else {
							$end = 5;
						}
						
						// Prepare the variable that holds the complete
						// string
						if(!isset($current[3])) {
							$current[3] = array();
						}
						if(!isset($current[3][$index])) {
							$current[3][$index] = '';
						}
						$current[3][$index] = self::_parseString(substr($line, $end + 1), $lineno);
						
						// Set the new context
						$context = 'msgstr';
					}
					// Unexpected msgstr
					else {
						throw new M_LocaleException(sprintf(
							'%s: Unexpected msgstr at Line #%s', 
							__CLASS__, 
							$lineno
						));
					}
				}
			}
		}
		
		// Add the last entry to the output:
		self::_parsePoFileEntry($out, $current, $count);
		
		// The first msgid should be an empty untranslated string. This
		// indicates that the corresponding msgstr holds the headers
		// of the .po file
		$h = array_shift($out);
		if($h === FALSE) {
			throw new M_LocaleException(sprintf(
				'%s: Missing Portable Object Headers',
				__CLASS__
			));
		}
		
		// Return parsed result:
		return array(self::_parseHeaders($h['msgstr'][0]), $out);
	}
	
	/**
	 * Parse .po file entry
	 */
	private function _parsePoFileEntry(array &$repository, array $entry, $iteration = 0) {
		// Required fields: msgid, msgstr
		if(!isset($entry[1]) || !isset($entry[3]) || !isset($entry[3][0])) {
			throw new M_LocaleException(sprintf(
				'%s: Missing msgid or msgstr',
				__CLASS__
			));
		}
		// If this is the first entry, the msgid should be empty!
		// (indicating that the corresponding msgstr stores the 
		// headers of the file)
		if($iteration == 0) {
			if(!empty($entry[1])) {
				throw new M_LocaleException(sprintf(
					'%s: First msgid should be empty (Portable Object Headers)',
					__CLASS__
				));
			}
		}
		// If this is not the first entry, the msgid cannot be empty!
		// (also, we check the character encoding)
		else {
			if(empty($entry[1])) {
				throw new M_LocaleException(sprintf(
					'%s: msgid cannot be empty',
					__CLASS__
				));
			}
		}
		// save the new entry to provided repo:
		$repository[$entry[1]] = array(
			'translator'   => isset($entry[0][0]) ? $entry[0][0] : '',
			'extracted'    => isset($entry[0][1]) ? $entry[0][1] : '',
			'reference'    => isset($entry[0][2]) ? $entry[0][2] : '',
			'flag'         => isset($entry[0][3]) ? $entry[0][3] : '',
			'previous'     => isset($entry[0][4]) ? $entry[0][4] : '',
			'msgid-plural' => isset($entry[2])    ? $entry[2]    : FALSE,
			'msgstr'       => $entry[3]
		);
	}
	
	/**
	 * Parse string
	 * 
	 * This private method us used internally, to parse the value of a
	 * string in the Portable Object file. This method is used to parse
	 * the string values of
	 * 
	 * <code>
	 *    msgid
	 *    msgstr
	 * </code>
	 * 
	 * NOTE:
	 * This method is used when constructing a Portable Object out of
	 * a .po file; see {@link M_LocalePortableObject::__construct()}
	 * 
	 * @throws M_LocaleException
	 * @access public
	 * @param string $string
	 * 		The string
	 * @return string
	 */
	private function _parseString($string, $lineNo) {
		// Remove redundant spaces:
		$string = trim($string);
		
		// Make sure the string has been delimited correctly:
		if($string{0} != '"') {
			throw new M_LocaleException(sprintf(
				'%s: Malformed string value at Line #%s',
				__CLASS__,
				$lineNo
			));
		}
		if(substr($string, -1) != '"') {
			throw new M_LocaleException(sprintf(
				'%s: Malformed string value at Line #%s',
				__CLASS__,
				$lineNo
			));
		}
		
		// Extract the string value
		$string = substr($string, 1, -1);
		
		// Unescape the string value
		// Escape Sequence   | Intended Character
		// ------------------|------------------------------
		// \?                | ? (U+003F)
		$string = str_replace('\\?',   '?',   $string);
		// ------------------|------------------------------
		// \'                | ' (U+0027)
		$string = str_replace('\\\'',  '\'',  $string);
		// ------------------|------------------------------
		// \"                | " (U+0022)
		$string = str_replace('\\"',   '"',   $string);
		// ------------------|------------------------------
		// \\                | \ (U+005C)
		$string = str_replace('\\\\',  '\\',  $string);
		// ------------------|------------------------------
		// \n                | New line
		$string = str_replace('\\n',   "\n",  $string);
		// ------------------|------------------------------
		// \r                | CR (U+000D)
		$string = str_replace('\\r',   "\r",  $string);
		// ------------------|------------------------------
		// \t                | HT (U+0009)
		$string = str_replace('\\t',   "\t",  $string);
		
		// return the final string
		return $string;
	}
	
	/**
	 * Parse headers
	 * 
	 * This private method is used internally, to parse the headers
	 * out of the first msgstr in the Portable Object file.
	 * 
	 * NOTE:
	 * This method is used when constructing a Portable Object out of
	 * a .po file; see {@link M_LocalePortableObject::__construct()}
	 * 
	 * @throws M_LocaleException
	 * @access private
	 * @return array
	 */
	private function _parseHeaders($msgstr) {
		$out = array();
		foreach(explode("\n", str_replace("\r", '', $msgstr)) as $line) {
			$line = trim($line);
			if(!empty($line)) {
				$pos = strpos($line, ':');
				if($pos === FALSE) {
					throw new M_LocaleException(sprintf(
						'%s: Malformed header: %s',
						__CLASS__,
						$line
					));
				}
				$out[trim(substr($line, 0, $pos))] = rtrim(trim(substr($line, $pos + 1)), ';');
			}
		}
		return $out;
	}
	
	/**
	 * Parse Plural-Forms header
	 * 
	 */
	private function _parseHeaderPluralForms($headerValue) {
		$match = array();
		if(preg_match('/^nplurals[\s=]+([0-9]+);[\s]+plural[\s=]+([^;]+);?$/im', trim($headerValue), $match)) {
			return array($match[1], $match[2]);
		} else {
			return FALSE;
		}
	}
	
	/* -- PRIVATE: Seralizing to save .po file -- */
	
	/**
	 * Serialize a string
	 * 
	 * TODO: UTF-8 quotes
	 * When single quote characters or double quote characters are used 
	 * in translations for your language, and your locale's encoding 
	 * is one of the ISO-8859-* charsets, it is best if you create your 
	 * PO files in UTF-8 encoding, instead of your locale's encoding. 
	 * This is because in UTF-8 the real quote characters can be 
	 * represented (single quote characters: U+2018, U+2019, double 
	 * quote characters: U+201C, U+201D), whereas none of ISO-8859-* 
	 * charsets has them all.
	 * 
	 * Additional notes on the utility of this method:
	 * - will break up string in different lines, as specified in the
	 *   GNU gettext manual
	 * - will escape special characters in the string
	 */
	private function _serializeString($string) {
		// Encode the string to the charset of the PO:
		//$charset = $this->getCharacterEncoding();
		//if($charset != M_LocaleCharset::UTF8) {
			// $string = M_LocaleCharset::convert($string, M_LocaleCharset::UTF8, $charset);
		//}
		
		// Escape the string value. Note that we do unescape the string
		// values (see _parseString()), but we don't escape all of them
		// when serializing the string. This provides with better 
		// results for external PO Editors (eg POEdit)
		// Escape Sequence   | Intended Character
		// ------------------|------------------------------
		// \\                | \ (U+005C)
		$string = str_replace('\\',  '\\\\',  $string);
		// ------------------|------------------------------
		// \?                | ? (U+003F)
		// $string = str_replace('?',   '\\?',   $string);
		// ------------------|------------------------------
		// \'                | ' (U+0027)
		// $string = str_replace('\'',  '\\\'',  $string);
		// ------------------|------------------------------
		// \"                | " (U+0022)
		$string = str_replace('"',   '\\"',   $string);
		// ------------------|------------------------------
		// \r                | CR (U+000D)
		$string = str_replace("\r",  '\\r',   $string);
		// ------------------|------------------------------
		// \t                | HT (U+0009)
		$string = str_replace("\t",  '\\t',   $string);
		
		// If the string contains new lines, break up the string into 
		// multiple lines:
		$n = substr_count($string, "\n");
		if($n > 0) {
			if($n == 1 && substr($string, -1) == "\n") {
				return '"'. str_replace("\n", '\\n"', $string);
			} else {
				return str_replace("\n\"\"", '', '""' . "\n" . '"'. str_replace("\n", '\\n"' . "\n" . '"', $string) .'"');
			}
		}
		// If the string does not contain new lines:
		else {
			return '"' . $string . '"';
		}
	}
}

?>