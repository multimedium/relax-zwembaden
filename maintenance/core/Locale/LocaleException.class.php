<?php
/**
 * M_LocaleException class
 * 
 * Exceptions thrown by the Locale API
 * 
 * @package Core
 */
class M_LocaleException extends M_Exception {
}
?>