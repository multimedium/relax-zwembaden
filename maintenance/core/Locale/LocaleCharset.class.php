<?php
/**
 * M_LocaleCharset class
 * @package Core
 */

// M_LocaleCharset uses the ConvertCharset class
// (Author: Mikolaj Jedrzejak, Poland)
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'charsets/ConvertCharset.class.php';

/**
 * M_LocaleCharset class
 *
 * M_LocaleCharset allows to convert the character encoding of a given
 * string from one given charset to another.
 * 
 * NOTE:
 * M_LocaleCharset forms part of the Locale API. For more information, 
 * start the intro at {@link M_Locale}.
 * 
 * @package Core
 */
class M_LocaleCharset extends M_Object {
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP037 = 'cp037';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP1006 = 'cp1006';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP1026 = 'cp1026';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP424 = 'cp424';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP437 = 'cp437';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP500 = 'cp500';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP737 = 'cp737';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP775 = 'cp775';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP850 = 'cp850';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP852 = 'cp852';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP855 = 'cp855';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP856 = 'cp856';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP857 = 'cp857';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP860 = 'cp860';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP861 = 'cp861';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP862 = 'cp862';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP863 = 'cp863';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP864 = 'cp864';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP865 = 'cp865';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP866 = 'cp866';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP869 = 'cp869';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP874 = 'cp874';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const CP875 = 'cp875';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const GSM0338 = 'gsm0338';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_1 = 'iso-8859-1';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_2 = 'iso-8859-2';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_3 = 'iso-8859-3';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_4 = 'iso-8859-4';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_5 = 'iso-8859-5';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_6 = 'iso-8859-6';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_7 = 'iso-8859-7';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_8 = 'iso-8859-8';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_9 = 'iso-8859-9';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_10 = 'iso-8859-10';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_11 = 'iso-8859-11';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_13 = 'iso-8859-13';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_14 = 'iso-8859-14';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_15 = 'iso-8859-15';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ISO_8859_16 = 'iso-8859-16';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const KOI8_R = 'koi8-r';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const KOI8_U = 'koi8-u';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const MAZOVIA = 'mazovia';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const NEXTSTEP = 'nextstep';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const STDENC = 'stdenc';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const SYMBOL = 'symbol';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const TURKISH = 'turkish';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const US_ASCII = 'us-ascii';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const US_ASCII_QUOTES = 'us-ascii-quotes';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const WINDOWS_1250 = 'windows-1250';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const WINDOWS_1251 = 'windows-1251';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const WINDOWS_1252 = 'windows-1252';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const WINDOWS_1253 = 'windows-1253';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const WINDOWS_1254 = 'windows-1254';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const WINDOWS_1255 = 'windows-1255';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const WINDOWS_1256 = 'windows-1256';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const WINDOWS_1257 = 'windows-1257';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const WINDOWS_1258 = 'windows-1258';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const X_MAX_CE = 'x-max-ce';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const X_MAX_CYRILLIC = 'x-max-cyrillic';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const X_MAX_GREEK = 'x-max-greek';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const X_MAX_ICELANDIC = 'x-max-icelandic';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const X_MAX_ROMAN = 'x-max-roman';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const ZDINGBAT = 'zdingbat';
	
	/**
	 * Character Encoding
	 * 
	 * This constant can be used to address a specific charset. 
	 * Typically, this constant is used to specify encodings to 
	 * {@link M_LocaleCharset::convert()}
	 */
	const UTF8 = 'utf-8';
	
	/**
	 * Convert encoding
	 * 
	 * NOTE:
	 * This method will throw an exception if either one of the 
	 * requested encodings is not supported. For more information about
	 * supported locales, read {@link M_LocaleCharset::isSupported()}.
	 * 
	 * Example 1, convert a string from windows-1250 to UTF-8
	 * <code>
	 *    echo M_LocaleCharset::convert(
	 *       'My String', 
	 *       M_LocaleCharset::WINDOWS_1250, 
	 *       M_LocaleCharset::UTF8
	 *    );
	 * </code>
	 * 
	 * @throws M_LocaleException
	 * @access public
	 * @param string $string
	 * 		The string that should be converted from one encoding to
	 * 		another.
	 * @return string
	 */
	public function convert($string, $from, $to) {
		echo "convert $from - $to<br>";
		if(!self::isSupported($from)) {
			throw new M_LocaleException(sprintf(
				'Unsupported Character Encoding %s',
				strtoupper($from)
			));
		}
		if(!self::isSupported($to)) {
			throw new M_LocaleException(sprintf(
				'Unsupported Character Encoding %s',
				strtoupper($to)
			));
		}
		$converter = new ConvertCharset($from, $to, TRUE);
		return $converter->Convert($string);
	}
	
	/**
	 * Is charset supported
	 * 
	 * This method will tell if a given character encoding is supported
	 * by {@link M_LocaleCharset}. Note that the return value of this
	 * method is TRUE if the encoding is supported, FALSE if not.
	 *
	 * @access public
	 * @param string $charset
	 * @return boolean
	 */
	public function isSupported($charset) {
		$charset = strtolower($charset);
		if($charset == 'utf-8') {
			return TRUE;
		} else {
			return is_file(
				dirname(__FILE__) . DIRECTORY_SEPARATOR . 'charsets/ConvertTables/' . strtolower($charset)
			);
		}
	}
	
	/**
	 * Is UTF-8?
	 * 
	 * This method will tell whether or not a given string has been
	 * encoded in UTF-8. Note however that this method will take its
	 * BEST GUESS!
	 *
	 * @access public
	 * @param string $string
	 * 		The string of which to check whether or not it is
	 * 		encoded in UTF-8
	 * @return boolean $flag
	 * 		Returns TRUE if the string seems to be encoded in UTF-8,
	 * 		FALSE if not
	 */
	public function isUtf8($string) {
		for($i = 0; $i < strlen($str); $i ++) {
			if (ord($str[$i]) < 0x80) continue; // 0bbbbbbb
			elseif ((ord($str[$i]) & 0xE0) == 0xC0) $n=1; // 110bbbbb
			elseif ((ord($str[$i]) & 0xF0) == 0xE0) $n=2; // 1110bbbb
			elseif ((ord($str[$i]) & 0xF8) == 0xF0) $n=3; // 11110bbb
			elseif ((ord($str[$i]) & 0xFC) == 0xF8) $n=4; // 111110bb
			elseif ((ord($str[$i]) & 0xFE) == 0xFC) $n=5; // 1111110b
			else return false; // Does not match any model
			for ($j=0; $j<$n; $j++) { // n bytes matching 10bbbbbb follow ?
				if ((++$i == strlen($str)) || ((ord($str[$i]) & 0xC0) != 0x80))
				return false;
	        }
		}
    	return true;
	}
	
	/**
	 * Get UTF-8 BOM (Byte Order Mark)
	 *
	 * Information about BOM extracted from Unicode:
	 * ({@link http://unicode.org/faq/utf_bom.html#bom1})
	 * 
	 * A byte order mark (BOM) consists of the character code U+FEFF 
	 * at the beginning of a data stream, where it can be used as a 
	 * signature defining the byte order and encoding form, primarily 
	 * of unmarked plaintext files. Under some higher level protocols, 
	 * use of a BOM may be mandatory (or prohibited) in the Unicode 
	 * data stream defined in that protocol.
	 * 
	 * Where is a BOM useful?
	 * A BOM is useful at the beginning of files that are typed as text,
	 * but for which it is not known whether they are in big or little 
	 * endian format—it can also serve as a hint indicating that the 
	 * file is in Unicode, as opposed to in a legacy encoding and 
	 * furthermore, it act as a signature for the specific encoding 
	 * form used.
	 * 
	 * What does 'endian' mean?
	 * Data types longer than a byte can be stored in computer memory 
	 * with the most significant byte (MSB) first or last. The former 
	 * is called big-endian, the latter little-endian. When data is 
	 * exchanged, bytes that appear in the "correct" order on the 
	 * sending system may appear to be out of order on the receiving 
	 * system. In that situation, a BOM would look like 0xFFFE which 
	 * is a noncharacter, allowing the receiving system to apply byte 
	 * reversal before processing the data. UTF-8 is byte oriented and 
	 * therefore does not have that issue. Nevertheless, an initial BOM 
	 * might be useful to identify the datastream as UTF-8.
	 * 
	 * A BOM can be used as a signature no matter how the Unicode text 
	 * is transformed: UTF-16, UTF-8, UTF-7, etc. The exact bytes 
	 * comprising the BOM will be whatever the Unicode character FEFF 
	 * is converted into by that transformation format. In that form, 
	 * the BOM serves to indicate both that it is a Unicode file, and 
	 * which of the formats it is in. Examples:
	 * 
	 * <code>
	 *    Bytes            | Encoding Form
	 *    ----------------------------------------------
	 *    00 00 FE FF      | UTF-32, big-endian
	 *    FF FE 00 00      | UTF-32, big-endian
	 *    FE FF            | UTF-16, big-endian
	 *    FF FE            | UTF-16, little-endian
	 *    EF BB BF         | UTF-8
	 * </code>
	 * 
	 * This method will provide you with the UTF-8 BOM!
	 * 
	 * @access public
	 * @return string
	 */
	public function getUtf8Bom() {
		return chr(0xEF) . chr(0xBB) . chr(0xBF);
	}
	
	/**
	 * Has UTF-8 BOM?
	 * 
	 * This method will tell whether or not a given stream (string)
	 * has been marked with the UTF-8 BOM. For more info about the 
	 * UTF-8 BOM, read {@link M_LocaleCharset::getUtf8Bom()}.
	 * 
	 * @access public
	 * @param string $stream
	 * 		The stream in which to check if a UTF-8 BOM is present
	 * @return boolean $flag
	 * 		Returns TRUE if UTF-8 BOM is present, FALSE if not
	 */
	public function hasUtf8Bom($stream) {
		$bom = self::getUtf8Bom();
		if(!strncmp($stream, $bom, strlen($bom))) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Strip UTF-8 BOM
	 * 
	 * This method will strip the UTF-8 BOM from the given stream 
	 * (string). For more information about the UTF-8 BOM, read the 
	 * docs on {@link M_LocaleCharset::getUtf8Bom()}.
	 * 
	 * @access public
	 * @param string $stream
	 * 		The stream from which to remove the UTF-8 BOM
	 * @return string $string
	 * 		The stream, without the UTF-8 BOM
	 */
	public function stripUtf8Bom($stream) {
		if(self::hasUtf8Bom($stream)) {
			return substr($stream, strlen(self::getUtf8Bom()));
		} else {
			return $stream;
		}
	}
}