<?php
/**
 * M_Decorator class
 * 
 * M_Decorator is used to more naturally chain objects in a
 * Decorator(Decorated) design. For example, M_Decorator is 
 * specially handy in image filters, because each filter may
 * receive additional arguments. For more info, see:
 * 
 * - Decorated object: {@link M_FilterImage}
 * - Decorator object: {@link M_FilterImageDecorator}
 * 
 * However, M_Decorator can be practical in any decorator/
 * decorated pattern. For example, M_Decorator allows to
 * easily swap the decorated object, while maintaining the 
 * chain of decorators.
 * 
 * To learn more about the Decorator(Decorated) pattern, read
 * http://en.wikipedia.org/wiki/Decorator_pattern
 * 
 * @package Core
 */
class M_Decorator {
	/**
	 * The decorated object
	 * 
	 * @access private
	 * @var object
	 */
	private $_decorated;
	
	/**
	 * The chain of decorators
	 * 
	 * This property stores the chain of decorators that should be
	 * applied to the decorated object.
	 * 
	 * @access private
	 * @var array
	 */
	private $_decorator = array();
	
	/**
	 * The decorator interface
	 * 
	 * This property holds the interface that is implemented by
	 * decorators. Read {@link M_Decorator::setInterface()} for more
	 * information.
	 * 
	 * @access private
	 * @var string
	 */
	private $_interface;
	
	/**
	 * Constructor
	 * 
	 * Optionall, the constructor will accept the decorated object.
	 * You can choose to provide with the decorated object after
	 * constructing the M_Decorator object. Read the documentation on
	 * {@link M_Decorator::setDecorated()} for more info.
	 * 
	 * @access public
	 * @param object $object
	 * 		The decorated object
	 * @return M_Decorator
	 */
	public function __construct($object = NULL) {
		if($object) {
			$this->setDecorated($object);
		}
	}
	
	/**
	 * Set decorated object
	 * 
	 * This method will set the decorated object. M_Decorator can be 
	 * practical to easily and quickly swap the decorated object, 
	 * while maintaining the chain of decorators.
	 * 
	 * Example 1, apply text filters on a given text
	 * <code>
	 *    // filter subject
	 *    $text = new M_FilterTextValue('Hello World!');
	 *    
	 *    // create chain of decorators
	 *    $decorator = new M_Decorator;
	 *    $decorator->addDecorator('M_FilterTextPunctuation');
	 *    $decorator->addDecorator('M_FilterTextUTF8Accents');
	 *    
	 *    // set the decorated object
	 *    $decorator->setDecorated($text);
	 * </code>
	 * 
	 * In Example 1, we will apply the filters
	 * 
	 * - {@link M_FilterTextPunctuation}, and then
	 * - {@link M_FilterTextUTF8Accents}
	 * 
	 * on the provided text subject. Let's assume we want to apply the
	 * same chain of filters on another text. We would go about doing
	 * so, by simply changing the decorated object:
	 * 
	 * Example 2
	 * <code>
	 *    // change filter subject
	 *    $text = new M_FilterTextValue('This is a new text');
	 *    
	 *    // change the decorated object that has been set in Example 1
	 *    $decorator->setDecorated($text);
	 * </code>
	 * 
	 * Now, if we build the chain again with {@link M_Decorator::getChain()},
	 * we will apply the same chain of filters on the new text.
	 * 
	 * @access public
	 * @throws M_DecoratorException
	 * @param object $object
	 * 		The decorated object
	 * @return void
	 */
	public function setDecorated($object) {
		if(is_object($object)) {
			$this->_decorated = $object;
		} else {
			throw new M_DecoratorException('Decorated object should be an object');
		}
	}
	
	/**
	 * Add a decorator
	 * 
	 * Check the documentation on {@link M_Decorator::setDecorated()}
	 * for an example on how to add decorators to the chain with
	 * {@link M_Decorator::addDecorator()}.
	 * 
	 * To add arguments to the decorator class, simply add arguments
	 * to the addDecorator() call. Check out Example 1.
	 * 
	 * Example 1
	 * <code>
	 *    // filter subject
	 *    $img = new M_FilterImage('img/picture.jpg');
	 *    
	 *    // create chain of decorators
	 *    $decorator = new M_Decorator;
	 *    $decorator->addDecorator('M_FilterImageResize', 200, 300);
	 *    $decorator->addDecorator('M_FilterImageGrayscale');
	 *    
	 *    // set the decorated object
	 *    $decorator->setDecorated($img);
	 * </code>
	 * 
	 * In Example 1, we will apply the filters
	 * 
	 * - {@link M_FilterImageResize}, and then
	 * - {@link M_FilterImageGrayscale}
	 * 
	 * on the provided image resource. Furthermore, we provide the
	 * filter {@link M_FilterImageResize} with two (additional) 
	 * arguments:
	 * 
	 * <code>
	 *    200
	 *    300
	 * <code>
	 * 
	 * In Example 1, when M_Decorator constructs an object of the
	 * decorator {@link M_FilterImageResize}, it will pass the 
	 * following arguments into its constructor:
	 * 
	 * - argument 1: the decorated object
	 * - argument 2: first additional argument (200)
	 * - argument 3: second additional argument (300)
	 * 
	 * Note that the decorated object is always prepended to the 
	 * set of additional arguments, when a decorator is constructed.
	 * 
	 * @access public
	 * @param string $className
	 * 		The class name of the decorator
	 * @return void
	 */
	public function addDecorator($className) {
		$args = array();
		for($i = 1, $n = func_num_args(); $i < $n; $i ++) {
			$args[] = func_get_arg($i);
		}
		$this->_decorator[] = array($className, $args);
	}
	
	/**
	 * Add a decorator, with arguments
	 * 
	 * Check the documentation on {@link M_Decorator::setDecorated()}
	 * and {@link M_Decorator::addDecorator()} for examples on how to 
	 * add decorators to the chain. This method does exactly the same 
	 * thing as {@link M_Decorator::addDecorator()}, but it collects
	 * the additional arguments in a different fashion.
	 * 
	 * If using addDecoratorArgs(), you should provide the additional
	 * arguments all in one array. Check out Example 1 and 2 for a 
	 * comparison between {@link M_Decorator::addDecorator()} and this 
	 * method.
	 * 
	 * Example 1
	 * <code>
	 *    // filter subject
	 *    $img = new M_FilterImage('img/picture.jpg');
	 *    
	 *    // create chain of decorators
	 *    $decorator = new M_Decorator;
	 *    $decorator->addDecorator('M_FilterImageResize', 200, 300);
	 *    $decorator->addDecorator('M_FilterImageGrayscale');
	 *    
	 *    // set the decorated object
	 *    $decorator->setDecorated($img);
	 * </code>
	 * 
	 * Example 2, a rewrite of Example 1 with addDecoratorArgs()
	 * <code>
	 *    // filter subject
	 *    $img = new M_FilterImage('img/picture.jpg');
	 *    
	 *    // create chain of decorators
	 *    $decorator = new M_Decorator;
	 *    $decorator->addDecoratorArgs('M_FilterImageResize', array(200, 300));
	 *    $decorator->addDecoratorArgs('M_FilterImageGrayscale');
	 *    
	 *    // set the decorated object
	 *    $decorator->setDecorated($img);
	 * </code>
	 */
	public function addDecoratorArgs($className, array $args = array()) {
		$this->_decorator[] = array($className, $args);
	}
	
	/**
	 * Set shared interface
	 * 
	 * The Decorator pattern only works if both the decorator and 
	 * the decorated implement the same interface. This philosophy is
	 * what makes the elements in the chain of decorators chainable, 
	 * and interchangeable.
	 * 
	 * This method will set the interface that must be implemented
	 * by both the decorated and decorator classes. Once you set the
	 * name of an interface with this method, M_Decorator will throw
	 * an {@link M_DecoratorException} if you try to add a decorator
	 * (or decorated) that does not implement the specified interface.
	 * 
	 * @access public
	 * @param string $interface
	 * 		The name of the interface
	 * @return void
	 */
	public function setInterface($interface) {
		$this->_interface = $interface;
	}
	
	/**
	 * Get chain
	 * 
	 * This method will create the chain of decorators on the 
	 * decorated object. The result of this method is an object that 
	 * contains the chain of objects.
	 * 
	 * Example 1, apply text filters on a given text
	 * <code>
	 *    // filter subject
	 *    $text = new M_FilterTextValue('Hello World!');
	 *    
	 *    // create chain of decorators
	 *    $decorator = new M_Decorator;
	 *    $decorator->addDecorator('M_FilterTextPunctuation');
	 *    $decorator->addDecorator('M_FilterTextUTF8Accents');
	 *    
	 *    // set the decorated object
	 *    $decorator->setDecorated($text);
	 * </code>
	 * 
	 * In Example 1, we will apply the filters (decorators)
	 * 
	 * - {@link M_FilterTextPunctuation}, and then
	 * - {@link M_FilterTextUTF8Accents}
	 * 
	 * on the provided text subject. Once we have set up the chain
	 * of decorators, we can ask M_Decorator to create the chain. By 
	 * doing so, M_Decorator will construct the objects as following 
	 * (continuing on Example 1):
	 * 
	 * <code>
	 *    new M_FilterTextUTF8Accents(
	 *       new M_FilterTextPunctuation(
	 *          new M_FilterTextValue('Hello World!')
	 *       )
	 *    );
	 * </code>
	 * 
	 * So, in the case of Example 1, this method would return an 
	 * object of {@link M_FilterTextUTF8Accents} that contains an 
	 * object of {@link M_FilterTextPunctuation} as the decorated 
	 * object, which in turn contains the {@link M_FilterTextValue}
	 * as the decorated object.
	 * 
	 * Knowing that the decorators and decorated all implement the 
	 * same {@link MI_Filter} interface, which is a requirement in
	 * the decorator pattern, we can now call the apply() method on
	 * the chain:
	 * 
	 * Example 2
	 * <code>
	 *    // filter subject
	 *    $text = new M_FilterTextValue('Hello World!');
	 *    
	 *    // create chain of decorators
	 *    $decorator = new M_Decorator;
	 *    $decorator->addDecorator('M_FilterTextPunctuation');
	 *    $decorator->addDecorator('M_FilterTextUTF8Accents');
	 *    
	 *    // set the decorated object
	 *    $decorator->setDecorated($text);
	 *    
	 *    // get the chain, and call the apply() method of each object
	 *    // in the chain
	 *    echo $decorator->getChain()->apply();
	 * </code>
	 * 
	 * If, for any reason, M_Decorator fails to construct an object
	 * in the chain, M_Decorator will throw an {@link M_DecoratorException}.
	 * 
	 * @access public
	 * @throws M_DecoratorException
	 * @return object
	 */
	public function getChain() {
		$object = $this->_decorated;
		for($i = 0, $n = count($this->_decorator); $i < $n; $i ++) {
			$class = new ReflectionClass($this->_decorator[$i][0]);
			if($class->isInstantiable()) {
				if($this->_interface) {
					if(!in_array($this->_interface, class_implements($class->getName()))) {
						throw new M_DecoratorException(sprintf(
							'M_Decorator cannot create the chain, because %s does not implement the %s interface',
							$this->_decorator[$i][0],
							$this->_interface
						));
					}
				}
				$args = $this->_decorator[$i][1];
				array_unshift($args, $object);
				$object = $class->newInstanceArgs($args);
			} else {
				throw new M_DecoratorException(sprintf(
					'%s cannot be instantiated', 
					$this->_decorator[$i][0]
				));
			}
		}
		return $object;
	}
}
?>