<?php
/**
 * M_ServiceException
 * 
 * Used to handle exceptions that get thrown by the M_Service classes
 * 
 * @package Core
 */
class M_ServiceException extends M_Exception {
}