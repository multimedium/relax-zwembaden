<?php
/**
 * M_ServiceTwitterStatus
 *
 * ... contains the data of a twitter user-status
 */
class M_ServiceTwitterStatus {

	/* -- PROPERTIES -- */

	private $_createdAt;
	private $_id;
	private $_text;
	private $_source;

	private $_user;
	
	/* -- GETTERS -- */
	
	/**
	 * @return the $_createdAt
	 */
	public function getCreatedAt() {
		return new M_Date($this->_createdAt);
	}

	/**
	 * @return the $_id
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * @return the $_text
	 */
	public function getText() {
		return $this->_text;
	}

	/**
	 * @return the $_source
	 */
	public function getSource() {
		return $this->_source;
	}

	/**
	 * get User
	 *
	 * @return M_ServiceTwitterUser $user
	 */
	public function getUser() {
		return $this->_user;
	}
	
	/* -- SETTERS -- */

	/**
	 * @param $_createdAt the $_createdAt to set
	 */
	public function setCreatedAt($_createdAt) {
		$this->_createdAt = $_createdAt;
	}

	/**
	 * @param $_id the $_id to set
	 */
	public function setId($_id) {
		$this->_id = $_id;
	}

	/**
	 * @param $_text the $_text to set
	 */
	public function setText($_text) {
		$this->_text = $_text;
	}

	/**
	 * @param $_source the $_source to set
	 */
	public function setSource($_source) {
		$this->_source = $_source;
	}

	/**
	 * Set the user of this status
	 *
	 * @param M_ServiceTwitterUser $user
	 */
	public function setUser(M_ServiceTwitterUser $user) {
		$this->_user = $user;
	}

}