<?php
class M_ServiceGoogleMapsLocation {

	/**
	 * Latitude
	 *
	 * @var float
	 */
	private $_latitude;

	/**
	 * Longitude
	 *
	 * @var float
	 */
	private $_longitude;

	/**
	 * Create a new M_ServiceGoogleMapsLocation using a string
	 *
	 * A location string is typically a combination of langitude and latitude
	 * separated with a ,
	 *
	 * @example M_ServiceGoogleMapsLocation
	 * @param string $arg
	 * @return  M_ServiceGoogleMapsLocation
	 */
	public static function constructFromString($arg) {
		$location = new M_ServiceGoogleMapsLocation();
		$locationData = explode(',', (string)$arg);

		//check if 2 elements are found
		if (count($locationData) != 2) {
			throw new M_Exception(sprintf(
				'Cannot create a new M_ServiceGoogleMapsLocation from "%s"',
				$arg)
			);
		}

		return $location
				->setLatitude($locationData[0])
				->setLongitude($locationData[1]);
	}

	/**
	 * Get Latitude
	 *
	 * @return float
	 */
	public function getLatitude() {
		return $this->_latitude;
	}

	/**
	 * Set Latitude
	 *
	 * @param float $arg
	 * @return M_ServiceGoogleMapsLocation
	 */
	public function setLatitude($arg) {
		$this->_latitude = $arg;
		return $this;
	}

	/**
	 * Get Longitude
	 *
	 * @return float
	 */
	public function getLongitude() {
		return $this->_longitude;
	}

	/**
	 * Set Longitude
	 *
	 * @param float $arg
	 * @return M_ServiceGoogleMapsLocation
	 */
	public function setLongitude($arg) {
		$this->_longitude = $arg;
		return $this;
	}

	/**
	 * Create a location string
	 *
	 * Returns a typical location string: "latitude,longitude" if both longitude
	 * and latitude are set
	 *
	 * @return string|null
	 */
	public function toString() {
		if ($this->getLatitude() && $this->getLongitude()) {
			return $this->getLatitude().','.$this->getLongitude();
		}else return null;
	}

	/**
	 * Magic string method
	 *
	 * @see M_ServiceGoogleMaps::toString()
	 * @return string
	 */
	public function __toString() {
		return $this->toString();
	}
}
