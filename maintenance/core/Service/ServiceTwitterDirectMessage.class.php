<?php
/**
 * M_ServiceTwitterUser
 *
 * ... contains the data of a twitter direct message
 */
class M_ServiceTwitterDirectMessage {

	/* -- PROPERTIES -- */
	private $_id;
	private $_text;
	private $_senderId;
	private $_recipientId;
	private $_createdAt;
	private $_senderScreenName;
	private $_recipientScreenName;
	
	/**
	 * @return the $Id
	 */
	public function getId() {
		return $this->Id;
	}

	/**
	 * @return the $Text
	 */
	public function getText() {
		return $this->Text;
	}

	/**
	 * @return the $SenderId
	 */
	public function getSenderId() {
		return $this->SenderId;
	}

	/**
	 * @return the $RecipientId
	 */
	public function getRecipientId() {
		return $this->RecipientId;
	}

	/**
	 * @return the $CreatedAt
	 */
	public function getCreatedAt() {
		return $this->CreatedAt;
	}

	/**
	 * @return the $SenderScreenName
	 */
	public function getSenderScreenName() {
		return $this->SenderScreenName;
	}

	/**
	 * @return the $RecipientScreenName
	 */
	public function getRecipientScreenName() {
		return $this->RecipientScreenName;
	}

	/**
	 * @param $Id the $Id to set
	 */
	public function setId($Id) {
		$this->Id = $Id;
	}

	/**
	 * @param $Text the $Text to set
	 */
	public function setText($Text) {
		$this->Text = $Text;
	}

	/**
	 * @param $SenderId the $SenderId to set
	 */
	public function setSenderId($SenderId) {
		$this->SenderId = $SenderId;
	}

	/**
	 * @param $RecipientId the $RecipientId to set
	 */
	public function setRecipientId($RecipientId) {
		$this->RecipientId = $RecipientId;
	}

	/**
	 * @param $CreatedAt the $CreatedAt to set
	 */
	public function setCreatedAt($CreatedAt) {
		$this->CreatedAt = $CreatedAt;
	}

	/**
	 * @param $SenderScreenName the $SenderScreenName to set
	 */
	public function setSenderScreenName($SenderScreenName) {
		$this->SenderScreenName = $SenderScreenName;
	}

	/**
	 * @param $RecipientScreenName the $RecipientScreenName to set
	 */
	public function setRecipientScreenName($RecipientScreenName) {
		$this->RecipientScreenName = $RecipientScreenName;
	}
}