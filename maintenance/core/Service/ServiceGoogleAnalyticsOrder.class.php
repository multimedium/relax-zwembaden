<?php
/**
 * M_ServiceGoogleAnalyticsOrder
 *
 * This class can be used to create objects that represent an order, in Google
 * Analytics. Typically, an object of this class is used to track a new e-commerce
 * order on the website. For example:
 *
 * <code>
 *    M_ServiceGoogleAnalytics
 *       ::getInstance()
 *       ->addTrackingOfOrder(
 *          new M_ServiceGoogleAnalyticsOrder(
 *             '1234',           // order ID - required
 *             'Womens Apparel', // affiliation or store name
 *             '28.28',          // total - required
 *             '1.29',           // tax
 *             '15.00',          // shipping
 *             'San Jose',       // city
 *             'California',     // state or province
 *             'USA'             // country
 *          )
 *       );
 * </code>
 *
 * @package Core
 */
class M_ServiceGoogleAnalyticsOrder extends M_Object {

	/* -- PROPERTIES -- */

	/**
	 * Order ID
	 *
	 * @access private
	 * @var string
	 */
	private $_id;

	/**
	 * Affiliation, or store name
	 *
	 * @access private
	 * @var string
	 */
	private $_affiliation;

	/**
	 * Total amount
	 *
	 * @access private
	 * @var float
	 */
	private $_total;

	/**
	 * Tax
	 *
	 * @access private
	 * @var float
	 */
	private $_tax;

	/**
	 * Shipping costs
	 *
	 * @access private
	 * @var float
	 */
	private $_shipping;

	/**
	 * City
	 *
	 * @access private
	 * @var string
	 */
	private $_city;

	/**
	 * State/Province
	 *
	 * @access private
	 * @var string
	 */
	private $_state;

	/**
	 * Country
	 *
	 * @access private
	 * @var string
	 */
	private $_country;

	/**
	 * Items in the order
	 *
	 * @access private
	 * @var array $items
	 *		The collection of contained {@link M_ServiceGoogleAnalyticsOrderItem}
	 *		instances
	 */
	private $_items = array();

	/* -- CONSTRUCTOR -- */

	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $id
	 * @param string $affiliation
	 * @param float  $total
	 * @param float  $tax
	 * @param float  $shipping
	 * @param string $city
	 * @param string $state
	 * @param string $country
	 * @return M_ServiceGoogleAnalyticsOrder
	 */
	public function __construct($id, $affiliation = NULL, $total = 0, $tax = 0, $shipping = 0, $city = NULL, $state = NULL, $country = NULL) {
		// Set the Order ID
		$this->setId($id);

		// Set the affiliation (if any)
		if($affiliation) {
			$this->setAffiliation($affiliation);
		}

		// Set total (if any)
		if($total) {
			$this->setTotal($total);
		}

		// Set tax (VAT)
		if($tax) {
			$this->setTax($tax);
		}

		// Set shipping costs (if any)
		if($shipping) {
			$this->setShipping($shipping);
		}

		// Set City (if any)
		if($city) {
			$this->setCity($city);
		}

		// Set state or province (if any)
		if($state) {
			$this->setState($state);
		}

		// Set country (if any)
		if($country) {
			$this->setCountry($country);
		}
	}

	/* -- GETTERS -- */

	/**
	 * Get Order ID
	 *
	 * @access public
	 * @return string
	 */
	public function getId() {
		return $this->_id;
	}

	/**
	 * Get Afilliation or store name
	 *
	 * @access public
	 * @return string
	 */
	public function getAffiliation() {
		return $this->_affiliation;
	}

	/**
	 * Get total amount
	 *
	 * @access public
	 * @return float
	 */
	public function getTotal() {
		return $this->_total;
	}

	/**
	 * Get tax
	 *
	 * @access public
	 * @return float
	 */
	public function getTax() {
		return $this->_tax;
	}

	/**
	 * Get shipping costs
	 *
	 * @access public
	 * @return float
	 */
	public function getShipping() {
		return $this->_shipping;
	}

	/**
	 * Get city
	 *
	 * @access public
	 * @return string
	 */
	public function getCity() {
		return $this->_city;
	}

	/**
	 * Get State or province
	 *
	 * @access public
	 * @return string
	 */
	public function getState() {
		return $this->_state;
	}

	/**
	 * Get country
	 *
	 * @access public
	 * @return string
	 */
	public function getCountry() {
		return $this->_country;
	}

	/**
	 * Get contained items
	 *
	 * Will provide with the collection of {@link M_ServiceGoogleAnalyticsOrderItem}
	 * instances that has been added to the order previously with the method
	 * {@link M_ServiceGoogleAnalyticsOrder::addItem()}
	 *
	 * @access public
	 * @return float
	 */
	public function getItems() {
		return new M_ArrayIterator($this->_items);
	}

	/* -- SETTERS -- */

	/**
	 * Set Order ID
	 *
	 * @access public
	 * @param string $id
	 * @return M_ServiceGoogleAnalyticsOrder $order
	 *		Returns itself, for a fluent programming interface
	 */
	public function setId($id) {
		$this->_id = (string) $id;
		return $this;
	}

	/**
	 * Set Afilliation or store name
	 *
	 * @access public
	 * @param string $afilliation
	 * @return M_ServiceGoogleAnalyticsOrder $order
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAffiliation($affiliation) {
		$this->_affiliation = (string) $affiliation;
		return $this;
	}

	/**
	 * Set Total amount
	 *
	 * @access public
	 * @param float $total
	 * @return M_ServiceGoogleAnalyticsOrder $order
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTotal($total) {
		$this->_total = (float) $total;
		return $this;
	}

	/**
	 * Set Tax
	 *
	 * @access public
	 * @param float $tax
	 * @return M_ServiceGoogleAnalyticsOrder $order
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTax($tax) {
		$this->_tax = (float) $tax;
		return $this;
	}

	/**
	 * Set Shipping costs
	 *
	 * @access public
	 * @param float $shipping
	 * @return M_ServiceGoogleAnalyticsOrder $order
	 *		Returns itself, for a fluent programming interface
	 */
	public function setShipping($shipping) {
		$this->_shipping = (float) $shipping;
		return $this;
	}

	/**
	 * Set city
	 *
	 * @access public
	 * @param string $city
	 * @return M_ServiceGoogleAnalyticsOrder $order
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCity($city) {
		$this->_city = (string) $city;
		return $this;
	}

	/**
	 * Set state or province
	 *
	 * @access public
	 * @param string $state
	 * @return M_ServiceGoogleAnalyticsOrder $order
	 *		Returns itself, for a fluent programming interface
	 */
	public function setState($state) {
		$this->_state = (string) $state;
		return $this;
	}

	/**
	 * Set country
	 *
	 * @access public
	 * @param string $country
	 * @return M_ServiceGoogleAnalyticsOrder $order
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCountry($country) {
		$this->_country = (string) $country;
		return $this;
	}

	/**
	 * Add an item to the order
	 *
	 * @see M_ServiceGoogleAnalyticsOrder::getItems()
	 * @access public
	 * @param M_ServiceGoogleAnalyticsOrderItem $item
	 * @return M_ServiceGoogleAnalyticsOrder $order
	 *		Returns itself, for a fluent programming interface
	 */
	public function addItem(M_ServiceGoogleAnalyticsOrderItem $item) {
		$this->_items[] = $item;
		return $this;
	}
}