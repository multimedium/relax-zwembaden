<?php
/**
 * M_ServiceGoogleAnalytics
 *
 * @package Core
 */
class M_ServiceGoogleAnalytics extends M_Object {

	/* -- PROPERTIES -- */

	/**
	 * Singleton instance
	 *
	 * Stores the singleton instance that can be constructed with the static
	 * {@link M_ServiceGoogleAnalytics::getInstance()}
	 *
	 * @static
	 * @access private
	 * @var M_ServiceGoogleAnalytics
	 */
	private static $_instance;

	/**
	 * Unique Account Identifier
	 *
	 * Stores the account identifier. Google calls this the UA variable
	 *
	 * @access private
	 * @var string
	 */
	private $_ua;

	/**
	 * Tracking instructions
	 *
	 * Stores the tracking instructions that have been added to the Google
	 * Analytics singleton object
	 *
	 * @access private
	 * @var array
	 */
	private $_trackingInstructions = array();

	/* -- CONSTRUCTORS -- */

	/**
	 * PRIVATE Constructor
	 *
	 * Note that, since there can be only one google analytics account per page,
	 * the constructor is private. Instead, you are required to construct a singleton
	 * with the typical {@link M_ServiceGoogleAnalytics::getInstance()} method.
	 *
	 * @access private
	 * @return M_ServiceGoogleAnalytics
	 */
	private final function __construct() {}

	/**
	 * Singleton constructor
	 *
	 * @static
	 * @access public
	 * @return M_ServiceGoogleAnalytics
	 */
	public static function getInstance() {
		// If not already constructed
		if(! self::$_instance) {
			// Then, construct the singleton now:
			self::$_instance = new self;
		}

		// Return the singleton:
		return self::$_instance;
	}

	/* -- GETTERS -- */

	/**
	 * Get javascript code
	 *
	 * Will render the javascript that is required to register all of the
	 * requested tracking instructions.
	 *
	 * @access public
	 * @return string
	 */
	public function getJavascriptCode() {
		// If no Unique Account ID has been provided to the Google Analytics
		// singleton, then we cannot generate the javascript code for tracking.
		// First, we check if an account ID has been provided
		if(! $this->_ua) {
			// If not, then we throw an exception to inform about the error
			throw new M_Exception(sprintf(
				'Cannot render the javascript code for Google Analytics. The Unique ' .
				'Account ID (UA) has not yet been provided to the singleton object %s. ' .
				'Please use %s::%s() to set the Account ID',
				$this->getClassName(),
				$this->getClassName(),
				'setAccount'
			));
		}

		// New-line character to be used in the javascript:
		$nl = "\n";

		// The output; the javascript code
		$js  = '';
		$js .= '<script type="text/javascript">' . $nl;
		$js .=    'var _gaq = _gaq || []; ' . $nl;
		$js .=    '_gaq.push([\'_setAccount\', \''.$this->_ua.'\']); ' . $nl;

		// For each of the tracking instructions that have been added to the
		// google analytics singleton:
		foreach($this->_trackingInstructions as $instruction) {
			// We add the javascript code for the current instruction:
			$js .= $this->_getJavascriptCodeForInstruction($instruction);
		}

		// Add Google Analytics' script snippet
		$js .= '(function() { ' . $nl;
		$js .=    'var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true; ' . $nl;
		$js .=    'ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\'; ' . $nl;
		$js .=    'var s = document.getElementsByTagName(\'script\')[0]; ' . $nl;
		$js .=    's.parentNode.insertBefore(ga, s); ' . $nl;
		$js .= '})();' . $nl;
		$js .= '</script>';

		// Finally, return the script
		return $js;
	}

	/* -- SETTERS -- */

	/**
	 * Set Account
	 *
	 * Set the Google Analytics account for which the tracking instructions should
	 * be registered. Google refers to this Account ID with the variable name UA.
	 *
	 * @access public
	 * @param string $ua
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAccount($ua) {
		$this->_ua = (string) $ua;
		return $this;
	}

	/**
	 * Add tracking of page view
	 *
	 * Will add the necessary tracking instructions so that the page view is
	 * registered in Google Analytics
	 *
	 * @access public
	 * @param string $ua
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function addTrackingOfPageView() {
		$this->_addTrackingInstruction('_trackPageview', array(), TRUE);
		return $this;
	}

	/**
	 * Add tracking of order
	 *
	 * Will add the necessary tracking instructions so that a given order is
	 * registered in Google Analytics (E-Commerce)
	 *
	 * @access public
	 * @param M_ServiceGoogleAnalyticsOrder $order
	 *		The order that is to be registered in the statistics
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function addTrackingOfOrder(M_ServiceGoogleAnalyticsOrder $order) {
		// Add the transaction:
		$this->_addTrackingInstruction('_addTrans', array(
			$order->getId(),
			$order->getAffiliation(),
			$order->getTotal(),
			$order->getTax(),
			$order->getShipping(),
			$order->getCity(),
			$order->getState(),
			$order->getCountry()
		));

		// Then, we add the items in the transaction. For each of the items in
		// the order:
		foreach($order->getItems() as $item) {
			/* @var $item M_ServiceGoogleAnalyticsOrderItem */
			// Add the tracking instructions for the current item
			$this->_addTrackingInstruction('_addItem', array(
				$item->getOrderId(),
				$item->getCode(),
				$item->getName(),
				$item->getCategory(),
				$item->getPricePerUnit(),
				$item->getQuantity()
			));
		}

		// Finally, add the instruction to track the entire transaction:
		$this->_addTrackingInstruction('_trackTrans');
		
		// Return myself:
		return $this;
	}

	/**
	 * Add tracking of order
	 *
	 * Will add the necessary tracking instructions so that a given order is
	 * registered in Google Analytics (E-Commerce)
	 *
	 * @access public
	 * @param M_ServiceGoogleAnalyticsEvent $event
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function addTrackingOfEvent(M_ServiceGoogleAnalyticsEvent $event) {
		// Compose the collection of arguments
		$args = array(
			$event->getCategory(),
			$event->getAction()
		);

		// If the event has been given a description:
		if($event->getDescription()) {
			// Then, we add that description to the list of arguments:
			$args[] = $event->getDescription();
		}

		// If the event has been given a value:
		if($event->getValue()) {
			// Then, we add that value to the list of arguments:
			$args[] = $event->getValue();
		}

		// Add the transaction:
		$this->_addTrackingInstruction('_trackEvent', $args);

		// Return myself:
		return $this;
	}

	/**
	 * Set User Session Timeout
	 *
	 * This method sets the new session cookie timeout, in seconds. By default,
	 * session timeout is set to 30 minutes. Session timeout is used to compute
	 * visits, since a visit ends after 30 minutes of browser inactivity or upon
	 * browser exit. If you want to change the definition of a "session" for your
	 * particular needs, you can pass in the number of seconds to define a new
	 * value. This will impact the Visits reports in every section where the
	 * number of visits are calculated, and where visits are used in computing
	 * other values.
	 *
	 * For example, the number of visits will increase if you shorten the session
	 * timeout, and will decrease if you increase the session timeout. You can
	 * change the expiration timeout to 0 to indicate that this cookie should be
	 * deleted when the browser is closed.
	 *
	 * @access public
	 * @param integer $numberOfSeconds
	 *		The number of seconds
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setUserSessionDuration($numberOfSeconds) {
		$this->_addTrackingInstruction('_setSessionCookieTimeout', array(((int) $numberOfSeconds * 1000)));
		return $this;
	}

	/**
	 * Set Visitor's cookie lifetime
	 *
	 * This method sets the Google Analytics visitor cookie expiration, in seconds.
	 * By default, the visitor cookie is set to expire in 2 years. If you prefer,
	 * you can change the expiration date of the visitor cookie using this method.
	 * You can change the expiration timeout to 0 to indicate that this cookie
	 * should be deleted when the browser is closed.
	 *
	 * @access public
	 * @param integer $numberOfSeconds
	 *		The number of seconds
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setVisitorCookieLifetime($numberOfSeconds) {
		$this->_addTrackingInstruction('_setVisitorCookieTimeout', array(((int) $numberOfSeconds * 1000)));
		return $this;
	}

	/**
	 * Set the browser tracking module.
	 *
	 * By default, Google Analytics tracks browser information from your visitors
	 * and provides more data about your visitor's browser settings that you get
	 * with a simple HTTP request. If you desire, you can turn this tracking off
	 * by setting the parameter to false. If you do this, any browser data will
	 * not be tracked and cannot be recovered at a later date, so use this feature
	 * carefully.
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE to enable the tracking of browser information, FALSE to
	 *		disable the registration of that information.
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTrackingOfBrowserInfo($flag) {
		$this->_addTrackingInstruction('_setClientInfo', array((bool) $flag));
		return $this;
	}

	/**
	 * Set the "allow domain hash" flag.
	 *
	 * By default, this value is set to true. The domain hashing functionality in
	 * Google Analytics creates a hash value from your domain, and uses this number
	 * to check cookie integrity for visitors. If you have multiple sub-domains,
	 * such as example1.example.com and example2.example.com, and you want to
	 * track user behavior across both of these sub-domains, you would turn off
	 * domain hashing so that the cookie integrity check will not reject a user
	 * cookie coming from one domain to another. Additionally, you can turn this
	 * feature off to optimize per-page tracking performance.
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE to allow domain name hashing, FALSE to not allow this
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAllowDomainNameHash($flag) {
		$this->_addTrackingInstruction('_setAllowHash', array((bool) $flag));
		return $this;
	}

	/**
	 * Set the Flash detection flag.
	 *
	 * By default, Google Analytics tracks Flash player information from your
	 * visitors and provides detailed data about your visitor's Flash player
	 * settings. If you desire, you can turn this tracking off by setting the
	 * parameter to false. If you do this, any Flash player data will not be
	 * tracked and cannot be recovered at a later date, so use this feature
	 * carefully.
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE to enable the tracking of flash, FALSE to
	 *		disable the registration of that information.
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTrackingOfFlash($flag) {
		$this->_addTrackingInstruction('_setDetectFlash', array((bool) $flag));
		return $this;
	}

	/**
	 * Set the title detection flag.
	 *
	 * By default, page title detection for your visitors is on. This information
	 * appears in the Contents section under "Content by Title." If you desire,
	 * you can turn this tracking off by setting the parameter to false. You
	 * could do this if your website has no defined page titles and the Content
	 * by Title report has all content grouped into the "(not set)" list. You
	 * could also turn this off if all your pages have particularly long titles.
	 * If you do this, any page titles that are defined in your website will not
	 * be displayed in the "Content by Title" reports. This information cannot
	 * be recovered at a later date once it is disabled.
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE to enable the tracking of page titles, FALSE to
	 *		disable the registration of that information.
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTrackingOfPageTitles($flag) {
		$this->_addTrackingInstruction('_setDetectTitle', array((bool) $flag));
		return $this;
	}

	/**
	 * Set the campaign tracking flag.
	 *
	 * By default, campaign tracking is set to TRUE for standard Google Analytics
	 * set up. If you wish to disable campaign tracking and the associated cookies
	 * that are set for campaign tracking, you can use this method.
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE to enable the tracking of campaigns, FALSE to
	 *		disable the registration of that information.
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTrackingOfCampaign($flag) {
		$this->_addTrackingInstruction('_setCampaignTrack', array((bool) $flag));
		return $this;
	}

	/**
	 * Set the campaign tracking cookie expiration time, in seconds.
	 *
	 * By default, campaign tracking is set for 6 months. In this way, you can
	 * determine over a 6-month period whether visitors to your site convert based
	 * on a specific campaign. However, your business might have a longer or
	 * shorter campaign time-frame, so you can use this method to adjust the
	 * campaign tracking for that purpose. You can change the expiration timeout
	 * to 0 to indicate that this cookie should be deleted when the browser is
	 * closed.
	 *
	 * @access public
	 * @param integer $numberOfSeconds
	 *		The number of seconds
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCampaignTimeout($numberOfSeconds) {
		$this->_addTrackingInstruction('_setCampaignCookieTimeout', array(((int) $numberOfSeconds * 1000)));
		return $this;
	}

	/**
	 * Set Campaign Name Key
	 * 
	 * Google Analytics automatically collects your Google AdWords data if you
	 * have linked your Adwords account to your Analytics account. To track keyword
	 * links from other advertising sources, or from email campaigns or similar
	 * sources, you can create custom campaigns fields using this method (among
	 * others).
	 *
	 * This method sets the campaign name key. The campaign name key is used to
	 * retrieve the name of your advertising campaign from your campaign URLs. You
	 * would use this function on any page that you want to track click-campaigns
	 * on.
	 *
	 * For example, suppose you send an email to registered users about a special
	 * offer, and the link to that offer looks like:
	 *
	 * <code>http://mysite.net/index.html?source=In+House&method=email&offer_type=Fall+email+offers</code>
	 *
	 * In this url, the key "offer_type" delineates the name supplied in the URL
	 * for that campaign. (This is the name that appears in the list of Campaigns
	 * in the Traffic Sources report.) To use that key as your customized campaign
	 * name key, you would set:
	 *
	 * <code>
	 *    M_ServiceGoogleAnalytics
	 *       ::getInstance()
	 *       ->setCampaignNameKey('offer_type');
	 * </code>
	 *
	 * @access public
	 * @param string $nameKey
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCampaignNameKey($nameKey) {
		$this->_addTrackingInstruction('_setCampNameKey', array((string) $nameKey));
		return $this;
	}

	/**
	 * Set Campaign Medium Key
	 *
	 * Google Analytics automatically collects your Google AdWords data if you
	 * have linked your Adwords account to your Analytics account. To track keyword
	 * links from other advertising sources, or from email campaigns or similar
	 * sources, you can create custom campaigns fields using this method (among
	 * others).
	 *
	 * This method sets the campaign medium key, which is used to retrieve the
	 * medium from your campaign URLs. The medium appears as a segment option in
	 * the Campaigns report.
	 *
	 * For example, suppose you have an ad on another website with this URL
	 * to your site:
	 *
	 * <code>http://mysite.net/index.html?source=giganoshopper.com&method=ad&offer_type=Christmas+specials&description=Garden+gloves</code>
	 *
	 * In this url, the key "method" delineates the medium in the URL for that
	 * campaign. To use that key as your customized campaign content key, you
	 * would set:
	 *
	 * <code>
	 *    M_ServiceGoogleAnalytics
	 *       ::getInstance()
	 *       ->setCampaignMediumKey('method');
	 * </code>
	 *
	 * @access public
	 * @param string $mediumKey
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCampaignMediumKey($mediumKey) {
		$this->_addTrackingInstruction('_setCampMediumKey', array((string) $mediumKey));
		return $this;
	}

	/**
	 * Set Campaign Source Key
	 *
	 * Google Analytics automatically collects your Google AdWords data if you
	 * have linked your Adwords account to your Analytics account. To track keyword
	 * links from other advertising sources, or from email campaigns or similar
	 * sources, you can create custom campaigns fields using this method (among
	 * others).
	 *
	 * This method sets the campaign source key, which is used to retrieve the
	 * campaign source from the URL. "Source" appears as a segment option in the
	 * Campaigns report.
	 *
	 * For example, suppose you have an ad on another website with this URL to
	 * your site:
	 *
	 * <code>http://mysite.net/index.html?source=giganoshopper.com&method=referral&offer_type=Christmas+specials&description=Garden+gloves</code>
	 *
	 * In this url, the key "source" delineates the source in the URL for that
	 * campaign. To use that key as your customized campaign source key, you
	 * would set:
	 *
	 * <code>
	 *    M_ServiceGoogleAnalytics
	 *       ::getInstance()
	 *       ->setCampaignSourceKey('source');
	 * </code>
	 *
	 * @access public
	 * @param string $sourceKey
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCampaignSourceKey($sourceKey) {
		$this->_addTrackingInstruction('_setCampSourceKey', array((string) $sourceKey));
		return $this;
	}

	/**
	 * Set Campaign Keyword/Term Key
	 *
	 * Google Analytics automatically collects your Google AdWords data if you
	 * have linked your Adwords account to your Analytics account. To track keyword
	 * links from other advertising sources, or from email campaigns or similar
	 * sources, you can create custom campaigns fields using this method (among
	 * others).
	 *
	 * This method sets the campaign term key, which is used to retrieve the
	 * campaign keywords from the URL.
	 *
	 * For example, suppose you have a paid ad on a search engine tagged as follows:
	 *
	 * <code>http://mysite.net/index.html?source=weSearch4You.com&method=paidSearchAd&offer_type=Christmas+specials&description=Garden+gloves&term=garden+tools</code>
	 *
	 * In this url, the key "term" delineates the keyword terms in the URL for
	 * that campaign. To use that key as your customized campaign term key, you
	 * would set:
	 *
	 * <code>
	 *    M_ServiceGoogleAnalytics
	 *       ::getInstance()
	 *       ->setCampaignTermKey('term');
	 * </code>
	 *
	 * @access public
	 * @param string $termKey
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCampaignTermKey($termKey) {
		$this->_addTrackingInstruction('_setCampTermKey', array((string) $termKey));
		return $this;
	}

	/**
	 * Set Campaign Ad Content Key
	 *
	 * Google Analytics automatically collects your Google AdWords data if you
	 * have linked your Adwords account to your Analytics account. To track keyword
	 * links from other advertising sources, or from email campaigns or similar
	 * sources, you can create custom campaigns fields using this method (among
	 * others).
	 *
	 * This method sets the campaign ad content key. The campaign content key is
	 * used to retrieve the ad content (description) of your advertising campaign
	 * from your campaign URLs. Use this function on the landing page defined in
	 * your campaign.
	 *
	 * For example, suppose you have an ad on another website with this URL to
	 * your site:
	 *
	 * <code>http://mysite.net/index.html?source=giganoshopper.com&method=referral&offer_type=Christmas+specials&description=Garden+gloves</code>
	 *
	 * In this url, the key "description" delineates the content supplied in the
	 * URL for that campaign. (These terms and phrases appear under the Ad Content
	 * column in the Campaign detail page in the Traffic Sources report.) To use
	 * that key as your customized campaign content key, you would set:
	 *
	 * <code>
	 *    M_ServiceGoogleAnalytics
	 *       ::getInstance()
	 *       ->setCampaignAdContentKey('description');
	 * </code>
	 *
	 * @access public
	 * @param string $contentKey
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCampaignAdContentKey($contentKey) {
		$this->_addTrackingInstruction('_setCampContentKey', array((string) $contentKey));
		return $this;
	}

	/**
	 * Set Campaign No-Override Key Variable
	 *
	 * Google Analytics automatically collects your Google AdWords data if you
	 * have linked your Adwords account to your Analytics account. To track keyword
	 * links from other advertising sources, or from email campaigns or similar
	 * sources, you can create custom campaigns fields using this method (among
	 * others).
	 *
	 * This method sets the campaign no-override key variable, which is used to
	 * retrieve the campaign no-override value from the URL. By default, this
	 * variable and its value are not set. For campaign tracking and conversion
	 * measurement, this means that, by default, the most recent impression is
	 * the campaign that is credited in your conversion tracking. If you prefer
	 * to associate the first-most impressions to a conversion, you would set this
	 * method to a specific key, and in the situation where you use custom campaign
	 * variables, you would use this method to set the variable name for campaign
	 * overrides. The no-override value prevents the campaign data from being
	 * over-written by similarly-defined campaign URLs that the visitor might
	 * also click on.
	 *
	 * If you have an ad on another website with this URL to your site:
	 *
	 * <code>http://mysite.net/index.html?source=giganoshopper.com&method=referral&offer_type=Christmas+specials&description=Garden+gloves&noo=1234</code>
	 *
	 * In this url, the key "noo" delineates the no-override value in the URL for
	 * that campaign. To use that key as your customized campaign no-override key,
	 * you would set:
	 *
	 * <code>
	 *    M_ServiceGoogleAnalytics
	 *       ::getInstance()
	 *       ->setCampaignNoOverrideKey('noo');
	 * </code>
	 *
	 * @access public
	 * @param string $nooKey
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCampaignNoOverrideKey($nooKey) {
		$this->_addTrackingInstruction('_setCampNOKey', array((string) $nooKey));
		return $this;
	}

	/**
	 * Ignore keyword, in organic search result
	 *
	 * This method sets the string as ignored term(s) for Keywords reports. Use
	 * this to configure Google Analytics to treat certain search terms as direct
	 * traffic, such as when users enter your domain name as a search term. When
	 * you set keywords using this method, the search terms are still included in
	 * your overall page view counts, but not included as elements in the
	 * Keywords reports.
	 *
	 * For example:
	 * <code>
	 *    M_ServiceGoogleAnalytics
	 *       ::getInstance()
	 *       ->addIgnoredOrganicKeyword('www.mydomainname.com');
	 * </code>
	 *
	 * @access public
	 * @param string $ignoredKeyword
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function addIgnoredOrganicKeyword($ignoredKeyword) {
		$this->_addTrackingInstruction('_addIgnoredOrganic', array((string) $ignoredKeyword));
		return $this;
	}

	/**
	 * Exclude/Ignore Referring website
	 *
	 * Excludes a source as a referring site. Use this option when you want to
	 * set certain referring links as direct traffic, rather than as referring
	 * sites. For example, your company might own another domain that you want
	 * to track as direct traffic so that it does not show up on the "Referring
	 * Sites" reports. Requests from excluded referrals are still counted in your
	 * overall page view count.
	 *
	 * For example:
	 * <code>
	 *    M_ServiceGoogleAnalytics
	 *       ::getInstance()
	 *       ->addIgnoredReferringWebsite('www.sister-website.com');
	 * </code>
	 *
	 * @access public
	 * @param string $ignoredReferral
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function addIgnoredReferringWebsite($ignoredReferral) {
		$this->_addTrackingInstruction('_addIgnoredRef', array((string) $ignoredReferral));
		return $this;
	}

	/**
	 * Add organic traffic
	 *
	 * Adds a search engine to be included as a potential search engine traffic
	 * source. By default, Google Analytics recognizes a number of common search
	 * engines, but you can add additional search engine sources to the list.
	 *
	 * @access public
	 * @param string $newSearchEngine
	 *		The search engine that is providing with organic search results
	 * @param string $newSearchKeyword
	 *		The keyword that has provided with organic search results in the new
	 *		search engine
	 * @param string $prepend
	 *		If TRUE, prepends the new engine to the beginning of the organic
	 *		source list. If FALSE adds the new engine to the end of the list.
	 *		This parameter's default value is set to false.
	 * @return M_ServiceGoogleAnalytics $service
	 *		Returns itself, for a fluent programming interface
	 */
	public function addOrganic($newSearchEngine, $newSearchKeyword, $prepend = FALSE) {
		// Add the instruction for new organic traffic:
		$this->_addTrackingInstruction('_addOrganic', array(
			// Engine for new organic source.
			(string) $newSearchEngine,
			// Keyword name for new organic source.
			(string) $newSearchKeyword,
			// Prepend?
			(bool) $prepend
		));

		// Return myself
		return $this;
	}

	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Add tracking instruction
	 *
	 * Is used by other methods in this class, such as
	 *
	 * - {@link M_ServiceGoogleAnalytics::addTrackingOfPageView()}
	 * - {@link M_ServiceGoogleAnalytics::addTrackingOfOrder()}
	 * - ...
	 *
	 * in order to add tracking instructions that should be registered in the
	 * statistics of the website.
	 *
	 * @access protected
	 * @param string $name
	 *		The name of the instruction
	 * @param array $args
	 *		The arguments for the instruction (optional)
	 * @param bool $prepend
	 *		Set to TRUE if you want to prepend the instruction to the beginning
	 *		of the queue, or FALSE if you want to append to the end of the queue
	 * @return void
	 */
	protected function _addTrackingInstruction($name, array $args = array(), $prepend = false) {
		// Merge the instruction name with the arguments, into one array
		array_unshift($args, $name);

		// If prepending:
		if($prepend) {
			// Then, add the instruction to the beginning of the queue:
			array_unshift($this->_trackingInstructions, $args);
		}
		// If appending:
		else {
			// Then, add to the end of the queue:
			$this->_trackingInstructions[] = $args;
		}
	}

	/**
	 * Get javascript code for a given instruction
	 *
	 * Is used by {@link M_ServiceGoogleAnalytics::getJavascriptCode()} to render
	 * the javascript code for a given instruction.
	 *
	 * @access protected
	 * @param array $instruction
	 * @return string
	 */
	protected function _getJavascriptCodeForInstruction(array $instruction) {
		// New-line character to be used in the javascript:
		$nl  = "\n";
		
		// Add the line that pushes a new instruction:
		$js  = '_gaq.push([';

		// We initiate a counter, which we will use while looping the
		// current instruction's arguments:
		$i = 0;
		$n = count($instruction);

		// For each of the parameters in the current instruction:
		foreach($instruction as $arg) {
			// If the current argument is a boolean
			if(is_bool($arg)) {
				// Then, we add the instruction argument without any string
				// delimiters, and we make sure that we output either 'true'
				// or 'false':
				$js .= $arg ? 'true' : 'false';
			}
			// If the current argument is a number:
			elseif(is_numeric($arg)) {
				// Then, again, we add the instruction argument without any
				// string delimiters:
				$js .= $arg;
			}
			// If the current argument is a string:
			else {
				// Make sure that we do not terminate the string literal in js
				$arg = str_replace('\'', '\\\'', (string) $arg);

				// We add the instruction argument:
				$js .= '\''. $arg .'\'';
			}

			// If this is still not the last argument, then we do not add a
			// comma separator in the javascript line:
			if(++ $i < $n) {
				$js .= ', ';
			}
		}

		// Finish the line that adds (pushes) the instruction to the queue
		// of tracking instructions
		$js .= ']); ' . $nl;

		// Return the javascript code
		return $js;
	}
}