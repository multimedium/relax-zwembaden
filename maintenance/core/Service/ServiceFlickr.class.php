<?php
/**
 * M_ServiceFlickr
 * 
 * ... contains functions to communicate with the Flickr API
 * 
 * @package Core
 */
class M_ServiceFlickr extends M_ServiceWithApiKey {
	/**
	 * Get Flickr User ID
	 * 
	 * Will provide with the Flickr User ID of a given username.
	 * 
	 * @access public
	 * @param string $username
	 * 		The username of the user
	 * @return string
	 */
	public function getUserId($username) {
		// We try to fetch the user id:
		$rs = $this->_call('flickr.people.findByUsername', array(
			'username' => (string) $username
		));
		
		// If the request failed
		$isOk = ($rs && isset($rs['user']['id']));
		if(! $isOk) {
			// Return FAILURE
			return FALSE;
		}
		// Return the User ID
		return $rs['user']['id'];
	}
	
	/**
	 * Get User Info
	 * 
	 * Will provide with more information about a Flickr User.
	 * 
	 * @access public
	 * @param string $userId
	 * 		The User ID. For more info, read {@link M_ServiceFlickr::getUserId()}
	 * @return M_ServiceFlickrUser
	 */
	public function getUser($userId) {
		// We try to fetch the user id:
		$rs = $this->_call('flickr.people.getInfo', array(
			'user_id' => (string) $userId
		));
		
		// If the request failed
		$isOk = ($rs && isset($rs['person']) && is_array($rs['person']));
		if(! $isOk) {
			// Return FAILURE
			return FALSE;
		}
		
		// Return the user:
		return $this->_getUserFromRecord($userId, $rs['person']);
	}
	
	/**
	 * Get photos from Flickr User
	 * 
	 * Will provide with a the collection of photos from a given user at Flickr.
	 * 
	 * @access public
	 * @param string $userId
	 * 		The User ID. For more info, read {@link M_ServiceFlickr::getUserId()}
	 * @param integer $numberOfItemsPerPage
	 * 		The number of pictures to be fetched per page
	 * @param integer $pageNumber
	 * 		The page to be requested
	 * @return ArrayIterator $photos
	 * 		A collection of {@link M_ServiceFlickrPhoto} objects
	 */
	public function getPhotosOfUserId($userId, $numberOfItemsPerPage = 100, $pageNumber = 1) {
		// We try to fetch info about the photos:
		$rs = $this->_call('flickr.people.getPublicPhotos', array(
			'user_id'  => (string) $userId,
			'per_page' => (int) $numberOfItemsPerPage,
			'page'     => (int) $pageNumber,
			// We fetch all extras, so we can limit number of requests
			'extras'   => 'license, date_upload, date_taken, owner_name, ' . 
			              'icon_server, original_format, last_update, geo, ' . 
			              'tags, machine_tags, o_dims, views, media, ' . 
			              'path_alias, url_sq, url_t, url_s, url_m, url_o'
		));
		
		// If the request failed
		$isOk = (
			$rs && isset($rs['photos']) && is_array($rs['photos']) && 
			isset($rs['photos']['photo']) && is_array($rs['photos']['photo'])
		);
		
		if(! $isOk) {
			// Return FAILURE
			return FALSE;
		}
		
		// Output variable
		$out = array();
		
		// For each of the photos in the result set:
		foreach($rs['photos']['photo'] as $photo) {
			// Add to output
			$out[] = $this->_getPhotoFromRecord($photo, $userId);
		}
		
		// Return the collection
		return new ArrayIterator($out);
	}
	
	/**
	 * Get photosets
	 * 
	 * Returns the photosets belonging to the specified user. The user is being
	 * represented by a User ID.
	 *
	 * @see M_ServiceFlickr::getUserId()
	 * @access public
	 * @param string $userId
	 * 		The Flickr User ID; {@link M_ServiceFlickr::getUserId()}
	 * @param string $search
	 * 		If you want to search the photosets, you can specify the search 
	 * 		keyword here
	 * @return ArrayIterator $photosets
	 * 		A collection of {@link M_ServiceFlickrPhotoset} instances
	 */
	public function getPhotosetsOfUserId($userId, $search = NULL) {
		// We try to fetch info about the photosets:
		$rs = $this->_call('flickr.photosets.getList', array(
			'user_id' => $userId
		));
		
		// If the request failed
		$isOk = (
			$rs && isset($rs['photosets']) && is_array($rs['photosets']) && 
			isset($rs['photosets']['photoset']) && is_array($rs['photosets']['photoset'])
		);
		
		if(! $isOk) {
			// Return FAILURE
			return FALSE;
		}
		
		// Output variable
		$out = array();
		
		// For each of the photosets in the result set:
		foreach($rs['photosets']['photoset'] as $set) {
			// Get the photoset
			$photoset = $this->_getPhotosetFromRecord($set);
			
			// If no search keyword has been provided, we add the photoset to the
			// output. If a search has been specified, we check if it matches first
			if(! $search || ($search && stripos($photoset->getTitle(), $search) !== FALSE)) {
				// Add to output
				$out[] = $photoset;
			}
		}
		
		// Return the collection
		return new ArrayIterator($out);
	}
	
	/**
	 * Get photoset by Photoset ID
	 * 
	 * Gets information about a photoset with a given ID.
	 *
	 * @see M_ServiceFlickr::getPhotosetsOfUserId()
	 * @access public
	 * @param string $photosetId
	 * @return M_ServiceFlickrPhotoset $photoset
	 */
	public function getPhotoset($photosetId) {
		// We try to fetch info about the photoset:
		$rs = $this->_call('flickr.photosets.getInfo', array(
			'photoset_id' => $photosetId
		));
		
		// If the request failed
		$isOk = ($rs && isset($rs['photoset']) && is_array($rs['photoset']));
		
		if(! $isOk) {
			// Return FAILURE
			return FALSE;
		}
		
		// Return the photoset:
		return $this->_getPhotosetFromRecord($rs['photoset']);
	}
	
	/**
	 * Get photos in photoset
	 * 
	 * Get the list of photos in a set; {@link M_ServiceFlickrPhotoset}. The photoset
	 * is being represented by its ID.
	 *
	 * @access M_ServiceFlickr::getPhotosetsOfUserId()
	 * @param string $photosetId
	 * @param integer $numberOfItemsPerPage
	 * 		The number of pictures to be fetched per page
	 * @param integer $pageNumber
	 * 		The page to be requested
	 * @return ArrayIterator $photos
	 * 		A collection of {@link M_ServiceFlickrPhoto} objects
	 */
	public function getPhotosOfPhotosetId($photosetId, $numberOfItemsPerPage = 100, $pageNumber = 1) {
		// We try to fetch info about the photos:
		$rs = $this->_call('flickr.photosets.getPhotos', array(
			'photoset_id' => (string) $photosetId,
			'per_page'    => (int) $numberOfItemsPerPage,
			'page'        => (int) $pageNumber,
			// We fetch all extras, so we can limit number of requests
			'extras'      => 'license, date_upload, date_taken, owner_name, ' . 
			                 'icon_server, original_format, last_update, geo, ' . 
			                 'tags, machine_tags, o_dims, views, media, ' . 
			                 'path_alias, url_sq, url_t, url_s, url_m, url_o'
		));
		
		// If the request failed
		$isOk = (
			$rs && isset($rs['photoset']) && is_array($rs['photoset']) && 
			isset($rs['photoset']['photo']) && is_array($rs['photoset']['photo'])
		);
		
		if(! $isOk) {
			// Return FAILURE
			return FALSE;
		}
		
		// Output variable
		$out = array();
		
		// For each of the photos in the result set:
		foreach($rs['photoset']['photo'] as $photo) {
			// Add to output
			$out[] = $this->_getPhotoFromRecord($photo);
		}
		
		// Return the collection
		return new ArrayIterator($out);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get {@link M_ServiceFlickrUser} from array record
	 * 
	 * @access protected
	 * @param string $userId
	 * 		The Flickr user id
	 * @param array $record
	 * 		The user record
	 * @return M_ServiceFlickrUser
	 */
	protected function _getUserFromRecord($userId, $record) {
		$user = new M_ServiceFlickrUser($this, $userId, $record['username']);
		$user->setIsAdmin((int) $record['isadmin'] == 1);
		$user->setIsPro((int) $record['ispro'] == 1);
		$user->setLocation($record['location']['_content']);
		$user->setMobileUri(new M_Uri($record['mobileurl']['_content']));
		$user->setProfileUri(new M_Uri($record['profileurl']['_content']));
		$user->setPhotosUri(new M_Uri($record['photosurl']['_content']));
		$user->setNumberOfPhotos($record['photos']['count']);
		$user->setFirstDateOfPhoto(new M_Date((int) $record['photos']['firstdate']['_content']));
		$user->setPathAlias($record['path_alias']);
		$user->setRealName($record['realname']['_content']);
		return $user;
	}
	
	/**
	 * Get {@link M_ServiceFlickrPhotoset} from array record
	 * 
	 * @access protected
	 * @param array $record
	 * 		The photoset record
	 * @return M_ServiceFlickrPhotoset
	 */
	protected function _getPhotosetFromRecord($record) {
		$set = new M_ServiceFlickrPhotoset($this, $record['id']);
		$set->setTitle($record['title']['_content']);
		$set->setDescription($record['description']['_content']);
		$set->setNumberOfPhotos($record['photos']);
		if(isset($record['owner'])) {
			$set->setUserId($record['owner']);
		}
		return $set;
	}
	
	/**
	 * Get {@link M_ServiceFlickrPhoto} from array record
	 * 
	 * @access protected
	 * @param array $record
	 * 		The photo record
	 * @param string $userId
	 * 		The Flickr user id
	 * @return M_ServiceFlickrPhoto
	 */
	protected function _getPhotoFromRecord($record, $userId = NULL) {
		$p = new M_ServiceFlickrPhoto($this, $record['id']);
		$p->setDateTaken(new M_Date($record['datetaken']));
		$p->setDateUploaded(new M_Date((int) $record['dateupload']));
		$p->setHeightOfImage($record['height_m']);
		$p->setHeightOfLargeImage($record['height_o']);
		$p->setHeightOfMediumImage($record['height_s']);
		$p->setHeightOfThumbnail($record['height_t']);
		$p->setHeightOfThumbnailSquare($record['height_sq']);
		$p->setIsPublic((int) $record['ispublic'] == 1);
		$p->setLatitude($record['latitude']);
		$p->setLongitude($record['longitude']);
		$p->setMediaStatus($record['media_status']);
		$p->setMediaType($record['media']);
		$p->setNumberOfViews($record['views']);
		$p->setTags($record['tags']);
		$p->setTitle($record['title']);
		$p->setUriOfImage(new M_Uri($record['url_m']));
		$p->setUriOfLargeImage(new M_Uri($record['url_o']));
		$p->setUriOfMediumImage(new M_Uri($record['url_s']));
		$p->setUriOfThumbnail(new M_Uri($record['url_t']));
		$p->setUriOfThumbnailSquare(new M_Uri($record['url_sq']));
		$p->setWidthOfImage($record['width_m']);
		$p->setWidthOfLargeImage($record['width_o']);
		$p->setWidthOfMediumImage($record['width_s']);
		$p->setWidthOfThumbnail($record['width_t']);
		$p->setWidthOfThumbnailSquare($record['width_sq']);
		if($userId) {
			$p->setUserId($userId);
		}
		return $p;
	}
	
	/**
	 * Call a Flickr method
	 * 
	 * This method will call a flickr method, and return the result that is
	 * returned by the service.
	 * 
	 * @access protected
	 * @param string $flickrMethod
	 * 		The method name
	 * @param string $flickrMethodArguments
	 * 		The method's set of arguments
	 * @return array $response
	 * 		The Flickr response. If the request failed, this method will return 
	 * 		(boolean) FALSE instead!
	 */
	protected function _call($flickrMethod, array $flickrMethodArguments = array()) {
		// The API interface to Flickr is very simple. In order to create a new
		// request, you simply pay a visit to the API URL, and provide the API
		// key, the method name and arguments as request variables. First of all,
		// we compose the URI to communicate with:
		$uri = new M_Uri('http://api.flickr.com/services/rest/');
		
		// Provide the URI with the arguments:
		$uri->setQueryVariables(array_merge($flickrMethodArguments, array(
			'api_key' => $this->getApiKey(),
			'method'  => $flickrMethod,
			'format'  => 'php_serial'
		)));
		
		// Get the final URI
		$uriString = $uri->toString();
		
		// Before sending a request to this URI, we check if cache memory has 
		// been specified:
		$cache = $this->getCacheMemory();
		if($cache) {
			// If so, prepare the Cache ID:
			$cacheId = 'flickr.' . md5($uriString);
			
			// Check if we can read the requested data from cache:
			$temp = $cache->read($cacheId);
			if($temp !== NULL) {
				// If so, we return the data from cache
				return $temp;
			}
		}
		
		// If we are still here, we did not get any data from cache. So, we try
		// to get a response from the service (Flickr):
		$rawResponse = file_get_contents($uriString);
		
		// The response we get is a serialized array. In order to interpret this
		// response, we unserialize the string:
		$response = @unserialize($rawResponse);
		
		// If we could not unserialize the response:
		if(! $response) {
			// Set error message
			$this->setErrorMessage(sprintf(
				'Fatal error; Unexpected response from Flickr: %s - [%s]',
				$rawResponse,
				$uri->toString()
			));
			
			// return FALSE (failure)
			return FALSE;
		}
		
		// If Flickr reports an error message
		$isOk = (isset($response['stat']) && $response['stat'] == 'ok');
		if(! $isOk) {
			// Set error message and -code (if any)
			$this->setErrorMessage($response['message'] ? (string) $response['message'] : '');
			$this->setErrorCode(   $response['code']    ? (string) $response['code']    : '');
			
			// return FALSE (failure)
			return FALSE;
		}
		
		// If cache memory is available:
		if($cache) {
			// Write to cache:
			$cache->write($cacheId, $response);
		}
		
		// Return the response:
		return $response;
	}
}