<?php
/**
 * M_ServiceYouTubeVideoView
 *
 * Contains functions to create a video view
 *
 * @author Tim Segers
 * @package Core
 */
class M_ServiceYouTubeVideoView extends M_ViewCoreHtml {

	/* -- CONSTANTS -- */

	/**
	 * Size - Small
	 *
	 * This constant can be used to describe a predetermined video size.
	 * Typically used in {@link M_ServiceYouTubeVideoView::setSize()}
	 *
	 * @see M_ServiceYouTubeVideoView::setSize()
	 * @var string
	 */
	const SIZE_SMALL = 'small';

	/**
	 * Size - Medium
	 *
	 * This constant can be used to describe a predetermined video size.
	 * Typically used in {@link M_ServiceYouTubeVideoView::setSize()}
	 *
	 * @see M_ServiceYouTubeVideoView::setSize()
	 * @var string
	 */
	const SIZE_MEDIUM = 'medium';

	/**
	 * Size - LARGE
	 *
	 * This constant can be used to describe a predetermined video size.
	 * Typically used in {@link M_ServiceYouTubeVideoView::setSize()}
	 *
	 * @see M_ServiceYouTubeVideoView::setSize()
	 * @var string
	 */
	const SIZE_LARGE = 'large';

	/**
	 * Size - MAX
	 *
	 * This constant can be used to describe a predetermined video size.
	 * Typically used in {@link M_ServiceYouTubeVideoView::setSize()}
	 *
	 * @see M_ServiceYouTubeVideoView::setSize()
	 * @var string
	 */
	const SIZE_MAX = 'max';

	/* -- PROPERTIES -- */

	/**
	 * Video
	 *
	 * Stores the video that is to be rendered for display. This contains
	 * an M_ServiceYouTubeVideo object.
	 *
	 * @access private
	 * @var M_ServiceYouTubeVideo
	 */
	private $_video;

	/**
	 * Contains the desired video height
	 *
	 * @access private
	 * @var int
	 */
	private $_height;

	/**
	 * Contains the desired video width
	 *
	 * @access private
	 * @var int
	 */
	private $_width;

	/**
	 * Contains the fullscreen integer
	 *
	 * @acces private
	 * @var int
	 */
	private $_fullscreen;

	/**
	 * Contains the hd integer
	 *
	 * @access private
	 * @var int
	 */
	private $_hd;

	/**
	 * Contains the border integer
	 *
	 * @var int
	 */
	private $_border;

	/**
	 * Contains the related videos integer
	 *
	 * @var int
	 */
	private $_relatedVideos;

	/* -- SETTERS/GETTERS -- */

	/**
	 * Get Height
	 *
	 * This function will return the currently
	 * set height of the M_ServiceYouTubeVideoView
	 *
	 * @acces public
	 * @return int
	 */
	public function getHeight() {
		return $this->_height;
	}

	/**
	 * Set Height
	 *
	 * This function can be used to set the height of the video view in pixels.
	 * This is mostly the case when we don't make use of the predetermined
	 * video sizes in {@link M_ServiceYouTubeVideoView::setSize()}
	 *
	 * @acces public
	 * @param int $height
	 * @return M_ServiceYouTubeVideoView
	 */
	public function setHeight($height) {
		$this->_height = (int)$height;
		return $this;
	}

	/**
	 * Get Width
	 *
	 * This function will return the currently
	 * set width of the M_ServiceYouTubeVideoView
	 *
	 * @acces public
	 * @return int
	 */
	public function getWidth() {
		return $this->_width;
	}

	/**
	 * Set Width
	 *
	 * This function can be used to set the width of the video view in pixels.
	 * This is mostly the case when we don't make use of the predetermined
	 * video sizes in {@link M_ServiceYouTubeVideoView::setSize()}
	 *
	 * @acces public
	 * @param int $width
	 * @return M_ServiceYouTubeVideoView
	 */
	public function setWidth($width) {
		$this->_width = (int)$width;
		return $this;
	}

	/**
	 * Get Fullscreen
	 *
	 * Returns the current value of the fullscreen integer. This integer is
	 * used to prohibit or allow the user th put the video in fullscreen mode
	 * through the standard fullscreen button in the bottom right corner of
	 * the video view.
	 *
	 * @access public
	 * @return bool
	 */
	public function getAllowFullscreen() {
		return $this->_fullscreen;
	}

	/**
	 * Set Fullscreen
	 *
	 * This integer (which acts as a boolean - 0 or 1) determines wheter
	 * teh fullscreen option of a video is available or not. Typically this
	 * means showing the small fullscreen option button at the bottom rigbht
	 * corner of the video. When set to 0, this integer will make the video
	 * display without fullscreen option.
	 *
	 * If not set, this function will follow the default setting from the
	 * video itself.
	 *
	 * @access public
	 * @param int $fullscreen
	 * @return M_ServiceYouTubeVideoView
	 */
	public function setAllowFullscreen($fullscreen) {
		$this->_fullscreen = (int)$fullscreen;
		return $this;
	}

	/**
	 * Get Hd
	 *
	 * This function will return the current value of the HD boolean. This
	 * boolean allows or prohibits the user from putting the video in another
	 * HD modus than the standard one through the hd dropdown menu in the bottom
	 * right corner of the video view. Typicall, this dropdown has values like
	 * 480p or 1080p.
	 *
	 * @access public
	 * @return int
	 */
	public function getHd() {
		return $this->_hd;
	}
	
	/**
	 * Set Hd
	 *
	 * Setting this integer will allow or prohibit the user from changing the
	 * HD settings from the video. Typically this exists of showing or hiding
	 * the HD options dropdown list to the bottom right corner of the video
	 * view. This menu typically holds values like 480p or 1080p.
	 *
	 * If not set, this function will follow the default setting from the
	 * video itself.
	 *
	 * @access public
	 * @param int $hd
	 * @return M_ServiceYouTubeVideoView
	 */
	public function setHd($hd) {
		$this->_hd = (int)$hd;
		return $this;
	}

	/**
	 * Get Border
	 *
	 * This function will return the currently set value for the border int.
	 * This integer defines wheter or not to show the default YouTube border
	 * around the video. In practise this consists of a 2px thick black or dark
	 * grey border around the video.
	 *
	 * @access public
	 * @return int
	 */
	public function getShowBorder() {
		return $this->_border;
	}

	/**
	 * Set Border
	 *
	 * This function shows or hides the default border YouTube puts around
	 * its videos. By default most videos don't have a border anymore, but
	 * in case they do, this function can be used to disable it.
	 *
	 * @access public
	 * @param int $border
	 * @return M_ServiceYouTubeVideoView
	 */
	public function setShowBorder($border) {
		$this->_border = (int)$border;
		return $this;
	}

	/**
	 * Get ShowRelatedVideos
	 *
	 * This function will return the currently set value for the
	 * related videos, which enables or disables showing the related videos
	 * in the video embed menu, which can be access by default through the
	 * most bottom right corner button in the video view.
	 *
	 * @access public
	 * @return int
	 */
	public function getShowRelatedVideos() {
		return $this->_relatedVideos;
	}

	/**
	 * Set ShowRelatedVideos
	 *
	 * This function allows you to enable or disable the showing of
	 * related videos in the video embed screen, which can be accessed by default
	 * through the button in the bottom right corner of the video view.
	 *
	 * Setting this flag to disabled will simply hide all related videos
	 * for the user.
	 *
	 * @access public
	 * @param bool $flag
	 * @return M_ServiceYouTubeVideoView
	 */
	public function setShowRelatedVideos($flag) {
		$this->_relatedVideos = (bool) $flag;
		return $this;
	}

	/**
	 * Set video
	 * 
	 * Will set the video that is to be rendered for display
	 *
	 * @access public
	 * @param M_ServiceYouTubeVideo $video
	 * @return M_ServiceYouTubeVideoView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setVideo(M_ServiceYouTubeVideo $video) {
		$this->_video = $video;
		return $this;
	}

	/**
	 * Set size of video
	 *
	 * This setter can be used to set the size of the video to a
	 * predetermined size. Available sizes are:
	 *
	 * - {@link M_ServiceYouTubeVideoView::SIZE_SMALL}
	 * - {@link M_ServiceYouTubeVideoView::SIZE_MEDIUM}
	 * - {@link M_ServiceYouTubeVideoView::SIZE_LARGE}
	 * - {@link M_ServiceYouTubeVideoView::SIZE_MAX}
	 *
	 * - By default, these sizes are:
	 *		SIZE_SMALL: 560x340
	 *		SIZE_MEDIUM: 640x385
	 *		SIZE_LARGE: 853x505
	 *		SIZE_MAX: 1280x745
	 *
	 * If no constant is called, the video will by default output as 640x385.
	 *
	 * @access public
	 * @param string $size
	 */
	public function setSize($size) {
		switch($size) {
			
			case self::SIZE_SMALL:
				$this->setWidth(560);
				$this->setHeight(340);
				break;

			case self::SIZE_MEDIUM:
				$this->setWidth(640);
				$this->setHeight(385);
				break;

			case self::SIZE_LARGE:
				$this->setWidth(853);
				$this->setHeight(505);
				break;

			case self::SIZE_MAX:
				$this->setWidth(1280);
				$this->setHeight(745);
				break;

			default:
				$this->setWidth(640);
				$this->setHeight(385);
				break;
		}
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get Video Specific Uri
	 *
	 * Will create a direct uri to a video based on set properties.
	 *
	 * @access public
	 * @return M_Uri $uri
	 */
	public function getVideoViewUri() {
		$uri = new M_Uri('http://www.youtube.com/v/' . $this->_video->getVideoId());
		$uri->setQueryVariable('hl', 'nl_NL');
		$uri->setQueryVariable('fs', $this->getAllowFullscreen());
		$uri->setQueryVariable('border', $this->getShowBorder());
		$uri->setQueryVariable('hd', $this->getHd());
		$uri->setQueryVariable('rel', $this->getShowRelatedVideos());
		return $uri;
	}

	/**
	 * Get default HTML rendering code
	 *
	 * This function will crate and return the html code needed to show a youtube
	 * video on a page, thus creating the object and embed tags within html.
	 *
	 * @access protected
	 * @return string $html
	 */
	protected function getHtml() {
		$uri = $this->getVideoViewUri();

		$html  = '<object class="ytVideoObject" width="'. $this->getWidth() .'" height="'. $this->getHeight() .'">';
		$html .= '<param name="movie" value="'. $uri .'"></param>';
		$html .= '<param name="allowFullScreen" value="true"></param>';
		$html .= '<param name="allowscriptaccess" value="always"></param>';
		$html .= '<embed src="'. $uri .'" type="application/x-shockwave-flash"';
		$html .= 'allowscriptaccess="always"';
		$html .= 'allowfullscreen="true"';
		$html .= 'width="'. $this->getWidth() .'"';
		$html .= 'height="'. $this->getHeight() .'"';
		$html .= '</embed></object>';

		return $html;
	}

	/**
	 * Get Template
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithTemplateBasePath('core/YouTubeVideo.tpl');
	}
}