<?php
/**
 * M_ServiceTwitterUser
 *
 * ... contains the data of a twitter user
 */
class M_ServiceTwitterUser {

	/* -- PROPERTIES -- */

	private $_userId;
	private $_name;
	private $_screenName;
	private $_location;
	private $_description;
	private $_profileImageUrl;
	private $_url;
	private $_protected;
	private $_bgColor;
	private $_textColor;
	private $_linkColor;
	private $_sidebarBgColor;
	private $_sidebarBorderColor;
	private $_followingCount;
	private $_followersCount;
	private $_favouritesCount;
	private $_utcOffset;
	private $_bgImage;
	private $_bgTile;
	private $_statusesCount;
	
	private $_status;
	
	
	/* -- GETTERS -- */
	
	/**
	 * @return the $_userId
	 */
	public function getUserId() {
		return $this->_userId;
	}

	/**
	 * @return the $_name
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * @return the $_screenName
	 */
	public function getScreenName() {
		return $this->_screenName;
	}

	/**
	 * @return the $_location
	 */
	public function getLocation() {
		return $this->_location;
	}

	/**
	 * @return the $_description
	 */
	public function getDescription() {
		return $this->_description;
	}

	/**
	 * @return the $_profileImageUrl
	 */
	public function getProfileImageUrl() {
		return $this->_profileImageUrl;
	}

	/**
	 * @return the $_url
	 */
	public function getUrl() {
		return $this->_url;
	}

	/**
	 * @return the $_protected
	 */
	public function getProtected() {
		return $this->_protected;
	}

	/**
	 * @return the $_bgColor
	 */
	public function getBgColor() {
		return $this->_bgColor;
	}

	/**
	 * @return the $_textColor
	 */
	public function getTextColor() {
		return $this->_textColor;
	}

	/**
	 * @return the $_linkColor
	 */
	public function getLinkColor() {
		return $this->_linkColor;
	}

	/**
	 * @return the $_sidebarBgColor
	 */
	public function getSidebarBgColor() {
		return $this->_sidebarBgColor;
	}

	/**
	 * @return the $_sidebarBorderColor
	 */
	public function getSidebarBorderColor() {
		return $this->_sidebarBorderColor;
	}

	/**
	 * @return the $_followingCount
	 */
	public function getFollowingCount() {
		return $this->_followingCount;
	}

	/**
	 * @return the $_followersCount
	 */
	public function getFollowersCount() {
		return $this->_followersCount;
	}

	/**
	 * @return the $_favouritesCount
	 */
	public function getFavouritesCount() {
		return $this->_favouritesCount;
	}

	/**
	 * @return the $_utcOffset
	 */
	public function getUtcOffset() {
		return $this->_utcOffset;
	}

	/**
	 * @return the $_bgImage
	 */
	public function getBgImage() {
		return $this->_bgImage;
	}

	/**
	 * @return the $_bgTile
	 */
	public function getBgTile() {
		return $this->_bgTile;
	}

	/**
	 * @return the $_statusesCount
	 */
	public function getStatusesCount() {
		return $this->_statusesCount;
	}

	/**
	 * Status
	 * 
	 * @return M_ServiceTwitterStatus $status
	 */
	public function getCurrentStatus() {
		return $this->_status;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * @param $_userId the $_userId to set
	 */
	public function setUserId($_userId) {
		$this->_userId = $_userId;
	}

	/**
	 * @param $_name the $_name to set
	 */
	public function setName($_name) {
		$this->_name = $_name;
	}

	/**
	 * @param $_screenName the $_screenName to set
	 */
	public function setScreenName($_screenName) {
		$this->_screenName = $_screenName;
	}

	/**
	 * @param $_location the $_location to set
	 */
	public function setLocation($_location) {
		$this->_location = $_location;
	}

	/**
	 * @param $_description the $_description to set
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
	}

	/**
	 * @param $_profileImageUrl the $_profileImageUrl to set
	 */
	public function setProfileImageUrl($_profileImageUrl) {
		$this->_profileImageUrl = $_profileImageUrl;
	}

	/**
	 * @param $_url the $_url to set
	 */
	public function setUrl($_url) {
		$this->_url = $_url;
	}

	/**
	 * @param $_protected the $_protected to set
	 */
	public function setProtected($_protected) {
		$this->_protected = $_protected;
	}

	/**
	 * @param $_bgColor the $_bgColor to set
	 */
	public function setBgColor($_bgColor) {
		$this->_bgColor = $_bgColor;
	}

	/**
	 * @param $_textColor the $_textColor to set
	 */
	public function setTextColor($_textColor) {
		$this->_textColor = $_textColor;
	}

	/**
	 * @param $_linkColor the $_linkColor to set
	 */
	public function setLinkColor($_linkColor) {
		$this->_linkColor = $_linkColor;
	}

	/**
	 * @param $_sidebarBgColor the $_sidebarBgColor to set
	 */
	public function setSidebarBgColor($_sidebarBgColor) {
		$this->_sidebarBgColor = $_sidebarBgColor;
	}

	/**
	 * @param $_sidebarBorderColor the $_sidebarBorderColor to set
	 */
	public function setSidebarBorderColor($_sidebarBorderColor) {
		$this->_sidebarBorderColor = $_sidebarBorderColor;
	}

	/**
	 * @param $_followingCount the $_followingCount to set
	 */
	public function setFollowingCount($_followingCount) {
		$this->_followingCount = $_followingCount;
	}

	/**
	 * @param $_followersCount the $_followersCount to set
	 */
	public function setFollowersCount($_followersCount) {
		$this->_followersCount = $_followersCount;
	}

	/**
	 * @param $_favouritesCount the $_favouritesCount to set
	 */
	public function setFavouritesCount($_favouritesCount) {
		$this->_favouritesCount = $_favouritesCount;
	}

	/**
	 * @param $_utcOffset the $_utcOffset to set
	 */
	public function setUtcOffset($_utcOffset) {
		$this->_utcOffset = $_utcOffset;
	}

	/**
	 * @param $_bgImage the $_bgImage to set
	 */
	public function setBgImage($_bgImage) {
		$this->_bgImage = $_bgImage;
	}

	/**
	 * @param $_bgTile the $_bgTile to set
	 */
	public function setBgTile($_bgTile) {
		$this->_bgTile = $_bgTile;
	}

	/**
	 * @param $_statusesCount the $_statusesCount to set
	 */
	public function setStatusesCount($_statusesCount) {
		$this->_statusesCount = $_statusesCount;
	}

	/**
	 * Set twitter status
	 * 
	 * @param M_ServiceTwitterStatus $status
	 */
	public function setCurrentStatus(M_ServiceTwitterStatus $status) {
		$this->_status = $status;
	}

}