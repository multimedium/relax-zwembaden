<?php
/**
 * M_ServiceYouTube
 * 
 * ... contains functions to communicate with YouTube
 *
 * @author Tim Segers
 * @see http://code.google.com/intl/nl/apis/youtube/getting_started.html
 * @package Core
 */

class M_ServiceYouTube extends M_Service {

	/**
	 * Get the user's metadata
	 *
	 * This function will fetch the metadata from a given YouTube user by
	 * their YouTube username. This function will return an M_ServiceYouTubeUser,
	 * setting the following:
	 * - username
	 * - authorname
	 * - channel
	 * - last time the channel was updated
	 * - the number of videos
	 *
	 * @access public
	 * @param string $username
	 * @return M_ServiceYouTubeUser
	 *		The user's metadata
	 */
	public function getUser($username) {
		// setup connection with feed
		$xml = M_ServiceYouTubeHelper::connectToXmlFeed($username);

		return M_ServiceYouTubeHelper::getUserInfoFromXml($xml, $username);
	}

	/**
	 * Get videos from a given user
	 *
	 * This function will return an M_ArrayIterator filled with
	 * M_ServiceYouTubeVideo objects, including all videos from the specified
	 * user. As of yet this function expects a string as username directly,
	 * rather then the entire M_ServiceYouTubeUser object. To get all videos
	 * directly from an M_ServiceYouTubeUser, simply call the getVideos()
	 * function on the M_ServiceYouTubeUser object itself.
	 *
	 * @access public
	 * @param string $username
	 * @return M_ArrayIterator $out
	 *		The requested videos of the indicated user
	 */
	public function getVideosByUsername($username) {
		// The output collection;
		$out = array();

		// setup connection with feed
		$xml = M_ServiceYouTubeHelper::connectToXmlFeed($username);
		$entryCounter = 0;

		// For each entry, fetch a video
		foreach($xml->entry as $entry) {
			$out[] = M_ServiceYouTubeHelper::getVideoFromXmlElement($entry, $username, $entryCounter);
			$entryCounter += 1;
		}
		return new M_ArrayIterator($out);
	}

	/**
	 * Get video metadata by a given video Id
	 *
	 * This function will collect the metadata of a certain video, starting
	 * from a given video ID. This video ID is usually something like
	 * L5NjeNUVkYE or 4lQ1Pz_O-j0, e.g. a random looking string unique for
	 * every video on YouTube.
	 *
	 * @access public
	 * @param string $videoId
	 * @return M_ServiceYouTubeVideo
	 */
	public function getVideoByVideoId($videoId) {
		
		// Connect to the XML feed
		$xml = M_ServiceYouTubeHelper::connectToSingleVideoXmlFeed($videoId);

		return M_ServiceYouTubeHelper::getVideoFromXmlElement($xml, '', 0);
	}
}