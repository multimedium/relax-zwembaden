<?php
/**
 * M_Service
 * 
 * @package Core
 */
abstract class M_Service extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Error message
	 * 
	 * This property stores the message about the error that may have been caused 
	 * during communication with the service. This information can be retrieved 
	 * with {@link M_Service::getErrorMessage()}
	 * 
	 * @access private
	 * @var string
	 */
	private $_errorMessage;
	
	/**
	 * Error code
	 * 
	 * This property stores the error code of the error that may have been caused 
	 * during communication with the service. This information can be retrieved 
	 * with {@link M_Service::getErrorCode()}
	 * 
	 * @access private
	 * @var string
	 */
	private $_errorCode;
	
	/**
	 * Cache
	 * 
	 * This property stores the cache object that is in charge of caching the
	 * responses from the service
	 * 
	 * @access private
	 * @var M_CacheMemory
	 */
	private $_cache;
	
	/* -- GETTERS -- */
	
	/**
	 * Get error message
	 * 
	 * Will provide with the message about the error that may have been caused
	 * during communication with the service.
	 * 
	 * @access public
	 * @return string
	 */
	public function getErrorMessage() {
		return $this->_errorMessage;
	}
	
	/**
	 * Get error code
	 * 
	 * Will provide with the code that describes the error that may have been caused
	 * during communication with the service.
	 * 
	 * @access public
	 * @return string
	 */
	public function getErrorCode() {
		return $this->_errorCode;
	}
	
	/**
	 * Get cache object
	 * 
	 * Will provide with the cache object that is in charge of storing the 
	 * responses from the service.
	 * 
	 * @access public
	 * @return M_CacheMemory
	 */
	public function getCacheMemory() {
		return $this->_cache;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set error message
	 * 
	 * @see M_Service::getErrorMessage()
	 * @access public
	 * @param string $errorMessage
	 * @return void
	 */
	public function setErrorMessage($errorMessage) {
		$this->_errorMessage = (string) $errorMessage;
	}
	
	/**
	 * Set error code
	 * 
	 * @see M_Service::getErrorCode()
	 * @access public
	 * @param string $errorCode
	 * @return void
	 */
	public function setErrorCode($errorCode) {
		$this->_errorCode = (string) $errorCode;
	}
	
	/**
	 * Set cache memory
	 * 
	 * @see M_Service::getCacheMemory()
	 * @access public
	 * @param M_CacheMemory $cache
	 * @return void
	 */
	public function setCacheMemory(M_CacheMemory $cache) {
		$this->_cache = $cache;
	}
}