<?php
/**
 * M_ServiceGoogleMaps
 * 
 * ... contains functions to communicate with the Google Maps API
 *
 * @link http://code.google.com/intl/nl/apis/maps/documentation/v3/basics.html
 * @package Core
 * @author Ben Brughmans
 */
class M_ServiceGoogleMaps extends M_Service {

	/**
	 * Status G_GEO_SUCCESS
	 */
	const STATUS_CODE_OK = 200;
	
	/**
	 * Holds the url which points to the geocoding service
	 * 
	 * @var string
	 */
	private $_geoServiceUrl = 'http://maps.google.com/maps/geo';

	/**
	 * Get the geo-service URI
	 * 
	 * @return M_Uri
	 */
	private function _getGeoServiceUri() {
		return new M_Uri($this->_geoServiceUrl);
	}

	/**
	 * Get the coördinates of an address
	 *
	 * Will return M_ServiceGoogleMapsLocation holding the latitude and longitude.
	 *
	 * Also the given {@link M_ContactAddress} object will be updated with
	 * the correct latitude and longitude
	 *
	 * @param M_ContactAddress $address
	 * @link http://code.google.com/apis/maps/documentation/reference.html#GGeoStatusCode
	 * @return M_ServiceGoogleMapsLocation
	 * @author Ben Brughmans
	 */
	public function getCoordinatesByAddress( M_ContactAddress $address) {
		$location = $this->getCoordinatesByString($address->toString());

		// update address
		 $address->setLatitude($location->getLatitude())
			->setLongitude($location->getLongitude());

		// return location
		 return $location;
	}

	/**
	 * Get the coördinates of a string
	 *
	 * Will return array holding latitude and longitude values:
	 * array('lat' => 51.24658296002271 , 'lng' => 5.201823749999992);
	 *
	 * @param string $address
	 * @link http://code.google.com/apis/maps/documentation/reference.html#GGeoStatusCode
	 * @return M_ServiceGoogleMapsLocation
	 * @author Ben Brughmans
	 */
	public function getCoordinatesByString($string) {
		$uri = $this->_getGeoServiceUri();

		//set all properties
		$uri->setQueryVariable('q', $string);
		$uri->setQueryVariable('output', 'json');
		$uri->setQueryVariable('oe', 'utf8');
		$uri->setQueryVariable('sensor', 'false');
		
		//get Google Maps Coordinates
		$jsonData = file_get_contents($uri);
		$jsonData = utf8_encode($jsonData);
		$arrayData = M_Helper::jsonDecode($jsonData);

		//get and check status code
		$statusCode = $arrayData->Status->code;

		if($statusCode != self::STATUS_CODE_OK) {
			throw new M_Exception(sprintf(
				'Cannot get coördinates for address "%s", received error with'.
				'status code %s',
				$string,
				$statusCode)
			);
		}

		// return location object
		$location = new M_ServiceGoogleMapsLocation();
		$location->setLatitude($arrayData->Placemark[0]->Point->coordinates[1]);
		$location->setLongitude($arrayData->Placemark[0]->Point->coordinates[0]);

		return $location;
	}
}