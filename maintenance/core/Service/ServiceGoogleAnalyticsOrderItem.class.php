<?php
/**
 * M_ServiceGoogleAnalyticsOrderItem
 *
 * ... Is used in combination with
 *
 * - {@link M_ServiceGoogleAnalyticsOrder}, and
 * - {@link M_ServiceGoogleAnalytics::addTrackingOfOrder()}
 *
 * to track e-commerce orders on a website, via Google Analytics
 *
 * @package Core
 */
class M_ServiceGoogleAnalyticsOrderItem extends M_Object {

	/* -- PROPERTIES -- */

	/**
	 * Order ID
	 *
	 * @access private
	 * @var string
	 */
	private $_orderId;

	/**
	 * Item Code/SKU
	 *
	 * Google refers to this property as the Product SKU (Stock-Keeping Unit). A
	 * stock-keeping unit or SKU is a unique identifier for each distinct product
	 * and service that can be purchased.
	 *
	 * In other words, typically this property is populated with the product code
	 * that is being used on the website.
	 *
	 * @access private
	 * @var string
	 */
	private $_code;

	/**
	 * Item Name/Title
	 *
	 * @access private
	 * @var string
	 */
	private $_name;

	/**
	 * Item Category
	 *
	 * Stores the product category, or product variant
	 *
	 * @access private
	 * @var string
	 */
	private $_category;

	/**
	 * Price per unit
	 *
	 * @access private
	 * @var float
	 */
	private $_pricePerUnit;

	/**
	 * Quantity
	 *
	 * @access private
	 * @var integer
	 */
	private $_quantity;

	/* -- CONSTRUCTOR -- */

	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $orderId
	 * @param string $code
	 * @param string $name
	 * @param float  $pricePerUnit
	 * @param int    $quantity
	 * @param string $category
	 * @return M_ServiceGoogleAnalyticsOrderItem
	 */
	public function __construct($orderId, $code, $name, $pricePerUnit = 0, $quantity = 0, $category = NULL) {
		// Set the Order ID
		$this->setOrderId($orderId);

		// Set the code of the item:
		$this->setCode($code);

		// Set the name/title
		$this->setName($name);

		// Set the price per unit
		$this->setPricePerUnit($pricePerUnit);

		// Set the quantity
		$this->setQuantity($quantity);

		// Set category/variant (if any)
		if($category) {
			$this->setCategory($category);
		}
	}

	/* -- GETTERS -- */

	/**
	 * Get Order ID
	 *
	 * @access public
	 * @return string
	 */
	public function getOrderId() {
		return $this->_orderId;
	}

	/**
	 * Get Item code
	 *
	 * @see M_ServiceGoogleAnalyticsOrderItem::$_code
	 * @access public
	 * @return string
	 */
	public function getCode() {
		return $this->_code;
	}

	/**
	 * Get name/title of item
	 *
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * Get category (or product variant)
	 *
	 * @access public
	 * @return string
	 */
	public function getCategory() {
		return $this->_category;
	}

	/**
	 * Get Price per unit
	 *
	 * @access public
	 * @return float
	 */
	public function getPricePerUnit() {
		return $this->_pricePerUnit;
	}

	/**
	 * Get Quantity
	 *
	 * @access public
	 * @return integer
	 */
	public function getQuantity() {
		return $this->_quantity;
	}

	/* -- SETTERS -- */

	/**
	 * Set Order ID
	 *
	 * @access public
	 * @param string $orderId
	 * @return M_ServiceGoogleAnalyticsOrderItem $item
	 *		Returns itself, for a fluent programming interface
	 */
	public function setOrderId($orderId) {
		$this->_orderId = (string) $orderId;
		return $this;
	}

	/**
	 * Set Item Code/SKU
	 *
	 * @see M_ServiceGoogleAnalyticsOrderItem::$_code
	 * @access public
	 * @param string $code
	 * @return M_ServiceGoogleAnalyticsOrderItem $item
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCode($code) {
		$this->_code = (string) $code;
		return $this;
	}

	/**
	 * Set Item Name/Title
	 *
	 * @access public
	 * @param string $code
	 * @return M_ServiceGoogleAnalyticsOrderItem $item
	 *		Returns itself, for a fluent programming interface
	 */
	public function setName($name) {
		$this->_name = (string) $name;
		return $this;
	}

	/**
	 * Set Item Category (product category, or product variant)
	 *
	 * @access public
	 * @param string $category
	 * @return M_ServiceGoogleAnalyticsOrderItem $item
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCategory($category) {
		$this->_category = (string) $category;
		return $this;
	}

	/**
	 * Set Price per unit
	 *
	 * @access public
	 * @param string $pricePerUnit
	 * @return M_ServiceGoogleAnalyticsOrderItem $item
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPricePerUnit($pricePerUnit) {
		$this->_pricePerUnit = (float) $pricePerUnit;
		return $this;
	}

	/**
	 * Set quantity
	 *
	 * @access public
	 * @param string $quantity
	 * @return M_ServiceGoogleAnalyticsOrderItem $item
	 *		Returns itself, for a fluent programming interface
	 */
	public function setQuantity($quantity) {
		$this->_quantity = (int) $quantity;
		return $this;
	}
}