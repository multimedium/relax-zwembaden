<?php
/**
 * M_ServiceYouTubeUser
 *
 * ... contains functions to interact with a YouTube user
 *
 * @author Tim Segers
 * @package Core
 */
class M_ServiceYouTubeUser extends M_Object {

	/* -- PROPERTIES --*/

	/**
	 * Username
	 *
	 * Contains the username of the youtube user
	 *
	 * @access private
	 * @var string
	 */
	private $_username;

	/**
	 * Authorname
	 *
	 * Contains the author name of this youtube user
	 *
	 * @access private
	 * @var string
	 */
	private $_authorname;

	/**
	 * Channel
	 *
	 * Contains the direct URI to the user's youtube channel
	 *
	 * @access private
	 * @var M_Uri
	 */
	private $_channel;

	/**
	 * Last Updated Channel Date
	 *
	 * Contains the last date the user's channel has been edited
	 *
	 * @access private
	 * @var M_Date
	 */
	private $_lastUpdatedChannel;

	/**
	 * Number Of Videos
	 *
	 * Contains the number of videos this user has uploaded
	 *
	 * @access private
	 * @var int
	 */
	private $_numberOfVideos;

	/* -- GETTERS -- */

	/**
	 * Get the username
	 *
	 * Get the username of this youtube user
	 *
	 * @access public
	 * @return string $username
	 */
	public function getUsername() {
		return $this->_username;
	}

	/**
	 * Get the authorname
	 *
	 * Get the authorname of this youtube user
	 *
	 * @access public
	 * @return string $authorname
	 */
	public function getAuthorname() {
		return $this->_authorname;
	}

	/**
	 * Get the uri to the user's channel
	 *
	 * @access public
	 * @return M_Uri $channel
	 */
	public function getChannel() {
		return $this->_channel;
	}

	/**
	 * Get the last date the user updated their channel
	 * 
	 * @access public
	 * @return M_Date $lastUpdatedChannel
	 */
	public function getLastUpdatedChannel() {
		return $this->_lastUpdatedChannel;
	}

	/**
	 * Get the number of videos the user has uploaded
	 * 
	 * @access public
	 * @return int $numberOfVideos
	 */
	public function getNumberOfVideos() {
		return $this->_numberOfVideos;
	}

	/**
	 * Get videos
	 *
	 * Returns a collection with all the videos this user has uploaded,
	 * containing elements of {@link M_ServiceYouTubeVideo}
	 *
	 * @access public
	 * @return M_ArrayIterator $videos
	 */
	public function getVideos() {
		$service = new M_ServiceYouTube();
		return $service->getVideosByUsername($this->getUsername());
	}

	/* -- SETTERS -- */

	/**
	 * Set the username
	 *
	 * @access public
	 * @param string $username
	 * @return M_ServiceYouTubeUser
	 */
	public function setUsername($username) {
		$this->_username = (string) $username;
		return $this;
	}

	/**
	 * Set the authorname
	 *
	 * @access public
	 * @param string $authorname
	 * @return M_ServiceYouTubeUser
	 */
	public function setAuthorname($authorname) {
		$this->_authorname = (string) $authorname;
		return $this;
	}

	/**
	 * Set the channel uri
	 *
	 * @access public
	 * @param M_Uri $channel
	 * @return M_ServiceYouTubeUser
	 */
	public function setChannel(M_Uri $channel) {
		$this->_channel = $channel;
		return $this;
	}

	/**
	 * Set the last date the channel was updated
	 *
	 * @access public
	 * @param M_Date $lastUpdatedChannel
	 * @return M_ServiceYouTubeUser
	 */
	public function setChannelLastUpdated(M_Date $lastUpdatedChannel) {
		$this->_lastUpdatedChannel = $lastUpdatedChannel;
		return $this;
	}

	/**
	 * Set the number of videos
	 *
	 * @access public
	 * @param int $numberOfVideos
	 * @return M_ServiceYouTubeUser
	 */
	public function setNumberOfVideos($numberOfVideos) {
		$this->_numberOfVideos = (int) $numberOfVideos;
		return $this;
	}
}