<?php

// Twitter class
require_once(M_Loader::getAbsolute('core/_thirdparty/twitter/Twitter.class.php'));

/**
 * M_ServiceTwitter
 *
 * ... contains functions to communicate with a Twitter stream
 *
 *
 * @package Core
 * @author Ben Brughmans
 */

class M_ServiceTwitter {

	// Twitter
	private $_twitter;

	// Key
	private $_key;

	// Secret
	private $_secret;


	/* -- PUBLIC -- */

	/**
	 * Construct
	 *
	 * Create a twitter Client
	 * 
	 * @param string $username
	 * @param string $password
	 */
	public function __construct($key, $secret) {
		// Username & Password
		$this->_key = $key;
		$this->_secret = $secret;
	}

	/**
	 * Get the UserTimeline
	 * 
	 * @param int $limit
	 * @return M_ArrayIterator
	 */
	public function getUserTimeLine($limit = 20) {
		// Ask the timeline from the Twitter-object
		$timeline = $this->_getTwitter()->statusesUserTimeline('multimedium_');
		return $this->_getStatusObjectsFromTimelineArray($timeline, $limit);
	}
	
	/* -- PRIVATE / PROTECTED -- */
	
	/**
	 * Get Twitter
	 * 
	 * @return Twitter
	 */
	protected function _getTwitter() {
		if(is_null($this->_twitter)) {
			try {
				$twitter = new Twitter($this->_key, $this->_secret);

				// get a request token
				$twitter->oAuthRequestToken();

				$this->_twitter = $twitter;
			
			} catch(Exception $e) {}

		}

		return $this->_twitter;
	}

	/**
	 * Get status objects from timeline array
	 * 
	 * @param array $timeline
	 * @return M_ArrayIterator
	 */
	protected function _getStatusObjectsFromTimelineArray($timeline, $limit = 20) {
		$result = array();

		// Create a arrayiterator of the statuses
		foreach($timeline as $status) {
			$result[] = $this->_arrayToServiceTwitterStatus($status);
		}

		// Slice the limit
		$result = array_slice($result, 0, $limit);

		// Return the iterator
		return new M_ArrayIterator($result);
	}

	/**
	 * Array To Object M_ServiceTwitterStatus
	 * 
	 * @param array $status
	 * @return M_ServiceTwitterStatus
	 */
	protected function _arrayToServiceTwitterStatus(array $status) {
		// Create status object
		$serviceTwitterStatus = new M_ServiceTwitterStatus();

		// Set the variables
		$serviceTwitterStatus->setId($status['id']);
		$serviceTwitterStatus->setText($status['text']);
		$serviceTwitterStatus->setCreatedAt($status['created_at']);
		$serviceTwitterStatus->setSource($status['source']);
		$serviceTwitterStatus->setUser($this->_arrayToServiceTwitterUser($status['user']));

		// Return the object
		return $serviceTwitterStatus;
	}

	/**
	 * Array To Object M_ServiceTwitterUser
	 * 
	 * @param array $user
	 * @param bool $extended
	 * @return M_ServiceTwitterUser
	 */
	protected function _arrayToServiceTwitterUser(array $user, $extended = false) {

		// Create object
		$serviceTwitterUser = new M_ServiceTwitterUser();

		// Set the basic properties
		$serviceTwitterUser->setName($user['name']);
		$serviceTwitterUser->setScreenName($user['screen_name']);
		$serviceTwitterUser->setDescription($user['description']);
		$serviceTwitterUser->setLocation($user['location']);
		$serviceTwitterUser->setUrl($user['url']);
		$serviceTwitterUser->setProtected($user['protected']);
		$serviceTwitterUser->setFollowersCount($user['followers_count']);
		$serviceTwitterUser->setProfileImageUrl($user['profile_image_url']);

		// If extended
		if($extended)
		{
			// Set extra properties
			if(isset($user['profile_background_color'])) $serviceTwitterUser->setBgColor(utf8_decode((string) $user['profile_background_color']));
			if(isset($user['profile_text_color'])) $serviceTwitterUser->setTextColor(utf8_decode((string) $user['profile_text_color']));
			if(isset($user['profile_link_color'])) $serviceTwitterUser->setLinkColor(utf8_decode((string) $user['profile_link_color']));
			if(isset($user['profile_sidebar_fill_color'])) $serviceTwitterUser->setSidebarBgColor(utf8_decode((string) $user['profile_sidebar_fill_color']));
			if(isset($user['profile_sidebar_border_color'])) $serviceTwitterUser->setSidebarBorderColor(utf8_decode((string) $user['profile_sidebar_border_color']));
			if(isset($user['statuses_count'])) $serviceTwitterUser->setStatusesCount((int) $user['statuses_count']);
			if(isset($user['favourites_count'])) $serviceTwitterUser->setFavouritesCount((int) $user['favourites_count']);
			if(isset($user['utc_offset'])) $serviceTwitterUser->setUtcOffset((int) $user['utc_offset']);
		}

		// Return the object
		return $serviceTwitterUser;
	}



}
?>