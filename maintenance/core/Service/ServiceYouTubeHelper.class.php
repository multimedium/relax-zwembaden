<?php
/**
 * M_ServiceYouTubeHelper
 *
 * @author Tim Segers
 * @package Core
 */
class M_ServiceYouTubeHelper extends M_Object {

	/**
	 * Connect To XML Feed
	 *
	 * Set the link to the user's upload feed and make a SimpleXML connection
	 * to the file. If this is ok, register all namespaces so we can use them
	 * in our SimpleXML queries for data parsing.
	 *
	 * @access public static
	 * @param string $username
	 * @return SimpleXML instance $xml
	 */
	public static function connectToXmlFeed($username) {
		// Compose the URL to the feed:
		$feedLink = new M_Uri(self::getUserUploadFeedUri($username));

		/* simpleXML implementation */

		// load the document
		try {
			$xml = new SimpleXMLElement($feedLink,null,true);
		} catch (Exception $e) {
			throw new M_Exception(sprintf(
				'Could not load user upload feed. User: %s, Feedlink: %s',
				$username,
				$feedLink)
			);
		}

		// register all namespaces
		$namespaceArray = $xml->getDocNamespaces();

		foreach($namespaceArray as $idNamespace => $namespace) {
			$nsPrefix = $idNamespace;
			$nsLink = $namespace;

			$xml->registerXPathNamespace($nsPrefix, $nsLink);
		}

		return $xml;
	}

	/**
	 * Connect To Single Video XML Feed
	 *
	 * Set the link to a single video feed and make a SimpleXML connection
	 * to the file. If this is ok, register all namespaces so we can use them
	 * in our SimpleXML queries for data parsing.
	 *
	 * @access public static
	 * @param string $username
	 * @return SimpleXML instance $xml
	 */
	public static function connectToSingleVideoXmlFeed($videoId) {
		// Compose the URL to the feed:
		$feedLink = new M_Uri(self::getSingleVideoFeedUri($videoId));

		/* simpleXML implementation */

		// load the document
		try {
			$xml = new SimpleXMLElement($feedLink,null,true);
		} catch (Exception $e) {
			throw new M_Exception(sprintf(
				'Could not load user upload feed. User: %s, Feedlink: %s',
				$username,
				$feedLink)
			);
		}

		// register all namespaces
		$namespaceArray = $xml->getDocNamespaces();

		foreach($namespaceArray as $idNamespace => $namespace) {
			$nsPrefix = $idNamespace;
			$nsLink = $namespace;

			$xml->registerXPathNamespace($nsPrefix, $nsLink);
		}

		return $xml;
	}

	/**
	 * Get Video Metadata From an XML Element
	 *
	 * For each video in the XML file, this function will parse the data
	 * and pass it on to the video object class. 
	 *
	 * @param SimpleXMLElement $element
	 * @param string $username
	 *		The username
	 * @param int $entryCounter
	 *		A simple counter used to skip from entry to entry
	 * @return M_ServiceYouTubeVideo
	 */
	public static function getVideoFromXmlElement(SimpleXMLElement $element, $username, $entryCounter) {

		// set the namespaces for the xpath queries and add them to their respective arrays
		$commentsArray = $element->xpath('//gd:feedLink');
		$ratingArray = $element->xpath('//gd:rating');
		$statsArray = $element->xpath('//yt:statistics');
		$durationArray = $element->xpath('//yt:duration');
		$thumbnailArray = $element->xpath('//media:thumbnail');

		$videoId = self::getVideoIdFromString((string) $element->id);
		$videolink = self::getVideoLinkById((string) $videoId);

		if(isset($ratingArray[$entryCounter]['average'])) {
			$ratingValue = $ratingArray[$entryCounter]['average'];
			$rating = round((float) $ratingValue, 2);
		} else {
			$rating = t("This video hasn't been rated yet");
		}

		if(isset($ratingArray[$entryCounter]['numRaters'])) {
			$ratingCount = $ratingArray[$entryCounter]['numRaters'];
			$ratingAmountOf = $ratingCount;
		} else {
			$ratingAmountOf = t("This video hasn't been rated yet");
		}

		/*
		 * Array with 5 links related to the video
		 *
		 * [0] = direct link to video
		 * [1] = link to feed with video responses
		 * [2] = link to feed with related videos
		 * [3] = mobile page of this video
		 * [4] = link to self
		 * [5] = link to feed with comments
		 *
		 */
		$linkArray = array();

		foreach($element->link as $link) {
			$linkValue = $link['href'];
			array_push($linkArray, $linkValue);
		}

		/*
		 * As there are multiple category and tag elements present in the
		 * XML feed, we need to run through them with a loop to determine
		 * which is which, based on the given scheme attribute
		 */
		$categoryArray = array();
		$tagArray = array();

		foreach($element->category as $categoryValue) {

			switch ($categoryValue['scheme']) {
				case 'http://schemas.google.com/g/2005#kind':
					// do nothing in this case
					break;

				case 'http://gdata.youtube.com/schemas/2007/categories.cat':
					$category = $categoryValue['term'];
					break;

				case 'http://gdata.youtube.com/schemas/2007/keywords.cat':
					$tagValue = $categoryValue['term'];
					array_push($tagArray, $tagValue);
					break;

				default:
					break;
			}

			if(isset($element->category['label'])) {
				$categoryName = $categoryValue['label'];
			} else {
				$categoryName = $categoryValue['term'];
				array_push($categoryArray, $categoryName);
			}
		}

		// Array with small thumbnail links.
		$smallThumbsUriArray = array();

		foreach($thumbnailArray as $thumbEntry) {
			$nr = substr(strrchr($thumbEntry['url'], "/"), 1);
			if($thumbEntry['url'] == 'http://i.ytimg.com/vi/' . $videoId . '/' . $nr) {
				if ($thumbEntry['width'] == 320) {
					$bigThumbnailUri = $thumbEntry['url'];
				} else {
					array_push($smallThumbsUriArray, $thumbEntry['url']);
				}
			}
		}

		// Create a new M_ServiceYouTubeVideo object and insert all values
		$video = new M_ServiceYouTubeVideo();
		$video
			->setTitle((string) $element->title)
			->setVideoId((string) $videoId)
			->setDescription((string) $element->content)
			->setCategory((string) $category)
			->setAuthor((string) $element->author->name)
			->setPublicationDate(self::getDateFromString($element->published))
			->setLastUpdatedDate(self::getDateFromString($element->updated))
			->setTagline(self::createTagLineFromTagArray($tagArray))
			->setRating((float) $rating)
			->setNumberOfRatings((int) $ratingAmountOf)
			->setTimesFavorited((int) $statsArray[$entryCounter]['favoriteCount'])
			->setNumberOfPageviews((int) $statsArray[$entryCounter]['viewCount'])
			->setNumberOfComments((int) $commentsArray[$entryCounter]['countHint'])
			->setCommentsFeedUri(new M_Uri($commentsArray[$entryCounter]['href']))
			->setVideoUri(new M_Uri(self::getVideoLinkById((string) $videoId)))
			->setRelatedVideosFeedUri(new M_Uri($linkArray[2]))
			->setVideoResponsesFeedUri(new M_Uri($linkArray[1]))
			->setVideoMobileFeedUri(new M_Uri($linkArray[3]))
			->setVideoDuration((int) $durationArray[$entryCounter]['seconds'])
			->setBigThumbnailUri(new M_Uri($bigThumbnailUri))
			->setSmallThumbUriOne(new M_Uri($smallThumbsUriArray[0]))
			->setSmallThumbUriTwo(new M_Uri($smallThumbsUriArray[1]))
			->setSmallThumbUriThree(new M_Uri($smallThumbsUriArray[2]));

		// Return the video
		return $video;
	}

	/**
	 * Get user
	 *
	 * Get user metadata and info from a specified user by username
	 *
	 * @access public static
	 * @param SimpleXMLObject $xml
	 * @param string $username
	 * @return M_ServiceYouTubeUser
	 */
	public static function getUserInfoFromXml($xml, $username) {
		// Calculate the number of videos
		$numberOfVideos = 0;
		foreach($xml->entry as $entry) {
			$numberOfVideos +=1;
		}

		// Create a new M_ServiceYouTubeUser object and set its properties
		$user = new M_ServiceYouTubeUser();
		$user
			->setUsername((string) $xml->author->name)
			->setAuthorname((string) $xml->entry->author->name)
			->setChannel(new M_Uri(self::getChannelByUsername($username)))
			->setChannelLastUpdated(new M_Date($xml->updated))
			->setNumberOfVideos((int) $numberOfVideos);

		// Return the user
		return $user;
	}

	/**
	 * Will properly format and return the date string from the xml
	 *
	 * @access public static
	 * @param string $string
	 *		This is the date string provided by the xml file
	 * @return M_Date
	 */
	public static function getDateFromString($string) {
		// 2010-04-15T06:47:00.000Z
		$matches = array();
		if(preg_match('/([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2}).*/', $string, $matches) == 1) {
			return new M_Date(array(
				'year'   => $matches[1],
				'month'  => $matches[2],
				'day'    => $matches[3],
				'hour'   => $matches[4],
				'minute' => $matches[5],
				'second' => $matches[6]
			));
		} else {
			throw new M_Exception(sprintf(
				'Cannot parse date from string %s',
				$string
			));
		}
	}

	/**
	 * Will create a tagline from the array of tags
	 *
	 * @access public static
	 * @param string $string
	 *		The array of tags
	 * @return string
	 */
	public static function createTagLineFromTagArray($tagArray) {
		if($tagArray) {
			$taglineVar = "";
			foreach($tagArray as $tag) {
				$taglineVar = $taglineVar . ', ' . $tag;
			}

			// ommit the first comma and space of the string
			$tagline = substr($taglineVar, 2);
			return $tagline;
		} else {
			throw new M_Exception(sprintf(
				'Cannot parse tagline from array %s',
				M_Debug::printr($tagArray)
			));
		}
	}

	/**
	 * Will retrieve the video id from the given video uri
	 *
	 * @access public static
	 * @param string $string
	 *		The video uri
	 * @return string
	 */
	public static function getVideoIdFromString($string) {
		// http://gdata.youtube.com/feeds/api/videos/z_BshfWGQos
		if($string) {
			$id = substr(strrchr($string, "/"), 1);
			return $id;
		} else {
			throw new M_Exception(sprintf(
				'Cannot parse id from string %s',
				$string
			));
		}
	}

	/**
	 * Will create the direct link to a video from the given video id
	 *
	 * @access public static
	 * @param int $id
	 *		The video id
	 * @return M_Uri $videoLink
	 */
	public static function getVideoLinkById($id) {
		// http://www.youtube.com/watch?v=EisxFvVVJXo
		if($id) {
			$videolink = new M_Uri('http://www.youtube.com/watch');
			$videolink->setQueryVariable('v', $id);
			return $videolink;
		} else {
			throw new M_Exception(sprintf(
				'Cannot parse url from string %s',
				$id
			));
		}
	}

	/**
	 * Will create a direct link to the user's uploads feed
	 *
	 * @access public static
	 * @param string $username
	 *		The username
	 * @return M_Uri $feedLink
	 */
	public static function getUserUploadFeedUri($username) {
		// http://gdata.youtube.com/feeds/api/users/username/uploads
		if($username) {
			$feedLink = new M_Uri('http://gdata.youtube.com/feeds/api/users/'. $username .'/uploads');
			return $feedLink;
		} else {
			throw new M_Exception(sprintf(
				'Cannot compuse feedlink URL from string $s',
				$username
			));
		}
	}

	/**
	 * Will create a direct link to a single video's feed link
	 *
	 * @access public static
	 * @param string $videoId
	 *		The video ID
	 * @return M_Uri $feedLink
	 */
	public static function getSingleVideoFeedUri($videoId) {
		// http://gdata.youtube.com/feeds/api/videos/z_BshfWGQos
		if($videoId) {
			$feedLink = new M_Uri('http://gdata.youtube.com/feeds/api/videos/'. $videoId);
			return $feedLink;
		} else {
			throw new M_Exception(sprintf(
				'Cannot compuse feedlink URL from string $s',
				$videoId
			));
		}
	}

	/**
	 * Get the user's channel URL
	 *
	 * @access public
	 * @param string $username
	 * 		The username. For more info, read {@link M_ServiceYouTube::getUser()}
	 * @return M_Uri $channel
	 */
	public static function getChannelByUsername($username) {
		// http://www.youtube.com/username
		if($username) {
			$channel = new M_Uri('http://www.youtube.com/' . $username);
			return $channel;
		} else {
			throw new M_Exception(sprintf(
				'Cannot compose the channel URL from string $s',
				$username
			));
		}
	}
}