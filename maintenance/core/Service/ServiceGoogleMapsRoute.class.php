<?php
/**
 * ServiceGoogleMapsRoute
 *
 * Implementation of the Google Directions API. For more info, read the docs at
 * {@link http://code.google.com/intl/nl/apis/maps/documentation/directions}
 *
 * @author Tim Segers
 * @package core
 */
class M_ServiceGoogleMapsRoute {

	/* -- CONSTANTS -- */

	/**
	 * Base Request URI
	 */
	const BASE_REQUEST_URI = 'http://maps.googleapis.com/maps/api/directions/json';

	/**
	 * Route request return status: OK
	 */
	const STATUS_OK = 'OK';

	/**
	 * Route request return status: NOT_FOUND
	 */
	const STATUS_NOT_FOUND = 'NOT_FOUND';

	/**
	 * Route request return status: ZERO_RESULTS
	 */
	const STATUS_ZERO_RESULTS = 'ZERO_RESULTS';

	/**
	 * Route request return status: MAX_WAYPOINTS_EXCEEDED
	 */
	const STATUS_MAX_WAYPOINTS_EXCEEDED = 'MAX_WAYPOINTS_EXCEEDED';

	/**
	 * Route request return status: INVALID_REQUEST
	 */
	const STATUS_INVALID_REQUEST = 'INVALID_REQUEST';

	/**
	 * Route request return status: OVER_QUERY_LIMIT
	 */
	const STATUS_OVER_QUERY_LIMIT = 'OVER_QUERY_LIMIT';

	/**
	 * Route request return status: REQUEST_DENIED
	 */
	const STATUS_REQUEST_DENIED = 'REQUEST_DENIED';
	
	/**
	 * Route request return status: UNKNOWN_ERROR
	 */
	const STATUS_UNKNOWN_ERROR = 'UNKNOWN_ERROR';

	/* -- PROPERTIES -- */

	/**
	 * Holds the {@link M_ServiceGoogleMapsLocation} instance representing
	 * the location the route will depart from.
	 *
	 * @access private
	 * @var M_ServiceGoogleMapsLocation
	 */
	private $_fromLocation;

	/**
	 * Holds the {@link M_ServiceGoogleMapsLocation} instance representing
	 * the location the route will arrive at.
	 *
	 * @access private
	 * @var M_ServiceGoogleMapsLocation
	 */
	private $_toLocation;

	/**
	 * Holds the travel mode. For more information, read the docs on
	 * {@link M_ServiceGoogleMapsRoute::setTravelMode()}
	 *
	 * @access private
	 * @var string
	 */
	private $_travelMode;

	/**
	 * Holds wheter the request should include alternative routes or not,
	 * represented by a boolean.
	 *
	 * @access private
	 * @var boolean
	 */
	private $_allowAlternativeRoutes;

	/**
	 * Holds the boolean determining wheter or not we should avoid
	 * highways in the route request
	 *
	 * @access private
	 * @var boolean
	 */
	private $_avoidHighways;

	/**
	 * Holds the boolean determining wheter or not we should avoid
	 * toll ways / bridges in the route request
	 *
	 * @access private
	 * @var boolean
	 */
	private $_avoidTollways;

	/**
	 * Holds the unit system (metric | imperial) the route request will
	 * work with.
	 *
	 * @access private
	 * @var string
	 */
	private $_unitSystem;

	/**
	 * Holds the region ccTLD code used in the route request. For more info
	 * about this, read the docs on {@link M_ServiceGoogleMapsRoute::setRegion()}
	 *
	 * @access private
	 * @var string
	 */
	private $_region;

	/**
	 * Holds the language the response should be returned in. For more info
	 * about this, read the docs on {@link M_ServiceGoogleMapsRoute::setLanguage()}
	 */
	private $_language;

	/**
	 * Holds the sensor value
	 *
	 * Indicates whether or not the directions request comes from a device with
	 * a location sensor. This value must be either true or false.
	 *
	 * @access private
	 * @var bool
	 */
	private $_sensor = false;

	/**
	 * Holds the collection of waypoints we have to hold in account for
	 * the route request
	 *
	 * @access private
	 * @var array
	 */
	private $_waypoints = array();

	/**
	 * Holds the boolean determining wheter we should optimize the waypoint
	 * order automatically or not
	 *
	 * @access private
	 * @var bool
	 */
	private $_optimizeWaypointOrder;

	/**
	 * Array containing the response set for every route request made
	 *
	 * @access private
	 * @var array
	 */
	private $_info = array();

	/**
	 * String containing the error message from the HTTP request, if any
	 *
	 * @access private
	 * @var string
	 */
	private $_errorCode;

	/* -- PUBLIC FUNCTIONS -- */

	/**
	 * Constructor
	 *
	 * @access public
	 * @param M_ServiceGoogleMapsLocation $from
	 *		The {@link M_ServiceGoogleMapsLocation} instance representing the
	 *		location this route will depart from
	 * @param M_ServiceGoogleMapsLocation $to
	 *		The {@link M_ServiceGoogleMapsLocation} instance representing the
	 *		location this route will arrive at
	 */
	public function __construct(M_ServiceGoogleMapsLocation $from, M_ServiceGoogleMapsLocation $to) {
		return $this
				->setFromAddress($from)
				->setToAddress($to);
	}

	/**
	 * Set From Address
	 *
	 * This function can be used to set the departure location for this route.
	 *
	 * @access public
	 * @param M_ServiceGoogleMapsLocation $fromLocation
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setFromAddress(M_ServiceGoogleMapsLocation $fromLocation) {
		$this->_fromLocation = $fromLocation;
		return $this;
	}

	/**
	 * Get From Address
	 *
	 * Will return the from location. For more information, read the docs on
	 * {@link M_ServiceGoogleMaps::setFromAddress()}
	 *
	 * @access public
	 * @return M_ServiceGoogleMapsLocation
	 */
	public function getFromAddress() {
		return $this->_fromLocation;
	}

	/**
	 * Set To Address
	 *
	 * This function can be used to set the arrival location for this route.
	 *
	 * @access public
	 * @param M_ServiceGoogleMapsLocation $toLocation
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setToAddress(M_ServiceGoogleMapsLocation $toLocation) {
		$this->_toLocation = $toLocation;
	}

	/**
	 * Get To Address
	 *
	 * Will return the arrival location. For more information, read the docs on
	 * {@link M_ServiceGoogleMaps::setToAddress()}
	 *
	 * @access public
	 * @return M_ServiceGoogleMapsLocation
	 */
	public function getToAddress() {
		return $this->_toLocation;
	}

	/**
	 * Set Travel Mode
	 *
	 * This function can be used to set the travel mode. As of currently, 3
	 * modes are supported:
	 *		- driving (default)
	 *		- walking (where available)
	 *		- bicycling (only in USA)
	 *
	 * NOTE: if an invalid travel mode is given, or no travel mode was set,
	 * the route will be calculated for driving by default.
	 *
	 * @access public
	 * @param string $mode
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setTravelMode($mode) {
		switch($mode) {
			case 'driving':
			case 'walking':
			case 'bicycling':
				$this->_travelMode = $mode;
				break;

			default:
				break;
		}
		return $this;
	}

	/**
	 * Get Travel Mode
	 *
	 * Will return the travel mode. For more information, read the docs on
	 * {@link M_ServiceGoogleMaps::setTravelMode()}
	 *
	 * @access public
	 * @return string
	 */
	public function getTravelMode() {
		return $this->_travelMode;
	}

	/**
	 * Set Allow Alternative Routes
	 *
	 * If set to TRUE, the route request will provide alternative routes
	 * as well, if available. If set to FALSE, the route request will only
	 * provide the default (and shortest) route.
	 *
	 * NOTE: as of currently, the use of this function is discouraged,
	 * as we do not support multiple routes per request yet for processing.
	 * Setting this parameter to TRUE will, in other words, not do anything.
	 *
	 * @access public
	 * @param boolean $flag
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAllowAlternativeRoutes($flag) {
		$this->_allowAlternativeRoutes = (bool) $flag;
		return $this;
	}

	/**
	 * Get Allow Alternative Routes
	 *
	 * Will return the alternative routes bool. For more information,
	 * read the docs on {@link M_ServiceGoogleMaps::setAllowAlternativeRoutes()}
	 *
	 * @access public
	 * @return boolean
	 */
	public function getAllowAlternativeRoutes() {
		return $this->_allowAlternativeRoutes;
	}

	/**
	 * Set Avoid Highways
	 *
	 * If set to TRUE, the route request will avoid highways for the route
	 * calculation. If set to FALSE, it will not.
	 *
	 * !! IMPORTANT NOTE: only one of the avoid parameters can be used at a
	 * time, meaning that we can either avoid highways or avoid tollways, but
	 * not avoid both at the same time. Keep this in mind when setting the
	 * route parameters!
	 * 
	 * @access public
	 * @param boolean $flag
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAvoidHighways($flag) {
		$this->_avoidHighways = (bool) $flag;
		return $this;
	}

	/**
	 * Get Avoid Highways
	 *
	 * Will return the avoid highways bool. For more information,
	 * read the docs on {@link M_ServiceGoogleMaps::setAvoidHighways()}
	 *
	 * @access public
	 * @return boolean
	 */
	public function getAvoidHighways() {
		return $this->_avoidHighways;
	}

	/**
	 * Set Avoid Tollways
	 *
	 * If set to TRUE, the route request will avoid toll ways / bridges
	 * for the route calculation. If set to FALSE, it will not.
	 *
	 * !! IMPORTANT NOTE: only one of the avoid parameters can be used at a
	 * time, meaning that we can either avoid highways or avoid tollways, but
	 * not avoid both at the same time. Keep this in mind when setting the
	 * route parameters!
	 *
	 * @access public
	 * @param boolean $flag
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setAvoidTollWays($flag) {
		$this->_avoidTollways = (bool) $flag;
		return $this;
	}

	/**
	 * Get Avoid Tollways
	 *
	 * Will return the avoid tollways bool. For more information,
	 * read the docs on {@link M_ServiceGoogleMaps::setAvoidTollWays()}
	 *
	 * @access public
	 * @return boolean
	 */
	public function getAvoidTollways() {
		return $this->_avoidTollways;
	}

	/**
	 * Set Unit System
	 *
	 * This function can be used to set the unit system the route request
	 * will return its result in. As of currently, 2 systems are supported:
	 *		- metric (m, km, ...)
	 *		- imperial (miles, feet, ...)
	 *
	 * NOTE: if not specified, or an invalid system is given, by default
	 * the system will use the unit system of the country where the
	 * starting location is located. E.g. if we calculate a route from
	 * Antwerp, Belgium to London, England, the returning result will be
	 * expressed in kilometres, while the reverse route will be expressed
	 * in miles.
	 *
	 * @access public
	 * @param string $system
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setUnitSystem($system) {
		switch($system) {
			case 'metric':
			case 'imperial':
				$this->_unitSystem = (string) $system;

			default:
				break;
		}
		return $this;
	}

	/**
	 * Get Unit System
	 *
	 * Will return the unit system. For more information, read the docs
	 * on {@link M_ServiceGoogleMaps::setUnitSystem()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getUnitSystem() {
		return $this->_unitSystem;
	}

	/**
	 * Set Region
	 *
	 * This function allows you to specify a region for the request. This
	 * allows you to specify a region / country you want to calculate routes
	 * for. For example, if you would request a route from Toledo to Madrid
	 * in Spain without specifying the region, the Google Directions service
	 * will use the city Toledo in Ohio, USA. To solve this you should set
	 * this property to 'es', indicating the request should be regioned in
	 * and around Spain.
	 *
	 * For a complete list of which region codes are supported, read
	 * {@see http://en.wikipedia.org/wiki/CcTLD}.
	 *
	 * @access public
	 * @param string $region
	 *		The ccTLD region code, to be used in the request
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setRegion($region) {
		$this->_region = (string) $region;
		return $this;
	}

	/**
	 * Get Region
	 *
	 * Will return the region. For more information, read the docs
	 * on {@link M_ServiceGoogleMaps::setRegion()}
	 *
	 * @access public
	 * @return string
	 */
	public function getRegion() {
		return $this->_region;
	}

	/**
	 * Set Language
	 *
	 * This function may be used to set the language in which the request
	 * results should be returned. This language is represented by a language
	 * code (string). A full list of which languages are supported can be found
	 * at the following link:
	 * {@see http://spreadsheets.google.com/pub?key=p9pdwsai2hDMsLkXsoM05KQ&gid=1}
	 *
	 * NOTE: if no language is set, the Google Directions service will
	 * return the results in the browser language by default.
	 *
	 * @access public
	 * @param string $lang
	 *		The requested language
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */

	public function setLanguage($lang) {
		$this->_language = (string) $lang;
		return $this;
	}

	/**
	 * Get Language
	 *
	 * Will return the language. For more information, read the docs
	 * on {@link M_ServiceGoogleMaps::setLanguage()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getLanguage() {
		return $this->_language;
	}
	
	/**
	 * Set Waypoints
	 *
	 * This function may be used to specify one or more waypoints that have
	 * to be passed during the route. For example, if we have to travel from
	 * Brussels to Antwerp but we want to pass through Leuven and Hasselt
	 * first, we may specify an array containing the 2 coordinates for these
	 * 2 cities.
	 *
	 * NOTE: the order in which these waypoints are given also determines
	 * the order in which they will be passed en route. So given the previous
	 * example, the route will pass through Leuven first, then to Hasselt and
	 * finally arriving in Antwerp, while reversing Leuven and Hasselt would
	 * result in travelling from Brussels to Hasselt, then to Leuven, and
	 * finally to Antwerp.
	 *
	 * NOTE: the order of these waypoints can be automatically optimized
	 * bu setting {@link M_ServiceGoogleMapsRoute::setOptimizeWaypointOrder}
	 * to TRUE. Read the docs on that function for more information.
	 *
	 * NOTE: the number of waypoints is restricted to a maximum of 8 for the
	 * free version of the Google Directions API. When exceeding this
	 * number, and error will be thrown in the status of the response.
	 *
	 * @access public
	 * @param array $waypoints
	 *		The collection of {@link M_ServiceGoogleMapsLocation} instances,
	 *		each one representing a waypoint we should pass during our route.
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setWaypoints($waypoints) {
		$this->_waypoints = (array) $waypoints;
		return $this;
	}

	/**
	 * Get Waypoints
	 *
	 * Will return the waypoints. For more information, read the docs
	 * on {@link M_ServiceGoogleMaps::setWaypoints()}
	 *
	 * @access public
	 * @return array
	 *		The collection of {@link M_ServiceGoogleMapsLocation} objects
	 *		representing the waypoint locations
	 */
	public function getWaypoints() {
		return $this->_waypoints;
	}

	/**
	 * Add Waypoint
	 *
	 * This function allows you to add a waypoint to the route parameters.
	 * For more information on how these waypoints work, please read the docs
	 * on {@link M_ServiceGoogleMapsRoute::setWaypoints()}.
	 *
	 * @access public
	 * @param M_ServiceGoogleMapsLocation $location
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function addWaypoint(M_ServiceGoogleMapsLocation $location) {
		$this->_waypoints[] = $location;
		return $this;
	}

	/**
	 * Set Optimize Waypoint Order
	 * 
	 * If set to TRUE, an additional request parameter is added specifying we
	 * should automatically sort the given waypoints for the most
	 * optimal route. For example, when we specify a route from Brugge to
	 * Hasselt, passing the waypoints Antwerp and Gent without setting this
	 * parameter to TRUE, the route will pass through all points in the order
	 * Brugge -> Antwerp -> Gent -> Hasselt, while if we do set this
	 * parameter to TRUE, the route will go in the order
	 * Brugge -> Gent - Antwerp -> Hasselt, which is a way more logical
	 * and faster route.
	 *
	 * NOTE: to determine the best waypoint order, Google does not use the
	 * shortest possible distance in km, but the shortest travelling time
	 * in minutes. As a result, the optimized route can be longer in
	 * raw distance, but will have a shorter travelling time.
	 *
	 * NOTE: if no waypoints are specified for the route, this function
	 * will obviously have no effect on the route result.
	 * 
	 * @access public
	 * @param bool $flag
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setOptimizeWaypointOrder($flag) {
		$this->_optimizeWaypointOrder = (bool) $flag;
		return $this;
	}

	/**
	 * Get Optimize Waypoint Order
	 *
	 * Will return the optimize waypoints boolean. For more information,
	 * read the docs on {@link M_ServiceGoogleMaps::setOptimizeWaypointOrder()}
	 *
	 * @access public
	 * @return bool
	 */
	public function getOptimizeWaypointOrder() {
		return $this->_optimizeWaypointOrder;
	}

	/**
	 * Set Sensor
	 *
	 * Indicates whether or not the directions request comes from a device
	 * with a location sensor. This value must be either true or false.
	 *
	 * NOTE: this parameter is mandatory for the request, so if ignored,
	 * the request will by default use FALSE as sensor value.
	 *
	 * @param bool $flag
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function setSensor($flag) {
		$this->_sensor = (bool) $flag;
		return $this;
	}

	/**
	 * Get Sensor
	 *
	 * Will return the sensor boolean. For more information,
	 * read the docs on {@link M_ServiceGoogleMaps::setSensor()}
	 *
	 * @access public
	 * @return bool
	 */
	public function getSensor() {
		return $this->_sensor;
	}

	/**
	 * Get Distance
	 *
	 * This function will return the total distance for the calculated
	 * route. By default this will return the raw distance in meter.
	 * Optionally you may pass a parameter $formatted (flag), and when set to
	 * TRUE, this function will return a the formatted distance in km.
	 *
	 * @access public
	 * @param flag $formatted
	 *		If set to TRUE, this function will output the formatted distance in km
	 * @return int | string
	 *		The total distance in meters, or the formatted value in km
	 */
	public function getDistance($formatted = false) {
		// Fetch the route, returning the calculated distance
		$route = $this->_getRouteInfoFromUri($this->_getRequestUriFromParameters());

		if(isset($route['distance'])) {
			// If the formatted distance was requested
			if($formatted) return round($route['distance'] / 1000, 1) . ' km';

			// Return the distance in meter
			return $route['distance'];
		}

		// If we are still here something went wrong, so we return FALSE
		return FALSE;
	}

	/**
	 * Get Travelling time
	 *
	 * This function will return the total travelling time for the
	 * route. By default this will return the raw travelling time in seconds.
	 * Optionally you may pass a parameter $formatted (flag), and when set to
	 * TRUE, this function will return a formatted string in the form "xx h xx min".
	 *
	 * @access public
	 * @param flag $formatted
	 *		If set to TRUE, this function will output the travelling time
	 *		as a formatted string (xx h xx min)
	 * @return int | string
	 *		The total travelling time in seconds or the formatted value in
	 *		xxh xx min.
	 */
	public function getTravellingTime($formatted = false) {
		// Fetch the route
		$route = $this->_getRouteInfoFromUri($this->_getRequestUriFromParameters());

		if(isset($route['duration'])) {
			// If the formatted value in hours and minutes was requested
			if($formatted) {
				// Fetch the time array using the seconds
				$duration = M_Date::getTimeArray($route['duration'], M_Date::SECOND);

				// Parse the duration output
				$out = '';
				if($duration['hours'] > 0) $out .= $duration['hours'] . 'h ';
				$out .= $duration['minutes'] . 'min';

				// Return the value
				return $out;
			}

			// Return the duration in seconds
			return $route['duration'];
		}

		// If we are still here something went wrong, so we return FALSE
		return FALSE;
	}

	/**
	 * Calculate
	 *
	 * This function can be used to check if the route request succeeded,
	 * returning TRUE or FALSE in response.
	 * 
	 * This function will make the http request for this route using
	 * {@link M_ServiceGoogleMapsRoute::_getRouteInfoFromUri()} and
	 * process the status code given for the request. If the request
	 * succeeded properly (e.g. the status code is 'OK') we will return
	 * TRUE. If not, we will set the error code in the object and return
	 * FALSE.
	 *
	 * The status code can have the following possible values:
	 * - OK: indicates the response contains a valid result.
	 * - NOT_FOUND: indicates at least one of the locations specified
	 *   in the requests's origin, destination, or waypoints could not
	 *   be geocoded.
	 * - ZERO_RESULTS: indicates no route could be found between the
	 *   origin and destination.
	 * - MAX_WAYPOINTS_EXCEEDED: indicates that too many waypointss were
	 *   provided in the request The maximum allowed waypoints is 8, plus
	 *   the origin, and destination.
	 * - INVALID_REQUEST: indicates that the provided request was invalid.
	 * - OVER_QUERY_LIMIT: indicates the service has received too many requests
	 *   from your application within the allowed time period.
	 * - REQUEST_DENIED: indicates that the service denied use of the
	 *   directions service by your application.
	 * - UNKNOWN_ERROR: indicates a directions request could not be processed
	 *   due to a server error. The request may succeed if you try again.
	 *
	 * @access public
	 * @return M_ServiceGoogleMapsRoute
	 *		Returns itself, for a fluent programming interface
	 */
	public function calculate() {
		// Fetch the route
		$route = $this->_getRouteInfoFromUri($this->_getRequestUriFromParameters());

		// Check if the request status was OK
		if($route['status'] == self::STATUS_OK) {
			// The request succeeded properly, so return TRUE
			return TRUE;
		} else {
			// Another message was returned, indicating something went
			// wrong during the request. As a result, save the status
			// message in the property, and return FALSE
			$this->_errorCode = $route['status'];
			return FALSE;
		}
	}

	/**
	 * Get Error Code
	 *
	 * Will return the error code, if any was set during calculation. For
	 * more info about this, read the docs on {@link M_ServiceGoogleMapsRoute::calculate()}.
	 *
	 * @access public
	 * @return string
	 *		The errorcode in string format
	 */
	public function getErrorCode() {
		return $this->_errorCode;
	}

	/* -- PRIVATE / PROTECTED -- */

	/**
	 * Get Route Info From Uri
	 *
	 * This function will make the http request to the Google Directions API
	 * service, process the result using {@link M_ServiceGoogleMapsRoute::_processResponse()},
	 * save the response in an associative array in a private property, and
	 * finally return the array.
	 *
	 * NOTE: in the associative array we save the response for every
	 * unique url request. This way we only have to make every http request
	 * once, while if a parameter is added, a new http request is made.
	 *
	 * @access protected
	 * @param M_Uri $uri
	 * @return array
	 *		The array containing the response variables for this uri request
	 */
	protected function _getRouteInfoFromUri(M_Uri $uri) {
		// Fetch the string representation of the M_Uri object
		$uriString = $uri->toString();

		// If there is no resultset for this uri specified yet, we will have
		// to make and process the request:
		if(!isset($this->_info[$uriString])) {

			// Get a URI Session, which will help us to check whether or not the
			// URI exists, and to fetch the response we get from that address:
			$uriSession = $uri->getSession();

			// Check if the URL exists:
			if($uriSession->uriExists() && $uriSession->getHttpCode() == '200') {
				
				// Make the http request, fetching and processing the result
				$response = $this->_processResponse($uriSession->getContents());
			}

			// If the URL does not exist, throw an error
			else {
				throw new M_Exception(sprintf(
					'Could not make the http request for route calculation (invalid url specified or url does not exist) with url %s in %s',
					$uri->toString()
				));
			}

			// Save the response for the given uri
			$this->_info[$uriString] = $response;
		}

		// Return the response array
		return $this->_info[$uriString];
	}

	/**
	 * Process Response
	 *
	 * This function will process the response received from the Google
	 * service after the route request. This response is in JSON format,
	 * allowing us to easily read its contents. The response contains
	 * the following information:
	 *		- status: the status of the request. For more info about this,
	 *		  read the docs on {@link M_ServiceGoogleMapsRoute::calculate()}
	 *		- routes: array containing all possible routes
	 *
	 * Generally speaking the response will only contain one route, but if
	 * alternate routes were enabled through the use of
	 * {@link M_ServiceGoogleMaps::setAllowAlternateRoutes()}, we might
	 * receive more than one.
	 *
	 * Every route contains the following information:
	 *		- legs: every route contains a number of legs, every leg being
	 *		  the route from one waypoint to the next
	 *		- copyright info
	 *		- warnings (like traffic info)
	 *		- waypoint order: if waypoint optimization was enabled, this
	 *		  value will contain the order in which the waypoints were
	 *		  processed
	 *
	 * It is the legs of the route that we will have to process, as they
	 * contain the information we need, like distance and travelling time.
	 * Every one of these legs contains the following information:
	 *		- steps: an array with the steps describing how to travel down
	 *		  the leg. Typically, this will be information like "Turn left
	 *		  at Vrijheid, Hoogstraten, and keep that route for 200 meters".
	 *		  Every step also contains the distance travelled in meters, the
	 *		  travelling time in minutes and the latitude / longitude of the
	 *		  starting and end locations of that particular step.
	 *		- duration: the total time required to travel down the leg,
	 *		  expressed in seconds and as a text value in minutes.
	 *		- distance: the total distance travelled down the leg, expressed
	 *		  in meters as raw value and in kilometers or miles as a text value
	 *		- the starting and end location of the leg, both as coordinates
	 *		  and as text value
	 * 
	 * NOTE: For now, the most important information we need is the total
	 * distance and travelling time for every calculated route. Therefor,
	 * the different steps of the route will be processed, but not parsed
	 * entirely.
	 *
	 * NOTE: as of currently only the first (and therefor shortest) route
	 * provided will be parsed. Additional routes will be ignored for now.
	 * 
	 * TODO: parse the additional information, supporting multiple routes,
	 * information of every route and parsing the steps for every leg of a
	 * route.
	 * 
	 * @access protected
	 * @param string $response
	 *		The route request response, in JSON format
	 */
	protected function _processResponse($response) {
		// Decode the JSON encoded response
		$routeInfo = M_Helper::jsonDecode($response);

		// Create an array we will set all the response parameters in
		$out = array();

		// Set the status
		$out['status'] = $routeInfo->status;

		// Fetch all the different routes specified in the response
		$routes = $routeInfo->routes;

		// For now, only grab information of the first route
		$i = 0;
		foreach($routes as $route) {
			if($i == 0) {

				// Save the entire route information array in the output array
				$out['route'] = $route;
				
				// Calculate the total distance of the route. To do this, we
				// have to add up the distances for each route leg (e.g.: if
				// the route goes through 4 locations, we have to add up the
				// distance for every separate leg, as the total distance for
				// the entire route is not provided by default)
				//
				// Also, calculate the total travelling time, provided
				// in seconds. Same principle here, add up the travelling
				// times for each leg of the route.
				$distance = 0;
				$duration = 0;
				foreach($route->legs as $leg) {
					$d = $leg->distance;
					$distance += $d->value;

					$t = $leg->duration;
					$duration += $t->value;
				}

				// Add all parameters to the output array
				$out['distance'] = $distance;
				$out['duration'] = $duration;
			}
			$i++;
		}

		// Finally, return the output array
		return $out;
	}

	/**
	 * Get Request Uri From Parameters
	 *
	 * This function will construct and return an M_Uri object representing
	 * the request link that can be used for making the http request to the
	 * Google Directions API.
	 *
	 * NOTE:
	 *
	 * @access protected
	 * @return M_Uri
	 *		The route request {@link M_Uri} object
	 */
	protected function _getRequestUriFromParameters() {
		// Construct with base uri
		$uri = new M_Uri(self::BASE_REQUEST_URI);

		// Add from and to address
		$uri->setQueryVariable('origin', $this->_fromLocation->toString());
		$uri->setQueryVariable('destination', $this->_toLocation->toString());
		
		// If a travel mode was specified
		if($this->_travelMode) {
			$uri->setQueryVariable('mode', $this->_travelMode);
		}

		// Add the waypoints, if any were given
		if(count($this->_waypoints) > 0) {

			// If set, add the optimize waypoints parameter
			if($this->_optimizeWaypointOrder) {
				$waypoints = 'optimize:true|';
			} else {
				$waypoints = '';
			}

			// Add the waypoints as lat,long|lat,long|lat,long
			$i = 1;
			foreach($this->_waypoints as $waypoint) {
				/* @var $waypoint M_ServiceGoogleMapsLocation */
				if($i != 1) $waypoints .= '|';
				$waypoints .= $waypoint->toString();
				$i++;
			}
			$uri->setQueryVariable('waypoints', $waypoints);
		}

		// Set the alternatives boolean, if given
		if($this->_allowAlternativeRoutes) {
			$uri->setQueryVariable('alternatives', $this->_allowAlternativeRoutes ? 'true' : 'false');
		}

		// Set the avoid highways boolean, if given
		if($this->_avoidHighways) {
			$uri->setQueryVariable('avoid', 'highways');
		}

		// Set the avoid tollways boolean, if given
		if($this->_avoidTollways) {
			$uri->setQueryVariable('avoid', 'tollways');
		}

		// Set the unit system, if given
		if($this->_unitSystem) {
			$uri->setQueryVariable('units', $this->_unitSystem);
		}

		// Set the region, if given
		if($this->_region) {
			$uri->setQueryVariable('region', $this->_region);
		}

		// Set the language, if given
		if($this->_language) {
			$uri->setQueryVariable('language', $this->_language);
		}

		// Add the sensor boolean, which is mandatory
		$uri->setQueryVariable('sensor', $this->_sensor ? 'true' : 'false');

		// Return the constructed uri
		return $uri;
	}
}