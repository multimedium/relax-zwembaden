<?php
/**
 * M_DateTimezone class
 * 
 * The M_DateTimezone class you to get and set the timezone that is
 * used by all date/time functions in the core.
 * 
 * @author Tom Bauwens
 * @package Core
 */
class M_DateTimezone {
	/**
	 * Default timezone
	 * 
	 * Stores the default timezone, set by {@link M_DateTimezone::setDefaultTimezone()}
	 * 
	 * @access private
	 * @var string
	 */
	private static $_defaultTimezone;
	
	/**
	 * Set Default Timezone
	 * 
	 * Sets the default timezone that is used by all date/time functions in the
	 * core
	 * 
	 * @author Tom Bauwens
	 * @see M_DateTimezone::getDefaultTimezone()
	 * @static
	 * @access public
	 * @param string $timezoneIdentifier
	 * @return void
	 */
	public static function setDefaultTimezone($timezoneIdentifier) {
		// store internal reference
		self::$_defaultTimezone = (string) $timezoneIdentifier;
		
		// Set the timezone, with PHP's built-in functions
		date_default_timezone_set(self::$_defaultTimezone);
	}
	
	/**
	 * Is default timezone available?
	 * 
	 * Will check if a default timezone is available at the moment.
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @return bool $flag
	 * 		TRUE if timezone is available, FALSE if not
	 */
	public static function isDefaultTimezoneAvailable() {
		return (self::getDefaultTimezone() !== NULL);
	}
	
	/**
	 * Set Default Timezone
	 * 
	 * Sets the default timezone that is used by all date/time functions in the
	 * core
	 * 
	 * @author Tom Bauwens
	 * @see M_DateTimezone::setDefaultTimezone()
	 * @static
	 * @access public
	 * @return string $timezoneIdentifier
	 */
	public static function getDefaultTimezone() {
		// Has default timezone been provided to the M_DateTimezone class?
		if(self::$_defaultTimezone) {
			// If so, return this default timezone
			return self::$_defaultTimezone;
		}
		
		// If we are still here, we check if a default timezone is available
		// in the php.ini configuration file:
		if(ini_get('date.timezone')) {
			// If so, return that default timezone
			ini_get('date.timezone');
		}
		
		// If none of both are available, we return NULL
		return NULL;
	}
}