<?php
/**
 * M_Menu
 * 
 * M_Menu is used to describe a menu. A menu is a collection of menu items, each
 * of them represented by an instance of {@link M_MenuItem}.
 *
 * @package Core
 */
class M_Menu extends M_Object {
	/**
	 * Singleton objects
	 * 
	 * This property stores the singleton objects that are created by 
	 * {@link M_Menu::getInstance()}
	 * 
	 * @static
	 * @access private
	 * @var array
	 */
	private static $_instances = array();
	
	/**
	 * The Menu ID
	 * 
	 * @see M_Menu::getInstance()
	 * @access private
	 * @var string
	 */
	private $_id;
	
	/**
	 * The Menu title
	 * 
	 * @access private
	 * @var string
	 */
	private $_title;
	
	/**
	 * The Menu description
	 * 
	 * @access private
	 * @var string
	 */
	private $_description;
	
	/**
	 * The {@link M_MenuItem} instances
	 * 
	 * This property stores the collection of {@link M_MenuItem} instances that
	 * are contained by the {@link M_Menu} object.
	 * 
	 * @access private
	 * @var array
	 */
	private $_items = array();
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * PRIVATE Constructor
	 * 
	 * M_Menu is a singleton object. In order to construct the object, you should 
	 * use {@link M_ControllerDispatcher::getInstance()} instead.
	 * 
	 * @access private
	 * @param string $id
	 * 		The ID of the menu
	 * @return M_Menu
	 */
	private function __construct($id) {
		$this->_id = (string) $id;
	}
	
	/**
	 * Singleton constructor
	 * 
	 * A singleton object is created for each of the menu's in the application.
	 * In order to distinguish between different menu's, an ID needs to be provided
	 * to this method.
	 * 
	 * @see M_Menu::getInstanceFromXml()
	 * @uses M_Menu::__construct()
	 * @access public
	 * @param string $id
	 * 		The ID of the menu
	 * @return M_Menu
	 */
	public static function getInstance($id) {
		// Cast the ID to a string
		$id = (string) $id;
		
		// Check if the singleton exists:
		if(! isset(self::$_instances[$id])) {
			// If not, create it now:
			self::$_instances[$id] = new self($id);
		}
		
		// Return the requested singleton:
		return self::$_instances[$id];
	}
	
	/**
	 * Construct with XML
	 * 
	 * This method can be used to construct a singleton {@link M_MenuItem} instance, 
	 * based on a definition in an XML File.
	 * 
	 * NOTE:
	 * This method will throw an exception, if a singleton object with the provided
	 * ID already exists!
	 * 
	 * @throws M_MenuException
	 * @access public
	 * @return M_Menu
	 */
	public static function getInstanceFromXml($id, M_File $xml) {
		// Cast the ID to a string
		$id = (string) $id;
		
		// If the singleton object already exists
		if(isset(self::$_instances[$id])) {
			// we throw an exception:
			throw new M_MenuException(sprintf(
				'Cannot construct M_Menu Instance from XML "%s"; Singleton with ID "%s" already exists',
				$xml->getPath(),
				$id
			));
		}
		
		// We check if the file exists:
		if(! $xml->exists()) {
			// If the file does not exist, we throw an exception
			throw new M_MenuException(sprintf(
				'Cannot construct M_Menu with "%s"; Cannot find file',
				$xml->getPath()
			));
		}
		
		// Load the XML structure into a SimpleXMLElement instance:
		$element = simplexml_load_file($xml->getPath());
		
		// Construct the menu:
		self::$_instances[$id] = new self($id);
		
		// For each of the nodes in the element
		foreach($element->children() as $node) {
			// We create an M_MenuItem instance, and we add it to the menu:
			self::$_instances[$id]->addMenuItem(
				M_MenuItem::constructWithSimpleXMLElement($node)
			);
		}
		
		// Return the menu
		return self::$_instances[$id];
	}
	
	/* -- STATIC GETTERS -- */
	
	/**
	 * Get Menu Singletons
	 * 
	 * Will provide with the collection of menu (singleton) objects. The result
	 * of this method is an iterator of {@link M_Menu} objects.
	 * 
	 * @static
	 * @access public
	 * @return ArrayIterator $menus
	 * 		The collection of {@link M_Menu} objects
	 */
	public static function getInstances() {
		return new ArrayIterator(self::$_instances);
	}
	
	/* -- GETTERS -- */

	/**
	 * Get the ID
	 *
	 * @access public
	 * @return string
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Get the title of the menu
	 * 
	 * @access public
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * Get the description of the menu
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 * Get number of contained {@link M_MenuItem} instances
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfMenuItems() {
		return count($this->_items);
	}
	
	/**
	 * Get contained {@link M_MenuItem} instances
	 * 
	 * This method will provide with the collection of {@link M_MenuItem} instances
	 * that is contained by the menu. The result of this method is an instance
	 * of {@link ArrayIterator}
	 * 
	 * @access public
	 * @return ArrayIterator $iterator
	 * 		The collection of {@link M_MenuItem} instances
	 */
	public function getMenuItems() {
		return new ArrayIterator($this->_items);
	}
	
	/**
	 * Get contain menu item, by path
	 * 
	 * NOTE:
	 * Will return NULL, if no menu item could have been found!
	 * 
	 * @access public
	 * @param string $path
	 * 		The path to look for
	 * @param bool $recursive
	 * 		Set to TRUE if you want to search recursively, FALSE if not
	 * @return M_MenuItem
	 */
	public function getMenuItemByPath($path, $recursive = TRUE) {
		// For each of the contained menu items:
		foreach($this->_items as $currentItem) {
			// Check recursively?
			if($recursive) {
				// If so, check if the current item contains an item that
				// matches the provided path:
				$rs = $currentItem->getMenuItemByPath($path, TRUE);
				
				// If so, we return it as the result of the search:
				if($rs) {
					return $rs;
				}
			}
	
			// If the current item matches the requested path
			if($currentItem->isPath($path)) {
				// we return the item as the result of the search
				return $currentItem;
			}
		}
		
		// If we're still here, it means that we have not found the item:
		return NULL;
	}
	
	/**
	 * Get active menu item
	 * 
	 * Will provide with an instance of {@link M_MenuItem} that represents the
	 * active/selected item in the menu. To do so, it uses:
	 * 
	 * - {@link M_MenuHelper::getActivePath()}
	 * 
	 * To get the path of the active/selected menu item
	 * 
	 * - {@link M_Menu::getMenuItemByPath()}
	 * 
	 * To fetch the menu item that matches the path
	 * 
	 * @access public
	 * @return M_MenuItem
	 */
	public function getMenuItemActive() {
		return $this->getMenuItemByPath(M_MenuHelper::getActivePath(), TRUE);
	}
	
	/**
	 * Get active menu item IN ROOT OF MENU
	 * 
	 * @access public
	 * @return M_MenuItem
	 */
	public function getMenuItemActiveInRoot() {
		$out = $this->getMenuItemByPath(M_MenuHelper::getActivePath(), FALSE);
		if($out) {
			return $out;
		} else {
			foreach($this->_items as $item) {
				$out = $item->getMenuItemByPath(M_MenuHelper::getActivePath(), TRUE);
				if($out) {
					$parent = $out->getParent();
					while($parent) {
						$out = $parent;
						$parent = $parent->getParent();
					}
					return $out;
				}
			}
		}
		return NULL;
	}
	
	/**
	 * Get sitemap
	 */
	public function getSitemap() {
		return '';
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set the title of the menu
	 * 
	 * @access public
	 * @param string $title
	 * @return string
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	/**
	 * Set the description of the menu
	 * 
	 * @access public
	 * @param string $description
	 * @return string
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
	}

	/**
	 * Add a menu item
	 * 
	 * Will add a menu item, that is to be contained by the menu.
	 * 
	 * @uses M_MenuItem::setParent()
	 * @access public
	 * @param M_MenuItem $item
	 * @return M_MenuItem
	 */
	public function addMenuItem(M_MenuItem $item) {
		$this->_items[] = $item;
		return $this;
	}
	
	/**
	 * Remove contained menu items, by path
	 * 
	 * Will remove all contained menu items that match the provided path
	 * 
	 * @access public
	 * @param string $path
	 * 		The path to look for
	 * @param bool $recursive
	 * 		Set to TRUE if you want to search recursively, FALSE if not
	 * @return void
	 */
	public function removeMenuItemByPath($path, $recursive = FALSE) {
		// for each of the contained menu items:
		foreach($this->_items as $i => $currentItem) {
			// If the current item's path matches the provided one:
			if($currentItem->isPath($path)) {
				// Remove the menu item
				unset($this->_items[$i]);
			}
			
			// If searching recursively:
			if($recursive) {
				// Remove contained matching menu items
				$currentItem->removeMenuItemByPath($path, TRUE);
			}
		}
	}
	
	/**
	 * Remove contained menu item
	 * 
	 * Will remove the specified menu item from the menu
	 * 
	 * @access public
	 * @param M_MenuItem $item
	 * 		The item to be removed
	 * @return void
	 */
	public function removeMenuItem(M_MenuItem $item) {
		// for each of the contained menu items:
		foreach($this->_items as $i => $currentItem) {
			// If the current item is the one we are looking for:
			if($currentItem == $item) {
				// Remove the menu item
				unset($this->_items[$i]);
			}
		}
	}
	
	/**
	 * Remove all contained menu items
	 * 
	 * @access public
	 * @return void
	 */
	public function removeMenuItems() {
		$this->_items = array();
	}
	
	
}


































/*
 * news/my-headline
 * 
 * if(M_MenuItem->isDataObject()) {
 * 		M_MenuItem->getDataObjectFromPath();
 * 		M_MenuItem->getPath();
 * 		M_MenuItem->getPathWithDataObjectValues();
 * 		M_MenuItem->getTitle();
 * }
 * 
 * 
 * 
 * M_MenuItem > M_MenuItem > ...
 * 
 * <menu>
 * 		<item>
 * 			<title>News</title>
 * 			<description>The latest headlines in my website</description>
 * 			<path>news</path>
 * 			<item>
 * 				<title>@title</title>
 * 				<description></description>
 * 				<path>news/@url</path>
 * 				<dataObject>
 * 					<module>news</module>
					<dataObjectId>category</dataObjectId>
 * 				</dataObject>
 * 				<item>
 * 					<title>@title</title>
 * 					<description></description>
 * 					<path>news/@url/@url</path>
	 * 				<dataObject>
	 * 					<module>news</module>
						<dataObjectId>category</dataObjectId>
 * 					</dataObject>
	 * 				<dataObject>
	 * 					<module>news</module>
						<dataObjectId>news</dataObjectId>
 * 					</dataObject>	
 * 				</item>
 * 			</item>
 * 		</item>
 * 		<item>
 * 			<title>Contact</title>
 * 			<description>Contact form</description>
 * 			<path>/contact</path>
 * 		</item>
 * </menu>
 * 
 * 
 * {foreach from=$menu->getMenuItems() item="menu"}
 * 	<a href="{$menu->getPath}" {if $menu->isActive()} class="active"{/if}>...
 * {/foreach}
 * 
 * To make site building easier?
 * {$persistent.activeMenuPath}
 * {foreach $persistent.breadcrumb}
 */