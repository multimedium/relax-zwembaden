<?php
/**
 * M_MenuHelper
 * 
 * M_MenuHelper provides with functions to set the active menu item in 
 * the application.
 * 
 * @package Core
 */
class M_MenuHelper extends M_Object {
	/**
	 * The active path
	 * 
	 * This property stores the active path. Typically, the active path is used
	 * to get the active {@link M_MenuItem} instance
	 * 
	 * @static
	 * @access private
	 * @var string
	 */
	private static $_activePath = NULL;
	
	/**
	 * Get active path
	 * 
	 * Will provide with the active path. Typically, the active path is used
	 * to select the active {@link M_MenuItem} instance.
	 * 
	 * NOTE:
	 * If no active path has been provided to {@link M_MenuHelper}, the path will
	 * be fetched as following:
	 * 
	 * <code>
	 *    M_Request::getUri()->getRelativePath()
	 * </code>
	 * 
	 * @static
	 * @access public
	 * @return string
	 */
	public static function getActivePath() {
		// If no path has been provided explicitely as being the active one,
		// we fetch the active path from the request by default:
		if(self::$_activePath == NULL) {
			// Get the relative path (including prefix that has been set in M_Request
			$rel = (string) M_Request::getUri()->getRelativePath();
			
			// Get the prefix from M_Request
			$prefix = M_Request::getLinkPrefix();
			$prefixLength = strlen($prefix);
			
			// Strip the prefix from relative path (if present)
			if(! strncmp($rel, $prefix, $prefixLength)) {
				$rel = (string) substr($rel, $prefixLength);
			}
			
			// clean up, and set as active path now:
			self::$_activePath = M_Helper::trimCharlist($rel, '/');
		}
		
		return self::$_activePath;
	}
	
	/**
	 * Set active path
	 * 
	 * Will set the active path. Typically, the active path is used to select 
	 * the active {@link M_MenuItem} instance.
	 * 
	 * @access public
	 * @param string $path
	 * @return void
	 */
	public static function setActivePath($path) {
		self::$_activePath = M_Helper::trimCharlist($path, '/');
	}
	
	/**
	 * Get active menu item
	 * 
	 * Will provide with an instance of {@link M_MenuItem} that represents the
	 * active/selected item in the website/application. To do so, it uses:
	 * 
	 * - {@link M_MenuHelper::getActivePath()}
	 * 
	 * To get the path of the currently active/selected menu item, and
	 * 
	 * - {@link M_Menu::getInstances()}
	 * - {@link M_Menu::getMenuItemActive()}
	 * 
	 * To loop the menus in the application, and ask each of them for the menu 
	 * item that matches the active path. The first match is returned.
	 * 
	 * @access public
	 * @return M_MenuItem
	 */
	public static function getMenuItemActive() {
		// For each of the menus:
		foreach(M_Menu::getInstances() as $menu) {
			// If the active menu item is in this menu:
			$active = $menu->getMenuItemActive();
			if($active) {
				// Return the menu item:
				return $active;
			}
		}
		
		// If still here, return NULL:
		return NULL;
	}
	
	/**
	 * Get active menu item IN ROOT OF A MENU
	 * 
	 * Will provide with an instance of {@link M_MenuItem} that represents the
	 * active/selected item in the website/application. To do so, it uses:
	 * 
	 * - {@link M_MenuHelper::getActivePath()}
	 * 
	 * To get the path of the currently active/selected menu item, and
	 * 
	 * - {@link M_Menu::getInstances()}
	 * - {@link M_Menu::getMenuItemActiveInRoot()}
	 * 
	 * To loop the menus in the application, and ask each of them for the menu 
	 * item that matches the active path. The first match is returned.
	 * 
	 * @access public
	 * @return M_MenuItem
	 */
	public static function getMenuItemActiveInRoot() {
		// For each of the menus:
		foreach(M_Menu::getInstances() as $menu) {
			// If the active menu item is in this menu:
			$active = $menu->getMenuItemActiveInRoot();
			if($active) {
				// Return the menu item:
				return $active;
			}
		}
		
		// If still here, return NULL:
		return NULL;
	}
}