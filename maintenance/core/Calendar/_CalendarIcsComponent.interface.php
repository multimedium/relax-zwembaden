<?php
interface MI_CalendarIcsComponent {
	
	/* -- GETTERS -- */
	
	public function getType();
	public function toString();
}