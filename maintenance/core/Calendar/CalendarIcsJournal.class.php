<?php
/**
 * M_CalendarIcsJournal
 * 
 * M_CalendarIcsJournal is a calendar component for {@link M_CalendarIcs}.
 * 
 * The body of the iCalendar object ({@link M_CalendarIcs}) is made up of a list 
 * of calendar properties and one or more "calendar components". The calendar 
 * properties apply to the entire calendar. The calendar components are several 
 * calendar properties which create a calendar schematic (design). 
 * 
 * For example, the calendar component can specify an event, a to-do list, a 
 * journal entry, time zone information, or free/busy time information, or 
 * an alarm. For more information about calendar components, read the information
 * at {@link http://en.wikipedia.org/wiki/ICalendar}
 * 
 * This specific class is used to represent a specific calendar component:
 * a journal.
 * 
 * @package Core
 */
class M_CalendarIcsJournal extends M_CalendarIcsComponentAbstract {
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @uses M_CalendarIcsComponentAbstract::_setType()
	 * @access public
	 * @return M_CalendarIcsAlarm
	 */
	public function __construct() {
		$this->_setType('journal');
	}
}