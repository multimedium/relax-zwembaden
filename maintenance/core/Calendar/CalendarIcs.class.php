<?php
/**
 * M_CalendarIcs class
 * 
 * iCalendar (ICS) is a computer file format which allows internet users to send 
 * meeting requests and tasks to other internet users, via email, or sharing files 
 * with an .ics extension. 
 * 
 * Recipients of the iCalendar data file (with supporting software, such as an 
 * email client or calendar application) can respond to the sender easily or counter
 * propose another meeting date/time.
 * 
 * @package Core
 */
class M_CalendarIcs {
	
	/* -- PROPERTIES -- */
	
	/**
	 * The name of the calendar
	 * 
	 * This property stores the name of the calendar
	 * 
	 * @see M_CalendarIcs::setName()
	 * @access private
	 * @var string
	 */
	private $_name;
	
	/**
	 * The description of the calendar
	 * 
	 * This property stores the description of the calendar
	 * 
	 * @see M_CalendarIcs::setDescription()
	 * @access private
	 * @var string
	 */
	private $_description;
	
	/**
	 * The color of the calendar
	 * 
	 * @see M_CalendarIcs::setColor()
	 * @access private
	 * @var M_Color
	 */
	private $_color;
	
	/**
	 * The Product ID of the calendar
	 * 
	 * @see M_CalendarIcs::setProductId()
	 * @access private
	 * @var array
	 */
	private $_productId;
	
	/**
	 * ICS Components
	 * 
	 * This property stores the collection of ICS components in the calendar. The
	 * components are represented by instances that implement the interface
	 * {@link MI_CalendarIcsComponent}
	 * 
	 * @see M_CalendarIcs::addComponent()
	 * @see M_CalendarIcs::addEvent()
	 * @see M_CalendarIcs::addTodo()
	 * @see M_CalendarIcs::addJournal()
	 * @see M_CalendarIcs::addAlarm()
	 * @access public
	 * @var array
	 */
	private $_components = array();
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_CalendarIcs
	 */
	public function __construct() {
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set name
	 * 
	 * This method can be used to set the name of the calendar. Typically, this
	 * name will be used (as a default) by the Calendar application that is used
	 * by the visitor (eg. Apple iCal)
	 * 
	 * @access public
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->_name = (string) $name;
	}
	
	/**
	 * Set description
	 * 
	 * This method can be used to set the description of the calendar. Typically, 
	 * this name will be used (as a default) by the Calendar application that is 
	 * used by the visitor (eg. Apple iCal)
	 * 
	 * @access public
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->_description = (string) $description;
	}
	
	/**
	 * Set color
	 * 
	 * This method can be used to set the color of the calendar. This color may
	 * then be adapted by the visitor's Calendar application (eg. Apple iCal)
	 * 
	 * @access public
	 * @param M_Color $color
	 * @return void
	 */
	public function setColor(M_Color $color) {
		$this->_color = $color;
	}
	
	/**
	 * Set Product ID
	 * 
	 * This method can be used to set the Product ID in the calendar file. Typically,
	 * this is used to set the name of the application that has exported the
	 * ICS File.
	 * 
	 * @access public
	 * @param string $companyName
	 * @param string $productName
	 * @return void
	 */
	public function setProductId($companyName, $productName) {
		$this->_productId = array($companyName, $productName);
	}
	
	/**
	 * Add component to the calendar
	 * 
	 * This method can be used to add a component to the calendar. The component
	 * is represented by an instance that implements {@link MI_CalenderIcsComponent}
	 * 
	 * @see M_CalendarIcs::addEvent()
	 * @see M_CalendarIcs::addTodo()
	 * @see M_CalendarIcs::addJournal()
	 * @see M_CalendarIcs::addAlarm()
	 * @access public
	 * @param MI_CalendarIcsComponent $component
	 * @return void
	 */
	public function addComponent(MI_CalendarIcsComponent $component) {
		$this->_components[] = $component;
	}
	
	/**
	 * Add event to the calendar
	 * 
	 * @uses M_CalendarIcs::addComponent()
	 * @access public
	 * @param M_CalendarIcsEvent $event
	 * @return void
	 */
	public function addEvent(M_CalendarIcsEvent $event) {
		$this->addComponent($event);
	}
	
	/**
	 * Add to-do to the calendar
	 * 
	 * @uses M_CalendarIcs::addComponent()
	 * @access public
	 * @param M_CalendarIcsTodo $todo
	 * @return void
	 */
	public function addTodo(M_CalendarIcsTodo $todo) {
		$this->addComponent($todo);
	}
	
	/**
	 * Add journal to the calendar
	 * 
	 * @uses M_CalendarIcs::addComponent()
	 * @access public
	 * @param M_CalendarIcsJournal $journal
	 * @return void
	 */
	public function addJournal(M_CalendarIcsJournal $journal) {
		$this->addComponent($journal);
	}
	
	/**
	 * Add alarm to the calendar
	 * 
	 * @uses M_CalendarIcs::addComponent()
	 * @access public
	 * @param M_CalendarIcsAlarm $alarm
	 * @return void
	 */
	public function addAlarm(M_CalendarIcsAlarm $alarm) {
		$this->addComponent($alarm);
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get name
	 * 
	 * This method will provide with the name of the calendar, which may have been
	 * set previously with {@link M_CalendarIcs::setName()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Get description
	 * 
	 * This method will provide with the description of the calendar, which may 
	 * have been set previously with {@link M_CalendarIcs::setDescription()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 * Get color
	 * 
	 * This method will provide with the color of the calendar, which may 
	 * have been set previously with {@link M_CalendarIcs::setColor()}.
	 * 
	 * @access public
	 * @return string
	 */
	public function getColor() {
		return $this->_color;
	}
	
	/**
	 * Get Product ID: Company
	 * 
	 * This method will provide with the Product ID, but will provide only with 
	 * the name of the company.
	 * 
	 * @access public
	 * @return string
	 */
	public function getProductIdCompany() {
		return ($this->_productId
			? $this->_productId[0]
			: NULL
		);
	}
	
	/**
	 * Get Product ID: Product Name
	 * 
	 * This method will provide with the Product ID, but will provide only with 
	 * the name of the product.
	 * 
	 * @access public
	 * @return string
	 */
	public function getProductIdProductName() {
		return ($this->_productId
			? $this->_productId[1]
			: NULL
		);
	}
	
	/**
	 * Get components
	 * 
	 * Will provide with the collection of components that may have been added
	 * previously to the calendar, with
	 * 
	 * - {@link M_CalendarIcs::addComponent()}
	 * - {@link M_CalendarIcs::addEvent()}
	 * - {@link M_CalendarIcs::addTodo()}
	 * - {@link M_CalendarIcs::addJournal()}
	 * 
	 * @access public
	 * @return ArrayIterator $components
	 * 		The collection of instances that implement {@link MI_CalendarIcsComponent}
	 */
	public function getComponents() {
		return new ArrayIterator($this->_components);
	}
	
	/**
	 * Export to string
	 * 
	 * This method will export the source code of the .ICS file.
	 * 
	 * @access public
	 * @return string
	 */
	public function toString() {
		// New lines in the source of the ICS Component:
		$nl = M_CalendarIcsSourceHelper::getNewLine();
		
		// Start the output:
		$out  = M_CalendarIcsSourceHelper::getBeginTag('VCALENDAR');
		$out .= $nl;
		
		// Indicate the version with which we comply in this ICS File
		$out .= M_CalendarIcsSourceHelper::getSelfClosingTag('VERSION', '2.0');
		$out .= $nl;
		
		// We add information about the timezone to the calendar:
		// (if a default timezone is available)
		if(M_DateTimezone::isDefaultTimezoneAvailable()) {
			// Add the timezone to the source
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-WR-TIMEZONE', 
				M_CalendarIcsSourceHelper::getStringValue(
					M_DateTimezone::getDefaultTimezone()
				)
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// (For now) We use the Gregorian Calendar
		$out .= M_CalendarIcsSourceHelper::getSelfClosingTag('CALSCALE', 'GREGORIAN');
		$out .= $nl;
		
		// Check if the calendar has been named:
		if($this->_name) {
			// If so, add the name:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-WR-CALNAME', 
				M_CalendarIcsSourceHelper::getStringValue($this->_name)
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// Check if the calendar has been given a description:
		if($this->_description) {
			// If so, add the description:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-WR-CALDESC', 
				M_CalendarIcsSourceHelper::getStringValue($this->_description)
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// Check if a color has been assigned to the calendar
		if($this->_color) {
			// If so, add the color value:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'X-APPLE-CALENDAR-COLOR', 
				$this->_color->getHex(TRUE)
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// Check if the Product ID has been defined:
		if($this->_productId) {
			// If so, add the Product ID:
			$out .= M_CalendarIcsSourceHelper::getSelfClosingTag(
				'PRODID', 
				M_CalendarIcsSourceHelper::getStringValue(
					'-//' . $this->_productId[0] . '//' . $this->_productId[1] . '//' . strtoupper((string) M_Locale::getCategory(M_Locale::LANG))
				)
			);
			
			// Add a new line
			$out .= $nl;
		}
		
		// For each of the components:
		foreach($this->_components as $component) {
			// Add the current component's source code to the ICS Source code
			$out .= $component->toString();
			
			// Add a new line
			$out .= $nl;
		}
		
		// Finish the output
		$out .= M_CalendarIcsSourceHelper::getEndTag('VCALENDAR');
		
		// Return the final render:
		return $out;
	}
}