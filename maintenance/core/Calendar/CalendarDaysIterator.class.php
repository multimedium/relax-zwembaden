<?php
/**
 * M_CalendarDaysIterator class
 * 
 * Used to loop through the days in a {@link M_Calendar}. Typically,
 * the iterator is constructed with the {@link M_Calendar::getDaysIterator()}
 * method.
 * 
 * @package Core
 */
class M_CalendarDaysIterator implements Countable, Iterator {
	/**
	 * Count of days in the calendar
	 * 
	 * @access private
	 * @var integer
	 */
	private $_count;
	
	/**
	 * The cursor position in the iterator
	 * 
	 * @access private
	 * @var integer
	 */
	private $_index;
	
	/**
	 * The calendar on which the iterator is mounted
	 * 
	 * @access private
	 * @var M_Calendar
	 */
	private $_calendar;
	
	/**
	 * The first day of the collection
	 * 
	 * @see M_Calendar::getFirstDay()
	 * @access private
	 * @var M_Date
	 */
	private $_firstDay;
	
	/**
	 * The last day of the collection
	 * 
	 * @see M_Calendar::getLastDay()
	 * @access private
	 * @var M_Date
	 */
	private $_lastDay;
	
	/**
	 * The date object
	 * 
	 * ...as returned by the {@link M_CalendarDaysIterator::current()} method.
	 * 
	 * @access private
	 * @var M_Date
	 */
	private $_date;
	
	/**
	 * The first day of the week
	 * 
	 * @see M_CalendarDaysIterator::setFirstWeekday()
	 * @access private
	 * @var integer
	 */
	private $_firstDayOfWeek;
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param M_Calendar $calendar
	 * 		The calendar on which an iterator is being constructed
	 * @return M_CalendarDaysIterator
	 */
	public function __construct(M_Calendar $calendar) {
		$this->_count          = $calendar->getNumberOfDays();
		$this->_index          = 0;
		$this->_calendar       = $calendar;
		$this->_firstDay       = $calendar->getFirstDay();
		$this->_lastDay        = $calendar->getLastDay();
		$this->_date           = $calendar->getFirstDay();
		$this->_firstDayOfWeek = 1; // Default to monday as first day of week
	}
	
	/* -- ITERATOR, COUNTABLE INTERFACE -- */
	
	/**
	 * Support for the Countable interface
	 *
	 * @access public
	 * @return integer
	 * 		The item count
	 */
	public function count() {
		return $this->_count;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return M_Date $date
	 * 		The current date object
	 */
	public function current() {
		return $this->_date;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return string
	 * 		The key of the current date
	 */
	public function key() {
		return $this->_date->timestamp;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Moves the internal cursor to the next item in stored data
	 * 
	 * @access public
	 * @return void
	 */
	public function next() {
		// In order to prevent Daylight Saving Time from messing with the days in 
		// our calendar, we need to move differently through the days then by 
		// simply adding time to the date.
		// $this->_date->addTime(1, M_Date::DAY);
		$this->_date->setToDayAfterWithSameTime();
		$this->_index += 1;
	}
	
	/**
	 * Support for the Iterator interface
	 * 
	 * Resets the internal cursor for iteration. foreach() calls on
	 * an instance of the Registry class will call the rewind()
	 * method, to start looping at the beginning.
	 *
	 * @access public
	 * @return void
	 */
	public function rewind() {
		$this->_date = $this->_firstDay;
		$this->_index = 0;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Will indicate if the end of the iterator has been reached.
	 * 
	 * @return boolean
	 * 		Will return TRUE if not reached the end of the iterator,
	 * 		or FALSE if all items in the iterator have been accessed
	 * 		in the loop.
	 */
	public function valid() {
		return $this->_index < $this->_count;
	}
	
	/* -- ADDITIONAL ITERATOR MESSAGES -- */
	
	/**
	 * Set first day of the week
	 * 
	 * Will set the weekday index number that is to be used as first day of the
	 * week. For example, you could define monday as the first day of the week
	 * as following:
	 * 
	 * <code>
	 *    // Construct the iterator
	 *    $days = new M_CalendarDaysIterator(new M_Calendar(2009, 09));
	 *    
	 *    // Set monday as first day of the week
	 *    $days->setFirstWeekday(M_Date::MONDAY);
	 * </code>
	 * 
	 * @access public
	 * @param integer $weekdayIndex
	 * @return void
	 */
	public function setFirstWeekday($weekdayIndex) {
		// If the weekday index number is valid
		if($weekdayIndex >= 0 && $weekdayIndex < 7) {
			$this->_firstDayOfWeek = (int) $weekdayIndex;
		}
		// if not, throw an exception
		else {
			throw new M_CalendarException(sprintf(
				'Cannot set %s as first day of the week; Can only except from 0 (Sunday) to 6 (Saturday)',
				$weekdayIndex
			));
		}
	}

	/**
	 * Add days, to achieve complete weeks
	 * 
	 * It is very possible that the iterator of days of a given calendar starts
	 * in the middle of a week. For example, assume we want to ask the calendar of 
	 * January 2009 for the collection of days:
	 * 
	 * Example 1
	 * <code>
	 *    // Mount the calendar of January 2009
	 *    $cal = new M_Calendar(2009, 1);
	 * 
	 *    // For each the days in the calendar
	 *    foreach($cal->getDaysIterator() as $day) {
	 *       // Display the current day in the calendar:
	 *       echo $day->toString(M_Date::FULL) . '<br>';
	 *    }
	 * </code>
	 * 
	 * You will notice that the example above will output the following result:
	 * 
	 * <code>
	 *    donderdag 1 januari 2009
	 *    vrijdag 2 januari 2009
	 *    zaterdag 3 januari 2009
	 *    zondag 4 januari 2009
	 *    maandag 5 januari 2009
	 *    dinsdag 6 januari 2009
	 *    woensdag 7 januari 2009
	 *    donderdag 8 januari 2009
	 *    ...
	 * </code>
	 * 
	 * Note that, in our example, the collection of days starts with a thursday.
	 * However, if we want the collection to start at the beginning of the first
	 * week, we could do the following:
	 * 
	 * Example 2 (Rewritten Example 1)
	 * <code>
	 *    // Mount the calendar of January 2009
	 *    $cal = new M_Calendar(2009, 1);
	 *    
	 *    // Get the days in the calendar:
	 *    $days = $cal->getDaysIterator();
	 *    
	 *    // Add days, to have only complete weeks
	 *    // (starting at the beginning of first week, and ending at last day
	 *    // of the last week)
	 *    $days->addDaysForCompleteWeeks(); 
	 * 
	 *    // For each the days in the calendar
	 *    foreach($days as $day) {
	 *       // Display the current day in the calendar:
	 *       echo $day->toString(M_Date::FULL) . '<br>';
	 *    }
	 * </code>
	 * 
	 * Now, the example will output the following result:
	 * (Assuming that monday has been configured as the first day of the week;
	 * see {@link M_CalendarDaysIterator::setFirstWeekday()})
	 * 
	 * <code>
	 *    maandag 29 december 2009
	 *    dinsdag 30 december 2009
	 *    woensdag 31 december 2009
	 *    donderdag 1 januari 2009
	 *    vrijdag 2 januari 2009
	 *    zaterdag 3 januari 2009
	 *    zondag 4 januari 2009
	 *    maandag 5 januari 2009
	 *    dinsdag 6 januari 2009
	 *    woensdag 7 januari 2009
	 *    donderdag 8 januari 2009
	 *    ...
	 * </code>
	 * 
	 * In other words, this methods adds days to the collection, so that all of
	 * the weeks in the calendar are complete: starting at the first day of the
	 * week, and ending on the last day of the week.
	 * 
	 * Typically, this will add more days to the beginnging of the calendar and 
	 * to the end of the calendar, in order to "complete" the weeks in the calendar.
	 * 
	 * @see M_CalendarDaysIterator::setFirstWeekday()
	 * @access public
	 * @return void
	 */
	public function addDaysForCompleteWeeks() {
		// Get the first day in the collection
		$firstDay = $this->_calendar->getFirstDay();
		
		// Until this day is not the first day of the week:
		while($firstDay->getWeekday() != $this->_firstDayOfWeek) {
			// We keep going back a day:
			$firstDay->subtractTime(1, M_Date::DAY);
			
			// Also, we update the internals of the iterator:
			$this->_count += 1;
		}
		
		// Finally, Set the first day in the collection:
		$this->_firstDay = $firstDay;
		
		// In order to compare the current day with the last day of the week, 
		// we need to subtract 1 day from the first day of the week:
		$lastDayOfWeek = $this->_firstDayOfWeek - 1;
		
		// Note that, if the first day is defined as sunday, the last day will 
		// now be calculcated as being -1. In that case, we set the last day
		// as 6 (Saturday), in order to cycle through the days of a week.
		if($lastDayOfWeek < 0) {
			$lastDayOfWeek = 6;
		}
		
		// Get the last day in the collection
		$lastDay = $this->_calendar->getLastDay();
		
		// Until this day is not the last day of the week:
		while($lastDay->getWeekday() != $lastDayOfWeek) {
			// We keep going forward a day:
			$lastDay->addTime(1, M_Date::DAY);
			
			// Also, we update the internals of the iterator:
			$this->_count += 1;
		}
		
		// Finally, Set the last day in the collection:
		$this->_lastDay = $lastDay;
	}

	/**
	 * Get first day in the iterator
	 *
	 * @access public
	 * @return M_Date
	 */
	public function getFirstDay() {
		return $this->_firstDay;
	}
	
	/**
	 * Is current date first in collection?
	 * 
	 * Will tell whether or not the current date in the collection, as provided
	 * by {@link M_CalendarDaysIterator::current()}, is the first day in the 
	 * collection of days. Returns TRUE if so, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isCurrentFirstDay() {
		return ($this->_date->equals($this->_firstDay, M_Date::DAY));
	}

	/**
	 * Get last day
	 *
	 * @access public
	 * @return M_Date
	 */
	public function getLastDay() {
		return $this->_lastDay;
	}
	
	/**
	 * Is current date last in collection?
	 * 
	 * Will tell whether or not the current date in the collection, as provided
	 * by {@link M_CalendarDaysIterator::current()}, is the last day in the 
	 * collection of days. Returns TRUE if so, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isCurrentLastDay() {
		return ($this->_date->equals($this->_lastDay, M_Date::DAY));
	}
	
	/**
	 * Is current date in collection first day of week
	 * 
	 * Will tell whether or not the current date in the collection, as provided
	 * by {@link M_CalendarDaysIterator::current()}, is the first day of a week.
	 * Returns TRUE if so, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isCurrentFirstDayOfWeek() {
		return ($this->_date->getWeekday() == $this->_firstDayOfWeek);
	}
	
	/**
	 * Is current date in collection first day of week
	 * 
	 * Will tell whether or not the current date in the collection, as provided
	 * by {@link M_CalendarDaysIterator::current()}, is the first day of a week.
	 * Returns TRUE if so, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isCurrentLastDayOfWeek() {
		// In order to compare the current day with the last day of the week, 
		// we need to subtract 1 day from the first day of the week:
		$lastDayOfWeek = $this->_firstDayOfWeek - 1;
		
		// Note that, if the first day is defined as sunday, the last day will 
		// now be calculcated as being -1. In that case, we set the last day
		// as 6 (Saturday), in order to cycle through the days of a week.
		if($lastDayOfWeek < 0) {
			$lastDayOfWeek = 6;
		}
		
		// Check if the current day is the last of the week
		return ($this->_date->getWeekday() == $lastDayOfWeek);
	}
	
	/**
	 * Is current date the first in the month?
	 * 
	 * Will tell whether or not the current date in the collection, as provided
	 * by {@link M_CalendarDaysIterator::current()}, is the first day of the month.
	 * Returns TRUE if so, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isCurrentFirstDayOfMonth() {
		return ($this->_date->getDay() == 1);
	}
	
	/**
	 * Is current date the last in the month?
	 * 
	 * Will tell whether or not the current date in the collection, as provided
	 * by {@link M_CalendarDaysIterator::current()}, is the last day of the month.
	 * Returns TRUE if so, FALSE if not.
	 * 
	 * @access public
	 * @return boolean
	 */
	public function isCurrentLastDayOfMonth() {
		// Mount a calendar, to represent the month of the day we are evaluating
		$cal = new M_Calendar($this->_date->getYear(), $this->_date->getMonth());
		
		// Check if the date is the last day of its month
		return ($this->_date->getDay() == $cal->getNumberOfDays());
	}

	/**
	 * Get the current week iteration
	 *
	 * ... relative to the iterator's collection of weeks, starting from 0 (ZERO)
	 *
	 * @access public
	 * @return integer
	 */
	public function getIterationOfWeek() {
		return ($this->current()->getWeekNumber() - $this->_calendar->getFirstDay()->getWeekNumber());
	}

	/**
	 * Get the current day-of-week iteration, starting from 0 (ZERO)
	 *
	 * This method allows you to get the iteration of the current day of the week.
	 * Assume that we want to print the second tuesday of the month, then we
	 * could use the following code:
	 *
	 * <code>
	 *    // Get an iterator that allows us to iterate through the days of the
	 *    // current month:
	 *    $calendar = new M_Calendar(2010, 10);
	 *    $days     = $calendar->getDaysIterator();
	 *
	 *    // For each of the days in the calendar:
	 *    foreach($days as $day) {
	 *       // If the current date is a tuesday, and if this is the second tuesday
	 *       // we have encountered in the collection:
	 *       if($day->isTuesday() && $days->getIterationOfWeekday() == 1) {
	 *          // Print the date:
	 *          echo $day;
	 *          break;
	 *       }
	 *    }
	 * </code>
	 *
	 * @uses M_CalendarDaysIterator::getIterationOfWeek()
	 * @access public
	 * @return integer
	 */
	public function getIterationOfWeekday() {
		// If the week-day-number of the first day in the iterator is bigger than
		// the current one:
		if($this->_calendar->getFirstDay()->getWeekday() > $this->_date->getWeekday()) {
			// Then, we return the week number, minus 1
			return ($this->getIterationOfWeek() - 1);
		}

		// If still here, return the week number
		return $this->getIterationOfWeek();
	}
}