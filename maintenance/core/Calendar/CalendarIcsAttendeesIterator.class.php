<?php
/**
 * M_CalendarIcsAttendeesIterator class
 * 
 * Used to loop through the attendees of an event/meeting or to-do item. For more
 * info, read {@link M_CalendarIcsEvent::getAttendees()}.
 * 
 * @package Core
 */
class M_CalendarIcsAttendeesIterator implements Countable, Iterator {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Count of attendees
	 * 
	 * @access private
	 * @var integer
	 */
	private $_count = 0;
	
	/**
	 * The cursor position in the iterator
	 * 
	 * @access private
	 * @var integer
	 */
	private $_index = 0;
	
	/**
	 * The attendees
	 * 
	 * @access private
	 * @var array
	 */
	private $_attendees = array();
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return M_CalendarIcsAttendeesIterator
	 */
	public function __construct() {
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Support for the Countable interface
	 *
	 * @access public
	 * @return integer
	 * 		The item count
	 */
	public function count() {
		return $this->_count;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return M_Contact $attendee
	 * 		The current attendee in the collection
	 */
	public function current() {
		return $this->_attendees[$this->_index][0];
	}
	
	/**
	 * Get attendee's status
	 * 
	 * @access public
	 * @return string $status
	 * 		The status of the current attendee in the collection. For more info,
	 * 		read {@link M_CalendarIcsAttendeesIterator::addAttendee()}
	 */
	public function currentStatus() {
		return $this->_attendees[$this->_index][1];
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * @access public
	 * @return string
	 * 		The key of the current attendee in the collection
	 */
	public function key() {
		return $this->_index;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Moves the internal cursor to the next item in stored data
	 * 
	 * @access public
	 * @return void
	 */
	public function next() {
		$this->_index += 1;
	}
	
	/**
	 * Support for the Iterator interface
	 * 
	 * Resets the internal cursor for iteration. foreach() calls on
	 * an instance of the Registry class will call the rewind()
	 * method, to start looping at the beginning.
	 *
	 * @access public
	 * @return void
	 */
	public function rewind() {
		$this->_index = 0;
	}
	
	/**
	 * Support for the Iterator interface
	 *
	 * Will indicate if the end of the iterator has been reached.
	 * 
	 * @access public
	 * @return boolean
	 * 		Will return TRUE if not reached the end of the iterator,
	 * 		or FALSE if all items in the iterator have been accessed
	 * 		in the loop.
	 */
	public function valid() {
		return $this->_index < $this->_count;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Add attendee to the iterator
	 * 
	 * This method can be used to add an attendee to the collection. The info
	 * about the attendee is contained by an instance of {@link M_Contact}. 
	 * 
	 * Also, you can specify the status of the attendee, in the event/meeting.
	 * Read {@link M_CalendarIcsEvent::addAttendee()} for more info
	 * 
	 * @access public
	 * @param M_Contact $attendee
	 * 		The information about the attendee
	 * @param string $status
	 * 		The status of the attendee
	 * @return void
	 */
	public function addAttendee(M_Contact $attendee, $status = NULL) {
		$this->_attendees[] = array($attendee, $status);
		$this->_count += 1;
	}
}