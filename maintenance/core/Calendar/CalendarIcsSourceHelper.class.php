<?php
/**
 * M_CalendarIcsSourceHelper
 * 
 * This class is used by the iCalendar, {@link M_CalendarIcs}, and the calendar
 * components, in order to render the source (code). The helper functions in this
 * class are used intensively in:
 * 
 * - {@link M_CalendarIcs::toString()}
 * - {@link M_CalendarIcsAlarm::toString()}
 * - {@link M_CalendarIcsEvent::toString()}
 * - {@link M_CalendarIcsTodo::toString()}
 * - {@link M_CalendarIcsJournal::toString()}
 * 
 * @package Core
 */
class M_CalendarIcsSourceHelper {
	
	/* -- GETTERS -- */
	
	/**
	 * Get new line
	 * 
	 * Provides with the new-line character(s) that should be used in the source 
	 * code, to separate lines from each other.
	 * 
	 * @static
	 * @access public
	 * @return string
	 */
	public static function getNewLine() {
		return "\r\n";
	}
	
	/**
	 * Get begin tag
	 * 
	 * Provides with a formatted string, in order to open a given tag. This is used
	 * to open a tag that in turn contains other tags with data.
	 * 
	 * @static
	 * @see M_CalendarIcsSourceHelper::getEndTag()
	 * @access public
	 * @param string $name
	 * 		The name of the tag to be opened
	 * @return string
	 */
	public static function getBeginTag($name) {
		return 'BEGIN:' . (string) $name;
	}
	
	/**
	 * Get end tag
	 * 
	 * Provides with a formatted string, in order to end a given tag. This is used
	 * to close a tag that has been opened previously with the other helper function
	 * {@link M_CalendarIcsSourceHelper::getBeginTag()}
	 * 
	 * @static
	 * @see M_CalendarIcsSourceHelper::getBeginTag()
	 * @access public
	 * @param string $name
	 * 		The name of the tag to be opened
	 * @return string
	 */
	public static function getEndTag($name) {
		return 'END:' . (string) $name;
	}
	
	/**
	 * Get self-closing tag
	 * 
	 * Provides with a formatted string, in order to render a self-closing tag 
	 * with data. Unlike tags that
	 * 
	 * - are opened with {@link M_CalendarIcsSourceHelper::getBeginTag()},
	 * - then are closed with {@link M_CalendarIcsSourceHelper::getEndTag()}
	 * 
	 * this tag does not require you to close/end it; it is self-closing. You
	 * can compare it to self-closing tags in HTML; eg: <img ... />
	 * 
	 * @static
	 * @access public
	 * @param string $name
	 * 		The name of the self-closing tag
	 * @param string $value
	 * 		The value (contained data) of the tag
	 * @param array $attributes
	 * 		An associative array, with attributes of the tag
	 * @return string
	 */
	public static function getSelfClosingTag($name, $value, array $attributes = array()) {
		// The tag name:
		$out = (string) $name;
		
		// For each of the attributes:
		foreach($attributes as $currentName => $currentValue) {
			// Add the attribute to the tag
			$out .= ';' . (string) $currentName . '=' . $currentValue;
		}
		
		// Add the value:
		$out .= ':' . (string) $value;
		
		// Return the source:
		return $out;
	}
	
	/**
	 * Get string value
	 * 
	 * Provides with a formatted string, in order to use it as data in a given
	 * tag. Typically, this helper function is used to include large strings in
	 * the ICS Source code.
	 * 
	 * This method will escape special characters in the string, such as new-lines,
	 * tabs, etc...
	 * 
	 * @static
	 * @access public
	 * @param string $string
	 * 		The string to be formatted for use in ICS Source Code
	 * @return string
	 */
	public static function getStringValue($string) {
		$temp = str_replace("\r", '', (string) $string);
		$temp = str_replace("\n", '\n', $temp);
		$temp = str_replace("\t", '\t', $temp);
		return $temp;
	}
	
	/**
	 * Get date string
	 * 
	 * Provides with a formatted string representation of a given date, in order
	 * to use it as data in a given tag. Typically, this helper function is used
	 * to include dates in the ICS Source Code.
	 * 
	 * @static 
	 * @access public
	 * @param M_Date $date
	 * 		The date to be formatted for use in ICS Source Code
	 * @return string
	 */
	public static function getDateString(M_Date $date) {
		return date('Ymd\THis\Z', $date->getTimestamp());
	}
	
	/**
	 * Get duration string
	 * 
	 * This method will accept a given number. It will interpret this number as
	 * a duration in time (more specifically: as the number of seconds). It will
	 * then format a string that expresses this duration in ICS Source Code.
	 * 
	 * The following is an example of a duration that specifies an interval of 
	 * time of 1 hour and zero minutes and zero seconds
	 * 
	 * <code>DURATION:PT1H0M0S</code>
	 * 
	 * The following is an example of this property that specifies an interval 
	 * of time of 15 minutes.
	 * 
	 * <code>DURATION:PT15M</code>
	 * 
	 * @static
	 * @uses M_Date::getTimeArray()
	 * @access public
	 * @param integer $numberOfSeconds
	 * 		The number of seconds to be formatted for use in ICS Source Code
	 * @return string
	 */
	public static function getDurationString($numberOfSeconds) {
		// We have been provided a number of seconds. To express the duration
		// correctly, we have to explode the duration of time into an array of 
		// time blocks (hours, minutes, and seconds). We ask M_Date to provide
		// us with a time array:
		$array = M_Date::getTimeArray($numberOfSeconds, M_Date::SECOND);
		
		// Now, we have blocks of time, and we simply output it into the right
		// format:
		$out = 'PT';

		// If hours are included:
		if($array['hours']) {
			$out .= $array['hours'] . 'H';
		}

		// If minutes are included:
		if($array['minutes']) {
			$out .= $array['minutes'] . 'M';
		}

		// If seconds are included:
		if($array['seconds']) {
			$out .= $array['seconds'] . 'S';
		}
		
		// Return the final duration syntax
		return $out;
	}
}