<?php
	$server = $_SERVER['HTTP_HOST'];
	$server = str_replace('http://', '', $server);
	$server = str_replace('https://', '', $server);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $server; ?> - onderhoud</title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="nl" />

		<style type="text/css">
			/*
				Copyright (c) 2009, Yahoo! Inc. All rights reserved.
				Code licensed under the BSD License:
				http://developer.yahoo.net/yui/license.txt
				version: 2.7.0
				*/
			html{color:#000;background:#FFF;}body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,button,textarea,p,blockquote,th,td{margin:0;padding:0;}table{border-collapse:collapse;border-spacing:0;}fieldset,img{border:0;}address,caption,cite,code,dfn,em,strong,th,var,optgroup{font-style:inherit;font-weight:inherit;}del,ins{text-decoration:none;}li{list-style:none;}caption,th{text-align:left;}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}q:before,q:after{content:'';}abbr,acronym{border:0;font-variant:normal;}sup{vertical-align:baseline;}sub{vertical-align:baseline;}legend{color:#000;}input,button,textarea,select,optgroup,option{font-family:inherit;font-size:inherit;font-style:inherit;font-weight:inherit;}input,button,textarea,select{*font-size:100%;}

			body {
				font-family: Arial, Helvetica, sans-serif;
			}

			strong {
				font-weight:600;
			}
			
			#wrapper {
				font-size:14px;
				margin:0px auto;
				margin-top:30px;
				width:500px;
				padding:20px;
			}

			#container {
				padding:20px;
				border:1px solid #CCC;
				background:#f6f6f6;
			}

			h2 {
				font-size:22px;
				padding-bottom:15px;
				text-transform: lowercase;
			}

			h1 {
				font-size:30px;
				color:#CCC;
			}
	   </style>
	</head>

	<body>
		<div id="wrapper">
			<h1><?php echo $server;	?></h1>
			<div id="container">
				<h2>Even geduld aub...</h2>

				<p>
					Momenteel is onze website in <strong>onderhoud</strong>, binnen <strong>enkele ogenblikken</strong> zijn we terug online.<br /><br />

					Onze excuses voor het eventuele ongemak.<br /><br />

					Met vriendelijke groeten,<br />
					<?php echo $server; ?>
				</p>
			</div>
		</div>
	</body>
</html>