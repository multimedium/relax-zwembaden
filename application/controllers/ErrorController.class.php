<?php
/**
 * ErrorController
 *
 * @package App
 */
class ErrorController extends M_Controller {
	/**
	 * Error 404
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		// send 404 HTTP status
		M_Header::send404();

		// Load the Page404View, and display:
		M_Loader::getView('Page404View')->display();

		// Stop running the app
		die();
	}
}