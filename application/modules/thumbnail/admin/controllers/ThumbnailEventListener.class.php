<?php
/**
 * ThumbnailEventListener
 *
 * The ThumbnailEventListener listens to events in the admin module. The event
 * listener is used to allow direct access to image manipulations in the admin
 * module. For more info, read {@link ThumbnailModel::setDirectAccess()}
 * 
 * @package App
 * @subpackge Thumbnail
 */
class ThumbnailEventListener {
	/**
	 * Constructor
	 * 
	 * @author Tom Bauwens
	 * @access public
	 * @return ThumbnailEventListener
	 */
	public function __construct() {
		// Load the ThumbnailModel class
		M_Loader::loadModel('ThumbnailModel', 'thumbnail');
		
		// Allow direct access to image manipulations
		ThumbnailModel::getInstance()->setDirectAccess(TRUE);
	}
}