<?php
M_Loader::loadModel('ThumbnailModel', 'thumbnail');

/**
 * ThumbnailController
 *
 * @package App
 * @subpackage Thumbnail
 */
class ThumbnailController extends M_Controller {

	/**
	 * @var ThumbnailDefinition
	 */
	private $_thumbnailDefinition;

	/**
	 * Get the {@link ThumbnailDefinition} which will be used to create a thumbnail
	 * out of an image
	 *
	 * @param string $definition The id of the definition
	 * @return ThumbnailDefinition
	 */
	protected function _getThumbnailDefinition($definition = null) {
		M_Loader::loadModel('ThumbnailDefinition','thumbnail');
		if (is_null($this->_thumbnailDefinition)) {
			$this->_thumbnailDefinition = new ThumbnailDefinition($definition);
		}
		return $this->_thumbnailDefinition;
	}
	
	/**
	 * Route requests
	 *
	 * @return array
	 * @author b.brughmans
	 */
	public function __router() {
		return array(
			'resized/(:any)' => 'resized/$1',
			'square/(:any)' => 'square/$1',
			'cropped/(:any)' => 'cropped/$1',
			'resizedForced/(:any)' => 'resizedForced/$1',
			'(:any)' => 'index/$1'
		);
	}
	
	/**
	 * Image-file which will be used as overlay
	 *
	 * @var M_Image
	 */
	protected $_overlayImage;

	/**
	 * X-position which will be used as the X-coordinate of the start of the overlay
	 *
	 * @var M_Image
	 */
	protected $_overlayX;
	
	/**
	 * Y-position which will be used as the Y-coordinate of the start of the overlay
	 *
	 * @var M_Image
	 */
	protected $_overlayY;
	
	/**
	 * Set direct access allowed
	 * 
	 * Set if a user can access directly the 
	 * {@link ThumbnailController::resized()} or 
	 * {@link ThumbnailController::square()} methods.
	 * 
	 * Note: by default a user has always access unless direct-access has been
	 * defined as FALSE
	 *
	 * @param bool $bool
	 */
	public static function setDirectAccessAllowed($bool) {
		ThumbnailModel::getInstance()->setDirectAccess($bool);
	}
	
	/**
	 * @return M_Image
	 */
	public function getOverlayImage() {
		return $this->_overlayImage;
	}
	
	/**
	 * @param M_Image $_overlayImage
	 */
	public function setOverlayImage(M_Image $_overlayImage) {
		$this->_overlayImage = $_overlayImage;
	}
	
	/**
	 * @return int
	 */
	public function getOverlayX() {
		return $this->_overlayX;
	}
	
	/**
	 * @param int $_overlayX
	 */
	public function setOverlayX($_overlayX) {
		$this->_overlayX = $_overlayX;
	}
	
	/**
	 * @return int
	 */
	public function getOverlayY() {
		return $this->_overlayY;
	}
	
	/**
	 * @param int $_overlayY
	 */
	public function setOverlayY($_overlayY) {
		$this->_overlayY = $_overlayY;
	}
	
	/**
	 * Create thumbnail from request
	 *
	 * @return void
	 * @author b.brughmans
	 */
	public function index() {
		// Get the arguments that have been provided
		$args = func_get_args();
		
		// Get the definition from the ThumbnailModel class and apply it on a
		//{@link ThumbnailDefinition}
		$definitionName = array_shift($args);
		$definition = ThumbnailModel
			::getInstance()
			->getDefinition(
				$definitionName
			);
		$thumbnaildefinition = $this->_getThumbnailDefinition($definitionName);
		
		// Compose the full path to the new image:
		$imagePath = M_Helper::trimCharlist($definition->directory, '/' . DIRECTORY_SEPARATOR);
		if(! empty($imagePath)) {
			$imagePath .= DIRECTORY_SEPARATOR;
		}
		$imagePath .= implode(DIRECTORY_SEPARATOR, $args);

		//and finally: output the image
		$thumbnail = $thumbnaildefinition->getThumbnail(
			$this->_getImageFile($imagePath)
		);
		M_Header::sendDownload($thumbnail);
	}
	
	/**
	 * Square version of the image
	 * 
	 * This is a public method which simply redirects to the protected one 
	 * {@link ThumbnailController::_square()}. The only difference is we now
	 * check if a user has access
	 * 
	 * @access public
	 * @param integer $size
	 * 		The size (both width and height) of the thumbnail
	 * @param string $file
	 * 		The full path to the original image file
	 * @return void
	 */
	public function square($size, $file) {
		if ($this->_directAccessAllowed() == false) {
			throw new M_Exception('Direct access to square-image not allowed');
		}
		
		// The path to the image file will have been broken down into
		// various arguments, by M_ControllerDispatcher. We recompose
		// the path:
		$path = $file;
		for($i = 2, $n = func_num_args(); $i < $n; $i ++) {
			$path .= '/' . func_get_arg($i);
		}
		
		$this->_square($size, $path);
	}
	
	/**
	 * Resized version of the image (keeping aspect ratio)
	 * 
	 * This is a public method which simply redirects to the protected one 
	 * {@link ThumbnailController::_resized()}. The only difference is we now
	 * check if a user has access
	 *
	 * @access public
	 * @param integer $width
	 * 		The width of the thumbnail
	 * @param integer $height
	 * 		The height of the thumbnail
	 * @param string $file
	 * 		The full path to the original image file
	 * @return void
	 */
	public function resized($width, $height, $file) {
		if ($this->_directAccessAllowed() == false) {
			throw new M_Exception('Direct access to resized-image not allowed');
		}
		
		// The path to the image file will have been broken down into
		// various arguments, by M_ControllerDispatcher. We recompose
		// the path:
		$path = $file;
		for($i = 3, $n = func_num_args(); $i < $n; $i ++) {
			$path .= '/' . func_get_arg($i);
		}
		$this->_resized($width, $height, $path);
	}
	
	/**
	 * Resized version of the image (keeping aspect ratio) and forced to the
	 * given width and height
	 * 
	 * This is a public method which simply redirects to the protected one 
	 * {@link ThumbnailController::_resized()}. The only difference is we now
	 * check if a user has access
	 *
	 * @access public
	 * @param integer $width
	 * 		The width of the thumbnail
	 * @param integer $height
	 * 		The height of the thumbnail
	 * @param string $file
	 * 		The full path to the original image file
	 * @return void
	 */
	public function resizedForced($width, $height, $file) {
		if ($this->_directAccessAllowed() == false) {
			throw new M_Exception('Direct access to resized-image not allowed');
		}
		
		// The path to the image file will have been broken down into
		// various arguments, by M_ControllerDispatcher. We recompose
		// the path:
		$path = $file;
		for($i = 3, $n = func_num_args(); $i < $n; $i ++) {
			$path .= '/' . func_get_arg($i);
		}
		$this->_resized($width, $height, $path, true);
	}
	
	/**
	 * Cropped version of the image
	 * 
	 * This is a public method which simply redirects to the protected one 
	 * {@link ThumbnailController::_resized()}. The only difference is we now
	 * check if a user has access
	 *
	 * @access public
	 * @param integer $width
	 * 		The width of the thumbnail
	 * @param integer $height
	 * 		The height of the thumbnail
	 * @param string $file
	 * 		The full path to the original image file
	 * @return void
	 */
	public function cropped($width, $height, $x, $y, $file) {
		if ($this->_directAccessAllowed() == false) {
			throw new M_Exception('Direct access to cropped-image not allowed');
		}
		
		// The path to the image file will have been broken down into
		// various arguments, by M_ControllerDispatcher. We recompose
		// the path:
		$path = $file;
		for($i = 4, $n = func_num_args(); $i < $n; $i ++) {
			$path .= '/' . func_get_arg($i);
		}
		$this->_resized($width, $height, $x, $y, $path);
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Square version of the image
	 * 
	 * @access public
	 * @param integer $size
	 * 		The size (both width and height) of the thumbnail
	 * @param string $path
	 * 		The full path to the original image file
	 * @param integer $quality
	 * 		The quality of the thumbnail image file. For more information, read
	 * 		{@link MI_ImageResource::saveToFile()}
	 * @return void
	 */
	protected function _square($size, $path, $quality = 85) {
		//apply settings on thumbnaildefinition
		$td = $this->_getThumbnailDefinition()
				->setWidth($size)
				->setQuality($quality)
				->setType(ThumbnailDefinition::TYPE_SQUARE);

		//use an overlay?
		if($this->getOverlayImage()) {
			$td->setOverlay($this->getOverlayImage()->getPath())
					->setOverlayX($this->getOverlayX())
					->setOverlayY($this->getOverlayY());
		}
		
		// Send the image headers:
		M_Header::sendDownload($td->getThumbnail(
			$this->_getImageFile($path)
		));
	}
	
	/**
	 * Resized version of the image (keeping aspect ratio)
	 * 
	 * Serves a resized thumbnail version of the image, taking into
	 * account a given maximum width and height.
	 *
	 * @access protected
	 * @param integer $width
	 * 		The width of the thumbnail
	 * @param integer $height
	 * 		The height of the thumbnail
	 * @param string $path
	 * 		The full path to the original image file
	 * @param bool $forced
	 * 		Force the image to be within the given width and height
	 * @param integer $quality
	 * 		The quality of the thumbnail image file. For more information, read
	 * 		{@link MI_ImageResource::saveToFile()}
	 * @return void
	 */
	protected function _resized($width, $height, $path, $forced = false, $quality = 85) {
		// Apply settings on
		$td = $this->_getThumbnailDefinition()
				->setType(ThumbnailDefinition::TYPE_RESIZED)
				->setHeight($height)
				->setWidth($width)
				->setQuality($quality);

		//use an overlay?
		if($this->getOverlayImage()) {
			$td->setOverlay($this->getOverlayImage()->getPath())
					->setOverlayX($this->getOverlayX())
					->setOverlayY($this->getOverlayY());
		}
		
		// Send the image headers:
		M_Header::sendDownload(
			$td->getThumbnail($this->_getImageFile($path))
		);
	}
	
	/**
	 * Cropped version of the image
	 * 
	 * Serves a cropped thumbnail version of the image, taking into account
	 * a given width and height and a x and y position on which we will start
	 * to crop
	 * 
	 * @access protected
	 * @param integer $width
	 * 		The width of the thumbnail
	 * @param integer $height
	 * 		The height of the thumbnail
	 * @param integer $x
	 * 		The x-position on which we will start to crop
	 * @param integer $y
	 * 		The y-position on which we will start to crop
	 * @param integer $quality
	 * 		The quality of the thumbnail image file. For more information, read
	 * 		{@link MI_ImageResource::saveToFile()}
	 * @return void
	 */
	protected function _cropped($width, $height, $x = 0, $y = 0, $path, $quality = 85) {

		//apply settings on thumbnaildefinition
		$td = $this->_getThumbnailDefinition()
				->setType(ThumbnailDefinition::TYPE_CROPPED)
				->setWidth($width)
				->setHeight($height)
				->setCropX($x)
				->setCropY($y)
				->setQuality($quality);

		//use an overlay?
		if($this->getOverlayImage()) {
			$td->setOverlay($this->getOverlayImage()->getPath())
					->setOverlayX($this->getOverlayX())
					->setOverlayY($this->getOverlayY());
		}

		// Send the image headers:
		M_Header::sendDownload(
			$td->getThumbnail(
				$this->_getImageFile($path)
			)
		);
	}

	/**
	 * Get the image file
	 *
	 * @param string $path
	 * @return M_Image
	 */
	private function _getImageFile($path) {
		$image = new M_Image($path);

		//so the image does not exist. We suppose the developer has passed the
		//path with an id and url of the media item
		//is given instead of the path itself. Let's try to find it in the
		//database
		if ($image->exists() == false) {

			//the path is something like this: the filepath/id/basename-url.jpg
			//we get rid of the basename-url and get the id
			$path = explode('/', $path);
			array_pop($path);
			$id = array_pop($path);

			//get the media-item by it's id
			$mediaMapper = M_Loader::getDataObjectMapper('Media', 'media');
			$media = $mediaMapper->getOneByFieldWithRegistryCache(
				'id',
				$id
			);

			//Hurray! we found an image. Now we can get the path from the Media-item
			//to use the image
			/* @var $media Media */
			if ($media) {
				$image = new M_Image($media->getFilePath());
			}
		}

		return $image;
	}
	
	/**
	 * Check if a user has direct access to {@link ThumbnailController::square()}
	 * or {@link ThumbnailController::resized()} methods. This to prevent
	 * a user bypassing config settings
	 *
	 * Note: by default a user has always access unless direct-access has been
	 * defined as FALSE
	 * 
	 * @return bool
	 */
	protected function _directAccessAllowed() {
		//check if a user has direct access to one of "square" / "resized" methods
		$access = ThumbnailModel::getInstance()->getDirectAccess();
		
		if ($access === false) return false;
		return true;
	}
}