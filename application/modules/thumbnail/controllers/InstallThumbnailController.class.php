<?php
/**
 * InstallThumbnailController
 * 
 * @package App
 * @subpackage Thumbnail
 */
class InstallThumbnailController {
	/**
	 * Install the module
	 * 
	 * @access public
	 * @return void
	 */
	public function install() {
		// Get the module's configuration file
		$module = new M_ApplicationModule('thumbnail');
		
		// We create the directory where thumbnails are generated:
		$directory = new M_Directory(M_Loader::getAbsolute($module->getConfig()->directory));
		$directory->make();
	}
}