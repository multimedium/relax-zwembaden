<?php
/**
 * ThumbnailModel
 * 
 * @package App
 * @subpackage Thumbnail
 */
class ThumbnailModel extends M_Object {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Session namespace
	 * 
	 * Stores the session namespace that is used by this class, in order to store
	 * internal (and persistent) variables.
	 * 
	 * @access protected
	 * @var M_SessionNamespace
	 */
	protected $_sessionNamespace;
	
	private static $_instance;
	
	private function __construct() {
		
	}
	
	public static function getInstance() {
		if(! self::$_instance) {
			self::$_instance = new self;
		}
		return self::$_instance;
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set flag: Has direct access?
	 * 
	 * Will set whether or not the {@link ThumbnailController::square()} and
	 * {@link ThumbnailController::resize()}, ... methods can be called directly
	 * in the application. Access may be denied, in order to avoid the user from
	 * bypassing the thumbnail settings (overlay, ...) 
	 *
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if access is to be granted, FALSE if not
	 * @return void
	 */
	public function setDirectAccess($flag) {
		$this->_getSessionNamespace()->directAccess = (bool) $flag;
	}
	
	/**
	 * Get flag: Has direct access?
	 * 
	 * Will tell whether or not the {@link ThumbnailController::square()} and
	 * {@link ThumbnailController::resize()}, ... methods can be called directly
	 * in the application. Access may be denied, in order to avoid the user from
	 * bypassing the thumbnail settings (overlay, ...) 
	 *
	 * @access public
	 * @return boolean $flag
	 * 		Returns TRUE if access granted, FALSE if not
	 */
	public function getDirectAccess() {
		// Get the module's configuration:
		$config = M_Application::getConfigOfModule('thumbnail');
		
		// Return the result:
		return (
			// If permission has been granted explicitely
			$this->_getSessionNamespace()->directAccess === TRUE ||
			// Or, if the module specifically allows direct access at all times
			// (regardless of existing definitions)!
			(isset($config->alwaysAllowDirectAccess) && M_Helper::isBooleanTrue($config->alwaysAllowDirectAccess)) ||
			// Or, if the module does not define thumbnails
			! isset($config->definitions)
		);
	}
	
	/**
	 * Get thumbnail module configuration
	 *
	 * @access public
	 * @return MI_Config $config
	 * 		The configuration object
	 */
	public function getConfig() {
		// If not yet constructed
		$module = new M_ApplicationModule('thumbnail');
		return $module->getConfig();
	}
	
	/**
	 * Get a definition from config
	 *
	 * @access public
	 * @param string $name
	 * 		The name of the definition
	 */
	public function getDefinition($name) {
		// Check if the definition exists:
		$definition = $this->getConfig()->definitions->$name;
		if (is_null($definition)) {
			// If not, throw an exception
			throw new M_Exception(sprintf(
				'unknown thumbnail-definition "%s" defined', 
				$name)
			);
		}
		
		// return the definition
		return $definition;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get Session namespace
	 * 
	 * Will provide with the Session namespace that is used by this class to
	 * store internal (and persistent) variables.
	 *
	 * @access protected
	 * @return M_SessionNamespace
	 */
	protected function _getSessionNamespace() {
		// If not yet constructed
		if(is_null($this->_sessionNamespace)) {
			// Do so now:
			$this->_sessionNamespace = new M_SessionNamespace('thumbnail', true);
		}
		
		// Return the namespace:
		return $this->_sessionNamespace;
	}
}