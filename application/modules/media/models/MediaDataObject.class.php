<?php
/** 
 * MediaDataObject class
 * 
 * @see MediaDataObjectMapper
 * @see MediaDataObjectModel
 * @package App
 */
class MediaDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link MediaDataObject::getId()}
	 * - {@link MediaDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * title ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * title. To get/set the value of the title, read the documentation on 
	 * 
	 * - {@link MediaDataObject::getTitle()}
	 * - {@link MediaDataObject::getTitleId()}
	 * - {@link MediaDataObject::setTitle()}
	 * - {@link MediaDataObject::setTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_titleId;
	
	/**
	 * description ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * description. To get/set the value of the description, read the
	 * documentation on 
	 * 
	 * - {@link MediaDataObject::getText()}
	 * - {@link MediaDataObject::getTextId()}
	 * - {@link MediaDataObject::setText()}
	 * - {@link MediaDataObject::setTextId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_descriptionId;
	
	/**
	 * fileExtension
	 * 
	 * This property stores the fileExtension. To get/set the value of the
	 * fileExtension, read the documentation on 
	 * 
	 * - {@link MediaDataObject::getFileExtension()}
	 * - {@link MediaDataObject::setFileExtension()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_fileExtension;
	
	/**
	 * filesize
	 * 
	 * This property stores the filesize. To get/set the value of the
	 * filesize, read the documentation on 
	 * 
	 * - {@link MediaDataObject::getFilesize()}
	 * - {@link MediaDataObject::setFilesize()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_filesize;
	
	/**
	 * dateAdded
	 * 
	 * This property stores the dateAdded. To get/set the value of the
	 * dateAdded, read the documentation on 
	 * 
	 * - {@link MediaDataObject::getDateAdded()}
	 * - {@link MediaDataObject::setDateAdded()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_dateAdded;
	
	/**
	 * filePath
	 * 
	 * This property stores the filePath. To get/set the value of the
	 * filePath, read the documentation on 
	 * 
	 * - {@link MediaDataObject::getFilePath()}
	 * - {@link MediaDataObject::setFilePath()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_filePath;
	
	/**
	 * filenameOriginal
	 * 
	 * This property stores the filenameOriginal. To get/set the value of the
	 * filenameOriginal, read the documentation on 
	 * 
	 * - {@link MediaDataObject::getFilenameOriginal()}
	 * - {@link MediaDataObject::setFilenameOriginal()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_filenameOriginal;
	
	/**
	 * type
	 * 
	 * This property stores the type. To get/set the value of the type, read
	 * the documentation on 
	 * 
	 * - {@link MediaDataObject::getType()}
	 * - {@link MediaDataObject::setType()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_type;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link MediaDataObject::getPublished()}
	 * - {@link MediaDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * albumId
	 * 
	 * This property stores the albumId. To get/set the value of the albumId,
	 * read the documentation on 
	 * 
	 * - {@link MediaDataObject::getAlbumId()}
	 * - {@link MediaDataObject::setAlbumId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_albumId;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the order,
	 * read the documentation on 
	 * 
	 * - {@link MediaDataObject::getOrder()}
	 * - {@link MediaDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * Get title
	 * 
	 * Get title in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the title
	 * @return string|NULL
	 */
	public function getTitle($locale = NULL) {
		return $this->_getLocalizedText('title', $locale);
	}
	
	/**
	 * Get title ID
	 * 
	 * Get numeric reference to the translated title .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTitleId() {
		return $this->_titleId;
	}
	
	/**
	 * Set title
	 * 
	 * Set title, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated title
	 * @param string $locale
	 * 	The language in which to set the title
	 * @return void
	 */
	public function setTitle($text, $locale = NULL) {
		$this->_setLocalizedText('title', $text, $locale);
	}
	
	/**
	 * Set title ID
	 * 
	 * Set numeric reference to the translated title .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of title
	 * @return void
	 */
	public function setTitleId($id) {
		$this->_titleId = $id;
	}
	
	/**
	 * Get description
	 * 
	 * Get description in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the description
	 * @return string|NULL
	 */
	public function getText($locale = NULL) {
		return $this->_getLocalizedText('description', $locale);
	}
	
	/**
	 * Get description ID
	 * 
	 * Get numeric reference to the translated description .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTextId() {
		return $this->_descriptionId;
	}
	
	/**
	 * Set description
	 * 
	 * Set description, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated description
	 * @param string $locale
	 * 	The language in which to set the description
	 * @return void
	 */
	public function setText($text, $locale = NULL) {
		$this->_setLocalizedText('description', $text, $locale);
	}
	
	/**
	 * Set description ID
	 * 
	 * Set numeric reference to the translated description .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of description
	 * @return void
	 */
	public function setTextId($id) {
		$this->_descriptionId = $id;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return mixed
	 */
	public function setId($id) {
		$this->_id = $id;
	}
	
	/**
	 * Get fileExtension
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getFileExtension() {
		return $this->_fileExtension;
	}
	
	/**
	 * Set fileExtension
	 * 
	 * @access public
	 * @param mixed $fileExtension
	 * @return mixed
	 */
	public function setFileExtension($fileExtension) {
		$this->_fileExtension = $fileExtension;
	}
	
	/**
	 * Get filesize
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getFilesize() {
		return $this->_filesize;
	}
	
	/**
	 * Set filesize
	 * 
	 * @access public
	 * @param mixed $filesize
	 * @return mixed
	 */
	public function setFilesize($filesize) {
		$this->_filesize = $filesize;
	}
	
	/**
	 * Get dateAdded
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getDateAdded() {
		return $this->_dateAdded;
	}
	
	/**
	 * Set dateAdded
	 * 
	 * @access public
	 * @param mixed $dateAdded
	 * @return mixed
	 */
	public function setDateAdded($dateAdded) {
		$this->_dateAdded = $dateAdded;
	}
	
	/**
	 * Get filePath
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getFilePath() {
		return $this->_filePath;
	}
	
	/**
	 * Set filePath
	 * 
	 * @access public
	 * @param mixed $filePath
	 * @return mixed
	 */
	public function setFilePath($filePath) {
		$this->_filePath = $filePath;
	}
	
	/**
	 * Get filenameOriginal
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getFilenameOriginal() {
		return $this->_filenameOriginal;
	}
	
	/**
	 * Set filenameOriginal
	 * 
	 * @access public
	 * @param mixed $filenameOriginal
	 * @return mixed
	 */
	public function setFilenameOriginal($filenameOriginal) {
		$this->_filenameOriginal = $filenameOriginal;
	}
	
	/**
	 * Get type
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Set type
	 * 
	 * @access public
	 * @param mixed $type
	 * @return mixed
	 */
	public function setType($type) {
		$this->_type = $type;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return mixed
	 */
	public function setPublished($published) {
		$this->_published = $published;
	}
	
	/**
	 * Get albumId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getAlbumId() {
		return $this->_albumId;
	}
	
	/**
	 * Set albumId
	 * 
	 * @access public
	 * @param mixed $albumId
	 * @return mixed
	 */
	public function setAlbumId($albumId) {
		$this->_albumId = $albumId;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return mixed
	 */
	public function setOrder($order) {
		$this->_order = $order;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return MediaMapper $mapper
	 */
	public function getMapper() {
		if (is_null($this->_mapper)) {
			M_Loader::loadDataObjectMapper("Media","media");
			$this->_mapper = new MediaMapper();
		}
		return $this->_mapper;
	}
	
		
}