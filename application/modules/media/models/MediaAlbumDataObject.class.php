<?php
/** 
 * MediaAlbumDataObject class
 * 
 * @see MediaAlbumDataObjectMapper
 * @see MediaAlbumDataObjectModel
 * @package App
 */
class MediaAlbumDataObject extends M_DataObject {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link MediaAlbumDataObject::getId()}
	 * - {@link MediaAlbumDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * title ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * title. To get/set the value of the title, read the documentation on 
	 * 
	 * - {@link MediaAlbumDataObject::getTitle()}
	 * - {@link MediaAlbumDataObject::getTitleId()}
	 * - {@link MediaAlbumDataObject::setTitle()}
	 * - {@link MediaAlbumDataObject::setTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_titleId;
	
	/**
	 * description ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * description. To get/set the value of the description, read the
	 * documentation on 
	 * 
	 * - {@link MediaAlbumDataObject::getDescription()}
	 * - {@link MediaAlbumDataObject::getDescriptionId()}
	 * - {@link MediaAlbumDataObject::setDescription()}
	 * - {@link MediaAlbumDataObject::setDescriptionId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_descriptionId;
	
	/**
	 * url ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * url. To get/set the value of the url, read the documentation on 
	 * 
	 * - {@link MediaAlbumDataObject::getUrl()}
	 * - {@link MediaAlbumDataObject::getUrlId()}
	 * - {@link MediaAlbumDataObject::setUrl()}
	 * - {@link MediaAlbumDataObject::setUrlId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_urlId;
	
	/**
	 * realm
	 * 
	 * This property stores the realm. To get/set the value of the realm,
	 * read the documentation on 
	 * 
	 * - {@link MediaAlbumDataObject::getRealm()}
	 * - {@link MediaAlbumDataObject::setRealm()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_realm;
	
	/**
	 * parentAlbumId
	 * 
	 * This property stores the parentAlbumId. To get/set the value of the
	 * parentAlbumId, read the documentation on 
	 * 
	 * - {@link MediaAlbumDataObject::getParentAlbumId()}
	 * - {@link MediaAlbumDataObject::setParentAlbumId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_parentAlbumId;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the order,
	 * read the documentation on 
	 * 
	 * - {@link MediaAlbumDataObject::getOrder()}
	 * - {@link MediaAlbumDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * deletable
	 * 
	 * This property stores the deletable. To get/set the value of the
	 * deletable, read the documentation on 
	 * 
	 * - {@link MediaAlbumDataObject::getDeletable()}
	 * - {@link MediaAlbumDataObject::setDeletable()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_deletable;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link MediaAlbumDataObject::getPublished()}
	 * - {@link MediaAlbumDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * Get title
	 * 
	 * Get title in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the title
	 * @return string|NULL
	 */
	public function getTitle($locale = NULL) {
		return $this->_getLocalizedText('title', $locale);
	}
	
	/**
	 * Get title ID
	 * 
	 * Get numeric reference to the translated title .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTitleId() {
		return $this->_titleId;
	}
	
	/**
	 * Set title
	 * 
	 * Set title, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated title
	 * @param string $locale
	 * 	The language in which to set the title
	 * @return void
	 */
	public function setTitle($text, $locale = NULL) {
		$this->_setLocalizedText('title', $text, $locale);
	}
	
	/**
	 * Set title ID
	 * 
	 * Set numeric reference to the translated title .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of title
	 * @return void
	 */
	public function setTitleId($id) {
		$this->_titleId = $id;
	}
	
	/**
	 * Get description
	 * 
	 * Get description in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the description
	 * @return string|NULL
	 */
	public function getDescription($locale = NULL) {
		return $this->_getLocalizedText('description', $locale);
	}
	
	/**
	 * Get description ID
	 * 
	 * Get numeric reference to the translated description .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getDescriptionId() {
		return $this->_descriptionId;
	}
	
	/**
	 * Set description
	 * 
	 * Set description, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated description
	 * @param string $locale
	 * 	The language in which to set the description
	 * @return void
	 */
	public function setDescription($text, $locale = NULL) {
		$this->_setLocalizedText('description', $text, $locale);
	}
	
	/**
	 * Set description ID
	 * 
	 * Set numeric reference to the translated description .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of description
	 * @return void
	 */
	public function setDescriptionId($id) {
		$this->_descriptionId = $id;
	}
	
	/**
	 * Get url
	 * 
	 * Get url in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the url
	 * @return string|NULL
	 */
	public function getUrl($locale = NULL) {
		return $this->_getLocalizedText('url', $locale);
	}
	
	/**
	 * Get url ID
	 * 
	 * Get numeric reference to the translated url .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getUrlId() {
		return $this->_urlId;
	}
	
	/**
	 * Set url
	 * 
	 * Set url, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated url
	 * @param string $locale
	 * 	The language in which to set the url
	 * @return void
	 */
	public function setUrl($text, $locale = NULL) {
		$this->_setLocalizedText('url', $text, $locale);
	}
	
	/**
	 * Set url ID
	 * 
	 * Set numeric reference to the translated url .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of url
	 * @return void
	 */
	public function setUrlId($id) {
		$this->_urlId = $id;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return mixed
	 */
	public function setId($id) {
		$this->_id = $id;
	}
	
	/**
	 * Get realm
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getRealm() {
		return $this->_realm;
	}
	
	/**
	 * Set realm
	 * 
	 * @access public
	 * @param mixed $realm
	 * @return mixed
	 */
	public function setRealm($realm) {
		$this->_realm = $realm;
	}
	
	/**
	 * Get parentAlbumId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getParentAlbumId() {
		return $this->_parentAlbumId;
	}
	
	/**
	 * Set parentAlbumId
	 * 
	 * @access public
	 * @param mixed $parentAlbumId
	 * @return mixed
	 */
	public function setParentAlbumId($parentAlbumId) {
		$this->_parentAlbumId = $parentAlbumId;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return mixed
	 */
	public function setOrder($order) {
		$this->_order = $order;
	}
	
	/**
	 * Get deletable
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getDeletable() {
		return $this->_deletable;
	}
	
	/**
	 * Set deletable
	 * 
	 * @access public
	 * @param mixed $deletable
	 * @return mixed
	 */
	public function setDeletable($deletable) {
		$this->_deletable = $deletable;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return mixed
	 */
	public function setPublished($published) {
		$this->_published = $published;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return MediaAlbumMapper $mapper
	 */
	public function getMapper() {
		if (is_null($this->_mapper)) {
			M_Loader::loadDataObjectMapper("MediaAlbum","media");
			$this->_mapper = new MediaAlbumMapper();
		}
		return $this->_mapper;
	}
	
		
}