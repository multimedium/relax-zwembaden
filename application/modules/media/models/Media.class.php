<?php
/**
 * Media class
 * 
 * @package App
 * @subpackage Media
 */

// Load superclass (auto-generated)
M_Loader::loadDataObject('MediaDataObject','media');

/** 
 * Media class
 * 
 * @see MediaDataObject
 * @see MediaMapper
 * @package App
 * @subpackage Media
 */
class Media extends MediaDataObject {
	/**
	 *@const media-type image
	 */
	const TYPE_IMAGE = 'image';
	
	/**
	 *@const media-type video
	 */
	const TYPE_VIDEO = 'video';
	
	/**
	 *@const media-type video
	 */
	const TYPE_AUDIO = 'audio';
	
	/**
	 *@const media-type document
	 */
	const TYPE_DOCUMENT = 'document';
	
	/**
	 * @var M_File
	 */
	protected $_file;
	
	/**
	 * Get the file 
	 * 
	 * @return M_File
	 */
	public function getFile() {
		if (is_null($this->_file)) {
			$file  = new M_File(
				M_Loader::getAbsolute($this->getFilePath())
			);
			
			//only set file when it exists
			if ($file->exists()) $this->_file = $file;
		}
		return $this->_file;
	}

	/**
	 * Check if the file still exists
	 *
	 * @return bool
	 */
	public function getFileExists() {
		if ($this->getFile()) {
			return $this->getFile()->exists();
		}else {
			return false;
		}
	}
	
	/**
	 * Get the basename of the file
	 *
	 * @return string
	 */
	public function getBasename() {
		return $this->getFile()->getBasename();
	}
	
	/**
	 * Get basename based on the title
	 *
	 * Note: when the title of the media-item is empty, we use the normal
	 * basename. If we don't do this: {@link M_Uri::getPathElementFromString()}
	 * will throw an exception
	 *
	 * @author Wim Van De Mierop
	 * @return string
	 */
	public function getBasenameFromTitle() {
		$title = $this->getTitle();
		if($title) {
			return M_Uri::getPathElementFromString($title)  . '.' . $this->getFileExtension();
		}else {
			return $this->getBasename();
		}
	}
	
	/**
	 * Set the file, and subsequently save it's relative file path
	 * 
	 * @param M_File $file
	 * @return void
	 */
	public function setFile(M_File $file) {
		parent::setFilePath(
			M_Loader::getRelative($file->getPath())
		);
		$this->_file = $file;
	}
	
	/**
	 * Set relative file path and update reference to file 
	 * 
	 * @link MediaDataObject::getFile()
	 * @param str $str
	 * @return void
	 */
	public function setFilePath($str) {
		$this->setFile(
			new M_File(M_Loader::getAbsolute($str))
		);
	}
	
	/**
	 * Get the filesize from the referenced file
	 * 
	 * Note: this method will use {@link Media::getFile->getSize()}
	 * 
	 * @return int
	 */
	public function getFilesize() {
		if ($this->getFile()) return $this->getFile()->getSize();
		else return 0;
	}
	
	/**
	 * Get the file extension
	 * 
	 * Note: this method will use {@link Media::getFile->getFileExtension()}
	 * 
	 * @return str
	 */
	public function getFileExtension() {
		if ($this->getFile()) return $this->getFile()->getFileExtension();
		else return null;
	}
	
	/**
	 * Get the type of the image
	 * 
	 * @see self::TYPE_MEDIA
	 * @see self::TYPE_VIDEO
	 * @see self::TYPE_AUDIO
	 * @see self::TYPE_DOCUMENT
	 * @return string
	 * 		The file type (false on failure)
	 */
	public function getType() {
		$type = $this->_type;
		
		if (is_null($this->_type)) {
			$ext = strtolower($this->getFileExtension());
			
			if (in_array($ext, $this->getImageFileExtensions())) $type = self::TYPE_IMAGE;
			elseif (in_array($ext, $this->getDocumentFileExtensions())) $type = self::TYPE_DOCUMENT;
			elseif (in_array($ext, $this->getAudioFileExtensions())) $type = self::TYPE_AUDIO;
			elseif (in_array($ext, $this->getVideoFileExtensions())) $type = self::TYPE_VIDEO;
		}
		
		return $type;
	}
	
	/**
	 * Get link for the file
	 * 
	 * In order to conesequently display media-files throughout the application
	 * we use this method to create the link which will be displayed in e.g. 
	 * fancybox
	 * 
	 * @return string
	 */
	public function getLinkPrefix() {
		//if this is an image file we create a thumbnail
		if ($this->getType() == self::TYPE_IMAGE) {
			
			$linkPrefix = $this->getMapper()->getModule()->getConfig()->get(
				'imageLinkPrefix'
			);
			$link = M_Request::getLink(
				'thumbnail/'.$linkPrefix.'/'.$this->getFile()->getBasename()
			);
		}else {
			$link = M_Request::getLinkWithoutPrefix(
				$this->getFilePath()
			);
		}
		
		return $link;
	}
	
	/**
	 * Check if this file is an image-file
	 * 
	 * @return bool
	 * @see self::TYPE_IMAGE
	 * @see self::getType()
	 */
	public function isImage() {
		return $this->getType() == self::TYPE_IMAGE;
	}
	
	/**
	 * Check if this file is an audio-file
	 * 
	 * @return bool
	 * @see self::TYPE_AUDIO
	 */
	public function isAudio() {
		return $this->getType() == self::TYPE_AUDIO;
	}
	
	/**
	 * Check if this file is an video-file
	 * 
	 * @return bool
	 * @see self::TYPE_VIDEO
	 */
	public function isVideo() {
		return $this->getType() == self::TYPE_VIDEO;
	}
	
	/**
	 * Check if this file is an document-file
	 * 
	 * @return bool
	 * @see self::TYPE_DOCUMENT
	 */
	public function isDocument() {
		return $this->getType() == self::TYPE_DOCUMENT;
	}
	
	/**
	 * Supported image file extensions
	 *
	 * @return array
	 */
	public static function getImageFileExtensions() {
		return array(
			'jpg', 'jpeg', 'gif', 'png'
		);
	}
	
	/**
	 * Supported audio file extensions
	 *
	 * @return array
	 */
	public static function getAudioFileExtensions() {
		return array(
			'mp3', 'mp4', 'aac', 'wma'
		);
	}
	
	/**
	 * Supported video file extensions
	 *
	 * @return array
	 */
	public static function getVideoFileExtensions() {
		return array(
			'mpg', 'mpeg', 'wmv', 'avi'
		);
	}
	
	/**
	 * Supported document file extensions
	 *
	 * @return array
	 */
	public static function getDocumentFileExtensions() {
		return array(
			'odf', 'ods', 'odp', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pages', 'key', 'numbers'
		);
	}
	
	/**
	 * Get file extensions for a certain type of media
	 * 
	 * @see Media::TYPE_AUDIO
	 * @see Media::TYPE_DOCUMENT
	 * @see Media::TYPE_IMAGE
	 * @see Media::TYPE_VIDEO
	 * @param string $type
	 * @return array
	 */
	public static function getFileExtensionsByType($type) {
		switch ($type) {
			case self::TYPE_AUDIO:
				return self::getAudioFileExtensions();
				break;
			case self::TYPE_DOCUMENT:
				return self::getDocumentFileExtensions();
				break;
			case self::TYPE_IMAGE:
				return self::getImageFileExtensions();
				break;
			case self::TYPE_VIDEO:
				return self::getVideoFileExtensions();
				break;
			default:
				throw new M_Exception(
					sprintf('Cannot get file extensions for unsupported type %s', $type)
				);
				break;
		}
	}
	
	/**
	 * Get all available file extensions
	 * 
	 * @return array
	 */
	public static function getFileExtensions() {
		return array_merge(
			self::getAudioFileExtensions(),
			self::getDocumentFileExtensions(),
			self::getImageFileExtensions(),
			self::getVideoFileExtensions()
		);
	}
	
	/**
	 * Check if a file is a valid media file
	 * 
	 * @param M_File $file
	 * @param string $type
	 * 		Check if this is a media-file for a specific type
	 * @return bool
	 */
	public static function isMediaFile(M_File $file, $type = null) {
		$fileExtension = strtolower($file->getFileExtension());
		
		if (is_null($type)) {
			$fileExtensions = self::getFileExtensions();
		}else {
			$fileExtensions = self::getFileExtensionsByType($type);
		}

		return in_array($fileExtension, $fileExtensions);
	}
	
	/**
	 * Delete this object from the database
	 * 
	 * @param bool
	 * 		Forced delete: don't validate, for further information 
	 * 		see {@link MediaMapper::delete()}
	 * @return bool
	 */
	public function delete($forced = true) {
		return $this->getMapper()->delete($this, $forced);
	}
	
}