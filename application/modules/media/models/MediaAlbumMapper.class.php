<?php
/**
 * MediaAlbumMapper
 *
 * @package App
 * @subpackage Media
 */

// Media mapper is used in different methods in this mapper
M_Loader::loadDataObjectMapper('Media', 'media');

/**
 * MediaAlbumMapper
 *
 * @package App
 * @subpackage Media
 */
class MediaAlbumMapper extends M_DataObjectMapper  {
	/**
	 * @const Cross table where object-links to this album are stored
	 */
	const TABLE_MEDIA_ALBUM_X_OBJECT = 'media_album_x_object';
	
	/**
	 * Get the active module folder name
	 *
	 * @return str
	 */
	protected function _getModuleName() {
		return 'media';
	}

	/**
	 * Holds the filters to query on published media
	 *
	 * @seee addFilterMediaPublished()
	 * @var array
	 */
	protected $_filtersMediaPublished = array();

	/**
	 * Holds the filters to query on media-types
	 *
	 * @seee addFilterMediaType()
	 * @var array
	 */
	protected $_filtersMediaType = array();

	/**
	 * Holds the filters to query on object-type
	 *
	 * @seee addFilterObjectType()
	 * @var array
	 */
	protected $_filtersObjectType = array();

	/**
	 * Reset the existing query filters
	 *
	 * @return M_DataObjectMapper
	 */
	public function resetFilters()
	{
		$this->_filtersMediaPublished = array();
		$this->_filtersMediaType = array();
		$this->_filtersObjectType = array();
		return parent::resetFilters();
	}

	/**
	 * Get all media for this album
	 * 
	 * @see Media::TYPE_AUDIO
	 * @see Media::TYPE_DOCUMENT
	 * @see Media::TYPE_IMAGE
	 * @see Media::TYPE_VIDEO
	 * 
	 * @param MediaAlbum $album
	 * @param $type
	 * 		The type of media
	 * @param M_DbQueryFilter|array
	 * 		Pass one, ore more filters to the mediamapper
	 * @return M_ArrayIterator
	 */
	public function getMediaByAlbum(MediaAlbum $album, $type = null, $filters = null) {
		// Load the Media Mapper:
		$mapper = new MediaMapper();
		if (!is_null($filters)) {
			if(M_Helper::isInstanceOf($filters, 'M_DbQueryFilter')) {
				$mapper->addFilter($filters);
			}elseif(is_array($filters)) {
				$mapper->setFilters($filters);
			}else {
				throw new M_Exception(sprintf(
					'Invalid filter specified in %s::%s()',
					__CLASS__,
					'getMediaByAlbum'
				));
			}
		}
		
		// search for a special type of media
		if (!is_null($type)) {
			// To do so, we add a filter to the mapper:
			$filter = new M_DbQueryFilterWhere(
				'fileExtension', 
				Media::getFileExtensionsByType($type), 
				M_DbQueryFilterWhere::IN
			);
			
			$mapper->addFilter($filter);
		}
		$filterOrder = new M_DbQueryFilterOrder('order');
		$mapper->addFilter($filterOrder);
		
		// Get by media album:
		return $mapper->getMediaByAlbum($album);
	}
	
	/**
	 * Get the album for an object
	 *
	 * @param M_DataObject $object
	 * @return MediaAlbum
	 */
	public function getAlbumByObject(M_DataObject $object) {
		$select = $this->_getFilteredSelect();
		$select->joinInner(
			self::TABLE_MEDIA_ALBUM_X_OBJECT,
			null,
			'id',
			self::TABLE_MEDIA_ALBUM_X_OBJECT.'.album_id'
		);
		$select->where(array(
    		    array('object = ?', get_class($object)),
    		    array('module = ?', $object->getMapper()->getModule()->getId()),
    		    array('object_id = ?', $object->getId())
			)
		);
		
		return $this->_fetchOne($select->execute());
	}


	/**
	 * Add filter: search query
	 *
	 * Add a filter so only albums that match the query will be returned
	 *
	 * @access public
	 * @return MediaAlbumMapper
	 */
	public function addFilterSearchQuery($query) {
		return $this->addFilter(new M_DbQueryFilterSearch(array('title'), $query));
	}

	/**
	 * Add filter: root albums only
	 *
	 * Add a filter so only albums that have not been attached to another album
	 * get fetched from the database.
	 *
	 * @access public
	 * @return MediaAlbumMapper
	 */
	public function addFilterRootAlbumsOnly() {
		return $this->addFilter(new M_DbQueryFilterWhere('parent_album_id', 0));
	}

	/**
	 * Set the album for an object
	 *
	 * @param M_DataObject $object
	 * @param int $albumId
	 */
	public function setAlbumIdByObject(M_DataObject $object, $albumId) {
		/* @var $table M_DbTable */
		$table = $this->getDb()->getTable(self::TABLE_MEDIA_ALBUM_X_OBJECT);		
			
		//delete?
		if ($albumId == 0) {
			$res = $table->delete(
				'module = ? AND object = ? AND object_id = ?', 
				$object->getMapper()->getModule()->getId(),
				get_class($object),
				$object->getId()
			);
		}
		//update
		else {
			
			$insert = array(
				'module' => $object->getMapper()->getModule()->getId(),
				'object' => get_class($object),
				'object_id' => $object->getId(),
				'album_id' => $albumId
			);
			
			$update = array(
				'album_id' => $albumId
			);
			
			$res = $table->insertOrUpdate($insert,$update);
		}
		
		return $res;
	}
	
	/**
	 * Delete this album, and the corresponding records in it's crosstable
	 * 
	 * @param MediaAlbum $album
	 * @retun bool
	 */
	public function delete(MediaAlbum $album) {
		// Delete contained media items
		$this->_deleteMedia($album);
		
		// Delete references from other objects to this album
		$this->_deleteObjects($album);

		// Delete children
		$this->_deleteChildren($album);
		
		// Delete the media album
		return parent::delete($album);
	}
	
	/**
	 * Insert a new media album
	 * 
	 * @param MediaAlbum $album
	 * @return bool
	 */
	public function insert(MediaAlbum $album) {
		//set default value
		if ($album->getDeletable() == null) {
			$album->setDeletable(1);
		}
		
		return parent::insert($album);
	}
	
	/**
	 * Count the number of media-items for this album
	 *
	 * @param MediaAlbum $album
	 * @param bool
	 *		Select (non)published items only
	 * @return int
	 */
	public function getMediaCount(MediaAlbum $album, $published = null) {
		// Load the Media Mapper:
		$mapper = new MediaMapper();

		if (!is_null($published)) {
			$mapper->addFilterPublished((bool)$published);
		}
		// Ask the mapper for a count:
		return $mapper->getMediaCountByAlbum($album);
	}
	
	/**
	 * Delete all media-links for this album
	 * 
	 * @param MediaAlbum $album
	 * @return bool
	 */
	public function deleteMedia(MediaAlbum $album) {
		return $this->_deleteMedia($album);
	}

	/**
	 * Get an album by realm
	 *
	 * @param string $realm
	 */
	public function getByRealm($realm) {
		// Get filtered select
		$result = $this->_getFilteredSelect()
			// Add Where clause
			->where('realm = ?', $realm)
			// Execute
			->execute();

		// Count the result
		if($result->count() > 0) {
			// Return the text
			return $this->_fetchOne($result);
		} else {
			throw new M_Exception('No MediaAlbum found with realm "'.$realm.'"');
		}
	}

	/* -- Filters -- */

	/**
	 * Add a filter to this mapper so only (un)published media-albums will be
	 * retrieved
	 *
	 * @param bool $published
	 * @return MediaAlbumMapper
	 */
	public function addFilterPublished($published = true) {
		$this->addFilter(new M_DbQueryFilterWhere('published', (bool)$published));
		return $this;
	}

	/**
	 * Add a filter to retrieve only albums for which the parent album is $album
	 * 
	 * @param MediaAlbum $album
	 * @return MediaAlbumMapper
	 */
	public function addFilterParentAlbum(MediaAlbum $album) {
		return $this->addFilter(new M_DbQueryFilterWhere('parentAlbumId', $album->getId()));
	}

	/**
	 * Add a filter to query only on media albums which hold at least one
	 * published media-item
	 *
	 * @return MediaAlbumMapper
	 */
	public function addFilterMediaPublished( $published = true ) {
		$this->_filtersMediaPublished[] = (bool) $published;
		return $this;
	}

	/**
	 * Add a filter to query only on media albums which hold at least one
	 * media-item of type "image"
	 *
	 * @return MediaAlbumMapper
	 */
	public function addFilterMediaImage() {
		M_Loader::loadDataObject('Media', 'media');
		$this->_filtersMediaType[] = Media::TYPE_IMAGE;
		return $this;
	}

	/**
	 * Add a filter to query only on type of object e.g. News (in a certain
	 * module
	 *
	 * @param string $object Classname of object
	 * @param string $module
	 * @return MediaAlbumMapper
	 */
	public function addFilterObjectType($object, $module = null) {
		$this->_filtersObjectType[] = array(
			'object' => $object,
			'module' => $module
		);
		return $this;
	}

	/**
	 * Add a filter deletable
	 *
	 * @param int $deletable
	 * @return MediaAlbumMapper
	 */
	public function addFilterDeletable($deletable = TRUE) {
		$this->addFilter(new M_DbQueryFilterWhere('deletable', (int) $deletable));
		return $this;
	}

	/**
	 * Add a filter to select an album by it's realm
	 *
	 * @param string $realm
	 * @return MediaAlbumMapper
	 */
	public function addFilterRealm($realm) {
		$this->addFilter(new M_DbQueryFilterWhere('realm', (string) $realm));
		return $this;
	}

	/**
	 * Add filter: default sorting
	 *
	 * @access public
	 * @return MediaAlbumMapper $mapper
	 * 		Returns itself, for a fluent programming interface
	 */
	public function addFilterDefaultSorting($order = 'ASC') {
		return $this->addFilter(new M_DbQueryFilterOrder('order', $order));
	}

	/* -- Protected -- */

	/**
	 * Get the filtered query with filters on media-albums which hold at least
	 * one published media-item
	 *
	 * @param M_DbQuery $query
	 * @return M_DbQuery
	 */
	protected function _getFilteredQueryMediaPublished(M_DbQuery $query) {
		if (count($this->_filtersMediaPublished) == 0) return $query;

		$query->group($this->getTableName().'.id');
		
		$i = 1;
		foreach($this->_filtersMediaPublished AS $published) {
			$query->joinInner('media', 'media_published_' . $i, 'media_album.id', 'media_published_'.$i.'.album_id');
			$query->where('media_published_'.$i.'.published = '.(int)$published);
			$i++;
		}
		return $query;
	}

	/**
	 * Get the filtered query with filters on media-albums which hold at least
	 * one published media-item
	 *
	 * @param M_DbQuery $query
	 * @return M_DbQuery
	 */
	protected function _getFilteredQueryObjectType(M_DbQuery $query) {
		if (count($this->_filtersObjectType) == 0) return $query;

		$query->group($this->getTableName().'.id');
		$i = 1;
		foreach($this->_filtersObjectType AS $objectType) {
			$query->joinLeft(self::TABLE_MEDIA_ALBUM_X_OBJECT, 'object_' . $i, 'media_album.id', 'object_'.$i.'.album_id');
			$query->where('object_'.$i.'.object = ?', $objectType['object']);
			$module = $objectType['module'];
			if (!is_null($module)) $query->where('object_'.$i.'.module = ?', $module);
			
			$i++;
		}
		return $query;
	}

	/**
	 * Get the filtered query with filters on media-albums which hold at least
	 * one media-item of a certain type
	 *
	 * @param M_DbQuery $query
	 * @return M_DbQuery
	 */
	protected function _getFilteredQueryMediaType(M_DbQuery $query) {
		if (count($this->_filtersMediaType) == 0) return $query;

		$query->group($this->getTableName().'.id');
		$i = 1;
		foreach($this->_filtersMediaType AS $type) {
			$query->joinInner('media', 'media_type_' . $i, 'media_album.id', 'media_type_'.$i.'.album_id');
			$query->where('media_type_'.$i.'.type = ?', $type);
			$i++;
		}
		return $query;
	}


	/**
	 * Get the filtered query with additional filters
	 *
	 * @param MI_DbQuery $query
	 * @param array $filters
	 * @return M_DbQuery
	 */
	protected function _getFilteredQuery(MI_DbQuery $query, array $filters) {
		// 4. apply object type filters
		return $this->_getFilteredQueryObjectType(
			// 2. apply enddate filters
			$this->_getFilteredQueryMediaType(
				// 2. apply enddate filters
				$this->_getFilteredQueryMediaPublished(
					// 1. Apply regular filter(s):
					parent::_getFilteredQuery($query, $filters)
				)
			)
		);
	}
	
	/**
	 * Delete all media-links for this album from the databse
	 * 
	 * @param MediaAlbum $album
	 * @return bool
	 */
	protected function _deleteMedia(MediaAlbum $album) {
		// Load the Media Mapper:
		$mapper = new MediaMapper();
		
		// Ask the mapper to delete all media items from the album
		return $mapper->deleteMediaByAlbum($album);
	}
	
	/**
	 * Delete all objects-links to this album from the databse
	 * 
	 * @param MediaAlbum $album
	 * @return bool
	 */
	protected function _deleteObjects(MediaAlbum $album) {
		return $this->getDb()
			->getTable(self::TABLE_MEDIA_ALBUM_X_OBJECT)
			->delete('album_id = ?',$album->getId());
	}

	/**
	 * Delete all the children for this album
	 * 
	 * @param MediaAlbum $album
	 */
	protected function _deleteChildren(MediaAlbum $album) {
		/* @var $child MediaAlbum */
		foreach($album->getChildAlbums() AS $child) {
			$child->delete();
		}
	}
}