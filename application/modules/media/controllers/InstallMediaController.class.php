<?php
class InstallMediaController extends M_Controller {
	
	/**
	 * Additional install for media module
	 *
	 * @return bool
	 */
	public function install() {
		return $this->_createCrossTableMediaAlbumObject();
	}
	
	/**
	 * Create a table to store the dataobjects / album
	 * 
	 * @return bool
	 */
	protected function _createCrossTableMediaAlbumObject() {
		//create cross table : media objects / album
		$db = M_Db::getInstance();
		$table = $db->getNewTable();
		/* @var $table M_DbTableAdo */
		$table->setName(MediaAlbumMapper::TABLE_MEDIA_ALBUM_X_OBJECT);

		//add columns
		$columnModule = new M_DbColumn('module');
		$columnModule->setType(M_DbColumn::TYPE_VARCHAR);
		$columnModule->setLength(50);
		$table->addColumn($columnModule);
		
		$columnObject = new M_DbColumn('object');
		$columnObject->setType(M_DbColumn::TYPE_VARCHAR);
		$columnObject->setLength(50);
		$table->addColumn($columnObject);
		
		$columnObjectId = new M_DbColumn('object_id');
		$columnObjectId->setType(M_DbColumn::TYPE_INTEGER);
		$columnObjectId->setLength(11);
		$table->addColumn($columnObjectId);

		$columnAlbum = new M_DbColumn('album_id');
		$columnAlbum->setType(M_DbColumn::TYPE_INTEGER);
		$columnAlbum->setLength(11);
		$table->addColumn($columnAlbum);
		
		//add index
		$indexAlbum = new M_DbIndex('primary');
		$indexAlbum->addColumn($columnModule);
		$indexAlbum->addColumn($columnObject);
		$indexAlbum->addColumn($columnObjectId);
		$indexAlbum->setType(M_DbIndex::PRIMARY);
		$table->setPrimaryKey($indexAlbum);
		
		$table->create();
		
		//check if table exists
		return $db->hasTable($table->getName());
	}
}