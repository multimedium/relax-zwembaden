<?php
/**
 * MediaAlbumListViewHelper
 *
 */
class MediaAlbumListViewHelper {

	/**
	 * Add Columns to the News View
	 *
	 */
	public function addColumns() {
		return array(
			new AdminListColumnDefinition('Attached to', 1),
			new AdminListColumnDefinition('Contains #media', 2),
		);
	}
	
	/**
	 * Set column value
	 * 
	 * Manipulates the values shown in the list view. A string is 
	 * expected from this method.
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param AdminListColumnDefinition $column
	 * 		The column for which to set the value
	 * @param M_DataObject $dataObject
	 * 		The instance for which to set the value
	 * @param mixed $value
	 * 		The value that has been prepared by {@link AdminListView}.
	 * 		This will be the listed value, if not manipulated by this
	 * 		method.
	 * @return string|null
	 */
	public function setColumnValue(AdminListColumnDefinition $column, MediaAlbum $dataObject, $value) {
		
		switch($column->getIndex()) {
			case 0:
				return $value;
			case 1:
				if($dataObject->getParentAlbumId()){
					$mediaAlbumMapper = M_Loader::getDataObjectMapper('MediaAlbum', 'media');
					$parent = $mediaAlbumMapper->getOneByFieldWithRegistryCache(
						'id',
						$dataObject->getParentAlbumId()
					);
					return $parent->getTitle();
				}
				return '';
			case 2:
				$cnt = $dataObject->getMediaCount();
				return ($cnt == 0) ? '0' : (string)$cnt;
			//published: don't allow publish for non-deletable albums
			case 3:
				if(!$dataObject->isDeletable()) return " ";
			default:
				return $value;
		}
	}
	
	/**
	 * Set row options
	 * 
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param M_DataObject $dataObject
	 * 		The instance for which to set the row options
	 * @return ArrayIterator|null
	 */
	public function setRowOptions(M_DataObject $dataObject) {
		if($dataObject->isDeletable() == 0) {
			//add edit button
			$array[] = '<a href="'. M_Request::getLink('admin/edit/' . 
			$dataObject->getMapper()->getModule()->getId() . '/' . 
			$dataObject->getId() . '/' . 
			$dataObject->getMapper()->getModule()->getDataObjectIdOfObject($dataObject)
			. '" class="edit">' . t('Edit this item') . '</a>');
			
			//add option to go directly to media-page with this media-album
			//as filter
			$array[] = $this->_getRowOptionMedia($dataObject);
			
			return $array;
		}
	}
	
	/**
	 * Add options to this row
	 * 
	 * @return array
	 */
	public function addRowOptions(M_DataObject $dataObject) {
		return array($this->_getRowOptionMedia($dataObject));
	}
	
	/**
	 * Get the row option which will link to the media-page with a filter
	 * for this media-album
	 * 
	 * @return string
	 */
	private function _getRowOptionMedia(M_DataObject $dataObject) {
		$uri = new M_Uri(M_Request::getLink('admin/overview/media/media'));
		$uri->setQueryVariable('f1', 0);
		$uri->setQueryVariable('o1', 'EQ');
		$uri->setQueryVariable('v1', $dataObject->getId());
		return '<a href="'. $uri->getUri() .'" class="media" title="'.t('View media for this album').'">'.t('View media for this album').'</a>';
	}
}