<?php

class MediaAlbumAutocompleteAjaxView  {

    /**
     * The media-albums to display
     *
     * @var ArrayIterator
     */
    protected $_mediaAlbums;
    
    /**
     * Search string
     *
     * @var string
     */
    protected $_search;
    
	/**
	 * Set the tag for this view
	 *
	 * @param ArrayIterator $mediaAlbums
	 * @return void
	 */
	public function setMediaAlbums(ArrayIterator $mediaAlbums) {
	    $this->_mediaAlbums = $mediaAlbums;
	}
	
	/**
	 * Set the mediaAlbum after which we search after
	 *
	 * @param string $search
	 */
	public function setSearch($search) {
	    $this->_search = $search;
	}

	/**
	 * Get the resource for this view
	 *
	 * @return M_ViewHtmlResource
	 */
	public function display() {
	    /* @var $mediaAlbum MediaAlbum */
	    foreach ($this->_mediaAlbums AS $mediaAlbum) {
	        $text = new M_Text($mediaAlbum->getTitle());
	        $image = $mediaAlbum->getImages()->current();
	        $imageHtml = '';
	        if ($image instanceof Media ) $imageHtml = '<img src="'.M_Request::getLink().'/thumbnail/'.M_FieldMediaAlbum::THUMBNAIL_TYPE.'/'.M_FieldMediaAlbum::THUMBNAIL_SIZE.'/'.$image->getFilePath().'" style="vertical-align: middle;"/> ';
	        echo $imageHtml . $text->getSnippet('100') . '|' . $mediaAlbum->getId() . M_CodeHelper::getNewLine();
	    }
	}
}