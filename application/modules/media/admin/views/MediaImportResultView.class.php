<?php
class MediaImportResultView extends M_ViewHtml  {
	
	/**
	 * Get the resource for the media view
	 * 
	 * @return M_ViewHtmlResource
	 */
	public function getResource() {
		return new M_ViewHtmlResource(
			$this->_getResourcesPath() . '/templates/MediaImportResultView.tpl'
		);
	}

	/**
	 * Set the success parameter of a result
	 *
	 * @param flag $success
	 * @param string $error
	 */
	public function setSuccess($success, $error = NULL) {
		if(isset($error)) {
			$this->assign('error', $error);
		}
		$this->assign('success', $success);
	}
	
	/**
	 * Add the filename
	 *
	 * @param unknown_type $file
	 */
	public function setFile($file) {
		$this->assign('file', $file);
	}
	
	/**
	 * Add the original filename
	 *
	 * @param unknown_type $original
	 */
	public function setOriginal($original) {
		$this->assign('original', $original);
	}
	
	/**
	 * Get the resources path for the media module
	 *
	 * @return string
	 */
	protected function _getResourcesPath() {
		return AdminLoader::getAdminResourcesPath('media');
	}
	
}