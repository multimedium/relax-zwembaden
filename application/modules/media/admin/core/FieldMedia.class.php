<?php
class M_FieldMedia extends M_FieldSelect  {

	/**
	 * @var int
	 */
	protected $_height;
	
	/**
	 * @var int
	 */
	protected $_width;
	
	/**
	 * @return int
	 */
	public function getHeight() {
		return (is_null($this->_height)) ? 170 : $this->_height;
	}
	
	/**
	 * @param int $_height
	 */
	public function setHeight($_height) {
		$this->_height = $_height;
	}
	
	/**
	 * @return int
	 */
	public function getWidth() {
		return (is_null($this->_width)) ? 150 : $this->_width;
	}
	
	/**
	 * @param int $_width
	 */
	public function setWidth($_width) {
		$this->_width = $_width;
	}
	
	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldMedia} object,
	 * which allows {@link M_FieldMedia} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @return M_ViewFieldMedia
	 */
	public function getInputView() {
	    M_Loader::loadRelative(
	        M_Loader::getModulePath('media') .
            DIRECTORY_SEPARATOR .  
            'admin' .
            DIRECTORY_SEPARATOR .  
			'core' .
            DIRECTORY_SEPARATOR .  
	        	'ViewFieldMedia.class.php', 
	        true
	    );
	    
	    //set the options
		M_Loader::loadDataObjectMapper('Media', 'media');
		$mediaMapper = new MediaMapper();
		$filter = new M_DbQueryFilterOrder('dateAdded');
		$mediaMapper->addFilter($filter);
		
		//check if a album has been selected
		$albumId = M_Request::getVariable('albumFilter');
		
		//filter on media album?
		if ((int)$albumId > 0) {
			$mediaObjects = $mediaMapper->getMediaByAlbumId(
				$albumId
			);
		}else {
			$mediaObjects = $mediaMapper->getAll();
		}
		
		//get the file previews
		$mediaPreview = array();
		$i = 0;
		foreach($mediaObjects AS $media) {
			/* @var $media Media */
			$filePreview = new AdminViewFilePreview();
			$filePreview->setFile($media->getFile());
			$filePreview->setHeight($this->getHeight());
			$filePreview->setWidth($this->getWidth());
			$this->setItem($i ++, $filePreview);
		}
		
		return new M_ViewFieldMedia($this);
	}
}