<?php
class M_FieldMediaAlbumAutocomplete extends M_FieldAutoComplete {

	/**
	 * Get input control view
	 * 
	 * This method overrides {@link M_Field::getInputView()}.
	 * 
	 * This method provides with an {@link M_ViewFieldAutocomplete} object,
	 * which allows {@link M_Field} to include the input control 
	 * in the view that is returned by {@link M_Field::getView()}.
	 * 
	 * @access public
	 * @see M_Field::getView()
	 * @return M_ViewFieldMediaAlbumAutocomplete
	 */
	public function getInputView() {
		if ($this->getDescription() == false) {
			$this->setDescription(t('Type to find media albums'));
		}
		
		$this->setAjaxUrl(
			M_Request::getLink('admin/route/media/autocompleteMediaAlbumFieldAjax')
		);
		$this->setMultiple(false);
		$this->setHasTextValue(true);
		
		$view = new M_ViewFieldMediaAlbumAutocomplete($this);
		$view->setId($this->getId());
		$view->setModuleOwner($this->getModuleOwner());
		
		return $view;
	}
}