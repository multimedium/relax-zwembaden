<?php
class M_FieldMediaItem extends M_Field {

	/**
	 * Get input control view
	 *
	 * This method overrides {@link M_Field::getInputView()}.
	 *
	 * This method provides with an {@link M_ViewFieldAutocomplete} object,
	 * which allows {@link M_Field} to include the input control
	 * in the view that is returned by {@link M_Field::getView()}.
	 *
	 * @access public
	 * @see M_Field::getView()
	 * @return M_ViewFieldMediaAlbumAutocomplete
	 */
	public function getInputView() {
		if ($this->getDescription() == false) {
			$this->setDescription(t('Please select a media-item'));
		}

		$view = new M_ViewFieldMediaItem($this);
		$view->setId($this->getId());
		$view->setModuleOwner($this->getModuleOwner());

		return $view;
	}

	/**
	 * Get default thumbnail
	 * 
	 * @return string
	 */
	public function getDefaultThumbnail() {
		$media = $this->getMedia();
		if($media) {
			M_Request::getLink('thumbnail/square/30/' . $media->getFilePath() . '/' . $media->getBasename());
		} else {
			return FALSE;
		}
	}

	/**
	 * Get media
	 * 
	 * @return Media $media
	 */
	public function getMedia() {
		if($this->getValue()) {
			return M_Loader::getDataObjectMapper('Media', 'media')
					->getOneByFieldWithRegistryCache('id', $this->getValue());
		} else {
			return FALSE;
		}
	}
}