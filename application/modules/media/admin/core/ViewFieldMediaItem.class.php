<?php

class M_ViewFieldMediaItem extends M_ViewField {

	/* -- PROTECTED/PRIVATE -- */

	protected function getHtml() {
		/* No specific HTML */
		return false;
	}

	/**
	 * Get resource
	 * 
	 * @return string
	 */
	protected function getResource() {
		$js = M_ViewJavascript::getInstance();
		$js->addInline('var fieldMediaItemId = "' . $this->getField()->getId() . '";');
		
		return $this->_getResourceFromModuleOwner('core-form/FieldMediaItem.tpl');
	}

}