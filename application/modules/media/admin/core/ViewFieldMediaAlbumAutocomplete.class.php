<?php

class M_ViewFieldMediaAlbumAutocomplete extends M_ViewFieldAutocomplete {

	

	/**

	 * Get html for media-album

	 *

	 * We overwrite the getHtml function since we want to adjust the way

	 * how the received data is stored in the textValue field (remove <img/>)

	 * 

	 * @return string

	 */

	public function getHtml() {

		$html = parent::getHtml();

		$field = $this->getField();

		//create javascript

		$suffix = ($field->getHasTextValue()) ? self::TEXTVALUE_SUFFIX : '';



		$javascript = '$("#'. $this->getId() . $suffix . '").autocomplete("'.$field->getAjaxUrl().'", {

       		width: '.$field->getWidth().',

       		max: '.$field->getLimit().',

       		multiple: '.$field->getMultiple().',

       		minChars: '.$field->getMinChars().'}).result(function(event, item) {

				var $label = item[0];

				if(parseInt($label.indexOf("/>")) == -1) {

					$("#'. $this->getId() . $suffix . '").val($label);

				} else {

					$("#'. $this->getId() . $suffix . '").val($label.substring(parseInt($label.indexOf("/>")+3), $label.length));

				}

				$("#'. $this->getId() . '").val(item[1]);

			});';

		

		$javascriptInstance = M_ViewJavascript::getInstance();

		$javascriptInstance->addInline($javascript, 'form-autocomplete-'.$this->getId());

		

		return $html;

	}

	

	/**

	 * Get the resource from the module

	 * 

	 * @return M_ViewHtmlResource 

	 */

	protected function getResource() {

		return $this->_getResourceFromModuleOwner('form/FieldMediaAlbumAutocomplete.tpl');

	}

}