<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="breadcrumb">
		<ul>
			<li><a href="{link href="admin/overview/media/"}">Media</a></li>
			<li><span>{t text="Upload multiple files"}</span></li>
		</ul>
	</div>
	
	<div class="importMediaDescription">
		{t text="You are using the multiple upload form: you can add multiple items at once."}<br />
		<a href="{link}/admin/edit/media/0/media">{t text="Click here if you want to use the standard upload form."}</a>
	</div>
	
	{$form->setModuleOwner('admin')}
	{$form->getView()}
</div>