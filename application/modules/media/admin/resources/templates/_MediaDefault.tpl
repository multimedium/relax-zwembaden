{assign var="file" value=$media->getFile()}
{assign var="fileExtension" value=$file->getFileExtension()|strtolower}

{if $fileExtension == 'pdf'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/pdf.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'xls' || $fileExtension == 'xlsx' || $fileExtension == 'csv'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/excell.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'doc' || $fileExtension == 'docx'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/word.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'txt'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/txt.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'ppt' || $fileExtension == 'pps' || $fileExtension == 'pptx' || $fileExtension == 'ppsx'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/powerpoint.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'mdb' || $fileExtension == 'mdbx'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/access.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'pub' || $fileExtension == 'pubx'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/publisher.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'fla'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/flash-fla.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'swf'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/flash-swf.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'key'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/keynote.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'numbers'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/numbers.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'pages'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/pages.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'mp3' || $fileExtension == 'mp4' || $fileExtension == 'aac' || $fileExtension == 'wma'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/music.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'odf' || $fileExtension == 'ods' || $fileExtension == 'odp' || $fileExtension == 'odt'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/openoffice.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'psd'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/photoshop.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'rtf'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/rtf.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'mpg' || $fileExtension == 'mpeg' || $fileExtension == 'wmv' || $fileExtension == 'avi'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/video.png" alt="" title="{$media->getTitle()}" /></div>
{elseif $fileExtension == 'zip' || $fileExtension == 'rar' || $fileExtension == 'sit' || $fileExtension == 'sitx' || $fileExtension == 'gz' || $fileExtension == 'gzip' || $fileExtension == 'tar'}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/archive.png" alt="" title="{$media->getTitle()}" />{$media->getTitle()}</div>
{else}
	<div class="media-item-icon"><img src="{link prefix="false"}/application/modules/media/admin/resources/images/file-icons/file.png" alt="" title="{$media->getTitle()}" /></div>
{/if}
<div class="media-item-title">
	{snippet from=$media->getTitle() length="15"} - <strong>{$fileExtension}</strong>
</div>
<span class="buttonsBar">
	<ul>
		<li>
			<a href="{link href="admin/delete/media/"}{$media->getId()}" class="confirm delete" title="{t|att text="Delete this media item"}">
				{t text="Delete this media item"}
			</a>
		</li>
		<li>
			<a href="{link href="admin/edit/media/"}{$media->getId()}" class="edit" title="{t|att text="Edit this item"}">
				{t text="Edit this item"}
			</a>
		</li>
			<li>
				<a href="{link prefix="false"}/{$media->getFilepath()}" class="zoom" title="{$media->getTitle()|att}">
					{t text="Download this file"}
				</a>
			</li>
		<li>
			<a href="{link href="admin/toggle/media/media/"}{$media->getId()}/published" class="toggle-{if $media->getPublished()}1{else}0{/if}" title="{t|att text="Activate item?"}">
				{t text="Activate item?"}
			</a>
		</li>
	</ul>
</span>
