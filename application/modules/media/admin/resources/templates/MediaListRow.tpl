{foreach from=$instances item="instance"}
	<li id="idListedInstance{$instance.object->getId()}">
		{if $instance.object->getType() == 'image'}
			{assign var="mediaTpl" value="_MediaImage.tpl"}
		{else}
			{assign var="mediaTpl" value="_MediaDefault.tpl"}
		{/if}
		{include file=$mediaTpl media=$instance.object}
	</li>
{/foreach}