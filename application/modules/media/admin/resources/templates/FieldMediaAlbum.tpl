{if $albums|@count > 0}
{assign var="prefix" value="fieldMediaAlbum-"}
<div class="fieldMediaAlbum">
	{* Drop down *}
    <a id="{$prefix}{$field->getId()}" href="#" class="mediaAlbum popmenu">

	{* if there is an album present we can show it already *}
	{if $mediaAlbum}
		{assign var="defaultValue" value=$mediaAlbum->getId()}
		{assign var="media" value=$mediaAlbum->getImages()}
		{assign var="mediaCount" value=$mediaAlbum->getImages()|@count}
		
		{if $mediaCount > 0}
			{assign var="mediaItem" value=$media->current()}
			<img src="{link}/thumbnail/square/30/{$mediaItem->getFilePath()}" alt="{t text="Media file"}"/>
		{/if}
			{$mediaAlbum->getTitle()}
		({$mediaCount})
	{else}
		{assign var="defaultValue" value=""}
		{t text="Choose your album"}
	{/if}
	</a>
	
	{* store the value in a hidden field *}
	<input type="hidden" name="{$field->getId()}" value="{$defaultValue}"/>
</div>

<div id="{$prefix}{$field->getId()}Popmenu" rel="{$prefix}{$field->getId()}" class="mediaAlbumPopmenu hidden">
	<ul>
		{* Add an empty option *}
		<li><a href="0"><em>{t text="No album"}</em></a></li>
		{foreach from=$albums item="a"}
		{assign var="images" value=$a->getImages()}
		{assign var="imageCount" value=$a->getImages()|@count}
		{assign var="mediaCount" value=$a->getMediaCount()}
		<li>
		<a href="{$a->getId()}" title="{t text="Click to select this album"}">
		{if $imageCount > 0}
			{assign var="image" value=$images->current()}
			<img src="{link}/thumbnail/square/30/{$image->getFilePath()}" alt="{t text="Image file"}"/>
		{/if}
			{$a->getTitle()} ({$mediaCount})
		</a>
		</li>
		{/foreach}
	</ul>
</div>

{else} 
	{* No albums exist yet *}
	{t text="No albums available"}
{/if}

<div class="clear"></div>