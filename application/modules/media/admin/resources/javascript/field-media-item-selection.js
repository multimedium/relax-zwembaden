$(document).ready(function() {
	// Add cursor to each list-item
	updateMediaList();
	
	$('#idContentPane').bind('M_LoadInContentPane_Update', function($eventObject, url) {
		updateMediaList();
	});

	// When a new media-list object is created, we remove the buttons-bar and add
	// a cursor pointer to the item
	$('#idMediaList').live('M_LiveList_NewObject', function($event, $object) {
		$('.buttonsBar', $object).remove();
		$object.css('cursor', 'pointer');
	});

	// Click on media list: update the id in the parent window
	$('#idMediaList li').live('click', function() {
		var $id = $(this).attr('id').replace('idListedInstance', '');
		var $img = $('.media-item-icon img', this).attr('src');

		// We trigger the event so the template-editor is warned about the clicking event
		parent.$('#' + fieldMediaItemId).trigger("M_Media_Selected", [$id, $img]);
	});
});

function updateMediaList() {
	$('#idMediaList li').css('cursor', 'pointer');
	$('.buttonsBar').remove();
}