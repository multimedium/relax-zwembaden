$(document).ready(function() {
	// Initialize the fancybox on the '' anchor
	$('.field-media-item-button').fancybox({
		width: '80%',
		height: '80%',
		overlayShow: true,
		type: 'iframe',
		hideOnContentClick: false,
		hideOnOverlayClick: false,
		onComplete: function() {
			$(document).trigger('M_Media_IframeShow');
		},
		onClosed: function() {
			// unbind all Events!
			$(document).unbind('M_Media_IframeShow');
		}
	});

	// Bind an event
	$('#' + fieldMediaItemId).bind('M_Media_Selected', function($event, $id, $src) {

		// Create a small square thumbnail
		$src = $src.replace('square/150/', 'square/30/');
		$('.field-media-item-thumb').html('<img src="' + $src + '" alt="" />');

		// Set the value of the field
		$('#' + fieldMediaItemId).val($id);

		// Close the fancybox
		$.fancybox.close();
	});
});