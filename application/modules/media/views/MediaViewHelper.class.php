<?php
/**
 * Class MediaViewHelper
 * 
 * Some methods to improve Media-workflow
 * 
 * @author b.brughmans
 *
 */
class MediaViewHelper {
	
	/**
	 * Prepare a text to be used with media items
	 * 
	 * Due to path-issues media files won't be displayed correctly in a website.
	 * By consequence we need to update the href-attribute for the <a> element
	 * and the src-attribute for the image. The replacement itself is done by
	 * {@MediaViewHelper::_prepareMedia}
	 * 
	 * This method uses a definition to parse the width, height, and other 
	 * settings
	 * 
	 * @param string $text 
	 * 		The text to be searched in
	 * @param string $definition 
	 * 		Definition used to display the image
	 * @param string $href 
	 * 		Definition used to display when clicking on the image
	 * @return string $text The updated text
	 */
	public static function prepareTextForMediaDefinition($text, $definition, $href = null) {
	
		//check if a text is given
		if( strlen((string)$text) == 0) return $text;
		
		M_Loader::loadModel('ThumbnailModel', 'thumbnail');
		
		//set the new image path
		$imageUrl = M_Request::getLink('thumbnail') . '/'. $definition;
		
		//set the href url, if none is defined we use the href from the definition (link to real-file)
		$hrefUrl = null;
		if (!is_null($href)) {
			$hrefUrl = M_Request::getLink('thumbnail') . '/'. $href;
		}

		//get the path where images for this definition are stored
		$definitionConfig = ThumbnailModel::getInstance()->getDefinition($definition);
		if(!isset($definitionConfig->directory) || $definitionConfig->directory == '') {
			$path = M_Application::getConfigOfModule('media')->path;
		}else {
			$path = $definitionConfig->directory;
		}
		
		return self::_prepareText($text, $imageUrl, $hrefUrl, $path);
	}

	/**
	 * Prepare a text to be used with media items
	 * 
	 * Due to path-issues media files won't be displayed correctly in a website.
	 * By consequence we need to update the href-attribute for the <a> element
	 * and the src-attribute for the image. The replacement itself is done by
	 * {@MediaViewHelper::_prepareMedia}
	 *
	 * @param string $text 
	 * 		The text to be searched in
	 * @param string $thumbnail 
	 * 		Howto display thumbnails (resized/square)
	 * @param int $width 
	 * 		Thumbnail width
	 * @param int $height 
	 * 		Thumbnail height (only applicable when $thumbnail == resized)
	 * @param string $href
	 * 		Href-definition, only used when a definition is set
	 * @return string $text The updated text
	 */
	public static function prepareTextForMedia($text, $thumbnail, $width, $height = null) {
		//check if a text is given
		if (is_string($text) == false || strlen((string)$text) == 0) return $text;
		
		$module = new M_ApplicationModule('media');

		//set the new image path
		$imageUrl = M_Request::getLink('thumbnail') . '/'. $thumbnail . '/' . (int)$width .'/';
		if ($thumbnail == 'resized') $imageUrl .=  (int)$height . '/';
		$imageUrl .= $module->getConfig()->path;
		
		//set the new href path
		$hrefUrl = M_Request::getLink('thumbnail/'.$module->getConfig()->imageLinkPrefix);
		
		//set the absolute image path
		$imagePath = M_Loader::getAbsolute($module->getConfig()->path);
		
		return self::_prepareText($text, $imageUrl, $hrefUrl, $imagePath);
	}
	
	/**
	 * Prepare a text for media
	 *
	 * @param string $text
	 * @param string $imageUrl
	 * @param string $hrefUrl
	 * @param string $imagePath
	 * 		If null, the image won't be checked if it still exists. If not null,
	 * 		we remove the tag from the text
	 */
	protected static function _prepareText($text, $imageUrl, $hrefUrl = null, $imagePath = null) {
		//check if a text is given
		if (is_string($text) == false || strlen((string)$text) == 0) return $text;
		
		$dom = new M_HtmlDom($text);
		//find all media-href and image elements which link to media
		/* @var $href simple_html_dom_node */
		foreach($dom->find('a[href*=thumbnail],a[href*=media],img[src*=media], img[class*=media]') AS $href) {
			$old = $href->__toString();
			
			try {
				$new = self::_prepareMedia($href, $hrefUrl, $imageUrl, $imagePath);
				//we only replace if preparation succeeded
				if ($new !== false) {
					$text = str_replace($old,$new->__toString(),$text);
				}
			} catch (M_Exception $e) {
				M_Console::getInstance()->write($e->getMessage());
				//remove the tag completely when preparation failed
				$text = str_replace($old,'',$text);
			}
		}
		
		//clear memory
		$dom->clear();
		
		// return final text:
		return $text;
	}
	
	/**
	 * Does the replacement for {@link MediaViewHelper::prepareTextForMedia()}
	 * 
	 * @param M_HtmlDomNode $element The dom-element to be updated
	 * @param string $hrefUrl The new href-path
	 * @param string $imageUrl The new image-url
	 * @param string $imagePath The new image-path
	 * @return simple_html_dom_node $element The updated dom-element
	 */
	protected static function _prepareMedia($element, $hrefUrl = null, $imageUrl, $imagePath) {
		//if we found an image we can proceed
		if ($element->tag == 'img') {
			$image = $element;
		}
		//if not, we check the child of the href we found
		else {
			//get the image and it's src attribute
			/* @var $image M_HtmlDomNode */
			$image = $element->firstChild();
			//if the href doesn't contain an image we stop processing
			if ($image->tag != 'img') {
				return false;
			}
		}
		
		//get the image-source attribute
		$src = basename($image->getAttribute('src'));

		M_Loader::loadDataObjectMapper('Media', 'media');
		$mediaMapper = new MediaMapper();
		
		/* @var $mediaMapper MediaMapper */
		$media = $mediaMapper
					->addFilter(new M_DbQueryFilterWhere('filePath', $imagePath.'/'.$src))
					->getOne();

		
		//check if image still exists
		if ($imagePath) {
			$imageObject = new M_Image($imagePath . '/' . $src);
			if ($imageObject->exists() == false) {
				throw new M_Exception(
					sprintf('Image with path %s does not exist', $hrefUrl.'/'.$src)
				);
			}
		}

		/**
		 * If a media-item is found for this file: we rename the file
		 * so it uses the title from the media-item (for SEO purposes)
		 */
		if($media) {
			$src = $media->getId() . '/' . $media->getBasenameFromTitle();
		}

		//update image
		$image->setAttribute('src', $imageUrl . '/' . $src);
		
		//add wysiwyg styles to href
		$imageClass = trim($image->getAttribute('class'));
		
		//update href
		$hrefAttrib = $hrefUrl . '/' . $src;
		if ($element->tag == 'a' && !is_null($hrefUrl)) {
			$class = $element->getAttribute('class') . ' media' . ' ' . $imageClass;
			
			$element->setAttribute('class', $class);
			$element->setAttribute('href', $hrefAttrib);
			$element->setAttribute('rel', 'media');
		}
		
		//add a href when this is an image
		elseif ($element->tag =='img' && $element->getParent()->tag != 'a') {
			
			//get the title from the media item and use it as title and alt
			//attribute
			if ($media) {
	            $mediaText = new M_Text($media->getTitle());
				$alt = $mediaText->toHtmlAttribute();
				/* @var $media Media */
				$title = ' title="'.$alt.'"';
				$image->setAttribute('alt', $alt);
			}
			
			//set the default classes
			$elementClass = 'lightbox fancybox media';
			
			//add the classes which are applied to the image (using wysiwyg)
			if ($imageClass != "") $elementClass .= ' ' . $imageClass;

			// if $hrefUrl-element has been set: add a link to the image element
			if (!is_null($hrefUrl)) {
				$element->setOuterText('<a'.$title.' class="'.$elementClass.'" rel="media" href="'.$hrefAttrib.'">'.$element->toString().'</a>');
			}
		}
		
		return $element;
	}
	
	/**
	 * Get module
	 *
	 * @return M_ApplicationModule
	 */
	protected  static function _getModule() {
		static $module;
		
		if (is_null($module)) {
			$module = new M_ApplicationModule('media');
		}
		
		return $module;
	}
}