<?php
/**
 * InstallLocaleView class
 *
 * @package App
 * @subpackage Install
 */
class InstallLocaleView extends M_ViewHtml {
	/**
	 * Set available locales
	 * 
	 * Can be used to set the collection of locales that may be installed in the
	 * application. This method expects an iterator, of which the keys are the 
	 * standard ISO-Codes of the locales, and the values the names.
	 * 
	 * @access public
	 * @param ArrayIterator $locales
	 * 		The collection of locales that are available for installation, in the
	 * 		application.
	 * @return void
	 */
	public function setAvailableLocales(ArrayIterator $locales) {
		$this->assign('locales', $locales);
	}
	
	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('install').'/InstallLocale.tpl');
	}
}