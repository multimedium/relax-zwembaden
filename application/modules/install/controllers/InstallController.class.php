<?php
class InstallController extends M_Controller {
	/**
	 * Set the routing for this module
	 * 
	 * @return array
	 */
	public function __router() {
		return array(
			'locale' => 'locale',
			'acl' => 'acl',
			'(:string)/(:string)' => '$1/$2',
			'(:string)' => 'index/$1',
		);
	}
	
	/**
	 * Index action
	 *
	 * @param str $moduleName
	 */
	public function index($moduleName) {
		$this->_installModule((string)$moduleName);
	}
	
	/**
	 * Install locale(s)
	 * 
	 * @access public
	 * @return void
	 */
	public function locale() {
		// Check if new locale should be installed:
		$loc = M_Request::getVariable('locale', FALSE, M_Request::TYPE_STRING);
		
		// If so, first we make sure that the locale has not yet been installed:
		if($loc && ! M_LocaleMessageCatalog::isInstalled($loc)) {
			// To install the locale, we fetch the message catalog's properties
			// from the PO File:
			$file = new M_File(M_Loader::getResourcesPath('install') . '/po/' . $loc . '.po');
			
			// Default the PO File to "default.po", if no specific PO File exists
			// for the selected locale:
			if(! $file->exists()) {
				$file = new M_File(M_Loader::getResourcesPath('install') . '/po/default.po');
			}
			
			// Load the PO File:
			$po = new M_LocalePortableObject($file);
			
			// Install the message catalog:
			if(M_LocaleMessageCatalog::installLocale($loc, $po->getNumberOfPluralForms(), $po->getPluralFormula())) {
				echo 'Installed Locale <strong>'. $loc .'</strong> with PO File: <strong>'. $file->getPath() .'</strong>!!';
			}
		}
		
		// Get the locale data (in english)
		$data = M_LocaleData::getInstance('en');
		
		// Show interface, to install a new locale:
		M_Loader::loadView('InstallLocaleView', 'install');
		$view = new InstallLocaleView();
		$view->setAvailableLocales($data->getLanguages());
		$view->display();
	}

	/**
	 * Reinstall the resources and add custom resources too
	 */
	public function acl() {
		M_Loader::loadController('AdminControllerHelper', 'admin');
		M_Loader::loadController('InstallAdministratorController', 'administrator');
		AdminControllerHelper::loadAdminCoreClasses();
		InstallAdministratorController::acl();
		InstallAdministratorController::aclCustom();
	}
	
	/**
	 * Install a module
	 * 
	 * @param str
	 * 		The modulename
	 * @return void
	 */
	protected function _installModule($moduleName) {
	     //create temporary files-folder to store all model-files
		$directory = M_Fs::getUniqueTemporaryDirectory();

		//create files
		$module = new M_ApplicationModule($moduleName);
		$this->_createFiles($module, $directory);
		
		//check dependencies
		foreach($module->getDependencies() AS $module) {
			if ($this->_createFiles($module, $directory) == false) {
				throw new M_Exception(
					sprintf(
						'Module %s could not be installed', 
						$module->getId()
					)
				);
				break;
			}
		}
		
		//check if models were created
		if (count($directory->getItems()) > 0) {
			//create archive
			$archive = new M_Archive(
				M_Fs::getUniqueTemporaryDirectory()->getPath() . 
				DIRECTORY_SEPARATOR . 
				'install_'. $moduleName . '.zip'
			);
			
			//use archiver to add items
			$archiver = new M_Archiver();
			$archiver->setItems($directory->getItems());
			
			$archiver->compress($archive, $directory->getPath());
			
			//delete tmp directory which contains the files
			$directory->delete();

			//output to user
			echo sprintf(
				'Please download the zip-file <a href="%s">here</a> and don\'t forget to clean up afterwards.<br/><br/>
				If you\'re done saving the files in their correct location please continue to <a href="%s">install</a> the modules.', 
				M_Request::getLinkWithoutPrefix(M_Loader::getRelative($archive->getPath())),
				M_Request::getLink('install/step2/'.$moduleName));
		}
		
		//delete tmp directory
		else {
			$directory->delete();
		}
	}
	
	/**
	 * After the files have been created, we only need to install the modules
	 * themselves e.g. create database, or take other actions...
	 *
	 * @param str $moduleName
	 */
	public function step2($moduleName) {
		try {
			$this->_install(new M_ApplicationModule($moduleName));
		}catch (Exception $e) {
			throw $e;
		}
		
		echo sprintf('Module %s was succesfully installed!', $moduleName);
	}
	
	/**
	 * Install a module
	 *
	 * @param M_ApplicationModule $module
	 */
	protected function _install(M_ApplicationModule $module) {
		if ($module->install()) {
			//check dependencies
			foreach($module->getDependencies() AS $moduleDependant) {
				$this->_install($moduleDependant);
			}
		}
	}
	
	/**
	 * Create files for a module
	 *
	 * @param M_ApplicationModule $module
	 * 			The module which will be installed
	 * @param M_Directory $directory
	 * 			The directory in which files will be created
	 * @return bool
	 */
	protected function _createFiles(M_ApplicationModule $module, M_Directory $directory) {
		
		foreach($module->getDataObjectNames() AS $dataObjectName) {
			$this->_createModel($module, $dataObjectName, $directory);
		}
		
		return true;
	}
	
	/**
	 * Create model files
	 * 
	 * @param M_ApplicationModule $module
	 * @param str $objectName
	 * @param M_Directory $directory
	 * @todo use M_Code
	 */
	protected function _createModel( M_ApplicationModule $module, $objectName, M_Directory $directory ) {
		//create a subfolder for this module
		$moduleDirectory = new M_Directory(
			$directory->getPath() . DIRECTORY_SEPARATOR . $module->getId()
		);
		
		if ($moduleDirectory->exists() == false) {
			$moduleDirectory->make();
		}
		$moduleDirectory->setPermissions(M_FsPermissions::constructWithOctal(0777));
		
		//create mapper
		$mapperSource = $this->_createMapperSource($objectName, $module);
		$mapperFilename = $this->_createMapperFilename($objectName);

		//set mapper contents and save file
		$mapperFile = new M_File(
			$moduleDirectory->getPath() . DIRECTORY_SEPARATOR . $mapperFilename
		);
		$mapperFile->setContents($mapperSource);
		$mapperFile->setPermissions( M_FsPermissions::constructWithOctal(0777));
		
		$mapperClassName = $this->_createMapperClassName($objectName);
		
		//maybe this class is allready using in index, or another included file?
		//Note: this would normally never be the case, only when e.g. a module
		//is being reinstalled, and some mapper constants are being used for
		//routing or other purposes
		try {
			class_exists($mapperClassName);
		}catch(M_Exception $e) {
			M_Loader::loadAbsolute($mapperFile->getPath());
		}
			
		$mapper = new $mapperClassName;
		
		//create dataobject
		$objectFile = new M_File(
			$moduleDirectory->getPath() . 
			DIRECTORY_SEPARATOR . 
			$objectName . 'DataObject.class.php'
		);
		$objectFile->setContents($this->_createDataObjectSource($objectName,$mapper));
		
		//create object
		$objectFile = new M_File(
			$moduleDirectory->getPath() . 
			DIRECTORY_SEPARATOR . 
			$objectName . '.class.php'
		);
		$objectFile->setContents($this->_createObjectSource($objectName,$module));
	}
	
	/**
	 * Create source for an empty dataobjectmapper
	 * 
	 * @param str $objectName
	 * @param M_ApplicationModule $module
	 * @return str
	 * @todo use M_Code
	 */
	private function _createMapperSource($objectName, M_ApplicationModule $module) {
		//create class code
		$comment = new M_CodeComment();
		$comment->setTitle($objectName.'Mapper');
		$class = new M_CodeClass();
		$class->setName($objectName.M_DataObjectMapper::MAPPER_SUFFIX);
		$class->setParentClassName('M_DataObjectMapperCache');
		$method = new M_CodeFunction('_getModuleName');
		$method->setBody("return '".$module->getId()."';");
		$method->setAccess(M_CodeAccess::TYPE_PROTECTED);
		$class->addMethod($method);
		$class->setComments($comment);
		
		$str = "<?php
".$class->toString();
		return $str;
	}
	
	/**
	 * Create the source for the standard DataObject
	 *
	 * @param str $objectName
	 * @param M_DataObjectMapper $mapper
	 * @return str
	 */
	private function _createDataObjectSource($objectName, M_DataObjectMapper $mapper) {
		$factory = new M_DataObjectClassFactory($mapper);
		return $factory->getSourceOfObjectClass($objectName . 'DataObject');
	}
	
	/**
	 * Creat the source for the personal DataObject
	 *
	 * @param unknown_type $objectName
	 * @param M_ApplicationModule $module
	 * @return unknown
	 */
	private function _createObjectSource($objectName, M_ApplicationModule $module) {
		//create class code
		$comment = new M_CodeComment();
		$comment->setTitle($objectName);
		$class = new M_CodeClass();
		$class->setName($objectName);
		$class->setComments($comment);
		$class->setParentClassName($objectName.'DataObject');
		
		//create file
		$str = "<?php
M_Loader::loadDataObject('".$objectName."DataObject','".$module->getId()."');
";
		$str .= $class->toString();

		return $str;
	}
	
	/**
	 * Create the filename for the file which holds the object-mapper
	 * 
	 * @param str $object
	 * @return str
	 */
	private function _createMapperFilename($objectName) {
		return $objectName.M_DataObjectMapper::MAPPER_SUFFIX.'.class.php';
	}
	
	/**
	 * Create the filename for a datamapper
	 *
	 * @param str $objectName
	 * @return str
	 */
	private function _createMapperClassName($objectName) {
		return $objectName.M_DataObjectMapper::MAPPER_SUFFIX;
	}
}