
<form action="{link href="install/locale"}" method="get">
	<select size="1" name="locale">
		{foreach from=$locales item="name" key="locale"}
			<option value="{$locale}"{if $locale == $smarty.get.locale} selected="selected"{/if}>{$name}</option>
		{/foreach}
	</select>
	<input type="submit" name="go" value="ok" />
</form>
