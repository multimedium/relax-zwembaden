<?php
/**
 * AdminLoginController
 * 
 * @package App
 * @subpackage Admin
 */
class AdminLoginController extends M_Controller {
	/**
	 * Login page
	 * 
	 * @access public
	 * @return void
	 */
	public function index() {
		// Load the Login Form, and Login Page View
		AdminLoader::loadForm('AdminLoginForm',     'login');
		AdminLoader::loadView('AdminLoginFormView', 'login');

		// Get redirect url:
		$redirect = $this->_getLoginRedirectUrl();
		
		// Construct an instance of the Login Page View, and provide
		// it with a constructed Login Form:
		$form = new AdminLoginForm();
		
		// First, set Redirection URL
		$form->setRedirectUrl(M_Form::OK, $redirect); 

		// Also, set route:
		$form->setVariable('route', $redirect);
		
		// If the form has not yet been sent, or validation errors have occurred:
		if(!$form->run()) {
			// Construct the view, to show the form:
			$view = new AdminLoginFormView;
			$view->setLoginForm($form);
			$view->display();
		}
	}
	
	/**
	 * Activate new password
	 * 
	 * @access public
	 * @return void
	 */
	public function activate() {
		$ok = FALSE;
		
		// Load the AdministratorMapper, from the admin module
		M_Loader::loadDataObjectMapper('Administrator', 'administrator');
		
		// Construct the mapper:
		$administratorMapper = new AdministratorMapper();
		
		// Add the filters, to look up the administrator:
		$administratorMapper->addFilter(new M_DbQueryFilterWhere('active', 1));
		$administratorMapper->addFilter(new M_DbQueryFilterWhere('newPasswordActivationUrl', M_Request::getUri()));
		
		// Fetch:
		$administrators = $administratorMapper->getAll();
		
		// We expect to receive one unique administrator account:
		if(count($administrators) == 1) {
			// Get the administrator:
			$administrator = $administrators->current();
			
			// Activate the new password:
			$administrator->setPassword($administrator->getNewPassword());
			
			// Remove the activation url
			$administrator->setNewPassword('');
			$administrator->setNewPasswordActivationUrl('');
			
			// Save the changes:
			if($administrator->save()) {
				// Construct the view:
				AdminLoader::loadView('AdminLoginPasswordActivationView', 'login');
				$view = new AdminLoginPasswordActivationView();
				$view->setAdministrator($administrator);
				$view->display();
				$ok = TRUE;
			}
		}
		
		// If expired (Administrator account not found):
		if(! $ok) {
			AdminLoader::loadView('AdminLoginPasswordExpiredView', 'login');
			$view = new AdminLoginPasswordExpiredView();
			$view->display();
		}
	}
	
	/**
	 * Password recovery page
	 * 
	 * @access public
	 * @return void
	 */
	public function passwordRecovery() {
		// Load the Login Form, and Login Page View
		AdminLoader::loadForm('AdminLoginPasswordRecoveryForm', 'login');
		AdminLoader::loadView('AdminLoginPasswordRecoveryView', 'login');
		
		// Construct an instance of the Login Page View, and provide
		// it with a constructed Password Recovery Form:
		$form = new AdminLoginPasswordRecoveryForm();
		
		// First, set Redirection URL
		$form->setRedirectUrl(M_Form::OK, M_Request::getLink('admin/route/login/passwordSent'));
		$form->setRedirectUrl(M_Form::KO, M_Request::getLink('admin/route/login/passwordRecoveryError'));
		
		// If the form has not yet been sent, or validation errors have occurred:
		if(! $form->run()) {
			// Construct the view, to show the form:
			$view = new AdminLoginPasswordRecoveryView;
			$view->setPasswordRecoveryForm($form);
			$view->display();
		}
	}
	
	/**
	 * Password recovery: Thank you page
	 * 
	 * @access public
	 * @return void
	 */
	public function passwordSent() {
		// Load required classes
		AdminLoader::loadView('AdminLoginPasswordSentView', 'login');
		
		// Construct the "Thank you" page
		$view = new AdminLoginPasswordSentView();
		$view->display();
	}
	
	/**
	 * Password recovery: Error page
	 * 
	 * @access public
	 * @return void
	 */
	public function passwordRecoveryError() {
		// Load required classes
		AdminLoader::loadView('AdminLoginPasswordRecoveryErrorView', 'login');
		
		// Construct the "Thank you" page
		$view = new AdminLoginPasswordRecoveryErrorView();
		$view->display();
	}

	/**
	 * Get (Relative) Login Redirect Path
	 *
	 * @access private
	 * @return string
	 */
	private function _getLoginRedirectUrl() {
		// Ask the Admin Module for its configuration:
		$config = M_Application::getConfigOfModule('admin');
		
		// If the login redirect url is specified, return it
		if(isset($config->loginRedirectUrl) && ! empty($config->loginRedirectUrl)) {
			return $config->loginRedirectUrl;
		}
		
		// Check if a Redirect URL has been provided by a request variable
		// (via URL, or via hidden field in form)
		$route = M_Request::getVariable('route', FALSE, M_Request::TYPE_STRING);

		// If not
		if(! $route) {
			// We try to get the Redirect URL from the referrer page. We get the
			// referral page from the session, not from the browser:
			$uri = M_Session
				::getInstance()
				->getReferrer();

			// If a referral is not available
			if(! $uri) {
				// Then, return the default redirect url
				return $this->_getLoginDefaultRedirectUrl();
			}

			// Get the path:
			// $route = strtolower(M_Helper::trimCharlist(str_replace(M_Server::getBaseHref(), '', $uri->getPath()), '/'));
			$route = $uri->getRelativePath();
		}

		// Get link prefix
		$prefix = M_Request::getLinkPrefix();
		$length = strlen($prefix);

		// Remove the prefix from referral?
		if($length > 0 && substr($route, 0, $length) == $prefix) {
			// Yes, do so now:
			$route = M_Helper::trimCharlist(substr($route, $length), '/');
		}

		// If the referral URI is a login-related page, we do not use the
		// referral at all. So, we make sure that the URI is not login-
		// related:
		$notLoginRoute = (
			$route &&
			(
				strpos($route, 'login') === FALSE &&
				strpos($route, 'logout') === FALSE &&
				strpos($route, 'multikey') === FALSE &&
				strpos($route, 'authenticate') === FALSE
			)
		);

		if($notLoginRoute) {
			return $route;
		}

		// Return the default redirect url
		return $this->_getLoginDefaultRedirectUrl();
	}

	/**
	 * Get Default Login Redirect Path
	 *
	 * @access private
	 * @return string
	 */
	private function _getLoginDefaultRedirectUrl() {
		return 'admin/my-account';
	}
}