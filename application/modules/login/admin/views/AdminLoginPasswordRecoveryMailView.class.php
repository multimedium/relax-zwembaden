<?php
/**
 * AdminLoginPasswordRecoveryMailView class
 * 
 * @package App
 * @subpackage Login
 */
class AdminLoginPasswordRecoveryMailView extends M_ViewHtml {
	/**
	 * Set Administrator Account
	 * 
	 * Defines the Administrator Account to which the mail is being addressed.
	 * 
	 * @access public
	 * @param Administrator $administrator
	 * @return void
	 */
	public function setAdministrator(Administrator $administrator) {
		$this->assign('administrator', $administrator);
	}
	
	/**
	 * Set new password (not encrypted)
	 * 
	 * @access public
	 * @param Administrator $administrator
	 * @return void
	 */
	public function setNewPassword($password) {
		$this->assign('password', $password);
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(M_Loader::getModulePath('login') . '/admin/resources/templates/LoginPasswordRecoveryMail.tpl');
	}
}