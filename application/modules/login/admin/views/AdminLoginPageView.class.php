<?php
/**
 * AdminLoginPageView class
 * 
 * @package App
 * @subpackage Login
 */
abstract class AdminLoginPageView extends M_ViewHtml {
	/**
	 * Render HTML
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return string
	 */
	protected function _render() {
		// Construct the view resource (template)
		$rs = new M_ViewHtmlResource(M_Loader::getModulePath('login') . '/admin/resources/templates/LoginPage.tpl');
		
		// Render the HTML of this view, and assign it to the template:
		$rs->assign('content', parent::_render());
		$rs->assign('isMultikeyModuleInstalled', M_Application::isModuleInstalled('multikey'));
		
		// return the rendered HTML from the template:
		return $rs->fetch();
	}
}