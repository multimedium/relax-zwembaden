<?php
/**
 * AdminLoginPasswordRecoveryForm class
 * 
 * @package App
 */
class AdminLoginPasswordRecoveryForm extends M_Form {
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $id
	 * 		The ID of the form; as provided to the constructor of the
	 * 		{@link M_Form} class
	 * @return LoginForm
	 */
	public function __construct($id = 'login-form') {
		// Construct the form
		parent::__construct($id);
		
		// Attach the username field
		$field = new M_FieldText('email');
		$field->setTitle(t('Email address'));
		$field->setDescription(t('Please introduce your email address. A new password will be sent to you, along with instructions on how to activate your new password.'));
		$field->setMandatory(TRUE);
		$field->addValidator('M_ValidatorIsEmail', t('Please introduce a valid email address'));
		$this->addField($field);
	}
	
	/**
	 * Validate
	 */
	public function __validate() {
		// Load required classes
		M_Loader::loadModel('AdministratorMapper', 'administrator');
		AdminLoader::loadView('AdminLoginPasswordRecoveryMailView', 'login');
		
		// We prepare the mapper with which we will attempt to look up the 
		// administrator account:
		$administratorMapper = new AdministratorMapper();
		$administratorMapper->addFilter(new M_DbQueryFilterWhere('active', 1));
		
		// Download the form values:
		$values = $this->getValues();
		
		// Ask the mapper for the requested administrator:
		$administrators = $administratorMapper->getByField('email', $values['email']);
		
		// We only accept a unique administrator account. We make sure that 
		// the result set contains only 1 record:
		if($administrators->count() == 1) {
			// Get the identified administrator:
			$administrator = $administrators->current();
			
			// We ask M_Password for a new password, and assign the new password 
			// to the administrator:
			$password = M_Password::getNew(8);
			$administrator->setNewPassword(md5($password));
			
			// The new password is not active by default. We compose the URL that is
			// to be used to activate the password:
			$uri = new M_Uri(M_Request::getLink(
				'admin/route/login/activate/' . md5(
					time() . $administrator->getId() . $administrator->getEmail() . $administrator->getNewPassword()
				)
			));
			
			// We save the activation url in the administrator account
			$administrator->setNewPasswordActivationUrl($uri->toString());
			
			// We save the changes to the administrator account:
			// (we only continue if the changes to the administrator account
			// have saved successfully)
			if($administrator->save()) {
				// We need to send a mail to the administrator, to inform about
				// the new password! First, we construct the view of the mail's
				// contents:
				$view = new AdminLoginPasswordRecoveryMailView();
				$view->setAdministrator($administrator);
				$view->setNewPassword($password);
				
				// We compose the email message
				$email = new M_Mail;
				/*$mailer = new M_MailerSmtp();
				$mailer->setHost(new M_Uri('smtp://w035971:mmRocks14@uit.telenet.be'));
				$email->setMailer($mailer);*/
				
				// Set email recipient
				$email->addRecipient(
					M_MailHelper::getRecipientStringFormat(
						$administrator->getFirstname() . ' ' . $administrator->getFirstname(),
						$administrator->getEmail()
					)
				);
				
				// Set subject
				$email->setSubject(t('multimanage - Activate your new password'));
				
				// Set email message:
				$email->setBody($view->fetch());
				
				// Send the mail:
				if($email->send()) {
					// If the email has been sent, we return success as validation result
					return TRUE;
				}
			}
		}
		// Could not identify a unique Administrator account:
		else {
			// Set error message
			$this->setErrorMessage(t(
				'No administrator account has been found for e-mail address "@address". Please check your records, and try again.',
				array(
					'@address' => $values['email']
				)
			));
			
			// Return failure:
			return FALSE;
		}
		
		// If we are still here, we failed to identify the administrator, or maybe
		// we have failed to send a mail :) We show a generic failure message
		$this->setErrorMessage(t('Your request could not have been completed. Please try again later.'));
		return FALSE;
	}
	
	/**
	 * Form actions
	 * 
	 * All M_Form subclasses have to implement this method. 
	 * This method receives the final values, and performs the 
	 * specific form's actions on that given data set. The
	 * values that are passed into this method, have been 
	 * collected by {@link M_Form::getValues()}.
	 * 
	 * This method returns TRUE on success, and FALSE on failure.
	 * 
	 * @access public
	 * @see M_Form::getValues()
	 * @param array $values
	 * 		The values that have been collected by the method
	 * 		{@link M_Form::getValues()}.
	 * @return boolean
	 */
	protected function actions(array $values) {
		return TRUE;
	}
}