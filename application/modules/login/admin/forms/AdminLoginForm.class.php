<?php
/**
 * AdminLoginForm class
 * 
 * @package App
 */
class AdminLoginForm extends M_Form {
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param string $id
	 * 		The ID of the form; as provided to the constructor of the
	 * 		{@link M_Form} class
	 * @return LoginForm
	 */
	public function __construct($id = 'login-form') {
		// Construct the form
		parent::__construct($id);
		
		// Attach the username field
		$field = new M_FieldText('username');
		$field->setTitle(t('Username'));
		$field->setMandatory(TRUE);
		$this->addField($field);
		
		// Attach the password field
		$field = new M_FieldPassword('password');
		$field->setTitle(t('Password'));
		$field->setMandatory(TRUE);
		$this->addField($field);
	}
	
	/**
	 * Validate the form
	 * 
	 * @see M_Form::__validate()
	 * @access public
	 * @return bool
	 */
	public function __validate() {
		// Load AdministratorMapper from admin module:
		M_Loader::loadModel('AdministratorMapper', 'administrator');
		
		// We prepare the mapper with which we will attempt to login:
		$administratorMapper = new AdministratorMapper();
		$administratorMapper->addFilter(new M_DbQueryFilterWhere('active', 1));
		
		// Create an authentication attempt:
		$attempt = new M_AuthAttemptDataObject();
		$profile = new M_AuthAttemptDataObjectProfile();
		$profile->setDataObjectMapper($administratorMapper);
		$profile->setIdentityField('username');
		$profile->setIdentity($this->getField('username')->getValue());
		$profile->setPasswordField('password');
		$attempt->setPassword(md5($this->getField('password')->getValue()));
		$attempt->setProfile($profile);
		
		// Get the M_Auth Singleton:
		// (we do not set the storage of authenticated sessions. This will probably
		// have been set already by AdminController)
		$auth = M_Auth::getInstance();
		
		// Try to authenticate, with the authentication attempt we have created
		// before. If authentication completes successfully:
		if($auth->authenticate($attempt)) {
			// Set the IP of the administrator in the auth session:
			$auth->getStorage()->setIp(M_Client::getIp());
			
			// Return success:
			return TRUE;
		}
		// If authentication failed:
		else {
			// Check the error code of the authentication result:
			// switch($auth->getResult()->getCode()) {}
			
			// Set error message:
			$this->setErrorMessage(t('Login has failed. Please try again.'));
			
			// Return failure:
			return FALSE;
		}
	}
	
	/**
	 * Form actions
	 * 
	 * All M_Form subclasses have to implement this method. 
	 * This method receives the final values, and performs the 
	 * specific form's actions on that given data set. The
	 * values that are passed into this method, have been 
	 * collected by {@link M_Form::getValues()}.
	 * 
	 * This method returns TRUE on success, and FALSE on failure.
	 * 
	 * @access public
	 * @see M_Form::getValues()
	 * @param array $values
	 * 		The values that have been collected by the method
	 * 		{@link M_Form::getValues()}.
	 * @return boolean
	 */
	protected function actions(array $values) {
		// We return TRUE, because authentication already completed during validation
		// of the form:
		return TRUE;
	}
}