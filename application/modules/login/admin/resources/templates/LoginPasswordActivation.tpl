<h1>{translate text="Thank you"}</h1>
<p class="reading">
	{t text="Your new password has been activated successfully. From now on, you can use the new login credentials that have been emailed to you."}
	
	<br />
	<br />
	<a href="{link href="admin/route/login"}" title="{t|att text="Continue to login"}">
		{t text="Continue to login"}
	</a>
</p>
