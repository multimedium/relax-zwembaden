{* Styles that are applied throughout this template *}
{assign var="fontStyle" value="font-family: Arial, Verdana, Helvetica; color: #000000;"}
{assign var="h1Style" value="font-size: 15px; font-weight: bold;"}
{assign var="pStyle" value="font-size: 12px; line-height: 140%;"}
{assign var="thStyle" value="font-size: 12px; font-weight: bold; text-align: left; vertical-align: top;"}
{assign var="tdStyle" value="font-size: 12px; vertical-align: top;"}

<h1 style="{$fontStyle} {$h1Style}">{t text="Your new password"}</h1>

<p style="{$fontStyle} {$pStyle}">
	{t text="Dear @firstname @surnames," 
		firstname=$administrator->getFirstname() 
		surnames=$administrator->getSurnames()}
	<br /><br />
	
	{t text="Multimanage has created a new password for you. Your new password is: <strong>@password</strong>" 
		password=$password}
	<br /><br />
	
	{t text="Note however that this new password has <strong>not yet</strong> been <strong>activated</strong>. In order to activate the new password, you should visit the following <strong>Password Activation URL</strong>:"}
	<br /><br />
	
	<a href="{$administrator->getNewPasswordActivationUrl()}">{$administrator->getNewPasswordActivationUrl()}</a>
	<br /><br />
	
	{t text="You should be able to click the link above. If not, please copy the URL and paste it into the address bar of your browser."}
	<br /><br />
	
	{t text="Once you have activated your new password, you can login to <em>Multimanage</em> with <strong>your new credentials</strong>:"}
	<br /><br />
</p>

	<table cellpadding="5" cellspacing="0" border="0">
		<thead>
			<tr>
				<th style="{$fontStyle} {$thStyle}">{t text="Username"}</th>
				<th style="{$fontStyle} {$thStyle}">{t text="Password"}</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="{$fontStyle} {$tdStyle}">{$administrator->getUsername()}</td>
				<td style="{$fontStyle} {$tdStyle}">{$password}</td>
			</tr>
		</tbody>
	</table>

<p style="{$fontStyle} {$pStyle}">
	{t text="Of course, you may feel free to <strong>change this password</strong>, to any other password that is more comfortable to you."}
	{t text='You can do so, after logging in to Multimanage. Just click "<em>Edit my account</em>", and then "<em>Change Password</em>.'}
	<br /><br />
	
	{t text='Kind regards,<br /> Multimanage Team - Multimedium'}
	<br /><br />
</p>


