<h1>{translate text="Password Activation URL has expired"}</h1>
<p>
	{t text="Multimanage could not activate your password. Possible causes may be:"}
</p>

<ul>
	<li>
		<strong>{t text="The Password Activation URL is invalid"}</strong><br />
		{t text="Please make sure to copy the complete Password Activation URL into the address bar of your browser. Maybe you left out some part of the URL?"}
	</li>
	<li>
		<strong>{t text="The Password Activation URL has expired"}</strong><br />
		{t text="The Password Activation URL can only be used one time. Once used, the page expires and can no longer be used to activate your new password."}
		<br /><br />
		
		{link href="admin/route/login/passwordRecovery" assignto="hrefPasswordRecovery"}
		{t text='<strong>If you forgot your password</strong>, you should <a href="@url">have <em>Multimanage</em> generate a new one for you</a>.' 
			url=$hrefPasswordRecovery}
	</li>
</ul>