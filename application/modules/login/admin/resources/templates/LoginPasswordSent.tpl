<h1>{translate text="Thank you"}</h1>
<p class="reading">
	{t text="A new password has been sent to your email address, along with instructions on how to activate your new password."}
	
	<br />
	<br />
	<a href="{link href="admin/route/login"}" title="{t|att text="Continue to login"}">
		{t text="Continue to login"}
	</a>
</p>
