<?php
M_Loader::loadDataObject('DataObjectWithMedia', 'media');

/**
 * TextDataObject class
 *
 * @see TextDataObjectMapper
 * @see TextDataObjectModel
 * @package App
 */
class TextDataObject extends DataObjectWithMedia {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link TextDataObject::getId()}
	 * - {@link TextDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * parentTextId
	 * 
	 * This property stores the parentTextId. To get/set the value of the
	 * parentTextId, read the documentation on 
	 * 
	 * - {@link TextDataObject::getParentTextId()}
	 * - {@link TextDataObject::setParentTextId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_parentTextId;
	
	/**
	 * title ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * title. To get/set the value of the title, read the documentation on 
	 * 
	 * - {@link TextDataObject::getTitle()}
	 * - {@link TextDataObject::getTitleId()}
	 * - {@link TextDataObject::setTitle()}
	 * - {@link TextDataObject::setTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_titleId;
	
	/**
	 * text ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * text. To get/set the value of the text, read the documentation on 
	 * 
	 * - {@link TextDataObject::getText()}
	 * - {@link TextDataObject::getTextId()}
	 * - {@link TextDataObject::setText()}
	 * - {@link TextDataObject::setTextId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_textId;
	
	/**
	 * extraTitle ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * extraTitle. To get/set the value of the extraTitle, read the
	 * documentation on 
	 * 
	 * - {@link TextDataObject::getExtraTitle()}
	 * - {@link TextDataObject::getExtraTitleId()}
	 * - {@link TextDataObject::setExtraTitle()}
	 * - {@link TextDataObject::setExtraTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_extraTitleId;
	
	/**
	 * extraText ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * extraText. To get/set the value of the extraText, read the
	 * documentation on 
	 * 
	 * - {@link TextDataObject::getExtraText()}
	 * - {@link TextDataObject::getExtraTextId()}
	 * - {@link TextDataObject::setExtraText()}
	 * - {@link TextDataObject::setExtraTextId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_extraTextId;
	
	/**
	 * extraUrl
	 * 
	 * This property stores the extraUrl. To get/set the value of the
	 * extraUrl, read the documentation on 
	 * 
	 * - {@link TextDataObject::getExtraUrl()}
	 * - {@link TextDataObject::setExtraUrl()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_extraUrl;
	
	/**
	 * extraButtonTitle ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * extraButtonTitle. To get/set the value of the extraButtonTitle, read
	 * the documentation on 
	 * 
	 * - {@link TextDataObject::getExtraButtonTitle()}
	 * - {@link TextDataObject::getExtraButtonTitleId()}
	 * - {@link TextDataObject::setExtraButtonTitle()}
	 * - {@link TextDataObject::setExtraButtonTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_extraButtonTitleId;
	
	/**
	 * url ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * url. To get/set the value of the url, read the documentation on 
	 * 
	 * - {@link TextDataObject::getUrl()}
	 * - {@link TextDataObject::getUrlId()}
	 * - {@link TextDataObject::setUrl()}
	 * - {@link TextDataObject::setUrlId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_urlId;
	
	/**
	 * realm
	 * 
	 * This property stores the realm. To get/set the value of the realm,
	 * read the documentation on 
	 * 
	 * - {@link TextDataObject::getRealm()}
	 * - {@link TextDataObject::setRealm()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_realm;
	
	/**
	 * deletable
	 * 
	 * This property stores the deletable. To get/set the value of the
	 * deletable, read the documentation on 
	 * 
	 * - {@link TextDataObject::getDeletable()}
	 * - {@link TextDataObject::setDeletable()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_deletable;
	
	/**
	 * categoryId
	 * 
	 * This property stores the categoryId. To get/set the value of the
	 * categoryId, read the documentation on 
	 * 
	 * - {@link TextDataObject::getCategoryId()}
	 * - {@link TextDataObject::setCategoryId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_categoryId;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the order,
	 * read the documentation on 
	 * 
	 * - {@link TextDataObject::getOrder()}
	 * - {@link TextDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link TextDataObject::getPublished()}
	 * - {@link TextDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * inMenu
	 * 
	 * This property stores the inMenu. To get/set the value of the inMenu,
	 * read the documentation on 
	 * 
	 * - {@link TextDataObject::getInMenu()}
	 * - {@link TextDataObject::setInMenu()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_inMenu;
	
	/**
	 * Get title
	 * 
	 * Get title in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the title
	 * @return string|NULL
	 */
	public function getTitle($locale = NULL) {
		return $this->_getLocalizedText('title', $locale);
	}
	
	/**
	 * Get title ID
	 * 
	 * Get numeric reference to the translated title .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTitleId() {
		return $this->_titleId;
	}
	
	/**
	 * Set title
	 * 
	 * Set title, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated title
	 * @param string $locale
	 * 	The language in which to set the title
	 * @return void
	 */
	public function setTitle($text, $locale = NULL) {
		$this->_setLocalizedText('title', $text, $locale);
	}
	
	/**
	 * Set title ID
	 * 
	 * Set numeric reference to the translated title .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of title
	 * @return void
	 */
	public function setTitleId($id) {
		$this->_titleId = $id;
	}
	
	/**
	 * Get text
	 * 
	 * Get text in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the text
	 * @return string|NULL
	 */
	public function getText($locale = NULL) {
		return $this->_getLocalizedText('text', $locale);
	}
	
	/**
	 * Get text ID
	 * 
	 * Get numeric reference to the translated text .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTextId() {
		return $this->_textId;
	}
	
	/**
	 * Set text
	 * 
	 * Set text, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated text
	 * @param string $locale
	 * 	The language in which to set the text
	 * @return void
	 */
	public function setText($text, $locale = NULL) {
		$this->_setLocalizedText('text', $text, $locale);
	}
	
	/**
	 * Set text ID
	 * 
	 * Set numeric reference to the translated text .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of text
	 * @return void
	 */
	public function setTextId($id) {
		$this->_textId = $id;
	}
	
	/**
	 * Get extraTitle
	 * 
	 * Get extraTitle in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the extraTitle
	 * @return string|NULL
	 */
	public function getExtraTitle($locale = NULL) {
		return $this->_getLocalizedText('extraTitle', $locale);
	}
	
	/**
	 * Get extraTitle ID
	 * 
	 * Get numeric reference to the translated extraTitle .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getExtraTitleId() {
		return $this->_extraTitleId;
	}
	
	/**
	 * Set extraTitle
	 * 
	 * Set extraTitle, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated extraTitle
	 * @param string $locale
	 * 	The language in which to set the extraTitle
	 * @return void
	 */
	public function setExtraTitle($text, $locale = NULL) {
		$this->_setLocalizedText('extraTitle', $text, $locale);
	}
	
	/**
	 * Set extraTitle ID
	 * 
	 * Set numeric reference to the translated extraTitle .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of extraTitle
	 * @return void
	 */
	public function setExtraTitleId($id) {
		$this->_extraTitleId = $id;
	}
	
	/**
	 * Get extraText
	 * 
	 * Get extraText in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the extraText
	 * @return string|NULL
	 */
	public function getExtraText($locale = NULL) {
		return $this->_getLocalizedText('extraText', $locale);
	}
	
	/**
	 * Get extraText ID
	 * 
	 * Get numeric reference to the translated extraText .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getExtraTextId() {
		return $this->_extraTextId;
	}
	
	/**
	 * Set extraText
	 * 
	 * Set extraText, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated extraText
	 * @param string $locale
	 * 	The language in which to set the extraText
	 * @return void
	 */
	public function setExtraText($text, $locale = NULL) {
		$this->_setLocalizedText('extraText', $text, $locale);
	}
	
	/**
	 * Set extraText ID
	 * 
	 * Set numeric reference to the translated extraText .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of extraText
	 * @return void
	 */
	public function setExtraTextId($id) {
		$this->_extraTextId = $id;
	}
	
	/**
	 * Get extraButtonTitle
	 * 
	 * Get extraButtonTitle in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the extraButtonTitle
	 * @return string|NULL
	 */
	public function getExtraButtonTitle($locale = NULL) {
		return $this->_getLocalizedText('extraButtonTitle', $locale);
	}
	
	/**
	 * Get extraButtonTitle ID
	 * 
	 * Get numeric reference to the translated extraButtonTitle .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getExtraButtonTitleId() {
		return $this->_extraButtonTitleId;
	}
	
	/**
	 * Set extraButtonTitle
	 * 
	 * Set extraButtonTitle, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated extraButtonTitle
	 * @param string $locale
	 * 	The language in which to set the extraButtonTitle
	 * @return void
	 */
	public function setExtraButtonTitle($text, $locale = NULL) {
		$this->_setLocalizedText('extraButtonTitle', $text, $locale);
	}
	
	/**
	 * Set extraButtonTitle ID
	 * 
	 * Set numeric reference to the translated extraButtonTitle .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of extraButtonTitle
	 * @return void
	 */
	public function setExtraButtonTitleId($id) {
		$this->_extraButtonTitleId = $id;
	}
	
	/**
	 * Get url
	 * 
	 * Get url in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the url
	 * @return string|NULL
	 */
	public function getUrl($locale = NULL) {
		return $this->_getLocalizedText('url', $locale);
	}
	
	/**
	 * Get url ID
	 * 
	 * Get numeric reference to the translated url .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getUrlId() {
		return $this->_urlId;
	}
	
	/**
	 * Set url
	 * 
	 * Set url, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated url
	 * @param string $locale
	 * 	The language in which to set the url
	 * @return void
	 */
	public function setUrl($text, $locale = NULL) {
		$this->_setLocalizedText('url', $text, $locale);
	}
	
	/**
	 * Set url ID
	 * 
	 * Set numeric reference to the translated url .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of url
	 * @return void
	 */
	public function setUrlId($id) {
		$this->_urlId = $id;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return mixed
	 */
	public function setId($id) {
		$this->_id = $id;
	}
	
	/**
	 * Get parentTextId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getParentTextId() {
		return $this->_parentTextId;
	}
	
	/**
	 * Set parentTextId
	 * 
	 * @access public
	 * @param mixed $parentTextId
	 * @return mixed
	 */
	public function setParentTextId($parentTextId) {
		$this->_parentTextId = $parentTextId;
	}
	
	/**
	 * Get extraUrl
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getExtraUrl() {
		return $this->_extraUrl;
	}
	
	/**
	 * Set extraUrl
	 * 
	 * @access public
	 * @param mixed $extraUrl
	 * @return mixed
	 */
	public function setExtraUrl($extraUrl) {
		$this->_extraUrl = $extraUrl;
	}
	
	/**
	 * Get realm
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getRealm() {
		return $this->_realm;
	}
	
	/**
	 * Set realm
	 * 
	 * @access public
	 * @param mixed $realm
	 * @return mixed
	 */
	public function setRealm($realm) {
		$this->_realm = $realm;
	}
	
	/**
	 * Get deletable
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getDeletable() {
		return $this->_deletable;
	}
	
	/**
	 * Set deletable
	 * 
	 * @access public
	 * @param mixed $deletable
	 * @return mixed
	 */
	public function setDeletable($deletable) {
		$this->_deletable = $deletable;
	}
	
	/**
	 * Get categoryId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCategoryId() {
		return $this->_categoryId;
	}
	
	/**
	 * Set categoryId
	 * 
	 * @access public
	 * @param mixed $categoryId
	 * @return mixed
	 */
	public function setCategoryId($categoryId) {
		$this->_categoryId = $categoryId;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return mixed
	 */
	public function setOrder($order) {
		$this->_order = $order;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return mixed
	 */
	public function setPublished($published) {
		$this->_published = $published;
	}
	
	/**
	 * Get inMenu
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getInMenu() {
		return $this->_inMenu;
	}
	
	/**
	 * Set inMenu
	 * 
	 * @access public
	 * @param mixed $inMenu
	 * @return mixed
	 */
	public function setInMenu($inMenu) {
		$this->_inMenu = $inMenu;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return TextMapper $mapper
	 */
	public function getMapper() {
		if (is_null($this->_mapper)) {
			M_Loader::loadDataObjectMapper("Text","text");
			$this->_mapper = new TextMapper();
		}
		return $this->_mapper;
	}
	
		
}