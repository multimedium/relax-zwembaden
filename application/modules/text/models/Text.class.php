<?php
/**
 * Text
 * 
 * @package App
 * @subpackage Text
 */

// Load superclass
M_Loader::loadDataObject('TextDataObject', 'text');

/**
 * Text
 * 
 * @package App
 * @subpackage Text
 */
class Text extends TextDataObject {

	/**
	 * deletable
	 *
	 * This property stores the deletable. To get/set the value of the
	 * deletable, read the documentation on
	 *
	 * - {@link TextDataObject::getDeletable()}
	 * - {@link TextDataObject::setDeletable()}
	 *
	 * @access protected
	 * @var mixed
	 */
	protected $_deletable = 1;

	/**
	 * deletable
	 *
	 * This property stores the shown in menu. To get/set the value of the
	 * deletable, read the documentation on
	 *
	 * - {@link TextDataObject::getInMenu()}
	 * - {@link TextDataObject::setInMenu()}
	 *
	 * @access protected
	 * @var mixed
	 */
	protected $_inMenu = 1;

	
	/* -- SETTERS -- */
	
	/**
	 * Set title
	 * 
	 * @access public
	 * @param mixed $text
	 * @return mixed
	 */
	public function setTitle($text, $locale = NULL) {
		// Set the title
		parent::setTitle($text, $locale);
		
		// If no URL Suffix exists yet for this text:
		//if(! $this->getUrl($locale)) {
			// Set the URL Suffix now:
			$this->setUrl(M_Uri::getPathElementFromString($text), $locale);
		//}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get parent text
	 * 
	 * NOTE:
	 * Will return NULL if the parent could not have been found.
	 * 
	 * @access public
	 * @return void
	 */
	public function getParentText() {
		if($this->getParentTextId()) {
			$text = M_Loader
				::getDataObjectMapper('Text', 'text')
				->addFilterPublishedOnly()
				->getById($this->getParentTextId());
			
			if($text) {
				return $text;
			}
		}
		
		return NULL;
	}

	/**
	 * Check if this is a root text (and thus is not attached to any other
	 * text)
	 *
	 * @return bool
	 */
	public function isRootText() {
		return !$this->getParentTextId();
	}
	
	/**
	 * Get URL of parent
	 * 
	 * Will look up the URL of the parent (recursively). Typically used to compose
	 * the URL to the page inside the website.
	 * 
	 * @access public
	 * @return string
	 */
	public function getRecursiveParentUrl() {
		// Output
		$u = array();
		
		// Working variables
		$t = $this;
		$l = array();
		
		// For each of the parents:
		while(is_object($t = $t->getParentText())) {
			// If the current parent has not been added already
			if(! in_array($t->getId(), $l)) {
				// Then, add to output
				array_unshift($u, $t->getUrl());
			}
			// Break out of infinite loop:
			else {
				break;
			}
			
			// Log, to prevent infinite loop
			$l[] = $t->getId();
		}
		
		// Return output
		return implode('/', $u);
	}
	
	/**
	 * Get child texts
	 * 
	 * @access public
	 * @return ArrayIterator $texts
	 * 		The matching objects; instances of {@link Text}
	 */
	public function getChildTexts() {
		return M_Loader
			::getDataObjectMapper('Text', 'text')
			->addFilterPublishedOnly()
			->addFilterDefaultSorting()
			->addFilterParentText($this)
			->getAll();
	}
	
	/**
	 * Get number of child texts
	 * 
	 * @access public
	 * @return integer $count
	 */
	public function getNumberOfChildTexts() {
		return M_Loader
			::getDataObjectMapper('Text', 'text')
			->addFilterPublishedOnly()
			->addFilterParentText($this)
			->getCount();
	}
	
	/**
	 * Get text category
	 * 
	 * @access public
	 * @return TextCategory
	 */
	public function getCategory() {
		// Load the category mapper:
		M_Loader::loadDataObjectMapper('TextCategory', 'text');
		
		// Construct the mapper:
		$mapper = new TextCategoryMapper();
		
		// Ask the mapper for the category:
		return $mapper->getById($this->getCategoryId());
	}

	
	/**
	 * Get media album
	 * 
	 * Will provide with the Media Album that has been attached to the instance.
	 * Note that this method will return (boolean) FALSE if the album could not
	 * have been found
	 * 
	 * @access public
	 * @return MediaAlbum $album
	 */
	public function getMediaAlbum() {
		// Media gets attached to the event, by assign a media album to the object.
		// We get the MediaAlbum mapper, to fetch the album that has been attached
		// to the object:
		$mediaAlbumMapper = M_Loader::getDataObjectMapper('MediaAlbum', 'media');
		
		// Look up the media album:
		return $mediaAlbumMapper->getAlbumByObject($this);
	}

	/**
	 * Get breadcumbs (text objects)
	 *
	 * Returns an array with itself, aswell as all of its parent objects in
	 * the correct breadcrumb order
	 *
	 * @access public
	 * @return array
	 *		Returns array containing text objects in the correct order
	 */
	public function getBreadcrumb(){
		$array = array();
		$array[] = $this;
		$current = $this;
		//Loop for as long as the current is not a root text
		while(!$current->isRootText()){
			$array[] = $current->getParentText();
			$current = $current->getParentText();
		}
		return array_reverse($array);
	}

	/**
	 * Get the tags attached to this text
	 *
	 * @return M_ArrayIterator
	 */
	public function getTags() {
		$tagMapper = M_Loader::getDataObjectMapper('Tag', 'tag');
		/* @var $tagMapper TagMapper */
		return $tagMapper->getTagsByObject($this);
	}
}