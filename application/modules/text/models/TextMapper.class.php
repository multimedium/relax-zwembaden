<?php
/**
 * TextMapper
 * 
 * @see TextDataObject
 * @see Text
 * @package App
 * @subpackage Text
 */
class TextMapper extends M_DataObjectMapper {
	
	/* -- SETTERS -- */
	
	/**
	 * Add filter: default sorting
	 * 
	 * @access public
	 * @return TextMapper $mapper
	 * 		Returns itself, for a fluent programming interface
	 */
	public function addFilterDefaultSorting($order = 'ASC') {
		return $this->addFilter(new M_DbQueryFilterOrder('order', $order));
	}

	/**
	 * Add filter: search query
	 *
	 * Add a filter so only texts that match the query will be returned
	 *
	 * @access public
	 * @return TextMapper
	 */
	public function addFilterSearchQuery($query) {
		return $this->addFilter(new M_DbQueryFilterSearch(array('title','text'), $query));
	}
	
	/**
	 * Add filter: root pages only
	 * 
	 * Add a filter so only texts that have not been attached to another text 
	 * get fetched from the database.
	 * 
	 * @access public
	 * @return TextMapper
	 */
	public function addFilterRootTextsOnly() {
		return $this->addFilter(new M_DbQueryFilterWhere('parentTextId', 0));
	}
	
	/**
	 * Add filter: published
	 * 
	 * Add a filter so only published texts get fetched from the database.
	 * 
	 * @access public
	 * @return TextMapper
	 */
	public function addFilterPublishedOnly() {
		return $this->addFilter(new M_DbQueryFilterWhere('published', 1));
	}

		/**
	 * Add filter: in menu
	 *
	 * Add a filter so only texts which are allowed to be in the menu
	 * get fetched from the database.
	 *
	 * @access public
	 * @return TextMapper
	 */
	public function addFilterInMenuOnly() {
		return $this->addFilter(new M_DbQueryFilterWhere('inMenu', 1));
	}
	
	/**
	 * Add filter: parent text
	 * 
	 * @access public
	 * @return TextMapper
	 */
	public function addFilterParentText(Text $text) {
		return $this->addFilter(new M_DbQueryFilterWhere('parentTextId', $text->getId()));
	}
	
	/* -- GETTERS -- */


	
	/**
	 * Get by URL
	 * 
	 * Will try to fetch a unique object that matches the provided URL suffix. 
	 * Returns an instance of {@link Text}, or NULL if no matching 
	 * object could have been found.
	 * 
	 * @access public
	 * @param string $urlSuffix
	 * 		The URL Suffix to search with
	 * @return Text $text
	 * 		The matching object, or NULL if not found
	 */
	public function getByUrlSuffix($urlSuffix) {
		// Compose the SELECT statement, and execute
		$rs = $this->_getFilteredSelect(
			array(
				new M_DbQueryFilterWhere('url', (string) $urlSuffix)
			)
		)
			->limit(0, 1)
			->execute();;
		// If the result set contains the record we are looking for:
		if($rs && $rs->count() == 1) {
			// Return the object:
			return $this->_createObjectFromRecord($rs->current());
		}
		
		// Return NULL if still here
		// (If still here, it means that we did not find any matching object)
		return NULL;
	}
	
	/**
	 * Get by Realm
	 * 
	 * Will try to fetch a unique object that matches the provided realm. 
	 * Returns an instance of {@link Text}, or NULL if no matching 
	 * object could have been found.
	 * 
	 * @access public
	 * @param string $realm
	 * 		The realm to search with
	 * @return Text $text
	 * 		The matching object, or NULL if not found
	 */
	public function getByRealm($realm) {
		// Compose the SELECT statement, and execute
		$rs = $this->_getFilteredSelect(
			array(
				new M_DbQueryFilterWhere('realm', (string) $realm)
			)
		)
			->limit(0, 1)
			->execute();
		
		// If the result set contains the record we are looking for:
		if($rs && $rs->count() == 1) {
			// Return the object:
			return $this->_createObjectFromRecord($rs->current());
		}
		
		// Return FALSE if still here
		// (If still here, it means that we did not find any matching object)
		return NULL;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'text';
	}
}