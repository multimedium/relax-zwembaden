<?php
class TextEventListener {

	/**
	 * Construct
	 *
	 * @param AdminController $controller
	 */
	public function __construct(AdminController $controller) {

		//Add a validator before a banner is edited, so we can validate
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_EDIT,
			array($this, 'onDataObjectBeforeEdit')
		);
	}

	/**
	 * Do ... when object is about to be edited
	 *
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectBeforeEdit(AdminControllerEvent $event) {
		$form = $event->getForm();
		$text = $event->getDataObject();
		/* @var $text Text */
		if (!M_Helper::isInstanceOf($text, 'Text')) return false;
		//remove "attach to" and "published" fields for root texts
		if ($text instanceof Text && $text->isRootText()) {
			//$form->removeField('parentTextId')->removeField('published')->setVariable('parentTextId', 0);
		}
	}
}