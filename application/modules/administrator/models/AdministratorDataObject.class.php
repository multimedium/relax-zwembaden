<?php
/** 
 * AdministratorDataObject class
 * 
 * @see AdministratorMapper
 * @package App
 */
class AdministratorDataObject extends M_DataObject {
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link AdministratorDataObject::getId()}
	 * - {@link AdministratorDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * locale
	 * 
	 * This property stores the locale. To get/set the value of the locale, read the
	 * documentation on 
	 * 
	 * - {@link AdministratorDataObject::getLocale()}
	 * - {@link AdministratorDataObject::setLocale()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_locale;
	
	/**
	 * aclRoleId
	 * 
	 * This property stores the aclRoleId. To get/set the value of the aclRoleId, 
	 * read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getAclRoleId()}
	 * - {@link AdministratorDataObject::setAclRoleId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_aclRoleId;
	
	/**
	 * pictureFilename
	 * 
	 * This property stores the pictureFilename. To get/set the value of the
	 * pictureFilename, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getPictureFilename()}
	 * - {@link AdministratorDataObject::setPictureFilename()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_pictureFilename;
	
	/**
	 * title
	 * 
	 * This property stores the title. To get/set the value of the
	 * title, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getTitle()}
	 * - {@link AdministratorDataObject::setTitle()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_title;
	
	/**
	 * firstName
	 * 
	 * This property stores the firstName. To get/set the value of the
	 * firstName, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getFirstname()}
	 * - {@link AdministratorDataObject::setFirstname()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_firstName;
	
	/**
	 * surNames
	 * 
	 * This property stores the surNames. To get/set the value of the
	 * surNames, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getSurnames()}
	 * - {@link AdministratorDataObject::setSurnames()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_surNames;
	
	/**
	 * email
	 * 
	 * This property stores the email. To get/set the value of the email,
	 * read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getEmail()}
	 * - {@link AdministratorDataObject::setEmail()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_email;
	
	/**
	 * username
	 * 
	 * This property stores the username. To get/set the value of the
	 * username, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getUsername()}
	 * - {@link AdministratorDataObject::setUsername()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_username;
	
	/**
	 * password
	 * 
	 * This property stores the password. To get/set the value of the
	 * password, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getPassword()}
	 * - {@link AdministratorDataObject::setPassword()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_password;
	
	/**
	 * newPassword
	 * 
	 * This property stores the newPassword. To get/set the value of the
	 * newPassword, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getNewPassword()}
	 * - {@link AdministratorDataObject::setNewPassword()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_newPassword;
	
	/**
	 * newPasswordActivationUrl
	 * 
	 * This property stores the newPasswordActivationUrl. To get/set the value 
	 * of the newPasswordActivationUrl, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getNewPasswordActivationUrl()}
	 * - {@link AdministratorDataObject::setNewPasswordActivationUrl()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_newPasswordActivationUrl;
	
	/**
	 * streetAddress
	 * 
	 * This property stores the streetAddress. To get/set the value of the
	 * streetAddress, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getStreetAddress()}
	 * - {@link AdministratorDataObject::setStreetAddress()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_streetAddress;
	
	/**
	 * zip
	 * 
	 * This property stores the zip. To get/set the value of the
	 * zip, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getZip()}
	 * - {@link AdministratorDataObject::setZip()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_zip;
	
	/**
	 * city
	 * 
	 * This property stores the city. To get/set the value of the
	 * city, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getCity()}
	 * - {@link AdministratorDataObject::setCity()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_city;
	
	/**
	 * country
	 * 
	 * This property stores the country. To get/set the value of the
	 * country, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getCountry()}
	 * - {@link AdministratorDataObject::setCountry()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_country;
	
	/**
	 * phoneNumber
	 * 
	 * This property stores the phoneNumber. To get/set the value of the
	 * phoneNumber, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getPhoneNumber()}
	 * - {@link AdministratorDataObject::setPhoneNumber()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_phoneNumber;
	
	/**
	 * faxNumber
	 * 
	 * This property stores the faxNumber. To get/set the value of the
	 * faxNumber, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getFaxNumber()}
	 * - {@link AdministratorDataObject::setFaxNumber()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_faxNumber;
	
	/**
	 * organisation
	 * 
	 * This property stores the organisation. To get/set the value of the
	 * organisation, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getOrganisation()}
	 * - {@link AdministratorDataObject::setOrganisation()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_organisation;
	
	/**
	 * website
	 * 
	 * This property stores the website. To get/set the value of the
	 * website, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getWebsite()}
	 * - {@link AdministratorDataObject::setWebsite()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_website;
	
	/**
	 * active
	 * 
	 * This property stores the active. To get/set the value of the
	 * active, read the documentation on 
	 * 
	 * - {@link AdministratorDataObject::getActive()}
	 * - {@link AdministratorDataObject::setActive()}
	 * 
	 * @access protected
	 * @var int
	 * @author b.brughmans
	 */
	protected $_active;
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return mixed
	 */
	public function setId($id) {
		$this->_id = $id;
	}
	
	/**
	 * Get locale
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getLocale() {
		return $this->_locale;
	}
	
	/**
	 * Set locale
	 * 
	 * @access public
	 * @param mixed $locale
	 * @return mixed
	 */
	public function setLocale($locale) {
		$this->_locale = $locale;
	}
	
	/**
	 * Get aclRoleId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getAclRoleId() {
		return $this->_aclRoleId;
	}
	
	/**
	 * Set aclRoleId
	 * 
	 * @access public
	 * @param mixed $aclRoleId
	 * @return mixed
	 */
	public function setAclRoleId($aclRoleId) {
		$this->_aclRoleId = $aclRoleId;
	}
	
	/**
	 * Get pictureFilename
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPictureFilename() {
		return $this->_pictureFilename;
	}
	
	/**
	 * Set pictureFilename
	 * 
	 * @access public
	 * @param mixed $pictureFilename
	 * @return mixed
	 */
	public function setPictureFilename($pictureFilename) {
		$this->_pictureFilename = $pictureFilename;
	}
	
	/**
	 * Get title
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * Set title
	 * 
	 * @access public
	 * @param mixed $title
	 * @return mixed
	 */
	public function setTitle($title) {
		$this->_title = $title;
	}
	
	/**
	 * Get firstName
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getFirstname() {
		return $this->_firstName;
	}
	
	/**
	 * Set firstName
	 * 
	 * @access public
	 * @param mixed $firstName
	 * @return mixed
	 */
	public function setFirstname($firstName) {
		$this->_firstName = $firstName;
	}
	
	/**
	 * Get surNames
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getSurnames() {
		return $this->_surNames;
	}
	
	/**
	 * Set surNames
	 * 
	 * @access public
	 * @param mixed $surNames
	 * @return mixed
	 */
	public function setSurnames($surNames) {
		$this->_surNames = $surNames;
	}
	
	/**
	 * Get email
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getEmail() {
		return $this->_email;
	}
	
	/**
	 * Set email
	 * 
	 * @access public
	 * @param mixed $email
	 * @return mixed
	 */
	public function setEmail($email) {
		$this->_email = $email;
	}
	
	/**
	 * Get username
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getUsername() {
		return $this->_username;
	}
	
	/**
	 * Set username
	 * 
	 * @access public
	 * @param mixed $username
	 * @return mixed
	 */
	public function setUsername($username) {
		$this->_username = $username;
	}
	
	/**
	 * Get password
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPassword() {
		return $this->_password;
	}
	
	/**
	 * Set password
	 * 
	 * @access public
	 * @param mixed $password
	 * @return mixed
	 */
	public function setPassword($password) {
		$this->_password = $password;
	}
	
	/**
	 * Get newPassword
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getNewPassword() {
		return $this->_newPassword;
	}
	
	/**
	 * Set newPassword
	 * 
	 * @access public
	 * @param mixed $newPassword
	 * @return mixed
	 */
	public function setNewPassword($newPassword) {
		$this->_newPassword = $newPassword;
	}
	
	/**
	 * Get newPasswordActivationUrl
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getNewPasswordActivationUrl() {
		return $this->_newPasswordActivationUrl;
	}
	
	/**
	 * Set newPasswordActivationUrl
	 * 
	 * @access public
	 * @param mixed $newPasswordActivationUrl
	 * @return mixed
	 */
	public function setNewPasswordActivationUrl($newPasswordActivationUrl) {
		$this->_newPasswordActivationUrl = $newPasswordActivationUrl;
	}
	
	/**
	 * Get streetAddress
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getStreetAddress() {
		return $this->_streetAddress;
	}
	
	/**
	 * Set streetAddress
	 * 
	 * @access public
	 * @param mixed $streetAddress
	 * @return mixed
	 */
	public function setStreetAddress($streetAddress) {
		$this->_streetAddress = $streetAddress;
	}
	
	/**
	 * Get zip
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getZip() {
		return $this->_zip;
	}
	
	/**
	 * Set zip
	 * 
	 * @access public
	 * @param mixed $zip
	 * @return mixed
	 */
	public function setZip($zip) {
		$this->_zip = $zip;
	}
	
	/**
	 * Get city
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCity() {
		return $this->_city;
	}
	
	/**
	 * Set city
	 * 
	 * @access public
	 * @param mixed $city
	 * @return mixed
	 */
	public function setCity($city) {
		$this->_city = $city;
	}
	
	/**
	 * Get country
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCountry() {
		return $this->_country;
	}
	
	/**
	 * Set country
	 * 
	 * @access public
	 * @param mixed $country
	 * @return mixed
	 */
	public function setCountry($country) {
		$this->_country = $country;
	}
	
	/**
	 * Get phoneNumber
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPhoneNumber() {
		return $this->_phoneNumber;
	}
	
	/**
	 * Set phoneNumber
	 * 
	 * @access public
	 * @param mixed $phoneNumber
	 * @return mixed
	 */
	public function setPhoneNumber($phoneNumber) {
		$this->_phoneNumber = $phoneNumber;
	}
	
	/**
	 * Get faxNumber
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getFaxNumber() {
		return $this->_faxNumber;
	}
	
	/**
	 * Set faxNumber
	 * 
	 * @access public
	 * @param mixed $faxNumber
	 * @return mixed
	 */
	public function setFaxNumber($faxNumber) {
		$this->_faxNumber = $faxNumber;
	}
	
	/**
	 * Get organisation
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrganisation() {
		return $this->_organisation;
	}
	
	/**
	 * Set organisation
	 * 
	 * @access public
	 * @param mixed $organisation
	 * @return mixed
	 */
	public function setOrganisation($organisation) {
		$this->_organisation = $organisation;
	}
	
	/**
	 * Get website
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getWebsite() {
		return $this->_website;
	}
	
	/**
	 * Set website
	 * 
	 * @access public
	 * @param mixed $website
	 * @return mixed
	 */
	public function setWebsite($website) {
		$this->_website = $website;
	}
	
	/**
	 * Get active
	 * 
	 * @access public
	 * @return int
	 */
	public function getActive() {
		return $this->_active;
	}
	
	/**
	 * Set active
	 * 
	 * @access public
	 * @param mixed $active
	 * @return int
	 * @author b.brughmans
	 */
	public function setActive($active) {
		$this->_active = $active;
	}
}