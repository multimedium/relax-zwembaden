<?php
/**
 * Administrator class
 * 
 * @package App
 * @subpackage Administrator
 */

// Load the ProductFromFactory class
require dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdministratorDataObject.class.php';

/** 
 * Administrator class
 * 
 * @see AdministratorDataObject
 * @see AdministratorMapper
 * @package App
 * @subpackage Administrator
 */
class Administrator extends AdministratorDataObject {
	/**
	 * Get mapper
	 * 
	 * @see M_DataObject::getMapper()
	 * @access public
	 * @return AdministratorMapper
	 */
	public function getMapper() {
		M_Loader::loadDataObjectMapper('Administrator', 'administrator');
		return new AdministratorMapper();
	}
	
	/**
	 * Get picture
	 * 
	 * Will provide with an instance of {@link M_Image}, that contains a reference
	 * to the path that is returned by {@link AdministratorDataObject::getPictureFilename()}.
	 * 
	 * NOTE:
	 * This method always returns an instance of {@link M_Image}. However, this 
	 * does not necessarily mean that the image exists. To check if the image
	 * exists, you can use {@link M_Image::exists()}.
	 * 
	 * @access public
	 * @return M_Image
	 */
	public function getPicture() {
		return new M_Image($this->getPictureFilename());
	}
	
	/**
	 * Get contact
	 * 
	 * This method will convert the {@link Administrator} object into an instance
	 * of {@link M_Contact}
	 * 
	 * @access public
	 * @return M_Contact
	 */
	public function getContact() {
		$contact = new M_Contact();
		$contact->setFirstname($this->getFirstname());
		$contact->setSurnames($this->getSurnames());
		$contact->setNickname($this->getUsername());
		$contact->setPicture($this->getPicture());
		$contact->addEmail(new M_ContactEmail($this->getEmail()));
		if($this->getStreetAddress() || $this->getCity() || $this->getZip() || $this->getCountry()) {
			$contact->addAddress(new M_ContactAddress(
				$this->getStreetAddress(),
				$this->getCity(),
				$this->getZip(),
				NULL,
				$this->getCountry()
			));
		}
		if($this->getPhoneNumber()) {
			$contact->addPhoneNumber(new M_ContactPhoneNumber($this->getPhoneNumber(), M_ContactPhoneNumber::TYPE_PHONE));
		}
		if($this->getFaxNumber()) {
			$contact->addPhoneNumber(new M_ContactPhoneNumber($this->getFaxNumber(), M_ContactPhoneNumber::TYPE_FAX));
		}
		if($this->getTitle()) {
			$contact->setTitle($this->getTitle());
		}
		if($this->getOrganisation()) {
			$contact->setOrganisation($this->getOrganisation());
		}
		if($this->getWebsite()) {
			$contact->addWebsite(new M_Uri($this->getWebsite()));
		}
		return $contact;
	}
	
	/**
	 * Get name (firstname + surnames)
	 * 
	 * This method will return firstname and surnames of this administrator
	 * in one string
	 * 
	 * @author b.brughmans
	 * @see AdministratorDataObject::getFirstname()
	 * @see AdministratorDataObject::getSurnames()
	 * @return string
	 */
	public function getName() {
		
		$name = $this->getFirstname() ? $this->getFirstname() : '';
		$name .= $this->getSurnames() ? ' ' . $this->getSurnames() : '';
		
		return $name;
	}
}