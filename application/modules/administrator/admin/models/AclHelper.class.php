<?php
/**
 * Provides with helper methods to support the ACL interface
 *
 * @author Ben Brughmans
 */
class AclHelper {

	/**
	 * Get all permissions for a resource
	 *
	 * Some modules/resources can contain custom {@link M_AclPermission} instances.
	 * This method will return a list of which permission holds which resources
	 *
	 * @param M_AclResource $aclResource
	 * @return M_ArrayIterator
	 */
	public static function getPermissionsForResource(M_AclResource $aclResource) {
		$permissions = array();
		
		/* @var $permission M_AclPermission */
		foreach(M_Acl::getInstance()->getPermissions() AS $permission) {
			//resource specific permissions
			if ($permission->getResourceId() == $aclResource->getResourceId()) {
				$permissions[] = $permission;
			}
			//default permissions (CRUD)
			elseif(!$permission->getResourceId()) {
				$defaultPermissions[] = $permission;
			}
		}

		//if no specific permissions are found, use the default ones
		if (count($permissions) == 0) {
			$permissions = $defaultPermissions;
		}
		
		return new M_ArrayIterator($permissions);
	}

	/**
	 * Get all default permissions
	 *
	 * This function will provide the standard permissions (CRUD)
	 *
	 * @access public statis
	 * @return M_ArrayIterator 
	 */
	public static function getDefaultPermissions() {
		$defaultPermissions = array();
		/* @var $permission M_AclPermission */
		foreach(M_Acl::getInstance()->getPermissions() AS $permission) {
			//default permissions (CRUD)
			if(!$permission->getResourceId()) {
				$defaultPermissions[] = $permission;
			}
		}

		return new M_ArrayIterator($defaultPermissions);
	}

	/**
	 * Get the {@link M_Acl} config file for a module
	 *
	 * @param string $moduleId
	 * @return M_ConfigXml|false
	 */
	public static function getAclConfigForModule($moduleId) {
		$aclConfigFile = new M_File(
			M_Loader::getModulePath($moduleId).'/acl.xml'
		);
		if (!$aclConfigFile->exists()) return false;

		return new M_ConfigXml($aclConfigFile->getPath());
	}
}