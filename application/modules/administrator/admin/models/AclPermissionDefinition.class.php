<?php
class AclPermissionDefinition {

	/**
	 * Create a new AclPermissionDefinition from an XML definition
	 * 
	 * @param M_Config $config
	 */
	public function __constructFromXml(M_Config $config) {
		foreach($config->getRequired('resources') AS $resource) {

		}
	}

	/**
	 * @var M_ArrayIterator
	 */
	protected $_resources;

	/**
	 * Bind resources to this permission
	 *
	 * If resources are binded to a permission, this permission is only
	 * applicable to this/these resources
	 *
	 * @param M_ArrayIterator $resources
	 */
	public function setResources(M_ArrayIterator $resources) {
		$this->_resources = $resources;
	}


}