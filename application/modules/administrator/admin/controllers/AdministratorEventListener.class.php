<?php
/**
 * AdministratorEventListener
 */
class AdministratorEventListener {
	
	/**
	 * @var MediaMapper
	 */
	protected $_mediaMapper;
	
	/**
	 * Construct
	 *
	 * @param AdminController $controller
	 */
	public function __construct(AdminController $controller) {
		// Add Event listeners
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_EDIT, 
			array($this, 'onDataObjectBeforeEdit')
		);
		$controller->addEventListener(
			AdminControllerEvent::DATA_OBJECT_BEFORE_CREATE,
			array($this, 'onDataObjectBeforeEdit')
		);
	}
	
	/**
	 * Do ... before object gets edited
	 * 
	 * @param AdminControllerEvent $event
	 */
	public function onDataObjectBeforeEdit(AdminControllerEvent $event) {
		$this->_addAclRoles($event);
	}
	
	/**
	 * Get the roles from {@link M_Acl} and set them in the form
	 * 
	 * @param AdminControllerEvent $event
	 * @return void
	 */
	private function _addAclRoles(AdminControllerEvent $event) {
		$form = $event->getForm();

		$roles = array();
		/* @var $role M_AclRole */
		foreach(M_Acl::getInstance()->getRoles() AS $role) {
			$roles[$role->getRoleId()] = $role->getTitle();
		}
		$field = $form->getField('aclRoleId');
		if ($field) $field->setItems($roles);
	}
}