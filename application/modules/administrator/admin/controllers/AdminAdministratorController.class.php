<?php
/**
 * AdminAdministratorController
 *
 * Manages the administrator and ACL
 *
 * @package App
 * @subpackage Administrator
 *
 * @author Ben Brughmans
 */
class AdminAdministratorController extends M_Controller {

	/**
	 * Show an overview of all global permissions (not binded to a resource)
	 *
	 * NOTE: the overview will only be shown if the currently logged in
	 * administrator is allowed to view it
	 *
	 * @return void
	 */
	public function overviewAcl() {
		// Check if the currently logged in administrator is allowed to view
		if(!$this->_isAdministratorAllowed('acl', 'manageAcl')) $this->showAccessDenied();

		$this->_getAclListView()
			->setRules(new M_ArrayIterator(M_Acl::getInstance()->getRules()))
			->display();
	}

	/**
	 * Show an overview of all permissions for every resource
	 *
	 * NOTE: the overview will only be shown if the currently logged in
	 * administrator is allowed to view it
	 * 
	 * @return void
	 */
	public function overviewAclAdvanced() {

		// Check if the currently logged in administrator is allowed to view
		if(!$this->_isAdministratorAllowed('acl', 'manageAcl')) $this->showAccessDenied();

		$acl = M_Acl::getInstance();

		$resources = $acl->getResources();

		$permissions = $acl->getPermissions();

		$defaultPermissions = array();

		/* @var $permission M_AclPermission */
		foreach($permissions AS $permission) {
			$resourceId = $permission->getResourceId();
			if ($resourceId) {
				if (!isset($resources[$resourceId])) continue;
				$resource = $resources[$permission->getResourceId()];
				$resource->addPermission($permission);
			}

			//if this permission is not bound to a specific resource, we assume
			//it's a default permission (crud)
			else {
				$defaultPermissions[] = $permission;
			}
		}

		/* Assign data to view and display */
		$this->_getAclAdvancedView()
				->setResources($resources)
				->setRoles($acl->getRoles())
				->setDefaultPermissions(new M_ArrayIterator($defaultPermissions))
				->display();
	}

	/**
	 * Add or edit a global ACL-rule
	 *
	 * Not all rules need to be implied on a certain resource and/or permission.
	 * It's possible to play with these combinations,
	 *
	 * E.g.
	 * - user has no access to a resource (applies to all permissions)
	 * - user has access to all resources for all permissions (~GOD mode)
	 * - user can only view (applies to all resources)
	 * - ...
	 *
	 * @param int $id Database-id for a rule
	 */
	public function editAcl($id = null) {

		// Check if the currently logged in administrator is allowed to view
		if(!$this->_isAdministratorAllowed('acl', 'manageAcl')) $this->showAccessDenied();
		
		$form = $this->_getAclRuleForm()
				->setRedirectUrl(M_Form::OK, M_Request::getLink('admin/route/administrator/overviewAcl'))
				->setRedirectUrl(M_Form::KO, M_Request::getLink('admin/route/administrator/overviewAcl'))
				->setResultMessage(M_Form::OK, t('Your changes have been saved successfully'))
				->setResultMessage(M_Form::KO, t('Oops'));

		$rs = $form->run();

		//show the view if the form fails to validate, or has not yet been run
		if (!$rs) {
			//get the role
			$rule = M_Acl::getInstance()->getRuleData($id);
			if ($rule) {
				$form->setRole($rule['role']);
				if ($rule['resource']) $form->setResource($rule['resource']);
				if ($rule['permission']) $form->setPermission($rule['permission']);
				$form->setType($rule['type']);
				$form->setRuleId($rule['database_id']);
			}

			//show the view
			$this->_getAclRuleEditView()
				->setForm($form)
				->display();
		}
	}

	/**
	 * Delete a rule
	 * 
	 * @param int $id
	 */
	public function deleteAcl($id) {

		// Check if the currently logged in administrator is allowed to view
		if(!$this->_isAdministratorAllowed('acl', 'manageAcl')) $this->showAccessDenied();
		
		if (M_Acl::getInstance()->removeRuleByDatabaseId($id)) {
			M_Messenger::getInstance()->addMessage(
				t('The rule has succesfully been deleted'),
				M_Messenger::SUCCESS
			);
		}else {
			M_Messenger::getInstance()->addMessage(
				t('The rule could not be deleted'),
				M_Messenger::SUCCESS
			);
		}

		M_Header::redirect('admin/route/administrator/overviewAcl');
	}

	/**
	 * Show an overview of all available {@link M_AclRole} instances
	 *
	 * Each user has one role: this role defines what the user can or cannot
	 * do in multimanage
	 *
	 * NOTE: the overview will only be shown if the currently logged in
	 * administrator is allowed to view it
	 * 
	 * @return void
	 */
	public function overviewRole() {
		// Check if the currently logged in administrator is allowed to view
		if(!$this->_isAdministratorAllowed('aclRoles', 'manageRoles')) $this->showAccessDenied();

		$this->_getAclRoleListView()
				->setRoles(M_Acl::getInstance()->getRoles())
				->display();
	}

	/**
	 * Edit a {@link M_AclRole} instance, or add a new one
	 * 
	 * @param int $id
	 */
	public function editRole($id = null) {
		// Check if the currently logged in administrator is allowed to view
		if(!$this->_isAdministratorAllowed('aclRoles', 'manageRoles')) $this->showAccessDenied();

		$form = $this->_getAclRoleForm()
				->setRedirectUrl(M_Form::OK, M_Request::getLink('admin/route/administrator/overviewRole'))
				->setRedirectUrl(M_Form::KO, M_Request::getLink('admin/route/administrator/overviewRole'))
				->setResultMessage(M_Form::OK, t('Your changes have been saved successfully'))
				->setResultMessage(M_Form::KO, t('Oops'));

		//get the role
		$role = M_Acl::getInstance()->getRole($id);
		if ($role) {
			$form->setRole($role);
		}
		$rs = $form->run();

		//show the view if the form fails to validate, or has not yet been run
		if (!$rs) {
			$this->_getAclRoleEditView()
				->setForm($form)
				->display();
		}

	}

	/**
	 * Remove a role from the ACL
	 *
	 * @param string $id
	 */
	public function removeRole($id) {

		// Check if the currently logged in administrator is allowed to view
		if(!$this->_isAdministratorAllowed('aclRoles', 'manageRoles')) $this->showAccessDenied();
		
		$acl = M_Acl::getInstance();
		$role = $acl->getRole($id);

		//remove the role if it has been found
		if ($role) {
			//check if this role isn't in use
			$administratorMapper = M_Loader::getDataObjectMapper('Administrator', 'administrator');
			$roleCount = $administratorMapper->addFilter(new M_DbQueryFilterWhere('aclRoleId', $id))
							->getCount();

			//role in use
			if ($roleCount > 0) {
				//set a message to notify the user
				M_Messenger::getInstance()->addMessage(
					p(
						'The role "@role" could not be deleted: it is still used by @count administrator',
						'The role "@role" could not be deleted: it is still used by @count administrators',
						$roleCount,
						array('@role' => $role->getTitle(), '@count' => $roleCount)
					)
				);
			}

			//role not in use: continue
			else {
				$title = $role->getTitle();
				$acl->removeRole($role);

				//set a message to notify the user
				M_Messenger::getInstance()->addMessage(
					t('The role "@role" has successfully been removed from the ACL', array('@role' => $title))
				);
			}
		}

		//if role does not exist, notify the user
		else {
			M_Messenger::getInstance()->addMessage(
				t('The role could not be deleted: role does not exist.'),
				M_Messenger::FAILURE
			);
		}

		//back to overview page
		M_Header::redirect(M_Request::getLink('admin/route/administrator/overviewRole'));
	}

	/**
	 * Update the ACL for a role + resource + permission
	 *
	 * This action is called from within the {@link AdminAdministratorController::overviewAcl()}
	 * method
	 */
	public function updateAclAjax() {

		$role = M_Request::getVariable('role');
		$resource = M_Request::getVariable('resource');
		$permission = M_Request::getVariable('permission');
		$type = M_Request::getVariable('type');

		//cast to null or integers to make sure the correct action is taken
		if ($type === '') $type = null; //will delete rule
		else $type = (int)$type; //will delete rule and add new allow/deny rule

		$acl = M_Acl::getInstance();

		//check if a rule exists for this combination, if yes: remove it
		$rule = $acl->getRuleStatus($role, $resource, $permission);
		if (!is_null($rule)) {
			//remove rule expects an integer, not a boolean
			$aclRule = ($rule) ? M_Acl::RULE_ALLOW : M_ACL::RULE_DENY;
			$acl->removeRule($aclRule, $role, $resource, $permission);
		}

		//add a new rule, if user chose "allow" or "deny"
		if ($type === 1) {
			$acl->addAllowRule($role, $resource, $permission);
		}elseif($type === 0) {
			$acl->addDenyRule($role, $resource, $permission);
		}
	}

	/**
	 * Get a list of permissions for a resource
	 */
	public function getPermissionByResourceAjax() {
		$resourceId = M_Request::getVariable('resource');

		AdminLoader::loadModel('AclHelper', 'administrator');
		if($resourceId) {
			$permissions = AclHelper::getPermissionsForResource(
				M_Acl::getInstance()->getResource($resourceId)
			);
		} else {
			$permissions = AclHelper::getDefaultPermissions();
		}

		$out = '<option value="0">'.t('All').'</option>';
		/* @var $permission M_AclPermission */
		foreach($permissions AS $permission) {
			$out .= '<option value="'.$permission->getPermissionId().'">'.$permission->getTitle().'</option>';
		}

		echo $out;
	}

	/**
	 * Install ACL
	 *
	 * Remove all resources and permissions from acl and add them back again
	 */
	public function installAcl() {
		M_Loader::loadController('InstallAdministratorController', 'administrator');
		$administratorController = new InstallAdministratorController();
		$administratorController->acl();
		$administratorController->customAcl();
	}

	/**
	 * Install Custom ACL
	 *
	 * Reinstall all custom acl resources and permissions
	 */
	public function installCustomAcl() {
		M_Loader::loadController('InstallAdministratorController', 'administrator');
		$administratorController = new InstallAdministratorController();
		$administratorController->customAcl();
	}

	/**
	 * This function will load and display the "Access Denied" page
	 *
	 * @access public
	 * @return void
	 */
	public function showAccessDenied() {
		// Fetch the proper view
		$view = M_Loader::getView('AdminAccessDeniedView', 'admin');

		// Display the view
		$view->display();

		die();
	}

	/** -- PRIVATE methods -- */

	/**
	 * Is Administrator Allowed
	 *
	 * This method will check if the currently logged in administrator
	 * is allowed to view a specified item.
	 *
	 * NOTE: for now, we will simply check if we should allow to the ACL
	 * module as a whole, or not at all.
	 *
	 * TODO: implement extended access rules
	 *
	 * @access private
	 * @param string $resource
	 *		The acl resource name (in this case 'acl' or 'administrator')
	 * @param string $permission
	 *		The permission we need to check the rule of (view, delete, ...)
	 * @return bool
	 *		Returns TRUE if the administrator is allowed to view the item,
	 *		FALSE if not
	 */
	private function _isAdministratorAllowed($resource, $permission) {
		return M_Acl::getInstance()->isAllowed(
			$this->_getAdministrator()->getAclRoleId(),
			$resource,
			$permission
		);
	}

	/**
	 * Get the view which will present a list of {@link M_AclRole} whic aren't
	 * attached to a resource or permission
	 *
	 * @return AclView
	 */
	private function _getAclListView() {
		return Adminloader::getView('AclListView', 'administrator');
	}

	/**
	 * Get the view which will present a all permission or all resources and
	 * all roles
	 *
	 * @return AclAdvancedView
	 */
	private function _getAclAdvancedView() {
		return Adminloader::getView('AclAdvancedView', 'administrator');
	}

	/**
	 * Get the view which will present a list of {@link M_AclRole}
	 *
	 * @return AclRoleListView
	 */
	private function _getAclRoleListView() {
		return Adminloader::getView('AclRoleListView', 'administrator');
	}

	/**
	 * Get the view which will present a form to edit {@link M_AclRole}
	 *
	 * @return AclRoleEditView
	 */
	private function _getAclRoleEditView() {
		return Adminloader::getView('AclRoleEditView', 'administrator');
	}

	/**
	 * Get the view which will present a form to edit ACL rules
	 *
	 * @return AclRuleEditView
	 */
	private function _getAclRuleEditView() {
		return Adminloader::getView('AclRuleEditView', 'administrator');
	}

	/**
	 * Get the form which will allow the user to add or edit {@link M_AclRole}
	 *
	 * @return AclRoleForm
	 */
	private function _getAclRoleForm() {
		return Adminloader::getForm('AclRoleForm', 'administrator');
	}

	/**
	 * Get the form which will allow the user to add or edit global rules
	 *
	 * @return AclRuleForm
	 */
	private function _getAclRuleForm() {
		return Adminloader::getForm('AclRuleForm', 'administrator');
	}

	/**
	 * Will fetch and return the currently logged in administrator
	 *
	 * @access protected
	 * @return Administrator
	 *		The currently logged in {@link Administrator}
	 */
	protected function _getAdministrator() {
		return M_Loader::getDataObjectMapper('Administrator', 'administrator')
				->getById(M_Auth::getInstance()->getIdentity());
	}

}