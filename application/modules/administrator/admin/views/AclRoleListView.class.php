<?php
class AclRoleListView extends AdminPageView {
	
	/**
	 * @var M_ArrayIterator
	 */
	private $_roles;

	/**
	 * Assign a list of {@link AclRole} instances to the view
	 * 
	 * @param M_ArrayIterator $roles
	 * @return AclRoleListView
	 */
	public function setRoles(M_ArrayIterator $roles) {
		$this->_roles = $roles;
		return $this;
	}

	/**
	 * Do some preprocessing
	 */
	protected function preprocessing() {
		$this->assign('roles', $this->_roles);
	}
	
	/**
	 * Get the resource for this view
	 *
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('administrator').'/templates/AclRoleList.tpl'
		);
	}
}