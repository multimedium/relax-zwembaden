<?php
class AclRoleEditView extends AdminPageView {
	
	/**
	 * @var AclRoleForm
	 */
	private $_form;

	/**
	 * Assign a {@link AclRoleForm} to the view
	 * 
	 * @param AclRoleForm $form
	 * @return AclRoleEditView
	 */
	public function setForm(AclRoleForm $form) {
		$this->_form = $form;
		return $this;
	}

	/**
	 * Do some preprocessing
	 */
	protected function preprocessing() {
		$this->assign('form', $this->_form);
	}
	
	/**
	 * Get the resource for this view
	 *
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('administrator').'/templates/AclRoleEdit.tpl'
		);
	}
}