<?php
class AclListView extends AdminPageView {
	
	/**
	 * @var M_ArrayIterator
	 */
	private $_rules;

	/**
	 * Assign a list of rules to the view
	 *
	 * @param M_ArrayIterator $rules
	 * @return AclView
	 */
	public function setRules(M_ArrayIterator $rules) {
		$this->_rules = $rules;
		return $this;
	}

	/**
	 * Do some preprocessing
	 */
	protected function preprocessing() {
		$this->assign('rules', $this->_rules);
	}
	
	/**
	 * Get the resource for this view
	 *
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('administrator').'/templates/AclList.tpl'
		);
	}
}