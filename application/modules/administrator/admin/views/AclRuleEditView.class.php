<?php
class AclRuleEditView extends AdminPageView {
	
	/**
	 * @var AclRuleForm
	 */
	private $_form;

	/**
	 * Assign a {@link AclRuleForm} to the view
	 * 
	 * @param AclRoleForm $form
	 * @return AclRuleEditView
	 */
	public function setForm(AclRuleForm $form) {
		$this->_form = $form;
		return $this;
	}

	/**
	 * Do some preprocessing
	 */
	protected function preprocessing() {
		if (!$this->_form) {
			throw new M_Exception('No form available');
		}

		//assign form to view
		$this->assign('form', $this->_form);
	}
	
	/**
	 * Get the resource for this view
	 *
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(
			AdminLoader::getAdminResourcesPath('administrator').'/templates/AclRuleEdit.tpl'
		);
	}
}