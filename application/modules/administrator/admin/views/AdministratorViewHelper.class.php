<?php
/**
 * AdministratorViewHelper
 *
 * @access public
 * @package app
 * @subpackage admnistrator
 */
class AdministratorViewHelper {

	/* -- PUBLIC FUNCTIONS -- */


	/**
	 * Add Source Pane Menu
	 *
	 * This function can be used to add menu items to the source pane menu
	 *
	 * @access public
	 * @return array $menu
	 *		Array containing the menu items that are to be added
	 */
	public function addSourcePaneMenu() {

		$menu = M_Menu::getInstance('Acl');
		$menu->setTitle(t('ACL'));

		// Before adding the item, we have to check if this user is allowed
		$rule = M_Acl::getInstance()->isAllowed($this->_getAdministrator()->getAclRoleId(), 'acl', 'manageAcl');
		if($rule) {
			$menu->addMenuItem(new M_MenuItem('admin/route/administrator/overview-acl', t('Acl')));
			$menu->addMenuItem(new M_MenuItem('admin/route/administrator/overview-acl-advanced', t('Acl Advanced')));
		}

		$rule2 = M_Acl::getInstance()->isAllowed($this->_getAdministrator()->getAclRoleId(), 'aclRoles', 'manageRoles');
		if($rule2) {
			$menu->addMenuItem(new M_MenuItem('admin/route/administrator/overview-role', t('Acl Roles')));
		}

		return array($menu);
	}

	/**
	 * Set Source Pane Menu Weight
	 *
	 * This function can be used to set the weight of each added source pane
	 * menu item. The lower the weight, the higher it will be shown in the menu.
	 *
	 * @access public
	 * @param M_Menu $menu
	 * @return int
	 */
	public function setSourcePaneMenuWeight(M_Menu $menu) {
		switch($menu->getId()) {
			case 'Acl':
				return 3;
			
			case 'Administrators':
				return 6;
		}
	}

	/* -- PRIVATE FUNCTIONS -- */

	/**
	 * Will fetch and return the currently logged in administrator
	 *
	 * @access private
	 * @return Administrator
	 *		The currently logged in {@link Administrator}
	 */
	private function _getAdministrator() {
		return M_Loader::getDataObjectMapper('Administrator', 'administrator')
				->getById(M_Auth::getInstance()->getIdentity());
	}
}