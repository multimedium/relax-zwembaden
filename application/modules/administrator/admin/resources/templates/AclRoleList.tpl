<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="list">
	<table>
		<thead>
			<tr>
				<th>{t text="Code"}</th>
				<th>{t text="Title"}</th>
				<th>{t text="Description"}</th>
			</tr>
		</thead>
		<tbody>
			{foreach from=$roles key="i" item="r"}
			<tr class="{cycle values="row1,row2"}">
				<td class="tooltip">
					<ul>
						<li><a class="confirm delete" href="{link href="admin/route/administrator/removeRole/"}{$r->getRoleId()}">
							   <span>{t text="delete"}</span>
							</a>
						</li>
						<li><a class="edit" href="{link href="admin/route/administrator/editRole/"}{$r->getRoleId()}">
							   <span>{t text="edit"}</span>
							</a>
						</li>
					</ul>
				</td>
				<td>{$r->getRoleId()}</td>
				<td>{$r->getTitle()}</td>
				<td>{$r->getDescription()}</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
	</div>
</div>
<div id="administrator-aclRole-list-options" class="menu-sticky-bottom">
	<a id="idListPlusButton" class="button-plus popmenu" href="#">+</a>

	<div id="idListPlusButtonPopmenu" style="position: absolute; z-index: -100; top: -1000px; left: -1000px; display: none; height: 0px;">
		<ul>
			<li>
				<a href="{link href="admin/route/administrator/editRole/0"}">
					{t text="Create a new item in <strong>ACL Roles</strong>"}
				</a>
			</li>
		</ul>
	</div>
</div>
{include file="../javascript/_tooltip.js"}