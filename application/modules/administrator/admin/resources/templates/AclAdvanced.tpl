{cssfile file="application/modules/administrator/admin/resources/css/acl.css"}
{jsfile file="application/modules/administrator/admin/resources/javascript/acl.js"}

{assign var=roleCount value=$roles->count()}
<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="breadcrumb">
		<ul>
			<li><a href="{link href="admin/route/administrator/overview-acl"}">{t text="ACL"}</a></li>
			<li><span title="{t|att text="Manage your ACL"}">{t text="Advanced overview"}</span></li>
		</ul>
	</div>

	{* Hide / show all permissions which are not set
	<input type="checkbox" id="emptyRules" /> <label for="emptyRules">{t text="Toggle empty rules"}</label>
	*}
	
	{* Iteratate through all modules *}
	{foreach from=$resources item=moduleResources key=moduleId name="modules"}
	<div class="module" id="module-{$moduleId}">
		<h2{if $smarty.foreach.modules.first} class="first"{/if}>{$moduleId}</h2>

		{* For every module: iterate through it's resources *}
		{foreach from=$moduleResources item=resource}
		<h3>{$resource->getDataObjectId()}</h3>
		<table class="acl-overview" cellspacing="0">
		<tbody>

		{* Print the roles for every resource again *}
		<tr class="roles">
			<td></td>
		{foreach from=$roles item=role}
			<td>{$role->getTitle()}</td>
		{/foreach}
		</tr>

		{* Print a row for every permission *}
		{assign var=permissions value=$resource->getPermissions()}
		{if $permissions->count() == 0}
			{assign var=permissions value=$defaultPermissions}
		{/if}
		{foreach from=$permissions item=permission}
		<tr class="role-row {cycle values="row1,row2"}">
			<td>{$permission->getPermissionId()}</td>

			{* For every resource and permission: print all roles *}
			{foreach from=$roles item=role}
			{assign var=status value=$role->getRuleStatus($resource, $permission)}
			<td>
				<select name="acl|{$role->getRoleId()}|{$resource->getResourceId()}|{$permission->getPermissionId()}">
					<option value="">{t text="-"}</option>
					<option value="1"{if $status === true} selected="selected"{/if}>{t text="allow"}</option>
					<option value="0"{if $status === false} selected="selected"{/if}>{t text="deny"}</option>
				</select></td>
			{/foreach}
		</tr>

		{* End permission iteration*}
		{/foreach}

		{* End resource iteration *}
			</tbody>
		</table>
		{/foreach}

		{* End module iteration *}
	</div>
		{/foreach}
</div>
<div id="administrator-acl-list-options" class="menu-sticky-bottom">
	<a id="idListPlusButton" class="button-plus popmenu" href="#">+</a>

	<div id="idListPlusButtonPopmenu" style="position: absolute; z-index: -100; top: -1000px; left: -1000px; display: none; height: 0px;">
		<ul>
			<li>
				<a href="{link href="admin/route/administrator/editAcl/0"}">
					{t text="Create a new global rule in <strong>ACL</strong>"}
				</a>
			</li>
		</ul>
	</div>
</div>
