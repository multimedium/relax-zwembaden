{cssfile file="application/modules/administrator/admin/resources/css/acl.css"}
{jsfile file="application/modules/administrator/admin/resources/javascript/acl.js"}

<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="list">
	<table>
		<thead>
			<tr>
				<th>{t text="Role"}</th>
				<th>{t text="Type"}</th>
				<th>{t text="Permission"}</th>
				<th>{t text="Resource"}</th>
			</tr>
		</thead>
		<tbody>
			{foreach from=$rules key="i" item="r"}
			<tr class="{cycle values="row1,row2"}">
				<td class="tooltip">
					<ul>
						<li><a class="confirm delete" href="{link href="admin/route/administrator/deleteAcl/"}{$r.database_id}">
							   <span>{t text="delete"}</span>
							</a>
						</li>
						<li><a class="edit" href="{link href="admin/route/administrator/editAcl/"}{$r.database_id}">
							   <span>{t text="edit"}</span>
							</a>
						</li>
					</ul>
				</td>
				<td>{$r.role->getTitle()}</td>
				<td>{if $r.type}{t text="allow"}{else}deny{/if}</td>
				<td>{if $r.permission}{$r.permission->getPermissionId()}{else}{t text="all"}{/if}</td>
				<td>{if $r.resource}{$r.resource->getResourceId()}{else}{t text="All"}{/if}</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
	</div>
</div>
<div id="administrator-aclRule-list-options" class="menu-sticky-bottom">
	<a id="idListPlusButton" class="button-plus popmenu" href="#">+</a>

	<div id="idListPlusButtonPopmenu" style="position: absolute; z-index: -100; top: -1000px; left: -1000px; display: none; height: 0px;">
		<ul>
			<li>
				<a href="{link href="admin/route/administrator/editAcl/0"}">
					{t text="Create a new item in <strong>ACL</strong>"}
				</a>
			</li>
		</ul>
	</div>
</div>
{include file="../javascript/_tooltip.js"}