{capture name="tooltip"}
{*	There is no easy way to apply a tooltip on a custom list, that's why we've
	created a include file which does this for us. If, in the future, we want
	to modify the tooltip, we only need to change this include file
*}
	{literal}
	$('.list td.tooltip').M_Tooltip({
		tooltipClass : 'row-options-tooltip',
		tooltipPositioningReferenceX : $('#idContentPaneViewport'),
		tooltipPositioningReferenceY : $('#idContentPaneViewport')
	});

	// We create a fixed table head
	// (we wait until tooltips are initiated, because the tooltips will
	// affect the presence of th and td)
	/* setFixedTableHeadInList(); */

	// Init magic CSS, when tooltip is shown:
	$(document).bind('M_Tooltip_Show', function($eventObject, $parentElement, $tooltipElement) {
		initMagicCss($tooltipElement, { fillRemainingVspace : 'ignore' });
	});
	{/literal}
{/capture}

{jsinline script=$smarty.capture.tooltip context="tooltip"}