$(document).ready(function(){

	/**
	 * Update an ACL rule
	 */
	$('.acl-overview select').change(function(){
		var $ids = $(this).attr('name').split('|');
		$.M_AjaxRequest({
			url : jQueryBaseHref + '/admin/route/administrator/updateAclAjax',
			data : {
				role : $ids[1],
				resource: $ids[2],
				permission: $ids[3],
				type: $(this).val()
			},
			dataType : 'json',
			success : function(data) {
				//no action taken
			}
		});
	});

	/**
	 * Get the correct permissions for a resource
	 */
	$('#aclrule #resource').change(function(){
		$.M_AjaxRequest({
			url : jQueryBaseHref + '/admin/route/administrator/getPermissionByResourceAjax',
			data : {
				resource: $(this).val()
			},
			success : function(data) {
				$('#permission').html(data);
			}
		});
	});

	/**
	 * Show or hide all rules which are empty for all roles
	
	$('#emptyRules').click(function(){
		//hide empty rules
		if ($(this).attr('checked')) {

			//for every rule-row: check if each select is empty
			$('.role-row').each(function(){
				var $row = $(this);
				var emptyRule = true;
				$('select', $(this)).each(function(){
					if (emptyRule && $(this).val() != '') {
						emptyRule = false;
					}
				});

				if (emptyRule) $row.hide();
			});

			//now iteratate back again through all
			$('.acl-overview').each(function(){
				var $visibleRows = $('.role-row:visible', $(this)).length;
				var $module = $(this).closest('.module');
				if ($visibleRows == 0) {
					$module.hide();
				}
			});
		}
		//show all rows back again
		else {
			$('.module').show();
			$('.role-row:hidden').show();
		}
	});
	 */
});