<?php
class AclRuleForm extends M_Form {

	/**
	 * Create a new form
	 */
	public function __construct() {
		$this->_addFieldRole()
			->_addFieldResource()
			->_addFieldPermission()
			->_addFieldType()
			->setVariable('rule-id', '');
		
		parent::__construct('aclrule');
	}

	/**
	 * Add the role field to the form
	 * @return AclRuleForm
	 */
	protected function _addFieldRole() {
		$field = new M_FieldSelect('role');

		/* @var $role M_AclRole */
		foreach(M_Acl::getInstance()->getRoles() AS $role) {
			$field->setItem($role->getRoleId(), $role->getTitle());
		}
		$field->setMandatory(true);
		$field->setTitle(t('Role'));
		$this->addField($field);
		return $this;
	}

	/**
	 * Add the resource field to the form
	 * @return AclRuleForm
	 */
	protected function _addFieldResource() {
		$field = new M_FieldSelect('resource');
		/* @var $resource M_AclResource */
		$field->setItem('', t('All'));
		foreach(M_Acl::getInstance()->getResources() AS $resource) {
			$field->setItem($resource->getResourceId(), $resource->getTitle() .' ('.$resource->getResourceId().')');
		}
		$field->setTitle(t('Resource'));
		$this->addField($field);
		return $this;
	}

	/**
	 * Add the permission field to the form
	 * @return AclRuleForm
	 */
	protected function _addFieldPermission() {
		$field = new M_FieldSelect('permission');
		$field->setItem('', t('All'));

		// Fetch the default permissions, adding them to the field
		AdminLoader::loadModel('AclHelper', 'administrator');
		$permissions = AclHelper::getDefaultPermissions();
		foreach($permissions as $permission) {
			/* @var $permission M_AclPermission */
			$field->setItem($permission->getPermissionId(), $permission->getTitle());
		}
		
		$field->setTitle(t('Permission'));
		$this->addField($field);
		return $this;
	}

	/**
	 * Add the type field to the form
	 * @return AclRuleForm
	 */
	protected function _addFieldType() {
		$field = new M_FieldSelect('type');
		$field->setTitle(t('Type'));
		$field->setItem(M_Acl::RULE_ALLOW, t('Allow'));
		$field->setItem(M_Acl::RULE_DENY, t('Deny'));
		$this->addField($field);
		return $this;
	}

	/**
	 * Set the id for the rule we are editing
	 * 
	 * @param int $id
	 * @return AclRuleForm
	 */
	public function setRuleId($id) {
		$this->setVariable('rule-id', $id);
		return $this;
	}

	/**
	 * Set the role for the rule we are editing
	 *
	 * @param M_AclRole $role
	 * @return AclRuleForm
	 */
	public function setRole(M_AclRole $role) {
		$this->getField('role')->setDefaultValue($role->getRoleId());
		return $this;
	}

	/**
	 * Set the resource for the rule we are editing
	 *
	 * @param M_AclResource $resource
	 * @return AclRuleForm
	 */
	public function setResource(M_AclResource $resource) {
		$this->getField('resource')->setDefaultValue($resource->getResourceId());
		Adminloader::loadModel('AclHelper', 'administrator');

		//load the correct permissions for this field
		$permissionField = $this->getField('permission');
		foreach(AclHelper::getPermissionsForResource($resource) AS $permission) {
			$permissionField->setItem(
				$permission->getPermissionId() ,
				$permission->getTitle()
			);
		}
		return $this;
	}

	/**
	 * Set the permission for the rule we are editing
	 *
	 * @param M_AclPermission $permission
	 * @return AclRuleForm
	 */
	public function setPermission(M_AclPermission $permission) {
		$this->getField('permission')->setDefaultValue($permission->getPermissionId());
		return $this;
	}

	/**
	 * Set the type for the rule we are editing
	 *
	 * @param int $type
	 * @return AclRuleForm
	 */
	public function setType($type) {
		$this->getField('type')->setDefaultValue((int)$type);
		return $this;
	}

	/**
	 * Save the role
	 * 
	 * @param array $values
	 */
	public function actions(array $values) {
		$acl = M_Acl::getInstance();

		//remove the existing rule
		$acl->removeRuleByDatabaseId($this->getVariable('rule-id'));
		//set null value
		if ($values['permission'] == '0') $values['permission'] = null;

		//add a new rule
		$acl->addRule(
			$values['type'],
			$acl->getRole($values['role']),
			$acl->getResource($values['resource']),
			$acl->getPermission($values['permission'])
		);

		return true;
	}
}