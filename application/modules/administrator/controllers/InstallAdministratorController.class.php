<?php
class InstallAdministratorController extends M_Controller {

	/**
	 * Install
	 */
	public function install() {
		self::acl();
		self::aclCustom();
	}

	/**
	 * Install ACL resources and permissions
	 *
	 * @author Tom Bauwens
	 */
	public static function acl() {
		// Resources
		$table = M_Db::getInstance()->getTable('acl_resources');
		$table->truncate();

		// For each of the modules in the application:
		M_Loader::loadRelative('application/modules/admin/core/AdminLoader.class.php');
		AdminLoader::loadController('AdminControllerHelper', 'admin');
		AdminControllerHelper::loadAdminCoreClasses();
		
		foreach(AdminApplication::getModules() as $module) {
			// If manageable
			/* @var $module AdminModule */
			if($module->isMultimanageable() || $module->getId() == 'admin') {

				// Create a resource for the whole module
				$table->insert(array(
					'acl_resource_code'        => $module->getId(),
					'acl_resource_title'       => $module->getName(),
					'acl_resource_description' => ''
				));
				
				// For each of the data objects in the module:
				foreach($module->getDataObjectNames() as $id => $name) {
					// Create the resource:
					$table->insert(array(
						'acl_resource_code'        => $module->getId() . '-' . $id,
						'acl_resource_title'       => $module->getName(),
						'acl_resource_description' => ''
					));
				}
			}
		}

		// Permissions
		$table = M_Db::getInstance()->getTable('acl_permissions');
		$table->truncate();
		$table->insert(array(
			'acl_permission_code'  => 'create',
			'acl_permission_title' => 'Create new item'
		));
		$table->insert(array(
			'acl_permission_code'  => 'edit',
			'acl_permission_title' => 'Edit an item'
		));
		$table->insert(array(
			'acl_permission_code'  => 'delete',
			'acl_permission_title' => 'Delete an item'
		));
		$table->insert(array(
			'acl_permission_code'  => 'view',
			'acl_permission_title' => 'View'
		));
	}

	/**
	 * Parse all acl.xml files for every module, and add uninstalled permissions
	 * and resources
	 *
	 * @author Ben Brughmans
	 */
	public static function aclCustom() {
		$storage = new M_AclStorageDb();
		$acl = $storage->getAcl();
		AdminLoader::loadModel('AclHelper', 'administrator');
		/* @var $helper AclHelper */

		//for every module: check if an acl config file exists
		foreach(M_Application::getModules() AS $module) {
			$config = AclHelper::getAclConfigForModule($module->getId());

			//no config found for this module
			if (!$config) continue;

			//get all permissions and check if a resource is binded to it
			/* @var $permission M_Config */
			foreach($config->acl AS $permissionId => $permission) {
				//permission does not yet exist: create a new one
				if ($acl->getPermission($permissionId)) continue;
				
				$aclPermission = new M_AclPermission($permissionId);
				if ($permission->__isset('title')) {
					$aclPermission->setTitle($permission->title);
				}
				if ($permission->__isset('description')) {
					$aclPermission->setTitle($permission->description);
				}

				//add resource?
				if ($permission->__isset('resource')) {
					$resource = $permission->resource;
					$resourceId = $resource->getRequired('id');
					//if the resource does not yet exist: create a new one
					if(!$acl->getResource($resourceId)) {
						$aclResource = new M_AclResource($resourceId);
						if ($resource->__isset('title')) {
							$aclResource->setTitle($resource->title);
						}
						if ($resource->__isset('description')) {
							$aclResource->setDescription($resource->description);
						}
						$acl->addResource($aclResource);
					}

					//this permission only applies to this resource
					$aclPermission->setResourceId($resourceId);
				}
				//store the permission
				$acl->addPermission($aclPermission);
			}
		}
	}
}

