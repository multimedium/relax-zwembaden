<?php
class PortfolioController extends PageController {

	/**
	 * Router
	 * 
	 * @return array
	 */
	public function __router() {
		return array(
			'album' => 'album',
			'project' => 'project'
		);
	}

	/**
	 * Show the pictures for one album (relaxzwembaden page)
	 * 
	 * @param string $url
	 */
	public function album($url = null) {
		// Construct the view
		$view = M_loader::getView('PortfolioAlbumView', 'portfolio');
		$view->setActivePath('relaxzwembaden');

		//get the root album
		$rootAlbum = $this->_getMediaAlbumMapper()->addFilterRealm('relaxzwembaden')->getOne();

		/* @var $view HomeView */
		$mapper = $this->_getMediaAlbumMapper()
					->addFilterParentAlbum($rootAlbum)
					->addFilterPublished()
					->addFilterMediaPublished()
					->addFilterMediaImage()
					->addFilterDefaultSorting();

		//get the list of albums
		$albums = $mapper->getAll();

		//get the album itself
		if (!is_null($url)) {
			//get one album
			$album = $mapper->addFilter(new M_DbQueryFilterWhere('url', $url))
						->getOne();
		}

		//no album specified: get the first
		else {
			$album = $albums->first();
		}

		/* @var $album MediaAlbum */
		if(!$album) {
			$this->_404();
		}

		// create a pagejumper
		$pj = $this->_getPagejumper($album->getMediaCount(true), 1);
		$pj->setNumberOfPages(10);
		// get the active media item
		$media = new M_ArrayIterator($album->getMedia()->getArrayCopy());
		$media->seek($pj->getPage()-1);
		$media = $media->current();

		if ($album->getDescription()) {
			$text = new M_Text($album->getDescription());
			$view->setPageDescription($text->getSnippet(155));
		}
		
		// display the view
		$view->setMediaAlbum($album)
			->setMedia($media)
			->setPageJumper($pj)
			->setMediaAlbums($albums)
			->setPageTitle($album->getTitle() . ' - Relaxzwembaden')
			->display();
	}

	/**
	 * Show the pictures for one album
	 *
	 * @param string $url
	 */
	public function project($url = null) {
		// Construct the view
		$view = M_loader::getView('PortfolioProjectView', 'portfolio');
		$view->setActivePath('projecten');

		//get the root album
		$rootAlbum = $this->_getMediaAlbumMapper()->addFilterRealm('projecten')->getOne();

		/* @var $view HomeView */
		$mapper = $this->_getMediaAlbumMapper()
					->addFilterParentAlbum($rootAlbum)
					->addFilterPublished()
					->addFilterMediaPublished()
					->addFilterMediaImage()
					->addFilterDefaultSorting();

		//get the list of albums
		$albums = $mapper->getAll()->getArrayCopy();

		// create a pagejumper for the projects
		$pjProjects = $this->_getPagejumper(count($albums), 12);
		$pjProjects->setNumberOfPages(1);
		$pjProjects->getTotalCountOfPages();

		//get the album itself
		if (!is_null($url)) {
			//get one album
			$album = $mapper->addFilter(new M_DbQueryFilterWhere('url', $url))
						->getOne();

			/* @var $album MediaAlbum */
			if(!$album) {
				$this->_404();
			}

			//if this is a direct link to the project (and no pager is used)
			//we need to set the active page ourselves. We do this by iteration
			//through the list of albums and checking the position for this album
			//in that list
			if(!M_Request::getVariable('page')) {
			    foreach($albums as $key => $currentAlbum) {
                    if ($currentAlbum->getId() == $album->getId()) {

                        //this album is on spot x in the list, now we can calculate the page we need to show
                        $index = $key+1;
                        $page = ceil($index/12);
                        $pjProjects->setPage($page);
                        break;
                    }
                }
			}
		}

		$range = $pjProjects->getRange();
		$albumsRange = array();
		for($i = $range[0]-1; $i < $range[1] ; $i++) {
			$albumsRange[$albums[$i]->getId()] = $albums[$i];
		}

		//if no specific album is selected: get the first one of the range
		if(is_null($url)) {
            $albumsRangeCopy = $albumsRange;
			$album = array_shift($albumsRangeCopy);
		}

		// create a pagejumper for the pics
		$pj = $this->_getPagejumper($album->getMediaCount(true), 1, 'foto');
		$pj->setQueryVariableName('foto');
		$pj->setNumberOfPages(10);

		// get the active media item
		$media = new M_ArrayIterator($album->getMedia()->getArrayCopy());
		$media->seek($pj->getPage()-1);
		$media = $media->current();

		if ($album->getDescription()) {
			$text = new M_Text($album->getDescription());
			$view->setPageDescription($text->getSnippet(155));
		}

		// display the view
		$view->setMediaAlbum($album)
			->setMedia($media)
			->setPageJumper($pj)
			->setPageJumperProjects($pjProjects)
			->setMediaAlbums(new M_ArrayIterator($albumsRange))
			->setPageTitle($album->getTitle() . ' - Relaxzwembaden')
			->display();
	}
}