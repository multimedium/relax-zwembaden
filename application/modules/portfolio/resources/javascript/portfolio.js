$(document).ready(function(){
	var pjWidth = $('#portfolio-pagejumper ul li').length * 15;

	//subtract 5 pixels for every other browser than IE6
	if ($.browser.msie && $.browser.version.substr(0,1) > 6) {
		pjWidth -= 5;
	}

	$('#portfolio-pagejumper ul').css({width: pjWidth+'px', margin: '0 auto'});
});