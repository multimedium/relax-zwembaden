{jsfile file="application/modules/portfolio/resources/javascript/portfolio.js" context="portfolio"}
{cssfile file="application/modules/portfolio/resources/css/portfolio.css" context="portfolio"}
<div id="portfolio" class="content-wrapper">
	<div id="portfolio-sidebar">
		<table>

			{* First row: navigation and picture *}
			<tr>
				<td>
					<ul id="portfolio-nav" class="portfolio-nav">
					{foreach from=$mediaAlbums item=a}
					{assign var="active" value=''}
					{if $a->getId() == $mediaAlbum->getId()}
						{assign var="active" value=' class="active"'}
					{/if}
						<li{$active}><a href="{link href="relaxzwembaden/"}{$a->getUrl()}" title="{t|att text="bekijk album @album" album=$a->getTitle()}">{$a->getTitle()}</a></li>
					{/foreach}
					</ul>

					<div id="portfolio-text">
						{$mediaAlbum->getDescription()}
					</div>
				</td>
			</tr>

			{* Second row: pagejumper *}
			<tr>
				<td class="bottom">
					{assign var=pages value=$pagejumper->getPages()}
					<div id="portfolio-pagejumper">
						<ul>
							{assign var=albumUrl value=$mediaAlbum->getUrl()}
							<li class="previous">
							{if !$pagejumper->isFirstPage()}
								<a title="{t|att text="vorige pagina"}" href="{link href="relaxzwembaden/$albumUrl"}?page={$pagejumper->getPage()-1}">&lt;</a>
							{/if}
							</li>
							{foreach from=$pages item='page'}
								<li {if $page.selected}class="active"{/if}>
									{if $page.selected}
										{$page.number}
									{else}
										<a title="{t|att text="foto @page" page=$page.number}" href="{$page.href}">{$page.number}</a>
									{/if}
								</li>
							{/foreach}
							<li class="next">
							{if !$pagejumper->isLastPage()}
								<a title="{t|att text="volgende pagina"}" href="{link href="relaxzwembaden/$albumUrl"}?page={$pagejumper->getPage()+1}">&gt;</a>
							{/if}
							</li>
						</ul>
					</div>
				</td>
			</tr>
		</table>
		<div class="clear"></div>
	</div>

	<div id="portfolio-image">
			<img src="{link href="thumbnail/portfolio/"}{$media->getId()}/{$media->getBasenameFromTitle()}" alt="{$media->getTitle()|att}" />
			{if !$pagejumper->isFirstPage()}
				<a id="porfolio-image-previous" title="{t|att text="vorige pagina"}" href="{link href="relaxzwembaden/$albumUrl"}?page={$pagejumper->getPage()-1}">&lt;</a>
			{/if}
			{if !$pagejumper->isLastPage()}
				<a id="porfolio-image-next" title="{t|att text="volgende pagina"}" href="{link href="relaxzwembaden/$albumUrl"}?page={$pagejumper->getPage()+1}">&gt;</a>
			{/if}
		</div>

	<div class="clear"></div>
</div>