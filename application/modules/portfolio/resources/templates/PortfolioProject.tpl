{jsfile file="application/modules/portfolio/resources/javascript/portfolio.js" context="portfolio"}
{cssfile file="application/modules/portfolio/resources/css/portfolio.css" context="portfolio"}
<div id="portfolio" class="content-wrapper">
	<div id="portfolio-sidebar">
		<table>
			{* First row: navigation *}
			<tr>
				<td>
					<ul id="project-nav" class="portfolio-nav">
						<li class="previous">
						{if !$pagejumperProjects->isFirstPage()}
						<a title="{t|att text="vorige pagina"}" href="{link href="projecten/$albumUrl"}?page={$pagejumperProjects->getPage()-1}">^</a>
						{/if}
						</li>

					{assign var=range value=$pagejumperProjects->getRange()}
					{foreach from=$mediaAlbums item='a' name="projects"}
						{* Use the pagejumper to navigate through the different projects. *}
						{assign var="active" value=''}
						{if $a->getId() == $mediaAlbum->getId()}
							{assign var="active" value='active'}
						{/if}
						<li class="{if $smarty.foreach.projects.last}last {/if}{$active}">
							<a href="{link href="projecten/"}{$a->getUrl()}" title="{t|att text="bekijk album @album" album=$a->getTitle()}">
							{$a->getTitle()}
							</a>
						</li>
					{/foreach}

						<li class="next">
						{if !$pagejumperProjects->isLastPage()}
						<a title="{t|att text="volgende pagina"}" href="{link href="projecten/$albumUrl"}?page={$pagejumperProjects->getPage()+1}">&or;</a>
						{/if}
						</li>
					</ul>
				</td>
			</tr>

			{* Second row: pagejumper *}
			<tr>
				<td class="bottom">
					{assign var=pages value=$pagejumper->getPages()}
					<div id="portfolio-pagejumper">
						<ul>
							{assign var=albumUrl value=$mediaAlbum->getUrl()}
							<li class="previous">
							{if !$pagejumper->isFirstPage()}
								<a title="{t|att text="vorige pagina"}" href="{link href="projecten/$albumUrl"}?foto={$pagejumper->getPage()-1}">&lt;</a>
							{/if}
							</li>
							{foreach from=$pages item='page'}
								<li {if $page.selected}class="active"{/if}>
									{if $page.selected}
										{$page.number}
									{else}
										<a title="{t|att text="foto @page" page=$page.number}" href="{$page.href|escape}">{$page.number}</a>
									{/if}
								</li>
							{/foreach}
							<li class="next">
							{if !$pagejumper->isLastPage()}
								<a title="{t|att text="volgende pagina"}" href="{link href="projecten/$albumUrl"}?foto={$pagejumper->getPage()+1}">&gt;</a>
							{/if}
							</li>
						</ul>
					</div>
				</td>
			</tr>
		</table>
		
		<div class="clear"></div>
	</div>

	<div id="portfolio-image">
		<img src="{link href="thumbnail/portfolio/"}{$media->getId()}/{$media->getBasenameFromTitle()}" alt="{$media->getTitle()|att}" />
		{if !$pagejumper->isFirstPage()}
			<a id="porfolio-image-previous" title="{t|att text="vorige pagina"}" href="{link href="projecten/$albumUrl"}?foto={$pagejumper->getPage()-1}">&lt;</a>
		{/if}
		{if !$pagejumper->isLastPage()}
			<a id="porfolio-image-next" title="{t|att text="volgende pagina"}" href="{link href="projecten/$albumUrl"}?foto={$pagejumper->getPage()+1}">&gt;</a>
		{/if}
	</div>

	<div class="clear"></div>
</div>