<?php
/**
 * PortfolioAlbumView
 *
 * @package App
 * @subpackage Portfolio
 */
M_Loader::loadView('PortfolioView', 'portfolio');

class PortfolioAlbumView extends PortfolioView {

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get the resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('portfolio', 'PortfolioAlbum.tpl');
	}
}