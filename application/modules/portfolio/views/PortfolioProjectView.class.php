<?php
/**
 * PortfolioAlbumView
 *
 * @package App
 * @subpackage Portfolio
 */
M_Loader::loadView('PortfolioView', 'portfolio');

class PortfolioProjectView extends PortfolioView {

	/**
	 * Stores the pagejumper
	 *
	 * @var M_Pagejumper
	 */
	private $_pj;

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Set the pagejumper
	 *
	 * @param M_Pagejumper $pj
	 * @return PortfolioView
	 */
	public function setPageJumperProjects(M_Pagejumper $pj) {
		$this->_pjProjects = $pj;
		return $this;
	}

	/**
	 * Preprocessing
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Assign presentation variables
		$this->assign('pagejumperProjects', $this->_pjProjects);
		parent::preProcessing();
	}

	/**
	 * Get the resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('portfolio', 'PortfolioProject.tpl');
	}
}