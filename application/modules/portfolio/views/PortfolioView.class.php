<?php
/**
 * PortfolioView
 *
 * @package App
 * @subpackage Portfolio
 */
abstract class PortfolioView extends PageView {

	/**
	 * Stores the album
	 *
	 * @var Media
	 */
	private $_mediaAlbum;

	/**
	 * Stores the list of media-albums
	 * 
	 * @var M_ArrayIterator
	 */
	private $_mediaAlbums;

	/**
	 * Stores the Media-item
	 *
	 * @var Media
	 */
	private $_media;

	/**
	 * Stores the pagejumper
	 *
	 * @var M_Pagejumper
	 */
	private $_pj;

	/**
	 * Set the list of media-albums
	 *
	 * @param M_ArrayIterator $albums
	 * @return PortfolioView
	 */
	public function setMediaAlbums(M_ArrayIterator $albums){
		$this->_mediaAlbums = $albums;
		return $this;
	}

	/**
	 * Set the active media-item
	 *
	 * @param Media $media
	 * @return PortfolioView
	 */
	public function setMedia(Media $media){
		$this->_media = $media;
		return $this;
	}

	/**
	 * Set the active media-album
	 *
	 * @param MediaAlbum $album
	 * @return PortfolioView
	 */
	public function setMediaAlbum(MediaAlbum $album){
		$this->_mediaAlbum = $album;
		return $this;
	}

	/**
	 * Set the pagejumper
	 * 
	 * @param M_Pagejumper $pj
	 * @return PortfolioView
	 */
	public function setPageJumper(M_Pagejumper $pj) {
		$this->_pj = $pj;
		return $this;
	}

	/**
	 * Preprocessing
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Assign presentation variables
		$this->assign('mediaAlbum', $this->_mediaAlbum);
		$this->assign('mediaAlbums', $this->_mediaAlbums);
		$this->assign('media', $this->_media);
		$this->assign('pagejumper', $this->_pj);
	}

	/* -- PRIVATE/PROTECTED -- */
}