<?php
/**
 * NewsController
 * 
 * @package App
 * @subpackage News
 */
class NewsController extends PageController {

	const NEWS_ITEMS_PER_PAGE = 3;

	/**
	 * @return array
	 */
	public function __router() {
		return array(
			'index' => 'index',
			'(:string)' => 'detail/$1'
		);
	}

	/**
	 * List view
	 *
	 * @return void
	 */
	public function index($page = null) {
		//get the mapper and add filters
		$mapper = $this->_getNewsMapper()
					->addFilterOrderStartDate()
					->addFilterPublishedOnly();

		//create a pagejumper and add it to the mapper
		$pj = $this->_getPagejumper($mapper->getCount(), self::NEWS_ITEMS_PER_PAGE);
		if(!is_null($page)) $pj->setPage ($page);
		$mapper->addFilter(new M_DbQueryFilterPagejumper($pj));

		//add data to the view and display
		$view = $this->_getNewsListView()
					->setActivePath('nieuws')
					->setPagejumper($pj)
					->setNews($mapper->getAll())
					->setPageTitle(t('Zwembaden - nieuws - pagina '.$pj->getPage()))
					->setPageDescription(t('Binnenzwembad of buitenzwembad, Relaxzwembaden zorgt voor waterplezier: lees ons laatste nieuws - pagina '.$pj->getPage()))
					->display();
	}

	/**
	 * Get the item and re-route to the correct page
	 *
	 * @param string $url
	 */
	public function detail($url) {
		$newsItem = $this->_getNewsMapper()->addFilterPublishedOnly()->addFilterUrl($url)->getOne();

		if (!$newsItem) $this->_404();
		//get the mapper and add filters
		$mapper = $this->_getNewsMapper()
					->addFilterOrderStartDate()
					->addFilterPublishedOnly();
		$ids = $mapper->getIds();

		$i = 1;
		foreach($ids AS $id) {
			if ($id == $newsItem->getId()) {
				//this album is on spot x in the list, now we can calculate the page we need to show
				$page = ceil($i/self::NEWS_ITEMS_PER_PAGE);
				$this->index($page);
				die();
			}

			$i++;
		}

		//we cannot end here...
		throw new M_Exception(sprintf(
			"Newsitem %s is published but wasn't found in the list",
			$newsItem->getTitle()
		));


	}

	/**
	 * RSS: Highlighted news
	 * 
	 * Will create an RSS Feed out of the news items in the website.
	 * 
	 * @access public
	 * @return void
	 */
	public function rss() {
		// Get News mapper:
		$mapper = $this
			->_getNewsMapper()
			->addFilterPublishedOnly()
			->addFilterOrderStartDate();
		
		// Construct a new RSS Feed
		$feed = new M_FeedRss();
		$feed->setTitle(t('Nieuws'));
		$feed->setLink(M_Request::getUri('nieuws/rss'));
		$feed->setDescription(t("Blijf op de hoogte van het laatste nieuws van Relaxzwembaden"));
		
		// For each of the news items
		foreach($mapper->getAll() as $news) {
			// Construct the description text:
			$desc = new M_Text($news->getText());
			
			// Construct a new item in the feed:
			$item = new M_FeedRssItem();
			
			// Set the title of the feed item
			$item->setTitle($news->getTitle());
			
			// Set the description of the feed item
			$item->setDescription($desc->getSnippet(500));
			
			// Set the link to the detail page in the website
			$item->setLink(
				new M_Uri(
					M_Request::getLink('nieuws/'.$news->getUrl())
				)
			);
			
			// If the news item has a start date:
			if($news->getStartDate()) {
				// Then, we use the start date as the publication date of the
				// feed item
				$item->setPublicationDate($news->getStartDate());
			}
			
			// Add the item to the feed:
			$feed->addItem($item);
		}

		// Flush the feed
		M_Header::send(M_Header::CONTENT_TYPE_XML);
		echo $feed->toString();
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get mapper: News Mapper
	 *
	 * @access protected
	 * @return NewsMapper $mapper
	 * 		The requested data object mapper
	 */
	protected function _getNewsMapper() {
		return M_Loader::getDataObjectMapper('News', 'news');
	}

	
	/**
	 * Get view: List of news items
	 * 
	 * @access protected
	 * @return NewsListView
	 */
	protected function _getNewsListView() {
		return M_Loader::getView('NewsListView', 'news');
	}
}