{cssfile file="application/modules/news/resources/css/news.css" context="news"}

<div id="news" class="content-wrapper">
	{if $news|@count > 0}
	<ul id="news-list">
		{foreach from=$news item=n name=news}
			{* Calculate the column-number so we can use it ass CSS-classname *}
			{capture name="column"}{math equation="x % 3" x=$smarty.foreach.news.iteration}{/capture}
			{assign var="column" value=$smarty.capture.column}
			{if $column == 0}{assign var="column" value=3}{/if}
			
			<li class="column-{$column}">{include file="_newsItem.tpl" news=$n}</li>
		{/foreach}
	</ul>

	<div id="pagejumper">
		{if $pagejumper->isFirstpage() == false}
			<a class="left" title="{t|att text="vorige pagina"}" href="{link href="nieuws?page="}{$pagejumper->getPage()-1}">{t|escape text="< vorige nieuws berichten"}</a>
		{/if}
		{if $pagejumper->isLastpage() == false}
			<a class="right" title="{t|att text="volgende pagina"}" href="{link href="nieuws?page="}{$pagejumper->getPage()+1}">{t|escape text="meer nieuws berichten lezen >"}</a>
		{/if}
	</div>

	{else}
		{t text="Geen nieuws beschikbaar op dit moment."}
	{/if}
	<div class="clear"></div>
</div>