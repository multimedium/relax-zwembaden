{assign var=images value=$news->getMedia('image', 3)->getArrayCopy()}
{assign var=imageCount value=$images|@count}
{assign var=colspan value=""}
{if $imageCount > 1}
	{assign var=colspan value=" colspan='$imageCount'"}
{/if}
<table>
	<tr>
		<td class="title-row"{$colspan}>
			<h2><span>*</span> {$news->getTitle()}</h2>
		</td>
	</tr>
	{if $imageCount > 0}
	<tr>
		{* Show only one image *}
		{if $imageCount == 1}
		{assign var="image" value=$images.0}

		<td class="image-list-item">
			<a title="{$image->getTitle()|att}" href="{link href="thumbnail/full/"}{$image->getId()}/{$image->getBasenameFromTitle()}" class="fancybox" rel="news-{$news->getId()}">
				<img src="{link href="thumbnail/news-single/"}{$image->getId()}/{$image->getBasenameFromTitle()}" alt="{$image->getTitle()|att}" />
			</a>
		</td>
		
		{* Multiple images *}
		{elseif $imageCount > 1}
			{foreach from=$images item=image name="images"}
			<td class="image-list-item{if !$smarty.foreach.images.last} image-list-item-last{/if}">
				<a title="{$image->getTitle()|att}" href="{link href="thumbnail/full/"}{$image->getId()}/{$image->getBasenameFromTitle()}" class="fancybox" rel="news-{$news->getId()}">
					<img src="{link href="thumbnail/news-multiple/"}{$image->getId()}/{$image->getBasenameFromTitle()}" alt="{$image->getTitle()|att}" />
				</a>
			</td>
			{/foreach}
		{/if}
	</tr>
	{/if}
	<tr>
		<td{$colspan} class="news-description">
			<div>
				{$news->getText()}
			</div>
		</td>
	</tr>
</table>