<?php
/** 
 * NewsDataObject class
 * 
 * @see NewsDataObjectMapper
 * @see NewsDataObjectModel
 * @package App
 */
M_Loader::loadDataObject('DataObjectWithMedia', 'media');

class NewsDataObject extends DataObjectWithMedia {
	
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link NewsDataObject::getId()}
	 * - {@link NewsDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * categoryId
	 * 
	 * This property stores the categoryId. To get/set the value of the
	 * categoryId, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getCategoryId()}
	 * - {@link NewsDataObject::setCategoryId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_categoryId;
	
	/**
	 * startdate
	 * 
	 * This property stores the startdate. To get/set the value of the
	 * startdate, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getStartDate()}
	 * - {@link NewsDataObject::setStartDate()}
	 * 
	 * @access protected
	 * @var M_Date
	 */
	protected $_startdate;
	
	/**
	 * enddate
	 * 
	 * This property stores the enddate. To get/set the value of the enddate,
	 * read the documentation on 
	 * 
	 * - {@link NewsDataObject::getEndDate()}
	 * - {@link NewsDataObject::setEndDate()}
	 * 
	 * @access protected
	 * @var M_Date
	 */
	protected $_enddate;
	
	/**
	 * title ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * title. To get/set the value of the title, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getTitle()}
	 * - {@link NewsDataObject::getTitleId()}
	 * - {@link NewsDataObject::setTitle()}
	 * - {@link NewsDataObject::setTitleId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_titleId;
	
	/**
	 * url ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * url. To get/set the value of the url, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getUrl()}
	 * - {@link NewsDataObject::getUrlId()}
	 * - {@link NewsDataObject::setUrl()}
	 * - {@link NewsDataObject::setUrlId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_urlId;
	
	/**
	 * text ID
	 * 
	 * This property stores the numeric reference to the translated text of
	 * text. To get/set the value of the text, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getText()}
	 * - {@link NewsDataObject::getTextId()}
	 * - {@link NewsDataObject::setText()}
	 * - {@link NewsDataObject::setTextId()}
	 * 
	 * @access protected
	 * @var integer
	 */
	protected $_textId;
	
	/**
	 * attachment
	 * 
	 * This property stores the attachment. To get/set the value of the
	 * attachment, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getAttachment()}
	 * - {@link NewsDataObject::setAttachment()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_attachment;
	
	/**
	 * attachmentName
	 * 
	 * This property stores the attachmentName. To get/set the value of the
	 * attachmentName, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getAttachmentName()}
	 * - {@link NewsDataObject::setAttachmentName()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_attachmentName;
	
	/**
	 * homepage
	 * 
	 * This property stores the homepage. To get/set the value of the
	 * homepage, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getHomepage()}
	 * - {@link NewsDataObject::setHomepage()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_homepage;
	
	/**
	 * published
	 * 
	 * This property stores the published. To get/set the value of the
	 * published, read the documentation on 
	 * 
	 * - {@link NewsDataObject::getPublished()}
	 * - {@link NewsDataObject::setPublished()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_published;
	
	/**
	 * order
	 * 
	 * This property stores the order. To get/set the value of the order,
	 * read the documentation on 
	 * 
	 * - {@link NewsDataObject::getOrder()}
	 * - {@link NewsDataObject::setOrder()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_order;
	
	/**
	 * Get title
	 * 
	 * Get title in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the title
	 * @return string|NULL
	 */
	public function getTitle($locale = NULL) {
		return $this->_getLocalizedText('title', $locale);
	}
	
	/**
	 * Get title ID
	 * 
	 * Get numeric reference to the translated title .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTitleId() {
		return $this->_titleId;
	}
	
	/**
	 * Set title
	 * 
	 * Set title, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated title
	 * @param string $locale
	 * 	The language in which to set the title
	 * @return void
	 */
	public function setTitle($text, $locale = NULL) {
		$this->_setLocalizedText('title', $text, $locale);
	}
	
	/**
	 * Set title ID
	 * 
	 * Set numeric reference to the translated title .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of title
	 * @return void
	 */
	public function setTitleId($id) {
		$this->_titleId = $id;
	}
	
	/**
	 * Get url
	 * 
	 * Get url in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the url
	 * @return string|NULL
	 */
	public function getUrl($locale = NULL) {
		return $this->_getLocalizedText('url', $locale);
	}
	
	/**
	 * Get url ID
	 * 
	 * Get numeric reference to the translated url .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getUrlId() {
		return $this->_urlId;
	}
	
	/**
	 * Set url
	 * 
	 * Set url, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated url
	 * @param string $locale
	 * 	The language in which to set the url
	 * @return void
	 */
	public function setUrl($text, $locale = NULL) {
		$this->_setLocalizedText('url', $text, $locale);
	}
	
	/**
	 * Set url ID
	 * 
	 * Set numeric reference to the translated url .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of url
	 * @return void
	 */
	public function setUrlId($id) {
		$this->_urlId = $id;
	}
	
	/**
	 * Get text
	 * 
	 * Get text in the requested language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale categories.
	 * 
	 * @access public
	 * @param string $locale
	 * 	The language in which to get the text
	 * @return string|NULL
	 */
	public function getText($locale = NULL) {
		return $this->_getLocalizedText('text', $locale);
	}
	
	/**
	 * Get text ID
	 * 
	 * Get numeric reference to the translated text .
	 * 
	 * @access public
	 * @return integer
	 */
	public function getTextId() {
		return $this->_textId;
	}
	
	/**
	 * Set text
	 * 
	 * Set text, in a given language
	 * 
	 * NOTE:
	 * If no language is provided to this method, this method will use the
	 * language that has been set in the locale category LANG. See {@link
	 * M_Locale::getCategory()} for more information about locale 
	 * categories.
	 * 
	 * @access public
	 * @param string $text
	 * 	The translated text
	 * @param string $locale
	 * 	The language in which to set the text
	 * @return void
	 */
	public function setText($text, $locale = NULL) {
		$this->_setLocalizedText('text', $text, $locale);
	}
	
	/**
	 * Set text ID
	 * 
	 * Set numeric reference to the translated text .
	 * 
	 * @access public
	 * @param integer $id
	 * 	The numeric reference value to translations of text
	 * @return void
	 */
	public function setTextId($id) {
		$this->_textId = $id;
	}
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return NewsDataObject
	 */
	public function setId($id) {
	$this->_id = $id;
		return $this;
	}
	
	/**
	 * Get categoryId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getCategoryId() {
		return $this->_categoryId;
	}
	
	/**
	 * Set categoryId
	 * 
	 * @access public
	 * @param mixed $categoryId
	 * @return NewsDataObject
	 */
	public function setCategoryId($categoryId) {
	$this->_categoryId = $categoryId;
		return $this;
	}
	
	/**
	 * Get startdate
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getStartDate() {
		return $this->_startdate;
	}
	
	/**
	 * Set startdate
	 * 
	 * @access public
	 * @param M_Date $startdate
	 * @return NewsDataObject
	 */
	public function setStartDate( M_Date $startdate) {
	$this->_startdate = $startdate;
		return $this;
	}
	
	/**
	 * Get enddate
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getEndDate() {
		return $this->_enddate;
	}
	
	/**
	 * Set enddate
	 * 
	 * @access public
	 * @param M_Date $enddate
	 * @return NewsDataObject
	 */
	public function setEndDate( M_Date $enddate = null) {
	$this->_enddate = $enddate;
		return $this;
	}
	
	/**
	 * Get attachment
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getAttachment() {
		return $this->_attachment;
	}
	
	/**
	 * Set attachment
	 * 
	 * @access public
	 * @param mixed $attachment
	 * @return NewsDataObject
	 */
	public function setAttachment($attachment) {
	$this->_attachment = $attachment;
		return $this;
	}
	
	/**
	 * Get attachmentName
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getAttachmentName() {
		return $this->_attachmentName;
	}
	
	/**
	 * Set attachmentName
	 * 
	 * @access public
	 * @param mixed $attachmentName
	 * @return NewsDataObject
	 */
	public function setAttachmentName($attachmentName) {
	$this->_attachmentName = $attachmentName;
		return $this;
	}
	
	/**
	 * Get homepage
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getHomepage() {
		return $this->_homepage;
	}
	
	/**
	 * Set homepage
	 * 
	 * @access public
	 * @param mixed $homepage
	 * @return NewsDataObject
	 */
	public function setHomepage($homepage) {
	$this->_homepage = $homepage;
		return $this;
	}
	
	/**
	 * Get published
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getPublished() {
		return $this->_published;
	}
	
	/**
	 * Set published
	 * 
	 * @access public
	 * @param mixed $published
	 * @return NewsDataObject
	 */
	public function setPublished($published) {
	$this->_published = $published;
		return $this;
	}
	
	/**
	 * Get order
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOrder() {
		return $this->_order;
	}
	
	/**
	 * Set order
	 * 
	 * @access public
	 * @param mixed $order
	 * @return NewsDataObject
	 */
	public function setOrder($order) {
	$this->_order = $order;
		return $this;
	}
	
	/**
	 * Get mapper
	 * 
	 * The mapper is the connection between the object and database or
	 * other storage platform
	 * 
	 * @access public
	 * @return NewsMapper $mapper
	 */
	public function getMapper() {
		if (is_null($this->_mapper)) {
			M_Loader::loadDataObjectMapper("News","news");
			$this->_mapper = new NewsMapper();
		}
		return $this->_mapper;
	}
	
		
}