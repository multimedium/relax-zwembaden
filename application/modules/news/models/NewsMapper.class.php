<?php
/**
 * NewsMapper
 * 
 * @package App
 * @subpackage News
 */
class NewsMapper extends M_DataObjectMapperCache {

	/**
	 * @var array
	 */
	protected $_filtersEnddate = array();

	/**
	 * @var array
	 */
	protected $_filtersStartdate = array();

	/**
	 * Add a filter to retrieve news-items after a certain start date, or all
	 * news-items without a startdate (NULL)
	 *
	 * @param M_Date $startdate
	 *		Leave empty to use the current time
	 * @return NewsMapper
	 */
	public function addFilterStartDate(M_Date $startdate = null) {
		if (is_null($startdate)) $startdate = new M_Date();
		$this->_filtersStartdate[] = $startdate;
		return $this;
	}

	/**
	 * Add a filter to retrieve news-items before a certain end date, or all
	 * news-items without a enddate (NULL)
	 *
	 * @param M_Date $startDate
	 *		Leave empty to use the current time
	 * @return NewsMapper
	 */
	public function addFilterEndDate(M_Date $enddate = null) {
		if (is_null($enddate)) $enddate = new M_Date();
		$this->_filtersEnddate[] = $enddate;
		return $this;
	}

	/**
	 * Add a filter to this mapper so only (un)published media will be retrieved
	 *
	 * @param bool $published
	 * @return MediaMapper
	 */
	public function addFilterPublished($published = true) {
		$this->addFilter(new M_DbQueryFilterWhere('published', (bool)$published));
		return $this;
	}

	/**
	 * Add a filter to this mapper so only items selected for homepage will
	 * be fetched
	 *
	 * @param bool $homepage
	 * @return MediaMapper
	 */
	public function addFilterHomepage($homepage = true) {
		$this->addFilter(new M_DbQueryFilterWhere('homepage', (bool)$homepage));
		return $this;
	}

	/**
	 * Add filters to retrieve only published news items: we will query on
	 * published, startdate and enddate
	 *
	 * @return NewsMapper
	 */
	public function addFilterPublishedOnly() {
		return $this->addFilterEndDate()->addFilterStartDate()->addFilterPublished();
	}

	/**
	 * Add a filter to select a newsitem by it's url
	 * 
	 * @param string $arg
	 * @return NewsMapper
	 */
	public function addFilterUrl($arg) {
		return $this->addFilter(new M_DbQueryFilterWhere('url', $arg));
	}

	/**
	 * Add a filter to this mapper so news-items will be ordered by startdate
	 *
	 * @return NewsMapper
	 */
	public function addFilterOrderStartDate($order = 'ASC') {
		$this->addFilter(new M_DbQueryFilterOrder('startdate', $order));
		return $this;
	}

	/**
	 * Get the filtered query with filters on enddate if applicable
	 *
	 * @param M_DbQuery $query
	 * @return M_DbQuery
	 */
	protected function _getFilteredQueryEnddate(M_DbQuery $query) {
		if (count($this->_filtersEnddate) == 0) return $query;

		/* @var $enddate M_Date */
		foreach($this->_filtersEnddate AS $enddate) {
			$query->openWhereBlock(M_DbQuery::OPERATOR_AND);
			$query->where('end_date >= ?', $enddate->getMysqlDateTime());
			$query->orWhere('end_date IS NULL');
			$query->closeWhereBlock();
		}
		return $query;
	}

	/**
	 * Get the filtered query with filters on startdate if applicable
	 *
	 * @param M_DbQuery $query
	 * @return M_DbQuery
	 */
	protected function _getFilteredQueryStartdate(M_DbQuery $query) {
		if (count($this->_filtersStartdate) == 0) return $query;

		/* @var $startdate M_Date */
		foreach($this->_filtersStartdate AS $startdate) {
			$query->openWhereBlock(M_DbQuery::OPERATOR_AND);
			$query->where('start_date <= ?', $startdate->getMysqlDateTime());
			$query->orWhere('start_date IS NULL');
			$query->closeWhereBlock();
		}
		return $query;
	}

	/**
	 * Get the filtered query with additional filters
	 *
	 * @param MI_DbQuery $query
	 * @param array $filters
	 * @return M_DbQuery
	 */
	protected function _getFilteredQuery(MI_DbQuery $query, array $filters) {
		// 3. apply start date filters
		return $this->_getFilteredQueryStartdate(
			// 2. apply enddate filters
			$this->_getFilteredQueryEnddate(
				// 1. Apply regular filter(s):
				parent::_getFilteredQuery($query, $filters)
			)
		);
	}

	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'news';
	}
}