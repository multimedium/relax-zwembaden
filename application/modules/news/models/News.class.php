<?php
/**
 * News
 * 
 * @package App
 * @subpackage News
 */

// Load superclass
M_Loader::loadDataObject('NewsDataObject', 'news');

/**
 * News
 * 
 * @package App
 * @subpackage News
 */
class News extends NewsDataObject {
	
	/* -- SETTERS -- */
	
	/**
	 * Set Title
	 * 
	 * @access public
	 * @param mixed $text
	 * @return mixed
	 */
	public function setTitle($text, $locale = NULL) {
		parent::setTitle($text, $locale);
		$this->setUrl(M_Uri::getPathElementFromString($text), $locale);
	}
}