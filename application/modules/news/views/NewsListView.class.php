<?php
/**
 * NewsListView
 * 
 * @package App
 * @subpackage News
 */
class NewsListView extends PageView {
	
	/* -- PROPERTIES -- */
	
	/**
	 * News
	 * 
	 * This property stores the news items that are to be shown in the view, and is 
	 * populated by {@link NewsListView::setNews()}.
	 * 
	 * @access private
	 * @var M_ArrayIterator $news
	 * 		The collection of {@link News} instances
	 */
	private $_news;

	/**
	 * @var M_Pagejumper
	 */
	private $_pagejumper;

	/* -- SETTERS -- */

	/**
	 * Set News
	 *
	 * This method will set the news items that are to be shown in the view.
	 *
	 * @access public
	 * @param M_ArrayIterator $news
	 * 		The collection of {@link News} instances
	 * @return NewsListView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setNews(M_ArrayIterator $news) {
		$this->_news = $news;
		return $this;
	}

	/**
	 * Set News
	 *
	 * This method will set the news items that are to be shown in the view.
	 *
	 * @access public
	 * @param M_Pagejumper $pj
	 * @return NewsListView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setPagejumper(M_Pagejumper $pj) {
		$this->_pj = $pj;
		return $this;
	}

	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure the collection of news items has been provided:
		if(! $this->_news) {
			// If not, throw an exception
			throw new M_ViewException(sprintf(
				'%s cannot render the view; please provide news items to %s::%s()',
				__CLASS__,
				__CLASS__,
				'setNews'
			));
		}

		// Assign presentation variables to the template:
		$this->assign('news', $this->_news);
		$this->assign('pagejumper', $this->_pj);
	}
	
	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('news', 'NewsList.tpl');
	}
}