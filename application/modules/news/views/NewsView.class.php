<?php
/**
 * NewsView
 * 
 * ... motherclass to all news-related views
 * 
 * @package App
 * @subpackage News
 */
abstract class NewsView extends PageView {
}