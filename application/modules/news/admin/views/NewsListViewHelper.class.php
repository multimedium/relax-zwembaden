<?php
class NewsListViewHelper {

	/**
	 * Set column value
	 *
	 * Manipulates the values shown in the list view. A string is
	 * expected from this method.
	 *
	 * NOTE:
	 * If this method does not return anything, it will be ignored!
	 *
	 * @access public
	 * @param AdminListColumnDefinition $column
	 * 		The column for which to set the value
	 * @param News $dataObject
	 * 		The instance for which to set the value
	 * @param mixed $value
	 * 		The value that has been prepared by {@link AdminListView}.
	 * 		This will be the listed value, if not manipulated by this
	 * 		method.
	 * @return string|null
	 */
	public function setColumnValue(AdminListColumnDefinition $column, News $dataObject, $value) {
		return null;
	}
}