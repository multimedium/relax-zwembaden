<?php
/**
 * ContactController class
 * 
 * @package App
 * @subpackage Contact
 */
class ContactController extends PageController {

	/**
	 * Route traffic
	 * 
	 * @return array
	 */
	public function __router() {
		return array(
			'bedankt' => 'thankyou',
			'oeps' => 'error',
		);
	}

	/**
	 * Contact page
	 * 
	 * @access public
	 * @return void
	 */
	public function index() {
		// Construct the form:
		$form = $this->_getContactForm();
		
		// Set Redirection URL for when form has been completed
		$form->setRedirectUrl(M_Form::OK, 'contact/bedankt');
		$form->setRedirectUrl(M_Form::KO, 'contact/oeps');
		
		// If the form has not been sent, or validation errors have
		// been generated, we display the form:
		
		if(! $form->run()) {
			// Provide the view with the form, and render for display:
			$this
				->_getContactFormView()
				->setContactForm($form)
				->setActivePath('contact')
				->setPageTitle(t('Zwembadbouwer in Herenthout - Antwerpen'))
				->setPageDescription(t('U bent op zoek naar een zwembad, of totaalproject met tuinontwerp? Contacteer Relaxzwembaden'))
				->display();
		}
	}
	
	/**
	 * Contact Confirmation page
	 * 
	 * @access public
	 * @return void
	 */
	public function thankyou() {
		$this
			->_getContactThankyouView()
			->setPageTitle(t('Contact us'))
			->setActivePath('contact')
			->setPageDescription(t('Questions? Comments? Please contact us, and we will get back to you as soon as possible.'))
			->display();
	}
	
	/**
	 * Contact Error page
	 * 
	 * @access public
	 * @return void
	 */
	public function error() {
		$this
			->_getContactErrorView()
			->setPageTitle(t('Contact us'))
			->setActivePath('contact')
			->setPageDescription(t('Questions? Comments? Please contact us, and we will get back to you as soon as possible.'))
			->display();
	}
	
	/**
	 * vCard Download
	 * 
	 * @access public
	 * @return void
	 */
	public function vcard() {
		// Get the vcard from the application's identity:
		$vcard = M_ApplicationIdentity
			::getInstance()
			->getContact()
			->getVcard();
		
		// Offer the vCard as a download
		M_Header::sendDownloadFromString(
			$vcard->toString(),
			new M_File($vcard->getSuggestedFilename())
		);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get view: Contact form
	 * 
	 * @access protected
	 * @return ContactFormView $view
	 * 		The requested view
	 */
	protected function _getContactFormView() {
		return M_Loader::getView('ContactFormView', 'contact');
	}
	
	/**
	 * Get view: Contact Thank You
	 * 
	 * @access protected
	 * @return ContactThankyouView $view
	 * 		The requested view
	 */
	protected function _getContactThankyouView() {
		return M_Loader::getView('ContactThankyouView', 'contact');
	}
	
	/**
	 * Get view: Contact Error
	 * 
	 * @access protected
	 * @return ContactErrorView $view
	 * 		The requested view
	 */
	protected function _getContactErrorView() {
		return M_Loader::getView('ContactErrorView', 'contact');
	}

	/**
	 * Get form
	 *
	 * @access protected
	 * @return ContactForm $form
	 * 		The requested form
	 */
	protected function _getContactForm() {
		return M_Loader::getform('ContactForm', 'contact');
	}
}