<?php
/**
 * ContactConfirmationMailView
 * 
 * @package App
 * @subpackage Contact
 */

// Load superclass
M_Loader::loadView('ContactMailView', 'contact');

/**
 * ContactConfirmationMailView
 * 
 * @package App
 * @subpackage Contact
 */
class ContactConfirmationMailView extends ContactMailView {
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('contact', 'ContactConfirmationMail.tpl');
	}
}