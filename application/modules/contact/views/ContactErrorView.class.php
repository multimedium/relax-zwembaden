<?php
/**
 * ContactErrorView
 * 
 * @package App
 * @subpackage Contact
 */

// Load superclass
M_Loader::loadView('ContactView', 'contact');

/**
 * ContactErrorView class
 *
 * @package App
 * @subpackage Contact
 */
class ContactErrorView extends ContactView {
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('contact', 'ContactError.tpl');
	}
}