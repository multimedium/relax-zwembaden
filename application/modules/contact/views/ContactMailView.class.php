<?php
/**
 * ContactMailView
 * 
 * @package App
 * @subpackage Contact
 */
class ContactMailView extends MailView {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Set first name
	 * 
	 * @access public
	 * @param string $name
	 * @return ContactMailView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setName($name) {
		$this->assign('name', (string) $name);
		return $this;
	}
	
	/**
	 * Set phone number
	 * 
	 * @access public
	 * @param string $number
	 * @return ContactMailView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setPhone($number) {
		$this->assign('phone', (string) $number);
		return $this;
	}
	
	/**
	 * Set email
	 * 
	 * @access public
	 * @param string $email
	 * @return ContactMailView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setEmail($email) {
		$this->assign('email', (string) $email);
		return $this;
	}
	
	/**
	 * Set comments
	 * 
	 * @access public
	 * @param string $comments
	 * @return ContactMailView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setComments($comments) {
		$this->assign('comments', (string) $comments);
		return $this;
	}

	/**
	 * Set info
	 *
	 * @access public
	 * @param array $info
	 * @return ContactMailView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setInfo($info) {
		$this->assign('info', (array) $info);
		return $this;
	}

	/**
	 * Set address
	 *
	 * @access public
	 * @param array $address
	 * @return ContactMailView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setAddress(M_ContactAddress $address) {
		$this->assign('address', $address);
		return $this;
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Get resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('contact', 'ContactMail.tpl');
	}
}