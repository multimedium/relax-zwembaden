<?php
/**
 * ContactThankyouView
 * 
 * @package App
 * @subpackage Contact
 */

// Load superclass
M_Loader::loadView('ContactView', 'contact');

/**
 * ContactThankyouView class
 *
 * @package App
 * @subpackage Contact
 */
class ContactThankyouView extends ContactView {
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('contact', 'ContactThankyou.tpl');
	}
}