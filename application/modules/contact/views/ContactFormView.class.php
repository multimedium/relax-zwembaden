<?php
/**
 * ContactFormView
 * 
 * @package App
 * @subpackage Contact
 */

// Load superclass
M_Loader::loadView('ContactView', 'contact');

/**
 * ContactFormView class
 * 
 * @package App
 * @subpackage Contact
 */
class ContactFormView extends ContactView {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Form
	 * 
	 * Stores the {@link M_Form} instance that is to be displayed in 
	 * the view
	 * 
	 * @access private
	 * @var M_Form
	 */
	private $_form;
	
	/* -- SETTERS -- */
	
	/**
	 * Set the Contact Form
	 * 
	 * This method can be used to set the {@link M_Form} that is to be
	 * used for interaction in the view. Typically, this form will be
	 * an instance of {@link ContactForm}.
	 *
	 * @access public
	 * @param ContactForm $form
	 * @return ContactFormView $view
	 * 		Returns itself, for a fluent programming interface
	 */
	public function setContactForm(ContactForm $form) {
		$this->_form = $form;
		return $this;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure the form has been provided
		if(! $this->_form) {
			// Throw an exception, because if not provided we cannot render
			throw new M_ViewException(sprintf(
				'Cannot render the Contact Form View; Please provide ContactForm to %s::%s()',
				__CLASS__,
				'setContactForm'
			));
		}
		
		// Assign presentation variables
		$this->assign('form', $this->_form);
	}
	
	/**
	 * Get resource
	 *
	 * @see M_ViewHtml::getResource()
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('contact', 'ContactForm.tpl');
	}
}