<?php
/**
 * Abstract Contact Form
 * 
 * Used as a mother-class to all forms in the contact module that send out
 * an email message.
 * 
 * @abstract
 * @package App
 * @subpackage Contact
 */
abstract class ContactAbstractForm extends M_Form {
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get email
	 * 
	 * Will construct an {@link M_Mail} object, given a Form ID.
	 * 
	 * @access protected
	 * @param string $formId
	 * 		Form ID: Used to fetch settings from the Contact config file (recipients, 
	 * 		recipients in BCC, CC, subject)
	 * @param array $subjectPlaceholderValues
	 * 		Will be used to replace placeholders with, in the subject line
	 * 		that has been defined in the configuration for the requested Form ID
	 * @return M_Mail $mail
	 * 		The email message
	 */
	protected function _getMail($formId, array $subjectPlaceholderValues = array()) {
		// Get the configuration for the requested form
		$config = $this->_getContactModuleConfigByFormId($formId);
		
		// Construct a new email message:
		$mail = new M_Mail;
		
		// Set context, for logging
		// (we convert camel-casing to words that are separated by an underscore)
		$mail->setContext('contact_' . strtolower(
			implode(
				'_', 
				M_Helper::explodeByCamelCasing((string) $formId)
			)
		));
		
		// Set the email recipient(s)
		if(isset($config->recipients) && ! empty($config->recipients)) {
			$mail->addRecipient($config->recipients);
		}
		
		// If recipients have been defined in CC
		if(isset($config->recipientsCc) && ! empty($config->recipientsCc)) {
			// Then add the recipient(s) to the email
			$mail->addCc($config->recipientsCc);
		}
		
		// If recipients have been defined in BCC
		if(isset($config->recipientsBcc) && ! empty($config->recipientsBcc)) {
			// Then add the recipient(s) to the email
			$mail->addBcc($config->recipientsBcc);
		}
		
		// Set the subject of the email
		if(isset($config->subject) && ! empty($config->subject)) {
			$mail->setSubject(t(
				$config->subject,
				$subjectPlaceholderValues
			));
		}
		
		// Set the mailer that is to be used
		// (also mandated by the branded shop's configuration object)
		$mail->setMailer($this->_getMailer());
		
		// Return the mail:
		return $mail;
	}
	
	/**
	 * Get mailer
	 * 
	 * This method provides with the {@link MI_Mailer} instance that is to be
	 * used to send out the email message.
	 * 
	 * @access protected
	 * @return MI_Mailer
	 */
	protected function _getMailer() {
		$config = M_Application::getConfigOfModule('contact');
		
		//if a smtp mailer has been defined in config, use this one
		if ($config->mailer->smtp) {
			return new M_MailerSmtp(new M_Uri($config->mailer->smtp));
		}
		
		//otherwise use default mailer
		else return new M_Mailer();
	}
	
	/**
	 * Get Config of Contact Module
	 * 
	 * Will provide with the configuration object of the contact module
	 * 
	 * @access protected
	 * @return MI_Config
	 */
	protected function _getContactModuleConfig() {
		return M_Application::getConfigOfModule('contact');
	}
	
	/**
	 * Get Config of Form ID
	 * 
	 * Will provide with the configuration object for a given Form ID
	 * 
	 * @access protected
	 * @return MI_Config
	 */
	protected function _getContactModuleConfigByFormId($formId) {
		// Cast the Form ID to a string
		$formId = (string) $formId;
		
		// Get the contact module's config:
		$config = $this->_getContactModuleConfig();
		
		// Make sure that the requested Form ID exists:
		if(! isset($config->$formId)) {
			// If not, we throw an exception:
			throw new M_Exception(sprintf(
				'Contact module: Cannot find the configuration of Form %s',
				$formId
			));
		}
		
		// Make sure that the config of the requested Form ID is a container of
		// configuration variables:
		if(! M_Helper::isInstanceOf($config->$formId, 'M_Config')) {
			// If not, we throw an exception:
			throw new M_Exception(sprintf(
				'Contact module: Cannot find the configuration of Form %s',
				$formId
			));
		}
		
		// Make sure that the required variables have been set in the configuration.
		// If not, we default them:
		// - Recipient(s)
		if(! isset($config->$formId->recipients)) {
			// Default the recipient to the Person/Entity of the application
			$config->$formId->recipients = M_ApplicationIdentity
				::getInstance()
				->getContact()
				->getName();
		}
		
		// - Subject
		if(! isset($config->$formId->subject)) {
			// Default the subject to "[PLASTORIA] Contact"
			$config->$formId->subject = '['. M_ApplicationIdentity::getInstance()->getContact()->getName() .'] Contact';
		}
		
		// - Recipients in CC
		if(! isset($config->$formId->recipientsCc)) {
			$config->$formId->recipientsCc = '';
		}
		
		// - Recipients in BCC
		if(! isset($config->$formId->recipientsBcc)) {
			$config->$formId->recipientsBcc = '';
		}
		
		// Return the configuration
		return $config->$formId;
	}
}