<?php
/**
 * ContactForm
 * 
 * @package App
 * @subpackage Contact
 */

// Load superclass
M_Loader::loadForm('ContactAbstractForm', 'contact');

/**
 * ContactForm
 * 
 * @package App
 * @subpackage Contact
 */
class ContactForm extends ContactAbstractForm {
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return ContactGenericForm
	 */
	public function __construct() {
		// Construct the form:
		parent::__construct('contact-form');
		
		// Attach the fields
		$this->_addFields();
		
		$this->setVariable('recaptchaResponse', '');
	}
	
	/* -- M_FORM -- */

	/**
	 * Validate
	 *
	 * @return bool
	 */
	public function validate()
	{
		if ( ! parent::validate()) {
			return false;
		}

		// Build POST request:
		$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
		$recaptcha_secret = '6Ld8UtoUAAAAACzqrA2btkkWMUlNpXIJWX_5USI2';
		$recaptcha_response = $this->getVariable('recaptchaResponse');

		// Make and decode POST request:;
		$uri = new M_Uri($recaptcha_url);
		$session = new M_UriSession($uri);
		$session->setRequestMethodPost();
		$session->setPostData(
			array(
				'secret' => $recaptcha_secret,
				'response' => $recaptcha_response,
			)
		);

		$recaptcha = $session->getContents();
		$recaptcha = json_decode($recaptcha);

		// Take action based on the score returned:
		if (! isset($recaptcha->score) || $recaptcha->score < 0.5) {
			return false;
		}

		return true;
	}
	
	/**
	 * Form Actions
	 * 
	 * @see M_Form::actions()
	 * @access public
	 * @param array $values
	 * 		The values, collected by {@link M_Form::getValues()}
	 * @return bool $flag
	 * 		Returns TRUE if the form has been handled successfully, FALSE if not
	 */
	public function actions(array $values) {
		// Get the email to be sent:
		$mail = $this->_getMail('contact');
		$mail->setReplyTo(M_MailHelper::getRecipientStringFormat($values['name'], $values['email']));

		// Set the email message's body
		$mail->setBody(
			$this
				->_getMailView($values)
				->fetch()
		);
		
		// Send the mail. If the email has been sent successfully:
		if($mail->send()) {
			// If successful, we send the confirmation email to the visitor.
			// Get the email to be sent:
			$mail = $this->_getMail('contactConfirmation');
			
			// Set the recipient of the email:
			$mail->addRecipient(
				M_MailHelper::getRecipientStringFormat(
					$values['name'], $values['email']
				)
			);
			
			// Set the email message's body
			$mail->setBody(
				$this
					->_getConfirmationMailView($values)
					->fetch()
			);
			
			// Send the email, and return the result of that sending as
			// the result of the form:
			return $mail->send();
		}
		
		// Return failure, if still here:
		return FALSE;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Add fields
	 * 
	 * ... Will construct, and add, the fields to the form
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _addFields() {
		$this->addField($this->_getFieldInfo());
		$this->addField($this->_getFieldName());
		$this->addField($this->_getFieldPhoneNumber());
		$this->addField($this->_getFieldEmailAddress());
		$this->addField($this->_getFieldAddress());
		$this->addField($this->_getFieldComments());
	}
	
	/**
	 * Get field: "First Name"
	 * 
	 * @access protected
	 * @return M_FieldText $field
	 * 		The requested field, for use in the form
	 */
	protected function _getFieldName() {
		$field = new M_FieldText('name');
		$field->setTitle('* '.t('NAAM'));
		$field->setMandatory(TRUE);
		$field->setDefaultErrorMessage(t('Gelieve dit veld niet leeg te laten'));
		return $field;
	}
	
	/**
	 * Get field: "Phone number"
	 * 
	 * @access protected
	 * @return M_FieldText $field
	 * 		The requested field, for use in the form
	 */
	protected function _getFieldPhoneNumber() {
		$field = new M_FieldText('phone');
		$field->setTitle('* '.t('TEL/GSM'));
		$field->setMandatory(TRUE);
		$field->setDefaultErrorMessage(t('Gelieve dit veld niet leeg te laten'));
		return $field;
	}
	
	/**
	 * Get field: "Email address"
	 * 
	 * @access protected
	 * @return M_FieldText $field
	 * 		The requested field, for use in the form
	 */
	protected function _getFieldEmailAddress() {
		$field = new M_FieldText('email');
		$field->setTitle('* '.t('EMAIL'));
		$field->setMandatory(TRUE);
		$field->setDefaultErrorMessage(t('Gelieve dit veld niet leeg te laten'));
		$field->addValidator('M_ValidatorIsEmail', t('Gelieve een geldig en bestaand e-mailadres in te geven'));
		return $field;
	}
	
	/**
	 * Get field: "Comments/Remarks"
	 * 
	 * @access protected
	 * @return M_FieldTextarea $field
	 * 		The requested field, for use in the form
	 */
	protected function _getFieldComments() {
		$field = new M_FieldTextarea('comments');
		$field->setTitle(t('Andere/Opmerkingen'));
		$field->setHeight(50);
		return $field;
	}

	/**
	 * Get field: "Info"
	 *
	 * @access protected
	 * @return M_FieldCheckboxes $field
	 * 		The requested field, for use in the form
	 */
	protected function _getFieldInfo() {
		$field = new M_FieldCheckboxes('info');
		$field->setTitle(t('Ja, wij hadden graag meer informatie ontvangen aangaande:'));

		$options = array(
			'totaalprojecten' => t('Totaalprojecten'),
			'ontwerp' => t('Ontwerp'),
			'uitvoeringen' => t('Uitvoeringen'),
			'technieken' => t('Technieken'),
			'skimmerbad' => t('Skimmerbad'),
			'overloopzwembad' => t('Overloopzwembad'),
			//'zeydon' => t('Zeydon Epoxy Pools'),
			'tuinaanleg' => t('Tuinaanleg'),
			'poolhouse' => t('Poolhouse'),
		);
		$field->setOptions($options);
		
		return $field;
	}

	/**
	 * Get field: "Address"
	 *
	 * @access protected
	 * @return M_FieldAddress $field
	 * 		The requested field, for use in the form
	 */
	protected function _getFieldAddress() {
		$field = new M_FieldAddress('address');
		$field->getFieldCity()->setHint(t('Gemeente'));
		$field->getFieldPostalCode()->setHint(t('Postcode'));
		$field->getFieldStreet()->setHint(t('Straat'));
		$field->getFieldStreetNumber()->setHint(t('nr.'));
		$field->setTitle('* '.t('ADRES'));
		$field->getFieldStreet()->setMandatory(true);
		$field->getFieldStreetNumber()->setMandatory(true);
		$field->getFieldCity()->setMandatory(true);
		$field->getFieldPostalCode()->setMandatory(true);
		$field->setDefaultErrorMessageCity(t('Gelieve een geldige postcode en gemeente in te geven'));
		$field->setDefaultErrorMessageStreet(t('Gelieve een geldige straat en nummer in te geven'));
		return $field;
	}
	
	/**
	 * Get Mail view
	 * 
	 * Constructs the view that renders the contents of the contact mail. Used by
	 * {@link ContactGenericForm::actions()}, to compose the email message's body.
	 * 
	 * @access protected
	 * @param array $values
	 * 		The values, collected by {@link M_Form::getValues()}
	 * @return ContactMailView
	 */
	protected function _getMailView(array $values) {
		// Load the view:
		M_Loader::loadView('ContactMailView', 'contact');
		
		// Construct the view:
		$view = new ContactMailView;
		
		// Assign data of person/entity
		$view
			->setName($values['name'])
			->setPhone($values['phone'])
			->setEmail($values['email'])
			->setComments($values['comments'])
			->setInfo($values['info'])
			->setAddress($values['address']);
		
		// Return the view:
		return $view;
	}
	
	/**
	 * Get Confirmation Mail view
	 * 
	 * Constructs the view that renders the contents of the confirmation mail (sent
	 * to the client/prospect). Used by {@link ContactGenericForm::actions()}, to 
	 * compose the email message's body.
	 * 
	 * @access protected
	 * @param array $values
	 * 		The values, collected by {@link M_Form::getValues()}
	 * @return ContactMailView
	 */
	protected function _getConfirmationMailView(array $values) {
		// Load the view:
		M_Loader::loadView('ContactConfirmationMailView', 'contact');
		
		// Construct the view:
		$view = new ContactConfirmationMailView;

		// Get company name out of identity.xml
		$company = M_ApplicationIdentity::getInstance()->getContact()->getName();
		
		// Assign data of person/entity
		$view
			->setName($values['name'])
			->setPhone($values['phone'])
			->setEmail($values['email'])
			->setComments($values['comments'])
			->setInfo($values['info'])
			->setAddress($values['address']);
		
		// Return the view:
		return $view;
	}
}