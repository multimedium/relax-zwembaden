{cssfile file="application/modules/contact/resources/css/contact.css" context="contact"}

{* This page is shown when the contact form has been sent successfully. Typically,
   this template is used to show the "Thank you" message to the visitor. *}
<div id="contact-message" class="content-wrapper">
	<p>
		{t text="Bedankt voor uw contact-aanvraag: uw bericht werd succesvol verstuurd. We contacteren u zo snel mogelijk."}
	</p>
	<p>
		<br/><a href="{link href="contact"}" title="{t|att text="contact-pagina"}">{t text="terug naar de contact-pagina"}</a>
	</p>
</div>