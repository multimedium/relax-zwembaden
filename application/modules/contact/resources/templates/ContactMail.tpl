
{assign var="fontStyle" value="font-family: Arial, Verdana, Helvetica; color: #000000; font-size: 14px;"}
{assign var="pStyle" value=" line-height: 140%;"}
{assign var="titleStyle" value=" font-weight: bold;"}

{* Introduction text. Note that we place this text inside a <P> tag, and we apply
   the styles we have prepared before. *}
<p style="{$fontStyle} {$pStyle}">
	{t text="U werd gecontacteerd via het formulier op de website:"}
</p>

{* If name is available: *}
{if $name}
	<div style="{$fontStyle} {$titleStyle}">{t text="Naam"}</div>
	<p style="{$fontStyle} {$pStyle}">{$name}</p>
{/if}

{* If a phone number is available: *}
{if $phone}
	<div style="{$fontStyle} {$titleStyle}">{t text="Telefoonnummer"}</div>
	<p style="{$fontStyle} {$pStyle}">{$phone}</p>
{/if}

{* If an email address is available: *}
{if $email}
	<div style="{$fontStyle} {$titleStyle}">{t text="E-mailadres"}</div>
	<p style="{$fontStyle} {$pStyle}">{$email}</p>
{/if}

{* If a subject is available: *}
{if $subject}
	<div style="{$fontStyle} {$titleStyle}">{t text="Onderwerp"}</div>
	<p style="{$fontStyle} {$pStyle}">{$subject}</p>
{/if}

{* If comments are available: *}
{if $comments}
	<div style="{$fontStyle} {$titleStyle}">{t text="Bericht"}</div>
	<p style="{$fontStyle} {$pStyle}">{$comments|nl2br}</p>
{/if}

{* If address is available *}
{if $address}
	<div style="{$fontStyle} {$titleStyle}">{t text="Adres"}</div>
	<p style="{$fontStyle} {$pStyle}">{$address->getStreetAddress()}<br/>{$address->getPostalCode()} {$address->getCity()}</p>
{/if}

{* If info is available *}
{if $info && $info|@count > 0}
	<div style="{$fontStyle} {$titleStyle}">{t text="Klant wenst info over"}</div>
	<p style="{$fontStyle} {$pStyle}">
	<ul>
		{foreach from=$info item=i}
		<li>{$i}</li>
		{/foreach}
	</ul>
	</p>
{/if}