
{* First of all, we prepare some (CSS) styles. These styles will be applied 
   throughout this template: *}
   
{assign var="fontStyle" value="font-family: Arial, Verdana, Helvetica; color: #000000; font-size: 14px;"}
{assign var="pStyle" value=" line-height: 140%;"}
{assign var="titleStyle" value=" font-weight: bold;"}

<p style="{$fontStyle} {$pStyle}">
	{t text="Hartelijk bedankt om ons te contacteren. We hebben uw bericht goed ontvangen, en contacteren u zo snel mogelijk."}
</p>
