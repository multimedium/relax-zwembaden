{cssfile file="application/modules/contact/resources/css/contact.css" context="contact"}
{jsfile file="application/modules/contact/resources/javascript/jquery.input-hint.js"}
{jsfile file="application/modules/contact/resources/javascript/contact.js"}

{literal}
	<script src='https://www.google.com/recaptcha/api.js?render=6Ld8UtoUAAAAAFHD6IYi6OWGk8n1rEmrQFJuwNeW'></script>
	<script>
		grecaptcha.ready(function() {
			grecaptcha.execute('6Ld8UtoUAAAAAFHD6IYi6OWGk8n1rEmrQFJuwNeW', {action: 'contact'})
					.then(function(token) {
						var recaptchaResponse = $('input[name="recaptchaResponse"]');
						recaptchaResponse.val(token);
					});
		});
	</script>
{/literal}
<div id="contact" class="content-wrapper">
	<div id="contact-wrapper">
		<div id="contact-column-form" class="contact-column">

			{* Form *}
			{if $form}
			<form id="contact-form"{foreach from=$form->getProperties() key="name" item="value"} {$name}="{$value}"{/foreach}>
				{foreach from=$form->getVariables() key="name" item="value"}
					{if $value|is_array}
						{foreach from=$value key="key" item="arrayValue"}
							<input type="hidden" name="{$name}[{$key}]" value="{$arrayValue}" />
						{/foreach}
					{else}
						<input type="hidden" name="{$name}" value="{$value}" />
					{/if}
				{/foreach}

				{* Render fields which are not part of a field-group *}
				{foreach from=$form->getFields(false) key="name" item="field"}
					{$field->setModuleOwner('contact')}
					{$field->getView()}
				{/foreach}

				<p>{t text="* verplichte velden"}</p>
				<div class="field-row-buttons">
					<input type="submit" value="{t|att text="VERZENDEN"}" />
					<div class="clear"><!-- --></div>
				</div>
			</form>
			{/if}
		</div>

		{* Contact info column *}
		<div id="contact-info-column" class="contact-column">
			<div class="contact-hours">
				<p>{t text="U kan ons enkel nog bezoeken na telefonische afspraak en bereiken via mail"}
				</p>
			</div>

			<div class="contact-info">
				<img src="{link href="logo.gif" type="images" module="contact"}" alt="{t|att text="relaxzwembaden"} "/>

				 <p>
					Relaxzwembaden BV<br/>
					{identity get="@address/street"}<br/>
					{identity get="@address/postalCode"} {identity get="@address/city"}<br/>
					{identity get="@phone/tel1"}<br/>
					{identity get="@phone/tel2"}<br/>
					<a title="mail {identity|att get="@name"}" href="mailto:{identity get="@email"}">{identity get="@email"}</a><br/>
					{t text="BTW"} {identity get="@vat"}
				</p>
			</div>
		</div>

		{* Contact route column *}
		<div id="contact-route-column" class="contact-column">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3841.7438338856277!2d4.794112434091985!3d51.14898823940538!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c1516c0f4036c7%3A0x9665898c7e44c763!2sHerentalsesteenweg%2081%2C%202270%20Herenthout!5e0!3m2!1snl!2sbe!4v1733840229539!5m2!1snl!2sbe" width="226" height="284" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe><br />
			<small><a href="https://www.google.be/maps/place/Herentalse+Steenweg+81,+2270+Herenthout/@51.2430928,4.9830476,17z/data=!3m1!4b1!4m2!3m1!1s0x47c6b3553d526bbb:0x4d7017a7a331fd5d">{t text="Grotere kaart weergeven"}</a></small>
		</div>

		<div class="clear"></div>
	</div>

{* Finish padding *}
</div>