{cssfile file="application/modules/contact/resources/css/contact.css" context="contact"}
{link href="contact" assignto="linkContact"}

{* This page is shown when the contact form has been sent successfully. Typically,
   this template is used to show the "Thank you" message to the visitor. *}
<div id="contact-message" class="content-wrapper">
	<p>
		{t text='Oeps... er ging iets mis bij het versturen van uw bericht. U kan opnieuw proberen of ons contacteren via e-mail of telefoon: bekijk onze <a href="@linkContact">contactpagina</a> voor onze gegevens.' linkContact=$linkContact}
	</p>
	<p>
		<br/><a href="{link href="contact"}" title="{t|att text="contact-pagina"}">{t text="terug naar de contact-pagina"}</a>
	</p>
</div>