<?php
/**
 * AdminWebmasterController
 * 
 * @package App
 * @subpackage Webmaster
 */
class AdminWebmasterController extends M_Controller {

	/**
	 * Webmaster dashboard
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->_getLocaleController()->index();
	}

	/** -- Locale actions -- **/
	
	/**
	 * Import .po file
	 *
	 * This method can be used to import the translated strings from a Portable
	 * Object into the application.
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be populated with
	 * 		the strings in the portable object file. If not provided, this method
	 * 		will use the currently selected locale
	 * @return void
	 */
	public function localeIn() {
		$this->_getLocaleController()->localeIn();
	}

	/**
	 * Export po file
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be exported to a
	 * 		portable object file. If not provided, this method will use the
	 * 		currently selected locale
	 * @return void
	 */
	public function localeOut($locale = NULL) {
		$this->_getLocaleController()->localeOut($locale);
	}

	/**
	 * Export po file (Admin strings only)
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be exported to a
	 * 		portable object file. If not provided, this method will use the
	 * 		currently selected locale
	 * @return void
	 */
	public function localeOutAdminOnly($locale = NULL) {
		$this->_getLocaleController()->localeOutAdminOnly($locale);
	}

	/**
	 * Export po file (Admin strings excluded)
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be exported to a
	 * 		portable object file. If not provided, this method will use the
	 * 		currently selected locale
	 * @return void
	 */
	public function localeOutAdminExcluded($locale = NULL) {
		$this->_getLocaleController()->localeOutAdminExcluded($locale);
	}

	/**
	 * Export po file (Untranslated strings only)
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be exported to a
	 * 		portable object file. If not provided, this method will use the
	 * 		currently selected locale
	 * @return void
	 */
	public function localeOutUntranslated($locale = NULL) {
		$this->_getLocaleController()->localeOutUntranslated($locale);
	}

	/** -- Webmaster actions -- **/

	/**
	 * Show an overview of the administrators
	 *
	 * @access public
	 * @return void
	 */
	public function overviewAdministrator() {
		$this->_getAdministratorController()->overview();
	}

	/** -- PRIVATE METHODS **/

	/**
	 * Get the AdminLocaleController
	 *
	 * @return AdminWebmasterLocaleController
	 */
	private function _getLocaleController() {
		AdminLoader::loadController(
			'AdminWebmasterLocaleController',
			'webmaster'
		);
		return new AdminWebmasterLocaleController();
	}
}