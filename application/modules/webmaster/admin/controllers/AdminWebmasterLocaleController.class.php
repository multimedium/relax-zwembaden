<?php
class AdminWebmasterLocaleController {
	/**
	 * Webmaster dashboard
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		// Get dashboard view
		$view = $this->_getAdminWebmasterDashboardView();

		// Provide the view with the installed locales:
		$view->setLocales(M_LocaleMessageCatalog::getInstalled());

		// Display the view
		$view->display();
	}

	/**
	 * Export po file
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be exported to a
	 * 		portable object file. If not provided, this method will use the
	 * 		currently selected locale
	 * @return void
	 */
	public function localeOut($locale = NULL) {
		// If locale has not been provided, set to currently selected:
		if(! $locale) {
			$locale = M_Locale::getCategory(M_Locale::LANG);
		}

		// Now, we offer the po as a download:
		M_Header::sendDownloadFromString(
			// Send the PO source code:
			$this->_getPO($locale)->toString(),
			// Send the properties of the download file:
			new M_File($locale . '.po')
		);
	}

	/**
	 * Export po file (Admin strings only)
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be exported to a
	 * 		portable object file. If not provided, this method will use the
	 * 		currently selected locale
	 * @return void
	 */
	public function localeOutAdminOnly($locale = NULL) {
		// If locale has not been provided, set to currently selected:
		if(! $locale) {
			$locale = M_Locale::getCategory(M_Locale::LANG);
		}

		// Now, we offer the po as a download:
		M_Header::sendDownloadFromString(
			// Send the PO source code:
			$this->_getPO($locale, 'admin')->toString(),
			// Send the properties of the download file:
			new M_File($locale . '.po')
		);
	}

	/**
	 * Export po file (Admin strings excluded)
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be exported to a
	 * 		portable object file. If not provided, this method will use the
	 * 		currently selected locale
	 * @return void
	 */
	public function localeOutAdminExcluded($locale = NULL) {
		// If locale has not been provided, set to currently selected:
		if(! $locale) {
			$locale = M_Locale::getCategory(M_Locale::LANG);
		}

		// Now, we offer the po as a download:
		M_Header::sendDownloadFromString(
			// Send the PO source code:
			$this->_getPO($locale, 'website')->toString(),
			// Send the properties of the download file:
			new M_File($locale . '.po')
		);
	}

	/**
	 * Export po file (Untranslated strings only)
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be exported to a
	 * 		portable object file. If not provided, this method will use the
	 * 		currently selected locale
	 * @return void
	 */
	public function localeOutUntranslated($locale = NULL) {
		// If locale has not been provided, set to currently selected:
		if(! $locale) {
			$locale = M_Locale::getCategory(M_Locale::LANG);
		}

		// Now, we offer the po as a download:
		M_Header::sendDownloadFromString(
			// Send the PO source code:
			$this->_getPO($locale, 'untranslated')->toString(),
			// Send the properties of the download file:
			new M_File($locale . '.po')
		);
	}

	/**
	 * Import .po file
	 *
	 * This method can be used to import the translated strings from a Portable
	 * Object into the application.
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be populated with
	 * 		the strings in the portable object file. If not provided, this method
	 * 		will use the currently selected locale
	 * @return void
	 */
	public function localeIn() {
		// Get the form:
		$form = $this->_getAdminWebmasterLocaleImportForm();

		// Set Redirect URL:
		$form->setRedirectUrl(M_Form::OK, 'admin/route/webmaster');
		$form->setRedirectUrl(M_Form::KO, 'admin/route/webmaster');

		// Set Redirect URL:
		$form->setResultMessage(M_Form::OK, t('The Portable Object file has been imported successfully!'));
		$form->setResultMessage(M_Form::KO, t('The Portable Object file could not have been imported! Please try again later'));

		// If the form has not been sent, or if errors occurred
		if(! $form->run()) {
			// In that case, we need to show the form in the view:
			$view = $this->_getAdminWebmasterLocaleImportFormView();

			// Assign the form to the view
			$view->setLocaleImportForm($form);

			// Display the view:
			$view->display();
		}
	}

	/* -- PROTECTED/PRIVATE -- */

	/**
	 * Get AdminWebmasterDashboardView
	 *
	 * @access protected
	 * @return AdminWebmasterDashboardView
	 */
	protected function _getAdminWebmasterDashboardView() {
		// Load the view:
		AdminLoader::loadView('AdminWebmasterDashboardView', 'webmaster');

		// Return the view:
		return new AdminWebmasterDashboardView;
	}

	/**
	 * Get AdminWebmasterLocaleImportFormView
	 *
	 * @access protected
	 * @return AdminWebmasterLocaleImportFormView
	 */
	protected function _getAdminWebmasterLocaleImportFormView() {
		// Load the view:
		AdminLoader::loadView('AdminWebmasterLocaleImportFormView', 'webmaster');

		// Return the view:
		return new AdminWebmasterLocaleImportFormView;
	}

	/**
	 * Get AdminWebmasterLocaleImportForm
	 *
	 * @access protected
	 * @return AdminWebmasterLocaleImportForm
	 */
	protected function _getAdminWebmasterLocaleImportForm() {
		// Load the form:
		AdminLoader::loadForm('AdminWebmasterLocaleImportForm', 'webmaster');

		// Return the form:
		return new AdminWebmasterLocaleImportForm;
	}

	/**
	 * Get PO
	 *
	 * @access public
	 * @param string $locale
	 * 		The locale of which the message catalog should be exported to a
	 * 		portable object file.
	 * @param string $selection
	 * 		With this selection, you can specify which strings should be exported
	 * 		to the PO file. Possible values: 'all' (default), 'admin', 'website',
	 * 		or 'untranslated'
	 * @return M_LocalePortableObject $po
	 * 		The portable object
	 */
	protected function _getPO($locale, $selection = 'all') {
		// Get the message catalog, that contains all translated strings
		$mc = M_LocaleMessageCatalog::getInstance($locale);

		// Now, we construct a Portable Object (which we will be offering
		// as a download)
		$po = new M_LocalePortableObject();

		// For each of the translated strings in the message catalog:
		foreach($mc->getIterator() as $string) {
			// Whether or not we add the string to the portable object, depends
			// on the selection that has been made:
			switch($selection) {
				// If only admin strings have been selected:
				case 'admin':
					// We make sure that the string comes from the admin:
					$b = (strpos($string->getComment(M_LocaleMessage::REFERENCE), '/admin') !== FALSE);
					break;

				// If admin strings have not been selected:
				case 'website':
					// We make sure that the string does not come from the admin:
					$b = (strpos($string->getComment(M_LocaleMessage::REFERENCE), '/admin') === FALSE);
					break;

				// If untranslated strings have been selected:
				case 'untranslated':
					// We make sure that the string has not been translated yet
					$b = (! $string->getTranslatedString());
					break;

				// If all have been selected
				// case 'website':
				default:
					// We simply add the string, without any check
					$b = TRUE;
					break;
			}

			// If the previous check returns TRUE
			if($b) {
				// Then, we add the string to the portable object:
				$po->setTranslatedString($string);
			}
		}

		// Return the PO
		return $po;
	}
}