<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="breadcrumb">
		<ul>
			<li><a href="{link href="admin/index/"}">Multimanage</a></li>
			<li><a href="{link href="admin/route/webmaster"}">{t text="Webmaster Tools"}</a></li>
			<li><span>{t text="Import language"}</span></li>
		</ul>
	</div>
	
	{$form->setModuleOwner('admin')}
	{$form->getView()}
</div>