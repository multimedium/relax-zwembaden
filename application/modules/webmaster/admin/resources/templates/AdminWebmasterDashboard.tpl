<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	
	<div class="breadcrumb">
		<ul>
			<li><a href="{link href="admin/index/"}">Multimanage</a></li>
			<li><span>{t text="Webmaster Tools"}</span></li>
		</ul>
	</div>
	
	<div id="webmaster-dashboard">
		<ul>
			<li id="webmaster-locale-in">
				<a href="{link href="admin/route/webmaster/localeIn"}">
					{t text="Import Portable Object"}
				</a>
			</li>
			<li id="webmaster-locale-out">
				<div>
					<a href="#" class="popmenu arrow-down" id="idExportPoButton">
						{t text="Export Portable Object"}
					</a>
					<div id="idExportPoButtonPopmenu" class="hidden">
						<ul title="{t|att text="All"}">
							{foreach from=$locales item="locale"}
								<li><a href="{link href="admin/route/webmaster/localeOut/`$locale`"}">{localedisplay name=$locale type="language"}</a></li>
							{/foreach}
						</ul>
						<ul title="{t|att text="Multimanage texts only"}">
							{foreach from=$locales item="locale"}
								<li><a href="{link href="admin/route/webmaster/localeOutAdminOnly/`$locale`"}">{localedisplay name=$locale type="language"}</a></li>
							{/foreach}
						</ul>
						<ul title="{t|att text="Website texts only"}">
							{foreach from=$locales item="locale"}
								<li><a href="{link href="admin/route/webmaster/localeOutAdminExcluded/`$locale`"}">{localedisplay name=$locale type="language"}</a></li>
							{/foreach}
						</ul>
						<ul title="{t|att text="Untranslated texts only"}">
							{foreach from=$locales item="locale"}
								<li><a href="{link href="admin/route/webmaster/localeOutUntranslated/`$locale`"}">{localedisplay name=$locale type="language"}</a></li>
							{/foreach}
						</ul>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>