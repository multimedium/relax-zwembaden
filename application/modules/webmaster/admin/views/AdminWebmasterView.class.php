<?php
/**
 * AdminWebmasterView
 * 
 * @package App
 * @subpackage Webmaster
 */
abstract class AdminWebmasterView extends AdminPageView {
	/**
	 * Constructor
	 * 
	 * @access public
	 * @return AdminWebmasterView
	 */
	public function __construct() {
		$css = M_ViewCss::getInstance();
		$css->addFile(
			new M_File(
				AdminLoader::getAdminResourcesPath('webmaster') . '/css/webmaster.css'
			)
		);
	}
}