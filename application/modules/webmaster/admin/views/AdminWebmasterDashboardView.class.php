<?php
/**
 * AdminWebmasterDashboardView
 * 
 * @package App
 * @subpackage Webmaster
 */

// Load superclass
AdminLoader::loadView('AdminWebmasterView', 'webmaster');

/**
 * AdminWebmasterDashboardView
 * 
 * @package App
 * @subpackage Webmaster
 */
class AdminWebmasterDashboardView extends AdminWebmasterView {
	
	/* -- SETTERS -- */
	
	/**
	 * Set locales
	 * 
	 * Provide with the locales for which tools should be made available in the 
	 * view
	 * 
	 * @access public
	 * @param array $locales
	 * @return void
	 */
	public function setLocales(array $locales) {
		$this->assign('locales', $locales);
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(AdminLoader::getAdminResourcesPath('webmaster') . '/templates/AdminWebmasterDashboard.tpl');
	}
}