<?php
/**
 * HomeView
 *
 * @package App
 * @subpackage Home
 */
class HomeView extends PageView {

	/**
	 * Stores the album
	 *
	 * @var Media
	 */
	private $_media;

	/**
	 * Set the media which is shown on the homepage
	 *
	 * @param M_ArrayIterator $texts
	 * @return HomeView
	 */
	public function setMedia(Media $media){
		$this->_media = $media;
		return $this;
	}


	/**
	 * Preprocessing
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		// Assign presentation variables
		$this->assign('media', $this->_media);
	}

	/* -- PRIVATE/PROTECTED -- */

	/**
	 * Get the resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return M_ViewHtmlResource::constructWithModuleId('home', 'Home.tpl');
	}
}