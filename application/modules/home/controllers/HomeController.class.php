<?php
/**
 * HomeController
 *
 * @package App
 * @subpackage Home
 */
class HomeController extends PageController {

	/* -- PUBLIC -- */

	/**
	 * Homepage
	 *
	 * @access public
	 * @return void
	 */
	public function index() {

		// Construct the view
		$view = M_loader::getView('HomeView', 'home');
		
		/* @var $view HomeView */
		$album = $this->_getMediaAlbumMapper()
					->addFilterRealm('homepage')
					->getOne();
		/* @var $album MediaAlbum */

		$media = $this->_getMediaMapper()
					->addFilterMediaAlbum($album)
					->addFilterPublished()
					->getRandom()->first();

		// display the view
		$view->setMedia($media)
				->setPageTitle(t('Zwembaden, binnenzwembad of buitenzwembad | Waterplezier met Relaxzwembaden'))
				->setPageDescription('')
				->display();
	}
}