<?php
/**
 * AdminFormDataObject
 * 
 * AdminFormDataObject, a subclass of {@link M_FormDataObject}, is used to
 * create/edit instances of {@link M_DataObject} in the Admin Module.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminFormDataObject extends M_FormDataObject {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Module
	 * 
	 * This property stores the Module of the object that is being edited
	 * or created.
	 * 
	 * @access protected
	 * @var AdminModule
	 */
	protected $_module;
	
	/**
	 * Data Object ID
	 * 
	 * This property stores the Data Object ID of the object that is being edited
	 * or created.
	 * 
	 * @access protected
	 * @var string
	 */
	protected $_dataObjectId;
	
	/**
	 * Admin List Definition
	 * 
	 * This property stores the Admin List Definition of the Data Object that is
	 * being edited or created.
	 * 
	 * @access protected
	 * @var AdminListDefinition
	 */
	protected $_listDefinition;
	
	/* -- PUBLIC -- */
	
	/**
	 * Form actions
	 *
	 * @see M_Form::actions()
	 * @access public
	 * @param array $values
	 * 		The values that have been collected by the method
	 * 		{@link M_Form::getValues()}.
	 * @return unknown
	 */
	public function actions(array $values) {
		// Is sorted data object?
		$isSorted = $this->_isSortedDataObject();
		
		// Is re-sorting required?
		$isResortingRequired = $this->_isResortingRequired();
		
		// Re-do the sorting order of previous filter values:
		if($isSorted && $isResortingRequired) {
			$this->_setOtherOrderIndexNumbers('object');
		}
		
		// If this is a new data object, and data objects are being sorted
		if($this->_dataObject->isNew() && $isSorted) {
			// We set the new order index number before saving the changes to 
			// the database. If we fail in doing so, we return a failure as the
			// result of the form:
			if(! $this->_setNewOrderIndexNumber()) {
				return FALSE;
			}
		}
		
		// If the changes to the object have been changed successfully:
		if(parent::actions($this->_getValuesWithNullValues($values))) {
			// Set Order Index Numbers for all data objects now. May be necessary
			// when:
			// - If a data object is given another value (eg. Category ID), the
			//   order index numner might have to be updated
			// - If a data object was not being sorted before, we might need to set
			//   all order index numbers for the first time 
			if($isSorted && $isResortingRequired) {
				$this->_setOtherOrderIndexNumbers('form');
			}
			
			// Return success
			return TRUE;
		}
		// If the changes to the object have not been made:
		else {
			// Return FAILURE!
			return FALSE;
		}
	}
	
	/**
	 * Get view
	 * 
	 * @see M_Form::getView()
	 * @access public
	 * @return AdminFormDataObjectView
	 */
	public function getView() {
		// Load the view:
		M_Loader::loadView('AdminFormDataObjectView', 'admin');
		
		// Construct the view:
		$view = new AdminFormDataObjectView($this);
		$view->setId('form-' . $this->_getModule()->getId() . '-' . $this->_getDataObjectId());
		
		// return the view
		return $view;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get values, with NULL values
	 * 
	 * Strings that represent the NULL value, by providing "null" or "NULL", will be converted
	 * to the actual NULL value by this method. This method is used internally in the form's actions,
	 * in order to correctly store the values introduced for the data object.
	 * 
	 * @access protected
	 * @param array $values
	 * @return array
	 */
   protected function _getValuesWithNullValues(array $values) {
		// For each of the provided values:
		foreach($values as $key => $value) {
			// If the current value is an array:
			if(is_array($value)) {
				// Run through the values recursively:
				$values[$key] = $this->_getValuesWithNullValues($value);
			}
			// If a string, and representing the NULL value
			elseif(is_string($value) && strtolower($value) == 'null') {
				// Set to NULL:
				$values[$key] = NULL;
			}
		}
		
		// Return values with NULL
		return $values;
   }
   
	/**
	 * Set order index number, for new data object
	 * 
	 * @access protected
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	protected function _setNewOrderIndexNumber() {
		// Ask the data object for its mapper:
		$mapper = $this->_dataObject->getMapper();
		$mapper->resetFilters();
		
		// Get the filters we should apply, in order to sort instances. We
		// use the Form's values to apply filter values. For each of the filters:
		$filters = $this->_getFiltersForSorting('form');
		
		// If the filters could not have been prepared, we return a failure:
		if($filters === FALSE) {
			return FALSE;
		}
		
		// For each of the filters:
		foreach($filters as $filter) {
			// Add the filter to the mapper:
			$mapper->addFilter($filter->getDbQueryFilter());
		}
		
		// Now that the mapper carries all necessary filters, we ask the mapper
		// for a new order index number:
		$orderFieldName = $this->_getAdminListDefinition()->getSortDragFieldName();
		$newOrder = $mapper->getMaximum($orderFieldName);
		
		// We set the new order index number in the data object:
		$this->_dataObject->$orderFieldName = ($newOrder === NULL) 
			? 1 
			: $newOrder + 1;
		
		// We return SUCCESS
		return TRUE;
	}
	
	/**
	 * Set order index numbers, for data objects
	 * 
	 * @access protected
	 * @param string $extractFrom
	 * 		See {@link AdminFormDataObject::_getFiltersForSorting()}
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	protected function _setOtherOrderIndexNumbers($extractFrom) {
		// Ask the data object for the corresponding mapper:
		$mapper = $this->_dataObject->getMapper();
		$mapper->resetFilters();
		
		// If we are setting order numbers for the object's current range, we
		// exclude the object itself from the range:
		if($extractFrom == 'object') {
			$mapper->addFilter(new M_DbQueryFilterWhere('id', $this->getDataObject()->getId(), M_DbQueryFilterWhere::NEQ));
		}
		
		// Get the filters we should apply, in order to sort instances. We
		// use the Form's values to apply filter values. For each of the filters:
		$filters = $this->_getFiltersForSorting($extractFrom);
		
		// If the filters could not have been prepared, we return a failure:
		if($filters === FALSE) {
			return FALSE;
		}
		
		// For each of the filters:
		foreach($filters as $filter) {
			// Add the filter to the mapper:
			$mapper->addFilter($filter->getDbQueryFilter());
		}
		
		// Get the field where the order is stored:
		$orderFieldName = $this->_getAdminListDefinition()->getSortDragFieldName();
		
		// Order by the current order:
		$mapper->addFilter(new M_DbQueryFilterOrder($orderFieldName, 'ASC'));
		
		// Now that the mapper carries all necessary filters, we ask the mapper
		// for the data objects that are affected by sorting. For each of the
		// data objects:
		$j = 0;
		foreach($mapper->getAll() as $object) {
			// Set the order for the current object:
			$object->$orderFieldName = ++ $j;
			
			// Save the object's new order:
			$object->save();
		}
		
		// Return success:
		return TRUE;
	}
	
	/**
	 * Get required filters, for sorting
	 * 
	 * Will provide with the collection of filters that is required for sorting. 
	 * Each of the filters will already carry a value (extracted either from the
	 * data object, or from the Form's values)
	 * 
	 * NOTE:
	 * This method will return FALSE, if the filters could not have been
	 * collected correctly!
	 * 
	 * @access protected
	 * @param string $extractFrom
	 * 		Set to 'object' if you which to extract the filters from the object,
	 * 		or 'form' if you want to use the Form values instead
	 * @return array $iterator
	 * 		A collection of {@link AdminListFilterDefinition} objects
	 */
	protected function _getFiltersForSorting($extractFrom) {
		// Prepare an empty collection (output)
		$out = array();
		
		// Get the Admin List Definition:
		$definition = $this->_getAdminListDefinition();
		
		// Check if the definition exists:
		if(! $definition->exists()) {
			// If not, we return FALSE
			return FALSE;
		}
		
		// Where is the order index number stored?
		$orderFieldName = $definition->getSortDragFieldName();
		
		// If not stored in any field:
		if(! $orderFieldName) {
			// We return FALSE
			return FALSE;
		}
		
		// Get the form values:
		$values = $this->getValues();
		
		// We ask the list definition for the filters that are required
		// for sorting. For each of the required filters:
		foreach($definition->getRequiredFiltersForSorting() as $filter) {
			// Get the field to which the filter should be applied
			$field = $filter->getField();
			
			// Now, we should apply a value in the filter. The value we
			// apply, depends on where we are extracting from (object/form).
			// If we are extracting the value from the data object:
			if($extractFrom == 'object') {
				// Get the value from the data object:
				$value = $this->getDataObject()->$field;
				
				// If no value could have been found, we return FALSE:
				if($value === NULL) {
					return FALSE;
				}
			}
			// If we are extracting the value from the form:
			else {
				// Check if a value has been provided:
				if(! isset($values[$field])) {
					// If not, we check if a variable has been set
					$value = $this->getVariable($field);

					// If not, we cannot render a new order index number. We
					// return a failure as the result of this method:
					if($value === NULL) {
						return FALSE;
					}
				}
				else {
					// Set the value:
					$value = $values[$field];
				}
			}
			
			// Set the value for the filter:
			$filter->setValue($value);
			
			// Add the filter to the collection:
			$out[] = $filter;
		}
		
		// Return the collection of filters:
		return $out;
	}
	
	/**
	 * Is sorting enabled?
	 * 
	 * Will tell whether or not sorting (draggable) has been enabled for the
	 * data object that is being edited/created.
	 * 
	 * @access protected
	 * @return bool $flag
	 * 		Returns TRUE if sorting is enabled, FALSE if not
	 */
	protected function _isSortedDataObject() {
		// Get the Admin List Definition:
		$definition = $this->_getAdminListDefinition();
		
		// Check if the definition exists:
		if($definition->exists()) {
			// Where is the order index number stored?
			$orderFieldName = $definition->getSortDragFieldName();
			
			// If stored in any (valid) field:
			if($orderFieldName && $this->getDataObject()->getMapper()->getField($orderFieldName)) {
				// Return TRUE
				return TRUE;
			}
		}
		
		// Return FALSE, if still here:
		return FALSE;
	}
	
	/**
	 * Is re-sorting required?
	 * 
	 * Will tell whether or not the objects need to be re-ordered. This is only
	 * the case if:
	 * 
	 * - values of required filters have been changed (For more info, read the
	 *   docs on {@link AdminFormDataObject::_getFiltersForSorting()})
	 * 
	 * - There are no required filters
	 * 
	 * @access protected
	 * @return bool $flag
	 * 		Returns TRUE if re-sorting is required, FALSE if not
	 */
	protected function _isResortingRequired() {
		// Get the required filters:
		$definition = $this->_getAdminListDefinition();
		
		// If the definition does not exist:
		if(! $definition->exists()) {
			// Then, re-sorting is not required:
			return FALSE;
		}
		
		// If no filters are required:
		if(count($definition->getRequiredFiltersForSorting()) == 0) {
			// Then no re-sorting is required:
			return FALSE;
		}
		
		// Compare the filter values from the object, against the new values
		// from the form. If we find difference(s), we need to do re-sorting:
		$oFilters = $this->_getFiltersForSorting('object');
		$fFilters = $this->_getFiltersForSorting('form');
		
		// If no filters available:
		if(! $oFilters || ! $fFilters ) {
			return FALSE;
		}

		// For each of the filters:
		foreach($oFilters as $i => $filter) {
			// Compare with new value:
			if($filter->getValue() != $fFilters[$i]->getValue()) {
				// If no match, we need to do re-sorting:
				return TRUE;
			}
		}
		
		// If still here, there were no differences! No re-sorting is required
		return FALSE;
	}
	
	/**
	 * Get Module
	 * 
	 * Will provide with the Module of the Data Object that is being edited or
	 * created.
	 * 
	 * @access protected
	 * @return M_ApplicationModule
	 */
	protected function _getModule() {
		// if not requested before:
		if(! $this->_module) {
			// Get the Module from the Data Object Mapper:
			$this->_module = new AdminModule($this->_dataObject->getMapper()->getModule()->getId());
		}
		
		// Return the module:
		return $this->_module;
	}
	
	/**
	 * Get Data Object ID
	 * 
	 * Will provide with the ID of the Data Object that is being edited or created
	 * 
	 * @access protected
	 * @return string
	 */
	protected function _getDataObjectId() {
		// If not requested before:
		if(! $this->_dataObjectId) {
			// Ask the module for the Data Object ID:
			$this->_dataObjectId = $this->_getModule()->getDataObjectIdOfObject($this->_dataObject);
		}
		
		// Return the Data Object ID
		return $this->_dataObjectId;
	}
	
	/**
	 * Get Admin List Definition
	 * 
	 * Will provide with the Admin List Definition of the data object that is being
	 * edited or created
	 * 
	 * @access protected
	 * @return AdminListDefinition
	 */
	protected function _getAdminListDefinition() {
		// If not requested before:
		if(! $this->_listDefinition) {
			// Mount the definition:
			$this->_listDefinition = new AdminListDefinition($this->_getModule(), $this->_getDataObjectId());
		}
		
		// Return the List Definition
		return $this->_listDefinition;
	}
}