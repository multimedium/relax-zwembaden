<?php
/**
 * AdminFormChangePassword
 * 
 * @package App
 * @subpackage Admin
 */
class AdminFormChangePassword extends M_FormDataObject {
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param Administrator $administrator
	 * 		The administrator of which the data should be edited by the form
	 * @return void
	 */
	public function __construct(Administrator $administrator) {
		// Construct the form:
		parent::__construct('admin-pwd');
		
		// Set the administrator:
		$this->setAdministrator($administrator);
		
		// Create a field for the administrator's current password
		$field = new M_FieldPassword('currentPassword');
		$field->setTitle(t('Your current password'));
		$field->setMandatory(TRUE);
		$field->setShowingStrength(TRUE);
		$this->addField($field);
		
		// Create a field for the administrator's new password
		$field = new M_FieldPassword('newPassword');
		$field->setTitle(t('Your new password'));
		$field->setMandatory(TRUE);
		$field->setShowingStrength(TRUE);
		$this->addField($field);
		
		// Create a field for the administrator's new password (confirm)
		$field = new M_FieldPassword('confirmPassword');
		$field->setTitle(t('Confirm your new password'));
		$field->setMandatory(TRUE);
		$this->addField($field);
	}
	
	/**
	 * Set Administrator
	 * 
	 * Can be used to set the administrator of which the data should be edited by
	 * the form.
	 * 
	 * @access public
	 * @param Administrator $administrator
	 * 		The administrator of which the data should be edited by the form
	 * @return void
	 */
	public function setAdministrator(Administrator $administrator) {
		$this->setDataObject($administrator);
	}
	
	/**
	 * Magic validation
	 * 
	 * @access public
	 * @return void
	 */
	public function __validate() {
		// Download form values:
		$values = $this->getValues();
		
		// Check if current password is correct:
		if($this->getDataObject()->getPassword() == md5($values['currentPassword'])) {
			// If so, we check if the new passwords match:
			if($values['newPassword'] == $values['confirmPassword']) {
				// Everything is OK now! :) We return success:
				return TRUE;
			}
			// If the passwords do not match, we return failure
			// (+ error message)
			else {
				$this->getField('confirmPassword')->setErrorMessage(t('Please make sure to correctly repeat the introduced password.'));
				return FALSE;
			}
		}
		// If the current password is not correct, we return failure
		// (+ error message)
		else {
			$this->getField('currentPassword')->setErrorMessage(t('Oops! That\'s not your current password! Please try again.'));
			return FALSE;
		}
	}
	
	/**
	 * Form actions
	 *
	 * @see M_Form::actions()
	 * @access public
	 * @param array $values
	 * 		The values that have been collected by the method {@link M_Form::getValues()}.
	 * @return unknown
	 */
	public function actions(array $values) {
		// Set the new password of the administrator:
		$this->_dataObject->setPassword(md5($values['newPassword']));
		
		// Save the changes to DB:
		return $this->_dataObject->save();
	}
}