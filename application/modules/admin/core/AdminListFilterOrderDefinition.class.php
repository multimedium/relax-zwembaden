<?php
/**
 * AdminListFilterOrderDefinition
 *
 * @package App
 * @subpackage Admin
 */
class AdminListFilterOrderDefinition extends AdminListFilterWithValuesDefinition {
	/**
	 * Sorting order
	 * 
	 * Stores the order in which the sorting filter should be applied (ASC/DESC)
	 * 
	 * Read the docs on 
	 * 
	 * - {@link AdminListFilterOrderDefinition::getOrder()}
	 * - {@link AdminListFilterOrderDefinition::setOrder()}
	 * 
	 * for more info.
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_sortingOrder;
	
	/**
	 * Get {@link M_DbQueryFilter}
	 * 
	 * @see AdminListFilterDefinition::getDbQueryFilter()
	 * @access public
	 * @return M_DbQueryFilterOrder
	 */
	public function getDbQueryFilter() {
		return new M_DbQueryFilterOrder($this->getValue(), $this->getOrder());
	}
	
	/**
	 * Get {@link AdminSmartFolderFilter}
	 * 
	 * @see AdminListDefinition::getAdminSmartFolderFilter()
	 * @access public
	 * @return AdminSmartFolderFilter
	 */
	public function getAdminSmartFolderFilter() {
		M_Loader::loadDataObject('AdminSmartFolderFilter', 'admin');
		$f = new AdminSmartFolderFilter();
		$f->setListFilterDefinitionIndex($this->getIndex());
		$f->setValue($this->getValue());
		$f->setComparison($this->getOrder());
		return $f;
	}
	
	/**
	 * Get type of filter
	 * 
	 * @see AdminListDefinition::getType()
	 * @access public
	 * @return string
	 */
	public function getType() {
		return 'order';
	}
	
	/**
	 * Get order
	 * 
	 * NOTE:
	 * This method excepts either ASC or DESC as (a string) value. If neither one
	 * of these values is given, this method will throw an exception.
	 * 
	 * @throws M_Exception
	 * @see AdminListFilterOrderDefinition::getOrder() 
	 * @access public
	 * @return string
	 */
	public function setOrder($order) {
		// Check if valid order. If not:
		$order = strtoupper((string) $order);
		if($order != 'ASC' && $order != 'DESC') {
			// throw new exception
			throw new M_Exception(sprintf(
				'Cannot set order "%s" in %s; Excepting "ASC" or "DESC"',
				$order,
				__CLASS__
			));
		}
		
		// Set the order:
		$this->_sortingOrder = $order;
	}
	
	/**
	 * Get order
	 * 
	 * Get the order in which the sorting filter should be applied (ASC/DESC).
	 * 
	 * @see AdminListFilterOrderDefinition::setOrder() 
	 * @access public
	 * @return string
	 */
	public function getOrder() {
		return $this->_sortingOrder;
	}
}