<?php
/**
 * AdminLoader class
 * 
 * This class is very similar to {@link M_Loader}, but is used to load
 * classes that have been crafted for the admin module.
 * 
 * @package App
 * @subpackage Admin
 */
define('FOLDER_ADMIN', 'admin');

class AdminLoader {
	/**
	 * Admin folder
	 * 
	 * This constant holds the folder name of the admin classes that are 
	 * being used throughout the application.
	 * 
	 * This constant, to the public API, is an alias for the
	 * following alternative methods of obtaining the forms' folder:
	 * 
	 * - {@link FOLDER_ADMIN}
	 * - {@link AdminLoader::getAdminFolder()}
	 *
	 */
	const ADMIN = FOLDER_ADMIN;
	
	/**
	 * Get Admin folder
	 * 
	 * This method returns the admin folder used throughout the active 
	 * application.
	 * 
	 * Note that the admin folder can only be used in combination with
	 * the admin module
	 * 
	 * This method, to the public API, is an alternative to the use
	 * of the following constants for obtaining the forms folder:
	 * 
	 * - {@link FOLDER_ADMIN}
	 * - {@link AdminLoader::ADMIN}
	 * 
	 * @access public
	 * @return string
	 */
	public static function getAdminFolder() {
		return self::ADMIN;
	}	
	
	/**
	 * Return the file path for a resource file used in admin-mode
	 * 
	 * @param str $module
	 * @return str
	 */
	public static function getAdminResourcesPath($module = null) {
		return self::getModuleAdminDirectory($module)->getPath() . '/' . M_Loader::getResourcesFolder();
	}
	
	/**
	 * Load view (for a specific module)
	 * 
	 * This method will try to load a view class in the given module.
	 * Based on the module name and view classname, the loader will be 
	 * able to determine the path, and load it correctly.
	 * 
	 * @static
	 * @see AdminLoader::getViewClassFile()
	 * @access public
	 * @param string $class
	 * 		The View Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return void
	 */
	public static function loadView($class, $moduleId) {
		$fn = self::getViewClassFile($class, $moduleId);
		if($fn->exists()) {
			require_once $fn->getPath();
		} else {
			throw new M_Exception(sprintf(
				'Could not load Admin View Class %s, from module %s',
				$class,
				$moduleId
			));
		}
	}

	/**
	 * Get a (constructed) view
	 *
	 * @access public
	 * @param string $class
	 * 		The class name of the view
	 * @param string $moduleId
	 * 		The module from which to load the controller
	 * @return M_View
	 */
	public static function getView($class, $moduleId = NULL) {
		// Load the view:
		self::loadView($class, $moduleId);

		// Construct the view, and return as result:
		return new $class;
	}
	
	/**
	 * Load controller (for a specific module)
	 * 
	 * This method will try to load a controller class in the given 
	 * module. Based on the module name and controller classname, the 
	 * loader will be able to determine the path, and load it correctly.
	 * 
	 * @static
	 * @see AdminLoader::getControllerClassFile()
	 * @access public
	 * @param string $class
	 * 		The Controller Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return void
	 */
	public static function loadController($class, $moduleId) {
		$fn = self::getControllerClassFile($class, $moduleId);
		if($fn->exists()) {
			require_once $fn->getPath();
		} else {
			throw new M_Exception(sprintf(
				'Could not load Admin Controller Class %s, from module %s',
				$class,
				$moduleId
			));
		}
	}

	/**
	 * Get a (constructed) controller
	 *
	 * @access public
	 * @param string $class
	 * 		The class name of the controller
	 * @param string $moduleId
	 * 		The module from which to load the controller
	 * @return M_Controller
	 */
	public static function getController($class, $moduleId = NULL) {
		// Load the controller:
		self::loadController($class, $moduleId);

		// Construct the controller, and return as result:
		return new $class;
	}
	
	/**
	 * Load form (for a specific module)
	 * 
	 * This method will try to load a form class in the given module. Based on 
	 * the module name and form classname, the loader will be able to 
	 * determine the path, and load it correctly.
	 * 
	 * @static
	 * @see AdminLoader::getFormClassFile()
	 * @access public
	 * @param string $class
	 * 		The Form Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return void
	 */
	public static function loadForm($class, $moduleId) {
		$fn = self::getFormClassFile($class, $moduleId);
		if($fn->exists()) {
			require_once $fn->getPath();
		} else {
			throw new M_Exception(sprintf(
				'Could not load Admin Form Class %s, from module %s',
				$class,
				$moduleId
			));
		}
	}


	/**
	 * Get a (constructed) form
	 *
	 * @access public
	 * @param string $class
	 * 		The class name of the form
	 * @param string $moduleId
	 * 		The module from which to load the form
	 * @return M_Form
	 */
	public static function getForm($class, $moduleId = NULL) {
		// Load the form:
		self::loadForm($class, $moduleId);

		// Construct the form, and return as result:
		return new $class;
	}
	
	/**
	 * Load model (for a specific module)
	 * 
	 * This method will try to load a model class in the given module. Based on 
	 * the module name and model classname, the loader will be able to 
	 * determine the path, and load it correctly.
	 * 
	 * @static
	 * @see AdminLoader::getModelClassFile()
	 * @access public
	 * @param string $class
	 * 		The Model Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return void
	 */
	public static function loadModel($class, $moduleId) {
		$fn = self::getModelClassFile($class, $moduleId);
		if($fn->exists()) {
			require_once $fn->getPath();
		} else {
			throw new M_Exception(sprintf(
				'Could not load Admin Model Class %s, from module %s',
				$class,
				$moduleId
			));
		}
	}
	
	/**
	 * Load Admin Core Class
	 * 
	 * @static
	 * @see AdminLoader::getCoreClassFile()
	 * @access public
	 * @param string $class
	 * 		The Admin Core Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return void
	 */
	public static function loadCoreClass($class, $moduleId = 'admin') {
		$fn = self::getCoreClassFile($class, $moduleId);
		if($fn->exists()) {
			require_once $fn->getPath();
		} else {
			throw new M_Exception(sprintf(
				'Could not load Admin Core Class %s',
				$class
			));
		}
	}
	
	/**
	 * Get view class file
	 * 
	 * Used by {@link AdminLoader::loadView()} to determine the absolute
	 * path to the {@link M_View} class.
	 * 
	 * @static
	 * @see AdminLoader::loadView()
	 * @access public
	 * @param string $class
	 * 		The View Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return M_File
	 */
	public static function getViewClassFile($class, $moduleId) {
		// If the view is from the Admin Module
		if($moduleId == 'admin') {
			return new M_File(
				M_Loader::getAbsolute(
					M_Loader::getModulePath('admin') . '/' .
					M_Loader::getViewsFolder() . '/' . 
					ucfirst($class) . '.class.php'
				)
			);
		}
		
		// If the view is not from the Admin Module:
		return self::getModuleAdminSourceFile(
			$moduleId, 
			M_Loader::getViewsFolder() . '/' . ucfirst($class) . '.class.php'
		);
	}
	
	/**
	 * Get controller class file
	 * 
	 * Used by {@link AdminLoader::loadController()} to determine the 
	 * absolute path to the controller class.
	 * 
	 * @static
	 * @see AdminLoader::loadController()
	 * @access public
	 * @param string $class
	 * 		The Controller Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return M_File
	 */
	public static function getControllerClassFile($class, $moduleId) {
		// If the controller is from the Admin Module
		if($moduleId == 'admin') {
			return new M_File(
				M_Loader::getAbsolute(
					M_Loader::getModulePath('admin') . '/' .
					M_Loader::getControllersFolder() . '/' . 
					ucfirst($class) . '.class.php'
				)
			);
		}
		
		// If the controller is not from the Admin Module:
		return self::getModuleAdminSourceFile(
			$moduleId, 
			M_Loader::getControllersFolder() . '/' . ucfirst($class) . '.class.php'
		);
	}
	
	/**
	 * Get form class file
	 * 
	 * Used by {@link AdminLoader::loadForm()} to determine the 
	 * absolute path to the {@link M_Form} class.
	 * 
	 * @static
	 * @see AdminLoader::loadForm()
	 * @access public
	 * @param string $class
	 * 		The Form Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return M_File
	 */
	public static function getFormClassFile($class, $moduleId) {
		// If the form is from the Admin Module
		if($moduleId == 'admin') {
			return new M_File(
				M_Loader::getAbsolute(
					M_Loader::getModulePath('admin') . '/' .
					M_Loader::getFormsFolder() . '/' . 
					ucfirst($class) . '.class.php'
				)
			);
		}
		
		// If the form is not from the Admin Module:
		return self::getModuleAdminSourceFile(
			$moduleId, 
			M_Loader::getFormsFolder() . '/' . ucfirst($class) . '.class.php'
		);
	}
	
	/**
	 * Get model class file
	 * 
	 * Used by {@link AdminLoader::loadModel()} to determine the 
	 * absolute path to the model class.
	 * 
	 * @static
	 * @see AdminLoader::loadModel()
	 * @access public
	 * @param string $class
	 * 		The Model Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return M_File
	 */
	public static function getModelClassFile($class, $moduleId) {
		// If the form is from the Admin Module
		if($moduleId == 'admin') {
			return new M_File(
				M_Loader::getAbsolute(
					M_Loader::getModulePath('admin') . '/' .
					M_Loader::getModelsFolder() . '/' . 
					ucfirst($class) . '.class.php'
				)
			);
		}
		
		// If the model is not from the Admin Module:
		return self::getModuleAdminSourceFile(
			$moduleId, 
			M_Loader::getModelsFolder() . '/' . ucfirst($class) . '.class.php'
		);
	}
	
	/**
	 * Get core class file
	 * 
	 * Used by {@link AdminLoader::loadCoreClass()} to determine the 
	 * absolute path to the Admin Core Class.
	 * 
	 * @static
	 * @see AdminLoader::loadCoreClass()
	 * @access public
	 * @param string $class
	 * 		The Admin Core Class
	 * @param string $moduleId
	 * 		The Module ID
	 * @return M_File
	 */
	public static function getCoreClassFile($class, $moduleId = 'admin') {
		// If the form is from the Admin Module
		if($moduleId == 'admin') {
			return new M_File(
				M_Loader::getAbsolute(
					M_Loader::getModulePath('admin') . '/' .
					'core/' .
					ucfirst($class) . '.class.php'
				)
			);
		}

		// If the form is not from the Admin Module:
		return self::getModuleAdminSourceFile(
			$moduleId, 
			'core/' . ucfirst($class) . '.class.php'
		);
	}
	
	/**
	 * Get XML Specification File
	 * 
	 * Provides an instance of {@link M_File} that points to the location
	 * of an XML file; e.g. the List Definition file
	 * 
	 * @static
	 * @access public
	 * @param string $xml
	 * 		The XML filename
	 * @param string $moduleId
	 * 		The Module ID
	 * @return M_File
	 */
	public static function getXmlSpecsFile($xml, $moduleId) {
		return self::getModuleAdminSourceFile($moduleId, 'resources/definitions/' . $xml);
	}
	
	/**
	 * Get Module's admin-related file
	 * 
	 * @static
	 * @access public
	 * @param string $moduleId
	 * @param string $path
	 * @return M_File
	 */
	public static function getModuleAdminSourceFile($moduleId, $path) {
		return new M_File(
			self::getModuleAdminDirectory($moduleId)->getPath() . '/' . 
			M_Helper::ltrimCharlist($path, '/')
		);
	}
	
	/**
	 * Get Module's admin folder
	 * 
	 * @static
	 * @access public
	 * @param string $moduleId
	 * @return M_Directory
	 */
	public static function getModuleAdminDirectory($moduleId) {
		// Get the configuration of the Admin module
		$config = M_Application::getConfigOfModule('admin');
		
		// Check if the path to definition files is defined:
		if(isset($config->moduleAdminDirectoryPath) && ! empty($config->moduleAdminDirectoryPath)) {
			// If so, we compose that path
			return new M_Directory(
				strtr($config->moduleAdminDirectoryPath, array(
					'@moduleId' => $moduleId
				))
			);
		}
		
		// If still here, return the default path:
		return new M_Directory(
			M_Loader::getModulePath($moduleId) . '/admin/'
		);
	}
	
	/**
	 * Has admin folder for modules, in config?
	 * 
	 * @static
	 * @access public
	 * @return boolean
	 */
	public static function hasModuleAdminDirectoryInConfig() {
		// Get the configuration of the Admin module
		$config = M_Application::getConfigOfModule('admin');
		
		// Check if the path to definition files is defined:
		return (
			isset($config->moduleAdminDirectoryPath) && 
			! empty($config->moduleAdminDirectoryPath)
		);
	}
}