<?php
/**
 * AdminListDefinition
 *
 * @package App
 * @subpackage Admin
 */
class AdminListDefinition extends AdminDefinition {
	/**
	 * Application Module
	 * 
	 * This property stores the Application Module for which we are
	 * getting a list definition. Note that the module is not being
	 * represented by an instance of {@link M_ApplicationModule}, but
	 * by an instance of {@link AdminModule}.
	 *
	 * @access private
	 * @var AdminModule
	 */
	private $_module;
	
	/**
	 * Data Object ID
	 * 
	 * This property stores the ID of the Data Object for which we are
	 * getting a list definition.
	 *
	 * @access private
	 * @var AdminModule
	 */
	private $_dataObjectId;
	
	/**
	 * XML
	 * 
	 * The list definition is being stored in an XML file. This property
	 * stores the SimpleXMLElement instance that helps us in reading 
	 * the XML file.
	 *
	 * @access private
	 * @var SimpleXMLElement
	 */
	private $_xml;
	
	/**
	 * Is validated?
	 * 
	 * This property stores a boolean flag, which tells us whether or not the
	 * XML Definition File has already been validated. Stores TRUE if already
	 * validated, FALSE if not. We store this state, in order to validate only
	 * once.
	 * 
	 * @see AdminListDefinition::_getXmlValidated()
	 * @access private
	 * @var bool
	 */
	private $_validated;
	
	/**
	 * Get title
	 * 
	 * @access public
	 * @return string|null
	 */
	public function getTitle() {
		$rs = $this->_getXmlValidated()->title;
		if($rs) {
			return (string) $rs[0];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get description
	 * 
	 * @access public
	 * @return string|null
	 */
	public function getDescription() {
		$rs = $this->_getXmlValidated()->xpath('description');
		if($rs) {
			return (string) $rs[0];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get flag: "Allow new items"
	 * 
	 * Will tell whether or not the list allows new items to be added or not... 
	 * Returns TRUE if new items can be added, FALSE if not.
	 * 
	 * NOTE:
	 * If this property is not specificly set in the list definition (XML) file,
	 * the return value will default to TRUE
	 * 
	 * @access public
	 * @return TRUE
	 */
	public function getFlagAllowNewItems() {
		$rs = $this->_getXmlValidated()->xpath('allowNewItems');
		if($rs) {
			return (strtolower((string) $rs[0]) != 'no');
		} else {
			return TRUE;
		}
	}
	
	/**
	 * Get Data Object Mapper
	 * 
	 * Will provide with a constructed Data Object Mapper. This mapper is in charge
	 * of fetching the data objects that are included in the list view. Note that
	 * this mapper may already have some filters applied to it:
	 * 
	 * - Default sorting; {@link AdminListDefinition::getColumnsDefaultSort()}
	 * 
	 * Furthermore, this method will apply the provided filters to the mapper.
	 * 
	 * @access public
	 * @param ArrayIterator $filters
	 * 		A collection of {@link AdminListFilterDefinition} instances. These 
	 * 		represent the filters that are to be applied to the list.
	 * @return M_DataObjectMapper
	 */
	public function getMapper(ArrayIterator $filters = NULL) {
		// We make sure that the iterator contains instances of AdminListFilterDefinition
		if(! M_Helper::isIteratorOfClass($filters, 'AdminListFilterDefinition')) {
			// If not, we throw an exception:
			throw new M_Exception(sprintf(
				'%s::%s() expects an iterator of AdminListFilterDefinition instances',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Construct a new Data Object Mapper for the list definition:
		$mapper = $this->getModule()->getDataObjectMapper($this->getDataObjectId());
		
		// First of all, we add the filters for default sorting. Note that we
		// take the applied filters into account:
		foreach($this->getColumnsDefaultSort($filters) as $field => $order) {
			// Apply the sorting filter:
			$mapper->addFilter(new M_DbQueryFilterOrder($field, $order));
		}
		
		// Now, we add the provided filters to the mapper. For each of the filters:
		foreach($filters as $filter) {
			// Note that we download an instance of M_DbQueryFilter from the
			// current AdminListFilterDefinition instance, in order to apply
			// it to the mapper:
			$mapper->addFilter($filter->getDbQueryFilter());
		}

		// If not fetching all columns
		if(! $this->isFetchingAllColumns()) {
			// Construct a filter:
			$filter = new M_DbQueryFilterColumn(TRUE);
			$filter->addColumn('id');

			// We also add filters to the mapper, in order to only fetch those
			// columns that are actually being shown. For each included column
			foreach($this->getColumns() as $column) {
				// Add to temporary array:
				/* @var $column AdminListColumnDefinition */
				$filter->addColumn($column->getField());
			}

			// For each extra field
			foreach($this->getExtraColumns() as $column) {
				// Add to temporary array
				$filter->addColumn($column);
			}

			// Add a filter to the mapper
			$mapper->addFilter($filter);
		}
		

		// Return the mapper:
		return $mapper;
	}
	
	/**
	 * Get required filters, for sorting
	 * 
	 * It is perfectly possible to allow manual sorting only if a set of filters
	 * has been applied to the list. This method will provide with the collection
	 * of filters that must be applied to the list, in order to manually sort!
	 * The collection is represented by an instance of ArrayIterator, populated
	 * by {@link AdminListFilterDefinition} objects.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getRequiredFiltersForSorting() {
		// Return value
		$out = array();
		
		// Check if the definition of sortable exists in the list definition:
		$rs = $this->_getSortDragField();
		if($rs) {
			// Check if filters are required:
			if(isset($rs['requireFilter'])) {
				// Get the required filters' description (string), and parse an 
				// array out of it:
				$temp = explode(',', preg_replace('/[\s]+/', '', (string) $rs['requireFilter']));
				
				// Now, we have an array of required filters. We still have to 
				// separate the index number from the required comparison operator.
				// For each of the filters:
				foreach($temp as $currentTemp) {
					// Check if comparison operator has been provided
					$currentTemp = explode(':', $currentTemp);
					
					// If not, throw an exception!
					if(count($currentTemp) != 2) {
						throw new M_Exception(sprintf(
							'Cannot identify required filters (for sorting). Missing Filter Index and/or Comparison Operator. You should define the required filter (for example) as following: "0:EQ"'
						));
					}
					
					// Check if a filter with the given index exists:
					$filter = $this->getFilter((int) $currentTemp[0]);
					
					// If not, throw an exception:
					if(! $filter) {
						throw new M_Exception(sprintf(
							'Cannot identify required filters (for sorting). Filter at index %d is undefined',
							(int) $currentTemp[0]
						));
					}
					
					// Make sure the filter is of type "where"
					if(! $filter->getType() == 'where') {
						throw new M_Exception(sprintf(
							'Required filters (for sorting) must be of type "where"!'
						));
					}
					
					// Set the comparison operator:
					$filter->setComparisonOperator((string) $currentTemp[1]);
					
					// Add the filter to the return value:
					$out[] = $filter;
				}
			}
		}
		
		// Return iterator:
		return new ArrayIterator($out);
	}
	
	/**
	 * Is sortable?
	 * 
	 * This method will tell whether or not the instances in the list can be
	 * manually sorted (drag and drop).
	 * 
	 * It is perfectly possible to allow manual sorting only if a set of filters
	 * has been applied to the list. So, in order to check if the list is manually
	 * sortable, you can provide this method with a collection of applied filters.
	 * Note that this collection should be an ArrayIterator with instances of
	 * {@link AdminListFilterDefinition}.
	 * 
	 * The definition of manual sorting (in the XML file), should be done as 
	 * following:
	 * 
	 * <code>
	 *    <sortDragField requireFilter="0:EQ,1:EQ">order</sortDragField>
	 * </code>
	 * 
	 * The "requireFilter" attribute indicates the collection of filters that
	 * are required, in order to manually sort instances in the list. The value
	 * of the tag holds the name of the field where the new order index number
	 * is to be stored.
	 * 
	 * Note that the "requireFilter" attribute defines both the index number of
	 * the filter, as the required comparison operator.
	 * 
	 * @throws M_Exception
	 * @access public
	 * @param ArrayIterator $filters
	 * 		Optionally, the collection of applied filters
	 * @return bool $flag
	 * 		Returns TRUE if the instances can be sorted, FALSE if not.
	 */
	public function isInstancesDraggableForSorting(ArrayIterator $filters = NULL) {
		// Check if the definition of sortable exists in the list definition:
		$rs = $this->_getSortDragField();
		
		// If not, we simply cannot sort :)
		if(! $rs) {
			return FALSE;
		}
		
		// Get the collection of required filters:
		$requiredFilters = $this->getRequiredFiltersForSorting();
		
		// If required filters have been defined:
		if($requiredFilters->count() > 0) {
			// If no filters have been provided, then obviously the instances 
			// cannot be sorted:
			if(! $filters) {
				return FALSE;
			}
			
			// If filters have been provided, we check the collection of filters
			// against the collection of required filters. First, we check if the
			// filters are instances of AdminListFilterDefinition:
			if(! M_Helper::isIteratorOfClass($filters, 'AdminListFilterDefinition')) {
				throw new M_Exception(sprintf(
					'Cannot determine if sortable; Collection of filters must be instances of AdminListFilterDefinition'
				));
			}
			
			// Check if matching:
			if($requiredFilters->count() != $filters->count()) {
				return FALSE;
			}
			
			// Then, we check if all required filters are being applied. First of all,
			// we download the index numbers of the required filters into an array:
			$requiredFilterIndexes = array();
			foreach($requiredFilters as $filter) {
				$requiredFilterIndexes[$filter->getIndex()] = $filter->getComparisonOperator();
			}
			
			// For each of the applied filters:
			foreach($filters as $filter) {
				// If the comparison operator matches:
				if(
					isset($requiredFilterIndexes[$filter->getIndex()]) && 
					$requiredFilterIndexes[$filter->getIndex()] == $filter->getComparisonOperator()
				) {
					// Remove the filter from required:
					unset($requiredFilterIndexes[$filter->getIndex()]);
				}
			}
			
			// We can sort, if the Required Filter Indexes' array is now empty:
			return (count($requiredFilterIndexes) == 0);
		}
		
		// If no required filters have been defined, we can only sort if no filters
		// have been applied at all:
		if($filters && $filters->count() > 0) {
			return FALSE;
		}
		
		// If still here, it means that we can sort :)
		return TRUE;
	}
	
	/**
	 * Get "Instance Sort Drag Field"
	 * 
	 * This method will provide with the field where the order index number is to
	 * be stored, when manually sorting the instances in the list.
	 * 
	 * NOTE:
	 * Will return NULL if the instances cannot be sorted manually. For more 
	 * info, read {@link AdminListDefinition::isInstancesDraggableForSorting()}
	 * 
	 * @see AdminListDefinition::isInstancesDraggableForSorting()
	 * @access public
	 * @return string
	 */
	public function getSortDragFieldName() {
		return (string) $this->_getSortDragField();
	}
	
	/**
	 * Get number of filters
	 * 
	 * Will provide with the number of filters that has been set by the
	 * List Definition.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfFilters() {
		return count($this->_getXmlValidated()->xpath('filters/filter'));
	}
	
	/**
	 * Get filters
	 * 
	 * Will provide with an iterator of filters. The collection of 
	 * columns is represented by an ArrayIterator, which is populated
	 * by a group of {@link AdminListFilterDefinition} instances.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getFilters() {
		// Initiate the collection
		$out = array();
		
		// For each of the filters in the definition:
		for($i = 0, $n = $this->getNumberOfFilters(); $i < $n; $i ++) {
			// Add the filter to the collection:
			$filter = $this->getFilter($i);
			if($filter) {
				$out[$i] = $filter;
			}
		}
		
		// Return the collection of filters:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get filter
	 * 
	 * Will provide with the filter at a given index. Check out 
	 * Example 1, to learn how you can extract the filters out of the
	 * list definition:
	 * 
	 * Example 1
	 * <code>
	 *    $list = new AdminListDefinition(new AdminModule('news'), 'news');
	 *    foreach($list->getFilters() as $filter) {
	 *       echo $filter->getTitle() . '<br>';
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @param integer $index
	 * 		The index number of the filter, starting at 0 (ZERO)
	 * @return AdminListFilterDefinition|null
	 */
	public function getFilter($index) {
		// Look for the requested filter:
		$rs = $this->_getXmlValidated()->xpath(sprintf('filters/filter[%d]', ($index + 1)));
		
		// If found:
		if($rs && isset($rs[0])) {
			// Save the matched node:
			$node = $rs[0];
			
			// There are different types of filters, and a class exists to describe
			// each specific definition. We check the type definition of the filter:
			switch(strtolower($node['type'])) {
				// WHERE
				case 'where':
					// Prepare temporary array of field definitions:
					$temp = array();
					
					// Get definitions of fields, for comparisons (if any)
					$rs = $this->_getXmlValidated()->xpath(sprintf('filters/filter[%d]/field[@for]', ($index + 1)));
					if($rs) {
						// For each of the defined fields:
						foreach($rs as $elm) {
							// Try to mount the field:
							try {
								$field = M_Field::factory('where-' . M_Helper::getUniqueToken(), $this->_getConfigFromSimpleXMLElement($elm));
							}
							// Catch errors:
							catch(Exception $e) {
								throw new M_Exception(sprintf(
									'Cannot mount field for comparison operators %s in filter %d (%s)',
									$elm['for'],
									$index,
									$e->getMessage()
								));
							}
							
							// For each of the comparison operators for which the
							// field has been defined:
							foreach(explode(',', (string) $elm['for']) as $op) {
								// Remember about the field:
								$op = trim($op);
								$temp[$op] = $field;
							}
						}
					}
					
					// Construct the definition of the "where" filter:
					$w = new AdminListFilterWhereDefinition($node->title);
					$w->setField($node->field);
					$w->setIndex($index);
					
					// For each of the supported comparisons
					foreach($node->comparisons->children() as $comparison) {
						// Add supported comparison operators:
						$w->addSupportedComparisonOperator(
							// Operator:
							(string) $comparison['operator'],
							// Operator title
							(string) $comparison,
							// Set the field (if any)
							(isset($temp[(string) $comparison['operator']]) ? $temp[(string) $comparison['operator']] : NULL)
						);
					}
					
					// return the definition object:
					return $w;
				
				// ORDER
				case 'order':
					// Construct the definition of the "order" filter, and return
					// it as result of this method:
					$o = new AdminListFilterOrderDefinition($node->title);
					$o->setIndex($index);
					
					// For each of the sortable fields:
					foreach($node->fields->children() as $field) {
						// Add supported value
						$o->setSupportedValue($field->getName(), (string) $field);
					}
					
					// return filter definition
					return $o;
				
				// LIMIT
				case 'limit':
					// Construct the definition of the "limit" filter, and return
					// it as result of this method:
					$l = new AdminListFilterLimitDefinition($node->title);
					$l->setIndex($index);
					
					// Add supported values
					$l->setSupportedValue(1,   p('1 item', '@count items', 1));
					$l->setSupportedValue(2,   p('1 item', '@count items', 2));
					$l->setSupportedValue(3,   p('1 item', '@count items', 3));
					$l->setSupportedValue(4,   p('1 item', '@count items', 4));
					$l->setSupportedValue(5,   p('1 item', '@count items', 5));
					$l->setSupportedValue(10,  p('1 item', '@count items', 10));
					$l->setSupportedValue(15,  p('1 item', '@count items', 15));
					$l->setSupportedValue(20,  p('1 item', '@count items', 20));
					$l->setSupportedValue(25,  p('1 item', '@count items', 25));
					$l->setSupportedValue(30,  p('1 item', '@count items', 30));
					$l->setSupportedValue(35,  p('1 item', '@count items', 35));
					$l->setSupportedValue(40,  p('1 item', '@count items', 40));
					$l->setSupportedValue(45,  p('1 item', '@count items', 45));
					$l->setSupportedValue(50,  p('1 item', '@count items', 50));
					$l->setSupportedValue(55,  p('1 item', '@count items', 55));
					$l->setSupportedValue(60,  p('1 item', '@count items', 60));
					$l->setSupportedValue(65,  p('1 item', '@count items', 65));
					$l->setSupportedValue(70,  p('1 item', '@count items', 70));
					$l->setSupportedValue(75,  p('1 item', '@count items', 75));
					$l->setSupportedValue(80,  p('1 item', '@count items', 80));
					$l->setSupportedValue(85,  p('1 item', '@count items', 85));
					$l->setSupportedValue(90,  p('1 item', '@count items', 90));
					$l->setSupportedValue(95,  p('1 item', '@count items', 95));
					$l->setSupportedValue(100, p('1 item', '@count items', 100));
					
					// return filter definition
					return $l;
			}
		}
		
		// If the filter has not been found, we return null;
		return NULL;
	}
	
	/**
	 * Get number of columns
	 * 
	 * Will provide with the number of columns that has been set by the
	 * List Definition.
	 * 
	 * @access public
	 * @return integer
	 */
	public function getNumberOfColumns() {
		return count($this->_getXmlValidated()->xpath('columns/column'));
	}
	
	/**
	 * Get columns
	 * 
	 * Will provide with an iterator of columns. The collection of 
	 * columns is represented by an ArrayIterator, which is populated
	 * by a group of {@link AdminListColumnDefinition} instances.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getColumns() {
		// Initiate the collection
		$out = array();
		
		// For each of the columns in the definition:
		for($i = 0, $n = $this->getNumberOfColumns(); $i < $n; $i ++) {
			// Add the column to the collection:
			$column = $this->getColumn($i);
			if($column) {
				$out[$i] = $column;
			}
		}
		
		// Return the collection of columns:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get column
	 * 
	 * Will provide with the column at a given index. Check out 
	 * Example 1, to learn how you can extract the columns out of the
	 * list definition:
	 * 
	 * Example 1
	 * <code>
	 *    $list = new AdminListDefinition(new AdminModule('news'), 'news');
	 *    for($i = 0, $n = $list->getNumberOfColumns(); $i < $n; $i ++) {
	 *       $column = $list->getColumn($i);
	 *       echo $column->getTitle() . '<br>';
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @param integer $index
	 * 		The index number of the column, starting at 0 (ZERO)
	 * @return AdminListColumnDefinition|null
	 */
	public function getColumn($index) {
		// Look for the requested column:
		$rs = $this->_getXmlValidated()->xpath(sprintf('columns/column[%d]', ($index + 1)));
		
		// If found:
		if($rs && isset($rs[0])) {
			// Save the matched node:
			$node = $rs[0];
			
			// Construct the AdminListColumnDefinition instance:
			$column = new AdminListColumnDefinition($node->title);
			$column->setField($node->field);
			$column->setIsSortable((isset($node['sortable']) && strtolower($node['sortable']) == 'true'));
			if(isset($node->width)) {
				$column->setWidth($node->width);
			}
			$column->setIndex($index);
			if(isset($node['type'])) {
				$column->setType((string) $node['type']);
			}
			return $column;
		}
		// If the title could not have been found, we return null
		else {
			return NULL;
		}
	}
	
	/**
	 * Get column, by field name
	 * 
	 * Will provide with the column in the list, given a field name.
	 * 
	 * Example 1
	 * <code>
	 *    $list = new AdminListDefinition(new AdminModule('news'), 'news');
	 *    for($i = 0, $n = $list->getNumberOfColumns(); $i < $n; $i ++) {
	 *       $column = $list->getColumn($i);
	 *       echo $column->getTitle() . '<br>';
	 *    }
	 * </code>
	 * 
	 * @see AdminListDefinition::getColumn()
	 * @access public
	 * @param string $fieldName
	 * 		The field name with which to look up the column
	 * @return AdminListColumnDefinition|null
	 */
	public function getColumnByFieldName($fieldName) {
		// For each of the columns in the list definition:
		foreach($this->getColumns() as $column) {
			// If the current column's field name matches the provided one:
			if($column->getField() == $fieldName) {
				// We return the column:
				return $column;
			}
		}
		
		// If we are still here, it means that we did not find the requested
		// column. In that case, we return NULL
		return NULL;
	}
	
	/**
	 * Get columns of default order
	 * 
	 * Will provide with the column(s) that are being used by default, to sort 
	 * the listed instances. Note that the result of this method is an iterator,
	 * of which the keys are the columns' field names, and the values are the
	 * sorting order (ASC/DESC).
	 * 
	 * NOTE:
	 * In order to define the columns for default sorting, you need to define 
	 * the columns as following, in the list definition file (XML):
	 * 
	 * <code>
	 *    <definition>
	 *    ...
	 *       <column sort="ASC">
	 *          ...
	 *       </column>
	 *       <column sort="DESC">
	 *          ...
	 *       </column>
	 *    ...
	 *    </definition>
	 * </code>
	 * 
	 * @access public
	 * @param ArrayIterator $filters
	 * 		A collection of {@link AdminListFilterDefinition} instances. These 
	 * 		represent the filters that are to be applied to the list.
	 * @return ArrayIterator $iterator
	 * 		The collection of columns that are to be used for sorting
	 */
	public function getColumnsDefaultSort(ArrayIterator $filters = NULL) {
		// We make sure that the iterator contains instances of AdminListFilterDefinition
		if(! M_Helper::isIteratorOfClass($filters, 'AdminListFilterDefinition')) {
			// If not, we throw an exception:
			throw new M_Exception(sprintf(
				'%s::%s() expects an iterator of AdminListFilterDefinition instances',
				__CLASS__,
				__FUNCTION__
			));
		}
		
		// Prepare an empty array, in which we store the columns:
		$out = array();
		
		// If the listed instances can be sorted (drag & drop) with the provided
		// collection of filters
		if($this->isInstancesDraggableForSorting($filters)) {
			// we first add a sorting filter in order to make sure that instances 
			// appear in the right order:
			$out[$this->getSortDragFieldName()] = 'ASC';
		}
		
		// Now, we need to run through the provided filters, to check whether
		// or not the instances are being sorted by a given field. For each of 
		// the provided filters:
		foreach($filters as $filter) {
			// If the current filter applies sorting:
			if($filter->getType() == 'order') {
				// We return the current collection of columns as the complete
				// collection of columns, because we no longer apply the default
				// sorting:
				return new ArrayIterator($out);
			}
		}
		
		// If we are still here, we need to apply default sorting. First, we get
		// the columns in the list:
		$columns = $this->_getXmlValidated()->xpath('columns/column');
		
		// If columns have been found:
		if($columns) {
			// For each of the columns in the list definition:
			foreach($columns as $i => $element) {
				// We check if the column carries the attribute "sort". This attribute
				// defines a column for default sorting.
				if(isset($element['sort'])) {
					// Note that the value of this attribute MUST be ASC/DESC. 
					if(! in_array(strtolower((string) $element['sort']), array('asc', 'desc'))) {
						// If that's not the case, we throw an exception, in order 
						// to notify about the malformed definition!
						throw new M_Exception(sprintf(
							'Either ASC or DESC is expected from the attribute "sort" in column at index %d',
							$i
						));
					}
					
					// We add the column to the collection of columns for sorting:
					$out[(string) $element->field] = strtoupper((string) $element['sort']);
				}
			}
		}
		
		// Return the final collection of sorting columns:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get list view helper
	 * 
	 * Will provide with a constructed List View Helper (which is contained
	 * by the List Definition; see XML File). This List View Helper is used in
	 * combination with {@link AdminListColumnsView::addHelper()}.
	 * 
	 * @access public
	 * @return object
	 */
	public function getListViewHelper() {
		// Get helper node:
		$helper = $this->_getXmlValidated()->helper;
		
		// If helper node is available:
		if($helper) {
			// Check if required child nodes are present:
			$hasModule = (isset($helper->module) && !empty($helper->module));
			$hasClass  = (isset($helper->class)  && !empty($helper->class));
			
			// If not, throw exception
			if(! $hasModule || ! $hasClass) {
				throw new M_Exception(sprintf(
					'Cannot determine List View Helper for module "%s" and DataObject "%s"; missing "module" or "class"',
					$this->getModule()->getId(),
					$this->getDataObjectId()
				));
			}
			
			// Class name:
			$class = (string) $helper->class;
			
			// Module ID:
			$moduleId = (string) $helper->module;
			
			// Check if the view helper exists:
			$helperFile = AdminLoader::getViewClassFile($class, $moduleId);
			if($helperFile->exists()) {
				// Load the class file:
				require_once $helperFile->getPath();
				
				// Construct an instance of the class, and return it:
				return new $class;
			}
			// If the module's view helper does not exist:
			else {
				throw new M_Exception(sprintf(
					'Cannot locate the View Helper "%s" in module "%s": %s',
					$class,
					$moduleId,
					$helperFile->getPath()
				));
			}
		}
		
		// If still here, return NULL
		return NULL;
	}

	/**
	 * Only fetch columns in list definition file?
	 *
	 * @access public
	 * @return bool
	 */
	public function isFetchingAllColumns() {
		// We check if "fetchAllColumns" has been deliberately been disabled.
		// We use this rather strange technique, for backwards compatibility :)
		// Microsoft style :)
		$rs = (
			isset($this->_getXmlValidated()->fetchAllColumns) &&
			strtolower($this->_getXmlValidated()->fetchAllColumns) == 'false'
		);

		// If not disabled, then we fetch all
		return (! $rs);
	}

	/**
	 * Get extra columns to be fetched
	 *
	 * Will return the collection of database columns to be included in the list
	 * definition. Note that this will only return a collection, if "Fetch all
	 * columns" has been disabled
	 *
	 * @access public
	 * @return array
	 */
	public function getExtraColumns() {
		// If all columns are fetched
		if($this->isFetchingAllColumns()) {
			// then, return empty array
			return array();
		}

		// If not, return the collection of extra columns:
		$temp = preg_split('/[,;-\s]+/m', (string) $this->_getXmlValidated()->fetchExtraColumns);

		// Output
		$out = array();

		// For each element in the resulting collection
		foreach($temp as $element) {
			// Clean up
			$element = trim($element);

			// Make sure it's not empty:
			if(! empty($element)) {
				// Add to output
				$out[] = $element;
			}
		}

		// Return the output
		return $out;
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Get XML Node with (manual) sorting options
	 * 
	 * Will provide with the instance of {@link SimpleXMLElement} that describes
	 * the sorting in the list. For more info, read the documentation on 
	 * {@link AdminListDefinition::isInstancesDraggableForSorting()}
	 * 
	 * NOTE:
	 * Returns NULL if the XML Node could not have been found.
	 * 
	 * @access protected
	 * @return SimpleXMLElement|NULL
	 */
	protected function _getSortDragField() {
		// Check if the definition of sortable exists in the list definition:
		$rs = $this->_getXmlValidated()->xpath('sortDragField');
		
		// If so,
		if($rs && count($rs) == 1) {
			// Return the node that defines the sorting:
			return $rs[0];
		}
		
		// If not, return NULL
		return NULL;
	}
	
	/**
	 * Get validated XML
	 * 
	 * Will provide with a validated instance of SimpleXMLElement. Note that the
	 * result of this method is a validated element, of which you may assume that
	 * all required elements are present.
	 * 
	 * NOTE:
	 * If a required element is missing, this method throws an exception
	 * 
	 * @throws M_Exception
	 * @access protected
	 * @return SimpleXMLElement
	 */
	protected function _getXmlValidated() {
		// Get the XML:
		$x = $this->_getXml();
		
		// If not validated before:
		if(! $this->_validated) {
			// Store the state of validation:
			$this->_validated = TRUE;
			
			// Recognized filter comparison operators:
			$_ops = array(
				'EQ'         => 'Equals', 
				'NEQ'        => 'Does not equal',
				'LT'         => 'Less than',
				'LTEQ'       => 'Less than, or equal to', 
				'GT'         => 'Greater than',
				'GTEQ'       => 'Greater than, or equal to', 
				'BEGINSWITH' => 'Begins with',
				'ENDSWITH'   => 'Ends with',
				'CONTAINS'   => 'Contains'
			);
			
			// We try to validate the XML File:
			try {
				// Does the definition have a title?
				$hasTitle = (isset($x->title) && !empty($x->title));
				
				// If not, throw an exception
				if(! $hasTitle) {
					throw new M_Exception('Invalid List Definition; Missing "title"');
				}
				
				// If list columns have been defined, we validate them:
				if(isset($x->columns) && count($x->columns) > 0) {
					$index = 0;
					foreach($x->columns->children() as $column) {
						// Has title and field?
						$hasTitle = (isset($column->title) && !empty($column->title));
						$hasField = (isset($column->field) && !empty($column->field));
						
						// If either one is missing, throw an exception
						if(! $hasTitle || ! $hasField) {
							throw new M_Exception(sprintf(
								'Invalid List Column Definition (index %d); Missing "title" or "field"',
								$index
							));
						}
						
						// Increment column's index number:
						$index += 1;
					}
				}
				
				// If list filters have been defined, we validate them:
				if(isset($x->filters) && count($x->filters) > 0) {
					$index = 0;
					foreach($x->filters->children() as $filter) {
						// Does the filter specify its type, and is that type valid?
						$hasType = (isset($filter['type']) && in_array((string) $filter['type'], array('where', 'order', 'limit')));
						
						// If the type has not been validated, we throw an exception
						if(! $hasType) {
							throw new M_Exception(sprintf(
								'Invalid Filter Definition (index %d); Missing "type", or "type" cannot be recognized. Possible types are "where", "order", or "limit"',
								$index
							));
						}
						
						// Does the filter specify its title?
						$hasTitle = (isset($filter->title) && !empty($filter->title));
						
						// If not, throw an exception
						if(! $hasTitle) {
							throw new M_Exception(sprintf(
								'Invalid Filter Definition (index %d); Missing "title"',
								$index
							));
						}
						
						// If the filter type is "where", we expect comparisons as well:
						if($filter['type'] == 'where') {
							// Check if comparisons have been given:
							$hasComparisons = (isset($filter->comparisons) && count($filter->comparisons) > 0);
							
							// If not provided, we throw an exception:
							if(! $hasComparisons) {
								throw new M_Exception(sprintf(
									'Invalid Filter Definition (index %d); Filter of type "where" expects definition of "comparisons"',
									$index
								));
							}
							
							// We validate the comparisons:
							foreach($filter->comparisons->children() as $comparison) {
								// Does the comparison define the operator, and is the operator
								// recognized?
								$hasOperator = (isset($comparison['operator']) && in_array($comparison['operator'], array_keys($_ops)));
								
								// If not, throw an exception:
								if(! $hasOperator) {
									throw new M_Exception(sprintf(
										'Invalid Filter Definition (index %d); Cannot recognize the comparison operator. Try one of the following: %s.',
										$index,
										implode(', ', $_ops)
									));
								}
								
								// Make sure that the operator has specified a title:
								$title = (string) $comparison;
								
								// If not, we default the value of the title. We do not throw
								// an exception in this case:
								if(empty($title)) {
									$comparison[0] = $_ops[(string) $comparison['operator']];
								}	
							}
						}
						
						// Increment filter's index number:
						$index += 1;
					}
				}
			}
			// If the XML file could not have been validated:
			catch(Exception $e) {
				throw new M_Exception(sprintf(
					'Could not parse the List Definition File "%s" for module "%s", at %s. %s',
					$this->_getXmlFile()->getBasename(),
					$this->getModule()->getId(),
					$this->_getXmlFile()->getPath(),
					$e->getMessage()
				));
			}
		}
		
		// Return the xml:
		return $x;
	}
	
	/**
	 * Get XML File
	 * 
	 * Will provide with the {@link M_File} instance that points to the
	 * XML file, or at least to where it is supposed to be located.
	 * This method does not check whether or not the file exists.
	 * 
	 * @access public
	 * @return M_File
	 */
	protected function _getXmlFile() {
		return AdminLoader::getXmlSpecsFile(
			$this->getModule()->getDataObjectClassName($this->getDataObjectId()) . 'List.xml',
			$this->getModule()->getId()
		);
	}
}