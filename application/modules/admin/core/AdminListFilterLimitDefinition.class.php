<?php
/**
 * AdminListFilterLimitDefinition
 *
 * @package App
 * @subpackage Admin
 */
class AdminListFilterLimitDefinition extends AdminListFilterWithValuesDefinition {
	/**
	 * Get {@link M_DbQueryFilter}
	 * 
	 * @see AdminListFilterDefinition::getDbQueryFilter()
	 * @access public
	 * @return M_DbQueryFilterLimit
	 */
	public function getDbQueryFilter() {
		return new M_DbQueryFilterLimit(0, $this->getValue());
	}
	
	/**
	 * Get {@link AdminSmartFolderFilter}
	 * 
	 * @see AdminListDefinition::getAdminSmartFolderFilter()
	 * @access public
	 * @return AdminSmartFolderFilter
	 */
	public function getAdminSmartFolderFilter() {
		M_Loader::loadDataObject('AdminSmartFolderFilter', 'admin');
		$f = new AdminSmartFolderFilter();
		$f->setListFilterDefinitionIndex($this->getIndex());
		$f->setValue($this->getValue());
		return $f;
	}
	
	/**
	 * Get type of filter
	 * 
	 * @see AdminListDefinition::getType()
	 * @access public
	 * @return string
	 */
	public function getType() {
		return 'limit';
	}
}