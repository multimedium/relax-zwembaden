<?php
/**
 * AdminDefinition class
 * 
 * @package App
 * @subpackage Admin
 */
abstract class AdminDefinition {
	/**
	 * Application Module
	 * 
	 * This property stores the Application Module for which we are getting a 
	 * definition. Note that the module is not being represented by an instance 
	 * of {@link M_ApplicationModule}, but by an instance of {@link AdminModule}.
	 *
	 * @access private
	 * @var AdminModule
	 */
	private $_module;
	
	/**
	 * Data Object ID
	 * 
	 * This property stores the ID of the Data Object for which we are
	 * getting a definition.
	 *
	 * @access private
	 * @var AdminModule
	 */
	private $_dataObjectId;
	
	/**
	 * XML
	 * 
	 * The form definition is being stored in an XML file. This property
	 * stores the SimpleXMLElement instance that helps us in reading 
	 * the XML file.
	 *
	 * @access private
	 * @var SimpleXMLElement
	 */
	private $_xml;

	/**
	 * Data objects
	 *
	 * @see AdminDefinition::addDataObject()
	 * @access private
	 * @var array
	 */
	private $_dataObjectValues = array();

	/**
	 * Enable populations?
	 *
	 * @see AdminDefinition::setEnablePopulations()
	 * @access private
	 * @var bool
	 */
	private $_enablePopulations = TRUE;

	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param AdminModule $module
	 * 		The application module for which to retrieve the definition.
	 * @param string $dataObjectId
	 * 		The ID of the Data Object for which to retrieve the definition. For 
	 * 		more information, read the documentation on
	 * 		{@link M_ApplicationModule::getDataObjectNames()}
	 * @return AdminListDefinition
	 */
	public function __construct(AdminModule $module, $dataObjectId) {
		// We make sure that the provided Data Object ID is existing
		// within the provided application module. To do so, we ask the
		// module for the class name of the corresponding M_DataObject
		// class:
		if(! $module->getDataObjectClassName($dataObjectId)) {
			// If the Data Object ID cannot be identified by the module,
			// we throw an exception. In that case, we cannot construct
			// the list definition...
			throw new M_ApplicationException(sprintf(
				'%s cannot identify the DataObject %s in module %s',
				__CLASS__,
				$dataObjectId,
				$module->getId()
			));
		}
		
		// Set internals:
		$this->_dataObjectId = (string) $dataObjectId;
		$this->_module = $module;
	}

	/* -- GETTERS -- */
	
	/**
	 * Exists?
	 * 
	 * Will tell if the definition exists.
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if existing, FALSE if not
	 */
	public function exists() {
		return $this->_getXmlFile()->exists();
	}
	
	/**
	 * Get Application Module
	 * 
	 * Will provide with the {@link AdminModule} that has been used to get the 
	 * definition.
	 * 
	 * @access public
	 * @return AdminModule
	 */
	public function getModule() {
		return $this->_module;
	}
	
	/**
	 * Get Data Object ID
	 * 
	 * Will provide with the Data Object ID that has been used to get the 
	 * definition.
	 * 
	 * @access public
	 * @return string
	 */
	public function getDataObjectId() {
		return $this->_dataObjectId;
	}
	
	/**
	 * Get ACL Resource ID
	 * 
	 * Will provide with the Resource ID that can be used to check for permissions
	 * in the {@link M_Acl} object.
	 * 
	 * @access public
	 * @return string
	 */
	public function getAclResourceId() {
		return $this->getModule()->getId() . '-' . $this->getDataObjectId();
	}

	/**
	 * Get flag: enable populateWithDataObjects and populateWithDataObjects?
	 *
	 * @access public
	 * @return bool $flag
	 *		Set to TRUE to enable populated nodes, FALSE to disable
	 */
	public function getEnablePopulations() {
		return $this->_enablePopulations;
	}

	/* -- SETTERS -- */
	
	/**
	 * Add Data Object
	 * 
	 * By adding data objects to the Admin Definition, you can insert the values
	 * of the data object into the definition file. This method will use the
	 * {@link M_DataObject::toArray()} method, to make the values of the data object
	 * available in the XML source.
	 * 
	 * Example 1
	 * <code>
	 *    // Get the form definition of module 'product'
	 *    $module = new AdminModule('product');
	 *    $def = $module->getFormDefinition();
	 *    
	 *    // Assuming that Product is a subclass of M_DataObject, add the data 
	 *    // object to the form definition:
	 *    $def->addDataObject('product', new Product());
	 * </code>
	 * 
	 * With the code above, you can now use the following in the XML source of 
	 * the admin definition file, assuming that the Product data object contains
	 * a field that is called 'title'
	 * 
	 * <code>
	 *    <title>Edit the product "$product.title"</title>
	 * </code>
	 * 
	 * @access public
	 * @param string $name
	 * @param M_DataObject $object
	 * @return void
	 */
	public function addDataObjectValues($name, M_DataObject $object) {
		// Export the values
		$this->_dataObjectValues[(string) $name] = $object->toArray();
		
		// Avoid NULL comparison with ID of new objects
		if($object->isNew()) {
			$this->_dataObjectValues[(string) $name]['id'] = 0;
		}
	}

	/**
	 * Set flag: enable populateWithDataObjects and populateWithDataObjects
	 *
	 * @access public
	 * @param bool $flag
	 *		Set to TRUE to enable populated nodes, FALSE to disable
	 * @return void
	 */
	public function setEnablePopulations($flag) {
		$this->_enablePopulations = (bool) $flag;
	}

	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get XML
	 * 
	 * Will provide with the instance of SimpleXMLElement that helps
	 * us in reading the definition's XML file
	 *
	 * @access protected
	 * @return SimpleXMLElement
	 */
	protected function _getXml() {
		// If the XML has not been requested before:
		if(! $this->_xml) {
			// We check if a custom folder has been defined for admin source files:
			if(AdminLoader::hasModuleAdminDirectoryInConfig()) {
				// Get the definition file
				$file = $this->_getXmlFile();
			}
			// If no custom folder is defined:
			else {
				// Get the default definition file
				$defaultFile = $this->_getXmlFile();
				
				// Check if the definition file has been copied to the application's
				// configuration directory:
				$file = new M_File(M_ApplicationConfig::getConfigDirectory() . '/' . $defaultFile->getBasename());

				// If the file cannot be found in the configuration directory, we 
				// make a copy now:
				$copy = false;
				if(! $file->exists()) {
					$copy = true;
				} elseif (M_Debug::getDebugMode() && $file->getLastModifiedDate()->isBefore($defaultFile->getLastModifiedDate())) {
					$copy = true;
				}
				if($copy) {
					$defaultFile->copy($file->getParent());
				}
			}
			
			// We (try to) parse the XML file into an instance of SimpleXmlElement:
			try {
				$this->_xml = @simplexml_load_file($file->getPath());
			}
			// If anything goes wrong, we throw an exception to inform
			// about the failure to parse the definition file.
			catch(Exception $e) {
				throw new M_ApplicationException(sprintf(
					'Could not parse Admin Definition %s of module %s (%s)',
					$file->getPath(),
					$this->_module->getId(),
					$e->getMessage()
				));
			}
		}
		
		// Return the XML
		return $this->_xml;
	}
	
	/**
	 * Get config, from SimpleXMLElement instance
	 * 
	 * Will provide with an instance of {@link M_Config}, based on a given
	 * SimpleXMLElement instance.
	 * 
	 * NOTE:
	 * If the {@link M_Config} instance could not have been constructed,
	 * this method will return NULL instead
	 * 
	 * @access protected
	 * @param string $xpath
	 * 		The xpath expression of which the result is used to construct
	 * 		the structure and data in the {@link M_Config} instance.
	 * @return M_Config|null
	 */
	protected function _getConfigFromSimpleXMLElement(SimpleXMLElement $element) {
		// Parse an M_Config object out of the xml element:
		return new M_Config($this->_getConfigAssocArrayFromXmlElement($element));
	}
	
	/**
	 * Get config, from xpath
	 * 
	 * Will provide with an instance of {@link M_Config}, based on the
	 * result of an xpath query. The xpath query is performed on the 
	 * the result of {@link AdminDefinition::_getXml()}.
	 * 
	 * NOTE:
	 * If the {@link M_Config} instance could not have been constructed,
	 * this method will return NULL instead
	 * 
	 * @access protected
	 * @param string $xpath
	 * 		The xpath expression of which the result is used to construct
	 * 		the structure and data in the {@link M_Config} instance.
	 * @return M_Config|null
	 */
	protected function _getConfigFromXpath($xpath) {
		// Run the Xpath Expression:
		$rs = $this->_getXml()->xpath($xpath);
		
		// If the result of the Xpath Expression is valid:
		if($rs !== FALSE && count($rs) == 1) {
			// Parse an M_Config object out of the xml element:
			return new M_Config($this->_getConfigAssocArrayFromXmlElement($rs[0]));
		}
		
		// If not valid, return NULL:
		return null;
	}
	
	/**
	 * Parse an associative array out of an XML Node
	 * 
	 * Used by {@link AdminFormDefinition::_getConfigFromXpath()} to 
	 * extract the data structure from the original XML file/string.
	 * 
	 * IMPORTANT NOTE:
	 * Will also respond to special notes, such as "populateWithDataObjects"
	 * 
	 * @access protected
	 * @param SimpleXMLElement $node
	 * 		The XML node
	 * @param array $parent
	 *		The parent element, in the outputted array (only for recursive use,
	 *		not to be used in calls from other functions)
	 * @return array $array
	 * 		An associative array
	 */
	protected function _getConfigAssocArrayFromXmlElement(SimpleXMLElement $node, & $parent = array()) {
		// Get the children of the node:
		$children = $node->children();
		
		// If at least 1 child is available in the node:
		if(count($children) > 0) {
			// Prepare output array
			$output = array();
			
			// For each of the childs:
			foreach($children as $child) {
				// The way in which we interpret the current node, depends on its name:
				switch($child->getName()) {
					// Populate with data objects:
					case 'populateWithDataObjects':
						// If populations have been enabled:
						if($this->getEnablePopulations()) {
							// Get options
							foreach($this->_getPopulationDataObjects($child) as $v => $l) {
								$output[$v] = $l;
							}

							// We do not localize the options
							$parent['localizeOptions'] = 0;
						}
						// If populations are disabled:
						else {
							// $output = array();
						}

						// Done :)
						break;
					
					// Populate with callback:
					case 'populateWithCallback':
						// If populations have been enabled:
						if($this->getEnablePopulations()) {
							// Get options
							foreach($this->_getPopulationCallback($child) as $v => $l) {
								$output[$v] = $l;
							}

							// We do not localize the options
							$parent['localizeOptions'] = 0;
						}
						// If populations are disabled:
						else {
							// $output = array();
						}

						// Done :)
						break;
					
					// Regular node:
					default:
						$output[$this->_getXmlElementName($child)] = $this->_getConfigAssocArrayFromXmlElement($child, $output);
						break;
				}
			}
			
			// Return output array
			return $output;
		}
		// If no childs, return the value of the node:
		else {
			return $this->_getXmlElementValue($node);
		}
	}
	
	/**
	 * Get XML Element name
	 * 
	 * @access protected
	 * @param SimpleXMLElement $node
	 * 		The XML node
	 * @return string $name
	 */
	protected function _getXmlElementName(SimpleXMLElement $node) {
		return strtr($node->getName(), array(
			'BOOLEAN_YES_INTEGER' => 1,
			'BOOLEAN_NO_INTEGER'  => 0
		));
	}
	
	/**
	 * Get XML Element value
	 * 
	 * Will parse the value of an XML Element. Is used by other methods in the
	 * {@link AdminListDefinition} class, in order to get the value of a given
	 * node in the XML. This method will recognize special syntaxes, as they are
	 * defined by {@link AdminDefinition::addDataObject()}
	 * 
	 * @access protected
	 * @param SimpleXMLElement $node
	 * 		The XML node
	 * @return mixed $value
	 */
	protected function _getXmlElementValue(SimpleXMLElement $node) {
		// Get the original value
		$value = trim((string) $node);
		
		// For each of the data objects that have been added to the definition:
		foreach($this->_dataObjectValues as $objectName => $objectValues) {
			// For each of the values in the current data object:
			foreach($objectValues as $objectValueName => $objectValue) {
				// Compose the variable name to be replaced:
				$variableName = '$' . $objectName . '.' . $objectValueName;
				
				// If the current value is the only one we need, then we 
				// return the value (maybe we want an array, and not a string
				// replace)
				if($value == $variableName) {
					// Return the value
					return $objectValue;
				}
				
				// If not, we place the value in the original string:
				$value = str_replace(
					// Variable name
					$variableName, 
					// new value
					$objectValue, 
					// The original string
					$value
				);
			}
		}

		// Also, some predefined values:
		$value = strtr(
			$value,
			array(
				'time()'        => time(),
				'uniqueToken()' => M_Helper::getUniqueToken()
			)
		);

		// Now, we check if modifiers have been defined in the value. A modifier
		// starts with the vertical bar (|), and provides arguments with the : symbol,
		// just like smarty.
		$modifiers = explode('|', $value);

		// For each of the modifiers:
		for($i = 1, $n = count($modifiers); $i < $n; $i ++) {
			// Get the modifier function name, and the parameters:
			$params   = explode(':', $modifiers[$i]);
			$function = array_shift($params);

			// Add the value as a parameter:
			array_push($params, $modifiers[0]);

			// Apply the modifier:
			$modifiers[0] = call_user_func_array($function, $params);
		}
		
		// Return value
		return $modifiers[0];
	}
	
	/**
	 * Get population of Data Object Values
	 * 
	 * 
	 */
	protected function _getPopulationDataObjects(SimpleXMLElement $element) {
		// Return value:
		$output = array();
		
		// Check if all required elements have been provided:
		$hasObjectName   = (isset($element->dataObjectId) && !empty($element->dataObjectId));
		$hasValueField   = (isset($element->value) && !empty($element->value));
		$hasLabelField   = (isset($element->label) && !empty($element->label));
		
		// If one of the elements is missing:
		if(! $hasObjectName || ! $hasValueField || !$hasLabelField) {
			throw new M_Exception('Cannot populate with Data Objects; Missing "dataObjectId", "value", or "label"');
		}
		
		// If everything is present:
		// Prepare output array
		$output = array();
		
		// Include empty value?
		if(isset($element->includeEmpty)) {
			$output[(string) $element->includeEmpty] = '-';
		}
		
		// Construct the module:
		if(isset($element->module) && !empty($element->module)) {
			$module = new AdminModule((string) $element->module);
			// Ask the module for the mapper of the requested data object:
			$mapper = $module->getDataObjectMapper((string) $element->dataObjectId);
		}
		// Or, load data object from application:
		else {
			$mapperClassName = ((string) $element->dataObjectId) . M_DataObjectMapper::MAPPER_SUFFIX;
			M_Loader::loadModel($mapperClassName);
			$mapper = new $mapperClassName;
		}
		
		// Has an order been provided?
		$hasOrderField = (isset($element->order) && !empty($element->order));
		if($hasOrderField) {
			// Explode the order fields into an array:
			foreach(explode(',', (string) $element->order) as $order) {
				// Get field and order:
				$order = trim($order);
				$order = explode(' ', $order);
				
				if(! isset($order[1])) {
					$order[1] = 'ASC';
				}
				
				// Add the filter, to order the data objects:
				$mapper->addFilter(new M_DbQueryFilterOrder($order[0], $order[1]));
			}
		}
		
		// Have filters been defined for the population
		foreach($element->xpath('filter') as $filter) {
			// Check presence of required fields
			$hasFilterField      = (isset($filter->field) && !empty($filter->field));
			$hasFilterValue      = (isset($filter->value));
			$hasFilterComparison = (isset($filter->comparison) && !empty($filter->comparison));
			
			// Default comparison to EQ
			if(! $hasFilterComparison) {
				$filter->comparison = 'EQ';
			}
			
			// Throw exception, if missing fields
			if(! $hasFilterField || ! $hasFilterValue) {
				throw new M_Exception(sprintf(
					'Cannot populate with Data Objects; Missing "field" or "value" in definition of filter'
				));
			}
			
			// Add the filter to the mapper:
			$mapper->addFilter(new M_DbQueryFilterWhere((string) $filter->field, $this->_getXmlElementValue($filter->value), (string) $filter->comparison));
		}
		
		// Prepare the getters:
		$value     = (string) $element->value;
		$label     = (string) $element->label;
		$labelType = isset($element->labelType) ? (string) $element->labelType : 'property';
		
		// Has a hierarchy been defined in the population?
		if(isset($element->hierarchy)) {
			// Check presence of required fields for hierarchy:
			// - The field on which to base the hierarchy:
			$hasHierarchyField       = (isset($element->hierarchy->field) && !empty($element->hierarchy->field));
			
			// If not provided
			if(! $hasHierarchyField) {
				// Then, we throw an exception
				throw new M_Exception(sprintf(
					'Cannot populate with Data Objects; Missing "field" definition of hierarchy'
				));
			}
			
			// - The string prefix for children:
			$hasHierarchyChildPrefix = (isset($element->hierarchy->childPrefix));
			if(! $hasHierarchyChildPrefix) {
				$element->hierarchy->childPrefix = '&nbsp;--&nbsp;';
			}
			
			// Add the instances to the output
			$this->_getPopulationDataObjectsOfParent($output, $mapper, $value, $label, $labelType, '--', (string) $element->hierarchy->field);
		}
		// If a hierarchy has not been defined:
		else {
			// Ask the mapper for the instances:
			foreach($mapper->getAll() as $object) {
				// Add the instances to the output array:
				$output[$object->$value] = $this->_getPopulationDataObjectDisplayValue($object, $label, $labelType);
			}
		}

		// Apply maxlength?
		if(isset($element->snippet)) {
			// For each of the elements in the output array
			foreach($output as $key => $value) {
				// Construct a M_Text instance
				$text = new M_Text($value);

				// Set the snippet as new value
				$output[$key] = $text->getSnippet((int) $element->snippet);
			}
		}
		
		// Return the output array:
		return $output;
	}
	
	protected function _getPopulationDataObjectsOfParent(array &$output, M_DataObjectMapper $mapper, $fieldKey, $fieldDisplayValue, $fieldDisplayValueType, $displayValuePrefix, $fieldParent, M_DataObject $parent = NULL) {
		// Get the IDs of the parent:
		$parentIds = $parent ? array($parent->getId()) : array(0, NULL);
		
		// For each parent id
		foreach($parentIds as $parentId) {
			// For each of the child objects 
			/* @var $object M_DataObject */
			foreach($mapper->getByField($fieldParent, $parentId) as $object) {
				// If the object already exists in the output, then we stop here. 
				// We do not want to get caught in an infinite loop
				if(isset($output[$object->$fieldKey])) {
					return;
				}

				// We add the object to the output:
				$output[$object->$fieldKey] = $displayValuePrefix . ' ' . $this->_getPopulationDataObjectDisplayValue($object, $fieldDisplayValue, $fieldDisplayValueType);

				// Also, we add the children of the current object:
				$this->_getPopulationDataObjectsOfParent($output, $mapper, $fieldKey, $fieldDisplayValue, $fieldDisplayValueType, $displayValuePrefix.$displayValuePrefix, $fieldParent, $object);
			}
		}
	}

	protected function _getPopulationDataObjectDisplayValue(M_DataObject $dataObject, $fieldDisplayValue, $fieldDisplayValueType) {
		// Depending on the type of getter:
		switch(strtolower($fieldDisplayValueType)) {
			// property (default):
			case 'getter':
				return $dataObject->$fieldDisplayValue();

			// property (default):
			case 'property':
			default:
				return $dataObject->$fieldDisplayValue;
		}
	}
	
	/**
	 * Get population by callback
	 */
	protected function _getPopulationCallback(SimpleXMLElement $element) {
		// Construct a Callback object
		$callback = new M_Callback();
		
		// Autoload?
		if(isset($element->autoloadFrom) && isset($element->callback)) {
			// Load:
			M_Loader::loadRelative((string) $element->autoloadFrom);
			
			// Set callback definition
			$callback->setCallbackByString((string) $element->callback);
		}
		// Not autoloading
		else {
			// Set callback definition
			$callback->setCallbackByString((string) $element);
		}
		
		// Get the result from the callback:
		return $callback->run();
	}
	
	/**
	 * Get XML File
	 * 
	 * Will provide with the {@link M_File} instance that points to the XML file, 
	 * or at least to where it is supposed to be located. This method does not 
	 * check whether or not the file exists.
	 * 
	 * NOTE:
	 * This method is used by
	 * 
	 * - {@link AdminDefinition::exists()}
	 * - {@link AdminDefinition::_getXml()}
	 * 
	 * Note that, in {@link AdminDefinition}, this method is signed as abstract.
	 * This means that this method MUST be implemented by subclasses!
	 * 
	 * @abstract
	 * @access protected
	 * @return M_File
	 */
	abstract protected function _getXmlFile();
}