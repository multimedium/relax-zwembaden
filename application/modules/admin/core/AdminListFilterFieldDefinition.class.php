<?php
/**
 * AdminListFilterFieldDefinition
 * 
 * Used by {@link AdminListDefinition} to work with filters. Note that this class
 * is an abstract implementation, used as superclass for filters that apply a value
 * to a field.
 *
 * @package App
 * @subpackage Admin
 */
abstract class AdminListFilterFieldDefinition extends AdminListFilterDefinition {
	/**
	 * Field
	 * 
	 * This property stores the field name of the filter. Typically,
	 * the field name is used with the magic getter/setter of an 
	 * {@link M_DataObject} instance.
	 *
	 * @see AdminListFilterDefinition::setField()
	 * @see AdminListFilterDefinition::getField()
	 * @access private
	 * @var string
	 */
	private $_field;
	
	/**
	 * Get field name
	 *
	 * @access public
	 * @return string|null
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 * Set field name
	 *
	 * @access public
	 * @param string $field
	 * 		The new field name of the filter
	 * @return void
	 */
	public function setField($field) {
		$this->_field = (string) $field;
	}
}