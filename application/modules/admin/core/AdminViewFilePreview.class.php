<?php
class AdminViewFilePreview extends M_ViewFilePreview {
	
	/**
	 * @var int
	 */
	protected $_width;
	
	/**
	 * @var int
	 */
	protected $_height;
	
	/**
	 * Preview-height
	 * 
	 * There's no need to set the height, by default 150px will be used to 
	 * preview files. If you wish you can modify the height by using 
	 * {@see AdminViewFilePreview::setHeight()}
	 * 
	 * @return int
	 */
	public function getHeight() {
		return (is_null($this->_height)) ? 150 : $this->_height;
	}
	
	/**
	 * @param int $_height
	 */
	public function setHeight($_height) {
		$this->_height = $_height;
	}
	
	/**
	 * Preview-width
	 * 
	 * There's no need to set the width, by default 150px will be used to 
	 * preview files. If you wish you can modify the width by using 
	 * {@see AdminViewFilePreview::setWidth()}
	 * 
	 * @return int
	 */
	public function getWidth() {
		return (is_null($this->_width)) ? 150 : $this->_width;
	}
	
	/**
	 * @param int $_width
	 */
	public function setWidth($_width) {
		$this->_width = $_width;
	}
	
	/**
	 * Default HTML source code rendering
	 * 
	 * @see M_ViewCoreHtml::getHtml()
	 * @access protected
	 * @return string
	 */
	protected function getHtml() {
		// Start container
		$html  = '<div class="file-preview" id="' . $this->getId() . '">';
		
		// How the HTML is rendered, depends on the file type (extension):
		switch($this->getVariable('extension')) {
			
			case 'jpg':
			case 'jpeg':
			case 'gif':
			case 'png':
				//create a link only if necesarry
				if ($this->getCreateLink()) 
					$html .= '<a class="fancybox" rel="filePreview" href="'. $this->getVariable('link') .'">';
				
					
				$html .= '<img class="'.$this->getClass().'" src="'. M_Request::getLink('thumbnail/resized/'.$this->getWidth().'/'.$this->getHeight().'/'.M_Loader::getRelative($this->_file->getPath())) .'" alt="'. htmlentities($this->getVariable('title')) .'" border="0" />';

				//close link only if necessary
				if ($this->getCreateLink()) 
					$html .= '</a>';
				
				if($this->_description) {
					$html .= '<p>';
					$html .=    $this->_description;
					$html .= '</p>';
				}
				break;
			
			case 'doc':
			case 'docx':
			case 'xls':
			case 'csv':
			case 'pdf':
			case 'zip':
			case 'rar':
			default:
				$html .= '<div class="file-'. $this->getVariable('extension') .'">';
				$html .=    '<a href="'. $this->getVariable('link') .'">';
				$html .=       $this->getVariable('title');
				$html .=    '</a>';
				$html .= '</div>';
				
				if($this->_description) {
					$html .= '<p>';
					$html .=    $this->_description;
					$html .= '</p>';
				}
				break;
		}
		
		// Finish container:
		$html .= '</div>';
		
		// return final render:
		return $html;
	}
}