<?php
/**
 * AdminListFilterWhereDefinition
 *
 * @package App
 * @subpackage Admin
 */
class AdminListFilterWhereDefinition extends AdminListFilterFieldDefinition {
	/**
	 * Supported Comparison Operators
	 * 
	 * This property stores the comparisons that can be applied by the filter (e.g.
	 * EQ, NEQ, LT, LTEQ, etc...)
	 * 
	 * @access private
	 * @var array
	 */
	private $_comparisonOperators = array();
	
	/**
	 * Comparison operator
	 * 
	 * This property stores the comparison operator that should be applied in the
	 * filter
	 * 
	 * @access private
	 * @var string
	 */
	private $_comparisonOperator;
	
	/**
	 * Comparison operators' fields
	 * 
	 * This property stores the {@link M_Field} instances that are used in
	 * combination with the filter's comparison operators.
	 * 
	 * @access private
	 * @var array
	 */
	private $_comparisonOperatorFields = array();
	
	/**
	 * Get comparison operators
	 * 
	 * Will return with the collection of operators that is supported by the 
	 * filter's definition. Note that the keys of the returned iterator are the
	 * operators, and the values are the titles of the operator:
	 * 
	 * Example 1
	 * <code>
	 *    // Construct a new filter definition:
	 *    $f = new AdminListFilterDefinition('Title');
	 *    $f->addSupportedComparisonOperator(M_DbQueryFilterWhere::EQ, 'equals');
	 *    $f->addSupportedComparisonOperator(M_DbQueryFilterWhere::CONTAINS, 'contains the text');
	 *    
	 *    // Get the comparisons:
	 *    foreach($f->getSupportedComparisonOperators() as $operator => $title) {
	 *       echo $operator . ': ' . $title;
	 *    }
	 * </code>
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getSupportedComparisonOperators() {
		return new ArrayIterator($this->_comparisonOperators);
	}
	
	/**
	 * Get comparison operator
	 * 
	 * Will return the comparison operator that may have been set previously
	 * with {@link AdminListFilterWhereDefinition::setComparisonOperator()}
	 * 
	 * @access public
	 * @return string
	 */
	public function getComparisonOperator() {
		return $this->_comparisonOperator;
	}
	
	/**
	 * Is a comparison operator?
	 * 
	 * Will check if a given comparison operator is supported by the current
	 * instance of {@link AdminListFilterDefinition}.
	 * 
	 * @access public
	 * @param string $operator
	 * 		The comparison operator for which to check if supported
	 * @return bool $flag
	 * 		Returns TRUE if supported, FALSE if not
	 */
	public function isComparisonOperatorSupported($operator) {
		return isset($this->_comparisonOperators[(string) $operator]);
	}
	
	/**
	 * Get field, for a given comparison operator
	 * 
	 * This method will provide with the field that has been defined in the filter
	 * definition, for a given comparison operator.
	 * 
	 * Note that, if no specific field has been defined for the operator, a text
	 * field will be provided by default.
	 * 
	 * Note that, if the comparison operator is not supported by the filter
	 * definition, this method will return NULL!
	 * 
	 * @access public
	 * @return M_Field
	 */
	public function getFieldForComparisonOperator($operator) {
		if(! $this->isComparisonOperatorSupported($operator)) {
			return NULL;
		}
		if(isset($this->_comparisonOperatorFields[(string) $operator])) {
			return $this->_comparisonOperatorFields[(string) $operator];
		} else {
			return new M_FieldText('where-' . M_Helper::getUniqueToken());
		}
	}
	
	/**
	 * Add supported comparison operator
	 * 
	 * @see AdminListFilterWhereDefinition::getSupportedComparisonOperators();
	 * @access public
	 * @param string $operator
	 * 		The comparison operator for which to add support
	 * @param string $title
	 * 		The title of the operator (for display)
	 * @param M_Field $field
	 * 		The field that is to be used to in combination with this comparison
	 * 		operator
	 * @return void
	 */
	public function addSupportedComparisonOperator($operator, $title, M_Field $field = NULL) {
		$this->_comparisonOperators[(string) $operator] = (string) $title;
		if($field) {
			$this->_comparisonOperatorFields[(string) $operator] = $field;
		}
	}
	
	/**
	 * Set comparison operator
	 * 
	 * Will set the comparison operator that should be applied in the filter. Note
	 * that this method will throw an exception, if the comparison operator is not
	 * supported by the filter.
	 * 
	 * {@link AdminListFilterWhereDefinition::addSupportedComparisonOperator()}
	 * can be used to add support for given comparison operators to the filter.
	 * 
	 * @throws M_Exception
	 * @access public
	 * @param string $operator
	 * 		The comparison operator to be applied by the filter
	 * @return void
	 */
	public function setComparisonOperator($operator) {
		$operator = (string) $operator;
		if($this->isComparisonOperatorSupported($operator)) {
			$this->_comparisonOperator = $operator;
		} else {
			throw new M_Exception(sprintf(
				'Filter Comparison Operator "%s" is not supported; supported operators are: %s',
				$operator,
				implode(array_keys($this->_comparisonOperators))
			));
		}
	}
	
	/* -- Implementations for abstract methods; defined in AdminListFilterDefinition -- */
	
	/**
	 * Get {@link M_DbQueryFilter}
	 * 
	 * @see AdminListFilterDefinition::getDbQueryFilter()
	 * @access public
	 * @return M_DbQueryFilterWhere
	 */
	public function getDbQueryFilter() {
		// Make sure the operator has been provided
		if(! $this->_comparisonOperator) {
			throw new M_Exception(sprintf(
				'Cannot apply filter; Please provide comparison operator to %s::%s()',
				__CLASS__,
				'setComparisonOperator'
			));
		}
		
		// Construct the filter
		return new M_DbQueryFilterWhere($this->getField(), $this->getValue(), $this->getComparisonOperator());
	}
	
	/**
	 * Get {@link AdminSmartFolderFilter}
	 * 
	 * @see AdminListDefinition::getAdminSmartFolderFilter()
	 * @access public
	 * @return AdminSmartFolderFilter
	 */
	public function getAdminSmartFolderFilter() {
		M_Loader::loadDataObject('AdminSmartFolderFilter', 'admin');
		
		$f = new AdminSmartFolderFilter();
		$f->setListFilterDefinitionIndex($this->getIndex());
		$f->setComparison($this->getComparisonOperator());
		$f->setValue($this->getValue());
		return $f;
	}
	
	/**
	 * Get type of filter
	 * 
	 * @see AdminListDefinition::getType()
	 * @access public
	 * @return string
	 */
	public function getType() {
		return 'where';
	}
}