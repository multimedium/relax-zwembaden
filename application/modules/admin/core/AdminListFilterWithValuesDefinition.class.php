<?php
/**
 * AdminListFilterWithValuesDefinition
 *
 * @package App
 * @subpackage Admin
 */
abstract class AdminListFilterWithValuesDefinition extends AdminListFilterDefinition {
	/**
	 * Supported values
	 * 
	 * This property stores the filter values that may be applied. Typically,
	 * this stores the menu options from which the user may pick a value, in order
	 * to apply the filter.
	 * 
	 * Read the docs on 
	 * 
	 * - {@link AdminListFilterDefinition::getSupportedValues()}
	 * - {@link AdminListFilterDefinition::setSupportedValue()}
	 * 
	 * for more info.
	 * 
	 * @access protected
	 * @var array
	 */
	protected $_values;
	
	/**
	 * Get supported values
	 * 
	 * Will provide with the values that may be applied in this filter or, in other
	 * words, will provide with the values that are supported by this filter.
	 * Typically, the supported values that are returned by this method, are
	 * used to create a menu from which the user can pick a filter value in order
	 * to apply the filter.
	 * 
	 * @access public
	 * @return ArrayIterator $iterator
	 * 		The collection of supported values, of which the keys are the values
	 * 		and the values are the labels for display
	 */
	public function getSupportedValues() {
		return new ArrayIterator($this->_values);
	}
	
	/**
	 * Is supported value?
	 * 
	 * Will check whether or not the given value (a key in the collection of
	 * values; see {@link AdminListFilterWithValuesDefinition::getSupportedValues()})
	 * is a supported value.
	 * 
	 * @access public
	 * @param mixed $value
	 * 		The value to look up
	 * @return bool $flag
	 * 		Returns TRUE if the value is supported, FALSE if not
	 */
	public function isSupportedValue($value) {
		return (isset($this->_values[$value]));
	}
	
	/**
	 * Set supported value
	 * 
	 * Sets/adds a supported value to the collection of supported values.
	 * 
	 * @access public
	 * @param mixed $value
	 * 		The value of the supported value
	 * @param string $label
	 * 		The label, for display, of the supported value
	 * @return void
	 */
	public function setSupportedValue($value, $label) {
		$this->_values[$value] = (string) $label;
	}
	
	/**
	 * Set filter value
	 *
	 * NOTE:
	 * Will throw an exception, if the filter value is not a supported filter 
	 * value. For more info on supported filter values, read the docs on:
	 * 
	 * {@link AdminListFilterWithValuesDefinition::$_values}
	 * {@link AdminListFilterWithValuesDefinition::getSupportedValues()}
	 * {@link AdminListFilterWithValuesDefinition::isSupportedValue()}
	 * {@link AdminListFilterWithValuesDefinition::setSupportedValue()}
	 * 
	 * @throws M_Exception
	 * @see AdminListFilterDefinition::setValue()
	 * @access public
	 * @param mixed $value
	 * 		The new value of the filter
	 * @return void
	 */
	public function setValue($value) {
		// Check if the value is supported
		if(! $this->isSupportedValue($value)) {
			throw new M_Exception(sprintf(
				'Cannot set filter value "%s": not in supported values',
				(string) $value
			));
		}
		
		// Set the value:
		$this->_value = $value;
	}
}