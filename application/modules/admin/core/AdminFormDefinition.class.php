<?php
/**
 * AdminFormDefinition
 *
 * @package App
 * @subpackage Admin
 */
class AdminFormDefinition extends AdminDefinition {

	/**
	 * Holds the form
	 *
	 * @author Ben Brughmans
	 * @var M_Form
	 */
	private $_form;

	/**
	 * Get title
	 * 
	 * @access public
	 * @return string|null
	 */
	public function getTitle() {
		$rs = $this->_getXml()->xpath('title');
		if($rs) {
			return (string) $rs[0];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get description
	 * 
	 * @access public
	 * @return string|null
	 */
	public function getDescription() {
		$rs = $this->_getXml()->xpath('description');
		if($rs) {
			return (string) $rs[0];
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get form
	 * 
	 * Will provide with the constructed {@link M_Form}, as defined by 
	 * the form definition.
	 * 
	 * @access public
	 * @return AdminFormDataObject
	 */
	public function getForm() {
		if (is_null($this->_form)) {
			// Get the M_Config to mount the form
			$config = $this->_getConfigFromXpath('form');
			
			// If the config has been constructed successfully
			if($config) {
				// The form must be a subclass of:
				$parentClass = 'AdminFormDataObject';

				// Load the parent form class:
				M_Loader::loadForm($parentClass, 'admin');

				// We make sure that the Form Type has been set. If not set,
				// we default it to AdminFormDataObject
				if(! isset($config->type)) {
					$config->type = $parentClass;
				}

				// Mount the form with the Form Factory :)
				$form = M_Form::factory($this->getModule()->getId(), $config, TRUE);

				// We make sure that the form is a subclass of AdminFormDataObject.
				// We need to check this, because we have to be able to
				// provide the form with an instance of M_DataObject
				if($form instanceof AdminFormDataObject) {
					$this->_form = $form;
				}
				// If the form is not a subclass of the AdminFormDataObject
				// class, we throw an exception
				else {
					throw new M_Exception(sprintf(
						'The form class must be a subclass of %s',
						$parentClass
					));
				}
			}
			else {
				// If we're still here, we have been unable to construct the
				// form. In that case, we return a default form:
				$this->_form = new AdminFormDataObject($this->_module->getId());
			}
		}

		return $this->_form;
	}
	
	/**
	 * Get fields
	 * 
	 * Will provide with the collection of constructed {@link M_Field} 
	 * instances that have been defined in the form definition.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getFields() {
		
	}
	
	/**
	 * Get field
	 * 
	 * @access public
	 * @param string $id
	 * 		The ID of the field
	 * @return M_Field|null
	 */
	public function getField($id) {
		$config = $this->_getConfigFromXpath('form/fields/*' . $id);
		if($config) {
			return M_Field::factory($id, $config);
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get field names
	 * 
	 * Will return with an iterator of fields in the definition. The keys of 
	 * the iterator are the fields, and the values are the fields' display names.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getFieldNames() {
		$out = array();
		
		// Run the Xpath Expression:
		$rs = $this->_getXml()->xpath('form/fields');
		
		// If the result of the Xpath Expression is valid:
		if($rs !== FALSE && count($rs) == 1) {
			// For each of the fields:
			foreach($rs[0] as $row) {
				// Add the field to the output:
				$out[(string) $row->getName()] = (string) $row->title;
			}
		}
		
		// If not valid, return NULL:
		return new ArrayIterator($out);
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Get XML File
	 * 
	 * Will provide with the {@link M_File} instance that points to the
	 * XML file, or at least to where it is supposed to be located.
	 * This method does not check whether or not the file exists.
	 * 
	 * @access public
	 * @return M_File
	 */
	protected function _getXmlFile() {
		return AdminLoader::getXmlSpecsFile(
			$this->getModule()->getDataObjectClassName($this->getDataObjectId()) . 'Form.xml',
			$this->getModule()->getId()
		);
	}
}