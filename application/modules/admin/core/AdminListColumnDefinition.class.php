<?php
/**
 * AdminListColumnDefinition
 * 
 * Used by {@link AdminListDefinition} to work with columns.
 *
 * @package App
 * @subpackage Admin
 */
class AdminListColumnDefinition {
	/**
	 * Index number
	 * 
	 * This property stores the column's index number. This index number
	 * defines the position in which the column is rendered in the view.
	 * This index number starts at 0 (ZERO). In other words, the first
	 * column carries index number 0.
	 * 
	 * NOTE:
	 * The default value of this property is 0
	 * 
	 * @access private
	 * @var integer
	 */
	private $_index = 0;
	
	/**
	 * Title
	 * 
	 * This property stores the title of the column.
	 *
	 * @access private
	 * @var string
	 */
	private $_title;
	
	/**
	 * Field
	 * 
	 * This property stores the field name of the column. Typically,
	 * the field name is used with the magic getter/setter of an 
	 * {@link M_DataObject} instance.
	 *
	 * @access private
	 * @var string
	 */
	private $_field;
	
	/**
	 * Sortable?
	 * 
	 * This property will store the boolean flag that tells whether
	 * or not the column can be used to sort the listed instances.
	 * 
	 * NOTE:
	 * The default value of this property is (boolean) FALSE
	 *
	 * @access private
	 * @var boolean
	 */
	private $_sortable = FALSE;
	
	/**
	 * Width
	 * 
	 * This property stores the width of the column
	 * 
	 * @access private
	 * @var string|integer
	 */
	private $_width;
	
	/**
	 * Type
	 * 
	 * This property stores the type of the column.
	 * 
	 * @access private
	 * @var string
	 */
	private $_type;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $title
	 * 		The title of the column
	 * @param integer $index
	 * 		The index number of the column
	 * @return AdminListColumnDefinition
	 */
	public function __construct($title, $index = 0, $width = NULL) {
		$this->setTitle($title);
		$this->setIndex($index);
		if($width) {
			$this->setWidth($width);
		}
	}
	
	/**
	 * Get index number
	 * 
	 * @see AdminListColumnDefinition::$_index
	 * @access public
	 * @return integer
	 */
	public function getIndex() {
		return $this->_index;
	}
	
	/**
	 * Get title
	 *
	 * @access public
	 * @return string
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 * Get field name
	 *
	 * @access public
	 * @return string|null
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 * Is sortable?
	 * 
	 * Will tell if the column can be used to sort listed instances.
	 * Returns TRUE if the column can be used, FALSE if not.
	 *
	 * @access public
	 * @return boolean
	 */
	public function isSortable() {
		return $this->_sortable;
	}
	
	/**
	 * Get width
	 * 
	 * @access public
	 * @return string|integer
	 */
	public function getWidth() {
		return $this->_width;
	}
	
	/**
	 * Get type
	 * 
	 * @access public
	 * @return string
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 * Set index number
	 * 
	 * For more information about the column's index number, read the
	 * docs on {@link AdminListColumnDefinition::$_index}. This method
	 * can be used to set the index number.
	 * 
	 * Note that the index number must be equal to or greater than 0
	 * (ZERO)!
	 * 
	 * @access public
	 * @param integer $index
	 * 		The new index number of the column
	 * @return void
	 */
	public function setIndex($index) {
		if(is_numeric($index) && $index >= 0) {
			$this->_index = (int) $index;
		} else {
			throw new M_Exception(sprinf(
				'A List Column must be assigned a numerical Index Number that is equal to or greater than ZERO'
			));
		}
	}
	
	/**
	 * Set title
	 *
	 * @access public
	 * @param string $title
	 * 		The new title of the column
	 * @return void
	 */
	public function setTitle($title) {
		$this->_title = (string) $title;
	}
	
	/**
	 * Set field name
	 *
	 * @access public
	 * @param string $field
	 * 		The new field name of the column
	 * @return void
	 */
	public function setField($field) {
		$this->_field = (string) $field;
	}
	
	/**
	 * Set flag: Is sortable?
	 *
	 * @see AdminListColumnDefinition::isSortable()
	 * @access public
	 * @param boolean $flag
	 * 		TRUE if the column can be used to sort instances, FALSE
	 * 		if not
	 * @return void
	 */
	public function setIsSortable($flag) {
		$this->_sortable = (bool) $flag;
	}
	
	/**
	 * Set width
	 * 
	 * @access public
	 * @param string|integer $width
	 * 		The width of the column
	 * @return void
	 */
	public function setWidth($width) {
		$this->_width = $width;
	}
	
	/**
	 * Set type
	 * 
	 * @access public
	 * @param string $type
	 * 		The type of the column
	 * @return void
	 */
	public function setType($type) {
		$this->_type = $type;
	}
}