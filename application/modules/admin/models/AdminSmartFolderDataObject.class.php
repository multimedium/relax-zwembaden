<?php
/** 
 * AdminSmartFolderDataObject class
 * 
 * @see AdminSmartFolderMapper
 * @package App
 */
class AdminSmartFolderDataObject extends M_DataObject {
	/**
	 * id
	 * 
	 * This property stores the id. To get/set the value of the id, read the
	 * documentation on 
	 * 
	 * - {@link AdminSmartFolderDataObject::getId()}
	 * - {@link AdminSmartFolderDataObject::setId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_id;
	
	/**
	 * moduleId
	 * 
	 * This property stores the moduleId. To get/set the value of the
	 * moduleId, read the documentation on 
	 * 
	 * - {@link AdminSmartFolderDataObject::getModuleId()}
	 * - {@link AdminSmartFolderDataObject::setModuleId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_moduleId;
	
	/**
	 * dataObjectId
	 * 
	 * This property stores the dataObjectId. To get/set the value of the
	 * dataObjectId, read the documentation on 
	 * 
	 * - {@link AdminSmartFolderDataObject::getDataObjectId()}
	 * - {@link AdminSmartFolderDataObject::setDataObjectId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_dataObjectId;
	
	/**
	 * administratorId
	 * 
	 * This property stores the administratorId. To get/set the value of the
	 * administratorId, read the documentation on 
	 * 
	 * - {@link AdminSmartFolderDataObject::getAdministratorId()}
	 * - {@link AdminSmartFolderDataObject::setAdministratorId()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_administratorId;
	
	/**
	 * name
	 * 
	 * This property stores the name. To get/set the value of the name, read
	 * the documentation on 
	 * 
	 * - {@link AdminSmartFolderDataObject::getName()}
	 * - {@link AdminSmartFolderDataObject::setName()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_name;
	
	/**
	 * description
	 * 
	 * This property stores the description. To get/set the value of the
	 * description, read the documentation on 
	 * 
	 * - {@link AdminSmartFolderDataObject::getDescription()}
	 * - {@link AdminSmartFolderDataObject::setDescription()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_description;
	
	/**
	 * operator
	 * 
	 * This property stores the operator. To get/set the value of the
	 * operator, read the documentation on 
	 * 
	 * - {@link AdminSmartFolderDataObject::getOperator()}
	 * - {@link AdminSmartFolderDataObject::setOperator()}
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $_operator;
	
	/**
	 * Get id
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 * Set id
	 * 
	 * @access public
	 * @param mixed $id
	 * @return mixed
	 */
	public function setId($id) {
		$this->_id = $id;
	}
	
	/**
	 * Get moduleId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getModuleId() {
		return $this->_moduleId;
	}
	
	/**
	 * Set moduleId
	 * 
	 * @access public
	 * @param mixed $moduleId
	 * @return mixed
	 */
	public function setModuleId($moduleId) {
		$this->_moduleId = $moduleId;
	}
	
	/**
	 * Get dataObjectId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getDataObjectId() {
		return $this->_dataObjectId;
	}
	
	/**
	 * Set dataObjectId
	 * 
	 * @access public
	 * @param mixed $dataObjectId
	 * @return mixed
	 */
	public function setDataObjectId($dataObjectId) {
		$this->_dataObjectId = $dataObjectId;
	}
	
	/**
	 * Get administratorId
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getAdministratorId() {
		return $this->_administratorId;
	}
	
	/**
	 * Set administratorId
	 * 
	 * @access public
	 * @param mixed $administratorId
	 * @return mixed
	 */
	public function setAdministratorId($administratorId) {
		$this->_administratorId = $administratorId;
	}
	
	/**
	 * Get name
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Set name
	 * 
	 * @access public
	 * @param mixed $name
	 * @return mixed
	 */
	public function setName($name) {
		$this->_name = $name;
	}
	
	/**
	 * Get description
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 * Set description
	 * 
	 * @access public
	 * @param mixed $description
	 * @return mixed
	 */
	public function setDescription($description) {
		$this->_description = $description;
	}
	
	/**
	 * Get operator
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getOperator() {
		return $this->_operator;
	}
	
	/**
	 * Set operator
	 * 
	 * @access public
	 * @param mixed $operator
	 * @return mixed
	 */
	public function setOperator($operator) {
		$this->_operator = $operator;
	}
}