<?php
/**
 * AdminSmartFolder class
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
M_Loader::loadDataObject('AdminSmartFolderDataObject', 'admin');

/**
 * AdminSmartFolder class
 * 
 * @package App
 * @subpackage Admin
 */
class AdminSmartFolder extends AdminSmartFolderDataObject {
	/**
	 * Filters 
	 * 
	 * This property stores the collection of filters that is contained by the
	 * smart folder. Each of the filters is represented by an instance of 
	 * {@link AdminSmartFolderFilter}.
	 * 
	 * @access private
	 * @var array
	 */
	private $_filters;
	
	/* -- GETTERS -- */
	
	/**
	 * Get mapper
	 * 
	 * @see M_DataObject::getMapper()
	 * @access public
	 * @return AdminSmartFolderMapper
	 */
	public function getMapper() {
		M_Loader::loadDataObjectMapper('AdminSmartFolder', 'admin');
		return new AdminSmartFolderMapper;
	}
	
	/**
	 * Get Module
	 * 
	 * Will provide with the {@link AdminModule} instance, in which the smart
	 * folder has been created.
	 * 
	 * @access public
	 * @return AdminModule
	 */
	public function getModule() {
		return new AdminModule($this->getModuleId());
	}
	
	/**
	 * Get filters
	 * 
	 * Will provide with the filters in the smart folder. Each filter is represented
	 * by an instance of {@link AdminSmartFolderFilter}.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getFilters() {
		// If not requested before:
		if(! $this->_filters) {
			// Load the AdminSmartFolderFilterMapper class:
			M_Loader::loadDataObjectMapper('AdminSmartFolderFilter', 'admin');
			
			// Construct the mapper:
			$mapper = new AdminSmartFolderFilterMapper();
			$mapper->addFilter(new M_DbQueryFilterOrder('id', 'ASC'));
			
			// Ask the mapper for the filters that belong to this folder
			$this->_filters = $mapper->getBySmartFolder($this)->getArrayCopy();
		}
		
		// Return filters:
		return new ArrayIterator($this->_filters);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Add filter
	 * 
	 * Adds a filter to the smart folder. The filter is represented by an instance
	 * of {@link AdminSmartFolderFilter}.
	 * 
	 * @access public
	 * @return void
	 */
	public function addFilter(AdminSmartFolderFilter $filter) {
		// If filters not populated yet:
		if(! $this->_filters) {
			$this->getFilters();
		}
		
		// Set a reference to the smart folder, in the filter:
		$filter->setFolderId($this->getId());
		
		// Add filter:
		$this->_filters[] = $filter;
	}
	
	/**
	 * Remove all filter
	 * 
	 * Will remove all filters in the smart folder
	 * 
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function clearFilters() {
		// For each of the attached filters:
		foreach($this->getFilters() as $i => $filter) {
			// Remove the filter:
			if(! $filter->delete()) {
				// If failed to remove filter, return FALSE
				return FALSE;
			}
			
			// Also remove the filter from internal array
			unset($this->_filters[$i]);
		}
		
		// Return success:
		return TRUE;
	}
}