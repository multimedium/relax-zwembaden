<?php
/**
 * AdminSmartFolderFilterMapper class
 * 
 * @package App
 * @subpackage Admin
 */
class AdminSmartFolderFilterMapper extends M_DataObjectMapper {
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'admin';
	}
	
	/**
	 * Get by smart folder
	 * 
	 * Will provide with the collection of filters that belong to a given
	 * {@link AdminSmartFolder}.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getBySmartFolder(AdminSmartFolder $smartFolder) {
		return $this->_fetchAll(
			$this
				->_getFilteredSelect()
				->where($this->getField('folderId')->db_name . ' = ?', $smartFolder->getId())
				->execute()
		);
	}
}