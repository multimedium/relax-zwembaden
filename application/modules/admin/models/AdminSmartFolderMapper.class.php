<?php
/**
 * AdminSmartFolderMapper class
 * 
 * @package App
 * @subpackage Admin
 */
class AdminSmartFolderMapper extends M_DataObjectMapper {
	/**
	 * Insert object into database
	 * 
	 * @see M_DataObjectMapper::insert()
	 * @access public
	 * @param M_DataObject $object
	 * 		The Smart Folder that is to be saved to the database
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function insert(M_DataObject $object) {
		// First, save the folder to the database:
		if(parent::insert($object)) {
			// Save the filters to the database
			return $this->_saveFiltersOfFolder($object);
		}
		
		// Return FALSE, if here
		return FALSE;
	}
	
	/**
	 * Update object in database
	 * 
	 * @see M_DataObjectMapper::update()
	 * @access public
	 * @param M_DataObject $object
	 * 		The Smart Folder that is to be saved to the database
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function update(M_DataObject $object) {
		// First, save the changes to the folder:
		if(parent::update($object)) {
			// Save the filters to the database
			return $this->_saveFiltersOfFolder($object);
		}
		
		// Return FALSE, if here
		return FALSE;
	}
	
	/**
	 * Delete object from database
	 * 
	 * @see M_DataObjectMapper::delete()
	 * @access public
	 * @param M_DataObject $object
	 * 		The Smart Folder that is to be saved to the database
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	public function delete(M_DataObject $object) {
		// For each of the filters, contained by the smart folder:
		foreach($object->getFilters() as $filter) {
			// Delete the filter:
			$filter->delete();
		}
		
		// Delete the smart folder:
		return parent::delete($object);
	}
	
	/**
	 * Get by administrator
	 * 
	 * Will provide with the collection of smart folders that belong to a given
	 * {@link Administrator}.
	 * 
	 * @access public
	 * @return ArrayIterator
	 */
	public function getByAdministrator(Administrator $administrator) {
		return $this->_fetchAll(
			$this
				->_getFilteredSelect()
				->where($this->getField('administratorId')->db_name . ' = ?', $administrator->getId())
				->execute()
		);
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * _getModuleName
	 * 
	 * @access protected
	 * @return void
	 */
	protected function _getModuleName() {
		return 'admin';
	}
	
	/**
	 * Save attached filters
	 * 
	 * @access protected
	 * @param AdminSmartFolder $folder
	 * @return bool $flag
	 */
	protected function _saveFiltersOfFolder(AdminSmartFolder $folder) {
		// First, we delete the filters that may have been attached to the folder:
		foreach($folder->getFilters() as $filter) {
			if(! $filter->delete()) {
				return FALSE;
			}
		}
		
		// Now, we save the filters to the database:
		foreach($folder->getFilters() as $filter) {
			$filter->insert();
		}
		
		// Return result:
		return TRUE;
	}
}