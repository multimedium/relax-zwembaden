<?php
/**
 * AdminSmartFolderFilter class
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
M_Loader::loadDataObject('AdminSmartFolderFilterDataObject','admin');

/**
 * AdminSmartFolderFilter class
 * 
 * @package App
 * @subpackage Admin
 */
class AdminSmartFolderFilter extends AdminSmartFolderFilterDataObject {
	
	/**
	 * Get mapper
	 * 
	 * @see M_DataObject::getMapper()
	 * @access public
	 * @return AdminSmartFolderMapper
	 */
	public function getMapper() {
		M_Loader::loadDataObjectMapper('AdminSmartFolderFilter', 'admin');
		return new AdminSmartFolderFilterMapper;
	}
	
	/**
	 * Get {@link M_DbQueryFilter}
	 * 
	 * NOTE:
	 * If the {@link M_DbQueryFilter} could not have been constructed, this
	 * method will return NULL instead.
	 * 
	 * @access public
	 * @return M_DbQueryFilter
	 */
	public function getDbQueryFilter() {
		// Get the definition of the filter:
		$definition = $this->getListFilterDefinition();
		
		// If the definition has been found:
		if(! $definition) {
			return NULL;
		}
		
		// Return the DB Query Filter:
		return $definition->getDbQueryFilter();
	}
	
	/**
	 * Set the value for this smartfolder-filter
	 *
	 * @param mixed $value
	 * @return AdminSmartFolderFilter
	 */
	public function setValue($value) {
		//check if this is a {@link M_Date} value, if so we need to update
		//the value to one we can use for later use. If we don't do this the
		//{@link M_Date::toString()} function will be called and this may
		//result in a relative-string such as "today"
		if ($value instanceof M_Date) {
			$value = $value->getMysqlDate();
		}
		//set the value
		parent::setValue($value);
		
		return $this;
	}
	
	/**
	 * Get {@link AdminListFilterDefinition}
	 * 
	 * Will provide with a definition of the filter, represented by an instance
	 * of {@link AdminListFilterDefinition} (subclass)
	 * 
	 * NOTE:
	 * If the list definition could not have been constructed, this method will
	 * return NULL instead.
	 * 
	 * @access public
	 * @return AdminListFilterDefinition
	 */
	public function getListFilterDefinition() {
		// Load the AdminSmartFolder model:
		M_Loader::loadDataObjectMapper('AdminSmartFolder', 'admin');
		
		// Get the folder mapper, to select the folder to which the filter belongs:
		$mapper = new AdminSmartFolderMapper();
		
		// Get the folder to which the filter belongs:
		$folder = $mapper->getById($this->getFolderId());
		
		// If the folder has not been found:
		if(! $folder) {
			return NULL;
		}
		
		// If the folder does not yet define the module:
		if(! $folder->getModuleId() || ! $folder->getDataObjectId()) {
			return NULL;
		}
		
		// Construct the list definition, based on the folder's properties:
		$definition = new AdminListDefinition(new AdminModule($folder->getModuleId()), $folder->getDataObjectId());
		
		// Does the list definition exist?
		if(! $definition->exists()) {
			return NULL;
		}
		
		// Ask the definition for the filter at given index:
		$filterDefinition = $definition->getFilter($this->getListFilterDefinitionIndex());
		
		// The way in which we pass properties to the filter definition, depends
		// on the type of the filter:
		switch($filterDefinition->getType()) {
			// If the filter is of type "where", we also add the comparison operator
			// to the definition:
			case 'where':
				/* @var $filterDefinition AdminListFilterWhereDefinition */
				// We get the filter value's field, for the comparison operator:
				$field = $filterDefinition->getFieldForComparisonOperator($this->getComparison());

				// If a field can be constructed:
				if($field) {
					// Then, set its default value, to the filter value
					$field->setDefaultValue($this->getValue());

					// Get the value, by delivering with the field
					$value = $field->deliver();

					// Update the filter definition:
					$filterDefinition->setComparisonOperator($this->getComparison());
					$filterDefinition->setValue($value);
				}
				// If a field cannot be constructed for the operator:
				else {
					// Then, we return NULL instead
					return NULL;
				}

				// Done :)
				break;
			
			// If the filter is of type "order", we also add the sorting order:
			case 'order':
				$filterDefinition->setValue($this->getValue());
				$filterDefinition->setOrder($this->getComparison());
				break;
			
			// If the filter is of type "limit", we add the count
			case 'limit':
				$filterDefinition->setValue($this->getValue());
				break;
				
		}
		
		// Return the final definition of the filter:
		return $filterDefinition;
	}
}