<?php
/**
 * AdminFormChangePasswordView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminPageView.class.php';

/**
 * AdminFormChangePasswordView
 * 
 * @package App
 * @subpackage Admin
 */
class AdminFormChangePasswordView extends AdminPageView {
	/**
	 * Set change-password form
	 * 
	 * @access public
	 * @param AdminFormChangePassword $form
	 * @return void
	 */
	public function setFormChangePassword(AdminFormChangePassword $form) {
		$this->assign('form', $form);
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminFormChangePassword.tpl');
	}
}