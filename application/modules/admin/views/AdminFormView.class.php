<?php
/**
 * AdminFormView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminPageView.class.php';

/**
 * AdminFormView
 * 
 * @package App
 * @subpackage Admin
 */
class AdminFormView extends AdminPageView {
	/**
	 * Form
	 * 
	 * The {@link M_Form} that is used to handle the requested action,
	 * and used to render the view.
	 * 
	 * @access protected
	 * @var M_Form
	 */
	protected $_form;
	
	/**
	 * Set form
	 * 
	 * @access public
	 * @param M_Form $form
	 * @return void
	 */
	public function setForm(M_Form $form) {
		$this->_form = $form;
	}
	
	/**
	 * Get form
	 * 
	 * @access public
	 * @return M_Form
	 */
	public function getForm() {
		return $this->_form;
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * Magic method, called by {@link M_ViewHtml} to do some preprocessing
	 * of the view, prior to rendering the HTML (template) resource.
	 * 
	 * @access public
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure the form has been assigned:
		if(! $this->_form) {
			throw new M_ViewException(sprintf(
				'No form has been assigned to %s',
				__CLASS__
			));
		} else {
			$this->assign('form', $this->_form);
		}
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminForm.tpl');
	}
}