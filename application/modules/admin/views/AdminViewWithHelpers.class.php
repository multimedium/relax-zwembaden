<?php
/**
 * AdminViewWithHelpers
 * 
 * @package App
 * @subpackage Admin
 */
abstract class AdminViewWithHelpers extends M_ViewHtml {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Helper object(s)
	 * 
	 * This property stores the helper object(s). {@link AdminPageViewWithHelpers}
	 * will ask this helper to add additional stuff to the view. Also, the helper 
	 * can manipulate the rendered HTML. 
	 * 
	 * {@link AdminListViewHelper} is an example of a typical View Helper class
	 * 
	 * @access private
	 * @var object
	 */
	private $_helpers = array();
	
	/**
	 * Auto-discovery of Helper object(s)
	 * 
	 * This property stores the flag that tells whether or not auto-discovery
	 * of view helpers should be enabled. For more info, read the docs on:
	 * 
	 * - {@link AdminViewWithHelpers::setAutoDiscoveryOfViewHelpers()}
	 * - {@link AdminViewWithHelpers::_autoDiscoverViewHelpers()}
	 * 
	 * By default, this property is set to TRUE (enabling auto-discovery)
	 * 
	 * @access private
	 * @var boolean
	 */
	private $_autoDiscoverHelpersEnabled = TRUE;
	
	/**
	 * Auto-discover initialized?
	 * 
	 * This property stores the flag that tells whether or not the auto-discovery
	 * of view helpers has been performed. Is set to TRUE if so, FALSE if not.
	 * 
	 * @access private
	 * @var boolean
	 */
	private $_autoDiscoverInit = FALSE;
	
	/**
	 * Auto-discovered helpers
	 * 
	 * This property stores the class name(s) of the auto-discovered view helpers.
	 * 
	 * @access private
	 * @var array
	 */
	private $_autoDiscoveredHelpers = array();
	
	/* -- SETTERS -- */
	
	/**
	 * Turn view helpers' auto-discovery on/off
	 * 
	 * By using this method, you can decide whether or not you want the view to
	 * auto-discover view helpers. For more information about auto-discovery of
	 * view helpers, read {@link AdminViewWithHelpers::_autoDiscoverViewHelpers()}
	 * 
	 * @access public
	 * @param boolean $flag
	 * 		Set to TRUE if you want to enable auto-discovery, FALSE if not
	 * @return void
	 */
	public function setAutoDiscoveryOfViewHelpers($flag) {
		$this->_autoDiscoverHelpersEnabled = (bool) $flag;
	}
	
	/**
	 * Add a helper
	 * 
	 * Expects a constructed helper object. If the provided helper is not an object, 
	 * an exception will be thrown!
	 * 
	 * @throws M_ViewException
	 * @access public
	 * @param object $object
	 * 		The Helper object
	 * @return void
	 */
	public function addHelper($object) {
		if(is_object($object)) {
			// Note that we use the object's class name as a key to store the 
			// helper in the internal array. This way, we avoid helper duplicates 
			// to be added to the view
			$this->_helpers[get_class($object)] = $object;
		} else {
			throw new M_ViewException(sprintf(
				'%s expects the helper to be an object',
				__CLASS__
			));
		}
	}
	
	/**
	 * Add helper(s) from auto-discovery
	 * 
	 * Will add the helper objects that are discovered by the view's method
	 * {@link AdminViewWithHelpers::_getHelpersFromAutoDiscovery()}, if not
	 * already done.
	 * 
	 * @uses AdminViewWithHelpers::_getHelpersFromAutoDiscovery()
	 * @uses AdminViewWithHelpers::addHelper()
	 * @access public
	 * @return void
	 */
	public function addHelpersFromAutoDiscovery() {
		// If auto-discovery has not yet been initialized:
		if(! $this->_autoDiscoverInit) {
			// Set the flag, for future requests
			$this->_autoDiscoverInit = TRUE;
			
			// For each of the helpers from auto-discovery:
			foreach($this->_getHelpersFromAutoDiscovery() as $helper) {
				// Add the helper:
				$this->addHelper($helper);
				
				// Also, remember this helper as an auto-discovered one:
				$this->_autoDiscoveredHelpers[] = get_class($helper);
			}
		}
	}

	/**
	 * Add a specific helper
	 *
	 * This method will add a specific helper object to the view, from all modules,
	 * or from a selected application module.
	 *
	 * For example, if you would want to add a helper object of the class
	 * "MediaViewHelper" to this view, you could do the following:
	 *
	 * Example 1
	 * <code>
	 *    class AdminMyAccountView extends AdminPageView {
	 *       public function __construct() {
	 *          $this->_addHelpersFromModule('ViewHelper', 'media');
	 *       }
	 *    }
	 * </code>
	 *
	 * Note that you should only provide the suffix of the class name, and that
	 * you ommit the module's ID in the call to this method. This is particularly
	 * handy if you want to load a specific view helper from *all* installed
	 * modules.
	 *
	 * For example, say we have two modules in our application, 'pages' and
	 * 'media', and you want to load the same view helper from both modules: the
	 * PagesViewHelper and MediaViewHelper. You could do the following:
	 *
	 * Example 2
	 * <code>
	 *    class AdminMyAccountView extends AdminPageView {
	 *       public function __construct() {
	 *          $this->_addHelpersFromModule('ViewHelper');
	 *       }
	 *    }
	 * </code>
	 *
	 * Note that, by ommitting the second parameter (module ID), this method will
	 * look for a helper class for all installed modules. If it can find the helper
	 * class, it will construct an instance and add it to the view as a helper
	 * object.
	 *
	 * @access public
	 * @param string $classNameSuffix
	 *		The class name suffix. The Module ID will be prepended to this string,
	 *		in order to compose the name of a helper class.
	 * @param string $moduleId
	 *		If you want the helper to be loaded from a specific module in the
	 *		application, and not from *all* modules, then you should provide the
	 *		ID of the module as a second argument
	 * @return void
	 */
	public function addHelpersFromModule($classNameSuffix, $moduleId = NULL) {
		// If no specific module has been specified:
		if(! $moduleId) {
			// For each of the installed modules in the application:
			foreach(AdminApplication::getModules() as $module) {
				// It would be silly to include 'admin' as a module with helpers.
				// That module, we skip! :)
				if($module->getId() != 'admin') {
					// We construct a helper object with the given suffix, and
					// with the current module:
					$helper = $this->_getHelperObject($module->getId(), $classNameSuffix);

					// If a helper has been constructed:
					if($helper) {
						// Then, add it to the view
						$this->addHelper($helper);
					}
				}
			}
		}
		// If a specific module has been requested:
		else {
			// We construct a helper object with the given suffix, and
			// with the provided module:
			$helper = $this->_getHelperObject($moduleId, $classNameSuffix);

			// If a helper has been constructed:
			if($helper) {
				// Then, add it to the view
				$this->addHelper($helper);
			}
		}
	}
	
	/**
	 * Clear helpers
	 * 
	 * Will remove *all* view helpers that are being used by the view.
	 * 
	 * @access public
	 * @return void
	 */
	public function clearHelpers() {
		$this->_helpers = array();
	}
	
	/**
	 * Clear helpers from auto-discovery
	 * 
	 * Will remove the view helpers that have been added to the view by auto-
	 * discovery
	 * 
	 * @access public
	 * @return void
	 */
	public function clearHelpersFromAutoDiscovery() {
		// For each of the auto-discovered helpers:
		foreach($this->_autoDiscoveredHelpers as $className) {
			// If the helper has been added:
			if(isset($this->_helpers[$className])) {
				// Remove it:
				unset($this->_helpers[$className]);
			}
		}
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get helpers
	 * 
	 * Will provide with the collection of view helpers that may have been added
	 * previously with {@link AdminViewWithHelpers::addHelper()}.
	 * 
	 * @access public
	 * @return ArrayIterator $helpers
	 * 		The collection of view helpers
	 */
	public function getHelpers() {
		return new ArrayIterator($this->_helpers);
	}
	
	/**
	 * Get auto-discovered helpers
	 * 
	 * Will provide with the sub-collection of helpers that has been auto-discovered
	 * by the view.
	 * 
	 * @access public
	 * @return ArrayIterator $helpers
	 * 		The sub-collection of view helpers that has been auto-discovered
	 */
	public function getHelpersFromAutoDiscovery() {
		// The output collection
		$out = array();
		
		// For each of the helpers:
		foreach($this->getHelpers() as $name => $object) {
			// If the helper has been auto-discovered:
			if(in_array($name, $this->_autoDiscoveredHelpers)) {
				// Add the helper to the output
				$out[$name] = $object;
			}
		}
		
		// Return the collection 
		return new ArrayIterator($out);
	}
	
	/**
	 * Get number of helpers
	 * 
	 * Will provide with the total number of view helpers that is being used by
	 * the view. These helpers are either attached previously to the view with
	 * {@link AdminViewWithHelpers::addHelper()}, or auto-discovered by the view's
	 * {@link AdminViewWithHelpers::_getHelpersFromAutoDiscovery()} method.
	 * 
	 * @access public
	 * @return integer $count
	 * 		The number of view helpers that has been attached to the view
	 */
	public function getNumberOfHelpers() {
		return count($this->_helpers);
	}
	
	/**
	 * Get number of auto-discovered helpers
	 * 
	 * Will provide 
	 */
	
	/**
	 * Is auto-discovery of view helpers enabled?
	 * 
	 * Will tell whether or not the auto-discovery of helpers has been enabled.
	 * Read {@link AdminViewWithHelpers::setAutoDiscoveryOfViewHelpers()} for
	 * more info.
	 * 
	 * @access public
	 * @return boolean $flag
	 * 		Returns TRUE if auto-discovery has been enabled, FALSE if not
	 */
	public function isAutoDiscoveryOfViewHelpersEnabled() {
		return $this->_autoDiscoverHelpersEnabled;
	}
	
	/**
	 * Is auto-discovery of view helpers initialized?
	 * 
	 * Will tell whether or not the auto-discovery of helpers has been performed
	 * yet. For more info, read:
	 * 
	 * - {@link AdminViewWithHelpers::setAutoDiscoveryOfViewHelpers()}
	 * - {@link AdminViewWithHelpers::isAutoDiscoveryOfViewHelpersEnabled()}
	 * 
	 * @access public
	 * @return boolean $flag
	 * 		Returns TRUE if auto-discovery has been performed, FALSE if not
	 */
	public function isAutoDiscoveryOfViewHelpersPerformed() {
		return $this->_autoDiscoverInit;
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get merged result from helper object(s)
	 * 
	 * @throws M_ViewException
	 * @access private
	 * @param string $message
	 * 		The message (method) to be sent to the helper object(s).
	 * @param ...
	 * 		A variable number of arguments to be passed along with the
	 * 		message (method arguments)
	 * @return mixed $rs
	 * 		Returns the result that has been produced by the helper object(s)
	 */
	protected function _getHelpersMergedResult($message) {
		// If no helper objects have been assigned, return NULL
		if($this->getNumberOfHelpers() == 0) {
			return NULL;
		}
		
		// Prepare the return value:
		$out = NULL;
		
		// Collect the arguments for the helpers:
		$args = array();
		for($i = 1, $m = func_num_args(); $i < $m; $i ++) {
			$args[] = func_get_arg($i);
		}
		
		// For each of the helpers:
		$i = 0;
		foreach($this->getHelpers() as $helper) {
			// Get the result of the current helper:
			$rs = $this->_getHelperResult($helper, $message, $args);
			
			// If a result is available:
			if($rs !== NULL) {
				// If this is the first result available:
				if($i ++ == 0) {
					// Set the return value:
					$out = $rs;
				}
				// If a result is already available:
				else {
					// The way in which we merge the values, depends on the data
					// type of the return value. If the current return value is 
					// an array:
					if(is_array($out)) {
						// If the new result is not an array, we throw an exception:
						// (because we are unable to merge the values)
						if(! is_array($rs)) {
							throw new M_ViewException(sprintf(
								'Unexpected result from %s::%s(). Cannot merge with array',
								get_class($helper),
								$message
							));
						}
						
						// Merge the arrays:
						$out = array_merge($out, $rs);
					}
					// If the current return value is anything but an array:
					else {
						// Overwrite return value
						$out = $rs;
					}
				}
			}
		}
		
		// Return final value:
		return $out;
	}
	
	/**
	 * Get decorated result from helper object(s)
	 * 
	 * @throws M_ViewException
	 * @access private
	 * @param string $message
	 * 		The message (method) to be sent to the helper object(s).
	 * @param array $decoratedArgumentIndex
	 * 		The index number (starting at ZERO) of the argument to be decorated
	 * 		by the return value(s) of the helper object(s).
	 * @param ...
	 * 		A variable number of arguments to be passed along with the
	 * 		message (method arguments)
	 * @return mixed $rs
	 * 		Returns the result that has been produced by the helper object(s)
	 */
	protected function _getHelpersDecoratedResult($message, $decoratedArgumentIndex) {
		// If no helper objects have been assigned, return NULL
		if($this->getNumberOfHelpers() == 0) {
			return NULL;
		}
		
		// Collect the arguments for the helpers:
		$args = array();
		for($i = 2, $m = func_num_args(); $i < $m; $i ++) {
			$args[] = func_get_arg($i);
		}
		
		// Make sure the decorated argument has been provided!
		// (throw exception if not)
		if(! isset($args[$decoratedArgumentIndex])) {
			throw new M_ViewException('Cannot get decorated result of helpers; Missing decorated argument');
		}
		
		// Prepare the return value:
		$out = NULL;
		
		// For each of the helpers:
		foreach($this->getHelpers() as $helper) {
			// Get the result of the current helper:
			$rs = $this->_getHelperResult($helper, $message, $args);
			
			// If a result is available:
			if($rs) {
				// Set the new value of the argument to be decorated:
				$args[$decoratedArgumentIndex] = $rs;
				
				// Set the return value:
				$out = $rs;
			}
		}
		
		// Return final value:
		return $out;
	}
	
	/**
	 * Call a helper
	 * 
	 * This method is used internally by {@link AdminViewWithHelpers} in order to
	 * call one of the helpers.
	 * 
	 * NOTE:
	 * Subclasses of {@link AdminViewWithHelpers} cannot directly access this method.
	 * They should use the following methods instead:
	 * 
	 * - {@link AdminViewWithHelpers::_getHelpersMergedResult()}
	 * - {@link AdminViewWithHelpers::_getHelpersDecoratedResult()}
	 * 
	 * NOTE:
	 * Will throw an exception if the helper object is not correctly implemented
	 * 
	 * @throws M_ViewException
	 * @access protected
	 * @param object $helper
	 * 		The helper object
	 * @param string $message
	 * 		The message (method) to be sent to the helper object.
	 * @param array $args
	 * 		The arguments to the message (method)
	 * @return mixed $rs
	 * 		Returns the result that has been produced by the helper object
	 */
	protected function _getHelperResult($helper, $message, array $args = array()) {
		// If the helper does not respond to this message, return NULL
		// echo ' - ' . get_class($helper);
		if(! method_exists($helper, $message)) {
			return NULL;
		}

		// echo '::'. $message .'()';
		// If the helper's method is private, return NULL
		$method = new ReflectionMethod($helper, $message);
		if($method->isPrivate()) {
			return NULL;
		}
		
		// If the helper's method is not correctly implemented (signature)
		// we throw an exception. In this case, we throw an exception
		// (instead of returning NULL), because this would be a programming
		// error!
		$numberOfArgs = count($args);
		if($method->getNumberOfParameters() < $numberOfArgs) {
			throw new M_ViewException(sprintf(
				'AdminListView Helper object "%s" does not correcty implement %s(). Some argument(s) missing.',
				get_class($helper),
				$message
			));
		}
		
		// For each of the arguments that have been defined in the helper
		// object's method implementation
		$i = 0;
		foreach($method->getParameters() as $param) {
			// Stop checking parameters, if we have no information about
			// the correspondingly provided argument
			if(! isset($args[$i])) {
				if(! $param->isOptional()) {
					throw new M_ViewException(sprintf(
						'AdminListView Helper object "%s" does not correcty implement %s(). %s does not know about the argument $%s.',
						get_class($helper),
						$message,
						__CLASS__,
						$param->getName()
					));
				}
				break;
			}
			
			// Is the current argument defined as being an object?
			$class = $param->getClass();
			if($class) {
				// Should it be declared as an object?
				if(! is_object($args[$i])) {
					// If not, throw an exception
					throw new M_ViewException(sprintf(
						'AdminListView Helper object "%s" does not correcty implement %s(). The argument $%s should not be declared as an instance of %s',
						get_class($helper),
						$message,
						$param->getName(),
						$class->getName()
					));
				}
				
				// In that case, it must be the of the same class as the 
				// correspondingly provided argument (or at least a 
				// superclass)
				$classOfProvidedArgument = new ReflectionClass($args[$i]);
				if($class->getName() != get_class($args[$i]) && ! $classOfProvidedArgument->isSubclassOf($class)) {
					// If not, throw an exception
					throw new M_ViewException(sprintf(
						'AdminListView Helper object "%s" does not correcty implement %s(). The argument $%s should be declared as instance of (super)class %s',
						get_class($helper),
						$message,
						$param->getName(),
						$classOfProvidedArgument->getName()
					));
				}
			}
			// If the current argument is not an object
			else {
				// but the provided one is:
				/*
				if(is_object($arg[$i])) {
					throw new M_ViewException(sprintf(
						'AdminListView Helper object does not correcty implement %s(). The argument $%s should be declared as instance of (super)class %s',
						$message,
						$param->getName(),
						get_class($arg[$i])
					));
				}
				 */
			}
			
			// Go to next argument
			$i ++;
		}
		
		// Get the feedback of the helper object:
		return call_user_func_array(array($helper, $message), $args);
	}
	
	/**
	 * Auto-discover view helpers
	 * 
	 * Typically, the view helpers that are being used, have been auto-discovered
	 * by the "helped" view itself.
	 * 
	 * If the view helpers' class names follow some predefined naming conventions, 
	 * they can be discovered automatically by the "helped" view. Note however 
	 * that these naming conventions are not defined by this method, but by 
	 * {@link AdminViewWithHelpers::_getAutoDiscoveredViewHelpersNamingConventions()}.
	 * 
	 * This method will use the naming conventions to look up view helpers. It 
	 * will go through all installed application modules, and check if view
	 * helpers can be found with a matching class name.
	 * 
	 * For each of the view helper classes that has been found during the look-up,
	 * an instance is constructed, and added to the resulting collection.
	 * 
	 * @uses AdminViewWithHelpers::_getAutoDiscoveredViewHelpersNamingConventions()
	 * @access protected
	 * @return array $helpers
	 * 		The view helpers that have been auto-discovered
	 */
	protected function _getHelpersFromAutoDiscovery() {
		// If view helpers should not be auto-discovered:
		if(! $this->isAutoDiscoveryOfViewHelpersEnabled()) {
			// do not auto-discover!
			return array();
		}
		
		// Resulting collection
		$out = array();
		
		// Get the class name suffixes:
		$suffixes = $this->_getAutoDiscoveredViewHelperClassNameSuffixes();
		
		// For each of the installed modules in the application:
		foreach(AdminApplication::getModules() as $module) {
			// It would be silly to include 'admin' as a module with helpers.
			// That module, we skip! :)
			if($module->getId() != 'admin') {
				// For each of the suffixes
				foreach($suffixes as $suffix) {
					// We construct a helper object with the current suffix, and
					// with the current module:
					$helper = $this->_getHelperObject($module->getId(), $suffix);
					
					// If a helper has been constructed:
					if($helper) {
						// Then, add it to the output
						$out[] = $helper;
					}
				}
			}
		}
		
		// Return the collection of helpers:
		return $out;
	}
	
	/**
	 * Get "class name suffixes" for auto-discovered view helpers
	 * 
	 * This method is used by {@link AdminViewWithHelpers::_autoDiscoverViewHelpers()}
	 * to auto-discover view helpers.
	 * 
	 * Since the naming conventions will be different for each of the classes
	 * that accept helpers, this method is defined as abstract. This way, all
	 * subclasses (view classes that accept view helpers) are *required* to 
	 * implement a method that provides with naming conventions!
	 * 
	 * @see AdminViewWithHelpers::_autoDiscoverViewHelpers()
	 * @access protected
	 * @return array $conventions
	 * 		The collection of naming conventions for view helpers.
	 */
	protected function _getAutoDiscoveredViewHelperClassNameSuffixes() {
		// Collection of naming conventions
		$out = array();
		
		// Get the class name of this view:
		$name = $this->getClassName();
		
		// While this is not the abstract AdminViewWithHelpers class:
		while($name != __CLASS__) {
			// Get the class name:
			$temp = $name;
			
			// If the current class name starts with 'Admin'
			if(! strncmp('Admin', $temp, 5)) {
				// Then, we strip 'Admin' from the class name suffix
				$temp = substr($temp, 5);
			}
			
			// If the current class name ends in 'View', we replace it by
			// 'ViewHelper':
			if(substr($temp, -4) == 'View') {
				$temp .= 'Helper';
			}
			
			// Finally, we add the class name suffix to the collection:
			$out[] = $temp;
			
			// get the superclass of this view
			$name = get_parent_class($name);
		}
		
		// We're done! Return the naming convention(s)
		return $out;
	}

	/**
	 * Get helper object, given a module ID and a class name suffix
	 *
	 * NOTE: This method will return NULL if the helper class could not be found
	 * in the module.
	 *
	 * @access protected
	 * @param string $moduleId
	 * @param string $classNameSuffix
	 * @return string
	 */
	protected function _getHelperObject($moduleId, $classNameSuffix) {
		// We compose the class name:
		$className = M_Helper::getCamelCasedString($moduleId) . $classNameSuffix;

		// We compose the full path to the helper class file:
		$file = AdminLoader::getViewClassFile($className, $moduleId);

		// If the class file exists:
		if($file->exists()) {
			// Then, we load the helper class:
			AdminLoader::loadView($className, $moduleId);

			// The class has been loaded, so we can now safely construct an
			// instance of the helper. So we construct it, and return it as the
			// result of this method:
			return new $className;
		}

		// If we are still here, return NULL instead
		return NULL;
	}
}