<?php
/**
 * AdminPageView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminViewWithHelpers.class.php';

/**
 * AdminPageView
 * 
 * AdminPageView, a subclass of {@link AdminViewWithHelpers}, renders the view of 
 * the administration page (Header, Source Pane, etc.).
 * 
 * @package App
 * @subpackage Admin
 */
abstract class AdminPageView extends AdminViewWithHelpers {

	/* -- CONSTANTS -- */

	/**
	 * View mode
	 *
	 * This constant can be used to address a view mode. Typically, this method
	 * is used as an argument to {@link PageView::setViewMode()}
	 *
	 * @access public
	 * @var string
	 */
	const VIEW_MODE_IFRAME = 'iframe';

	/**
	 * View mode
	 *
	 * This constant can be used to address a view mode. Typically, this method
	 * is used as an argument to {@link PageView::setViewMode()}
	 *
	 * @access public
	 * @var string
	 */
	const VIEW_MODE_NORMAL = 'normal';

	/* -- PROPERTIES -- */
	
	/**
	 * Administrator
	 * 
	 * This property stores the {@link Administrator} account for which the view
	 * is being rendered
	 * 
	 * @access protected
	 * @var Administrator
	 */
	protected $_administrator = NULL;
	
	/**
	 * Is AJAX Response?
	 * 
	 * This property stores the boolean flag that tells whether or not the view
	 * is used as an AJAX response.
	 * 
	 * @access protected
	 * @var bool
	 */
	protected $_isAjaxResponse = FALSE;

	/**
	 * Active menu item
	 *
	 * @access private
	 * @var string
	 */
	private $_activeMenuItem;

	/**
	 * View mode
	 *
	 * Stores the view mode of the {@link PageView} object.
	 *
	 * @access private
	 * @var string
	 */
	private $_viewMode;

	/* -- CONSTRUCTOR -- */

	/**
	 * Constructor
	 *
	 * @see AdminViewWithHelpers::addHelpersFromAutoDiscovery()
	 * @access public
	 * @return AdminPageView $view
	 *		The Admin Page View
	 */
	public function __construct() {
		// Add view helpers, from all modules, to this view
		$this->addHelpersFromModule('ViewHelper');
	}

	/* -- SETTERS -- */

	/**
	 * Set flag: Is AJAX Response?
	 * 
	 * If the view is rendered as an AJAX response, it will NOT include the 
	 * general Administration Page Structure (Header, Source Pane, etc.)
	 * 
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if the view is meant for AJAX, FALSE if not
	 * @return AdminPageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setIsAjaxResponse($flag) {
		$this->_isAjaxResponse = (bool) $flag;
	}

	/**
	 * Set active menu item
	 *
	 * @access public
	 * @param string $item
	 * @return AdminPageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setActiveMenuItem($item) {
		// Set active menu item in the view
		$this->_activeMenuItem = (string) $item;

		// Set active menu item in the M_Menu API
		M_MenuHelper::setActivePath($this->getActiveMenuItem());

		// Return myself
		return $this;
	}

	/**
	 * Set View Mode
	 *
	 * @see AdminPageView::VIEW_MODE_IFRAME
	 * @see AdminPageView::VIEW_MODE_NORMAL
	 * @access public
	 * @param string $viewMode
	 * @return AdminPageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setViewMode($viewMode) {
		// Make sure that the view mode is supported:
		if(! in_array($viewMode, array(self::VIEW_MODE_NORMAL, self::VIEW_MODE_IFRAME))) {
			// If not, we throw an exception to inform about the error
			throw new M_Exception(sprintf(
				'The view mode "%s" is not supported by %s',
				$viewMode,
				__CLASS__
			));
		}

		// Set the view mode:
		$this->_viewMode = (string) $viewMode;

		// Returns itself;
		return $this;
	}

	/* -- GETTERS -- */

	/**
	 * Get flag: Is AJAX Response?
	 * 
	 * @see AdminPageView::setIsAjaxResponse()
	 * @access public
	 * @return bool $flag
	 * 		Returns TRUE if the view is an AJAX Request, FALSE if not
	 */
	public function isAjaxResponse() {
		return $this->_isAjaxResponse;
	}

	/**
	 * Get active menu item
	 *
	 * @see PageView::setActiveMenuItem()
	 * @access public
	 * @return string
	 */
	public function getActiveMenuItem() {
		return $this->_activeMenuItem;
	}

	/**
	 * Get View Mode
	 *
	 * Will provide with the view mode that has been defined for this view
	 * previously with {@link PageView}. Note that, if no view mode has been
	 * specificed to this object explicitely, this method will listen to the
	 * request variable with name 'viewmode'. Depending on the value of that
	 * variable, the view will be set to a given mode:
	 *
	 * <code>'iframe'</code>
	 *
	 * If the variable carries this value, then the view mode will be set to
	 * {@link PageView::VIEW_MODE_IFRAME}
	 *
	 * <code>'normal'</code>
	 *
	 * If the variable carries any of these value, then the view mode will be
	 * set to {@link PageView::VIEW_MODE_NORMAL}
	 *
	 * @access public
	 * @param string $viewMode
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function getViewMode() {
		// If no view mode has been set
		if(! $this->_viewMode) {
			// Then, we define it now:
			$this->setViewMode(M_Request::getVariable('viewmode', self::VIEW_MODE_NORMAL));
		}

		// Return the view mode:
		return $this->_viewMode;
	}


	/**
	 * Return the file path for a template file in adminmode
	 *
	 * To use a file located in a template folder you can use this method: it
	 * will automatically generate a path to the template-folder, possibly
	 * in a specific module
	 *
	 * @param str $module
	 * @return str
	 */
	public static function getAdminTemplatesPath($module = null)
	{
		return AdminLoader::getAdminResourcesPath($module) . '/' . self::FOLDER_TEMPLATES;
	}
	
	/* -- PROTECTED / PRIVATE -- */
	
	/**
	 * Render HTML
	 *
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return string
	 */
	protected function _render() {
		// Do some browser checks:
		$isIE7      = (M_Browser::isIE() && (int) M_Browser::getVersion() >= 7);
		$isFirefox2 = (M_Browser::isFirefox() && (int) M_Browser::getVersion() >= 2);
		$isSafari3  = ((M_Browser::isSafari() && (int) M_Browser::getVersion() >= 3) || (M_Browser::isWebkitBased() && M_Browser::getVersion() >= 3));
		$isChrome   = M_Browser::isGoogleChrome();
		$isOpera    = M_Browser::isOpera();
		
		// If the browser is not supported:
		if(! $isIE7 && ! $isFirefox2 && ! $isSafari3 && ! $isChrome && ! $isOpera) {
			// Then we render the "Unsupported Browser" page instead
			$rs = new M_ViewHtmlResource(self::getTemplatesPath('admin') . '/AdminUnsupportedBrowser.tpl');
			return $rs->fetch();
		}
		
		// If this view is an AJAX Response:
		if($this->isAjaxResponse()) {
			// Return the HTML of this view
			return parent::_render();
		}
		// If this view is NOT an AJAX Response
		else {
			// Construct the view resource (template), based on the view mode
			switch($this->getViewMode()) {
				case self::VIEW_MODE_IFRAME:
					$rs = new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminPageIframeMode.tpl');
					break;
				default:
					$rs = new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminPage.tpl');
			}
			
			// Assign administrator:
			$rs->assign('administrator', $this->_getAdministrator());
			
			// Check if we need to display a logo
			$module = new M_ApplicationModule('Admin');
			$config = $module->getConfig();

			// The logo:
			$rs->assign('logo', $config->logo);

			// Check if we need to insert the filterMenu
			$showFilterMenu = M_Helper::isBooleanTrue($config->showFilterMenu);
			if($showFilterMenu) {
				M_ViewJavascript::getInstance()->addFile(new M_File(M_Loader::getResourcesPath('admin') .'/javascript/filtermenu.js'));
				M_ViewJavascript::getInstance()->addFile(new M_File(M_Loader::getResourcesPath('admin') .'/javascript/jquery.input-hint.js'));
				M_ViewCss::getInstance()->addFile(new M_File(M_Loader::getResourcesPath('admin') .'/css/filtermenu.css'));

			}
			$rs->assign('showFilterMenu', $showFilterMenu);
			
			// Render the HTML of this view, and assign it to the template:
			$rs->assign('content', parent::_render());
			
			// Assign messages from the messenger:
			$messenger = M_Messenger::getInstance();
			$rs->assign('messages', $messenger->getMessages());
			$messenger->clearMessages();
			
			// We assign the menu's to the view:
			$rs->assign('sourcePaneMenu', $this->_getSourcePaneMenu());
			$rs->assign('plusMenu', $this->_getPlusMenu());
			$rs->assign('activeMenuItem', $this->getActiveMenuItem());

			// Assign javascript and css files
			$js = M_ViewJavascript::getInstance();
			$css = M_ViewCss::getInstance();
			$rs->assign('viewJavascript', $js->toString());
			$rs->assign('viewCss', $css->toString());
			
			// return the rendered HTML from the template:
			return $rs->fetch();
		}
	}

	/**
	 * Get Source Pane menu
	 *
	 * @access protected
	 * @return M_ArrayIterator $menu
	 *		The collection of {@link M_Menu} instances
	 */
	public function _getSourcePaneMenu() {
		// Output collection
		$out = array();
		$tmp = array();
		
		$menuXml = new M_File(AdminLoader::getAdminResourcesPath('admin') . '/menu.xml');
		if($menuXml->exists()) {
			$out = array();
			foreach(M_Menu::getInstanceFromXml('AdminSourcePaneMenu', $menuXml)->getMenuItems()->current()->getMenuItems() as $i => $item) {
				$temp = M_Menu::getInstance('AdminSourcePaneMenu' . $i);
				$temp->setTitle($item->getTitle());
				foreach($item->getMenuItems() as $subitem) {
					$temp->addMenuItem($subitem);
				}
				$out[] = $temp;
			}
			return new M_ArrayIterator($out);
		}
		
		// Consolidate all of the menu items into one array:
		$menu = array_merge(
			$this->_getSourcePaneMenuFromDataObjects(),
			array($this->_getSourcePaneMenuSmartFolders()),
			$this->_getMenuFromHelpers('SourcePaneMenu')
		);

		// For each of the items in the array:
		foreach($menu as $i => $item) {
			// If the current item is a menu ITEM
			if(M_Helper::isInstanceOf($item, 'M_MenuItem')) {
				// Then, we add it to the first menu (which holds all ungrouped
				// items in the source pane menu)
				$menu[0]->addMenuItem($item);

				// We remove the item from the original collection
				unset($menu[$i]);
			}
		}

		// For each of the items in the array:
		foreach($menu as $i => $item) {
			// If the current item is a menu
			if(M_Helper::isInstanceOf($item, 'M_Menu')) {
				// We ask the helper(s) for the weight of the menu:
				$weight = $this->_getHelpersMergedResult('setSourcePaneMenuWeight', $item);

				// If no weight has been defined:
				if(! is_numeric($weight)) {
					// Then, we set the weight to the default: 0
					$weight = $i;
				}

				// We ask the helper to set/update the menu... Maybe the helper
				// wants to add menu items to a given menu in the source pane,
				// instead of adding a new menu:
				$this->_getHelpersMergedResult('setSourcePaneMenu', $item);

				// Add the menu to the temporary array:
				$temp[] = array(
					'menu'   => $item,
					'weight' => $weight
				);
			}
		}

		// Sort the menu:
		M_Array::sortMultiDimensional($temp, 'weight', SORT_ASC);

		// For each of the items in the sorted collection:
		foreach($temp as $tempEntry) {
			// Add to output:
			$out[] = $tempEntry['menu'];
		}
		
		// Return the output collection:
		return new M_ArrayIterator($out);
	}
	
	/**
	 * Get Source Pane items from data objects
	 * 
	 * This method will create the collection of menu items that is to be shown
	 * in the source pane (bar on the left).
	 *
	 * Note that this collection will be created automatically, by going through
	 * all of the installed modules. In each module, this method will look up the
	 * available data objects. A menu item is created for each data object, that
	 * is of course if a list definition exists for the data object in question.
	 * 
	 * @access protected
	 * @return array $menu
	 *		The collection of {@link M_Menu} instances
	 */
	protected function _getSourcePaneMenuFromDataObjects() {
		// Output collection
		$out = array();

		// First of all, create the menu that holds all ungrouped menu items:
		$out[] = M_Menu::getInstance('AdminSourcePane');

		// For each of the modules in the application:
		foreach(AdminApplication::getModules() as $id => $module) {
			/* @var $module AdminModule */
			// If the module is "multimanageable", or visible/available in the
			// admin module:
			if($module->isMultimanageable()) {
				// We get the names of the data objects that are available in
				// the current application module:
				$dataObjectNames = $module->getDataObjectNames();
				
				// We prepare a temporary array:
				$temp = array();
				
				// For each of the data objects in the current module:
				foreach($dataObjectNames as $id => $name) {
					// We get the list definition of this particular data object:
					$list = new AdminListDefinition($module, $id);
					
					// And then, we check if that list definition really exists:
					if($list->exists()) {
						// Yes, it does! :) Now, we make sure that the administrator
						// has access to the list of this data object. In order
						// to perform this check, we look at the ACL singleton:
						if($this->_getAclAllow($list->getAclResourceId(), 'view')) {
							// If so, we ask the list definition for the title,
							// and we create a menu item that represents the link
							// to the list of data objects:
							$temp[] = new M_MenuItem(
								'admin/overview/' . $module->getId() . '/' . $id,
								t($list->getTitle()),
								t($list->getDescription())
							);
						}
					}
				}
				
				// If the temporary array now holds at least 2 menu items, then
				// we create an instance of M_Menu out of the collection of items.
				// This way, we group the items in the source pane:
				if(count($temp) > 1) {
					// Create an instance of M_Menu
					$menu = M_Menu::getInstance('AdminSourcePane' . M_Helper::getCamelCasedString($module->getId()));

					// Set the title of the menu. Note that we translate the name
					// of the menu item:
					$menu->setTitle(t($module->getName()));

					// For each of the menu items in the group:
					foreach($temp as $item) {
						// Add the item to the menu:
						$menu->addMenuItem($item);
					}

					// Finally, add the menu to the output
					$out[] = $menu;
				}
				// If the temporary array only holds 1 item, then we do not group
				// it. In this case...
				elseif(count($temp) == 1) {
					// We add it to the menu that groups all ungrouped items:
					$out[0]->addMenuItem($temp[0]);
				}
			}
		}

		// Return the collection
		return $out;
	}

	/**
	 * Get Source Pane menu with smart folders
	 *
	 * This method will create the collection of menu items that is to be shown
	 * in the source pane (bar on the left).
	 *
	 * Note that this collection will be created automatically, by going through
	 * all of the available smart folders. The smart folders will be grouped in
	 * an instance of {@link M_Menu} with ID 'AdminSourcePaneSmartFolders'
	 *
	 * @access protected
	 * @return M_Menu $menu
	 */
	protected function _getSourcePaneMenuSmartFolders() {
		// Get the menu of smart folders
		$menu = M_Menu::getInstance('AdminSourcePaneSmartFolder');

		if(!$this->_getAclAllow('admin-folder', 'view')) return $menu;

		// For each of the smart folders available:
		foreach($this->_getSmartFolders() as $smartFolder) {
			/* @var $smartFolder AdminSmartFolder */
			// Create a menu item out of the current smart folder, and add it
			// to the menu:
			$menu
				->addMenuItem(new M_MenuItem(
					// Path
					'admin/smart-folder/' . $smartFolder->getId(),
					// Name/Title
					$smartFolder->getName(),
					// Description
					$smartFolder->getDescription()
				));
		}

		// Also, we add the menu item that allows to manage the smart folders:
		$menu
			->addMenuItem(new M_MenuItem(
				// Path
				'admin/smart-folders/',
				// Name/Title
				t('Edit my Smart Folders')
			));

		// Set the title of the menu
		$menu->setTitle(t('My folders'));

		// Return the menu
		return $menu;
	}

	/**
	 * Get Plus Menu
	 *
	 * @access protected
	 * @return M_ArrayIterator $menu
	 *		The collection of {@link M_Menu} instances
	 */
	public function _getPlusMenu() {
		// Output collection
		$out = array();
		$tmp = array();

		// Consolidate all of the menu items into one array:
		$menu = array_merge(
			$this->_getPlusMenuFromDataObjects(),
			$this->_getMenuFromHelpers('PlusMenu')
		);

		// For each of the items in the array:
		foreach($menu as $i => $item) {
			// If the current item is a menu ITEM
			if(M_Helper::isInstanceOf($item, 'M_MenuItem')) {
				// Then, we add it to the first menu (which holds all ungrouped
				// items in the source pane menu)
				$menu[0]->addMenuItem($item);

				// We remove the item from the original collection
				unset($menu[$i]);
			}
		}

		// For each of the items in the array:
		foreach($menu as $i => $item) {
			// If the current item is a menu
			if(M_Helper::isInstanceOf($item, 'M_Menu')) {
				// We ask the helper(s) for the weight of the menu:
				$weight = $this->_getHelpersMergedResult('setPlusMenuWeight', $item);

				// If no weight has been defined:
				if(! is_numeric($weight)) {
					// Then, we set the weight to the default: 0
					$weight = $i;
				}

				// We ask the helper to set/update the menu... Maybe the helper
				// wants to add menu items to a given menu, instead of adding a
				// new menu:
				$this->_getHelpersMergedResult('setPlusMenu', $item);

				// Add the menu to the temporary array:
				$temp[] = array(
					'menu'   => $item,
					'weight' => $weight
				);
			}
		}

		// Sort the menu:
		M_Array::sortMultiDimensional($temp, 'weight', SORT_ASC);
		
		// For each of the items in the sorted collection:
		foreach($temp as $tempEntry) {
			// Add to output:
			$out[] = $tempEntry['menu'];
		}
		
		// Return the output collection:
		return new M_ArrayIterator($out);
	}
	
	/**
	 * Get Plus Menu from data objects
	 *
	 * @see AdminPageView::_getSourcePaneMenuFromDataObjects()
	 * @access protected
	 * @return array
	 */
	protected function _getPlusMenuFromDataObjects() {
		// Output collection
		$out = array();

		// First of all, create the menu that holds all ungrouped menu items:
		$out[] = M_Menu::getInstance('PlusMenu');

		// For each of the modules in the application:
		foreach(AdminApplication::getModules() as $id => $module) {
			/* @var $module AdminModule */
			// If the module is "multimanageable", or visible/available in the
			// admin module:
			if($module->isMultimanageable()) {
				// We get the names of the data objects that are available in
				// the current application module:
				$dataObjectNames = $module->getDataObjectNames();

				// We prepare a temporary array:
				$temp = array();

				// For each of the data objects in the current module:
				foreach($dataObjectNames as $id => $name) {
					// Get the list & form definition of the data object:
					$list = new AdminListDefinition($module, $id);
					$form = new AdminFormDefinition($module, $id);

					// Check if the list & form definition is available for
					// this data object. Also make sure that the list allows
					// new items to be created:
					if($list->exists() && $form->exists() && $list->getFlagAllowNewItems()) {
						// Yes, it does! :) Now, we make sure that the administrator
						// has access to the list of this data object. In order
						// to perform this check, we look at the ACL singleton:
						if($this->_getAclAllow($list->getAclResourceId(), 'view')) {
							// If so, we ask the list definition for the title,
							// and we create a menu item that represents the link
							// to the list of data objects:
							$temp[] = new M_MenuItem(
								'admin/edit/' . $module->getId() . '/0/' . $id,
								t('Create a new item in <strong>@module</strong>', array('@module' => $list->getTitle())),
								$list->getDescription()
							);
						}
					}
				}

				// If the temporary array now holds at least 2 menu items, then
				// we create an instance of M_Menu out of the collection of items.
				// This way, we group the items in the source pane:
				if(count($temp) > 1) {
					// Create an instance of M_Menu
					$menu = M_Menu::getInstance('PlusMenu' . M_Helper::getCamelCasedString($module->getId()));

					// Set the title of the menu. Note that we translate the name
					// of the menu item:
					$menu->setTitle(t($module->getName()));

					// For each of the menu items in the group:
					foreach($temp as $item) {
						// Add the item to the menu:
						$menu->addMenuItem($item);
					}

					// Finally, add the menu to the output
					$out[] = $menu;
				}
				// If the temporary array only holds 1 item, then we do not group
				// it. In this case...
				elseif(count($temp) == 1) {
					// We add it to the menu that groups all ungrouped items:
					$out[0]->addMenuItem($temp[0]);
				}
			}
		}
		
		// Return the collection
		return $out;
	}

	/**
	 * Get menu items from helpers
	 *
	 * This method will create the collection of menu items. The collection of
	 * menu items that is provided by this method, is defined by the helper
	 * object(s) in the view.
	 *
	 * @access protected
	 * @param string $menuBaseId
	 * @return array $menu
	 *		A collection of {@link M_Menu} and {@link M_MenuItem} instances
	 */
	protected function _getMenuFromHelpers($menuBaseId) {
		// We ask the helpers for new menu items:
		$menu = $this->_getHelpersMergedResult('add' . $menuBaseId);

		// Has the helper provided us with new menus?
		if($menu) {
			// We make sure that the helpers provided us with an iterator
			// of M_Menu/M_MenuItem instances (array or iterator interface):
			if(M_Helper::isIteratorOfClass($menu, array('M_Menu', 'M_MenuItem'))) {
				// If the result is an array
				if(is_array($menu)) {
					// Then, return the array
					return $menu;
				}
				// If the result is an iterator object
				elseif(M_Helper::isInstanceOf($menu, 'ArrayIterator')) {
					// Then, we return a copy of the array
					return $menu->getArrayCopy();
				}
			}
			// If the return value of the helper is not an iterator:
			else {
				throw new M_ViewException(sprintf(
					'%s expects an array of M_Menu instances or M_MenuItem ' .
					'instances from Helper::%s()',
					__CLASS__,
					'add' . $menuBaseId
				));
			}
		}

		// Return empty array, if still here
		return array();
	}

	/**
	 * Get Smart Folders in Source Pane
	 * 
	 * @access protected
	 * @return array
	 */
	protected function _getSmartFolders() {
		// Load smart folder mapper
		M_Loader::loadDataObjectMapper('AdminSmartFolder', 'admin');
		
		// Construct a new mapper, in order to fetch the smart folders from the
		// database:
		$mapper = new AdminSmartFolderMapper();
		
		// Ask the mapper for the smart folders of the administrator:
		return $mapper->getByAdministrator($this->_getAdministrator());
	}
	
	/**
	 * Get administrator
	 * 
	 * @access protected
	 * @return Administrator
	 */
	protected function _getAdministrator() {
		// If not requested before:
		if(! $this->_administrator) {
			// Load the Administrator Mapper, which we'll use to look up the Administrator account
			M_Loader::loadDataObjectMapper('Administrator', 'administrator');
			
			// Get the M_Auth Singleton
			$auth = M_Auth::getInstance();
			
			// Ask the M_Auth Singleton for the administrator:
			$mapper = new AdministratorMapper();
			$this->_administrator = $mapper->getById($auth->getIdentity());
		}
		
		// Return the administrator
		return $this->_administrator;
	}
	
	/**
	 * Check ACL Permissions
	 * 
	 * Will check permissions with {@link M_Acl::isAllowed()}
	 * 
	 * @access private
	 * @return bool $flag
	 */
	protected function _getAclAllow($resource, $permission) {
		// Run inside try{} block, to avoid exceptions (undefined roles, resources
		// or permissions) from crashing the app 
		try {
			$b = M_Acl
				::getInstance()
				->isAllowed(
					// Role
					$this->_getAdministrator()->getAclRoleId(), 
					// Resource
					$resource, 
					// Permission
					$permission
				);
			
			return $b;
		}
		catch(M_Exception $e) {
			return FALSE;
		}
	}
}