<?php
/**
 * AdminErrorView
 * 
 * @package App
 * @subpackage Admin
 */

/**
 * AdminErrorView
 * 
 * AdminErrorView, a subclass of {@link M_ViewHtml}, renders the view of
 * the error page (when an Exception has been thrown).
 * 
 * @package App
 * @subpackage Admin
 */
class AdminErrorView extends M_ViewHtml {

	/**
     * Set an exception
     *
     * @param Exception $e
     */
    public function setException(Exception $e) {
		$this->assign('exception', $e);
	}


	/**
	 * Get template
	 *
	 * Will return the {@link M_ViewHtmlResource} instance that is being used by
	 * the view to render the HTML Source Code.
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminError.tpl');
	}
}