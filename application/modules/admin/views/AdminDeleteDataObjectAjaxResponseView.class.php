<?php
/**
 * AdminToggleAjaxView
 * 
 * AdminToggleAjaxView, a subclass of {@link AdminPageView}, renders the
 * AJAX Response that informs about the result of toggling an attribute of
 * a data object. For more info, read {@link AdminController::toggleAjax()}
 * 
 * @package App
 * @subpackage Admin
 */
class AdminDeleteDataObjectAjaxResponseView extends M_ViewHtml {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Result flag
	 * 
	 * @access private
	 * @var bool
	 */
	private $_flag = FALSE;
	
	/**
	 * Affected data object's ID
	 * 
	 * @access private
	 * @var integer
	 */
	private $_dataObjectId;
	
	/* -- SETTERS -- */
	
	/**
	 * Set affected data object's ID
	 * 
	 * @access public
	 * @param integer $id
	 * @return void
	 */
	public function setDataObjectId($id) {
		$this->_dataObjectId = (int) $id;
	}
	
	/**
	 * Set result of delete operation
	 * 
	 * Can be used to set the result of the delete operation (SUCCESS/FAILURE).
	 * This method expects a boolean, to describe the result.
	 * 
	 * @access public
	 * @param bool $flag
	 * 		The result of the delete operation
	 * @return void
	 */
	public function setResult($flag) {
		$this->_flag = (bool) $flag;
	}
	
	/**
	 * Display the view
	 * 
	 * @see M_ViewHtml::display()
	 * @access public
	 * @return void
	 */
	public function display() {
		M_Header::send(M_Header::CONTENT_TYPE_XML);
		parent::display();
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		$this->assign('flag', $this->_flag);
		$this->assign('dataObjectId', $this->_dataObjectId);
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminDeleteDataObjectAjaxResponse.tpl');
	}
}