<?php
/**
 * AdminFormDefinitionView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminFormView.class.php';

/**
 * AdminFormDefinitionView
 * 
 * @package App
 * @subpackage Admin
 */
class AdminFormDefinitionView extends AdminFormView {
	/**
	 * Form Definition
	 * 
	 * This property stores the definition of the form. The definition
	 * is represented by an instance of {@link AdminFormDefinition}.
	 *
	 * @access protected
	 * @var AdminFormDefinition
	 */
	protected $_definition;
	
	/**
	 * Set the form definition
	 *
	 * @access public
	 * @param AdminFormDefinition $definition
	 * 		The form definition that is to be used to render the view
	 * @return void
	 */
	public function setFormDefinition(AdminFormDefinition $definition) {
		$this->_definition = $definition;
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * Magic method, called by {@link M_ViewHtml} to do some preprocessing
	 * of the view, prior to rendering the HTML (template) resource.
	 * 
	 * @access public
	 * @return void
	 */
	protected function preProcessing() {
		// Make sure the form has been assigned:
		// (default the form to the one that is contained by the form definition)
		if(! $this->_form && ! $this->_definition) {
			throw new M_ViewException(sprintf(
				'No form has been assigned to %s',
				__CLASS__
			));
		}
		
		// Assign the form:
		$this->assign('form', $this->_form
			? $this->_form
			: $this->_definition->getForm()
		);
		
		// Assign the definition to the template
		$this->assign('definition', $this->_definition);
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminFormDefinition.tpl');
	}
}