<?php
/**
 * AdminListInstanceRowView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminListColumnsView.class.php';

/**
 * AdminListInstanceRowView
 * 
 * Renders the view of a row that is fetched to populate the list, when scrolling
 * through the list, via AJAX.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminListInstanceRowView extends AdminListColumnsView {
	/**
	 * Get template, for locale button
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the locale button of an instance
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListInstanceRow.tpl');
	}
}