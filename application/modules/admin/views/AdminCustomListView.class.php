<?php
/**
 * AdminCustomListView
 * 
 * Superclass for a module's custom list view
 * 
 * @package App
 * @subpackage Admin
 */
abstract class AdminCustomListView extends M_ViewHtml {
	
	/* -- SETTERS -- */
	
	/**
	 * Set list definition
	 * 
	 * @access public
	 * @param AdminListDefinition $definition
	 * @return void
	 */
	public function setListDefinition(AdminListDefinition $definition) {
		$this->assign('definition', $definition);
	}
	
	/**
	 * Set collection of applied filters
	 * 
	 * @access public
	 * @param ArrayIterator $filters
	 * @return void
	 */
	public function setListFilterDefinitions(ArrayIterator $filters) {
		if(M_Helper::isIteratorOfClass($filters, 'AdminListFilterDefinition')) {
			$this->assign('filters', $filters);
		} else {
			throw new M_ViewException(sprintf(
				'Expecting collection of %s instances in %s',
				'AdminListFilterDefinition',
				__CLASS__
			));
		}
	}
	
	/**
	 * Set listed instances
	 * 
	 * Can be used to set the collection of instances that is rendered
	 * in the list view.
	 * 
	 * @access public
	 * @param ArrayIterator $instances
	 * 		The collection of {@link M_DataObject} instances that is
	 * 		to be shown in the view
	 * @return void
	 */
	public function setDataObjects(ArrayIterator $instances) {
		$this->assign('instances', $instances);
	}
	
	/**
	 * Set Smart Folder
	 * 
	 * Optionally, the view will also accept a reference to a given smart folder.
	 * This smart folder is for displaying purpose.
	 * 
	 * @access public
	 * @param AdminSmartFolder $folder
	 * @return void
	 */
	public function setSmartFolder(AdminSmartFolder $folder) {
		$this->assign('smartFolder', $folder);
	}
	
	/**
	 * Set Page jumper
	 * 
	 * Will set the {@link M_Pagejumper} instance that is being used to build
	 * the page navigation.
	 * 
	 * @access public
	 * @param M_Pagejumper $pagejumper
	 * @return void
	 */
	public function setPagejumper(M_Pagejumper $pagejumper) {
		$this->assign('pagejumper', $pagejumper);
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get list definition
	 * 
	 * @see AdminCustomListView::setListDefinition()
	 * @access public
	 * @return AdminListDefinition
	 */
	public function getListDefinition() {
		return $this->getVariable('definition');
	}
	
	/**
	 * get applied filters
	 * 
	 * @see AdminCustomListView::setListFilterDefinitions()
	 * @access public
	 * @return ArrayIterator $iterator
	 * 		An iterator of {@link AdminListFilterDefinition} instances
	 */
	public function getListFilterDefinitions() {
		return $this->getVariable('filters');
	}
	
	/**
	 * Get listed instances
	 * 
	 * @see AdminCustomListView::setDataObjects()
	 * @access public
	 * @return ArrayIterator $iterator
	 * 		An iterator of {@link M_DataObject} instances
	 */
	public function getDataObjects() {
		return $this->getVariable('instances');
	}
	
	/**
	 * Get Page jumper
	 * 
	 * @see AdminCustomListView::setPagejumper()
	 * @access public
	 * @param M_Pagejumper $pagejumper
	 * @return M_Pagejumper $pagejumper
	 */
	public function getPagejumper() {
		return $this->getVariable('pagejumper');
	}
}