<?php
/**
 * AdminNewSmartFolderView
 * 
 * AdminNewFolderView, a subclass of {@link AdminPageView}, renders the
 * AJAX Response of creating a new smart folder.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminNewSmartFolderView extends M_ViewHtml {
	/**
	 * Set the new Smart folder
	 * 
	 * @access public
	 * @param AdminSmartFolder $folder
	 * 		The new smart folder
	 * @return void
	 */
	public function setSmartFolder(AdminSmartFolder $folder) {
		$this->assign('folder', $folder);
	}
	
	/**
	 * Display the view
	 * 
	 * @see M_ViewHtml::display()
	 * @access public
	 * @return void
	 */
	public function display() {
		M_Header::send(M_Header::CONTENT_TYPE_XML);
		parent::display();
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminNewSmartFolder.tpl');
	}
}