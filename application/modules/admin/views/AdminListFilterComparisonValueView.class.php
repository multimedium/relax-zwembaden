<?php
/**
 * AdminListFilterComparisonValueView
 * 
 * Renders the input control that allows the user to introduce a filter value,
 * that goes together with a given comparison operator.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminListFilterComparisonValueView extends M_ViewHtml {
	/**
	 * Set filter definition
	 * 
	 * Can be used to set the filter definition.
	 * 
	 * @access public
	 * @param AdminListFilterWhereDefinition $definition
	 * 		The definition of the filter for which to render a menu
	 * @return void
	 */
	public function setFilterDefinition(AdminListFilterWhereDefinition $definition) {
		$this->assign('definition', $definition);
	}
	
	/**
	 * Set comparison operator
	 * 
	 * Can be used to set the comparison operator for which to generate the
	 * filter value's input control. This comparison operator may define the
	 * type of the input control (text, select, date, etc.)
	 * 
	 * @access public
	 * @param string $operator
	 * 		The comparison operator; See {@link }
	 * @return void
	 */
	public function setComparisonOperator($operator) {
		$this->assign('operator', (string) $operator);
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is being used by 
	 * the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListFilterComparisonValue.tpl');
	}
}