<?php
/**
 * AdminSmartFolderChooseModuleView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminPageView.class.php';


/**
 * AdminSmartFolderChooseModuleView
 * 
 * AdminSmartFolderChooseModuleView, a subclass of {@link AdminPageView}, 
 * renders the start page of a new Smart Folder. This view is used to 
 * render the page that allows the user to pick a module, in which to
 * create the folder.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminSmartFolderChooseModuleView extends AdminPageView {
	/**
	 * Set the new Smart folder
	 * 
	 * @access public
	 * @param AdminSmartFolder $folder
	 * 		The new smart folder
	 * @return void
	 */
	public function setSmartFolder(AdminSmartFolder $folder) {
		$this->assign('folder', $folder);
	}
	
	/**
	 * Set list definitions
	 * 
	 * Set the collection of {@link AdminListDefinition} instances from which 
	 * the user can pick. A user does not choose from a list of modules to start 
	 * a folder (because a module may contain more than 1 list), but rather from 
	 * a list of List Definitions.
	 * 
	 * @access public
	 * @param ArrayIterator $definitions
	 * @return void
	 */
	public function setListDefinitions($definitions) {
		if(M_Helper::isIteratorOfClass($definitions, 'AdminListDefinition')) {
			$this->assign('definitions', $definitions);
		}
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminSmartFolderChooseModule.tpl');
	}
}