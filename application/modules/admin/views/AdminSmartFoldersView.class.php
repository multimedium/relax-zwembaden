<?php
/**
 * AdminSmartFoldersView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminPageView.class.php';

/**
 * AdminSmartFoldersView
 * 
 * AdminSmartFoldersView, a subclass of {@link AdminPageView}, renders the overview 
 * of a Smart Folders.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminSmartFoldersView extends AdminPageView {
	/**
	 * Set Smart Folders
	 * 
	 * @access public
	 * @param ArrayIterator $smartFolders
	 * 		The collection of {@link AdminSmartFolder} instances to be shown
	 * 		in the view
	 * @return void
	 */
	public function setSmartFolders(ArrayIterator $smartFolders) {
		$this->assign('smartFolders', $smartFolders);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminSmartFolders.tpl');
	}
}