<?php
/**
 * AdminListFiltersView
 * 
 * Renders the filter interface for a list in the Admin Module. Typically, this
 * view is inserted into {@link AdminListView}
 * 
 * @package App
 * @subpackage Admin
 */
class AdminListFiltersView extends M_ViewHtml {
	/**
	 * Constructor
	 *
	 * @access public
	 * @param AdminListDefinition $definition
	 * 		The list definition that is to be used to render the view
	 * @return AdminListView
	 */
	public function __construct(AdminListDefinition $definition) {
		$this->assign('definition', $definition);
	}
	
	/**
	 * Set smart folder
	 * 
	 * Optionally, the view will also accept a reference to a given smart folder.
	 * This is necessary to update the collection of filters in the smart folder,
	 * when changes are made by the user.
	 * 
	 * @access public
	 * @param AdminSmartFolder $folder
	 * 		The smart folder in which to save changes to the collection of
	 * 		filter values.
	 * @return void
	 */
	public function setSmartFolder(AdminSmartFolder $folder) {
		$this->assign('smartFolder', $folder);
	}
	
	/**
	 * Set List Filter Definitions
	 * 
	 * Sets the collection of {@link AdminListFilterDefinition} instances that
	 * has been applied to the list.
	 * 
	 * @access public
	 * @param ArrayIterator $filters
	 * 		The collection of {@link AdminListFilterDefinition} instances
	 * @return void
	 */
	public function setListFilterDefinitions(ArrayIterator $filters) {
		$this->assign('appliedFilters', $filters);
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * Magic method, called by {@link M_ViewHtml} to do some preprocessing
	 * of the view, prior to rendering the HTML (template) resource.
	 * 
	 * @access public
	 * @return void
	 */
	protected function preProcessing() {
		if(! M_Helper::isInstanceOf($this->getVariable('definition'), 'AdminListDefinition')) {
			throw new M_ViewException(sprintf(
				'Cannot render filters view; missing list definition'
			));
		}
		
		if(! M_Helper::isIteratorOfClass($this->getVariable('appliedFilters'), 'AdminListFilterDefinition')) {
			throw new M_ViewException(sprintf(
				'Cannot render filters view; missing filter definitions, or unexpected filter definitions. Filters must be instances of AdminListFilterDefinition'
			));
		}
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is being used by 
	 * the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminListFilters.tpl');
	}
}