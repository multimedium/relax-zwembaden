<?php
/**
 * AdminMyAccountView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminPageView.class.php';

/**
 * AdminMyAccountView
 * 
 * @package App
 * @subpackage Admin
 */
class AdminMyAccountView extends AdminPageView {
	/**
	 * Set Administrator
	 * 
	 * This method can be used to set the administrator of which to show the 
	 * account information.
	 * 
	 * @access public
	 * @param Administrator $administrator
	 * @return void
	 */
	public function setAdministrator(Administrator $administrator) {
		$this->assign('administrator', $administrator);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Preprocessing
	 * 
	 * Magic method, called by {@link M_ViewHtml} to do some preprocessing
	 * of the view, prior to rendering the HTML (template) resource.
	 * 
	 * @access public
	 * @return void
	 */
	protected function preProcessing() {
		if(! $this->getVariable('administrator')) {
			throw new M_ViewException(sprintf(
				'Cannot render the My Account View without an Administrator object; see %s::%s()',
				__CLASS__,
				'setAdministrator'
			));
		}
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminMyAccount.tpl');
	}
}