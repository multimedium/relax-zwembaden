<?php
/**
 * AdminPasswordStrengthView
 * 
 * @package App
 * @subpackage Admin
 */
class AdminPasswordStrengthView extends M_ViewHtml {
	/**
	 * Set password strength
	 * 
	 * @access public
	 * @param int $strength
	 * @return void
	 */
	public function setStrength($strength) {
		$this->assign('strength', $strength);
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Preprocessing
	 * 
	 * @see M_ViewHtml::_render()
	 * @access protected
	 * @return void
	 */
	protected function preProcessing() {
		$this->assign(
			'roundedStrength', 
			M_Helper::getIntegerClosestNeighbour(
				5, 
				$this->getVariable('strength')
			)
		);
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminPasswordStrength.tpl');
	}
}