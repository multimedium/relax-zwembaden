<?php
/**
 * AdminFormDataObjectView
 * 
 * Renders the view of the form {@AdminFormDataObject}. This form is used to
 * edit data objects, in multimanage.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminFormDataObjectView extends AdminViewWithHelpers {
	
	/* -- CONSTRUCTORS -- */
	
	/**
	 * Constructor
	 * 
	 * @access public
	 * @param AdminFormDataObject $form
	 * 		The form to be rendered for view
	 * @return AdminFormDataObjectView
	 */
	public function __construct(AdminFormDataObject $form) {
		$this->setAdminFormDataObject($form);
	}
	
	/* -- SETTERS -- */
	
	/**
	 * Set form
	 * 
	 * Sets the {@link AdminFormDataObject} that is to be rendered for view.
	 * 
	 * @access public
	 * @param AdminFormDataObject $form
	 * 		The form that is to be rendered for view
	 * @return void
	 */
	public function setAdminFormDataObject($form) {
		$this->_form = $form;
	}
	
	/* -- GETTERS -- */
	
	/**
	 * Get form
	 * 
	 * This method will provide with the {@link AdminFormDataObject} that is to
	 * be rendered for view.
	 * 
	 * @access public
	 * @return AdminFormDataObject $form
	 * 		The form that is to be rendered for view
	 */
	public function getAdminFormDataObject() {
		return $this->_form;
	}
	
	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Preprocessing
	 * 
	 * Magic method, called by {@link M_ViewHtml} to do some preprocessing
	 * of the view, prior to rendering the HTML (template) resource.
	 * 
	 * @access public
	 * @return void
	 */
	protected function preProcessing() {
		// Get the form:
		$form = $this->getAdminFormDataObject();
		
		// Ask the form for the data object:
		$dataObject = $form->getDataObject();
			
		// Prepare the (empty) collection of values in the reference locale
		$referenceValues = array();
		
		// If the form is being edited in a locale:
		if($form->hasLocale()) {
			// Then, we ask the form for the reference locale:
			$referenceLocale = $form->getLocaleReference();
			
			// For each of the translated fields in the data object:
			foreach($dataObject->getMapper()->getLocalizedFields() as $field) {
				// The getter method of the field is $getter
				$getter = (string) $field->getter;
				
				// Get the value, in the reference locale
				$referenceValues[(string) $field->name] = $dataObject->$getter($referenceLocale);
			}
		}
		
		// Also, we get the locales in which the data object is available:
		$locales = $dataObject->getLocalesOfLocalizedTexts()->getArrayCopy();
		$tempLocales = array();
		
		// For each of the installed locales
		foreach(M_LocaleMessageCatalog::getInstalled() as $locale) {
			// Check if the data object has been translated in this locale:
			$tempLocales[$locale] = in_array($locale, $locales);
		}
		
		// Assign presentation variables:
		$this->assign('dataObject', $dataObject);
		$this->assign('locales', $tempLocales);
		$this->assign('form', $form);
		$this->assign('referenceValues', $referenceValues);
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminFormDataObject.tpl');
	}
}