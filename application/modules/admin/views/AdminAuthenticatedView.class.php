<?php
/**
 * AdminAuthenticatedView
 * 
 * AdminAuthenticatedView, a subclass of {@link M_ViewHtml}, renders the
 * AJAX Response that tells whether or not the user has been authenticated.
 * 
 * @package App
 * @subpackage Admin
 */
class AdminAuthenticatedView extends M_ViewHtml {
	/**
	 * Set flag: is authenticated
	 * 
	 * @access public
	 * @param bool $flag
	 * 		Set to TRUE if authenticated, FALSE if not
	 * @return void
	 */
	public function setIsAuthenticated($flag) {
		$this->assign('isAuthenticated', (bool) $flag);
	}
	
	/**
	 * Display the view
	 * 
	 * @see M_ViewHtml::display()
	 * @access public
	 * @return void
	 */
	public function display() {
		M_Header::send(M_Header::CONTENT_TYPE_XML);
		parent::display();
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminAuthenticated.tpl');
	}
}