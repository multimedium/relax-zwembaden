<?php
/**
 * AdminModulesView
 * 
 * @package App
 * @subpackage Admin
 */

// Load superclass
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'AdminPageView.class.php';

/**
 * AdminModulesView
 * 
 * @package App
 * @subpackage Admin
 */
class AdminModulesView extends AdminPageView {
	/**
	 * Set collection of modules
	 * 
	 * @access public
	 * @param ArrayIterator|array $modules
	 * 		The collection of modules to be shown in the view
	 * @return void
	 */
	public function setModules($modules) {
		if(M_Helper::isIterator($modules)) {
			$this->assign('modules', $modules);
		}
	}
	
	/* -- PROTECTED/PRIVATE -- */
	
	/**
	 * Preprocessing
	 * 
	 * Magic method, called by {@link M_ViewHtml} to do some preprocessing
	 * of the view, prior to rendering the HTML (template) resource.
	 * 
	 * @access public
	 * @return void
	 */
	protected function preProcessing() {
		// If no modules have been provided, we default the selection to the
		// modules that are available in the application
		if(! $this->getVariable('modules')) {
			$this->assign('modules', AdminApplication::getModules());
		}
	}
	
	/**
	 * Get template
	 * 
	 * Will return the {@link M_ViewHtmlResource} instance that is
	 * being used by the view to render the HTML Source Code.
	 * 
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath('admin').'/AdminModules.tpl');
	}
}