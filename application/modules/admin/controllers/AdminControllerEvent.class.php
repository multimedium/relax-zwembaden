<?php
/**
 * AdminControllerEvent class
 * 
 * AdminControllerEvent is used to dispatch events that are triggered by the 
 * {@link AdminController} class.
 * 
 * @package Core
 */
class AdminControllerEvent extends M_Event {
	
	/* -- CONSTANTS -- */
	
	/* -- EVENT TYPES -- */
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about the fact that the
	 * Admin Module has been booted
	 * 
	 * @var string
	 */
	const BOOT = 'boot';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about a view
	 * (subclass of {@link AdminPageView}) being rendered. 
	 * 
	 * @var string
	 */
	const PAGE_VIEW = 'page-view';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about the fact that an 
	 * instance of {@link M_DataObject} is about to be saved.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_BEFORE_SAVE = 'data-object-before-save';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about the fact that an 
	 * instance of {@link M_DataObject} has been saved.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_SAVE = 'data-object-save';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about the fact that an 
	 * instance of {@link M_DataObject} is about to be edited.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_BEFORE_EDIT = 'data-object-before-edit';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about the fact that an 
	 * instance of {@link M_DataObject} has been edited.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_EDIT = 'data-object-edit';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about the fact that an
	 * instance of {@link M_DataObject} is about to be created.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_BEFORE_CREATE = 'data-object-before-create';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about a {@link M_DataObject}
	 * having been created.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_CREATE = 'data-object-create';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about the fact that an
	 * instance of {@link M_DataObject} is about to be deleted.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_BEFORE_DELETE = 'data-object-before-delete';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about a {@link M_DataObject}
	 * having been deleted.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_DELETE = 'data-object-delete';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about the fact that an
	 * attribute of an instance of {@link M_DataObject} is about to be toggled.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_BEFORE_TOGGLE = 'data-object-before-toggle';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about a toggled attribute
	 * in {@link M_DataObject}.
	 * 
	 * @var string
	 */
	const DATA_OBJECT_TOGGLE = 'data-object-toggle';

	/**
	 * Event Type
	 *
	 * Triggered in {@link AdminController}, to notify about the list view (of a
	 * data object) about to be rendered
	 *
	 * @var string
	 */
	const DATA_OBJECT_BEFORE_LIST_VIEW = 'data-object-before-list-view';
	
	/**
	 * Event Type
	 * 
	 * Triggered in {@link AdminController}, to notify about the list view (of a 
	 * data object) being rendered
	 * 
	 * @var string
	 */
	const DATA_OBJECT_LIST_VIEW = 'data-object-list-view';
	
	/* -- PROPERTIES -- */
	
	/**
	 * DataObject
	 * 
	 * This property stores the data-object for which the event is being triggered
	 * 
	 * @see AdminControllerEvent::getDataObject()
	 * @see AdminControllerEvent::setDataObject()
	 * @access private
	 * @var M_DataObject
	 */
	private $_dataObject;

	/**
	 * DataObject Mapper
	 *
	 * This property stores the data object mapper for which the event is being
	 * triggered
	 *
	 * @access private
	 * @var M_DataObjectMapper
	 */
	private $_dataObjectMapper;
	
	/**
	 * Form
	 * 
	 * This property stores the form for which the event is being triggered
	 * 
	 * @see AdminControllerEvent::getForm()
	 * @see AdminControllerEvent::setForm()
	 * @access private
	 * @var M_Form
	 */
	private $_form;
	
	/**
	 * View
	 * 
	 * This property stores the view for which the event is being triggered
	 * 
	 * @see AdminController::getView()
	 * @see AdminController::setView()
	 * @access private
	 * @var M_ViewHtml
	 */
	private $_view;
	
	/**
	 * Module
	 * 
	 * This property stores the module for which the event is being triggered
	 * 
	 * @access private
	 * @var AdminModule
	 */
	private $_module;
	
	/**
	 * Data Object ID
	 * 
	 * This property stores the Data Object ID for which the event is being triggered
	 * 
	 * @access private
	 * @var AdminModule
	 */
	private $_dataObjectId;
	
	/* -- GETTERS -- */
	
	/**
	 * Get {@link AdminController} instance
	 * 
	 * This method is an alias for {@link M_Event::getTarget()}. It will provide
	 * with the object that triggered the event (in other words, it will provide
	 * with the instance of {@link AdminController} that triggered the event).
	 * 
	 * @access public
	 * @return AdminController
	 */
	public function getAdminController() {
		return $this->getTarget();
	}

	/**
	 * Get {@link M_DataObject} instance
	 *
	 * This method will provide with the instance of {@link M_DataObject} for
	 * which the event has been triggered. By releasing the {@link M_DataObject},
	 * other objects can react to significant events related to that object.
	 *
	 * @access public
	 * @return M_DataObject
	 */
	public function getDataObject() {
		return $this->_dataObject;
	}

	/**
	 * Get {@link M_DataObjectMapper} instance
	 *
	 * This method will provide with the instance of {@link M_DataObjectMapper} for
	 * which the event has been triggered. By releasing {@link M_DataObjectMapper},
	 * other objects can react to significant events related to that object.
	 *
	 * @access public
	 * @return M_DataObject
	 */
	public function getDataObjectMapper() {
		return $this->_dataObjectMapper;
	}
	
	/**
	 * Get {@link M_Form} instance
	 * 
	 * This method will provide with the instance of {@link M_Form} for
	 * which the event has been triggered. By releasing the {@link M_Form},
	 * other objects can react to significant events related to that form.
	 * 
	 * @access public
	 * @return M_Form
	 */
	public function getForm() {
		return $this->_form;
	}
	
	/**
	 * Get {@link M_ViewHtml} instance
	 * 
	 * This method will provide with the instance of {@link M_ViewHtml} for
	 * which the event has been triggered. By releasing the {@link M_ViewHtml},
	 * other objects can react to significant events related to that view.
	 * 
	 * @access public
	 * @return M_ViewHtml
	 */
	public function getView() {
		return $this->_view;
	}
	
	/**
	 * Get {@link AdminModule} instance
	 * 
	 * This method will provide with the instance of {@link AdminModule} for
	 * which the event has been triggered. By releasing the {@link AdminModule},
	 * other objects can react to significant events related to that module.
	 * 
	 * @access public
	 * @return AdminModule
	 */
	public function getModule() {
		return $this->_module;
	}
	
	/**
	 * Get data object id
	 * 
	 * @access public
	 * @return AdminModule
	 */
	public function getDataObjectId() {
		return $this->_dataObjectId;
	}
	
	/* -- SETTERS -- */

	/**
	 * Set {@link M_DataObject} instance
	 *
	 * This method allows you to set the instance of {@link M_DataObject} for
	 * which the event is being triggered.
	 *
	 * @see AdminControllerEvent::getDataObject()
	 * @access public
	 * @param M_DataObject $object
	 * @return void
	 */
	public function setDataObject(M_DataObject $object) {
		$this->_dataObject = $object;
	}

	/**
	 * Set {@link M_DataObjectMapper} instance
	 *
	 * This method allows you to set the instance of {@link M_DataObjectMapper} for
	 * which the event is being triggered.
	 *
	 * @see AdminControllerEvent::getDataObjectMapper()
	 * @access public
	 * @param M_DataObjectMapper $mapper
	 * @return void
	 */
	public function setDataObjectMapper(M_DataObjectMapper $mapper) {
		$this->_dataObjectMapper = $mapper;
	}
	
	/**
	 * Set {@link M_Form} instance
	 * 
	 * This method allows you to set the instance of {@link M_Form} for
	 * which the event is being triggered.
	 * 
	 * @see AdminControllerEvent::getForm()
	 * @access public
	 * @param M_Form $form
	 * @return void
	 */
	public function setForm(M_Form $form) {
		$this->_form = $form;
	}
	
	/**
	 * Set {@link M_ViewHtml} instance
	 * 
	 * This method allows you to set the instance of {@link M_ViewHtml} for
	 * which the event is being triggered.
	 * 
	 * @see AdminControllerEvent::getView()
	 * @access public
	 * @param M_ViewHtml $view
	 * @return void
	 */
	public function setView(M_ViewHtml $view) {
		$this->_view = $view;
	}
	
	/**
	 * Set {@link AdminModule} instance
	 * 
	 * This method allows you to set the instance of {@link AdminModule} for
	 * which the event is being triggered.
	 * 
	 * @see AdminControllerEvent::getModule()
	 * @access public
	 * @param AdminModule $module
	 * @return void
	 */
	public function setModule(AdminModule $module) {
		$this->_module = $module;
	}
	
	/**
	 * Set Data Object ID
	 * 
	 * @see AdminControllerEvent::getDataObjectId()
	 * @access public
	 * @param string $dataObjectId
	 * @return void
	 */
	public function setDataObjectId($dataObjectId) {
		$this->_dataObjectId = $dataObjectId;
	}
}