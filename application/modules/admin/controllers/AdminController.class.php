<?php
/**
 * AdminController
 * 
 * @package App
 * @subpackage Admin
 */
class AdminController extends M_Controller {
	
	/* -- PROPERTIES -- */
	
	/**
	 * Administrator
	 * 
	 * Stores the authenticated administrator.
	 * 
	 * @access private
	 * @var Administrator
	 */
	private $_administrator = NULL;
	
	/**
	 * ACL
	 * 
	 * Stores the ACL Object, which is used to check permissions in the administration
	 * 
	 * @access private
	 * @var M_Acl
	 */
	private $_acl;
	
	/* -- PUBLIC -- */
	
	/**
	 * Magic Method: Router
	 * 
	 * Adds routing rules to the {@link M_ControllerDispatcher} instance. For 
	 * more info on routing rules, read the available documentation on the
	 * method {@link M_ControllerDispatcher::setRoutingRules()}.
	 * 
	 * @access public
	 * @return array
	 */
	public function __router() {
		return array(
			'(:string)/overview' => 'overview/$1',
			'(:string)/(:string)/new' => 'edit/$1/0/$2'
		);
	}
	
	/**
	 * Boot Admin Module (Construct Controller)
	 * 
	 * @access public
	 * @return AdminController
	 */
	public function __construct() {
		// Reset priority list: if we do not reset this, texts are inserted for all locales.
		M_Locale::setCategory(M_Locale::LANGUAGE, array());
		
		// Initiate the current Authentication scheme. We check if we can
		// identify the administrator. If we can't:
		if(! $this->_initAuth()) {
			// Get relative path of current request:
			$relativePath = M_Helper::ltrimCharlist(
				M_ControllerDispatcher::getInstance()->getPath(),
				'/'
			);

			// We check if the Multikey module has been installed. If not, we
			// fall back to the login module:
			$loginRelativePath = $this->_getLoginRelativePath();

			// We redirect the visitor (if not already redirected):
			$c1 = (strncmp($relativePath, 'admin/route/login', 17));
			$c2 = (strncmp($relativePath, 'admin/route/multikey', 20));
			if($c1 && $c2) {

				// Some requests are okay if not authenticated. Check if the current
				// request is not one of those

				// For new Admin versions: these urls are defined in config
				$config = M_Application::getConfigOfModule('admin');
				if ($config->__isset('allowUnAuthenticatedAccess')) {
					$allowUnAuthenticated = $config->allowUnAuthenticatedAccess->toArray();
				}
				
				// Older ones have these url's hardcoded:
				else {
					$allowUnAuthenticated = array(
						'admin/authenticated',
						'admin/route/media/uploadMediaItem'
					);
				}

				// Check if the current path exists in the array which holds
				// all the paths for wich the users doesn't have to be
				// authenticated. If not: redirect to login page
				if(!in_array($relativePath, $allowUnAuthenticated)) {
					// Redirect!
					M_Header::redirect($loginRelativePath . '?route=' . M_Request::getUri()->getRelativePath());
				}
			}
		}
		
		// Set locale:
		// M_Locale::setCategory(M_Locale::LANG, 'nl');
		
		// Load core classes of Admin Module
		self::_loadAdminCore();
		
		// Also, load core classes of other modules
		self::_loadAdminCoreModules();
		
		// Load Event Listeners:
		self::_loadAdminControllerEventListeners();
		
		// Initiate ACL
		$this->_getAcl();
		
		// Dispatch an event, to notify about the Admin Module having been booted:
		$this->dispatchEvent(new AdminControllerEvent(AdminControllerEvent::BOOT, $this));
	}
	
	/**
	 * Check identity
	 * 
	 * Will tell whether or not the current session has been authenticated. This
	 * request is being used by javascript (AJAX) every time an asynchronous
	 * request is made in javascript with $.M_AjaxRequest()
	 * 
	 * @access public
	 * @return void
	 */
	public function authenticated() {
		M_Loader::loadView('AdminAuthenticatedView', 'admin');
		$view = new AdminAuthenticatedView;
		$view->setIsAuthenticated($this->_getAdministratorId() > 0);
		$view->display();
	}

	/**
	 * Handle error
	 *
	 * @access public
	 * @author Ben Brughmans
	 * @return void
	 */
	public function handleError(Exception $e) {
		//write to log
		M_Console::getInstance()->write(sprintf(
			'Exception %s thrown: "%s\n%s"',
			$e->getCode(),
			$e->getMessage(),
			$e->getTraceAsString()
		));

		//try to log the exception in the database, if this fails we log it
		//in console
		if (M_Helper::isInstanceOf($e, 'M_Exception')) {
			try {
				$e->log();
			}catch(M_Exception $e) {
				M_Console::getInstance()->write(sprintf(
					'Cannot log exception in database with message "%s"',
					$e->getMessage())
				);
			}
		}

		M_Loader::loadView('AdminErrorView', 'admin');
		$view = new AdminErrorView();
		$view->setException($e);
		$view->display();
	}
	
	/**
	 * Admin homepage
	 * 
	 * @access public
	 * @return void
	 */
	public function index() {
		M_Header::redirect('admin/my-account');
	}
	
	/**
	 * Admin Logout
	 * 
	 * @access public
	 * @return void
	 */
	public function logout() {
		// Get the M_Auth singleton:
		$auth = M_Auth::getInstance();
		
		// Destroy the authenticated session that is being maintained in the
		// session:
		$auth->getStorage()->destroy();

		// Get the relative path to the login page:
		$route = $this->_getLoginRelativePath();

		// We try to get the referrer page:
		$uri = new M_Uri($_SERVER['HTTP_REFERER']);

		// If a referral is available
		if($uri->isInternal()) {
			// Then, add to the redirect url:
			$route .= '?route=' . $uri->getRelativePath();
		}
		
		// Redirect the user to the login page:
		M_Header::redirect($route);
	}
	
	/**
	 * Admin: about
	 * 
	 * @access public
	 * @return void
	 */
	public function about() {
		$this->_getAdminPageView('AdminAboutView')->display();
	}
	
	/**
	 * Admin: about (AJAX Response)
	 * 
	 * @access public
	 * @return void
	 */
	public function aboutAjax() {
		$view = $this->_getAdminPageView('AdminAboutView');
		$view->setIsAjaxResponse(TRUE);
		$view->display();
	}
	
	/**
	 * Admin: modules
	 * 
	 * @access public
	 * @return void
	 */
	public function modules() {
		$view = $this->_getAdminPageView('AdminModulesView');
		$view->setModules(AdminApplication::getModules());
		$view->display();
	}
	
	/**
	 * Admin: modules (AJAX Response)
	 * 
	 * @access public
	 * @return void
	 */
	public function modulesAjax() {
		$view = $this->_getAdminPageView('AdminModulesView');
		$view->setModules(AdminApplication::getModules());
		$view->setIsAjaxResponse(TRUE);
		$view->display();
	}
	
	/**
	 * Admin: Module Release Notes
	 * 
	 * @access public
	 * @param string $moduleId
	 * @return void
	 */
	public function releases($moduleId) {
		$view = $this->_getAdminPageView('AdminModuleReleaseNotesView');
		$view->setModule(new AdminModule($moduleId));
		$view->display();
	}
	
	/**
	 * Save a new smart folder (title only)
	 * 
	 * This method allows to create a new smart folder in the interface, via
	 * an AJAX Request. It will save the new smart folder with the name
	 * that is provided by Request Variable "name".
	 * 
	 * @access public
	 * @return void
	 */
	public function newSmartFolder() {
		// Load AdminSmartFolder model
		M_Loader::loadDataObject('AdminSmartFolder', 'admin');
		
		// Construct a new smart folder:
		$smartFolder = new AdminSmartFolder;
		
		// Set the title of the smart folder:
		$smartFolder->setName(M_Request::getVariable('name', t('Untitled')));
		
		// Set the owner (Administrator)
		$smartFolder->setAdministratorId($this->_getAdministratorId());
		
		// Save it to the database:
		if($smartFolder->save()) {
			// If the smart folder has been saved successfully, we
			// load the new-folder view:
			M_Loader::loadView('AdminNewSmartFolderView', 'admin');
			$view = new AdminNewSmartFolderView();
			$view->setSmartFolder($smartFolder);
			$view->display();
		}
	}
	
	/**
	 * Route to module controller
	 * 
	 * Used to route a request to the module's Admin Controller.
	 * 
	 * NOTE:
	 * If the module does not provide with an implementation for the request,
	 * an exception will be thrown. This will cause {@link AdminController::handleError()}
	 * to be called.
	 * 
	 * @throws M_Exception
	 * @access public
	 * @param string $module
	 * 		The module ID
	 * @param string $method
	 * 		The message (method) to be sent to the module's Admin Controller
	 * @param ...
	 * 		A variable number of arguments to be passed along with the
	 * 		message (method arguments)
	 * @return void
	 */
	public function route($module, $method = 'index') {
		// Collect the argument(s)
		$arg = array();
		for($i = 2, $numberOfArgs = func_num_args(); $i < $numberOfArgs; $i ++) {
			$arg[] = func_get_arg($i);
		}
		
		// If the request could not be handled by the module's Admin Controller:
		if(! $this->_dispatchToModuleController($module, $method, $arg)) {
			// Throw an exception:
			throw new M_Exception(sprintf(
				'Admin cannot handle request %s',
				__FUNCTION__ . '/' . $module . '/' . $method . '/' . implode('/', $arg)
			));
		}
	}
	
	/**
	 * My Account
	 * 
	 * Will construct the view that provides the administrator with an overview
	 * of his/her personal account data. Also, this view should provide with
	 * access to the Edit My Account form, and Change Password form.
	 * 
	 * @access public
	 * @return void
	 */
	public function myAccount() {
		$view = $this->_getAdminPageView('AdminMyAccountView');
		$view->setAdministrator($this->_getAdministrator());
		$view->display();
	}
	
	/**
	 * My Account: vcard download
	 * 
	 * @access public
	 * @return void
	 */
	public function myAccountVcard() {
		// Construct a vCard, based on My Account
		$vcard = $this->_getAdministrator()->getContact()->getVcard();
		
		// Offer the vCard as a download:
		M_Header::sendDownloadFromString($vcard->toString(), new M_File($vcard->getSuggestedFilename()));
	}
	
	/**
	 * My Account: Edit
	 * 
	 * Will construct the form that allows the administrator to edit his/her
	 * personal account.
	 * 
	 * @access public
	 * @return void
	 */
	public function editMyAccount() {
		// Load the form:
		M_Loader::loadForm('AdminFormMyAccount', 'admin');
		
		// Load the view:
		M_Loader::loadView('AdminFormMyAccountView', 'admin');
		
		// Construct the form:
		$form = new AdminFormMyAccount($this->_getAdministrator());
		
		// Set Redirection URL
		$form->setRedirectUrl(M_Form::SUCCESS, 'admin/my-account');
		$form->setRedirectUrl(M_Form::FAILURE, 'admin/my-account');
		
		// Set result messages
		$form->setResultMessage(M_Form::SUCCESS, t('Your account has been updated successfully!'));
		$form->setResultMessage(M_Form::FAILURE, t('Your account could not have been updated! Please try again later.'));
		
		// If the form could not have been completed
		// (not sent yet, or validation errors ocurred)
		if(! $form->run()) {
			// Construct the view:
			$view = $this->_getAdminPageView('AdminFormMyAccountView');
			$view->setFormMyAccount($form);
			$view->display();
		}
	}
	
	/**
	 * My Account: Change Password
	 * 
	 * Will construct the form that allows the administrator to edit his/her
	 * password.
	 * 
	 * @access public
	 * @return void
	 */
	public function changePassword() {
		// Load the form:
		M_Loader::loadForm('AdminFormChangePassword', 'admin');
		
		// Load the view:
		M_Loader::loadView('AdminFormChangePasswordView', 'admin');
		
		// Construct the form:
		$form = new AdminFormChangePassword($this->_getAdministrator());
		
		// Set Redirection URL
		$form->setRedirectUrl(M_Form::SUCCESS, 'admin/my-account');
		$form->setRedirectUrl(M_Form::FAILURE, 'admin/my-account');
		
		// Set result messages
		$form->setResultMessage(M_Form::SUCCESS, t('Your new password has been saved successfully!'));
		$form->setResultMessage(M_Form::FAILURE, t('Your password could not have been changed! Please try again later.'));
		
		// If the form could not have been completed
		// (not sent yet, or validation errors ocurred)
		if(! $form->run()) {
			// Construct the view:
			$view = $this->_getAdminPageView('AdminFormChangePasswordView');
			$view->setFormChangePassword($form);
			$view->display();
		}
	}
	
	/**
	 * Get password strength (AJAX Response)
	 * 
	 * @access public
	 * @return void
	 */
	public function passwordStrength() {
		// Get the password:
		$password = M_Request::getVariable('password', FALSE, M_Request::TYPE_STRING);
		if($password) {
			// Load the view
			M_Loader::loadView('AdminPasswordStrengthView', 'admin');
			
			// Construct the view
			$view = new AdminPasswordStrengthView();
			$view->setStrength(M_Password::getStrength($password));
			$view->display();
		}
	}
	
	/**
	 * Format a value as currency
	 * 
	 * @access public
	 * @return void
	 * @author Ben Brughmans
	 */
	public function formatCurrency() {
		// Get the field-value and the currency
		$value = M_Request::getVariable('data', FALSE, M_Request::TYPE_STRING);
		$currencyCode = M_Request::getVariable('currency', FALSE, M_Request::TYPE_STRING);
		
		if ($value !== FALSE && strlen($value) > 0) {
			try{
				$number = M_Number::constructWithString($value);
				$number->setNumber(round($number->getNumber(), 2));
				if ($currencyCode !== FALSE) {
					$r = $number->toCurrencyString($currencyCode);
				}else {
					$r = $number->toString();
				}
			}catch (M_Exception $e) {
				$r = false;
			}
			
			echo M_Helper::jsonEncode($r);
		}
	}
	
	/**
	 * Format a value as percent
	 * 
	 * @access public
	 * @return void
	 * @author Ben Brughmans
	 */
	public function formatPercent() {
		// Get the field-value and the currency
		$value = M_Request::getVariable('data', FALSE, M_Request::TYPE_STRING);
		if ($value !== FALSE && strlen($value) > 0) {
			try{
				$number = M_Number::constructWithString($value);
				$r = $number->toPercentString();
			}catch (M_Exception $e) {
				$r = false;
			}
			
			echo M_Helper::jsonEncode($r);
		}
	}

	/**
	 * Format a string as a path element
	 *
	 * NOTE: pass a variable "shorten" to shorten to total amount of words
	 * 
	 * @see M_Uri::getPathElementFromString()
	 * @return void
	 * @author Ben Brughmans
	 */
	public function formatUriPathElement() {
		// Get the field-value and the currency
		$value = M_Request::getVariable('data', FALSE, M_Request::TYPE_STRING);
		$shorten = M_Request::getVariable('shorten', FALSE, M_Request::TYPE_STRING);

		if ($value !== FALSE && strlen($value) > 0) {
			try{
				$r = M_Uri::getPathElementFromString($value, $shorten);
			}catch (M_Exception $e) {
				$r = false;
			}

			echo M_Helper::jsonEncode($r);
		}
	}
	
	/**
	 * Delete Locale
	 * 
	 * This method can be used to clear all localized fields of a dataObejct
	 * for a given locale.
	 * The method sets all localized fields (of a given locale) to an empty strings
	 * which makes sure the localized fields are removed.
	 * 
	 * @param string $moduleId
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()} 
	 * @param string $id
	 *		The Id of the Data Object
	 * @param string $locale
	 *		The locale of the Data Object
	 * @return boolean
	 */
	public function deleteLocale($moduleId, $dataObjectId, $id, $locale) {
		
		// Get the definition of the list:
		$definition = $this->_getListDefinition($moduleId, $dataObjectId);
		
		// Construct the mapper for a given moduleId
		$mapper = M_Loader::getDataObjectMapper($definition->getModule()->getDataObjectClassName($dataObjectId), $moduleId);
		
		// Get the dataObject for a given alphaId
		$dataObject = $mapper->getById($id);
		
		// Return false if no text or locale could be found
		if( is_null($dataObject) || is_null($locale)) {
			return false;
		}
		
		// Get all localized fields from the constructed mapper
		$fields = $mapper->getLocalizedFields();
		
		// Clear all localized fields by inserting empty strings
		foreach($mapper->getLocalizedFields() as $field) {
			
			// Convert the object element to a workable string
			$setter = (string) $field->setter;
			
			// Clear the current localized field for a given locale
			$dataObject->$setter('', $locale);
		}
		
		// Save the dataObject to store all changes
		$dataObject->save();

		// Finally redirect  back to the admin
		M_Header::redirect(M_Request::getLink('admin/overview/' . $moduleId . '/' . $dataObjectId));
		
	}
	
	/**
	 * Overview/List of items in module
	 *
	 * @access public
	 * @param string $moduleId
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()} 
	 * @return void
	 */
	public function overview($moduleId, $dataObjectId = NULL) {
		$this->_overview($moduleId, $dataObjectId);
	}
	
	/**
	 * Overview/List of items in module (AJAX Response)
	 *
	 * @access public
	 * @param string $moduleId
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()} 
	 * @return void
	 */
	public function overviewAjax($moduleId, $dataObjectId = NULL) {
		$this->_overview($moduleId, $dataObjectId, TRUE);
	}

	/**
	 * Smart Folders
	 * 
	 * Provides with an overview of the Smart Folders that have been saved by the
	 * administrator
	 * 
	 * @access public
	 * @return void
	 */
	public function smartFolders() {
		$this->_getSmartFoldersView()->display();
	}
	
	/**
	 * Smart Folders; AJAX Response
	 * 
	 * @see AdminController::smartFolders()
	 * @access public
	 * @return void
	 */
	public function smartFoldersAjax() {
		$view = $this->_getSmartFoldersView();
		$view->setIsAjaxResponse(TRUE);
		$view->display();
	}
	
	/**
	 * Edit Smart Folder
	 * 
	 * @access public
	 * @return void
	 */
	public function smartFolderEdit($id) {
		$this->_getFormView('admin', $id, 'folder')->display();
	}
	
	/**
	 * Delete Smart Folder
	 * 
	 * @access public
	 * @return void
	 */
	public function smartFolderDelete($id) {
		// Delete the item
		$this->_delete('admin', $id, 'folder');
		
		// Redirect to overview:
		M_Header::redirect('admin/smart-folders');
	}
	
	/**
	 * Smart Folder: Overview/List of items
	 * 
	 * @access public
	 * @param integer $id
	 * 		The ID of the Smart Folder
	 * @return void
	 */
	public function smartFolder($id) {
		$this->_smartFolder($id);
	}
	
	/**
	 * Smart Folder: Overview/List of items (AJAX Response)
	 * 
	 * @access public
	 * @param integer $id
	 * 		The ID of the Smart Folder
	 * @return void
	 */
	public function smartFolderAjax($id) {
		$this->_smartFolder($id, TRUE);
	}
	
	/**
	 * Smart Folder: define the module
	 * 
	 * @access public
	 * @param integer $smartFolderId
	 * @param string $moduleId
	 * @param string $dataObjectId
	 * @return void
	 */
	public function smartFolderDefineModule($smartFolderId, $moduleId, $dataObjectId) {
		// Load the Smart Folder model:
		M_Loader::loadDataObjectMapper('AdminSmartFolder', 'admin');
		
		// Construct a mapper, to fetch the requested Smart Folder:
		$mapper = new AdminSmartFolderMapper();
		
		// Get the requested Smart Folder:
		$folder = $mapper->getById((int) $smartFolderId);
		
		// If the folder has been found:
		if($folder) {
			// If the Smart Folder does not (yet) contain a reference
			// to module/data-object:
			if(! $folder->getModuleId()) {
				// We set the reference now:
				$folder->setModuleId($moduleId);
				$folder->setDataObjectId($dataObjectId);
				
				// We save the changes to the database:
				$folder->save();
			}
			
			// Redirect to the Smart Folder view:
			M_Header::redirect('admin/smart-folder/' . $folder->getId());
		}
	}
	
	/**
	 * Listed instances (when filter is applied; via ajax)
	 * 
	 * @uses AdminController::_instances()
	 * @access public
	 * @param string $moduleId
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()} 
	 * @return void
	 */
	public function instances($moduleId, $dataObjectId = NULL) {
		$this->_instances($moduleId, $dataObjectId);
	}
	
	/**
	 * Listed instances (smart folder, when filter is applied; via ajax)
	 * 
	 * @uses AdminController::_instances()
	 * @access public
	 * @param integer $smartFolderId
	 * 		The ID of the Smart Folder
	 * @return void
	 */
	public function instancesSmartFolder($smartFolderId) {
		// Load the Smart Folder model:
		M_Loader::loadDataObjectMapper('AdminSmartFolder', 'admin');
		
		// Construct a mapper, to fetch the requested Smart Folder:
		$mapper = new AdminSmartFolderMapper();
		
		// Get the requested Smart Folder:
		$folder = $mapper->getById((int) $smartFolderId);
		
		// If the folder has been found:
		if($folder) {
			// If the Smart Folder contains a reference to module/data-object:
			if($folder->getModuleId() && $folder->getDataObjectId()) {
				// Remove the filters that are currently attached to the smart folder:
				$folder->clearFilters();
				
				// Get the list definition;
				$definition = $this->_getListDefinition($folder->getModuleId(), $folder->getDataObjectId());
				
				// Add the filters to the smart folder:
				foreach($this->_getListFilterDefinitionsFromRequest($definition) as $filter) {
					$folder->addFilter($filter->getAdminSmartFolderFilter());
				}
				
				// Save the folder:
				$folder->save();
				
				// Output the instances in the smart folder:
				$this->_instances($folder->getModuleId(), $folder->getDataObjectId());
			}
		}
	}
	
	/**
	 * Listed instance (when list is scrolled through; via ajax)
	 * 
	 * @access public
	 * @param string $moduleId
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()} 
	 * @return void
	 */
	public function instanceRow($moduleId, $dataObjectId = NULL) {
		// Get the definition of the list
		$definition = $this->_getListDefinition($moduleId, $dataObjectId);

		// Note that, when instances are loaded in via AJAX, we do not populate
		// the definition file with data objects or callback functions...
		$definition->setEnablePopulations(FALSE);
		
		// Get applied filters:
		$filters = $this->_getListFilterDefinitionsFromRequest($definition);
		
		// Ask the list definition for the corresponding DataObjectMapper. Note 
		// that we apply the filters:
		$mapper = $definition->getMapper($filters);
		
		// If, with the collection of applied filters, the instances can be
		// sorted manually (drag-drop), then we sort the the instances by the
		// field that stores the order index number:
		if($definition->isInstancesDraggableForSorting($filters)) {
			// Add the sorting filter to the mapper:
			$mapper->addFilter(new M_DbQueryFilterOrder($definition->getSortDragFieldName(), 'ASC'));
		}
		
		// Add offset:
		$offset = M_Request::getVariable('offset', FALSE);
		if($offset !== FALSE) {
			$mapper->addFilter(new M_DbQueryFilterLimit($offset, M_Request::getVariable('count', 1)));
		}
		
		// Get the listed instances' view, and output:
		$this->_getListedInstanceRowView($definition, $mapper, $filters)->display();
	}
	
	/**
	 * Listed instance in smart folder (when list is scrolled through; via ajax)
	 * 
	 * @access public
	 * @param integer $smartFolderId
	 * 		The ID of the Smart Folder 
	 * @return void
	 */
	public function instanceRowSmartFolder($smartFolderId) {
		// Load the Smart Folder model:
		M_Loader::loadDataObjectMapper('AdminSmartFolder', 'admin');
		
		// Construct a mapper, to fetch the requested Smart Folder:
		$mapper = new AdminSmartFolderMapper();
		
		// Get the requested Smart Folder:
		$folder = $mapper->getById((int) $smartFolderId);
		
		// If the folder has been found:
		if($folder) {
			// If the Smart Folder contains a reference to module/data-object:
			if($folder->getModuleId() && $folder->getDataObjectId()) {
				// Get the definition of the list
				$definition = $this->_getListDefinition(
					$folder->getModuleId(), 
					$folder->getDataObjectId()
				);

				// Note that, when instances are loaded in via AJAX, we do not
				// populate the definition file with data objects or callback
				// functions...
				$definition->setEnablePopulations(FALSE);
				
				// Get applied filters (from smart folder):
				$filters = $this->_getListFilterDefinitionsFromSmartFolder($folder);
		
				// Ask the list definition for the corresponding DataObjectMapper. Note 
				// that we apply the filters:
				$mapper = $definition->getMapper($filters);
				
				// If, with the collection of applied filters, the instances can be
				// sorted manually (drag-drop), then we sort the the instances by the
				// field that stores the order index number:
				if($definition->isInstancesDraggableForSorting($filters)) {
					// Add the sorting filter to the mapper:
					$mapper->addFilter(new M_DbQueryFilterOrder($definition->getSortDragFieldName(), 'ASC'));
				}
				
				// Add offset:
				$offset = M_Request::getVariable('offset', FALSE);
				if($offset !== FALSE) {
					$mapper->addFilter(new M_DbQueryFilterLimit($offset, M_Request::getVariable('count', 1)));
				}
				
				// Get the listed instances' view, and output:
				$this->_getListedInstanceRowView($definition, $mapper, $filters)->display();
			}
		}
	}
	
	/**
	 * Filter interface elements
	 * 
	 * This method is used by the application, to generate the elements in the 
	 * filter interface.
	 * 
	 * @access public
	 * @param string $moduleId
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()} 
	 * @param integer $filterIndex
	 * 		The index number of the filter. This index number is used to get the
	 * 		definition of the filter, with {@link AdminListDefinition::getFilter()}
	 * @param integer $stepIndex
	 * 		The index number of the step in the filter interface. Accepted values:
	 * 		1 (step 1 of a given filter), 2 (step 2; only for filters of type "where")
	 * @return void
	 */
	public function filter($moduleId, $dataObjectId, $filterIndex, $stepIndex) {
		// Construct the module:
		$module = new AdminModule($moduleId);
		
		// Ask the module for the list definition of the requested data object
		$def = $module->getListDefinition($dataObjectId);
		
		// Ask the list definition for the requested filter:
		$filter = $def->getFilter($filterIndex);
		
		// If the filter has been found in the definition:
		if($filter) {
			// The way we render the filter interface, depends on the requested step:
			switch($stepIndex) {
				// Step 1 (Comparison operator / Filter Value)
				case 1:
					// Depending on the filter's type, we set the menu options 
					// that are to be rendered:
					if($filter->getType() == 'where') {
						$view = new AdminListFilterComparisonsView();
						$view->setFilterDefinition($filter);
						$view->display();
					} else {
						$view = new AdminListFilterValuesView();
						$view->setFilterDefinition($filter);
						$view->display();
					}
					break;
				
				// Step 2 (Filter value for filters with comparison operators)
				case 2:
					// Make sure the filter is of type "where"
					if($filter->getType() == 'where') {
						// Make sure a comparison operator has been provided:
						$operator = M_Request::getVariable('operator', FALSE, M_Request::TYPE_STRING);
						if($operator) {
							// Make sure the operator is supported by the filter:
							if($filter->isComparisonOperatorSupported($operator)) {
								// Construct the view:
								$view = new AdminListFilterComparisonValueView();
								$view->setFilterDefinition($filter);
								$view->setComparisonOperator($operator);
								$view->display();
							}
						}
					}
					// If the filter is of type "order"
					elseif($filter->getType() == 'order') {
						$view = new AdminListFilterSortingOrdersView();
						$view->setFilterDefinition($filter);
						$view->display();
					}
					break;
			}
		}
	}
	
	/**
	 * Toggle
	 * 
	 * @uses AdminController::_toggle()
	 * @access public
	 * @param string $moduleId
	 * 		The Module ID
	 * @param string $dataObjectId
	 * 		The Data Object ID
	 * @param integer $id
	 * 		The ID of the Data Object
	 * @param string $field
	 * 		The field of which to toggle the value
	 * @return void
	 */
	public function toggle($moduleId, $dataObjectId, $id, $field) {
		// Handle toggling:
		$this->_toggle($moduleId, $dataObjectId, $id, $field);
		
		// Redirect to overview:
		M_Header::redirect('admin/overview/' . $moduleId . '/' . $dataObjectId);
	}
	
	/**
	 * Toggle, via ajax
	 * 
	 * @see AdminController::toggle()
	 * @uses AdminController::_toggle()
	 * @access public
	 * @param string $moduleId
	 * 		The Module ID
	 * @param string $dataObjectId
	 * 		The Data Object ID
	 * @param integer $id
	 * 		The ID of the Data Object
	 * @param string $field
	 * 		The field of which to toggle the value
	 * @return void
	 */
	public function toggleAjax($moduleId, $dataObjectId, $id, $field) {
		// Handle toggling:
		$object = $this->_toggle($moduleId, $dataObjectId, $id, $field);
		
		// Construct the view that informs about the result of the toggle request:
		$view = new AdminToggleAjaxView();
		
		// If toggled successfully:
		if($object) {
			$view->setDataObject($object);
			$view->setNewToggledValue($object->$field);
			$view->setResult(TRUE);
		}
		// If failed to toggle:
		else {
			$view->setResult(FALSE);
		}
		
		// Display the view:
		$view->display();
	}
	
	/**
	 * Move instance
	 * 
	 * This method can be used to move an instance up- or downwards in a list. 
	 * Typically, it is used to manually sort a list of items in the database.
	 * 
	 * @access public
	 * @param string $moduleId
	 * 		The Module ID
	 * @param string $dataObjectId
	 * 		The Data Object ID
	 * @param string $field
	 * 		The field to which the new order should be saved
	 * @param integer $instanceId
	 * 		The ID of the Data Object instance
	 * @param string $diff
	 * 		The direction in which to move the instance, and the number of steps
	 * 		to move.
	 * @return void
	 */
	public function moveInstance($moduleId, $dataObjectId, $field, $instanceId, $diff) {
		// Move the instance:
		$this->_moveInstance(
			// In module:
			$moduleId, 
			// of data object:
			$dataObjectId, 
			// storing the new order index number in field:
			$field,
			// Affecting the instance:
			$instanceId,
			// Move distance:
			$diff,
			// Collection of filters:
			$this->_getListFilterDefinitionsFromRequest($this->_getListDefinition($moduleId, $dataObjectId))
		);
	}
	
	/**
	 * Move instance, in a Smart Folder
	 * 
	 * This method can be used to move an instance up- or downwards in a list. 
	 * This does the same thing as {@link AdminController::moveInstance()}, but
	 * this method is used inside a smart folder.
	 * 
	 * @access public
	 * @param integer $smartFolderId
	 * 		The ID of the Smart Folder
	 * @param string $field
	 * 		The field to which the new order should be saved
	 * @param integer $instanceId
	 * 		The ID of the Data Object instance
	 * @param string $diff
	 * 		The direction in which to move the instance, and the number of steps
	 * 		to move.
	 * @return void
	 */
	public function moveInstanceSmartFolder($smartFolderId, $field, $instanceId, $diff) {
		// Load the Smart Folder model:
		M_Loader::loadDataObjectMapper('AdminSmartFolder', 'admin');
		
		// Construct a mapper, to fetch the requested Smart Folder from the DB:
		$mapper = new AdminSmartFolderMapper();
		
		// Get the requested Smart Folder:
		$folder = $mapper->getById((int) $smartFolderId);
		
		// If the folder has been found:
		if($folder) {
			// Move the instance:
			$this->_moveInstance(
				// In module:
				$folder->getModuleId(), 
				// of data object:
				$folder->getDataObjectId(), 
				// storing the new order index number in field:
				$field,
				// Affecting the instance:
				$instanceId,
				// Move distance:
				$diff,
				// Collection of filters (from smart folder):
				$this->_getListFilterDefinitionsFromSmartFolder($folder)
			);
		}
	}
	
	/**
	 * Delete a data object
	 * 
	 * Will delete a given data object from the storage platform (probably DB).
	 * 
	 * @access public
	 * @param string $module
	 * 		The Module ID
	 * @param string $id
	 * 		The ID of the Data Object to be deleted
	 * @param string $dataObjectId
	 * 		The Data Object ID
	 * @return void
	 */
	public function delete($module, $id, $dataObjectId = NULL) {
		// Delete the item. If successful:
		if($this->_delete($module, $id, $dataObjectId)) {
			// Add a message to the messenger:
			$messenger = M_Messenger::getInstance();
			$messenger->addMessage(t('The selected item has been deleted successfully'), M_Messenger::SUCCESS);
		}
		// If failed:
		else {
			// Add a message to the messenger:
			$messenger = M_Messenger::getInstance();
			$messenger->addMessage(t('<strong>Error!</strong> The selected item could not have been deleted!'), M_Messenger::FAILURE);
		}
		
		// Redirect to overview:
		M_Header::redirect('admin/overview/' . $module . '/' . $dataObjectId);
	}
	
	/**
	 * Delete a data object (via AJAX)
	 * 
	 * @access public
	 * @param string $module
	 * 		The Module ID
	 * @param string $id
	 * 		The ID of the Data Object to be deleted
	 * @param string $dataObjectId
	 * 		The Data Object ID
	 * @return void
	 */
	public function deleteAjax($module, $id, $dataObjectId = NULL) {
		// Delete the item.
		$rs = $this->_delete($module, $id, $dataObjectId);
		
		// Construct the view, to inform about the result:
		$view = new AdminDeleteDataObjectAjaxResponseView();
		$view->setResult($rs);
		$view->setDataObjectId($id);
		$view->display();
	}
	
	/**
	 * Create/Edit
	 * 
	 * Will construct the form that allows the user to create/edit an item
	 * in a given module.
	 * 
	 * @access public
	 * @param string $module
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param mixed $id
	 * 		The ID of the item
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()}
	 * @return void
	 */
	public function edit($module, $id, $dataObjectId = NULL) {
		// If the request is not handled by the module's controller:
		if(! $this->_dispatchToModuleController($module, __FUNCTION__ . ucfirst($dataObjectId), array($id, $dataObjectId))) {
			// We render the view ourselves:
			$view = $this->_getFormView($module, $id, $dataObjectId);
			if($view) {
				$view->display();
			}
		}
	}
	
	/**
	 * Create/Edit (AJAX Response)
	 * 
	 * @see AdminController::edit()
	 * @access public
	 * @param string $module
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param mixed $id
	 * 		The ID of the item
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()}
	 * @return void
	 */
	public function editAjax($module, $id, $dataObjectId = NULL) {
		// If the request is not handled by the module's controller:
		if(! $this->_dispatchToModuleController($module, __FUNCTION__, array($id, $dataObjectId))) {
			// We render the view ourselves:
			$view = $this->_getFormView($module, $id, $dataObjectId);
			if($view) {
				$view->setIsAjaxResponse(TRUE);
				$view->display();
			}
		}
	}
	
	/* -- PRIVATE -- */
	
	/**
	 * Check if authenticated
	 * 
	 * This method is called to check whether or not the user has logged into
	 * the administration. If not authenticated, this method will redirect the
	 * request with {@link M_Header::redirect()}.
	 * 
	 * @access private
	 * @return integer
	 */
	private function _initAuth() {
		// Get the session singleton
		$session = M_Session::getInstance();
		
		// Get the M_Auth singleton, to check if the visitor has been authenticated
		// as Administrator
		$auth = M_Auth::getInstance();
		
		// Set up storage of authenticated sessions:
		$auth->setStorage(new M_AuthStorageDb($session->getId()));
		
		// Destroy expired sessions:
		$auth->getStorage()->destroyExpired();
		
		// Refresh the authenticated session (in storage)
		$auth->getStorage()->refresh();
		
		// Return the identity of the current user:
		return $auth->getIdentity();
	}

	/**
	 * Get relative path to preferred login module
	 *
	 * @access private
	 * @return string
	 */
	private function _getLoginRelativePath() {
		return M_Application::isModuleInstalled('multikey')
				? 'admin/route/multikey'
				: 'admin/route/login';
	}
	
	/**
	 * Get Administrator
	 * 
	 * Will provide with the {@link Administrator} that represents the authenticated 
	 * user in the Admin module.
	 * 
	 * NOTE:
	 * Will return FALSE, if the administrator could not have been found
	 * 
	 * @access private
	 * @return Administrator
	 */
	private function _getAdministrator() {
		// If not requested before:
		if($this->_administrator === NULL) {
			// Get the administrator from the db
			$this->_administrator = M_Loader
				::getDataObjectMapper('Administrator', 'administrator')
				->getById($this->_getAdministratorId());
		}
		// Return the administrator
		return $this->_administrator;
	}
	
	/**
	 * Get Administrator ID
	 * 
	 * Will provide with the {@link Administrator} ID that represents
	 * the authenticated user of the Admin module.
	 * 
	 * @access private
	 * @return integer
	 */
	private function _getAdministratorId() {
		$auth = M_Auth::getInstance();
		return $auth->getStorage()->getIdentity();
	}
	
	/**
	 * Get ACL
	 * 
	 * @access private
	 * @return M_Acl
	 */
	private function _getAcl() {
		// If not requested before:
		if(! $this->_acl) {
			// Then, we construct the storage object:
			$storage = new M_AclStorageDb();
			
			// Construct the ACL from the storage:
			$this->_acl = $storage->getAcl();
		}
		
		// Return the ACL:
		
		return $this->_acl;
	}
	
	/**
	 * Check ACL Permissions
	 * 
	 * Will check permissions with {@link M_Acl::isAllowed()}
	 * 
	 * @access private
	 * @return bool $flag
	 */
	private function _getAclAllow($resource, $permission) {
		// Run inside try{} block, to avoid exceptions (undefined roles, resources
		// or permissions) from crashing the app 
		try {
			$b = $this
				->_getAcl()
				->isAllowed(
					// Role
					$this->_getAdministrator()->getAclRoleId(), 
					// Resource
					$resource, 
					// Permission
					$permission
				);
			
			return $b;
		}
		catch(M_Exception $e) {
			return FALSE;
		}
	}
	
	/**
	 * Handle toggling
	 * 
	 * This method is used by the following pages, in order to handle the toggling
	 * of a data object attribute
	 * 
	 * - {@link AdminController::toggle()}
	 * - {@link AdminController::toggleAjax()}
	 * 
	 * The attribute's value will be toggled between 0/1
	 * 
	 * @access private
	 * @param string $moduleId
	 * 		The Module ID
	 * @param string $dataObjectId
	 * 		The Data Object ID
	 * @param integer $id
	 * 		The ID of the Data Object
	 * @param string $field
	 * 		The field of which to toggle the value
	 * @return M_DataObject $object
	 * 		Returns the {@link M_DataObject} of which the attribute has been
	 * 		toggled successfully, or returns FALSE on failure
	 */
	private function _toggle($moduleId, $dataObjectId, $id, $field) {
		// Get requested data object:
		$object = $this->_getDataObject($moduleId, $dataObjectId, $id);
		
		// If object found:
		if($object) {
			// Dispatch an event, to notify about the object that is about to be
			// deleted from the website:
			$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_BEFORE_TOGGLE, $this);
			$event->setDataObject($object);
			$event->setModule(new AdminModule($moduleId));
			$event->setDataObjectId($dataObjectId);
			$this->dispatchEvent($event);
			
			// If current value is 0, then set to 1, and vice-versa:
			$object->$field = (! $object->$field) ? 1 : 0;
			
			// Save the object, and if saved correctly:
			if($object->save()) {
				// Dispatch an event, to notify about the object that has been deleted
				$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_TOGGLE, $this);
				$event->setDataObject($object);
				$event->setModule(new AdminModule($moduleId));
				$event->setDataObjectId($dataObjectId);
				$this->dispatchEvent($event);
				
				// Return success
				return $object;
			}
		}
		
		// Return FAILURE, if still here:
		return FALSE;
	}
	
	/**
	 * Move instance
	 * 
	 * This method can be used to move an instance up- or downwards in a list. 
	 * Typically, it is used to manually sort a list of items in the database.
	 * This method is used by the following requests, to handle the sorting of
	 * instances:
	 * 
	 * - {@link AdminController::moveInstance()}
	 * 
	 * @access public
	 * @param string $moduleId
	 * 		The Module ID
	 * @param string $dataObjectId
	 * 		The Data Object ID
	 * @param string $field
	 * 		The field to which the new order should be saved
	 * @param integer $instanceId
	 * 		The ID of the Data Object instance
	 * @param string $diff
	 * 		The direction in which to move the instance, and the number of steps
	 * 		to move.
	 * @param ArrayIterator $filters
	 * 		The collection of filters that need to be applied, before selecting
	 * 		the affected instances from the database. The collection contains
	 * 		instances of {@link AdminListFilterDefinition}
	 * @return void
	 */
	private function _moveInstance($moduleId, $dataObjectId, $field, $instanceId, $diff, ArrayIterator $filters) {
		// Make sure the filters are instances of AdminListFilterDefinition
		if(! M_Helper::isIteratorOfClass($filters, 'AdminListFilterDefinition')) {
			throw new M_Exception(sprintf(
				'Cannot move instances; Expecting instances of AdminListFilterDefinition'
			));
		}
		
		// Get the requested data object:
		$object = $this->_getDataObject($moduleId, $dataObjectId, $instanceId);
		
		// If object found:
		if($object) {
			// We pre-calculate the inverse diff
			$inverseDiff = $diff * -1;
			
			// Get the current position of the moved instance:
			$position = $object->$field;
			
			// We fetch the affected data objects:
			$mapper = $object->getMapper();
			$mapper->addFilter(new M_DbQueryFilterOrder($field, 'ASC'));
			
			// We apply the filters, in order to select the affected instances
			// from the database. If moved downwards (positive direction):
			if($diff > 0) {
				$mapper->addFilter(new M_DbQueryFilterLimit(0, $diff + 1));
				$mapper->addFilter(new M_DbQueryFilterWhere($field, $position, M_DbQueryFilterWhere::GTEQ));
				$mapper->addFilter(new M_DbQueryFilterWhere($field, $position + $diff, M_DbQueryFilterWhere::LTEQ));
			}
			// If moved upwards (negative direction):
			else {
				$mapper->addFilter(new M_DbQueryFilterLimit(0, $inverseDiff + 1));
				$mapper->addFilter(new M_DbQueryFilterWhere($field, $position + $diff, M_DbQueryFilterWhere::GTEQ));
				$mapper->addFilter(new M_DbQueryFilterWhere($field, $position, M_DbQueryFilterWhere::LTEQ));
			}
			
			// Also, apply the filters that have been provided to this method
			foreach($filters as $filter) {
				$mapper->addFilter($filter->getDbQueryFilter());
			}
			
			// For each of the affected data objects:
			foreach($mapper->getAll() as $currentObject) {
				// If the current data object is the moved instance:
				if($currentObject->getId() == $instanceId) {
					// In order to calculate the new order index number, we simply
					// add the direction to the current position:
					$currentObject->$field = $position + $diff;
				}
				// If current data object is not the moved instance, we will have
				// to move it by -1 or 1
				else {
					$currentObject->$field += ($diff > 0 ? -1 : 1 );
				}
				
				// Save the new order index number of the object:
				$currentObject->save();
			}
		}
	}
	
	/**
	 * Handle overview of data objects
	 * 
	 * This method is used by the following pages, in order to handle the overview
	 * of data objects in a given module:
	 * 
	 * - {@link AdminController::overview()}
	 * - {@link AdminController::overviewAjax()}
	 * 
	 * @access private
	 * @param string $moduleId
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()} 
	 * @param bool $isAjaxResponse
	 * 		Set to TRUE if to be handled as AJAX Response, FALSE if not
	 * @return void
	 */
	private function _overview($moduleId, $dataObjectId, $isAjaxResponse = FALSE) {
		// If the request is not handled by the module itself:
		if(! $this->_dispatchToModuleController($moduleId, 'overview', array($dataObjectId, $isAjaxResponse))) {
			// Get the definition of the list:
			$definition = $this->_getListDefinition($moduleId, $dataObjectId);
			
			// Get the list view:
			$view = $this->_getListView($definition);
			
			// Is AJAX Response?
			if($isAjaxResponse) {
				$view->setIsAjaxResponse(TRUE);
			}

			// Set active menu item:
			$view->setActiveMenuItem('admin/overview/' . $definition->getModule()->getId() . '/' . $definition->getDataObjectId());

			// Output the view:
			$view->display();
		}
	}
	
	/**
	 * Handle overview of smart folders
	 * 
	 * This method is used by the following pages, in order to handle the overview
	 * of smart folders:
	 * 
	 * - {@link AdminController::smartFolders()}
	 * - {@link AdminController::smartFoldersAjax()}
	 * 
	 * @access private
	 * @param bool $isAjaxResponse
	 * 		Set to TRUE if to be handled as AJAX Response, FALSE if not
	 * @return AdminSmartFoldersView
	 */
	private function _getSmartFoldersView() {
		// Load the required classes:
		M_Loader::loadView('AdminSmartFoldersView', 'admin');
		M_Loader::loadDataObjectMapper('AdminSmartFolder', 'admin');
		
		// Construct the mapper:
		$mapper = new AdminSmartFolderMapper();
		
		// Construct the view:
		$view = $this->_getAdminPageView('AdminSmartFoldersView');
		$view->setSmartFolders($mapper->getByAdministrator($this->_getAdministrator()));

		// Set active menu item
		$view->setActiveMenuItem('admin/smart-folders');
		
		// Return the view:
		return $view;
	}
	
	/**
	 * Handle Smart Folder page
	 * 
	 * This method is used by the following pages, in order to handle
	 * the Smart Folder page:
	 * 
	 * - {@link AdminController::smartFolder()}
	 * - {@link AdminController::smartFolderAjax()}
	 * 
	 * @access private
	 * @param integer $id
	 * 		The ID of the Smart Folder
	 * @param bool $isAjaxResponse
	 * 		Set to TRUE if to be handled as AJAX Response, FALSE if not
	 * @return void
	 */
	private function _smartFolder($id, $isAjaxResponse = FALSE) {
		// Load the Smart Folder model:
		M_Loader::loadDataObjectMapper('AdminSmartFolder', 'admin');
		
		// Construct a mapper, to fetch the requested Smart Folder:
		$mapper = new AdminSmartFolderMapper();
		
		// Get the requested Smart Folder:
		$folder = $mapper->getById((int) $id);
		
		// If the folder has been found:
		if($folder) {
			// If the Smart Folder does not (yet) contain a reference
			// to module/data-object:
			if(! $folder->getModuleId()) {
				// Construct the view that allows the user to pick a list definition:
				$view = $this->_getAdminPageView('AdminSmartFolderChooseModuleView');
				$view->setSmartFolder($folder);
				$view->setListDefinitions($this->_getListDefinitions());
			}
			// If the Smart Folder already contains a reference to module/data-object:
			else {
				// Construct the list view:
				$definition = $this->_getListDefinition($folder->getModuleId(), $folder->getDataObjectId());
				
				// Get the list view:
				$view = $this->_getListView($definition, $folder);
			}
			
			// Is AJAX Response?
			if($isAjaxResponse) {
				$view->setIsAjaxResponse(TRUE);
			}

			// Set active menu item
			$view->setActiveMenuItem('admin/smart-folder/' . $folder->getId());
			
			// Display the view:
			$view->display();
		}
	}
	
	/**
	 * Handle listed instances in list
	 * 
	 * This method is used by the following pages, in order to handle the listed
	 * instances of data objects in a given module.
	 * 
	 * @access private
	 * @param string $moduleId
	 * 		The ID of the module. Read {@link AdminModule} for more info
	 * @param string $dataObjectId
	 * 		The ID of the Data Object. For more information, read the docs on
	 * 		{@link M_ApplicationModule::getDataObjectNames()} 
	 * @return void
	 */
	private function _instances($moduleId, $dataObjectId) {
		// If the request is not handled by the module itself:
		if(! $this->_dispatchToModuleController($moduleId, 'instances', array($dataObjectId))) {
			// Get the definition of the list
			$definition = $this->_getListDefinition($moduleId, $dataObjectId);
			
			// Get applied filters:
			$filters = $this->_getListFilterDefinitionsFromRequest($definition);
			
			// Ask the list definition for the corresponding DataObjectMapper. Note 
			// that we apply the filters:
			$mapper = $definition->getMapper($filters);
			
			// If, with the collection of applied filters, the instances can be
			// sorted manually (drag-drop), then we sort the the instances by the
			// field that stores the order index number:
			if($definition->isInstancesDraggableForSorting($filters)) {
				// Add the sorting filter to the mapper:
				$mapper->addFilter(new M_DbQueryFilterOrder($definition->getSortDragFieldName(), 'ASC'));
			}
			
			// Get the listed instances' view, and output:
			$this->_getListedInstancesView($definition, $mapper, $filters)->display();
		}
	}
	
	/**
	 * Get list filter definitions, from request
	 * 
	 * This method will create a collection of {@link AdminListFilterDefinition}
	 * instances, based on the variables that have been provided in the request.
	 * {@link M_Request::getVariable()} is used to check for the filter definitions.
	 * 
	 * @access private
	 * @param AdminListDefinition $definition
	 * 		The definition of the list, in order to get the definition of the
	 * 		filters that may have been applied in the request.
	 * @return M_ArrayIterator $filters
	 *		The collection of {@link AdminListFilterDefinition} instances to be
	 *		stored in the session
	 */
	private function _getListFilterDefinitionsFromRequest(AdminListDefinition $definition) {
		// Load the AdminControllerHelper class
		AdminLoader::loadController('AdminControllerHelper', 'admin');

		// The AdminControllerHelper class provides us with the filters that have
		// been provided with request variables (M_Request)
		$filters = AdminControllerHelper::getListFiltersFromRequest($definition);

		// If filters have been provided in the request:
		if(count($filters) > 0) {
			// Then, we store the filters in the session:
			AdminControllerHelper::setListFiltersInSession($definition, $filters);

			// And, we return the filters:
			return $filters;
		}
		// If no filters have been provided in the request:
		else {
			// We check if the filters have been explicitely removed from the list.
			// The request variable "clear" tells us about this:
			if(M_Request::getVariable('clear')) {
				// If explicitely removed, then we also remove the filters from
				// the session variable:
				AdminControllerHelper::clearListFiltersInSession($definition);

				// And we return an empty collection of filters:
				return new M_ArrayIterator(array());
			}
			
			// We return the filters from the session, if still here
			return AdminControllerHelper::getListFiltersFromSession($definition);
		}
	}

	/**
	 * Get list filter definitions, from smart folder
	 * 
	 * This method will create a collection of {@link AdminListFilterDefinition}
	 * instances, based on the collection of filters that is contained by a given
	 * instance of {@link AdminSmartFolder}.
	 * 
	 * @access private
	 * @param AdminSmartFolder $folder
	 * 		The smart folder, to get the filter definitions from
	 * @return ArrayIterator
	 */
	private function _getListFilterDefinitionsFromSmartFolder(AdminSmartFolder $folder) {
		// Prepare return value:
		$out = array();
		
		// For each of the filters in the smart folder:
		foreach($folder->getFilters() as $filter) {
			// Ask the filter for the equivalent M_DbQueryFilter:
			$out[] = $filter->getListFilterDefinition();
		}
		
		// Return the collection of filters:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get list view
	 * 
	 * Will return the list view, for a given module and Data Object ID. This 
	 * method will also apply filters to the list, if necessary. This collection
	 * of filters comes from:
	 * 
	 * - {@link AdminController::_getListFilterDefinitionsFromSmartFolder()}, or
	 * - {@link AdminController::_getListFilterDefinitionsFromRequest()}
	 * 
	 * @access private
	 * @param AdminListDefinition $definition
	 * 		The definition of the list
	 * @param AdminSmartFolder $smartFolder
	 * 		Optionally, you can provide with a smart folder. If provided, the filters
	 * 		will be collected with {@link AdminController::_getListFilterDefinitionsFromSmartFolder()}
	 * @return AdminListView
	 */
	private function _getListView(AdminListDefinition $definition, AdminSmartFolder $smartFolder = NULL) {
		// If the administrator's role does not have access to view the list:
		if(! $this->_getAclAllow($definition->getAclResourceId(), 'view')) {
			// Then, we return the "Access denied" view instead:
			return $this->_getAdminPageView('AdminAccessDeniedView');
		}
		
		// If a smart folder has been provided, we fetch the collection of filter
		// definitions from the smart folder. If not, we fetch the filter definitions
		// from the request variables:
		$filters = $smartFolder
			? $this->_getListFilterDefinitionsFromSmartFolder($smartFolder)
			: $this->_getListFilterDefinitionsFromRequest($definition);
		
		// Ask the list definition for the corresponding DataObjectMapper. Note 
		// that we apply the filters:
		$mapper = $definition->getMapper($filters);

		// Dispatch an event, to notify about the list view about to be rendered:
		$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_BEFORE_LIST_VIEW, $this);
		$event->setModule($definition->getModule());
		$event->setDataObjectId($definition->getDataObjectId());
		$event->setDataObjectMapper($mapper);
		$this->dispatchEvent($event);
		
		// Construct the list view:
		/* @var $view AdminListView */
		$view = $this->_getAdminPageView('AdminListView');
		$view->setListDefinition($definition);
		
		// Construct the filter interface view:
		$filtersView = new AdminListFiltersView($definition);
		if($smartFolder) {
			$filtersView->setSmartFolder($smartFolder);
		}
		
		// Provide the view with the current selection of applied filters:
		$filtersView->setListFilterDefinitions($filters);
		
		// Attach the filter interface view to the global list view:
		$view->setFiltersView($filtersView);
		
		// Assign the list instances' view to the global list view:
		$view->setListView($this->_getListedInstancesView($definition, $mapper, $filters));
		
		// For each of the list view helpers:
		foreach($this->_getListViewHelpers($definition) as $helper) {
			// Add the helper to the list view:
			$view->addHelper($helper);
		}
		
		// Also provide the list view with the current selection of applied filters:
		$view->setListFilterDefinitions($filters);
		
		// Allow new items to be created from the view?
		$view->setAllowNewItems(
			// If not prohibited by the list definition itself, and
			$definition->getFlagAllowNewItems() &&
			// If rules have been defined in the ACL, in order to allow access
			// for the authenticated administrator's role
			$this->_getAclAllow($definition->getAclResourceId(), 'create')
		);

		// Dispatch an event, to notify about the list view being rendered:
		$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_LIST_VIEW, $this);
		$event->setModule($definition->getModule());
		$event->setDataObjectId($definition->getDataObjectId());
		$event->setView($view);
		$this->dispatchEvent($event);
		
		// Return the view as result of this method
		return $view;
	}
	
	/**
	 * Get listed instances' view, for a given module and Data Object ID
	 * 
	 * @access private
	 * @param AdminListDefinition $definition
	 * 		The definition of the list
	 * @param M_DataObjectMapper $mapper
	 * 		The mapper, with which to retrieve the listed data objects.
	 * @param ArrayIterator $filters
	 * 		The collection of applied filters. This collection should contain
	 * 		instances of {@link AdminListFilterDefinition}
	 * @return M_ViewHtml|NULL
	 */
	private function _getListedInstancesView(AdminListDefinition $definition, M_DataObjectMapper $mapper, ArrayIterator $filters) {
		// Dispatch an event, to notify about the list view about to be rendered:
		$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_BEFORE_LIST_VIEW, $this);
		$event->setModule($definition->getModule());
		$event->setDataObjectId($definition->getDataObjectId());
		$event->setDataObjectMapper($mapper);
		$this->dispatchEvent($event);

		// Get the list view, if the module provides with one:
		$listView = $this->_getListInstancesViewFromModule(
			$definition->getModule()->getId(), 
			$definition->getDataObjectId()
		);
		
		// If the module does not provide with a list view
		if(! $listView) {
			// We construct the default list view of the Admin Module:
			$listView = new AdminListColumnsView($definition);
			
			// For each of the list view helpers:
			foreach($this->_getListViewHelpers($definition) as $helper) {
				// Add the helper to the default list view:
				$listView->addHelper($helper);
			}

			// Show delete button?
			$listView->setShowDeleteButton($this->_getAclAllow($definition->getAclResourceId(), 'delete'));

			// Show edit button?
			$listView->setShowEditButton($this->_getAclAllow($definition->getAclResourceId(), 'edit'));
			
			// Show locale buttons?
			$listView->setShowLocaleButtons($this->_getAclAllow($definition->getAclResourceId(), 'edit'));
		}
		
		// Set the list definition in the view:
		$listView->setListDefinition($definition);
		
		// Set the collection of applied filters:
		$listView->setListFilterDefinitions($filters);
		
		// The mapper that has been provided to this method carries all filters.
		// So, in order to provide the view with the data objects to be listed,
		// we could simply ask the mapper for the collection with getAll(). However,
		// in order to optimize loading time, we divide the collection up into 
		// different pages. To do so, we use the M_Pagejumper class:
		$count = $mapper->getCount();
		$numberOfItemsPerPage = $count < 5 ? $count : 5; // To avoid conflicts with limit-filter definition
		$pagejumper = new M_Pagejumper(
			$numberOfItemsPerPage, // 5 items per page
			$count // The total number of items in the list
		);
		
		// Note that, before asking the mapper for the data objects, we add another
		// filter. We do this to only fetch the data objects for the current page
		// (defined by the pagejumper).
		$mapper->addFilter(new M_DbQueryFilterLimit(
			($pagejumper->getPage() - 1) * $pagejumper->getNumberOfItemsPerPage(), // Offset
			$pagejumper->getNumberOfItemsPerPage() // Count
		));
		
		// Now, we assign the collection of data objects to be listed in the view:
		$listView->setDataObjects($mapper->getAll());
		
		// Also, we provide the view with the page jumper:
		$listView->setPagejumper($pagejumper);
		
		// Dispatch an event, to notify about the list view being rendered:
		$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_LIST_VIEW, $this);
		$event->setModule($definition->getModule());
		$event->setDataObjectId($definition->getDataObjectId());
		$event->setView($listView);
		$this->dispatchEvent($event);
		
		// return the view:
		return $listView;
	}
	
	/**
	 * Get instance row view, for a given module and Data Object ID
	 * 
	 * @access private
	 * @param AdminListDefinition $definition
	 * 		The definition of the list
	 * @param M_DataObjectMapper $mapper
	 * 		The mapper, with which to retrieve the listed data objects.
	 * @param ArrayIterator $filters
	 * 		The collection of applied filters. This collection should contain
	 * 		instances of {@link AdminListFilterDefinition}
	 * @return M_ViewHtml|NULL
	 */
	private function _getListedInstanceRowView(AdminListDefinition $definition, M_DataObjectMapper $mapper, ArrayIterator $filters) {
		// Dispatch an event, to notify about the list view about to be rendered:
		$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_BEFORE_LIST_VIEW, $this);
		$event->setModule($definition->getModule());
		$event->setDataObjectId($definition->getDataObjectId());
		$event->setDataObjectMapper($mapper);
		$this->dispatchEvent($event);

		// Get the list view, if the module provides with one:
		$listView = $this->_getListInstanceRowViewFromModule(
			$definition->getModule()->getId(), 
			$definition->getDataObjectId()
		);
		
		// If the module does not provide with a list view
		if(! $listView) {
			// We construct the default list view of the Admin Module:
			$listView = new AdminListInstanceRowView($definition);
			
			// For each of the list view helpers:
			foreach($this->_getListViewHelpers($definition) as $helper) {
				// Add the helper to the default list view:
				$listView->addHelper($helper);
			}

			// Show delete button?
			$listView->setShowDeleteButton($this->_getAclAllow($definition->getAclResourceId(), 'delete'));

			// Show edit button?
			$listView->setShowEditButton($this->_getAclAllow($definition->getAclResourceId(), 'edit'));

			// Show locale button?
			$listView->setShowLocaleButtons($this->_getAclAllow($definition->getAclResourceId(), 'edit'));
		}
		
		// Set the list definition in the view:
		$listView->setListDefinition($definition);
		
		// Set the collection of applied filters:
		$listView->setListFilterDefinitions($filters);
		
		// Now, we assign the collection of data objects to be listed in the view:
		// (theoretically just one)
		$listView->setDataObjects($mapper->getAll());
		
		// Dispatch an event, to notify about the list view being rendered:
		$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_LIST_VIEW, $this);
		$event->setModule($definition->getModule());
		$event->setDataObjectId($definition->getDataObjectId());
		$event->setView($listView);
		$this->dispatchEvent($event);
		
		// return the view:
		return $listView;
	}
	
	/**
	 * Get listed instances' view, from module
	 * 
	 * Will check if the given module provides with a view that renders a collection
	 * of data object instances. Note that this view should inherit from the 
	 * abstract {@link AdminCustomListView} class!
	 * 
	 * NOTE:
	 * If no view could have been found, this method will return NULL
	 * 
	 * @access private
	 * @param string $moduleId
	 * @param string $dataObjectId
	 * @return AdminCustomListView|NULL
	 */
	private function _getListInstancesViewFromModule($moduleId, $dataObjectId = NULL) {
		// Construct the requested module:
		$module = new AdminModule($moduleId);
		
		// If the Data Object ID has not been provided, get default of module:
		if(! $dataObjectId) {
			$dataObjectId = $this->_getDefaultDataObjectIdOf($module);
		}
		
		// Compose the view class name:
		$className = $module->getDataObjectClassName((string) $dataObjectId) . 'ListView';
		
		// Check if a view class file is available:
		$file = AdminLoader::getViewClassFile($className, (string) $moduleId);
		// If we cannot find the view class' file, we return (boolean) FALSE
		if(! $file->exists()) {
			return NULL;
		}
		
		// Load the view class into the application
		AdminLoader::loadView($className, $module->getId());
		
		// Check if the view class implements the MI_AdminListView class. If not,
		// we return NULL again!
		$class = new $className;
		if(! M_Helper::isInstanceOf($class, 'AdminCustomListView')) {
			// For development debugging, we throw an exception
			throw new M_Exception(sprintf(
				'%s should be a subclass of %s',
				$className,
				'AdminCustomListView'
			));
		}
		
		// Construct a new instance of the view class, and return it as result:
		return $class;
	}
	
	/**
	 * Get instance row view, from module
	 * 
	 * Will check if the given module provides with a view that renders an instance
	 * row (when scrolling through list; via ajax). Note that this view should 
	 * inherit from the abstract {@link AdminListInstanceRowView} class!
	 * 
	 * NOTE:
	 * If no view could have been found, this method will return NULL
	 * 
	 * @access private
	 * @param string $moduleId
	 * @param string $dataObjectId
	 * @return AdminListInstanceRowView|NULL
	 */
	private function _getListInstanceRowViewFromModule($moduleId, $dataObjectId = NULL) {
		// Construct the requested module:
		$module = new AdminModule($moduleId);
		
		// If the Data Object ID has not been provided, get default of module:
		if(! $dataObjectId) {
			$dataObjectId = $this->_getDefaultDataObjectIdOf($module);
		}
		
		// Compose the view class name:
		$className = $module->getDataObjectClassName((string) $dataObjectId) . 'ListRowView';
		
		// Check if a view class file is available:
		$file = AdminLoader::getViewClassFile($className, (string) $moduleId);
		// If we cannot find the view class' file, we return (boolean) FALSE
		if(! $file->exists()) {
			return NULL;
		}
		
		// Load the view class into the application
		AdminLoader::loadView($className, $module->getId());
		
		// Check if the view class implements the MI_AdminListView class. If not,
		// we return NULL again!
		$class = new $className;
		if(! M_Helper::isInstanceOf($class, 'AdminListInstanceRowView')) {
			// For development debugging, we throw an exception
			throw new M_Exception(sprintf(
				'%s should be a subclass of %s',
				$className,
				'AdminListInstanceRowView'
			));
		}
		
		// Construct a new instance of the view class, and return it as result:
		return $class;
	}
	
	/**
	 * Get list view helpers, from module
	 * 
	 * If the module does not provide with a custom list view (for more info,
	 * read {@link AdminController::_getListViewFromModule()}), the Admin Module
	 * uses the default {@link AdminListColumnsView} to render the list of
	 * instances.
	 * 
	 * This default view accepts the help of some helper objects, in order to
	 * render the final interface. This method is used to identify the collection
	 * of helpers that should be attached to a given list definition.
	 * 
	 * @access private
	 * @param AdminListDefinition $definition
	 * @return ArrayIterator
	 */
	private function _getListViewHelpers(AdminListDefinition $definition) {
		// The collection of helpers:
		$out = array();
		
		// Also, construct the Admin Module:
		$admin = new AdminModule('admin');
		
		// Ask the Admin Module for its configuration:
		$c = $admin->getConfig();
		
		// Construct the default Admin List View Helper (if any)
		if(isset($c->list) && isset($c->list->helper) && isset($c->list->helper->module) && isset($c->list->helper->class)) {
			// Get class name:
			$class = (string) $c->list->helper->class;
			
			// Load the view helper:
			AdminLoader::loadView($class, (string) $c->list->helper->module);
			
			// Construct the view, and add it as helper to the list view:
			$out[] = new $class;
		}
		
		// Get the list view helper from the list definition (if any)
		$helper = $definition->getListViewHelper();
		
		// Also add it to the list view:
		if($helper) {
			$out[] = $helper;
		}
		
		// Return the collection of helpers:
		return new ArrayIterator($out);
	}
	
	/**
	 * Get {@link AdminPageView} for form
	 * 
	 * @see AdminController::edit()
	 * @see AdminController::editAjax()
	 * @access private
	 * @return AdminFormDefinitionView
	 */
	private function _getFormView($module, $id, $dataObjectId = NULL) {
		// Load the view
		M_Loader::loadView('AdminFormDefinitionView', 'admin');
		
		// Construct the requested module:
		$adminModule = new AdminModule($module);
		
		// If the DataObject ID has not been provided, get default of module:
		if(! $dataObjectId) {
			$dataObjectId = $this->_getDefaultDataObjectIdOf($adminModule);
		}
		
		// Ask the module for the corresponding DataObject Mapper:
		$mapper = $adminModule->getDataObjectMapper($dataObjectId);
		
		// Get the data object to be created/edited
		// First, we make sure an existing DataObject ID has been provided:
		$hasId = (is_numeric($id) && $id > 0);
		if($hasId) {
			$object = $mapper->getById($id);
			if(! $object) {
				throw new M_Exception('Cannot find requested item');
			}
		}
		// If no existing ID is provided, we create a new DataObject:
		else {
			$object = $adminModule->getDataObject($dataObjectId);
		}
		
		// Get the Form Definition of the module:
		$formDefinition = new AdminFormDefinition($adminModule, $dataObjectId);
		
		// Check if the administrator has access to editing the data object:
		if(! $this->_getAclAllow($formDefinition->getAclResourceId(), $object->isNew() ? 'create' : 'edit')) {
			// If not, we return the "Access denied" view instead:
			return $this->_getAdminPageView('AdminAccessDeniedView');
		}
		
		// Add data objects to the definition:
		$this->_addDataObjectsToAdminDefinition($formDefinition);
		$formDefinition->addDataObjectValues('dataObject', $object);
		
		// Get the Form from the definition:
		$form = $formDefinition->getForm();
		
		// If a locale has been provided:
		$reqLocale = M_Request::getVariable('locale', FALSE, M_Request::TYPE_STRING, M_Request::GET);
		if($reqLocale) {
			// Set the locale in which to edit/create the data object
			$form->setLocale($reqLocale);
		}
		
		// If a reference locale has been provided:
		$reqLocale = M_Request::getVariable('localeRef', FALSE, M_Request::TYPE_STRING, M_Request::GET);
		if($reqLocale) {
			// Set the reference locale
			$form->setLocaleReference($reqLocale);
		}
		
		// Set the data object to be edited/created
		$form->setDataObject($object);
		
		// We dispatch an event, to notify about the data-object that is about
		// to be created/edited:
		$objectIsInDb = ($object->getId() > 0);
		$eventType = $objectIsInDb
			? AdminControllerEvent::DATA_OBJECT_BEFORE_EDIT
			: AdminControllerEvent::DATA_OBJECT_BEFORE_CREATE;
		
		$event = new AdminControllerEvent($eventType, $this);
		$event->setDataObject($object);
		$event->setForm($form);
		$event->setModule($adminModule);
		$event->setDataObjectId($dataObjectId);
		$this->dispatchEvent($event);
		
		//try to run, if form is sent and validated we can continue
		$tryRun = ($form->isSent() && $form->validate() === TRUE);
		
		//when a form has ran we try to execute
		$tryExecute = false;
		if ($tryRun) {
			// We dispatch an event to notify about the data-object that is about
			// to be saved
			$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_BEFORE_SAVE, $this);
			$event->setDataObject($object);
			$event->setForm($form);
			$event->setModule($adminModule);
			$event->setDataObjectId($dataObjectId);
			$this->dispatchEvent($event);
		
			//execute the form (and save the data)
			$tryExecute = $form->execute();
		}
		
		// If the form has not been sent, or validation errors
		// have been generated:
		if(! $tryExecute) {
			// Construct the form view, providing the form definition:
			$view = $this->_getAdminPageView('AdminFormDefinitionView');
			$view->setFormDefinition($formDefinition);
			$view->setForm($form);
			$view->setActiveMenuItem('admin/overview/' . $adminModule->getId() . '/' . $dataObjectId);
			return $view;
		}
		// If the form has been completed successfully
		else {
			// We dispatch an event to notify about the fact that the data-object
			// has been saved:
			$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_SAVE, $this);
			$event->setDataObject($object);
			$event->setForm($form);
			$event->setModule($adminModule);
			$event->setDataObjectId($dataObjectId);
			$this->dispatchEvent($event);
			
			// We dispatch an event, to notify about the fact that the data-object 
			// has been created/edited
			$eventType = $objectIsInDb
				? AdminControllerEvent::DATA_OBJECT_EDIT
				: AdminControllerEvent::DATA_OBJECT_CREATE;
			
			$event = new AdminControllerEvent($eventType, $this);
			$event->setDataObject($object);
			$event->setForm($form);
			$event->setModule($adminModule);
			$event->setDataObjectId($dataObjectId);
			$this->dispatchEvent($event);
			
			// Finish/Close the form:
			$form->close();
			
			// Return NULL (no form; already completed)
			return NULL;
		}
	}
	
	/**
	 * Delete a data object
	 * 
	 * Will delete a given data object from the DB. Used by:
	 * 
	 * - {@link AdminController::delete()}
	 * - {@link AdminController::smartFolderDelete()}
	 * 
	 * @access private
	 * @param string $module
	 * 		The Module ID
	 * @param string $id
	 * 		The ID of the Data Object to be deleted
	 * @param string $dataObjectId
	 * 		The Data Object ID
	 * @return bool $flag
	 * 		Returns TRUE on success, FALSE on failure
	 */
	private function _delete($module, $id, $dataObjectId = NULL) {
		// Construct the module:
		$module = new AdminModule($module);
		
		// If the DataObject ID has not been provided, get default of module:
		if(! $dataObjectId) {
			$dataObjectId = $this->_getDefaultDataObjectIdOf($module);
		}
		
		// Get data object mapper
		$mapper = $module->getDataObjectMapper($dataObjectId);
		
		// Select the object from db:
		$object = $mapper->getById($id);
		
		// If the object has been found, and the administrator has rights to delete it:
		if($object && $this->_getAclAllow($module->getId() . '-' . $dataObjectId, 'delete')) {

			// Dispatch an event, to notify about the object that is about to be
			// deleted from the website:
			$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_BEFORE_DELETE, $this);
			$event->setDataObject($object);
			$this->dispatchEvent($event);
			
			// Get the list definition of the data object that is being deleted:
			$listDefinition = $this->_getListDefinition($module->getId(), $dataObjectId);

			// If the list definition exists:
			if($listDefinition->exists()) {
				// Is the list sorted (drag-n-drop)
				$sortDragField = $listDefinition->getSortDragFieldName();
				if($sortDragField) {
					// We ask the list definition for the filters that are required
					// for sorting. Then, for each of the required filters:
					foreach($listDefinition->getRequiredFiltersForSorting() as $filter) {
						// Get the field to which the filter should be applied
						$field = $filter->getField();
						
						// Get the value from the data object, and set it as filter value:
						$filter->setValue($object->$field);
						
						// Add the filter to the mapper:
						$mapper->addFilter($filter->getDbQueryFilter());
					}
					
					// Also, add a filter to exclude the selected object:
					$mapper->addFilter(new M_DbQueryFilterWhere('id', $object->getId(), M_DbQueryFilterWhere::NEQ));
			
					// Order by the current order:
					$mapper->addFilter(new M_DbQueryFilterOrder($sortDragField, 'ASC'));
					
					// For each of the data objects that meet the filter criteria:
					$j = 0;
					foreach($mapper->getAll() as $affectedObject) {
						// Re-set the order index number:
						$affectedObject->$sortDragField = $j ++;
						
						// Save the object:
						$affectedObject->save();
					}
				}
			}
			
			// delete, and if deleted successfully:
			if($object->delete()) {
				// Dispatch an event, to notify about the object that has been deleted
				$event = new AdminControllerEvent(AdminControllerEvent::DATA_OBJECT_DELETE, $this);
				$event->setDataObject($object);
				$this->dispatchEvent($event);
				
				// Return success:
				return TRUE;
			}
			// If not deleted successfully:
			else {
				// Return failure:
				return FALSE;
			}
		}
	}
	
	/**
	 * Get List Definition
	 * 
	 * Will provide with an instance of {@link AdminListDefinition}, based on
	 * a Module ID, and a Data Object ID.
	 * 
	 * @access private
	 * @param string $moduleId
	 * @param string $dataObjectId
	 * @return AdminListDefinition
	 */
	private function _getListDefinition($moduleId, $dataObjectId = NULL) {
		// Construct the requested module:
		$module = new AdminModule($moduleId);
		
		// If the Data Object ID has not been provided, get default of module:
		if(! $dataObjectId) {
			$dataObjectId = $this->_getDefaultDataObjectIdOf($module);
		}
		
		// Construct the list definition:
		$definition = new AdminListDefinition($module, $dataObjectId);
		
		// Add data objects to the definition
		$this->_addDataObjectsToAdminDefinition($definition);
		
		// Return the definition
		return $definition;
	}
	
	/**
	 * Get List Definitions
	 * 
	 * Will provide with all available instances of {@link AdminListDefinition},
	 * from all modules in the application.
	 * 
	 * @access private
	 * @return ArrayIterator
	 */
	private function _getListDefinitions() {
		// Output:
		$out = array();
		
		// For each of the modules in the application:
		foreach(AdminApplication::getModules() as $module) {
			// if the module is multimanageable
			if($module->isMultimanageable()) {
				// For each of the data objects in the module:
				foreach($module->getDataObjectNames() as $id => $name) {
					// Construct the list definition of the current data object:
					$definition = new AdminListDefinition($module, $id);
					
					// If the list definition exists:
					if($definition->exists()) {
						// If the ACL allows access to the current definition:
						if($this->_getAclAllow($definition->getAclResourceId(), 'view')) {
							// Add data objects to the definition
							$this->_addDataObjectsToAdminDefinition($definition);
							
							// Add the definition to the output:
							$out[] = $definition;
						}
					}
				}
			}
		}
		
		// Return output
		return new ArrayIterator($out);
	}
	
	/**
	 * Set data objects in definition object
	 * 
	 * @access private
	 * @param AdminDefinition $definition
	 * @return void
	 */
	private function _addDataObjectsToAdminDefinition(AdminDefinition $definition) {
		// The authenticated administrator:
		$definition->addDataObjectValues('administrator', $this->_getAdministrator());
	}
	
	/**
	 * Get default DataObject ID of module
	 * 
	 * Will return the "default" DataObject ID of a given module. This default
	 * ID is the ID of the first DataObject that is returned by
	 * {@link M_ApplicationModule::getDataObjectNames()}.
	 * 
	 * NOTE:
	 * Will return NULL if no ID has been found
	 * 
	 * @access private
	 * @param AdminModule $module
	 * 		The Module, represented by an instance of {@link AdminModule}
	 * @return string
	 */
	private function _getDefaultDataObjectIdOf(AdminModule $module) {
		$i = $module->getDataObjectNames();
		if(count($i) > 0) {
			return $i->key();
		} else {
			return NULL;
		}
	}
	
	/**
	 * Get Data Object
	 * 
	 * Will return the Data Object instance, based on
	 * 
	 * - Module ID
	 * - Data Object ID
	 * - Instance ID
	 * 
	 * @access private
	 * @param string $moduleId
	 * 		The Module ID
	 * @param string $dataObjectId
	 * 		The Data Object ID
	 * @param integer $instanceId
	 * 		The Instance ID
	 * @return M_DataObject $object
	 * 		Returns an instance of {@link M_DataObject}, or FALSE if it could not
	 * 		find the instance in the database.
	 */
	private function _getDataObject($moduleId, $dataObjectId, $instanceId) {
		// Construct the module:
		$module = new AdminModule($moduleId);
		
		// Get data object mapper
		$mapper = $module->getDataObjectMapper($dataObjectId);
		// Select the object from db:
		$object = $mapper->getById($instanceId);
		
		// If object found:
		if($object) {
			return $object;
		}
		// If not:
		else {
			return FALSE;
		}
	}
	
	/**
	 * Get Admin Page View
	 * 
	 * Will provide with a view in the administration. The view that
	 * is returned by this method is a subpage of {@link AdminPageView}
	 * 
	 * NOTE:
	 * Will dispatch {@link AdminControllerEvent::PAGE_VIEW}
	 * 
	 * @access private
	 * @param string $viewClassName
	 * 		The requested view
	 * @return AdminPageView
	 */
	protected function _getAdminPageView($viewClassName) {
		// Load the view:
		M_Loader::loadView($viewClassName, 'admin');
		
		// Construct the view:
		$view = new $viewClassName;
		
		// Dispatch an event, to notify about the view being rendered:
		$event = new AdminControllerEvent(AdminControllerEvent::PAGE_VIEW, $this);
		$event->setView($view);
		$this->dispatchEvent($event);
		
		// Return the view:
		return $view;
	}
	
	/**
	 * Dispatch to controller
	 * 
	 * @see AdminController::_getModuleControllerCallback()
	 * @access private
	 * @param string $module
	 * 		The module of which you want to lookup the admin controller
	 * @param string $action
	 * 		The action (= method name of the controller)
	 * @param array $args
	 * 		The arguments for the controller's method
	 * @return bool $flag
	 * 		Returns TRUE if the request has been dispatched to another controller,
	 * 		FALSE if not dispatched at all
	 */
	private function _dispatchToModuleController($module, $action, array $args = array()) {
		// Check if the module provides with an admin controller:
		$callback = $this->_getModuleControllerCallback($module, $action, $args);
		
		// If so
		if($callback) {
			// we delegate the request to the module's controller
			call_user_func_array($callback[0], $callback[1]);
			
			// And we return TRUE
			return TRUE;
		}
		// If the module does not provide with the admin controller:
		else {
			// we return FALSE
			return FALSE;
		}
	}
	
	/**
	 * Get controller callback
	 * 
	 * Very similar to {@link M_ControllerDispatcher::getControllerCallback()}.
	 * This method will lookup the {@link M_Controller} class that is 
	 * in charge of handling a specific admin-related request.
	 * 
	 * Note that the following details are checked, before creating a
	 * controller callback:
	 * 
	 * - The PHP Class file should exist
	 * - The class should implement the {@link MI_Controller} interface
	 * - The class should be instantiable
	 * - The method should have been implemented in the class
	 * - The method may not be declared as private
	 * - The number of provided arguments should not be less than the
	 *   required number of arguments.
	 * 
	 * If either one of the conditions above are not met, this method
	 * will return (boolean) FALSE instead.
	 *
	 * @access private
	 * @param string $module
	 * 		The module of which you want to lookup the admin controller
	 * @param string $action
	 * 		The action (= method name of the controller)
	 * @param array $args
	 * 		The arguments for the controller's method
	 * @return array|FALSE
	 */
	private function _getModuleControllerCallback($module, $action, array $args = array()) {
		// We compose the class name of the controller
		$className = 'Admin'. M_Helper::getCamelCasedString($module, FALSE) .'Controller';
		
		// We check if we can load the controller class file.
		$classFile = AdminLoader::getControllerClassFile($className, $module);
		
		// If we cannot find the controller class' file, we return 
		// (boolean) FALSE
		if(! $classFile->exists()) {
			return FALSE;
		}
		
		// Load the controller class into the application
		AdminLoader::loadController($className, $module);
		
		// Just as M_ControllerDispatcher would, we make sure that the 
		// controller implements the MI_Controller interface:
		if(! M_Helper::isInterface($className, 'MI_Controller')) {
			// if not implementing the MI_Controller interface,
			// we again return FALSE.
			return FALSE;
		}
		
		// If so, we also make sure that we can create an instance of
		// the class:
		$class = new ReflectionClass($className);
		if(! $class->isInstantiable()) {
			// If not, we return FALSE as well:
			return FALSE;
		}

		/* @var M_Controller $controller */
		// Construct an instance of the controller:
		$controller = new $className;

		// Compose the name of the requested method
		$action = M_Helper::getCamelCasedString($action, TRUE);
		
		// We check if the controller object provides with an 
		// implementation of the requested method:
		if(! method_exists($controller, $action)) {
			// If not, we return FALSE
			return FALSE;
		}
		
		// Now, we have a controller object and a method name. 
		// We still need to make sure that the method is not 
		// private!
		$method = new ReflectionMethod($className, $action);
		
		// If the method is private, we will again return FALSE
		if($method->isPrivate()) {
			return FALSE;
		}
		
		// If not private, we still have a valid request! In that case,
		// we also make sure that the number of arguments in the
		// URI is matching the number of required arguments:
		if($method->getNumberOfRequiredParameters() > count($args)) {
			// If that's not the case, we will return FALSE
			return FALSE;
		}
		
		// Bind all event listeners from this controller to the newly created
		// one
		$this->copyEventListeners($controller);

		// Return the final callback definition
		return array(array($controller, $action), $args);
	}
	
	/**
	 * Load AdminController Event listeners
	 * 
	 * This method will check if an event listener exists, in each of the modules.
	 * 
	 * @access private
	 * @return void
	 */
	private function _loadAdminControllerEventListeners() {
		// Collection of listeners:
		$out = array();
		
		// For each of the modules:
		foreach(AdminApplication::getModules() as $module) {
			// Compose the event listener's name
			$className = M_Helper::getCamelCasedString($module->getId(), FALSE) . 'EventListener';
			
			// Check if a module exists:
			$file = AdminLoader::getControllerClassFile($className, $module->getId());
			
			// If so, load it:
			if($file->exists()) {
				// Load the source file:
				AdminLoader::loadController($className, $module->getId());
				
				// Construct the listener:
				$out[] = new $className($this);
			}
		}
		
		// Return the collection
		return new ArrayIterator($out);
	}
	
	/**
	 * Load Admin Core Classes
	 * 
	 * @access private
	 * @return void
	 */
	private function _loadAdminCore() {
		M_Loader::loadController('AdminControllerHelper', 'admin');
		AdminControllerHelper::loadAdminCoreClasses();
	}
	
	/**
	 * Load core of modules
	 * 
	 * Will load the core classes of all admin-enabled modules. This is typically
	 * done to allow modules to insert new core classes into the application's
	 * CMS environment (e.g. new form field types).
	 * 
	 * @access private
	 * @return void
	 */
	private function _loadAdminCoreModules() {
		// For each of the admin-enabled modules:
		foreach(AdminApplication::getModules() as $module) {
			// The directory where the module's core classes are located:
			$directory = new M_Directory(
				AdminLoader::getModuleAdminDirectory($module->getId())->getPath() . 
				'/core'
			);
			
			// If the core folder exists:
			if($directory->exists()) {
				// For each of the files in the core folder:
				foreach($directory->getItems() as $item) {
					// We ignore anything but files
					if($item->getFsItemType() == MI_Fs::TYPE_FILE) {
						// Register the core class, located in the current file:
						M_Loader::loadRelative($directory->getPath() . '/' . $item->getBasename());
						//M_Loader::addClassPath($directory->getPath(), 'M_' . str_replace('.class.php', '', $item->getBasename()));
					}
				}
			}
		}
	}
}