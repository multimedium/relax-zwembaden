<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div id="home-wrapper">
		<h2 class="big">{t text="Welcome @name" name=$administrator->getFirstname()}</h2>
		<div id="home-account">
			<h3>
				<a href="{link href="admin/my-account"}" title="{t|att text="Edit my account"}">
					{t text="Edit my account"}
				</a>
			</h3>
			<div class="description">
				{t text="Change your personal data and preferences"}
			</div>
		</div>
		<div id="home-about">
			<h3>
				{t text="Multimanage. multi-what?"}
			</h3>
			<div class="description">
				{t text="Read more about Multimanage."}
			</div>
		</div>
	</div>
</div>