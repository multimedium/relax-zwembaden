{if $form->getErrorMessage()}
	<div class="error-message">
		{$form->getErrorMessage()}
	</div>
{/if}

{* If more than one locale is available in the environment, and the object is being localized: *}
{assign var="dataObject" value=$form->getDataObject()}
{assign var="dataObjectIsLocalized" value=$dataObject->isLocalized()}
{if $locales|@count > 1 && $dataObjectIsLocalized}
	
	{* Show the locale options *}
	<div class="form-locale-menu">
	
		{t text="Edit this item in"} 
		<a href="" class="popmenu arrow-down" id="idLocaleButton">{localedisplay name=$form->getLocale() type="language"}</a>
		
		{t text="and compare with"}
		<a href="" class="popmenu arrow-down" id="idLocaleReferenceButton">{localedisplay name=$form->getLocaleReference() type="language"}</a>
	</div>
	
	<div id="idLocaleButtonPopmenu" class="hidden">
		<ul>
			{foreach from=$locales item="isAvailable" key="locale"}
				{localedisplay name=$locale type="language" assignto="localeDisplay"}
				<li><a href="{urlcopy locale=$locale}"><img src="{link module="admin" type="images"}/locale-{if $isAvailable}1{else}0{/if}.png" border="0" style="vertical-align: middle;" alt="{$locale}" /> {localedisplay name=$locale type="language"}</a></li>
			{/foreach}
		</ul>
	</div>
	
	<div id="idLocaleReferenceButtonPopmenu" class="hidden">
		<ul>
			{foreach from=$locales item="isAvailable" key="locale"}
				{localedisplay name=$locale type="language" assignto="localeDisplay"}
				<li><a href="{urlcopy localeRef=$locale}"><img src="{link module="admin" type="images"}/locale-{if $isAvailable}1{else}0{/if}.png" border="0" style="vertical-align: middle;" alt="{$locale}" /> {localedisplay name=$locale type="language"}</a></li>
			{/foreach}
		</ul>
	</div>
{/if}

<form id="{$viewId}"{foreach from=$form->getProperties() key="name" item="value"} {$name}="{$value}"{/foreach}>
	
	{foreach from=$form->getVariables() key="name" item="value"}
		{if $value|is_array}
			{foreach from=$value key="key" item="arrayValue"}
				<input type="hidden" name="{$name}[{$key}]" value="{$arrayValue}" />
			{/foreach}
		{else}
			<input type="hidden" name="{$name}" value="{$value}" />
		{/if}
	{/foreach}
	
	{* Contains all the field-groups *}
	{assign var="fieldGroups" value=$form->getFieldGroups()}
	{* Contains all the fields which are member of a group *}
	{assign var="groupFields" value=$form->getGroupFields()}
	
	{* Render fields which are not part of a field-group *}
	{foreach from=$form->getFields(false) key="name" item="field"}
		{if $groupFields->offsetExists($field->getId()) == false}
			{$field->setModuleOwner('admin')}
			{$field->getView()}
			{* Show locale reference text (for translate purposes) *}
			{if $locales|@count > 1 && $dataObjectIsLocalized && $dataObject->isLocalizedField($name)}
				<div class="field-reference-value">
					<strong>
						{localedisplay name=$form->getLocaleReference() type="language"}
					</strong>:
					<br />
					{$referenceValues.$name|strip_tags:'<b><strong><em><i><u>'}
				</div>
			{/if}
			
			<div class="clear"><!--  --></div>
		{/if}
	{/foreach}

	{* Render field groups *}
	{foreach from=$fieldGroups item="group"}
		{$group->setModuleOwner('admin')}

		<fieldset class="group-row{if $group->getFoldable()} foldable{/if}" id="group-{$group->getId()}">
		{if $group->getTitle()}
			<legend class="group-title{if $group->getFolded()} folded{/if}">{$group->getTitle()}</legend>
		{/if}
		{if $group->getDescription()}
			<div class="group-description note">{$group->getDescription()}</div>
		{/if}
		
		{* render the fields *}
		<div class="group-fields">
			{foreach from=$group->getFields() key="name" item="field"}
				{$field->setModuleOwner('admin')}
				{$field->getView()}

				{* Show locale reference text (for translate purposes) *}
				{if $locales|@count > 1 && $dataObjectIsLocalized && $dataObject->isLocalizedField($name)}
					<div class="field-reference-value">
						<strong>
							{localedisplay name=$form->getLocaleReference() type="language"}
						</strong>:
						<br />
						{$referenceValues.$name|strip_tags:'<b><strong><em><i><u>'}
					</div>
				{/if}
			{/foreach}
		</div>
		
		</fieldset>
		<div class="clear"><!--  --></div>
	{/foreach}
	
	<div class="field-row-buttons">
		<label class="left button-submit"><input type="submit" value="{t text="Save"}" /><span>{t text="Save"}</span></label>
		<div class="clear"><!-- --></div>
	</div>