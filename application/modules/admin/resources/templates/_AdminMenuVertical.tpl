{if $menu->getTitle()}<h2 id="{$menu->getId()}Title">{$menu->getTitle()}</h2>{/if}
<ul id="{$menu->getId()}" class="vertical">
	{foreach from=$menu->getMenuItems() item="menuItem"}
		<li{if $menuItem->isActive()} class="active"{/if}><a href="{link href=$menuItem->getPath()}" title="{$menuItem->getDescription()|att}">{$menuItem->getTitle()}</a></li>
	{/foreach}
</ul>