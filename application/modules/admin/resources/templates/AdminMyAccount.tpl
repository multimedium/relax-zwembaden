<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="breadcrumb">
		<ul>
			<li><a href="{link href="admin/index/"}">Multimanage</a></li>
			<li><span>{t text="My account"}</span></li>
		</ul>
	</div>
	
	{assign var="picture" value=$administrator->getPicture()}
	<div id="my-account">
		<div id="my-account-picture">
			{if $picture->exists()}
				<img src="{link href="thumbnail/square/250/"}{$picture->getPath()}" alt="{$administrator->getFirstName()|att}" />
			{else}
				<img src="{link href="my-account-no-picture.gif" type="images" module="admin"}" alt="{$administrator->getFirstName()|att}" />
			{/if}
		</div>
		<div id="my-account-details">
			<h2 class="big">{$administrator->getTitle()} {$administrator->getFirstName()} {$administrator->getSurnames()}</h2>
			<div>
				<span class="label">{t text="First name"}: </span>
				<span class="labelled-data">{$administrator->getFirstName()}</span>
			</div>
			<div>
				<span class="label">{t text="Last name"}: </span>
				<span class="labelled-data">{$administrator->getSurnames()}</span>
			</div>
			<div>
				<span class="label">{t text="Username"}: </span>
				<span class="labelled-data">{$administrator->getUsername()}</span>
			</div>
			<div>
				<span class="label">{t text="Password"}: </span>
				<span class="labelled-data">
					&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;
					<a href="{link href="admin/change-password"}" title="{t|att text="Change my password"}">{t text="Change my password"}</a>
				</span>
			</div>
			{if $administrator->getStreetAddress()}
			<div>
				<span class="label">{t text="Street name (+house number)"}: </span>
				<span class="labelled-data">{$administrator->getStreetAddress()}</span>
			</div>
			{/if}
			{if $administrator->getZip()}
			<div>
				<span class="label">{t text="Postal code"}: </span>
				<span class="labelled-data">{$administrator->getZip()}</span>
			</div>
			{/if}
			{if $administrator->getCity()}
			<div>
				<span class="label">{t text="City"}: </span>
				<span class="labelled-data">{$administrator->getCity()}</span>
			</div>
			{/if}
			{if $administrator->getCountry()}
			<div>
				<span class="label">{t text="Country"}: </span>
				<span class="labelled-data">{$administrator->getCountry()}</span>
			</div>
			{/if}
			{if $administrator->getWebsite()}
			<div>
				<span class="label">{t text="Website"}: </span>
				<span class="labelled-data"><a href="{$administrator->getWebsite()}" target="_blank">{$administrator->getWebsite()}</a></span>
			</div>
			{/if}
			{if $administrator->getEmail()}
			<div>
				<span class="label">{t text="Email"}: </span>
				<span class="labelled-data"><a href="mailto:{$administrator->getEmail()}">{$administrator->getEmail()}</a></span>
			</div>
			{/if}
			{if $administrator->getPhoneNumber()}
			<div>
				<span class="label">{t text="Phone number"}: </span>
				<span class="labelled-data">{$administrator->getPhoneNumber()}</span>
			</div>
			{/if}
			{if $administrator->getFaxNumber()}
			<div>
				<span class="label">{t text="Fax number"}: </span>
				<span class="labelled-data">{$administrator->getFaxNumber()}</span>
			</div>
			{/if}
		</div>
		<div class="clear"><!--  --></div>
		
		<br />
		<a href="{link href="admin/my-account-vcard"}" class="left button"><span>{t text="Download my vCard"}</span></a>
		<a href="{link href="admin/edit-my-account"}" class="m-left button"><span>{t text="Edit My Account"}</span></a>
		<a href="{link href="admin/change-password"}" class="m-left button"><span>{t text="Change my password"}</span></a>
	</div>
</div>