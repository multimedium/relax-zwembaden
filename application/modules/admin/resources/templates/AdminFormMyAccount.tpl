<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="breadcrumb">
		<ul>
			<li><a href="{link href="admin/index/"}">Multimanage</a></li>
			<li><a href="{link href="admin/my-account/"}">{t text="My Account"}</a></li>
			<li><span>{t text="Edit My Account"}</span></li>
		</ul>
	</div>
	
	{$form->setModuleOwner('admin')}
	{$form->getView()}
</div>