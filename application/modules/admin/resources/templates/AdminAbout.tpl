<div class="padding-from-frame">
	<div class="about">
		<p class="longtext">{t text='Multimanage is een <strong>cms</strong> dat kan gebruikt worden voor het <strong>beheren</strong> van uw website, het <strong>verwerken</strong> van gegevens, tot ... alles wat u maar kan bedenken.'}</p>
		<p class="longtext">{t text='Dankzij een <strong>modulaire</strong> aanpak kiest u zelf welke modules geïnstalleerd worden; hierdoor verwerkt Multimanage de <strong>eenvoudigste taken</strong> maar even goed <strong>complexe processen</strong>.  Voor elk probleem bestaat een oplossing: bekijk enkele <strong>case-studies</strong> en je zal merken dat dankzij multimanage heel wat <strong>bedrijfsprocessen</strong> eenvoudiger (ja, zelfs leuker) werden om uit te voeren.'}</p>

<p class="longtext"><strong>{t text="Waarom Multimanage?"}</strong>
<ul><li>{t text="<strong>Modulair</strong>: je kiest (en betaalt) enkel wat je nodig hebt"}</li>
<li>{t text="<strong>Flexibel</strong>: bestaande modules kunnen uitgebreid/aangepast worden"}</li>
<li>{t text="Heldere (en consistente) <strong>gebruikersinterface</strong>"}</li>
<li>{t text="Het <strong>werkt</strong>"}</li><ul></p>
{*<blockquote>{t text="Is er iets wat Multimanage niet kan? Nee, het is simpelweg een kwestie van de juiste modules voor de juiste taak te kiezen. Bovendien kunnen bestaande modules uitgebreid worden, of nieuwe modules ontwikkeld worden, wanneer er nog geen oplossing is voor een specifiek probleem."}</blockquote>*}
		<p>
			<a href="{link href="admin/modules"}" class="arrow" title="Installed Modules">
				{t text="View installed modules"}
			</a>
		</p>
		<p>
			<a href="http://www.multimedium.be/portfolio" class="arrow" target="_blank">
				{t text="View websites that are powered by Multimanage"}
			</a>
		</p>
	</div>
</div>


