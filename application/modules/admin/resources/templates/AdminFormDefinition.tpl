<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	{if $definition}
		{assign var="module"   value=$definition->getModule()}
		<div class="breadcrumb">
			<ul>
				<li><a href="{link href="admin/overview/"}{$module->getId()}/{$definition->getDataObjectId()}">{t text=$module->getName()}</a></li>
				<li><span title="{t text=$definition->getDescription()|att}">{t text=$definition->getTitle()}</span></li>
			</ul>
		</div>
	{/if}
	
	{$form->setModuleOwner('admin')}
	{$form->getView()}
</div>