{assign var="module" value=$definition->getModule()}
{assign var="dataObjectId" value=$definition->getDataObjectId()}
{assign var="filters" value=$definition->getFilters()}

{if $filters|@count > 0}
	
	<div class="filters">
		<a href="#" class="filter-start">{t text="Apply filters to the list"}</a>
		
		{foreach from=$appliedFilters item="appliedFilter" name="i"}
			<div class="filter">
				<a href="#" class="remove right">{t text="Remove this filter"}</a>
				<a href="#" class="add right">{t text="Add a new filter"}</a>
				<div class="step-0">
					<select size="1">
						<option value="">{t text="Select a filter"}</option>
						{foreach from=$filters item="filter"}
							<option value="{$filter->getIndex()}" class="{$filter->getType()}"{if $filter->getIndex() == $appliedFilter->getIndex()} selected="selected"{/if}>
								{$filter->getTitle()}
							</option>
						{/foreach}
					</select>
				</div>
				<div class="step-1">
					{if $appliedFilter->getType() == 'where'}
						{include file="AdminListFilterComparisons.tpl" definition=$appliedFilter}
					{else}
						{include file="AdminListFilterValues.tpl" definition=$appliedFilter}
					{/if}
				</div>
				<div class="step-2">
					{if $appliedFilter->getType() == 'where'}
						{include file="AdminListFilterComparisonValue.tpl" definition=$appliedFilter operator=$appliedFilter->getComparisonOperator()}
					{elseif $appliedFilter->getType() == 'order'}
						{include file="AdminListFilterSortingOrders.tpl" definition=$appliedFilter}
					{/if}
				</div>
				<div class="clear"><!--  --></div>
			</div>
		{/foreach}
		
		{if $appliedFilters|@count == 0}
			<div class="hidden filter">
				<a href="#" class="remove right">{t text="Remove this filter"}</a>
				<a href="#" class="add right">{t text="Add a new filter"}</a>
				<div class="step-0">
					<select size="1">
						<option value="">{t text="Select a filter"}</option>
						{foreach from=$filters item="filter"}
							<option value="{$filter->getIndex()}" class="{$filter->getType()}">
								{$filter->getTitle()}
							</option>
						{/foreach}
					</select>
				</div>
				<div class="step-1">
				</div>
				<div class="step-2">
				</div>
				<div class="clear"><!--  --></div>
			</div>
		{else}
			<script type="text/javascript">$('a.filter-start').hide();</script>
		{/if}
	</div>

	<script type="text/javascript">
	{literal}
		// Make a backup of a filter bar (to restore, when all filters are removed):
		var filterTemplateBackup = $('.filter:eq(0)').html();
		
		// When clicked on "Apply Filters":
		$('a.filter-start').click(function() {
			// Remove the link "Apply Filters":
			$(this).hide();
			
			// Show the filters:
			$('.filter').removeClass('hidden');
			
			// Return false (overriding the default behavior of the link)
			return false;
		});

		// Initiate the filter interface:
		function initFilterTemplateInterface() {
			// Build Filter Interface:
			M_FilterTemplateInterface($('.filter'), {
				filterAjaxStep1Callback : function(selectedFilter) {
					return {
						url : jQueryBaseHref + '/admin/filter/{/literal}{$module->getId()}/{$dataObjectId}{literal}/'+ selectedFilter +'/1'
					};
				},
				filterAjaxStep2Callback : function(selectedFilter, selectedComparison) {
					return {
						url  : jQueryBaseHref + '/admin/filter/{/literal}{$module->getId()}/{$dataObjectId}{literal}/'+ selectedFilter +'/2',
						data : {
							operator : selectedComparison
						}
					};
				},
				filterAddButton : 'a.add',
				filterRemoveButton : 'a.remove'
			});

			// Listen to removal of filter bars:
			$(document).bind('M_FilterRowRemove', function($eventObject, $removedFilterBar) {
				// If the removed filter bar is the last one:
				if($('.filter').length == 1) {
					// Show the filter bar template again:
					$('a.filter-start').removeClass('hidden').after('<div class="hidden filter">' + filterTemplateBackup + '</div>');

					// Initiate the interface:
					initFilterTemplateInterface();

					// Reset the state of the interface:
					M_FilterTemplateInterfaceStep1Empty($('.filter'));
					M_FilterTemplateInterfaceStep2Empty($('.filter'));

					// Slide down the "Start applying filters" button again
					$('a.filter-start').slideDown(50);
				}
			});

			// Listen to update of filter values:
			$(document).bind('M_ChangeFilterValues', function($eventObject, collectionOfFilters) {
				// Initiate the data object (for Ajax Call)
				var _d = {};
				var _i = 0;
				
				// For each of the filters:
				for(var i in collectionOfFilters) {
					// Add the filter to the data
					_d['f' + (++ _i)] = collectionOfFilters[i].field;
					_d['v' + _i] = collectionOfFilters[i].value;
					if(collectionOfFilters[i].comparison) {
						_d['o' + _i] = collectionOfFilters[i].comparison;
					}
				}

				// If no filters are active
				if(collectionOfFilters.length == 0) {
					// Then, we add a variable that indicates that all filters
					// have been removed from the list:
					_d['clear'] = 1;
				}
				
				// Content Pane Viewport, where the instances are being listed:
				var $contentPaneViewport = $('#idContentPaneViewport');
				
				// Show loading state in Content Pane Viewport
				$contentPaneViewport.M_LoadingState({
					isLoading : true
				});
				
				// Do an Ajax Request, to update the listed instances:
				$.ajax({
					url  : {/literal}{if isset($smartFolder)}jQueryBaseHref + '/admin/instances-smart-folder/{$smartFolder->getId()}'{else}jQueryBaseHref + '/admin/instances/{$module->getId()}/{$dataObjectId}'{/if}{literal},
					data : _d,
					dataType : 'html',
					success : function(data) {
						// Show updated collection of instances:
						$contentPaneViewport.html(data);
				
						// Hide loading state in Content Pane Viewport
						$contentPaneViewport.M_LoadingState({
							isLoading : false
						});

						// Simulate an update via M_LoadInContentPane
						$('#idContentPaneViewport').trigger('M_LoadInContentPane_Update');
					}
				});
			});
		}

		initFilterTemplateInterface();
	{/literal}
	</script>
	
{/if}