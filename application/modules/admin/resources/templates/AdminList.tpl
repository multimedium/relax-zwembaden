{* Get the current application module, from definition *}
{assign var="module" value=$definition->getModule()}

{* Get the page-jumper, from the list view *}
{assign var="pageJumper" value=$listView->getPagejumper()}

{* View of applied filters, or to start applying filters: *}
{$filtersView->fetch()}

{* If at least 1 item is available: *}
{if $pageJumper->getTotalCount() > 0}
	
	{* View of listed instances *}
	<div class="content-pane-viewport fill-remaining-vspace {$module->getId()}-{$definition->getDataObjectId()}-list" id="idContentPaneViewport">
		{$listView->fetch()}
	</div>

{* If no items are available *}
{else}
	
	{* Show message *}
	<div class="content-pane-viewport fill-remaining-vspace {$module->getId()}-{$definition->getDataObjectId()}-list" id="idContentPaneViewport">
		<h2 class="big" style="padding-top: 80px; text-align: center;">
			{t text="No items are available in @listName" listName=$definition->getTitle()}
		</h2>
	</div>
	
{/if}
	
{* List options *}
{if $listOptions|@count > 0}
	<div class="menu-sticky-bottom" id="{$module->getId()}-{$definition->getDataObjectId()}-list-options">
		{foreach from=$listOptions item="option"}
			{$option}
		{/foreach}
	</div>
{/if}