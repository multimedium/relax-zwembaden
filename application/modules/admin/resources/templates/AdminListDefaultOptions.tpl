{* Plus Button *}
<a href="#" class="button-plus popmenu" id="idListPlusButton">+</a>

{* Contextual menu, activated by a click on the plus button *}
<div id="idListPlusButtonPopmenu">
	<ul>
		<li>
			<a href="{$uriNew}">
				{t text=$definition->getTitle() assignto="definitionLocalizedTitle"}
				{t text='Create a new item in <strong>@module</strong>' module=$definitionLocalizedTitle}
			</a>
		</li>
	</ul>
</div>