<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Multimanage. Unsupported Browser.</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="en" />
	<meta name="Keywords" content="multimanage, cms, multimedium, content, management, system" />
	<meta name="description" content="Multimanage - Powered by multimedium" />
	<meta name="copyright" content="Multimedium BVBA" />
	<meta name="author" content="Multimedium BVBA" />
	<meta http-equiv="imagetoolbar" content="no" />
	
	<link href="favicon.ico" rel="icon" type="image/x-icon" />
	<link rel="stylesheet" href="{link href="unsupported-browser.css" type="css" module="admin"}" type="text/css" media="screen" />
	
	<meta name="robots" content="all" />
	<meta name="revisit-after" content="5 days" />
	<meta name="rating" content="general" />
	<meta name="distribution" content="global" />
	<meta http-equiv="imagetoolbar" content="no" />
</head>
<body>
	<div id="wrapper">
		<h1><span>{t text="Multimanage. Unsupported Browser."}</span></h1>
		<p class="text">
			{t text="Your browser is not supported by <em>Multimanage</em>. Some parts of the application may not work properly with the browser you are using. For a better experience, please use any of the following browsers instead:"}
		</p>
		<div id="browsers">
			<ul>
				<li><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx" id="browser-ie7plus">Internet Explorer 7+</a></li>
				<li><a href="http://www.firefox.com" id="browser-firefox2plus">Firefox 2+</a></li>
				<li><a href="http://www.apple.com/safari/" id="browser-safari3plus">Safari 3+</a></li>
				<li><a href="http://www.google.com/chrome" id="browser-chrome">Google Chrome</a></li>
				<li><a href="http://www.opera.com" id="browser-opera">Opera</a></li>
			</ul>
		</div>
		<p class="text">
			{t text='If you require more information about supported browsers, please do not hesitate to <a href="http://www.multimedium.be/contact">contact Multimedium</a>.'}
		</p>
	</div>
</body>
</html>