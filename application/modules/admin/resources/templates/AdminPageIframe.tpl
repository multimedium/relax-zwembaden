<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	{include file="_AdminPageHead.tpl"}
</head>
<body>
	{if $messages|@count > 0}
	<div class="messages">
		{foreach from=$messages item="message"}
			<div class="message-row">
				<a href="#" class="message-close" title="{t|att text="Hide this message"}">{t text="Hide this message"}</a>
				<strong>{$message->title}</strong>
				{$message->description}
			</div>
		{/foreach}
	</div>
	{/if}
	<div id="idContentPaneIframe">
		{$content}
	</div>
	
	{* Javascript resources; to build interactive interface *}
	{$viewJavascript}
</body>
</html>