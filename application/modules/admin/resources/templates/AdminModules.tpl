<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="list">
		<table cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th width="220">{t text="Name"}</th>
					<th>{t text="Version"}</th>
					<th>{t text="Description"}</th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$modules item="module" name="i"}
				<tr{if $smarty.foreach.i.iteration % 2 == 0} class="row2"{else} class="row1"{/if}>
					<td width="220"><strong>{$module->getName()}</strong></td>
					<td>{$module->getVersion()}</td>
					<td>
						{$module->getDescription()}
						<div>
							<a href="{link href="admin/releases/"}{$module->getId()}" class="arrow clear">
								{t text="View Release notes"}
							</a>
						</div>
					</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
</div>