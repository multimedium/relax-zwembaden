<title>Multimanage</title>
	{* Disable IE compatibility mode *}
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="nl" />

	<meta name="Keywords" content="multimanage, cms, multimedium, content, management, system" />
	<meta name="description" content="Multimanage - Powered by multimedium" />
	<meta name="copyright" content="Multimedium BVBA" />
	<meta name="author" content="Multimedium BVBA" />
	<meta http-equiv="imagetoolbar" content="no" />

	<link href="{link href="favicon.ico" type="images" module="admin"}" rel="icon" type="image/x-icon" />

	<link rel="stylesheet" href="{link href="all.css" type="css" module="admin"}" type="text/css" media="screen" />
	<link rel="stylesheet" href="{link href="print.css" type="css" module="admin"}" type="text/css" media="print" />
	
	<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="{link href="browser-hacks-ie6.css" type="css" module="admin"}" media="screen"/>
	<![endif]-->

	{$viewCss}
	
	{* Adjust the logo *}
	{config var="logoFilePath" module="admin" assignto="logoFilePath"}
	{if $logoFilePath && $logoFilePath != ""}
		{link href=$logoFilePath prefix="false" assignto=logoFilePath}
		{literal}
		<style type="text/css">
			#header h1 {
				background-image: url('{/literal}{$logoFilePath}{literal}');
			}
		</style>
		{/literal}
	{/if}

	<meta name="robots" content="all" />
	<meta name="revisit-after" content="5 days" />
	<meta name="rating" content="general" />
	<meta name="distribution" content="global" />
	<meta http-equiv="imagetoolbar" content="no" />

	<script type="text/javascript" language="Javascript">
		var jQueryBaseHref = '{link}';
		var jQueryBaseHrefWithoutPrefix = '{link prefix="false"}';
	</script>
	
	<script src="{link href="jquery-1.8.3.min.js?201311051710" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	<script src="{link href="jquery-ui-1.10.3.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	<script src="{link href="jquery.fancybox.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	<script src="{link href="jquery.farbtastic.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	{* <script src="{link href="jquery-history-remote.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script> *}
	<script src="{link href="session.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	<script src="{link href="jquery.cookie.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	<script src="{link href="jquery.hoverIntent.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	<script src="{link href="jquery.chosen.min.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	<script src="{link href="jquery-m-interface.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	<script src="{link href="filters.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>
	<script src="{link href="admin.js" type="javascript" module="admin"}" type="text/javascript" language="Javascript"></script>