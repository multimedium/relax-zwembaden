<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="breadcrumb">
		<ul>
			<li><a href="{link href="admin/modules"}">{t text="Installed Modules"}</a></li>
			<li><span>{$module->getName()}</span></li>
		</ul>
	</div>
	<div class="list">
		<table cellpadding="0" cellspacing="0" border="0">
			<thead>
				<tr>
					<th>{t text="Version"}</th>
					<th>{t text="Description"}</th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$module->getReleaseNotes() item="note" name="i"}
				<tr{if $smarty.foreach.i.iteration % 2 == 0} class="row2"{else} class="row1"{/if}>
					<td>{$note->getVersion()}</td>
					<td>{$note->getDescription()}</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
</div>
<div class="small note-below-viewport">
	{* t 
		text='The release notes that are being published on this page may have been subject to change. Please visit <a href="@url">@url</a> for more information, and for the latest Multimanage Release Notes.'
		url="http://www.multimedium.be/multimanage/releases" *}
	
</div>