<ul id="{$menu->getId()}">
	{if $menu->getTitle()}
		<li id="{$menu->getId()}Title">
			<a href="#">
				{$menu->getTitle()}
			</a>
			<ul>
				{foreach from=$menu->getMenuItems() item="menuItem"}
					<li{if $menuItem->isActive()} class="active"{/if}><a href="{link href=$menuItem->getPath()}" title="{$menuItem->getDescription()|att}">{$menuItem->getTitle()}</a></li>
				{/foreach}
			</ul>
		</li>
	{else}
		{foreach from=$menu->getMenuItems() item="menuItem"}
			<li{if $menuItem->isActive()} class="active"{/if}><a href="{link href=$menuItem->getPath()}" title="{$menuItem->getDescription()|att}">{$menuItem->getTitle()}</a></li>
		{/foreach}
	{/if}
</ul>