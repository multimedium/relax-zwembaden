<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div class="list">
		<table cellpadding="0" cellspacing="0" border="0">
			<thead>
				<th class="hidden"></th>
				<th>{t text="Name"}</th>
				<th>{t text="Description"}</th>
			</thead>
			<tbody>
				{foreach from=$smartFolders item="folder" name="i"}
					<tr class="{if $smarty.foreach.i.iteration % 2 == 0}row2{else}row1{/if}" id="idListedInstance{$folder->getId()}">
						<td class="tooltip">
							<ul>
								<li>
									<a href="{link href="admin/smart-folder-delete"}/{$folder->getId()}" class="confirm delete" title="{t|att text="Delete this Smart Folder"}">
										{t text="Delete this Smart Folder"}
									</a>
								</li>
								<li>
									<a href="{link href="admin/smart-folder-edit"}/{$folder->getId()}" class="edit" title="{t|att text="Edit this Smart Folder"}">
										{t text="Edit this Smart Folder"}
									</a>
								</li>
							</ul>
						</td>
						<td width="160">{$folder->getName()}</td>
						<td><div class="longtext-preview">{snippet from=$folder->getDescription() length=150}</div></td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
	<script type="text/javascript" language="Javascript">
	{literal}
		$(document).ready(function() {
			$('.list td.tooltip').M_Tooltip({
				tooltipClass : 'row-options-tooltip',
				tooltipPositioningReferenceX : $('#idContentPaneViewport'),
				tooltipPositioningReferenceY : $('#idContentPaneViewport')
			});
		});
		
		// Init magic CSS, when tooltip is shown:
		$(document).bind('M_Tooltip_Show', function($eventObject, $parentElement, $tooltipElement) {
			$('.confirm').M_ConfirmLink({
				confirmText : 'Are you sure you want to remove this Smart Folder?'
			});
		});
	{/literal}
	</script>
</div>