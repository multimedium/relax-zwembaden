{$field->setModuleOwner('admin')}

<div class="field-row" id="{$viewId}">
	<div class="field-container">
		{if $field->getErrorMessage()}
			<div class="error-message">
				{$field->getErrorMessage()}
			</div>
		{/if}
		{assign var="input" value=$field->getInputView()} {$input}
		<label for="{$input->getId()}" class="checkbox">
			{$field->getTitle()}
		</label>
		{if $field->getDescription()}
			<div class="small note">
				{$field->getDescription()}
			</div>
		{/if}
	</div>
	<div class="clear"></div>
</div>