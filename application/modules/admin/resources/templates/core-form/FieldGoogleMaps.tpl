{jsexternal external="http://maps.google.com/maps/api/js?sensor=false" context="google-maps-api"}
{jsfile file="application/modules/admin/resources/javascript/jquery.gmap.js" context="google-maps-field"}

{* Create an empty div from which we will create a map *}
<div class="field-google-maps"
		align=""style="width:{$field->getWidth()}px;height:{$field->getHeight()}px;"
		id="{$field->getId()}-map">
</div>

{* Use 3 hidden input types to store the value (location), current zoomlevel and maptype *}
<input type="hidden" id="{$field->getId()}" name="{$field->getId()}" value="{$field->getValue()}"/>
<input type="hidden" id="{$field->getId()}-zoom-level" name="{$field->getId()}-zoom-level" value="{$field->getZoomLevel()}"/>
<input type="hidden" id="{$field->getId()}-map-type" name="{$field->getId()}-map-type" value="{$field->getMapType()}"/>