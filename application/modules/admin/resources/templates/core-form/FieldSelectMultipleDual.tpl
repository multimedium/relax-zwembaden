{cssfile file='application/modules/admin/resources/css/jquery-ui/ui.multiselect.css'}
{jsfile file='application/modules/admin/resources/javascript/jquery-ui-custom/ui.multiselect.js'}

{literal}
	<script type="text/javascript">
		<!--
			 $(document).ready(function()  {
				 {/literal}
				$("#{$field->getId()}").multiselect();
				{literal}
			});
		-->
	</script>
{/literal}

{assign var='fieldValue' value=$field->getValue()}

<select size="5" class="multiselect" name="{$field->getId()}[]" style="width:{$field->getWidth()}px;height:{$field->getHeight()}px" multiple="multiple" id="{$field->getId()}">

		{foreach from=$field->getGroups() item='group'}
			<optgroup label="{$group[0]}">
				{foreach from=$group[1] key='value' item='label'}
					<option value="{$value}"{if in_array($value, $fieldValue)} selected="selected"{/if}>{$label}</option>
				{/foreach}
			</optgroup>
		{/foreach}

		{foreach from=$field->getItems() key='value' item='label'}
			<option value="{$value}"{if in_array($value, $fieldValue)} selected="selected"{/if}>{$label}</option>
		{/foreach}

</select>