{if $field->getHint()}
	{assign var="fieldId" value=$field->getId()}
	{jsfile file="application/modules/admin/resources/javascript/jquery.input-hint.js" context="form-hint"}
	{jsinline script="$('#`$fieldId`').inputHint()" context="text-hint-`$fieldId`"}
{/if}
<input type="text" id="{$viewId}" name="{$field->getId()}" value="{$field->getValue()}" class="field-text" {if $field->getTabIndex()}tabindex="{$field->getTabIndex()}"{/if}{if $field->getReadonly()}readonly="readonly" {/if}{if $field->getMaxlength()}maxlength="{$field->getMaxlength()}" {/if}{if $field->getHint()} title="{$field->getHint()|att}" {/if}style="width: {if $field->getWidth()}{$field->getWidth()}px{else}200px{/if}" />