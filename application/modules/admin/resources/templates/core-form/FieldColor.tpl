{assign var="fieldValue" value=$field->getValue()}
{if !$fieldValue}
	{assign var="fieldValue" value="#000000"}
{else}
	{assign var="fieldValue" value=$fieldValue->getHex(true)}
{/if}
<input type="text" class="field-text" id="{$viewId}" style="background-color:{$fieldValue}" name="{$field->getId()}" value="{$fieldValue}" />

<script type="text/javascript" language="Javascript">
{literal}
	$(document).ready(function() {
		// Create color picker, if not already done
		var $colorPicker = $('#colorPicker-{/literal}{$viewId}{literal}');
		if($colorPicker.length == 0) {
			$('<div id="colorPicker-{/literal}{$viewId}{literal}"></div>').css({
				position: "absolute",
				top: "0px",
				left: "0px",
				backgroundColor: "#000000",
				padding: "20px"
			}).appendTo('body').hide();
		}
		
		// When focussing the field, show the color picker
		$('{/literal}#{$viewId}{literal}').focus(function() {
			var $t = $(this);
			$('#colorPicker-{/literal}{$viewId}{literal}').farbtastic('{/literal}#{$viewId}{literal}');
			$('#colorPicker-{/literal}{$viewId}{literal}').css({
				left : $t.offset().left,
				top : $t.offset().top + 30
			}).show();
		});
		
		// When blurring away from the field, hide the color picker
		$('{/literal}#{$viewId}{literal}').blur(function() {
			$('#colorPicker-{/literal}{$viewId}{literal}').hide();
		});
	});
{/literal}
</script>
