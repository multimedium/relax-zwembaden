<textarea id="{$viewId}" rows="{$field->getRows()}" cols="{$field->getCols()}" name="{$field->getId()}" class="field-text field-multiline" style="width: {$field->getWidth()}px; height: {$field->getHeight()}px"{if $field->getHint()} title="{$field->getHint()|att}"{/if}>{$field->getValue()}</textarea>

{* add charcounter when maxlength has been set *}
{if $field->getCharcounter()}
	{capture name="charcounter"}
		{literal}$("#{/literal}{$field->getId()}{literal}").charCounter({/literal}{$field->getMaxlength()}{literal}, {
				container: "<div></div>",
				classname: "charcounter",
				format: "%1",
				pulse: false,
				delay: 50
		});
		{/literal}
	{/capture}

	{jsinline script=$smarty.capture.charcounter}
	{jsfile file="application/modules/admin/resources/javascript/jquery.charcounter.js" context="jquery.charcounter.js"}
{/if}

{if $field->getAutogrow()}
	{jsinline file="application/modules/admin/resources/javascript/jquery.autogrow.js" context="jquery.autogrow.js"}
{/if}
