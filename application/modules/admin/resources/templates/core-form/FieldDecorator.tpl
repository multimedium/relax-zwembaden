{$field->setModuleOwner('admin')}

<div class="field-row" id="{$viewId}">
    {if $field->getTitle()}
	<label for="{$field->getId()}">
		{$field->getTitle()}
	</label>
    {/if}
	<div class="field-container">
		{if $field->getErrorMessage()}
			<div class="error-message">
				{$field->getErrorMessage()}
			</div>
		{/if}
		{$field->getInputView()}
		{if $field->getDescription()}
			<div class="small note">
				{$field->getDescription()}
			</div>
		{/if}
	</div>
	<div class="clear"></div>
</div>