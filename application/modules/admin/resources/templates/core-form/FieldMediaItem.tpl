{jsfile file="application/modules/media/admin/resources/javascript/field-media-item.js" context="field-media-item"}

{* Create a button to select media *}
<div class="field-media-item-button-wrapper">
	<div class="field-media-item-thumb left">
		
	</div>
	<a href="{link href="admin/overview/media"}?viewmode=iframe&amp;field=field-media-item&amp;fieldId={$field->getId()}"
	   class="field-media-item-button button left" 
	   title="{t text="Select media item"}"
	   data-id="{$field->getId()}"
	>
		<span>{t text="Choose..."}</span>
	</a>
</div>

{* Field media item *}
<input type="hidden" id="{$field->getId()}" name="{$field->getId()}" value="{$field->getValue()}"/>