{if $extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'png'}
	<div class="image-preview">
		<a href="{$link}" title="{$title|att}" class="fancybox">
			<img src="{link}/thumbnail/resized/300/150/{$file->getPath()}" alt="{$title|att}" border="0" />
		</a>
		{if $description}
			<p>{$description}</p>
		{/if}
	</div>
{else}
	<div class="file-preview file-{$extension}">
		<a href="{$link}">
			{$title}
		</a>
		{if $description}
			<p>{$description}</p>
		{/if}
	</div>
{/if}