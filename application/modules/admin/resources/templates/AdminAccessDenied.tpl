<div class="padding-from-frame">
	<h2 class="big">
		{t text="Access denied"}
	</h2>
	<p class="longtext">
		{t text="We are very sorry to inform you that you do not have access to the requested resource and/or permission. Please contact your webmaster, if you feel that this is a mistake and you should be granted access."}
	</p>
</div>