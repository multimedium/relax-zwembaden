<div class="content-pane-viewport fill-remaining-vspace" id="idContentPaneViewport">
	<div id="new-smart-folder-wrapper">
		<h2 class="big">{t text="Create a new Smart Folder"}</h2>
		<p class="with-h2-big longtext">
			{t text="A Smart Folder is an intelligent search folder that updates itself automatically."}
			{t text="In order to start your own Smart Folder, you first need to pick a module in which you want to search for items."}
		</p>
		<p class="longtext">
			{t text="Choose the module for the Smart Folder <strong><em>@name</em></strong>:" name=$folder->getName()}
		</p>
		<ul>
			{foreach from=$definitions item="definition"}
				{assign var="module" value=$definition->getModule()}
				<li>
					<a href="{link href="admin/smart-folder-define-module"}/{$folder->getId()}/{$module->getId()}/{$definition->getDataObjectId()}">
						{$definition->getTitle()}
					</a>
				</li>
			{/foreach}
		</ul>
	</div>
</div>