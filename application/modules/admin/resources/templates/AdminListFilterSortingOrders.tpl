<select size="1">
	<option value="ASC"{if $definition->getOrder() == "ASC"} selected="selected"{/if}>{t text="In ascending order (0-9, a-z, ...)"}</option>
	<option value="DESC"{if $definition->getOrder() == "DESC"} selected="selected"{/if}>{t text="In descending order (9-0, z-a, ...)"}</option>
</select>