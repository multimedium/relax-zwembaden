{assign var="isSortable" value=$definition->isInstancesDraggableForSorting($filters)}
{foreach from=$instances item="instance" name="i"}
	<tr class="{if $smarty.foreach.i.iteration % 2 == 0}row2{else}row1{/if}" id="idListedInstance{$instance.object->getId()}">
		{if $instance.options|@count > 0}
		<td class="tooltip">
			<ul>
				{foreach from=$instance.options item="option"}
					<li>{$option}</li>
				{/foreach}
			</ul>
		</td>
		{/if}
		{foreach from=$instance.columns item="column" name="j" key="k"}
			<td{if $smarty.foreach.j.first && $isSortable} class="draggable"{/if}>{$column}</td>
		{/foreach}
	</tr>
{/foreach}