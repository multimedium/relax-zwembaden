
// We do an AJAX Request every x minutes, to keep the current session alive and kicking :)
// First, we define the function that does the call. This is the function that
// will be called on each interval...
function getRefreshedAuthSessionInformation() {
	// Do the AJAX Call:
	$.ajax({
		url      : jQueryBaseHref + '/admin/authenticated',
		dataType : 'xml',
		success : function(data) {
			// When done, we check if the session is still alive:
			var identity = $('authenticated', data).text();
		}
	});
}

// Now, we declare the function that takes care of the session interval:
function setRefreshingAuthSessionInterval() {
	// First, it refreshes the session:
	getRefreshedAuthSessionInformation();

	// Then, it sets the interval for the next check:
	// (Currently, we check every 10 minutes)
	setTimeout('setRefreshingAuthSessionInterval()', 100000);
}

// Start the interval:
setRefreshingAuthSessionInterval();