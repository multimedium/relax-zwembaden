/**
 * Initiate Filter interface
 * 
 * The Filter interface is based on a template filter row. This template is used
 * to generate new filter rows. To do so, it uses $.fn.M_Clone() on the template 
 * element.
 * 
 * The template interface can be configured quite flexibly; the following options
 * are made available for the setup of a Filter interface:
 * 
 * <code>filterAjaxStep1Callback</code>
 * 
 * A callback function that provides with the Ajax Call Properties (url, data, 
 * dataType), in order to fetch the interface of step 1 in a given filter. The
 * result of the Ajax Call is shown in options.filterStep1Container
 * 
 * <code>filterAjaxStep2Callback</code>
 * 
 * A callback function that provides with the Ajax Call Properties (url, data, 
 * dataType), in order to fetch the interface of step 2 in a given filter. The
 * result of the Ajax Call is shown in options.filterStep2Container
 * 
 * <code>filterStep1Container</code>
 * 
 * This option contains a string, that will enable the filter interface to identify
 * the container of the interface in step 1 of a given filter. This string holds
 * a selector string (jquery xpath), to select the container inside the filter
 * row where the new interface is being requested.
 * 
 * <code>filterStep2Container</code>
 * 
 * This option contains a string, that will enable the filter interface to identify
 * the container of the interface in step 2 of a given filter. This string holds
 * a selector string (jquery xpath), to select the container inside the filter
 * row where the new interface is being requested.
 * 
 * <code>filterAddButton</code>
 * 
 * This string holds a selector string (jquery xpath), to select the button that
 * creates a new filter row. The value of this option is defaulted to "a.add"
 * 
 * <code>filterRemoveButton</code>
 * 
 * This string holds a selector string (jquery xpath), to select the button that
 * removes a selected filter row. The value of this option is defaulted to "a.remove"
 * 
 * <code>filterMaximumRows</code>
 * 
 * This option sets the maximum allowed number of filter rows in the filter 
 * interface.
 * 
 * @param $templateFilterBar
 * @param options
 * @return void
 */
function M_FilterTemplateInterface($templateFilterBar, options) {
	// Merge the provided options with the default values:
	$templateFilterBar.data('M_FilterInterfaceOptions', jQuery.extend({
		filterAjaxStep1Callback : null,
		filterAjaxStep2Callback : null,
		filterStep0Container : '.step-0',
		filterStep1Container : '.step-1',
		filterStep2Container : '.step-2',
		filterAddButton : 'a.add',
		filterRemoveButton : 'a.remove',
		filterMaximumRows : 8
	}, options));
	
	// Remove previous listeners:
	$(document).unbind('M_ChangeFilterValues');
	$(document).unbind('M_FilterRowRemove');
	
	// Trigger init event
	$(document).trigger('M_FilterInit');
	
	// Set the actions on the template filter bar:
	$templateFilterBar.each(function() {
		M_FilterTemplateInterfaceActions($(this));
	});
}

/**
 * Apply actions
 * 
 * This method is used to initially set the actions of the interface elements in 
 * a new filter row. This method will set the actions of the "add" and "remove"
 * button, listen to a change of filter, etc...
 * 
 * This method is called only once, on each of the filter rows, so the actions
 * get assigned to the interface elements.
 * 
 * @param $filterBar
 * @return void
 */
function M_FilterTemplateInterfaceActions($filterBar) {
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	
	// Listen to the event of the template filter bar being cloned:
	$filterBar.bind('M_Clone', function($eventObject, $clone, $original, $container) {
		// Copy the filter interface options:
		$clone.data('M_FilterInterfaceOptions', $original.data('M_FilterInterfaceOptions'));
		
		// Set the actions in the cloned filter bar
		M_FilterTemplateInterfaceActions($clone);
		
		// Empty step 0, 1 and 2
		$(_o.filterStep0Container + ' select:eq(0)', $clone).val('');
		M_FilterTemplateInterfaceStep1Empty($clone);
		M_FilterTemplateInterfaceStep2Empty($clone);
	});
	
	// Look for the "Add filter" button, and assign actions to the click event
	// of that button.
	$(_o.filterAddButton, $filterBar).click(function() {
		// When clicked, clone the filter template:
		// (We have already set an event listener to respond to this event; see
		// M_FilterTemplateInterface())
		$filterBar.M_Clone();
	});
	
	// Look for the "Remove filter" button, and assign the actions to the click
	// event of that button:
	$(_o.filterRemoveButton, $filterBar).click(function() {
		// When clicked, remove the filter:
		// First, we animate the filter out of view:
		$filterBar.show().slideUp(150, function() {
			// If filter data was attached to the filter bar:
			if(M_FilterTemplateInterfaceDataOfFilterBar($filterBar)) {
				// Update the contents of the list to which the filters are being applied:
				// (ignore the filter value contained by self)
				M_FilterTemplateInterfaceData($filterBar, true);
			}
			
			// Trigger event
			$(document).trigger('M_FilterRowRemove', [$(this)]);
			
			// Remove this filter, when animation is completed:
			$(this).remove();
			
			// Simulate window-resize. This way, we notify about the siblings
			// possibly being resized?
			$(window).resize();
		});
	});
	
	// Look for the menu that presents the available filters, and assign actions
	// to the change event:
	var $f = $(_o.filterStep0Container + ' select:eq(0)', $filterBar);
	$f.change(function() {
		// If a filter has been selected
		if(jQuery.trim($(this).val()) != '') {
			// Get the selected filter, and get the comparisons/values for the 
			// selected filter:
			M_FilterTemplateInterfaceStep1($filterBar, $(this).val());
		}
		// If no filter has been selected:
		else {
			M_FilterTemplateInterfaceStep1Empty($filterBar);
			M_FilterTemplateInterfaceStep2Empty($filterBar);
		}
	});
	
	// Assign actions to step 1 and 2 (if available)
	M_FilterTemplateInterfaceStep1Actions($filterBar, $f.val());
	M_FilterTemplateInterfaceStep2Actions($filterBar);
	M_FilterTemplateInterfaceSetFilterValue($filterBar);
}

/**
 * Empty Step 1
 * 
 * This method empties Filter Interface Step 1
 */
function M_FilterTemplateInterfaceStep1Empty($filterBar) {
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	
	// Empty step 2:
	$(_o.filterStep1Container, $filterBar).html('<span></span>');
}

/**
 * Get Interface of step 1
 * 
 * This method will fetch the Interface of Step 1, in a given filter row. It will
 * use options.filterAjaxStep1Callback to get the Ajax Call Properties, and
 * options.filterStep1Container to show the result of the ajax call.
 * 
 * Once the interface has been shown, this method will listen to changes in the
 * new interface. Any change will trigger the interface of step 2 to be rendered,
 * by M_FilterTemplateInterfaceStep2().
 * 
 * @param $filterBar
 * @param selectedFilter
 * @return void
 */
function M_FilterTemplateInterfaceStep1($filterBar, selectedFilter) {
	// Show loading state of this step
	M_FilterTemplateInterfaceToggleLoadingState($filterBar, 1, true);
	
	// Empty step 2
	M_FilterTemplateInterfaceStep2Empty($filterBar);
	
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	
	// Get the Ajax Request Properties:
	var a = M_FilterTemplateInterfaceValidAjaxProperties(_o.filterAjaxStep1Callback(selectedFilter), {}, 'html');
	
	// If the Ajax Request Properties are OK:
	if(a) {
		// Do the Ajax Request, in order to fetch the interface of Step 2:
		$.ajax({
			url      : a.url,
			data     : a.data,
			dataType : a.dataType,
			// When the Ajax Request has successfully been completed:
			success  : function(data) {
				// Hide loading state of this step
				M_FilterTemplateInterfaceToggleLoadingState($filterBar, 1, false);
				
				// We show the filter interface
				$(_o.filterStep1Container, $filterBar).html(data); // .hide().stop().fadeIn();
				
				// Apply the actions on the affected items
				M_FilterTemplateInterfaceStep1Actions($filterBar, selectedFilter, true);
			}
		});
	}
}

/**
 * Apply actions, in step 1
 * 
 * This method is used to initially set the actions of the interface elements in 
 * a filter's step 1 interface.
 * 
 * @param $filterBar
 * @return void
 */
function M_FilterTemplateInterfaceStep1Actions($filterBar, selectedFilter, triggerActions) {
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	
	// We set the actions on the filter interface.
	var $s = $(_o.filterStep1Container + ' select:eq(0)', $filterBar);
	if($s.length > 0) {
		// When the selection changes:
		$s.change(function() {
			// Get the selected item, and get the values for the selected filter + 
			// comparison:
			M_FilterTemplateInterfaceStep2($filterBar, selectedFilter, $(this).val());
		});
		
		// Also trigger first time?
		if(triggerActions) {
			$s.change();
		}
	}
}

/**
 * Empty Step 2
 * 
 * This method empties Filter Interface Step 2
 */
function M_FilterTemplateInterfaceStep2Empty($filterBar) {
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	
	// Empty step 2:
	$(_o.filterStep2Container, $filterBar).html('<span></span>');
}

/**
 * Get Interface of step 2
 * 
 * This method will fetch the Interface of Step 2, in a given filter row. It will
 * use options.filterAjaxStep2Callback to get the Ajax Call Properties, and
 * options.filterStep2Container to show the result of the ajax call.
 * 
 * Once the interface has been shown, this method will listen to changes in the
 * new interface. Any change will trigger the filter value of the filter row to
 * be set and/or updated.
 * 
 * @param $filterBar
 * @param selectedFilter
 * @return void
 */
function M_FilterTemplateInterfaceStep2($filterBar, selectedFilter, selectedComparison) {
	// Show loading state of this step
	M_FilterTemplateInterfaceToggleLoadingState($filterBar, 2, true);
	
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	
	// Get the Ajax Request Properties:
	var a = M_FilterTemplateInterfaceValidAjaxProperties(_o.filterAjaxStep2Callback(selectedFilter, selectedComparison), {}, 'html');
	
	// If the Ajax Request Properties are OK:
	if(a) {
		// Do the Ajax Request, in order to fetch the interface of Step 3:
		$.ajax({
			url      : a.url,
			data     : a.data,
			dataType : a.dataType,
			// When the Ajax Request has successfully been completed:
			success  : function(data) {
				// Hide loading state of this step
				M_FilterTemplateInterfaceToggleLoadingState($filterBar, 2, false);
				
				// We show the filter value interface
				$(_o.filterStep2Container, $filterBar).html(data); // .hide().stop().fadeIn();
				
				// If an input control is available:
				if(jQuery.trim(data) != '') {
					// Set the actions on the input control in step 2
					M_FilterTemplateInterfaceStep2Actions($filterBar, true);
				}
				// If no input control is available for the filter value:
				else {
					// M_FilterTemplateInterfaceSetFilterValue($filterBar);
					if(M_FilterTemplateInterfaceSetFilterValue($filterBar)) {
						M_FilterTemplateInterfaceData($filterBar);
					}
				}
			}
		});
	}
}

/**
 * Apply actions, in step 2
 * 
 * This method is used to initially set the actions of the interface elements in 
 * a filter's step 2 interface.
 * 
 * @param $filterBar
 * @return void
 */
function M_FilterTemplateInterfaceStep2Actions($filterBar, triggerActions) {
	
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	
	// Initiate MagicCss classes in filter field
	initMagicCss($(_o.filterStep2Container, $filterBar));
	
 	// Listen to update of date field
 	var $t = $(_o.filterStep2Container + ' .hasDatepicker', $filterBar);
 	if($t.length > 0) {
		$t.datepicker('option', 'onSelect', function(dateText, inst) {
			// Set filter value
			if(M_FilterTemplateInterfaceSetFilterValue($filterBar)) {
				M_FilterTemplateInterfaceData($filterBar);
			}
		});
		
		// Stop here
		return;
	}
	
	// Listen to text update:
	var $t = $(_o.filterStep2Container + ' input[type="hidden"]', $filterBar);

	// (visible) text field
	if($t.length == 0) {
		$t = $(_o.filterStep2Container + ' :text', $filterBar);
		if($t.length > 0) {
			$t.blur(function() {
				if(M_FilterTemplateInterfaceSetFilterValue($filterBar)) {
					M_FilterTemplateInterfaceData($filterBar);
				}
			});

			$t.keyup(function($eventObject) {
				var k = $eventObject.charCode ? $eventObject.charCode : $eventObject.keyCode ? $eventObject.keyCode : 0;
				if(k == 13) { // "Enter" key
					$(this).blur();
				}
			});

			if(triggerActions) {
				$t.blur();
			} else {
				if($t.val() == '') {
					$t.focus();
				}
			}
		}
	}
	// Hidden text field
	else {
		$t.change(function() {
			if(M_FilterTemplateInterfaceSetFilterValue($filterBar)) {
				M_FilterTemplateInterfaceData($filterBar);
			}
		});
	}
	
	// Listen to menu change:
	var $s = $(_o.filterStep2Container + ' select', $filterBar);
	if($s.length > 0) {
		$s.change(function() {
			if(M_FilterTemplateInterfaceSetFilterValue($filterBar)) {
				M_FilterTemplateInterfaceData($filterBar);
			}
		});
		
		if(triggerActions) {
			$s.change();
		}
	}
}

/**
 * Set filter value
 * 
 * Will set the filter value of the given filter row
 * 
 * @param $filterBar
 * @return bool
 * 		Returns TRUE if the filter data has been set, FALSE if not
 */
function M_FilterTemplateInterfaceSetFilterValue($filterBar) {
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	var _s = false;
	
	// Is a text input control available in step 2?
	// - First, check for date field (jQuery UI Datepicker)
	var f = $(_o.filterStep2Container + ' .hasDatepicker', $filterBar);
	var v = '';
	if(f.length == 0) {
		// If not found, we check for a hidden field
		f = $(_o.filterStep2Container + ' input[type="hidden"]', $filterBar);
		if(f.length == 0) {
			// If not found, we check for a simple text field:
			f = $(_o.filterStep2Container + ' :text', $filterBar);
			if(f.length == 0) {
				// If not found, we look for a select menu:
				f = $(_o.filterStep2Container + ' select', $filterBar);
			}
		}
		
		// If a field has been found, we download its value:
		if(f) {
			v = f.val();
		}
	// We have found a datepicker!
	} else {
		// Get its value, and format it:
		var _tempDate = f.datepicker('getDate');
		if(_tempDate) {
			v = (_tempDate.getYear() < 1900 ? 1900 + _tempDate.getYear() : _tempDate.getYear()) + '-' + (_tempDate.getMonth() + 1) + '-' + _tempDate.getDate();
		}
	}
	
	// If we have successfully prepared a value:
	if(jQuery.trim(v) != '') {
		_s = true;
		$filterBar.data('_M_FilterValue', {
			field      : $(_o.filterStep0Container + ' select:eq(0)', $filterBar).val(),
			comparison : $(_o.filterStep1Container + ' select:eq(0)', $filterBar).val(),
			value      : v
		});
	}
	// If not, set new filter value (without comparison operator)
	else {
		// Get filter value field:
		f = $(_o.filterStep1Container + ' select:eq(0)', $filterBar);
		if(f.length > 0) {
			// Set the new filter value:
			if(jQuery.trim(f.val()) != '') {
				_s = true;
				$filterBar.data('_M_FilterValue', {
					field      : $(_o.filterStep0Container + ' select:eq(0)', $filterBar).val(),
					value      : f.val(),
					comparison : null
				});
			}
		}
	}
	
	// Return result:
	return _s;
}

/**
 * Get filter values
 * 
 * This method is called whenever a new filter row is completed, or whenever a
 * filter row is removed from the interface. This method will collect all filter
 * values, and trigger/dispatch an event to notify about the new collection of 
 * filter values.
 * 
 * @param $filterBar
 * @return void
 */
function M_FilterTemplateInterfaceData($filterBar, ignoreSelf) {
	// Prepare a local variable, to hold the collection of values:
	var _c = new Array();
	
	// Prepare a local variable, to hold a filter value
	var _v;
	
	// For each of the other filter bars:
	$filterBar.siblings().each(function() {
		// Download filter value from the filter bar:
		_v = M_FilterTemplateInterfaceDataOfFilterBar($(this));
		
		// If a filter value is available, add it to the collection:
		if(_v) {
			_c.push(_v);
		}
	});
	
	// Add filter value of provided filter bar (not included in previous selection
	// of siblings):
	if(! ignoreSelf) {
		// Download filter value from the filter bar:
		_v = M_FilterTemplateInterfaceDataOfFilterBar($filterBar);
		
		// If a filter value is available, add it to the collection:
		if(_v) {
			_c.push(_v);
		}
	}
	
	// We trigger an event, to notify about the filter values being applied to
	// the list:
	$(document).trigger('M_ChangeFilterValues', [_c]);
}

/**
 * Get filter value
 * 
 * Accepts a $() element, and returns the filter value that is contained by it. 
 * Will check if the element really is a filter row, and whether or not a filter
 * value is contained by it.
 * 
 * NOTE:
 * Returns null if no filter value could have been fetched
 * 
 * @param $filterBar
 * @return Array
 */
function M_FilterTemplateInterfaceDataOfFilterBar($filterBar) {
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	
	// If the provided element is a filter bar:
	if(_o) {
		// If the filter bar has already received a filter value:
		var _v = $filterBar.data('_M_FilterValue');
		if(_v) {
			return _v;
		}
	}
	
	// If still here, there is no filter value!
	return null;
}

/**
 * Validate Ajax Call Properties
 * 
 * This method is used to validate the Ajax Call Properties, that are provided
 * by
 * 
 * - options.filterAjaxStep1Callback
 * - options.filterAjaxStep2Callback
 * 
 * @param a
 * @param defaultData
 * @param defaultDataType
 * @return stdObj
 */
function M_FilterTemplateInterfaceValidAjaxProperties(a, defaultData, defaultDataType) {
	// Check if an URL has been provided:
	if(! a.url) {
		alert('Cannot retrieve Filter Interface; Missing URL!');
		return;
	}
	
	// Check if data is availble, for the request:
	if(! a.data) {
		a.data = defaultData;
	}
	
	// Check if data type has been provided:
	if(! a.dataType) {
		a.dataType = defaultDataType;
	}
	
	// Check if data type is valid:
	if(a.dataType != 'html' && a.dataType != 'xml') {
		alert('Cannot retrieve Filter Interface; Wrong dataType!');
		return;
	}
	
	// Finally, return the ajax call properties:
	return a;
}

/**
 * Toggle loading state
 * 
 * @param $filterBar
 * @param stepIndex
 * @param isLoading
 * @return void
 */
function M_FilterTemplateInterfaceToggleLoadingState($filterBar, stepIndex, isLoading) {
	// Get the setup values:
	var _o = $filterBar.data('M_FilterInterfaceOptions');
	
	// Get the element where we should show the loading state:
	var $e;
	switch(stepIndex) {
		case 0: $e = $(_o.filterStep0Container, $filterBar); break;
		case 1: $e = $(_o.filterStep1Container, $filterBar); break;
		case 2: $e = $(_o.filterStep2Container, $filterBar); break;
	}
	
	if(isLoading) {
		$e.html('<div class="loading">Loading</div>');
	} else {
		$e.html('');
	}
}
