// When the document has finished loading:
$(window).load(function() {
	// Initiate the main interface:
	initInterfaceViewport();
	
	// Initiate the "Magic" CSS Classes:
	initMagicCss($('body'));

	// When the source pane is scrolled
	$('#idSourcePaneViewport').scroll(function() {
		// Then, we remember the current scrolling position
		$.cookie('SourcePaneScrollPosition', $(this).scrollTop());
	});

	// Get the previous scroll position of the source pane
	var sourcePaneScrollPosition = $.cookie('SourcePaneScrollPosition');

	// If a previous scrolling position was remembered:
	if(sourcePaneScrollPosition) {
		// Then, scroll to that position now:
		$('#idSourcePaneViewport').scrollTop(sourcePaneScrollPosition);
	}
	
	//init suckerfish plugin
	$('#idSourcePane.horizontal #idSourcePaneViewport > ul > li > a').M_Suckerfish({
		offsetY: 30,
		trigger: 'click',
		onGetSf : function($triggerElement) {
			return $triggerElement.next('ul');
		}
	});
	
	// HOTFIX for draggables in IE9
	// HOTFIX: We can't upgrade to jQuery UI 1.8.6 (yet)
	// This hotfix makes older versions of jQuery UI drag-and-drop work in IE9
	(function($){var a=$.ui.mouse.prototype._mouseMove;$.ui.mouse.prototype._mouseMove=function(b){if($.browser.msie&&document.documentMode>=9){b.button=1};a.apply(this,[b]);}}(jQuery));
});

// Function that initializes the main interface viewport
function initInterfaceViewport() {
	// Get default menu items (overview links)
	var $sourcePaneOverviewButtons = $('#idSourcePane a[href*="admin/overview/"], #idSourcePane a[href*="admin/smart-folder/"]');

	// Load menu items in Source pane in the Content Pane, via AJAX:
	$sourcePaneOverviewButtons.M_LoadInContentPane({
		callbackUrl : function($triggerElement) {
			var u = $triggerElement.attr('href');
			u = u.replace(/admin\/overview\//, 'admin/overview-ajax/');
			u = u.replace(/admin\/smart-folders\//, 'admin/smart-folders-ajax/');
			u = u.replace(/admin\/smart-folder\//, 'admin/smart-folder-ajax/');
			return u;
		}
	});
	
	// Also, we toggle the active item in the Source Pane
	$('#idSourcePane ul li a').M_ToggleActive({
		callbackGetToggledElement : function($triggerElement) {
			return $triggerElement.parent();
		}
	});
	
	// Also, we unselect active selection, when clicked in source pane
	/* $sourcePaneOverviewButtons.click(function() {
		$e = $.M_SelectablesActiveList();
		if($e) {
			$e.M_SelectionClear();
		}
	}); */
	
	// When we leave the page, we also unselect:
	$(window).unload(function() {
		$e = $.M_SelectablesActiveList();
		if($e) {
			$e.M_SelectionClear();
		}
	});
	
	// Make foldables in the Source Pane. For each of the menu titles:
	$('#idSourcePane h2').addClass('link-cursor').M_Foldable({unfoldOnChange: true});
	
	// When the content pane is updated, redo the magic css:
	$('#idContentPane').bind('M_LoadInContentPane_Update', function($eventObject, url) {
		initMagicCss($(this));
	});
	
	// Create a new Smart Folder Button:
	$('#idButtonNewSmartFolder').M_ElementCreator({
		// We create the new button in ...
		newButtonContainer       : $('#AdminSourcePaneSmartFolder'),
		// We decorate the temporary input control as following:
		callbackInputDecorator   : function($input) {
			$input.wrap('<li id="new-button-li-temp" style="padding: 10px;"></li>');
		},
		// We decorate the final element as following:
		callbackElementDecorator : function($element) {
			// Save the button to the database:
			$.M_AjaxRequest({
				url : jQueryBaseHref + '/admin/new-smart-folder/',
				data : {name : $element.text()},
				dataType : 'xml',
				success : function(data) {
					// Remove the temporary decoration:
					$('#new-button-li-temp').remove();
					
					// Create the button in the interface:
					$element.wrap('<li><a href="'+ jQueryBaseHref +'/admin/smart-folder/'+ $('smartfolder id', data).text() +'"></a></li>');
				}
			});
		}
	});
	
	// Close message row with close button:
	$('a.message-close').click(function() {
		// .slideUp() can be done by using animate with genFx("hide", 1);
		// This allows us to control every step of the animation too...
		$(this).parent().remove();
		
		// We simulate window resizing, so elements react to an updated viewport size
		$(window).resize();
	});
	
	// If not closed with the button, we close it automatically after 15 seconds
	setTimeout('$(".messages").remove(); $(window).resize();', 15000);

	// Hotkeys:
	$(document).bind('keyup', function(event) {
		// Prepare local variables:
		var $e;
		
		// SHIFT + ALT + ...
        if(event.shiftKey && event.ctrlKey) {
        	// F : Filter
        	if(event.which == 70) {
        		// Check if filter bars are available:
        		$e = $('.filter .add:visible:eq(0)');
        		if($e.length == 1) {
        			// If so, simulate a click on the "plus" button:
        			$e.click();
        		}
        		// Check if "Start filters" button is available
        		else {
        			// a.filter-start
        			$e = $('a.filter-start:visible:eq(0)');
        			if($e.length == 1) {
        				$e.click();
        			}
        		}
        	}
        	// D : Remove last filter
        	else if(event.which == 68) {
        		// Check if filter bar is available:
        		$e = $('.filter .remove:visible:last');
    			if($e.length == 1) {
    				$e.click();
    			}
        	}
        	// S : Submit form / Save filters (smart folder)
        	else if(event.which == 83) {
        		// Check if filter bars are available:
        		$e = $('.filter:visible');
    			if($e.length == 1) {
    				var name = prompt('Please introduce a name for your new Smart Folder');
    			}
    			// If not, we check if a form is available:
    			else {
    				$e = $('form:eq(0)');
    				if($e.length == 1) {
    					// If so, submit the form:
    					$e.submit();
    				}
    			}
        	}
        	// M : Close message(s)
        	else if(event.which == 77) {
        		$('a.message-close').click();
        	}
        	// L : Login / Logout
        	else if(event.which == 76) {
        		window.location.href = jQueryBaseHref + '/admin/logout';
        	}
        	// I : My Account
        	else if(event.which == 73) {
        		window.location.href = jQueryBaseHref + '/admin/edit-my-account';
        	}
        	// V : Download my vcard
        	else if(event.which == 86) {
        		window.location.href = jQueryBaseHref + '/admin/my-account-vcard';
        	}
        	// N : Create new item
        	else if(event.which == 78) {
        		// $('#idSourcePane .button-plus').click();
        		$('#idButtonNewSmartFolder').click();
        	}
        	// H : Homepage
        	else if(event.which == 72) {
        		window.location.href = jQueryBaseHref + '/admin';
        	}
        	// 1 : 1st item in source pane
        	else if(event.which == 49) {
        		$('#idSourcePaneViewport a:eq(0)').click();
        	}
        	// 2 : 2nd item in source pane
        	else if(event.which == 50) {
        		$('#idSourcePaneViewport a:eq(1)').click();
        	}
        	// 3 : 3rd item in source pane
        	else if(event.which == 51) {
        		$('#idSourcePaneViewport a:eq(2)').click();
        	}
        	// 4
        	else if(event.which == 52) {
        		$('#idSourcePaneViewport a:eq(3)').click();
        	}
        	// 5
        	else if(event.which == 53) {
        		$('#idSourcePaneViewport a:eq(4)').click();
        	}
        	// 6
        	else if(event.which == 54) {
        		$('#idSourcePaneViewport a:eq(5)').click();
        	}
        	// 7
        	else if(event.which == 55) {
        		$('#idSourcePaneViewport a:eq(6)').click();
        	}
        	// 8
        	else if(event.which == 56) {
        		$('#idSourcePaneViewport a:eq(7)').click();
        	}
        	// 9
        	else if(event.which == 57) {
        		$('#idSourcePaneViewport a:eq(8)').click();
        	}
        	// P : Toggle all to "active"
        	else if(event.which == 80) {
        		var sure = confirm('Are you sure you want to publish all inactive items?');
        		if(sure) {
        			$('a.toggle-0').click();
        		}
        	}
        	// U : Toggle all to "inactive"
        	else if(event.which == 85) {
        		var sure = confirm('Are you sure you want to unpublish all active items?');
        		if(sure) {
        			$('a.toggle-1').click();
        		}
        	}
        	// T : Toggle all (switch to opposite)
        	else if(event.which == 84) {
        		var sure = confirm('Are you sure you want to unpublish all active items, and publish all inactive items?');
        		if(sure) {
        			$('a.toggle-1, a.toggle-0').click();
        		}
        	}
        	// A : Select all (selectables)
        	else if(event.which == 65) {
        		// Get latest active list of selectables
        		$e = $.M_SelectablesActiveList();
        		if($e) {
        			$e.M_SelectionSelectAll();
        		}
        	}
        	// C : Clear selection (selectables)
        	else if(event.which == 67) {
        		// Get latest active list of selectables
        		$e = $.M_SelectablesActiveList();
        		if($e) {
        			$e.M_SelectionClear();
        		}
        	}
        }
    });
}

// Function that assigns actions to "magic" CSS Classes:
function initMagicCss($container, ignoreClasses) {
	// Bug fix for jQuery UI (is leaving a div.ui-effects-wrapper in the body!)
	// $('.ui-effects-wrapper').remove();
	
	// Initiate ignoreClasses
	if(typeof ignoreClasses == 'undefined') {
		ignoreClasses = {};
	}
	
	// jQuery Chosen:
	if(! ignoreClasses.chosen) {
		$('select[multiple][class!="multiselect"]', $container).each(function() {
			$(this).chosen({
			});
		});
	}
	
	// Popmenu:
	if(! ignoreClasses.popmenu) {
		$('.popmenu', $container).each(function() {
			$(this).M_Popmenu({
				menuContainer : $('#' + $(this).attr('id') + 'Popmenu')
			});
		});
	}
	
	// Fill remaining space vertically:
	if(! ignoreClasses.fillRemainingVspace) {
		$('.fill-remaining-vspace', $container).M_FillRemainingVerticalSpace({
			// Only take into account these siblings:
			watchSiblings : '#filtermenu-wrapper, #header, .messages, .menu-sticky-bottom, .filters, .content-pane, .source-pane'
		});
	}
	
	// Confirm links
	/*if(! ignoreClasses.confirm) {
		$('.confirm').M_ConfirmLink({
			confirmText : 'Are you sure?'
		});
	}*/
	
	// Delete links
	if(! ignoreClasses.deleteButton) {
		// For each of the links that delete data objects:
		$('a[href*="delete"]', $container).each(function() {
			// If delete actions have not yet been assigned to the button
			if(! $(this).data('M_DeleteButton')) {
				// Assign the actions to the button
				$(this).data('M_DeleteButton', 'yes');
				
				// When the link is clicked on:
				$(this).click(function() {
					// Confirm box:
					var sure = confirm('Are you sure you want to delete this item?');
					
					// If pressed OK:
					if(sure) {
						// Delete via AJAX
						$.M_AjaxRequest({
							// Compose the URL, based on the current URL
							url : $(this).attr('href').replace(/\/delete\//, '/delete-ajax/'),
							// The expected data type is XML
							dataType : 'xml',
							// When the response comes in:
							success : function(data) {
								var $id = $('response id', data);
								var $rs = $('response result', data);
								var $ll = $('.list table tbody');
								
								// If the object has been deleted successfully:
								if($rs.length == 1 && $id.length == 1 && $rs.text() == "1") {
									// Unselect the row
									$('#idListedInstance' + $id.text()).M_SelectableUnselect();
									
									// Remove the object's row:
									$('#idListedInstance' + $id.text()).fadeOut(200, function() {
										// Update the live list:
										if($ll.data('M_LiveListOptions')) {
											$ll.trigger('M_LiveListDeleteItem', $(this));
											// $ll.trigger('M_LiveListUpdate');
										}

										// Remove the item
										$(this).remove();
										
										// Remove the tooltip;:
										$.M_TooltipRemove();
									});
								}
							}
						});
					}
					
					// Override default behavior of the link
					return false;
				});
			}
		});
	}
	
	// Fancybox links
	if(! ignoreClasses.facybox) {
		$('.fancybox', $container).each(function() {
			
			// First check if there is already a fancybox:
			if( ! $(this).data().fancybox )	{
				$(this).fancybox({
					openSpeed : 'fast',
					closeSpeed : 'fast',
					prevSpeed : 'normal',
					nextSpeed : 'normal',
					prevEffect : 'fade',
					nextEffect : 'fade',
					closeClick : false,
					helpers : {
						title : {
							type : 'over'
						}
					},
					padding: 5
				});
			}
			
		});
	}
	
	// Toggle links:
	if(! ignoreClasses.toggle) {
		$('a[class^="toggle"]', $container).each(function() {
			if(! $(this).data('M_Toggle')) {
				$(this).data('M_Toggle', 'yes');
				$(this).click(function() {
					// Parse the AJAX link:
					var o = $(this);
					var u = $(this).attr('href').replace(/admin\/toggle\//, 'admin/toggle-ajax/');
					
					// Request toggle, via ajax
					$.M_AjaxRequest({
						url : u,
						dataType : 'xml',
						success : function(data) {
							// Look for new toggled value:
							var t = $('value', data);
							
							// If found:
							if(t.length > 0) {
								// Set class of clicked button
								o.attr('class', 'toggle-' + t.text());
								
								// Dispatch event, to notify about toggled button
								o.trigger('change');
								o.trigger('M_ToggleButton', [t.text()]);
							}
						}
					});
					
					return false;
				});
			}
		});
	}
	
	// Password fields:
	if(! ignoreClasses.passwordStrength) {
		// Get container of password fields:
		var $p = $('.password-strength').parent();
		
		// If at least 1 container has been found:
		if($p.length > 0) {
			// Look up the password field:
			$('input[type="password"]', $p).keyup(function() {
				// get a reference to the field:
				var $t = $(this);
				
				// If the password is not empty:
				if(jQuery.trim($t.val()) != '') {
					// Get the password's strength:
					$.M_AjaxRequest({
						url : jQueryBaseHref + '/admin/password-strength/',
						data : {
							password : $t.val()
						},
						success : function(data) {
							$('.password-strength', $t.parent()).remove();
							$t.parent().append(data);
						}
					});
				}
				// If the password is empty:
				else {
					// Remove the strength indicator
					$('.password-strength', $t.parent()).remove();
				}
			});
		}
	}
	
	// Currency fields
	if(! ignoreClasses.formatCurrency) {
		$('.field-currency.autoformat').blur(function(){
			var $t = $(this);
			
			//don't format is value is empty
			if ($t.val() == '') return;
			
			//format
			$.M_AjaxRequest({
				url : jQueryBaseHref + '/admin/format-currency/',
				dataType: 'json',
				data : {
					data : $(this).val()
				},
				success : function(data) {
					if (data) $t.val(data);
					else {
						alert('Oops... this value could not be formatted');
						$t.val('');
					}
				}
			});
		});
	}
	
	// Percent fields
	if(! ignoreClasses.formatPercent) {
		$('.field-percent.autoformat').blur(function(){
			var $t = $(this);

			//don't format is value is empty
			if ($t.val() == '') return;
			
			//format
			$.M_AjaxRequest({
				url : jQueryBaseHref + '/admin/format-percent/',
				dataType: 'json',
				data : {
					data : $(this).val()
				},
				success : function(data) {
					if (data) $t.val(data);
					else {
						alert('Oops... this value could not be formatted');
						$t.val('');
					}
				}
			});
		});
	}
	
	// Path element fields
	if(! ignoreClasses.formatPathElement) {
		//format the value of a uri-path-element-field to a string that
		//can be safely used as a path element
		$('.field-uri-path-element').M_BindOnce('blur', function(){
			formatUriPathElement($(this).val(), $(this));
		});

		//we can "relate" another field to this uri-path-element-field field.
		//so we can use the value of this related field to create a path-
		//element-string
		$('.field-uri-path-element-source-field-id').each(function(){
			var $sourceField = $('#'+$(this).val());

			//field doesn't exist
			if ($sourceField.length == 0) return false;

			//get the uri-path-element field which we will update
			var $uriPathElementField = $(this).siblings('.field-uri-path-element');

			//update the url on blur
			$sourceField.M_BindOnce('blur', function() {
				//only update if the field hasn't been set yet 
				//when the uri-path-element-field is readonly, the user cannot
				//modify this field so we can safely update it anytime
				if ($uriPathElementField.attr('readonly') || $uriPathElementField.val() == "") {
					var formatPath = true;
				}
				//format it
				formatUriPathElement($sourceField.val(), $uriPathElementField);
			});
		});
	}

	//Create foldable fieldsets
	$('fieldset.foldable .group-title').each(function(){
		$(this).M_Foldable({foldableElement: $(this).siblings('.group-fields')});
	});
}

/**
 * Format a value as a path-element string and update a field with this string
 *
 * @param string The source value, which will be formatted
 * @param object Form-field, which will be updated with the path-element-string
 * @return void
 * @author Ben Brughmans
 */
function formatUriPathElement($value, $field) {
	//don't format for an empty value
	if ($value == '') return false;

	//get the value to which we need to shorten the field
	var $shorten = $field.siblings('.field-uri-path-element-maximum-amount-of-words').val();

	//format
	$.M_AjaxRequest({
		url : jQueryBaseHref + '/admin/format-uri-path-element/',
		dataType: 'json',
		data : {
			data : $value,
			shorten : $shorten
		},
		success : function(data) {
			if ($field.val() != "" && data != $field.val()) {
				if (!confirm("Are you sure you want to update the url field?")) {
					return false;
				}
			}
			if (data) $field.val(data);
			else {
				alert('Oops... this value could not be used as url');
				$field.val('');
			}
		}
	});
}