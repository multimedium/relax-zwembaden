/**
 * Maps
 * 
 * This array is used to store different instances of Google Maps in order to
 * be able to use multiple maps on one page
 *
 */
var maps = new Array();

/**
 * jQuery gMap
 *
 * @author	Ben Brughmans
 */
(function($)
{
	// Main plugin function
	$.fn.gMap = function(options){
		
		// Iterate each matched element
		this.each(function(){
			
			var mapId = $(this).attr('id');
			var iteration = mapId.substring(0, mapId.length - 4)
			
			maps[iteration] = new Array();

			// Create new map and set initial options
			maps[iteration]['map'] = new google.maps.Map(this);

			// Get latitude and longitude from options
			var coordinates = $('#' + iteration).val().split(',');
			maps[iteration]['latlng'] = new google.maps.LatLng(coordinates[0],coordinates[1]);
			
			// Center the map
			maps[iteration]['map'].setCenter(maps[iteration]['latlng'])
			
			// Get Zoom level
			var zoomlevel = $('#' + iteration + '-zoom-level').val();
			maps[iteration]['map'].setZoom(parseInt(zoomlevel));
			
			// Get the Map type
			maps[iteration]['map'].setMapTypeId(google.maps.MapTypeId.ROADMAP);

			//add a marker
			maps[iteration]['marker'] = new google.maps.Marker({
				position: maps[iteration]['latlng'],
				map: maps[iteration]['map'],
				title: maps[iteration]['map'].title,
				draggable: true,
				visible: maps[iteration]['map'].showMarker
			});

			//update latitude and longitude when user stops dragging
			google.maps.event.addListener(maps[iteration]['marker'], 'dragend', function() {
				//update location when user stops dragging the marker
				var  fieldId  =    mapId.replace(/\-map$/,  '');
                updateFieldCoordinates(fieldId,  maps[iteration]['marker'].position);
			});

			//update the zoomlevel-value when user changes the zoom
			google.maps.event.addListener(maps[iteration]['marker'], 'zoom_changed', function() {
				getFieldZoomLevel(iteration).val(maps[iteration]['marker'].getZoom());
			});

			//update the maptype-value when user changes the maptype
			google.maps.event.addListener(maps[iteration]['marker'], 'maptypeid_changed', function() {
				getFieldMapType(iteration).val(maps[iteration]['marker'].getMapTypeId());
			});			

		});
	}

	//get the button which will retrieve the location using
	//the current address
	$('.address-container .button-submit').click(function(){

		// Get the id of the current map
		var mapId = $(this).attr('id').replace('-button-locate','');

		//get coÃ¶rdinates by address
		var latlng = getLatLngByAddress(
			getAddress($(this).closest('.field-row')),
			function($coordinates){

				// Set the marker
				maps[mapId]['marker'].visible = true;
				maps[mapId]['marker'].setPosition($coordinates);
				maps[mapId]['map'].setCenter($coordinates);

				// Update the field value
				updateFieldCoordinates(mapId, $coordinates);

				// Update the field value
				updateFieldZoomLevel(mapId, maps[mapId]['map'].getZoom());
			}
		);
	});

	//get the button which will clear the current location
	$('.address-container .button').click(function(){
		
		// Get the id of the current map
		var mapId = $(this).attr('id').replace('-button-clear','');

		// Hide marker
		maps[mapId]['marker'].setVisible(false);
		
		// Reset coordinates
		resetCoordinates(mapId);
		
	});

	/**	
	 * Update the field with zoom level data
	 *
	 * @param object
	 * @return void
	 */
	function updateFieldZoomLevel(mapId,zoomLevel) {
		$('#' + mapId + '-zoom-level').val(zoomLevel);
	}
	
	/**	
	 * Update the field with geolocation data
	 *
	 * @param object
	 * @return void
	 */
	function updateFieldCoordinates(mapId,latlng) {
		$('#' + mapId).val(latlng.lat()+","+latlng.lng());
	}

	/**
	 * Reset the value of the field with geolocation data
	 *
	 * @return void
	 */
	function resetCoordinates(mapId) {
		('#' + mapId).val('');
	}

	/**
	 * Get the value of the current address
	 *
	 * @return string
	 */
	function getAddress(object) {	
		var address = object.val();

		if(address == "") {
			// street address
			var tmp = object.find('.field-address-street input')
			if (tmp.length != 0 && tmp.val() != tmp.attr('title')) {
				address +=  ' ' + tmp.val();
			}
			// street number
			tmp = object.find('.field-address-street-number input')
			if (tmp.length != 0 ) {
				address +=  ' ' + tmp.val();
			}
			// postal code
			tmp = object.find('.field-address-postal-code input')
			if (tmp.length != 0 && tmp.val() != tmp.attr('title')) {
				address +=  ' ' + tmp.val();
			}

			//city field: autocomplete? If so: use the text_Value
			var cityField = object.find('.field-address-city input');
			var city = '';

			if (cityField.siblings('.autocomplete').length > 0) {
				city = $('#'+cityField.attr('name')+'_textValue').val();
			}
			else {
				city = cityField.val();
			}
			address += ' '+ city;
			
			// region
			tmp = object.find('.field-address-postal-region input')
			if (tmp.length != 0 && tmp.val() != tmp.attr('title')) {
				address +=  ' ' + tmp.val();
			}
			
			// country: input box
			tmp = object.find('.field-address-country input')
			if (tmp.length != 0 && tmp.val() != tmp.attr('title')) {
				address +=  ' ' + tmp.val();
			}
			// country: select
			tmp = object.find('.field-address-country select')
			if (tmp.length != 0 && tmp.val() != tmp.attr('title')) {
				address +=  ' ' + tmp.val();
			}
			
		}

		return address;
	}

	/**
	 * Retrieve the latitude and longitude of an address
	 *
	 * @param string _address
	 * @param function _cb
	 *		Callback function to which the position is passed as parameter
	 * @return object
	 */
	function getLatLngByAddress(_address, _cb) {
		//initialize a geocode to retrieve coÃ¶rdinates later on
		var geocoder = new google.maps.Geocoder();

		//call
		geocoder.geocode( { 'address': _address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var coordinates = results[0].geometry.location;

				//pass position to callback
				if (typeof _cb == "function"){
					_cb(coordinates);
				}

			} else {
				alert("Oops... we could not locate the address you entered");
			}
		});
	}
	
})(jQuery);
