// Add a variable to hold the filtermenu item
var $filtermenu;
var $filtermenuClear;
var $menuListItems;

$(document).ready(function() {
	// initialize the variables to work with
	$filtermenu = $('#filtermenu');
	$filtermenuClear = $('#filtermenu-clear');
	$menuListItems = $('#idSourcePaneViewport li');
	$menuListTitles = $('#idSourcePaneViewport h2');

	// Get the default value
	var $defaultValue = $.cookie('m-filter-menu');
	if($defaultValue == null) {$defaultValue = ''}

	// Add filtermenu input field
	$filtermenu.val($defaultValue);
	$filtermenu.inputHint({});

	// on keydown: update the source
	$filtermenu.keyup(function() {
		filterSourcePaneViewport();
	});

	// on change: save the value in a cookie
	$filtermenu.blur(function() {
		if(!$(this).hasClass('hint')) {
			$.cookie('m-filter-menu', $(this).val(), {path: '/'});
		} else {
			$.cookie('m-filter-menu', null, {path: '/'});
		}
	});

	// Click on clear button: update the menu
	$filtermenuClear.click(function(e) {
		$filtermenu.val('');
		filterSourcePaneViewport();
		$filtermenu.trigger('blur');

		e.preventDefault();
	});

	// Trigger the filter if a default value is found
	if($defaultValue != '') {
		filterSourcePaneViewport();
	}
});

/**
 * Filter the menu
 */
function filterSourcePaneViewport() {
	// Search for list-items in the menu
	$string = $filtermenu.val().toLowerCase();

	if($string == '') {
		$filtermenuClear.hide();
	} else {
		$filtermenuClear.show();
	}

	// Show/hide the list-items
	$menuListItems.each(function() {
		var $text = $(this).text().toLowerCase();
		if($text.substring(0, $string.length) == $string) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});

	// Show/hide the titles
	$menuListTitles.each(function() {
		if($('li:visible', $(this).next('ul')).length > 0) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
	
}