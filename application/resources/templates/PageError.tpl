{* This page is shown when a page error occurs. *}

<div class="padding-from-frame">
	<h2 class="big">{t text="Oops... something went wrong"}</h2>
	<div class="reading">
		<p>
			{t text="We are very sorry: the page you requested can't be displayed. We will try to fix this as soon as possible."}	
		</p>
		<p>
			{link assignto="link"}
			{t text='You can safely <a href="@homepage">continue browsing</a> our website.' homepage=$link}
		</p>
	</div>
</div>