{* This page is shown when a 404 error occurs. *}
<div class="padding-from-frame">
	<h1>{t text="Oops... this page doesn't seem to exist (anymore)"}</h1>
	<div class="reading">
		<p>
			{t text="The page you're looking for has been moved, does not exist anymore, or has never existed."}
		</p>
		<p>
			{link assignto="link"}
			{t text='You can safely <a href="@homepage">continue browsing</a> our website.' homepage=$link}
		</p>
	</div>
</div>