<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		{* Page Title *}
		<title>{$pageTitle}</title>

		{* Meta data: Character Encoding, and active language *}
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="{locale category="LANG"}" />

		{* Meta data: Page Description and keywords *}
		<meta name="description" content="{$pageDescription}" />

		{* Meta data: Author and Copyright information *}
		<meta name="copyright" content="{identity get="@name"}" />
		<meta name="author" content="Multimedium BVBA" />
		<meta name="robots" content="all" />
		<meta name="revisit-after" content="5 days" />
		<meta name="rating" content="general" />
		<meta name="distribution" content="global" />

		{* RSS *}
		<link rel="alternate" type="application/rss+xml" title="{t|att text="Relaxzwembaden - nieuws"}" href="{link href="nieuws/rss"}" />

		{* Favicon *}
		<link href="{link type="images" href="favicon.ico"}" rel="icon" type="image/x-icon" />

		{* CSS *}
		<link rel="stylesheet" href="{link type="css" href="all.css"}" type="text/css" media="screen" />
		<!--[if lt IE 8]>
		<link rel="stylesheet" href="{link type="css" href="ie.css"}" type="text/css" media="screen" />
		<![endif]-->

		{* If a Canonical URI applies to this view: *}
		{if $canonicalUri}
			{* Then, include the link to the Canonical URI *}
			<link rel="canonical" href="{$canonicalUri}" />
		{/if}

		{* Additional CSS, to be loaded for this specific view *}
		{css}

		{* Google Analytics *}
		{google_analytics ua="UA-22881015-1"}
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				{* Logo *}
				<a href="{link}" id="logo" title="{t|att text="homepage"}">{t text="Relax zwembaden - zwembadbouwer"}</a>

				{* Navigation *}
				<ul id="nav">
					<li{if $activePath == 'relaxzwembaden'} class="active"{/if}><a href="{link href="relaxzwembaden"}" title="{t|att text="meer informatie over onze zwembaden"}">{t text="relaxzwembaden"}</a></li>
					<li{if $activePath == 'projecten'} class="active"{/if}><a title="{t|att text="foto's van onze projecten"}" href="{link href="projecten"}">{t text="projecten"}</a></li>
					<li{if $activePath == 'contact'} class="active"{/if}><a title="{t|att text="contacteer ons"}" href="{link href="contact"}">{t text="contact"}</a></li>
					<li{if $activePath == 'nieuws'} class="active"{/if}><a title="{t|att text="nieuws"}" href="{link href="nieuws"}">{t text="nieuws"}</a></li>
				</ul>

				{* News *}
				<div id="latest-news">
					{if $persistent.latestNews|@count > 0}
					<ul>
					{foreach from=$persistent.latestNews item=news}
					<li><a title="{snippet from=$news->getText() length=100}" href="{link href="nieuws/"}{$news->getUrl()}">{$news->getTitle()}</a></li>
					{/foreach}
					</ul>

					{else}
					<p class="italic">
						{t text="momenteel geen nieuws"}
					</p>
					{/if}
				</div>
				
				{* Some legal text *}
				<div id="header-legal">
					{if $persistent.infoText}{snippet from=$persistent.infoText->getText() length=400}{/if}
				</div>
			</div>
			{* Content (rendered by the wrapped template) *}
			{$content}

			<div id="footer">
				<div id="footer-info" class="footer-pff">
					<div id="footer-open-hours">
						{t text="U kan ons enkel nog bezoeken na telefonische afspraak en bereiken via mail"}
					</div>
					<div id="footer-address">
						Relaxzwembaden BV<br/>
						{identity get="@address/street"}<br/>
						{identity get="@address/postalCode"} {identity get="@address/city"}
					</div>
					<div id="footer-contact">
						{identity get="@phone/tel1"}<br/>
						{identity get="@phone/tel2"}<br/>
					</div>
					<div id="footer-email">
						<a title="{t|att text="mail"} {identity|att get="@name"}" href="mailto:{identity get="@email"}">{identity get="@email"}</a>
					</div>
				</div>
				<hr/>
				<div id="footer-credits" class="footer-pff">
					<div class="left">
						{date format="yyyy"} &copy; All rights reserved by V.W.PROJECTS nv
					</div>
					<div class="right">{t|escape text="fotografie & webdesign"} &copy; <a title="{t text="mail Foto Van Huffel"}" href="http://www.fotovanhuffel.be">www.fotovanhuffel.be</a></div>
				</div>
			</div>
		</div>

		{* Variables for javascript *}
		<script type="text/javascript" language="Javascript">
			var jQueryBaseHref = '{link}';
			var jQueryBaseHrefWithoutPrefix = '{link prefix="false"}';
		</script>

		{* Javascript files *}
		<script src="{link href="jquery-1.4.2.min.js" type="javascript"}" type="text/javascript" language="Javascript"></script>
		<script src="{link href="jquery.fancybox-1.3.0.pack.js" type="javascript"}" type="text/javascript" language="Javascript"></script>
		<script src="{link href="jquery.easing-1.3.js" type="javascript"}" type="text/javascript" language="Javascript"></script>
		<script src="{link href="jquery.m.js" type="javascript"}" type="text/javascript" language="Javascript"></script>
		<script src="{link href="page.js" type="javascript"}" type="text/javascript" language="Javascript"></script>

		{* Additional javascript *}
		{javascript}
	</body>
</html>