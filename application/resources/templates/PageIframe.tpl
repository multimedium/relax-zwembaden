<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		{* Page Title *}
		<title>{$pageTitle}</title>

		{* Meta data: Character Encoding, and active language *}
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="{locale category="LANG"}" />

		{* Meta data: Page Description and keywords *}
		<meta name="keywords" content="" />
		<meta name="description" content="{$pageDescription}" />

		{* Meta data: Author and Copyright information *}
		<meta name="copyright" content="{identity get="@name"}" />
		<meta name="author" content="Multimedium BVBA" />
		<meta name="robots" content="all" />
		<meta name="revisit-after" content="5 days" />
		<meta name="rating" content="general" />
		<meta name="distribution" content="global" />

		{* Favicon *}
		<link href="{link type="images" href="favicon.ico"}" rel="icon" type="image/x-icon" />

		{* CSS *}
		<link rel="stylesheet" href="{link type="css" href="iframe.css"}" type="text/css" media="screen" />

		{* If a Canonical URI applies to this view: *}
		{if $canonicalUri}
			{* Then, include the link to the Canonical URI *}
			<link rel="canonical" href="{$canonicalUri}" />
		{/if}

		{* Additional CSS, to be loaded for this specific view *}
		{css}

		{* Google Analytics *}
		{google_analytics ua="?"}
	</head>
	<body>
		{* Content (rendered by the wrapped template) *}
		{$content}

		{* Variables for javascript *}
		<script type="text/javascript" language="Javascript">
			var jQueryBaseHref = '{link}';
			var jQueryBaseHrefWithoutPrefix = '{link prefix="false"}';
		</script>

		{* Javascript files *}
		<script src="{link href="jquery-1.4.2.min.js" type="javascript"}" type="text/javascript" language="Javascript"></script>
		<script src="{link href="jquery.fancybox-1.3.0.pack.js" type="javascript"}" type="text/javascript" language="Javascript"></script>
		<script src="{link href="jquery.easing-1.3.js" type="javascript"}" type="text/javascript" language="Javascript"></script>
		<script src="{link href="page.js" type="javascript"}" type="text/javascript" language="Javascript"></script>

		{* Additional javascript *}
		{javascript}
	</body>
</html>