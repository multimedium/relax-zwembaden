{$content}
<br/><br/>
<p style="font-family: Arial, Verdana, Helvetica; color: #000000;font-size: 14px;  line-height: 140%;">
{t text="Met vriendelijke groeten"},<br/><br/>
{identity get="@name"}<br/>
{identity get="@address/street"}<br/>
{identity get="@address/postalCode"} {identity get="@address/city"}<br/>
{t text="tel."} {identity get="@phone/tel"}<br/>
{t text="fax"} {identity get="@phone/fax"}<br/>
{t text="e-mail"} {identity get="@email"}<br/>
{t text="www"} <a href="{link}">{link}</a>
</p>