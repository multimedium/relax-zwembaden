
// When the DOM is ready
$('document').ready(function(){

	/* -- Fancybox -- */

	// Set Fancybox defaults
	$.fn.fancybox.defaults.overlayColor = '#000';
	$.fn.fancybox.defaults.overlayOpacity = 0.7;
	$.fn.fancybox.defaults.hideOnContentClick = false;
	$.fn.fancybox.defaults.padding = 0;
	$.fn.fancybox.defaults.titlePosition = "inside";
	$.fn.fancybox.defaults.centerOnScroll = true;
	
	// Activate Fancybox links
	$('a.fancybox').fancybox();
	
	/* -- Thumbnails -- */

	// Fade in thumbnail images
	$('.tile a, .fancybox, .thumbnail').children('img').FadeInImageOnLoad();
});

/**
 * Toggle a html-element
 * 
 * This function will show or hide (animated) a element
 *
 * @param element jQuery element
 * @param bool TRUE to show, FALSE to hide
 * @return void
 */
function toggleElement(element, displayFlag, speed) {
	if (!$(element).is(':animated')) {
		(displayFlag) ? $(element).stop().slideDown(speed) : $(element).stop().slideUp(speed);
	}
}