/**
 * Assign Unique ID
 * 
 * Will assign a Unique ID to the matched HTML Elements. If the element already
 * has a value for the id="" attribute, this function will leave the element
 * unaffected. Elements that do not have a value for the id="" attribute are
 * assigned a unique one.
 * 
 * @example alert($('.anonymous').P_AssignId().attr('id'));
 * @return $(matchedElements)
 */
$.fn.AssignId = function() {
	if(typeof M_UniqueIdCounter == 'undefined') {
		M_UniqueIdCounter = 0;
	}
	
	this.each(function() {
		var i = $(this).attr('id');
		if(! i) {
			$(this).attr('id', 'p-interface-id-' + (++ M_UniqueIdCounter));
		}
	});
	
	return this;
}

/**
 * Fade in the matched image(s), when loaded
 *
 * @return $(matchedElements)
 */
$.fn.FadeInImageOnLoad = function(options) {
	// Set the options:
	var ops = jQuery.extend()
	// For each of the matched elements:
	$(this).each(function() {
		// Hide the image
		$(this).stop().fadeTo(0, 0.1);

		// Backup some variables:
		var $t   = $(this);
		var href = $t.attr('src');

		// Create an image object:
		var img = new Image();

		// Temporarily change the image to a blank image:
		$t.attr('src', jQueryBaseHrefWithoutPrefix + '/application/resources/images/blank.gif');
		
		// When the final image is loaded:
		img.onload = function() {
			// Fade in the image
			$t.attr('src', href);
			$t.fadeTo(300, 1);
		}

		// Load the final image
		img.src = href;
	});
}

/**
 * Get (translated) text
 * 
 * Will fetch a text from the application's locale message catalog, and return it.
 * Will work like the Locale API in PHP, like the corresponding t() function
 * 
 * @return string
 */
$.t = function(untranslatedString) {
	return untranslatedString;
}