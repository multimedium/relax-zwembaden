<?php
M_Loader::loadView('PageView');
class PageErrorView extends PageView {
    /**
     * Set an exception
     *
     * @param Exception $e
     */
    public function setException(Exception $e) {
		$this->assign('exception', $e);
	}

	/**
	 * Get Resource 
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath().'/PageError.tpl');
	}
}