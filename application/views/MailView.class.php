<?php
/**
 * MailView class
 *
 * @package App
 */
abstract class MailView extends M_ViewHtml {

	/**
	 * Holds the resourcewrapper
	 *
	 * @var M_ViewHtmlResource
	 */
	private $_resourceWrapper;
	
	/**
	 * Render HTML
	 *
	 * @see M_ViewHtml::_render()
	 * @return string
	 */
	protected function _render() {
		// Assign variables to the page template resource, and render:
		$rs = $this->_getResourceWrapper();
		$rs->assign('content', parent::_render());
		$rs->assign('identity', M_ApplicationIdentity::getInstance()->getContact());
		//$rs->assign('googleAnalytics', M_Application::getConfig()->GoogleAnalytics);
		return $rs->fetch();
	}

	/**
	 * Get wrapper resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceWrapper() {
		if (is_null($this->_resourceWrapper)) {
			$this->_resourceWrapper = M_ViewHtmlResource::constructWithTemplateBasePath('Mail.tpl');
		}

		return $this->_resourceWrapper;
	}
}