<?php
class Page404View extends PageView {
	/**
	 * Get Resource 
	 * @return M_ViewHtmlResource
	 */
	protected function getResource() {
		return new M_ViewHtmlResource(self::getTemplatesPath().'/Page404.tpl');
	}
}