<?php
/**
 * PageView
 * 
 * @package App
 */
abstract class PageView extends M_ViewHtml {

	/* -- CONSTANTS -- */

	/**
	 * View mode
	 *
	 * This constant can be used to address a view mode. Typically, this method
	 * is used as an argument to {@link PageView::setViewMode()}
	 *
	 * @access public
	 * @var string
	 */
	const VIEW_MODE_IFRAME = 'iframe';

	/**
	 * View mode
	 *
	 * This constant can be used to address a view mode. Typically, this method
	 * is used as an argument to {@link PageView::setViewMode()}
	 *
	 * @access public
	 * @var string
	 */
	const VIEW_MODE_AJAX_INLINE = 'ajax-inline';

	/**
	 * (Default) View mode
	 *
	 * This constant can be used to address a view mode. Typically, this method
	 * is used as an argument to {@link PageView::setViewMode()}
	 *
	 * @access public
	 * @var string
	 */
	const VIEW_MODE_NORMAL = 'normal';

	/* -- PROPERTIES -- */

	/**
	 * SEO Entry
	 *
	 * @see PageView::_getSeo()
	 * @access private
	 * @var string
	 */
	private $_seo = 0;

	/**
	 * Page title
	 *
	 * @access private
	 * @var string
	 */
	private $_pageTitle;
	
	/**
	 * Page Description (META-DATA)
	 *
	 * @access private
	 * @var string
	 */
	private $_pageDescription;

	/**
	 * Wrapper Template
	 *
	 * @see PageView::_getResourceWrapper()
	 * @access private
	 * @var M_ViewHtmlResource
	 */
	private $_resourceWrapper;

	/**
	 * Active path
	 *
	 * @see getActivePath()
	 * @access private
	 * @var string
	 */
	private $_activePath;

	/**
	 * Canonical URI
	 *
	 * @see PageView::getCanonicalUri()
	 * @access private
	 * @var M_Uri
	 */
	protected $_canonicalUri;

	/**
	 * View mode
	 *
	 * Stores the view mode of the {@link PageView} object.
	 *
	 * @access private
	 * @var string
	 */
	private $_viewMode;

	/* -- SETTERS -- */
	
	/**
	 * Set the page description
	 * 
	 * @access public
	 * @param string $pageDescription
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPageDescription($pageDescription) {
		$this->_pageDescription = (string) $pageDescription;
		return $this;
	}

	/**
	 * Set the pagetitle
	 *
	 * @access public
	 * @param string $pageTitle
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setPageTitle($pageTitle) {
		$this->_pageTitle = (string) $pageTitle;
		return $this;
	}

	/**
	 * Set the active path
	 *
	 * @param string $arg
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setActivePath($arg) {
		$this->_activePath = (string) $arg;
		return $this;
	}

	/**
	 * Set Canonical URI
	 *
	 * @see PageView::_getCanonicalUri()
	 * @param M_Uri $uri
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setCanonicalUri(M_Uri $uri) {
		$this->_canonicalUri = $uri;
		return $this;
	}

	/**
	 * Set View Mode
	 *
	 * @see PageView::VIEW_MODE_IFRAME
	 * @see PageView::VIEW_MODE_AJAX_INLINE
	 * @see PageView::VIEW_MODE_NORMAL
	 * @access public
	 * @param string $viewMode
	 * @return PageView $view
	 *		Returns itself, for a fluent programming interface
	 */
	public function setViewMode($viewMode) {
		// Make sure that the view mode is supported:
		if(! in_array($viewMode, array(self::VIEW_MODE_NORMAL, self::VIEW_MODE_IFRAME, self::VIEW_MODE_AJAX_INLINE))) {
			// If not, we throw an exception to inform about the error
			throw new M_Exception(sprintf(
				'The view mode "%s" is not supported by %s',
				$viewMode,
				__CLASS__
			));
		}

		// Set the view mode:
		$this->_viewMode = (string) $viewMode;

		// Returns itself;
		return $this;
	}

	/* -- GETTERS -- */

	/**
	 * Get the page description
	 *
	 * @access public
	 * @return string
	 */
	public function getPageDescription() {
		// Get SEO Entry
		$seo = $this->_getSeo();

		// Return the page description:
		return $seo ? $seo->getDescription() : $this->_pageDescription;
	}

	/**
	 * Get the pagetitle
	 *
	 * @return string
	 */
	public function getPageTitle() {
		// Get SEO Entry
		$seo = $this->_getSeo();

		// Return the page title:
		return $seo ? $seo->getTitle() : $this->_pageTitle;
	}

	/**
	 * Get the active path
	 *
	 * Note: if no path has been set, this method will ask and return the
	 * path from {@link M_ControllerDispatcher}
	 *
	 * @return string
	 */
	public function getActivePath() {
		if(! $this->_activePath) {
			return M_ControllerDispatcher::getInstance()->getPath();
		} else {
			return $this->_activePath;
		}
	}

	/**
	 * Get the canonical uri
	 *
	 * When multiple pages hold the same content, you can set use a unique
	 * canonical uri: this way robots such as Google know both pages are actually
	 * the same.
	 *
	 * By doing this you will not be penalized for serving duplicate content
	 *
	 * Canonical urls can be used for e.g. when using a pagejumper or other
	 * query variables (sorting, filters). Also http://www.url.be and http://www.url.be/home
	 * could both use one canonical url (http://www.url.be)
	 *
	 * If no {@link M_Uri} is set through {@link PageController::__setCanonicalUrl()}
	 * the active uri without query variables will be used.
	 *
	 * To use the canonical-url in your template you can use the following syntax:
	 *
	 * <link rel="canonical" href="http://www.url.be/path/to/page" />
	 *
	 * @see http://googlewebmastercentral.blogspot.com/2009/02/specify-your-canonical.html
	 * @author Ben Brughmans
	 * @return M_Uri|NULL
	 */
	public function getCanonicalUri() {
		// If a Canonical URI has not been provided to the view:
		if(! $this->_canonicalUri) {
			// Then, we will return the active URI, but without any query variables.
			// That is, of course, if query variables are present:
			$uri = M_Request::getUri();

			// Does the URI have query variables?
			if($uri->hasQueryVariables()) {
				// If so, we remove the query variables:
				$uri->removeQueryVariables();

				// And use the resulting URI as the canonical one:
				$this->_canonicalUri = $uri;
			}
		}

		// Return the Canonical URI:
		return $this->_canonicalUri;
	}

	/**
	 * Get View Mode
	 *
	 * Will provide with the view mode that has been defined for this view
	 * previously with {@link PageView}. Note that, if no view mode has been
	 * specificed to this object explicitely, this method will listen to the
	 * request variable with name 'viewmode'. Depending on the value of that
	 * variable, the view will be set to a given mode:
	 *
	 * <code>'iframe'</code>
	 *
	 * If the variable carries this value, then the view mode will be set to
	 * {@link PageView::VIEW_MODE_IFRAME}
	 *
	 * <code>'ajax-inline'</code>
	 *
	 * If the variable carries any of these value, then the view mode will be
	 * set to {@link PageView::VIEW_MODE_AJAX_INLINE}
	 *
	 * <code>'normal'</code>
	 *
	 * If the variable carries any of these value, then the view mode will be
	 * set to {@link PageView::VIEW_MODE_NORMAL}
	 *
	 * @access public
	 * @return string $viewMode
	 */
	public function getViewMode() {
		// If no view mode has been set
		if(! $this->_viewMode) {
			// Then, we define it now:
			$this->setViewMode(M_Request::getVariable('viewmode', self::VIEW_MODE_NORMAL));
		}

		// Return the view mode:
		return $this->_viewMode;
	}

	/* -- PRIVATE/PROTECTED -- */
	
	/**
	 * Render HTML
	 *
	 * @see M_ViewHtml::_render()
	 * @return string
	 */
	protected function _render() {
		// Get the wrapper
		$rs = $this->_getResourceWrapper();

		// Assign presentation variables to the wrapper:
		$rs->assign('pageTitle',       $this->getPageTitle());
		$rs->assign('pageDescription', $this->getPageDescription());
		$rs->assign('activePath',      $this->getActivePath());
		$rs->assign('identity',        M_ApplicationIdentity::getInstance()->getContact());
		$rs->assign('canonicalUri',    $this->getCanonicalUri());

		// The render of the to-be-wrapped template:
		$rs->assign('content',         parent::_render());

		// System: Languages
		$rs->assign('languages', M_LocaleMessageCatalog::getInstalled());

		// System: Persistent variables
		$rs->assign('persistent',      self::getPersistentVariables());

		// System: Pending messages
		$messenger = M_Messenger::getInstance();
		$rs->assign('messages', $messenger->getMessages());
		$messenger->clearMessages();

		// System: Browser info
		$this->assignPersistent('browser', array(
			'id'      => M_Browser::getName() . M_Browser::getMajorVersion(),
			'name'    => M_Browser::getName(),
			'version' => array(
				'major' => M_Browser::getMajorVersion(),
				'minor' => M_Browser::getMinorVersion()
			)
		));
		
		// Return the render of the wrapper template
		return $rs->fetch();
	}

	/**
	 * Get {@link Seo}
	 *
	 * Will fetch the {@link Seo} Data Object that has been created for the
	 * specific request. Will use {@link M_Request::getUri()} to do so...
	 *
	 * @access protected
	 * @return Seo
	 */
	protected function _getSeo() {
		// If not requested before:
		if($this->_seo === 0) {
			// If the SEO Module has been installed in the application:
			if(M_Application::isModuleInstalled('seo')) {
				// Prepare the mapper with which we look up SEO entries:
				$mapper = M_Loader::getDataObjectMapper('Seo', 'seo');

				/* @var $mapper SeoMapper */
				// Ask the mapper for the SEO entry;
				$this->_seo = $mapper
					->getByField('url', M_Request::getUri()->getRelativePath())
					->current();
			}
		}

		// Return the SEO Entry
		return $this->_seo;
	}
	
	/**
	 * Get wrapper resource
	 *
	 * @access protected
	 * @return M_ViewHtmlResource
	 */
	protected function _getResourceWrapper() {
		// If not requested before:
		if(is_null($this->_resourceWrapper)) {
			// Then, construct the wrapper now. Note that the wrapper of the 
			// view depends on the view mode:
			switch($this->getViewMode()) {
				// Iframe
				case self::VIEW_MODE_IFRAME:
					$this->_resourceWrapper = M_ViewHtmlResource::constructWithTemplateBasePath('PageIframe.tpl');

				// Inline view, probably loaded by AJAX
				case self::VIEW_MODE_AJAX_INLINE:
					$this->_resourceWrapper = M_ViewHtmlResource::constructWithTemplateBasePath('PageAjaxInline.tpl');

				// Normal view (default):
				case self::VIEW_MODE_NORMAL:
				default:
					$this->_resourceWrapper = M_ViewHtmlResource::constructWithTemplateBasePath('Page.tpl');
			}
		}

		// Return the wrapper template:
		return $this->_resourceWrapper;
	}
}