<?php
// M_ControllerDispatcher Routing Rules:
$routing = array(
	'projecten' => 'portfolio/project',
	'projecten/(:string)' => 'portfolio/project/$1',
	'relaxzwembaden' => 'portfolio/album',
	'relaxzwembaden/(:string)' => 'portfolio/album/$1',
	'nieuws(:any)' => 'news$1'
);