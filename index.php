<?php

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
//die('website is even in onderhoud');
// Load M_Loader
require_once 'bootstrap-secure.php';
require_once 'config/routing.php';
require_once 'core/Object/Object.class.php';
require_once 'core/M.class.php';
require_once 'core/Loader.class.php';
require_once 'ControllerDispatcherEventListener.class.php';

// Set M_Loader as the autoloader
M_Loader::setAsAutoloader();

// Set date time zone
M_DateTimezone::setDefaultTimezone('Europe/Brussels');

// Use our own exception handler
M_ExceptionErrorHandler::start();

// Set encoding
$header = new M_Header();
$header->send(M_Header::CONTENT_TYPE_TEXT_HTML, 'charset=utf-8');

// Load configuration of the website:
// - Database connection(s)

M_Loader::loadRelative('config/db-connection.php');

// Start the session:
$session = M_Session::getInstance();
$session->start();

// Check if this is https, if not: redirect to https
$uri = M_Request::getUri();
$host = $uri->getHost();
if(! in_array($host, array("localhost","www.relaxzwembaden.local")) && $uri->getScheme() != 'https') {
    $uri->setScheme('https');
    M_Header::redirect($uri, 301);
}
// Register the default database connection (not yet establishing
// the connection though)
M_Db::registerConnectionId(DB_DRIVER, DB_URL);

// Register database connection to central database of errors (exceptions)
M_Db::registerConnectionId(DB_DRIVER_EXCEPTION, DB_URL_EXCEPTION, DB_ID_EXCEPTION);

// Create a logger of exceptions, and set it up in such a way that errors get
// logged in the central database. The connection to the database is configured
// in config/db-connection.php
$logger = new M_ExceptionLoggerDb();
$logger->setDbId(DB_ID_EXCEPTION);

// Set the logger
M_Exception::setExceptionLogger($logger);

// Construct an instance of the M_ControllerDispatcher class, to handle
// the page request:
$dispatcher = M_ControllerDispatcher::getInstance();
// Get installed locales:
$locales = M_LocaleMessageCatalog::getInstalled();
//die('website is in maintenance');

// If more than one locale is available, then we will select the currently active
// locale by looking at the browser or the URI. If only one locale is active
// though, we do not include the locale name in links of the website, and we do
// not fetch the browser locale. IMPORTANT NOTE: if you want to include the locale
// in (internal) links of the website, then replace the if() below by an if(TRUE)
if(count($locales) > 1) {
	// We set the homepage of the website/application. In order to do so, we first
	// detect the preferred locale from the current visitor's browser...
	$locale = M_Browser::getLocaleName();

	// If that locale is not supported by the website/application
	if(! M_LocaleMessageCatalog::isInstalled($locale)) {
		// Then, we default the locale to the first one we can find in the current
		// installation:
		$locale  = $locales[0];
	}

	// Set the Homepage URI
	$dispatcher->setHomepageUri(new M_Uri(M_Request::getLink($locale . '/home')));

	// We set up a listener for the controller dispatcher:
	$dispatcher->addEventListener(
		M_ControllerDispatcherEvent::RECEIVE_PATH,
		array(new ControllerDispatcherEventListener(), 'onReceivePath')
	);
}
// If only one locale can be used in the app:
else {
	// Set the Homepage URI (without locale prefix)
	$dispatcher->setHomepageUri(new M_Uri(M_Request::getLink('home')));

	// Set the active locale
	M_Locale::setCategory(M_Locale::LANG, $locales[0]);
}

// Set a cache object, to optimize the message catalog:
M_LocaleMessageCatalog::setCacheObject(
	new M_CachePHP(
		M_Loader::getAbsolute('files/cache')
	)
);

// Set up routing
$dispatcher->setRoutingRules($routing);

// Dispatch
$dispatcher->dispatch();